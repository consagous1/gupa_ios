//
//  AppConstant.swift
//

import Foundation
import UIKit

class AppConstant: NSObject {
    internal let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    internal static let IPHONE_X_DEFAULT_NAVIGATION_BAR_HEIGHT:CGFloat = 88 + 24
    
    enum UIUserInterfaceIdiom : Int
    {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.nativeBounds.width
        static let SCREEN_HEIGHT        = UIScreen.main.nativeBounds.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 1136.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 1136.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 1334.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 2208.0
        static let IS_IPHONE_X  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 2436.0
    }
    
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
                isSim = true
            #endif
            return isSim
        }()
    }
    
    class func klogString(_ message: String,
                          function: String = #function,
                          file: String = #file,
                          line: Int = #line) {
        if(APIParamConstants.DEFAULT_ENV != APIParamConstants.Env.PRODUCTION.rawValue){
            print("Message \"\(message)\" (File: \(file), Function: \(function), Line: \(line))")
        }
    }
    
    class func kLogString(_ items: Any...){
        if(APIParamConstants.DEFAULT_ENV != APIParamConstants.Env.PRODUCTION.rawValue){
            print(items)
        }
    }
    
    class func klogInteger(_ logMessage: NSInteger, functionName: String = #function) {
        if(APIParamConstants.DEFAULT_ENV != APIParamConstants.Env.PRODUCTION.rawValue){
            print("\(functionName): \(logMessage)")
        }
    }
    class func klogNumber(_ logMessage: NSNumber, functionName: String = #function) {
        if(APIParamConstants.DEFAULT_ENV != APIParamConstants.Env.PRODUCTION.rawValue){
            print("\(functionName): \(logMessage)")
        }
    }
    
    class func getDeviceCurrentTimeMilliSeconds()-> CLong{
        return (CLong)(Date().timeIntervalSince1970*1000)
    }
    
    class func getDeviceCurrentTimeMilliSecondsForHttpRequest()-> CLong{
        return (CLong)(Date().timeIntervalSince1970)
    }
    
    //Default REST Error Code and Message
    internal static let DEFAULT_REST_ERROR_CODE = "default_rest_error_code"
    internal static let DEFAULT_REST_ERROR_MESSAGE = "Unable to process your request.\nPlease try again later."
    
    //USER DEFAULT KEY'S
    internal static let userDefaultAppTokenKey = "appToken"
    internal static let userDefaultUserNameKey = "userName"
    internal static let userDefaultUserIdKey = "userId"
    internal static let userDefaultUserFullNameKey = "userFullName"
    
    internal static let kFcmToken = "fcm_token"
    internal static let userDefaultUserLoginInfoKey = "userLoginInfo"
    internal static let kNotificationCount = "NotificationCount"
    internal static let kChatCount = "ChatCount"
    internal static let kTotalChatCount = "TotalChatCount"
    internal static let kRefreshChatList = "RefreshChatList"
    internal static let kRefreshHomeList = "RefreshHomeList"
    internal static let kRefreshHomeListFromSocket = "RefreshHomeListFromSocket"
    internal static let kGetGroupInfoOnChatList = "GetGroupInfoOnChatList"
    internal static let kUserAddedToGroup = "userAddedToGroup"

    internal static let kUserLat = "lat"
    internal static let kUserLong = "long"

    //Permissions Data
    internal static let userPermissionisFileManager = "isFileManager"
    internal static let userPermissionisClearChat = "isClearChat"
    internal static let userPermissionisDeleteChat = "isDeleteChat"
    
    internal static let kDeviceType = "Apple"
    
    internal static let LOCAL_NOTIFICATION_IDENTIFIER = "localNotification"
    
    //LOCALITY VALUES
    internal static let localCountryName = "name"
    internal static let localCountryDialCode = "dial_code"
    internal static let localCountryCode = "code"
    
    //Storyboard Name 
    internal static let loginViewStoryboard = "LoginViewController"
    internal static let customAlertStoryboard = "ForgotPasswordViewController"
    internal static let homeViewStoryboard = "HomeViewController"
    internal static let popUpViewStoryboard = "PopUpVC"
    internal static let chatVCStoryboard = "ChatViewController"
    
    internal static let headerColorChangeConstant:CGFloat = 10

}
