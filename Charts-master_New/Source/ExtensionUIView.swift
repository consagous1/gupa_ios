//
//  ExtensionUIView.swift
//  Flum
//
//  Created by RajeshYadav on 21/12/16.
//  Copyright © 2016 47billion. All rights reserved.
//

import Foundation

extension UIView {
    /**
     *  Pins the subview of the receiver to the edge of its frame, as specified by the given attribute, by adding a layout constraint.
     *
     *  @param subview   The subview to which the receiver will be pinned.
     *  @param attribute The layout constraint attribute specifying one of `NSLayoutAttributeBottom`, `NSLayoutAttributeTop`, `NSLayoutAttributeLeading`, `NSLayoutAttributeTrailing`.
     */
    func msg_pinSubview(_ subview: UIView, toEdge attribute: NSLayoutAttribute, withConstant:CGFloat) {
        self.addConstraint(NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: subview, attribute: attribute, multiplier: 1.0, constant: withConstant))
    }
    
    /**
     *  Pins all edges of the specified subview to the receiver.
     *
     *  @param subview The subview to which the receiver will be pinned.
     */
    func msg_pinAllEdgesOfSubview(_ subview: UIView) {
        self.msg_pinSubview(subview, toEdge: .bottom, withConstant: 0.0)
        self.msg_pinSubview(subview, toEdge: .top, withConstant: 0.0)
        self.msg_pinSubview(subview, toEdge: .leading, withConstant: 0.0)
        self.msg_pinSubview(subview, toEdge: .trailing, withConstant: 0.0)
    }
    
    /**
     *  Pins all edges of the specified subview to the receiver.
     *
     *  @param subview The subview to which the receiver will be pinned.
     */
    func msg_pinAllEdgesOfMediaSubview(_ subview: UIView) {
        //self.msg_pinSubview(subview, toEdge: .bottom)
        self.msg_pinSubview(subview, toEdge: .top, withConstant: -5.0)
        self.msg_pinSubview(subview, toEdge: .leading, withConstant: 0.0)
        self.msg_pinSubview(subview, toEdge: .trailing, withConstant: 0.0)
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
