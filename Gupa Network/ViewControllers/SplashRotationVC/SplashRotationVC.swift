//
//  SplashRotationVC.swift
//  Gupa Network
//
//  Created by mac on 26/07/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class SplashRotationVC: UIViewController, UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        let path = Bundle.main.path(forResource: "logo", ofType: "html")
        let url = URL(fileURLWithPath: path!)
        let request = URLRequest(url: url)
        
        webView.loadRequest(request)
        self.view.addSubview(webView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.perform(#selector(SplashRotationVC.cancelWeb), with: nil, afterDelay: 1.0)
    }
    
    
    @objc func cancelWeb()
    {
        if let isLogin = AppTheme.getIsLogin() as? Bool {
            if isLogin == true{
                let newRegiVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(newRegiVC, animated: true)
            }else{
                let newRegiVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.navigationController?.pushViewController(newRegiVC, animated: true)
            }
        }
    }


}
