//
//  InviteController.swift
//  Gupa Network
//
//  Created by MWM23 on 11/3/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher
import AddressBook
import Contacts

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class InviteController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var src_user: UISearchBar!
    @IBOutlet weak var tbl_memberList: UITableView!
    @IBOutlet weak var btn_inviteAll: UIButton!
    @IBOutlet weak var btn_select_all: UIButton!
    @IBOutlet weak var btn_inviteByEmail: UIButton!
    
    //Alert view element
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var subAlertView: UIView!
    @IBOutlet weak var emailTextView: UITextField!
    @IBOutlet weak var cancelButtonActionOnAlert: UIButton!
    @IBOutlet weak var sendActionOnAlert: UIButton!
    
     var loading : JGProgressHUD! = JGProgressHUD()
     var arrOfResponseKey :  NSMutableArray = NSMutableArray()
     var adbk : ABAddressBook!
     var aryContacts : NSMutableArray = NSMutableArray()
     var isrespons : Bool = false
    
     var count = 0
     var array_checked_User : NSMutableArray = NSMutableArray()
     var arrSelectedCell : NSMutableArray = NSMutableArray()
     var arrSelectedEmails : NSMutableArray = NSMutableArray()
     var dict_checked_user_detail: NSDictionary = NSDictionary()
    
     static var isInvite : Bool = false
     var searchActive : Bool = false
     var filtered : NSArray = NSArray()
    
     var btnSearchUser:UIButton = UIButton()
     var btnFollow:UIButton = UIButton()
    
    
    //MARK: Controller life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.arrSelectedCell = NSMutableArray()
        
        appDelegate.centerNav = self.navigationController
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 66.0/255.0, green: 199.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        
        showRightNavigationBarButtonHide()
        
        getContacts()
        
       // checkUserExist()
        self.title = "Invite"
        
        alertView.isHidden = true
        cancelButtonActionOnAlert.layer.borderColor = UIColor.lightGray.cgColor
        cancelButtonActionOnAlert.layer.borderWidth = 0.5;
        cancelButtonActionOnAlert.layer.cornerRadius = 4
        
        sendActionOnAlert.layer.borderColor = UIColor.lightGray.cgColor
        sendActionOnAlert.layer.borderWidth = 0.5;
        sendActionOnAlert.layer.cornerRadius = 4
        
        self.tbl_memberList.estimatedRowHeight = 100
        self.tbl_memberList.rowHeight = UITableViewAutomaticDimension
        self.tbl_memberList.setNeedsLayout()
        self.tbl_memberList.layoutIfNeeded()
        self.tbl_memberList.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func showRightNavigationBarButtonHide()
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnSearchUser = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnSearchUser.setImage(imgChat, for: UIControlState())
        btnSearchUser.addTarget(self, action: #selector(InviteController.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnSearchUser)
        
        navigationItem.rightBarButtonItems = [btnForChat]
    }
    
    @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    

    //=====================================
    //MARK: UISearchBar delegate methods
    //=====================================
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
       // src_user.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        //src_user.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let resultPredicate = NSPredicate(format: "email contains[cd] %@", searchText)
        filtered = array_checked_User.filtered(using: resultPredicate) as NSArray
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tbl_memberList.reloadData()
    }
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
        if(searchActive) {
            row = filtered.count
            return filtered.count
        }
        if self.array_checked_User.count != 0
        {
            row =  self.array_checked_User.count
            return row
        }
            return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        self.loading.dismiss()
        var dic : NSDictionary! = NSDictionary()
        var cell: InviteCell! = tableView.dequeueReusableCell(withIdentifier: "InviteCell") as? InviteCell
        
        if cell == nil {
            cell = InviteCell(style: UITableViewCellStyle.default, reuseIdentifier: "InviteCell")
        }
        if(searchActive){
            dic = filtered.object(at: indexPath.row) as! NSDictionary
        } else {
            dic = self.array_checked_User.object(at: indexPath.row) as! NSDictionary
        }
        
        cell.button_invite.tag = indexPath.row
        cell.button_invite.setImage(UIImage(named: "img_uncheck-1.png"), for: UIControlState())
        
        for i in 0..<self.arrSelectedCell.count {
            if indexPath.row == Int(self.arrSelectedCell[i] as! String) {
                cell.button_invite.setImage(UIImage(named: "img_checkbox.png"), for: UIControlState())

            }
        }
        print(dic)
        if let status : NSInteger = dic .value(forKey: "status") as? NSInteger {
            if status == 1 {
                if let tempArray : NSArray = dic .value(forKey: "users") as? NSArray {
                    var dict_user : NSDictionary = NSDictionary()
                    for userDetail in tempArray {
                        dict_user = userDetail as! NSDictionary
                    }
                    
                    if let str_email : String = dict_user.value(forKey: "email") as? String{
                        cell.label_email.text = str_email
                    }
                    if let str_name = dict_user.value(forKey: "name") as? String{
                        cell.label_name.text = str_name as String
                    }
                    if let url = URL(string:(dict_user["profile_pic"] as? String)!) {
                        
                        cell.image_static.kf.setImage(with: url, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: nil)
                        
//                        cell.image_static.kf_setImageWithURL(url, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
                    }
                    InviteController.isInvite = false
                    cell.button_invite.setTitle("Follow", for: UIControlState())
                }
            }
        } else {
            InviteController.isInvite = true
            cell.label_name.text = dic .value(forKey: "name") as? String
            cell.label_email.text = dic .value(forKey: "email") as? String
        }
        cell.button_invite.addTarget(self, action: #selector(InviteController.inviteAction(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }

    
    @objc func inviteAction(_ sender: UIButton!)
    {
        let index = "\(Int(sender.tag))"
        var dic : NSDictionary! = NSDictionary()
        if(searchActive){
            dic = filtered.object(at: sender.tag) as! NSDictionary
        } else {
            dic = self.array_checked_User.object(at: sender.tag) as! NSDictionary
        }
        if dic.count == 2{
            if self.arrSelectedCell.contains(index) == true {
                arrSelectedCell.remove(index)
                self.arrSelectedCell.remove(self.arrSelectedCell.index(of: index))
                self.arrSelectedEmails.remove(self.array_checked_User[sender.tag])
            }
            else {
                self.arrSelectedCell.add(index)
                self.arrSelectedEmails.add(self.array_checked_User[sender.tag])
            }
            tbl_memberList.reloadData()
        }else{
            print("element is 3")
        }
        
    }
    
    
    @IBAction func inviteSelectUser(_ sender: AnyObject) {
        
        let arr_sender_email : NSMutableArray = NSMutableArray()
        
        if arrSelectedEmails.count != 0 {
            for element in arrSelectedEmails {
                let dict  = element as! NSDictionary
                
                arr_sender_email.add(dict.value(forKey: "email")!)
            }
            if let str_sender_email = AppTheme.getLoginDetails().value(forKey: "email") {
                var stringRepresentation : String  = String()
              stringRepresentation =   arr_sender_email.componentsJoined(by: ", ")
              callInviteUser(stringRepresentation, senderEmail: str_sender_email as! String)
            }
        }
    }
    
    @IBAction func inviteAllUser(_ sender: AnyObject) {
    }
    
    func callInviteUser(_ email: String, senderEmail: String)  {
        
        if (!isNetworkAvailable) {
            self.loading.dismiss()
            return
        }

        let paraDict = NSMutableDictionary()
        paraDict.setValue(email, forKey:"email")
        paraDict.setValue(senderEmail, forKey:"sender_email")
        
        AppTheme().callPostService(String(format: "%@user_invite_i", AppUrl), param: paraDict) {(result, data) -> Void in                                                                  ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                self.searchActive = false
                self.src_user.resignFirstResponder()
                self.arrSelectedCell.removeAllObjects()
                self.tbl_memberList.reloadData()
            }else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
    
    //MARK:- Call contact/email from device
    
    func determineStatus() -> Bool {
        let status = ABAddressBookGetAuthorizationStatus()
        switch status {
        case .authorized:
            return self.createAddressBook()
        case .notDetermined:
            var ok = false
            ABAddressBookRequestAccessWithCompletion(nil)
            {
                (granted:Bool, err:CFError!) in
                DispatchQueue.main.async
                {
                    if granted
                    {
                        ok = self.createAddressBook()
                    }
                }
            }
            if ok == true {
                return true
            }
            self.adbk = nil
            return false
        case .restricted:
            self.adbk = nil
            return false
        case .denied:
            self.adbk = nil
            return false
        }
    }

    
    func createAddressBook() -> Bool {
        if self.adbk != nil {
            return true
        }
        var err : Unmanaged<CFError>? = nil
        let adbk : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &err).takeRetainedValue()
        if adbk == nil {
            print(err)
            self.adbk = nil
            return false
        }
        self.adbk = adbk
        return true
    }
    
    
    func getContactNames() {
        if !self.determineStatus() {
            print("not authorized")
            return
        }
        let people = ABAddressBookCopyArrayOfAllPeople(adbk).takeRetainedValue() as NSArray as [ABRecord]
        for person in people {
            
            if let personDetails = ABRecordCopyCompositeName(person)
            {
                //print(personDetails)
                if let personName : String = personDetails.takeRetainedValue() as String
                {
                    print(personName)
                }
            }
            
            //            if(ABRecordCopyCompositeName(person).takeRetainedValue() == "Mami Surat")
            //            {
            //                print("Alert here comes the crash")
            //            }
            //
            //            print(ABRecordCopyCompositeName(person).takeRetainedValue())
        }
    }
    
    
    func getContacts() {
        let store = CNContactStore()
        
        if CNContactStore.authorizationStatus(for: .contacts) == .notDetermined {
            store.requestAccess(for: .contacts, completionHandler: { (authorized: Bool, error: NSError?) -> Void in
                if authorized {
                    self.retrieveContactsWithStore(store: store)
                }
                } as! (Bool, Error?) -> Void)
        } else if CNContactStore.authorizationStatus(for: .contacts) == .authorized {
            self.retrieveContactsWithStore(store: store)
        }
    }
    
        func retrieveContactsWithStore(store: CNContactStore) {
            
            do {
                let groups = try store.groups(matching: nil)
                
                if groups.count > 0 {
                    
                    let predicate = CNContact.predicateForContactsInGroup(withIdentifier: groups[0].identifier)
                    //let predicate = CNContact.predicateForContactsMatchingName("John")
                    let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactEmailAddressesKey] as [Any]
                    
                    let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                    
                    for contact in contacts
                    {
                        let formatter = CNContactFormatter()
                        let name: String =  (formatter.string(from: contact))!
                        let email: String =  (contact.emailAddresses.first?.value as String?)!
                        let dict: NSMutableDictionary = NSMutableDictionary()
                        dict.setValue(email, forKey: "email")
                        dict.setValue(name, forKey: "name")
                        aryContacts.add(dict)
                    }
                    
                    DispatchQueue.main.async {
                        
                    }
                }
             
            } catch {
                print(error)
            }
        }
        
   
    func processContactNames() -> NSArray
    {
        var errorRef: Unmanaged<CFError>?
        let addressBook: ABAddressBook? = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        let allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as [ABRecord]
        
        let aryPhoneContacts : NSMutableArray = NSMutableArray()
        let aryTemp : NSMutableArray = NSMutableArray()
        
        for person: ABRecord in allPeople as [ABRecord]{
            if let firstName = ABRecordCopyValue(person, kABPersonFirstNameProperty) {
                if let lastName = ABRecordCopyValue(person, kABPersonLastNameProperty) {
                    let ln:String = (lastName.takeRetainedValue() as? String) ?? ""
                    let fn:String = (firstName.takeRetainedValue() as? String) ?? ""
                    print("\(ln) - \(fn)")
                }
                
                if let userEmail = ABRecordCopyValue(person, kABPersonEmailProperty) {
//                    aryPhoneContacts
                }
                
            }
         }
        return aryTemp
    }
    
    func isValidEmail(_ testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let range = testStr.range(of: emailRegEx, options:.regularExpression)
        let result = range != nil ? true : false
        return result
    }
    
    func processAddressbookRecord(_ addressBookRecord: ABRecord) -> NSMutableDictionary
    {
        var contactName: String = ""//ABRecordCopyCompositeName(addressBookRecord).takeRetainedValue() as NSString as String
        
        if let personDetails = ABRecordCopyCompositeName(addressBookRecord)
        {
            //print(personDetails)
            if let personName : String = personDetails.takeRetainedValue() as String
            {
                contactName = personName
            }
        }
        
        NSLog("contactName: \(contactName)")
        //processEmail(addressBookRecord)
        let dictDetails : NSMutableDictionary = NSMutableDictionary()
        if(contactName != "")
        {
            let tempArry : NSMutableArray = processEmail(addressBookRecord)
            if(tempArry.count > 0)
            {
                dictDetails.setObject(contactName, forKey: "name" as NSCopying)
                dictDetails.setObject(tempArry.object(at: 0), forKey: "email" as NSCopying)
            }
        }
        return dictDetails
    }
    
    func processEmail(_ addressBookRecord: ABRecord) -> NSMutableArray
    {
        let emailArray:ABMultiValue = extractABEmailRef(ABRecordCopyValue(addressBookRecord, kABPersonEmailProperty))!
        let arEmailList : NSMutableArray = NSMutableArray()
//        for elemen in  emailArray {
//           
//        }
        //Rajesh Block
//        for j in 0..< ABMultiValueGetCount(emailArray)
//            {
//                let emailAdd = ABMultiValueCopyValueAtIndex(emailArray, j)
//                let myString : String = extractABEmailAddress(emailAdd)!
//                var isEmail: Bool {
//                    do {
//                        let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
//                        return regex.firstMatch(in: myString, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, myString.characters.count)) != nil
//                    } catch {
//                        return false
//                    }
//
//                }
//            if(isEmail)
//            {
//                arEmailList.add(myString)
//            }
//            NSLog("email: \(myString)")
//
//        }
        return arEmailList
    }
    
    func extractABAddressBookRef(_ abRef: Unmanaged<ABAddressBook>!) -> ABAddressBook? {
        if let ab = abRef
        {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
        }
        return nil
    }
    
    func extractABEmailRef (_ abEmailRef: Unmanaged<ABMultiValue>!) -> ABMultiValue? {
        if let ab = abEmailRef
        {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
        }
        return nil
    }
    
    func extractABEmailAddress (_ abEmailAddress: Unmanaged<AnyObject>!) -> String? {
        if abEmailAddress != nil
        {
            return Unmanaged.fromOpaque(abEmailAddress.toOpaque()).takeUnretainedValue() as CFString as String
        }
        return nil
    }
    
    //MARK:- Update user existing and call invite them
    func checkUserExist() {
        
        if (!isNetworkAvailable) {
            return
        }
//        if isRechableToInternet() == false {
//            return
//        }
        if count < aryContacts.count
        {
            dict_checked_user_detail = aryContacts.object(at: count) as! NSDictionary
            if let str_email : String = dict_checked_user_detail .value(forKey: "email") as? String{
              //  if let str_email_sender : String = AppTheme.getUserEmail() as String{
                 //   callFunctionCheckExistUser(str_email, senderEmail: str_email_sender)
                    count += 1
               // }
            }
        } else {
            tbl_memberList.reloadData()
        }
    }
    
    //Check user exist call method
//    func callFunctionCheckExistUser(_ email_check: String, senderEmail: String){
////        let arr_sender_email : NSMutableArray = NSMutableArray()
////        arr_sender_email.addObject(self.emailTextView.text!)
////        var stringRepresentation : String  = String()
////        stringRepresentation =   arr_sender_email.componentsJoinedByString(", ")
//
//                let parameters : [String: AnyObject] = ["email" : email_check as AnyObject,
//                                                        "sender_email" : senderEmail as AnyObject]
//
//                AppTheme().callGetCode(String(format: "%@exists_user?", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
//                    let array : NSMutableArray = NSMutableArray()
//                    if result == "success"
//                    {
//                        self.loading.dismiss()
//                        var respDict : NSDictionary = NSDictionary()
//                        respDict = data as! NSDictionary
//                        print(respDict)
//                        if let status : NSInteger = respDict.value(forKey: "status") as? NSInteger  {
//                            if status == 1 {
//                                self.dict_checked_user_detail = respDict
//                            }
//                        }
//                        self.array_checked_User.add(self.dict_checked_user_detail)
//                        array.add(self.dict_checked_user_detail)
//                        self.tbl_memberList.reloadData()
//                        self.checkUserExist()
//                    }
//                    else  {
//                        self.loading.dismiss()
//                       // self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:  "", showInView: self.view)
//                    }
//                }
//     }
    @IBAction func btn_select_all(_ sender: AnyObject) {
        if self.btn_select_all.tag == 0 {
            self.btn_select_all.tag = 1
            self.btn_select_all.setImage(UIImage(named: "img_checkbox.png")!, for: UIControlState())
           
            for i in 0..<self.array_checked_User.count {
                arrSelectedCell.add("\(i)")
                }
            self.tbl_memberList.reloadData()
        }
        else {
            self.btn_select_all.tag = 0
            self.btn_select_all.setImage(UIImage(named: "img_uncheck-1.png")!, for: UIControlState())
            self.arrSelectedCell.removeAllObjects()
//            self.arrSelectedSong.removeAllObjects()
            self.tbl_memberList.reloadData()
        }
    }
    
    //MARK :- Invite by email and popup actions
    @IBAction func btn_invite_by_email(_ sender: AnyObject) {
        alertView.isHidden = false
    }
    
    @IBAction func btn_send_alertview(_ sender: AnyObject) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
//        if isRechableToInternet() == false {
//            return
//        }
        if emailTextView.text == "" || !AppTheme().isValidEmail(emailTextView.text as String!)
        {
            if  !AppTheme().isValidEmail(emailTextView.text as String!) == true{
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter right email in format.", showInView: self.view)
            } else
            {
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Email.", showInView: self.view)
            }
        }
        else
        {
            let arr_sender_email : NSMutableArray = NSMutableArray()
            arr_sender_email.add(self.emailTextView.text!)
            if let str_sender_email = AppTheme.getLoginDetails().value(forKey: "email") {
                var stringRepresentation : String  = String()
                stringRepresentation =   arr_sender_email.componentsJoined(by: ", ")
                callInviteUser(stringRepresentation, senderEmail: str_sender_email as! String)
            }
            
        }
    }
      
    @IBAction func btn_cancel_alert(_ sender: AnyObject) {
        alertView.isHidden = true
    }
}
