//
//  TransportController.swift
//  Gupa Network
//
//  Created by MWM23 on 12/6/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher
import MediaPlayer
import AssetsLibrary
import MobileCoreServices
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class TransportController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIGestureRecognizerDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var src_sourse: UISearchBar!
    @IBOutlet weak var src_destination: UISearchBar!
    
    @IBOutlet weak var tblTransportList: UITableView!
    @IBOutlet weak var tblSourceList: UITableView!
    @IBOutlet weak var tblDestinationList: UITableView!
    
    @IBOutlet weak var view_sourceContainer: UIView!
    @IBOutlet weak var view_destinationContainer: UIView!
    
    //Review Pop Up Elements=============================
    @IBOutlet weak var viewForCity: UIView!
    @IBOutlet weak var tbl_city: UITableView!
    @IBOutlet weak var btnCancelSelection: UIButton!
    @IBOutlet weak var lblSelectListTitle: UILabel!
    
    
    @IBOutlet weak var viewFor_bg_popup: UIView!
    @IBOutlet weak var view_container_for_review_popup: UIView!
    @IBOutlet weak var view_container_for_checkInCheckOut: UIView!
    @IBOutlet weak var viewForDatePicker: UIView!
    @IBOutlet weak var timePicker:UIDatePicker!
    @IBOutlet weak var txtNameOnTicket: UITextField!
    @IBOutlet weak var viewFor_depart_arrival_cities: UIView!
    @IBOutlet weak var viewFor_depart_arrival_time: UIView!
    @IBOutlet weak var txtBusName: UITextField!
    @IBOutlet weak var txtTicketNumber: UITextField!
    @IBOutlet weak var textview_arrival_city: UITextView!
    @IBOutlet weak var btnImageCapture: UIButton!
    @IBOutlet weak var lbl_select_image: UILabel!
    
    @IBOutlet weak var textview_departure_city: UITextView!
    @IBOutlet weak var textview_arrival_time: UITextView!
    @IBOutlet weak var textview_depart_time: UITextView!
    @IBOutlet weak var textview_description: UITextView!
    
    @IBOutlet weak var txtBusNumber: UITextField!
    @IBOutlet weak var txtDepartStation: UITextField!
    @IBOutlet weak var txtArrivalStation: UITextField!
   // @IBOutlet weak var textview_description: UITextView!
    
    @IBOutlet weak var btnCheckForConnectToBusCompany: UIButton!
    @IBOutlet weak var btnCancelCheckInCheckOut: UIButton!
    @IBOutlet weak var btnSubmitCheckInCheckOut: UIButton!
    
    var connectedLike: String = "0"
    var buttonSelected : UserDefaults!
    var timeString : String = String()
    var isTransportSearching : Bool = false
    var str_Transport_id : String = String()
    var isCamera : Bool = false
    
    var picker:UIImagePickerController?=UIImagePickerController()
    var chosenImage: UIImage!
    var imageVideoData: Data = Data()
    //====================================================
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var arr_transportList : NSMutableArray! = NSMutableArray()
    var arr_sourceList : NSMutableArray! = NSMutableArray()
    var arr_destinationList : NSMutableArray! = NSMutableArray()
    
    var btnSearchUser:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    
    var searchActive : Bool = false
    var selectedSearchBar : NSInteger = 105
    var str_source_id : String = String()
    var str_destination_id : String = String()
    
    
    var filtered : NSArray = NSArray()
    var iataCode: String!
    
    let identifier = "TransportCell"
    let identifier_sourceDestination = "CommentSearchCell"
   
    //MARK:- Viewcontroller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
        self.title = "Road Transport"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        if let str : UIBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController()) {
            self.navigationItem.leftBarButtonItem = str
        }
        textview_arrival_time.delegate = self
        textview_depart_time.delegate = self
        showRightNavigationBarButtonHide()
        
        view_sourceContainer.isHidden = true
        view_destinationContainer.isHidden = true
        tblTransportList.isHidden = true
        //Manage tableview cell height automatically
        tableCellHeight()
        tblTransportList.register(UINib(nibName: "TransportCell", bundle: nil),forCellReuseIdentifier: identifier)
        
        if iataCode == nil{
            if isGPSOn() == false{
                return
            }
        }
        if let str_aita_code : NSString =  AppTheme.getPresentLocation() {
            iataCode = str_aita_code as String
            self.src_sourse.text =  iataCode
            textview_departure_city.text = iataCode
        }
    }
    
    func writeReview()  {
        
        viewFor_bg_popup.isHidden = true
        view_container_for_review_popup.isHidden = true
        view_container_for_checkInCheckOut.isHidden = true
        viewForCity.isHidden = true
        
        self.viewFor_depart_arrival_time.layer.borderColor = UIColor.lightGray.cgColor
        self.viewFor_depart_arrival_time.layer.borderWidth = 1
        self.viewFor_depart_arrival_cities.layer.borderColor = UIColor.lightGray.cgColor
        self.viewFor_depart_arrival_cities.layer.borderWidth = 1
        
        self.textview_description.layer.cornerRadius = 5
        self.textview_description.layer.borderColor = UIColor.lightGray.cgColor
        self.textview_description.layer.borderWidth = 1
        self.textview_description.text = "Enter Description (Optional)."
        
        self.textview_arrival_city.text = "Arrival City"
        self.textview_depart_time.text = "Departure Time"
        self.textview_arrival_time.text = "Arrival Time"
    }

    func updateUI() {
        
        //Popup view design for review============
        self.view_container_for_review_popup.isHidden = true
        self.view_container_for_checkInCheckOut.layer.cornerRadius = 5
        self.view_container_for_checkInCheckOut.layer.masksToBounds = true
        
        self.btnCancelCheckInCheckOut.layer.cornerRadius = 2
        self.btnCancelCheckInCheckOut.layer.masksToBounds = true
        self.btnCancelCheckInCheckOut.layer.borderColor = UIColor.lightGray.cgColor
        self.btnCancelCheckInCheckOut.layer.borderWidth = 1
        
        self.btnSubmitCheckInCheckOut.layer.cornerRadius = 2
        self.btnSubmitCheckInCheckOut.layer.masksToBounds = true
        self.btnSubmitCheckInCheckOut.layer.borderColor = UIColor.lightGray.cgColor
        self.btnSubmitCheckInCheckOut.layer.borderWidth = 1
        
        //City detail view design for departure/arrival city=======================
        self.viewForCity.layer.cornerRadius = 5
        self.viewForCity.layer.masksToBounds = true
        
//        self.btnCancelSelection.layer.cornerRadius = 2
//        self.btnCancelSelection.layer.masksToBounds = true
//        self.btnCancelSelection.layer.borderColor = UIColor.lightGray.cgColor
//        self.btnCancelSelection.layer.borderWidth = 1
    }
    
    func tableCellHeight()  {
        
        self.tblTransportList.estimatedRowHeight = 100
        self.tblTransportList.rowHeight = UITableViewAutomaticDimension
        self.tblTransportList.setNeedsLayout()
        self.tblTransportList.layoutIfNeeded()

        
        self.tblSourceList.estimatedRowHeight = 100
        self.tblSourceList.rowHeight = UITableViewAutomaticDimension
        self.tblSourceList.setNeedsLayout()
        self.tblSourceList.layoutIfNeeded()
        
        self.tblDestinationList.estimatedRowHeight = 100
        self.tblDestinationList.rowHeight = UITableViewAutomaticDimension
        self.tblDestinationList.setNeedsLayout()
        self.tblDestinationList.layoutIfNeeded()
        
        tblTransportList.register(UINib(nibName: "TransportCell", bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    func showRightNavigationBarButtonHide()                               //RightNavigationBarButton hide unhide
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnSearchUser = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnSearchUser.setImage(imgChat, for: UIControlState())
        btnSearchUser.addTarget(self, action: #selector(TransportController.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnSearchUser)
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(TransportController.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
   @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        view_sourceContainer.isHidden = true
        view_destinationContainer.isHidden = true
         writeReview()
         updateUI()
    }
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
        if tableView == tblSourceList {
            if arr_sourceList.count != 0 {
                row =  self.arr_sourceList.count
                return row
            }
        }else if tableView == tblDestinationList{
            if arr_destinationList.count != 0 {
                row =  self.arr_destinationList.count
                return row
            }
        }else if tableView == tblTransportList{
            if self.arr_transportList.count != 0 {
                row =  self.arr_transportList.count * 2
                return row
            }
        } else if tableView == tbl_city{
            if selectedSearchBar == 108 {
                if arr_sourceList.count != 0 {
                    row =  self.arr_sourceList.count
                    return row
                }
            }else if selectedSearchBar == 109{
                if arr_destinationList.count != 0 {
                    row =  self.arr_destinationList.count
                    return row
                }
            }else if selectedSearchBar == 110{
                if arr_sourceList.count != 0 {
                    row =  self.arr_sourceList.count
                    return row
                }
            }
        } else{
            row =  0 //self.arr_transportList.count
            return row
        }
        return row
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        self.loading.dismiss()
       // var cell:UITableViewCell?
        if tableView == tblSourceList {
            
            var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
            if cell == nil {
                cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")}
            var dic: NSDictionary! = NSDictionary()
            dic = self.arr_sourceList.object(at: indexPath.row) as! NSDictionary
            if self.arr_sourceList.count > indexPath.row {
                if let str_city_name : String = dic .value(forKey: "location") as? String{
                    if let str_country = dic .value(forKey: "country") as? String{
                        cell.titleLabel.text = "\(str_city_name), \(str_country)"
                    }
                }
            }
            return cell!
        }
        if tableView == tblDestinationList {
            var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
            if cell == nil {
                cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")}
            var dic: NSDictionary! = NSDictionary()
            dic = self.arr_destinationList.object(at: indexPath.row) as! NSDictionary
            if self.arr_destinationList.count > indexPath.row {
                if let str_city_name : String = dic .value(forKey: "location") as? String{
                    if let str_country = dic .value(forKey: "country") as? String{
                        cell.titleLabel.text = "\(str_city_name), \(str_country)"
                    }
                }
            }
            return cell!
        } else if tableView == tblTransportList {
            var cell: TransportCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? TransportCell
            if cell == nil {
                cell = TransportCell(style: UITableViewCellStyle.default, reuseIdentifier: identifier)
            }

            var dic: NSDictionary! = NSDictionary()
            dic = self.arr_transportList.object(at: indexPath.row/2) as! NSDictionary
             if indexPath.row % 2 == 1 {
                cell.layer.cornerRadius=10 //set corner radius here
                cell.layer.borderColor = UIColor.lightGray.cgColor  // set cell border color here
                cell.layer.borderWidth = 2
                cell.lbl_trans_arrival_time.isHidden = true
                cell.lbl_trans_depart_time.isHidden = true
                cell.txtCall.isHidden = true
//                cell.lbl_trans_subtitle.hidden = true
                if let str_title : String = dic["transport_name"] as? String{
                    cell?.lbl_trans_title.text = str_title
                }
                if let str_add : String = dic["description"] as? String{
                    cell.lbl_trans_subtitle.text = str_add
                }
//                if let str_call = dic .valueForKey("phone_no") as? String {
//                    let contact_string: String = str_call
//                    let stringContact: String = contact_string.stringByReplacingOccurrencesOfString(",", withString: " , ")
//                    cell.txtCall.text = stringContact
//                }
                if let str_trans_depart_city : String = dic["destination_station"] as? String{
                    cell?.lbl_trans_arrival_city.text = str_trans_depart_city
                }
                if let str_trans_arrival_city : String = dic["source_station"] as? String{
                    cell.lbl_trans_depart_city.text = str_trans_arrival_city
                }
                if let str_profile_pic : String = dic["image"] as? String {
                    let url = URL(string:str_profile_pic)
                    
                    cell?.img_transport.kf.setImage(with: url, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: nil)
                    
                    
//                    cell?.img_transport.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
                }
                return cell!
             }else {
                var cell = tableView .dequeueReusableCell(withIdentifier: "cell")
                if cell == nil {
                    cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
                }
                cell!.isUserInteractionEnabled = false
                cell?.backgroundColor = UIColor.clear
                return cell!
            }
        } else if tableView == tbl_city{
            var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
            
            if cell == nil {
                cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
            }
            if selectedSearchBar == 108 {
                let temp_dic: NSDictionary = self.arr_sourceList.object(at: indexPath.row) as! NSDictionary
                if arr_sourceList.count > indexPath.row {
                    if let str_city_name : String = temp_dic .value(forKey: "location") as? String{
                        if let str_country = temp_dic .value(forKey: "country") as? String{
                            cell.titleLabel.text = "\(str_city_name), \(str_country)"
                        }
                    }
                }
                return cell
            }else if selectedSearchBar == 109{
                let temp_dic: NSDictionary = self.arr_destinationList.object(at: indexPath.row) as! NSDictionary
                if arr_destinationList.count > indexPath.row {
                    if let str_city_name : String = temp_dic .value(forKey: "location") as? String{
                        if let str_country = temp_dic .value(forKey: "country") as? String{
                            cell.titleLabel.text = "\(str_city_name), \(str_country)"
                        }
                    }
                }
            } else if selectedSearchBar == 110{
                let temp_dic: NSDictionary = self.arr_sourceList.object(at: indexPath.row) as! NSDictionary
                if arr_sourceList.count > indexPath.row {
                    if let str_transport_name : String = temp_dic .value(forKey: "transport_name") as? String{
                            cell.titleLabel.text = str_transport_name
                    }
                }
                return cell
            }
          return cell
        }else{
            var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "cell")!
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")}
            return cell!
        }        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var dic: NSDictionary! = NSDictionary()
        if tableView == tblSourceList {
            dic = self.arr_sourceList.object(at: indexPath.row) as! NSDictionary
            if let str_city_name = dic .value(forKey: "location") as? String {
                if selectedSearchBar == 106 {
                    self.src_sourse.text = str_city_name
                }else if selectedSearchBar == 108{
                    if let str_city_name = dic .value(forKey: "name") as? String {
                        self.textview_departure_city.text = str_city_name
                    }
                }
            }
            if let str_city_id = dic .value(forKey: "id") as? String {
                self.str_source_id = str_city_id
            }
            self.view_sourceContainer.isHidden = true
        }
        if tableView == tblDestinationList {
            dic = self.arr_destinationList.object(at: indexPath.row) as! NSDictionary
            if let str_city_name = dic .value(forKey: "location") as? String {
                if selectedSearchBar == 107 {
                   self.src_destination.text = str_city_name
                }else if selectedSearchBar == 109{
                    if let str_city_name = dic .value(forKey: "name") as? String {
                        self.textview_arrival_city.text = str_city_name
                    }
                }
            }
            if let str_city_id = dic .value(forKey: "id") as? String {
                self.str_destination_id = str_city_id
            }
            if selectedSearchBar == 107 {
                if src_sourse.text != "" || src_destination.text != "" || src_destination.text != nil || src_sourse.text != "" {
//                    if iataCode == nil{
//                        if isGPSOn() == false{
//                            self.loading.dismiss()
//                            return
//                        }
//                    }
                    src_destination.resignFirstResponder()
                    getTransportList(src_sourse.text!, destin_city: src_destination.text!)
                }
            }
            
            self.view_destinationContainer.isHidden = true
//            if str_source_id != "" && str_destination_id != ""{
//               getTransportList(str_source_id, destin_id: str_destination_id)
//            }
        }
        if tableView == tblTransportList {
            let destVC : TransportDetailController! // = <#value#>
            destVC = self.storyboard?.instantiateViewController(withIdentifier: "TransportDetailController") as! TransportDetailController
            dic = self.arr_transportList.object(at: indexPath.row/2) as! NSDictionary
            if let str_bus = dic .value(forKey: "bus_id") as? String {
               destVC.str_bus_id = str_bus
            }
            self.navigationController?.pushViewController(destVC, animated: true)
        } else if tableView == tbl_city{
            if selectedSearchBar == 108 {
                let dic: NSDictionary = self.arr_sourceList.object(at: indexPath.row) as! NSDictionary
                if let str_city_name  = dic .value(forKey: "location") as? String{
                    textview_arrival_city.text = str_city_name
                }
                if let str_city_id : String = dic .value(forKey: "id") as? String{
                    str_destination_id = str_city_id
                }
            }else if selectedSearchBar == 109{
                let dic: NSDictionary = self.arr_destinationList.object(at: indexPath.row) as! NSDictionary
                
                if let str_city_name  = dic .value(forKey: "location") as? String{
                    textview_departure_city.text = str_city_name
                }
                if let str_city_id : String = dic .value(forKey: "id") as? String{
                    str_source_id = str_city_id
                }
            }else if selectedSearchBar == 110 {
                let dic: NSDictionary = self.arr_sourceList.object(at: indexPath.row) as! NSDictionary
                if let str_city_name  = dic .value(forKey: "transport_name") as? String{
                    txtBusName.text = str_city_name
                }
                if let str_city_id : String = dic .value(forKey: "id") as? String{
                    str_Transport_id = str_city_id
                }
            }
            viewForCity.isHidden = true
        }
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblTransportList {
            var cell: TransportCell! = tableView.dequeueReusableCell(withIdentifier: "TransportCell") as? TransportCell
            if cell == nil {
                cell = TransportCell(style: UITableViewCellStyle.default, reuseIdentifier: "TransportCell")
            }
            tblTransportList.backgroundColor = UIColor.clear
            tblTransportList.separatorStyle = UITableViewCellSeparatorStyle.none
            tblTransportList.estimatedRowHeight = 100.0
            tblTransportList.rowHeight = UITableViewAutomaticDimension
            if indexPath.row%2 == 0 {
                return 10.0
            }
            return  tblTransportList.rowHeight
        }else {
            tbl_city.estimatedRowHeight = 100.0
            tbl_city.rowHeight = UITableViewAutomaticDimension
            return tbl_city.rowHeight
        }
    }
    //Call method for transport list
    func getTransportList(_ source_city : String, destin_city : String) {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
//        startLoader("Loading", showInView: self.view)
            let parameters : [String: AnyObject] =  ["departure_city" : source_city as AnyObject,
                                                     "arrival_city" : destin_city as AnyObject]
            AppTheme().callGetService(String(format: "%@transports", AppUrl), param: parameters) { (result, data) -> Void in
                
                if result == "success"{
                    self.loading.dismiss()
                    self.tblTransportList.isHidden = false
                    var respDict : NSDictionary = NSDictionary()
                    respDict = data as! NSDictionary
                    if let array_response : NSArray = respDict["response"] as? NSArray{
                        //arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                        self.arr_transportList = array_response.mutableCopy() as! NSMutableArray
                    }else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                        self.arr_transportList = array_Response_mutable
                    }
                    self.tblTransportList.reloadData()
                }else{
                    self.loading.dismiss()
//                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                    let refreshAlert = UIAlertController(title: "Enter Details.", message: " No bus found for this route. Do you want to enter bus detail?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                        self.viewFor_bg_popup.isHidden = false
                        self.view_container_for_review_popup.isHidden = false
                        self.view_container_for_checkInCheckOut.isHidden = false
                        self.viewForDatePicker.isHidden = true
                        self.viewFor_bg_popup.isUserInteractionEnabled = true
                    }))
                    
                    refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                        print("Handle Cancel Logic here")
                    }))
                    
                   self.present(refreshAlert, animated: true, completion: nil)
                }
            }
    }
    
    func getSourceList(_ searchText : String) {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        var str_api_formate : String = String()
        let parameters : [String: AnyObject]!
        
       
        if selectedSearchBar == 106 || selectedSearchBar == 108 {
            str_api_formate = "city_search" // "city_search_departure?"
        }else if selectedSearchBar == 107 || selectedSearchBar == 109 {
           str_api_formate = "city_search" // "city_search_arrival?"
        }
        parameters = ["search" : searchText as AnyObject]
        
        AppTheme().callGetService(String(format: "%@%@", AppUrl, str_api_formate), param: parameters) { (result, data) -> Void in
            
            if result == "success"{
                self.loading.dismiss()
                
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    if self.selectedSearchBar == 106{
                      self.view_sourceContainer.isHidden = false
                      self.arr_sourceList = array_response.mutableCopy() as! NSMutableArray
                        self.tblSourceList.reloadData()
                    } else if self.selectedSearchBar == 108{
                        self.view_sourceContainer.isHidden = false
                        self.arr_sourceList = array_response.mutableCopy() as! NSMutableArray
                        self.tbl_city.reloadData()
                    } else if self.selectedSearchBar == 107{
                        self.view_destinationContainer.isHidden = false
                        self.arr_destinationList = array_response.mutableCopy() as! NSMutableArray
                        self.tblDestinationList.reloadData()
                    }else if self.selectedSearchBar == 109{
                        self.view_destinationContainer.isHidden = false
                        self.arr_destinationList = array_response.mutableCopy() as! NSMutableArray
                        self.tbl_city.reloadData()
                    }
                }else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                    if self.selectedSearchBar == 106{
                        self.view_sourceContainer.isHidden = false
                        self.arr_sourceList = array_Response_mutable
                        self.tblSourceList.reloadData()
                    } else if self.selectedSearchBar == 107{
                        self.view_destinationContainer.isHidden = false
                        self.arr_destinationList = array_Response_mutable
                        self.tblDestinationList.reloadData()
                    }
                }
            }
        }
    }

    func getDestinationList(_ searchText : String) {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let parameters : [String: AnyObject] = ["search" : searchText as AnyObject]
        AppTheme().callGetService(String(format: "%@city_search_arrival", AppUrl), param: parameters) { (result, data) -> Void in
            
            if result == "success"{
                self.loading.dismiss()
                self.view_destinationContainer.isHidden = false
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arr_destinationList = array_response.mutableCopy() as! NSMutableArray
                }else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                    self.arr_destinationList = array_Response_mutable
                }
                self.tblDestinationList.reloadData()
            }
        }
    }
    
    //==================================
    // MARK: - textField Delegates
    //==================================
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtBusName{
        }
       
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtBusName {
            if(txtBusName.text?.characters.count > 0) {
               // viewForCity.hidden = true
            }else if textField == txtArrivalStation{
//                txtArrivalStation.resignFirstResponder()
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtBusName{
            if(txtBusName.text?.characters.count > 0 || string.characters.count > 0)
            {
                if isTransportSearching
                {
                    isTransportSearching = true
                    lblSelectListTitle.text = "Select Transport"
                    arr_sourceList.removeAllObjects()
                    tbl_city.reloadData()
                }
                var strSearch : String = ""
                if(txtBusName.text?.characters.count > 0)
                {
                    strSearch =  txtBusName.text! + string // "\(txtCity.text)\(string)"
                } else{
                    strSearch = string
                }
                self.viewForCity.isHidden = false
                selectedSearchBar = 110
                getTransportList(strSearch)
            }
        }
        return true
    }
    
    func getTransportList(_ searchText : String) {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let parameters : [String: AnyObject]!
        parameters = ["search" : searchText as AnyObject]
        AppTheme().callGetService(String(format: "%@transport_list", AppUrl), param: parameters) { (result, data) -> Void in
            
            if result == "success"{
                self.loading.dismiss()
                
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    if self.selectedSearchBar == 110{
                        self.view_sourceContainer.isHidden = false
                        self.viewForCity.isHidden = false
                        self.arr_sourceList = array_response.mutableCopy() as! NSMutableArray
                        self.tbl_city.reloadData()
                    } else if self.selectedSearchBar == 108{
                        self.view_sourceContainer.isHidden = false
                        self.arr_sourceList = array_response.mutableCopy() as! NSMutableArray
                        self.tbl_city.reloadData()
                    } else if self.selectedSearchBar == 107{
                        self.view_destinationContainer.isHidden = false
                        self.arr_destinationList = array_response.mutableCopy() as! NSMutableArray
                        self.tblDestinationList.reloadData()
                    }else if self.selectedSearchBar == 109{
                        self.view_destinationContainer.isHidden = false
                        self.arr_destinationList = array_response.mutableCopy() as! NSMutableArray
                        self.tbl_city.reloadData()
                    }
                }else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                    if self.selectedSearchBar == 106{
                        self.view_sourceContainer.isHidden = false
                        self.arr_sourceList = array_Response_mutable
                        self.tblSourceList.reloadData()
                    } else if self.selectedSearchBar == 107{
                        self.view_destinationContainer.isHidden = false
                        self.arr_destinationList = array_Response_mutable
                        self.tblDestinationList.reloadData()
                    }
                }
            }
        }
    }
    
    // MARK: - UITextview delegate methods
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == textview_description{
            if textview_description.text == "Enter Description (Optional)." {
                textview_description.text = ""
            }
        } else if textView == textview_arrival_city{
            if textview_arrival_city.text != "" {
                selectedSearchBar = 108
                getSourceList(textView.text)
            }
        }else if textView == textview_departure_city{
            if textview_departure_city.text != "" {
                selectedSearchBar = 109
                getSourceList(textView.text)
            }
        }
    }
    
    /*func textview(textView: UITextView) -> Bool {
        if (textView == textview_depart_time) || (textView == textview_arrival_time ) {
            if textView == textview_arrival_time {
                if textview_arrival_time.text == "Arrival Time" || textview_arrival_time.text != "Arrival Time" {
                    textview_arrival_time.resignFirstResponder()
                    textview_arrival_time.text = ""
                }
            }else if textView == textview_depart_time  {
                if textview_depart_time.text == "Departure Time" || textview_depart_time.text != "" {
                    textview_depart_time.resignFirstResponder()
                    textview_depart_time.text = ""
                }
            }
        }
        return true
    } */
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == textview_description  {
            if textview_description.text == "Enter Description (Optional)." {
                textview_description.text = ""
            }
        } else if textView == textview_arrival_city{
             self.viewForDatePicker.isHidden = true
            viewForCity.isHidden = false
            if textview_arrival_city.text == "Arrival City" {
                textview_arrival_city.text = ""
            }
        }else if textView == textview_departure_city{
             self.viewForDatePicker.isHidden = true
            viewForCity.isHidden = false
            if textview_departure_city.text != "Departure City" {
                textview_departure_city.text = ""
            }
        }
       /* if (textView == textview_arrival_time) || (textView == textview_depart_time ) {
            self.viewForDatePicker.hidden = false
            if textView == textview_arrival_time {
                textview_arrival_time.resignFirstResponder()
                textview_depart_time.resignFirstResponder()
                if textview_arrival_time.text == "Arrival Time" {
                    textview_arrival_time.text = ""
                    textview_arrival_time.tag = 5
                    textview_depart_time.tag = 101
                }
            }else{
                textview_depart_time.resignFirstResponder()
                textview_arrival_time.resignFirstResponder()
                if textview_depart_time.text == "Departure Time" {
                    textview_depart_time.text = ""
                    textview_depart_time.tag = 6
                    textview_arrival_time.tag = 100
                }
            }
            self.viewForDatePicker.hidden = false
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            var timeString: String! = String()
            timeString = dateFormatter.stringFromDate(timePicker.date)
            timePicker.datePickerMode = UIDatePickerMode.DateAndTime
            timePicker.addTarget(self, action: #selector(TransportController.dueDateChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        } */
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if (textView == textview_arrival_time ) || (textView == textview_depart_time ) {
            if textView == textview_arrival_time {
                if textview_arrival_time.text == "Arrival Time" || textview_arrival_time.text != "Arrival Time" {
                    textview_arrival_time.resignFirstResponder()
                    textview_arrival_time.text = ""
                    textview_arrival_time.tag = 5
                    textview_depart_time.tag = 101
                }
            }else if textView == textview_depart_time  {
                if textview_depart_time.text == "Departure Time" || textview_depart_time.text != "" {
                    textview_depart_time.resignFirstResponder()
                    textview_depart_time.text = ""
                    textview_depart_time.tag = 6
                    textview_arrival_time.tag = 100
                }
            }
            self.viewForDatePicker.isHidden = false
            self.timePicker.bringSubview(toFront: self.viewForDatePicker)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            self.timePicker.backgroundColor = UIColor.white
            timePicker.datePickerMode = UIDatePickerMode.dateAndTime
            textView.inputView = self.timePicker
            
            // ToolBar
            let toolBar = UIToolbar()
            toolBar.frame = CGRect(x: 0, y: 0, width: self.timePicker.frame.size.width, height: 44)
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
            toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
            
            // Adding Button ToolBar
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(TransportController.doneClick(_:)))
            let flexibleSpaceLeft:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            flexibleSpaceLeft.width = 120
            
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(TransportController.cancelClick(_:)))
            toolBar.setItems([cancelButton, flexibleSpaceLeft, doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            textView.inputAccessoryView = toolBar
            viewForDatePicker.addSubview(toolBar)
        }else{
           /* if textView == textview_arrival_city{
                viewForCity.hidden = false
                if textview_arrival_city.text == "Arrival City"  {
//                    textview_arrival_city.resignFirstResponder()
                    textview_arrival_city.tag = 9
                    textview_arrival_city.tag = 110
                    textview_arrival_city.text = ""
                }
            } else if textView == textview_departure_city {
                viewForCity.hidden = false
                if textview_departure_city.text != "Departure City" || textview_departure_city.text == "" {
//                    textview_departure_city.resignFirstResponder()
                    
                    textview_departure_city.tag = 10
                    textview_departure_city.tag = 111
                    textview_departure_city.text = ""
                }
            }else if textView == textview_description{
                if textview_description.text == "Enter Description (Optional)." {
                    textview_description.text = ""
                }
            } */
            return true
        }
        return false
    }
    
    
    @objc func cancelClick(_ sender:UIDatePicker){
        self.viewForDatePicker.isHidden = true
    }
    
    @objc func doneClick(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if textview_arrival_time.tag == 5 {
            textview_arrival_time.text = dateFormatter.string(from: timePicker.date)
        }else if textview_depart_time.tag == 6{
            textview_depart_time.text = dateFormatter.string(from: timePicker.date)
        }
        timePicker.resignFirstResponder()
        self.viewForDatePicker.resignFirstResponder()
        self.viewForDatePicker.isHidden = true
        textview_arrival_time.inputView = nil
        textview_arrival_time.delegate = self
        textview_arrival_time.endEditing(true)
        textview_depart_time.inputView = nil
        textview_depart_time.delegate = self
        textview_depart_time.endEditing(true)
    }
    
    func dueDateChanged(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if textview_arrival_time.tag == 5 {
            textview_arrival_time.text = dateFormatter.string(from: sender.date)
            
        }else if textview_depart_time.tag == 6{
            textview_depart_time.text = dateFormatter.string(from: sender.date)
        }
        self.viewForDatePicker.isHidden = true
//        self.timePicker.hidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == textview_description{
            if textview_description.text == "" {
                textview_description.text = "Enter Description (Optional)."
            }
        }else if textView == textview_arrival_city{
            if textview_arrival_city.text == "" {
                textview_arrival_city.text = "Arrival City"
            }
        }else if textView == textview_departure_city{
            if textview_departure_city.text == "" {
                textview_departure_city.text = "Departure City"
            }
        }else if (textView == textview_arrival_time) || (textView == textview_depart_time ) {
            textview_depart_time.resignFirstResponder()
            textview_arrival_time.resignFirstResponder()
            self.viewForDatePicker.isHidden = false
            if textView == textview_arrival_time {
                if textview_arrival_time.text == "" {
                    textview_arrival_time.resignFirstResponder()
                    textview_depart_time.resignFirstResponder()
                    textview_arrival_time.text = "Arrival Time"
                    textview_arrival_time.tag = 5
                    textview_depart_time.tag = 101
                }
            }else{
                if textview_depart_time.text == "" {
                    textview_depart_time.resignFirstResponder()
                    textview_arrival_time.resignFirstResponder()
                    textview_depart_time.text = "Departure Time"
                    textview_depart_time.tag = 6
                    textview_arrival_time.tag = 100
                }
            }
        }
    }
    //=====================================
    //MARK: UISearchBar delegate methods
    //=====================================
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar == src_sourse {
            selectedSearchBar = 106
            self.view_destinationContainer.isHidden = true
            src_destination.resignFirstResponder()
        }else{
            selectedSearchBar = 107
            self.view_sourceContainer.isHidden = true
            src_sourse.resignFirstResponder()
        }
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar == src_sourse {
            let tap = UITapGestureRecognizer(target: self, action: #selector(TransportController.handleTap(_:)))
            tap.delegate = self
            src_sourse.addGestureRecognizer(tap)
           print("in first search")
        }else{
        print("herer")
            let tap = UITapGestureRecognizer(target: self, action: #selector(TransportController.handleTap(_:)))
            tap.delegate = self
            src_destination.addGestureRecognizer(tap)
        }
        searchActive = false;
//                let tap = UITapGestureRecognizer(target: self, action: #selector(TransportController.handleTap(_:)))
//                tap.delegate = self
//                src_sourse.addGestureRecognizer(tap)

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == src_sourse {
            
        }else{
            
        }
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == src_sourse {
            
        }else{
            
        }
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar == src_sourse {
            selectedSearchBar = 106
            if searchText == "" {
                self.view_sourceContainer.isHidden = true
                src_sourse.resignFirstResponder()
            }else{
                getSourceList(searchText)

            }
        }else{
            selectedSearchBar = 107
            if searchText == "" {
                self.view_destinationContainer.isHidden = true
                src_sourse.resignFirstResponder()
            }else{
                getSourceList(searchText)
            }
        }
//        let resultPredicate = NSPredicate(format: "transport_name contains[cd] %@", searchText)
//        filtered = arr_transportList.filteredArrayUsingPredicate(resultPredicate)
//        if(filtered.count == 0){
//            searchActive = false;
//        } else {
//            searchActive = true;
//        }
//        self.tblTransportList.reloadData()
    }
    
    @objc  func handleTap (_ tap : UITapGestureRecognizer)
    {
        src_sourse.resignFirstResponder()
        src_destination.resignFirstResponder()
//        if(txtViewMessage.isFirstResponder())
//        {
//            layoutBottomMenu.constant = 0
//        }
    }
    
    //MARK: Pop up cancel, submit, check-uncheck action methods
    
    @IBAction func btnCancel_On_Popup(_ sender: AnyObject) {
        self.viewFor_bg_popup.isHidden = true
        view_sourceContainer.isHidden = true
        view_destinationContainer.isHidden = true
        self.viewFor_bg_popup.isUserInteractionEnabled = false
    }
    @IBAction func btn_submit_on_popup(_ sender: AnyObject) {
        submit_detail()
    }
    @IBAction func btn_Check_uncheck_connected(_ sender: AnyObject) {
        if connectedLike == "0" {
            connectedLike = "1"
            btnCheckForConnectToBusCompany.setImage(UIImage(named: "img_check.png"), for: UIControlState())
        }else{
            connectedLike = "0"
            btnCheckForConnectToBusCompany.setImage(UIImage(named: "img_uncheck.png"), for: UIControlState())
        }
    }
    
    func submit_detail(){
        if txtBusName.text == "" || txtBusName.text == nil || txtBusNumber.text == "" || txtBusNumber.text == nil || txtNameOnTicket.text == "" || txtNameOnTicket.text == nil || txtTicketNumber.text == "" || txtTicketNumber.text == nil || textview_departure_city.text == "Departure City" || textview_departure_city.text == "" || textview_arrival_city.text == "Arrival City" || textview_arrival_city.text == "" || textview_arrival_time.text == "Arrival Time" || textview_arrival_time.text == "" || textview_depart_time.text == "Departure Time" || textview_depart_time.text == ""  {
            
            if txtBusName.text == "" || txtBusName.text == nil {
                 self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Transport Name.", showInView: self.view)
            } else if txtNameOnTicket.text == "" || txtNameOnTicket.text == nil{
                 self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Name On Ticket.", showInView: self.view)
            } else if txtTicketNumber.text == "" || txtTicketNumber.text == nil{
                 self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Ticket Number.", showInView: self.view)
            } else if txtBusNumber.text == "" || txtBusNumber.text == nil{
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Bus Number.", showInView: self.view)
            } else if textview_departure_city.text == "Departure City" || textview_departure_city.text == ""{
                 self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Departure City.", showInView: self.view)
           }else if textview_arrival_city.text == "Arrival City" || textview_arrival_city.text == ""{
                 self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Arrival City.", showInView: self.view)
           }else if textview_depart_time.text == "Departure Time" || textview_depart_time.text == ""{
                 self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Departure Time.", showInView: self.view)
         } else if textview_arrival_time.text == "Arrival Time" || textview_arrival_time.text == "" {
                 self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Arrival Time.", showInView: self.view)
            }
        } else{
            busRegistration()
        }
    }
    
    func busRegistration() {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if textview_description.text == "Enter Description (Optional)." || textview_description.text == ""  {
            textview_description.text = ""
        } else if str_Transport_id == "" {
            str_Transport_id = ""
        }
            var fName = "FeedBack_iOS_Report_Image_";
            fName = fName + DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .medium)
            fName += ".jpeg"
            fName = fName.trimmingCharacters(in: CharacterSet.whitespaces)
            fName = fName.replacingOccurrences(of: " ", with: "")
            
            if (!isNetworkAvailable) {
                Utility.showNetWorkAlert()
                return
            }
       
            if let str_userID = AppTheme.getLoginDetails().object(forKey: "id") {
                
            let parameters : [String: AnyObject] = ["user_id" : str_userID as AnyObject,
                                                        "transport_id" : str_Transport_id as AnyObject,
                                                        "transport_name": txtBusName.text as AnyObject,
                                                        "ticket_name": self.txtNameOnTicket.text  as AnyObject,
                                                        "ticket_number": self.txtTicketNumber.text as AnyObject,
                                                        "connected_like": connectedLike as AnyObject,
                                                        "arrival_time": textview_arrival_time.text as AnyObject,
                                                        "departure_time": textview_depart_time.text as AnyObject,
                                                        "arrival_station" : str_destination_id as AnyObject,
                                                        "departure_station" : str_source_id as AnyObject,
                                                        "bus_number" : txtBusNumber.text as AnyObject,
                                                        "description" : textview_description.text as AnyObject]
            
            AppTheme().callPostWithMultipartServices(String(format: "%@add_transport_bus", AppUrl), param: parameters, imageData: imageVideoData, file_type: "image") { (result, data) -> Void in
                
                if(result == "success") {
                    self.loading.dismiss()
                    let dict : NSDictionary = data as! NSDictionary
                    var arr_busDetail : NSArray! = NSArray()
                    var dict_temp : NSDictionary! = NSDictionary()
                    if let arr = (dict .value(forKey: "details") as AnyObject).value(forKey: "bus_details") as? NSArray{
                        arr_busDetail = arr
                        for element in arr_busDetail{
                            dict_temp = element as! NSDictionary
                        }
                        if dict_temp.count > 0{
                            let destVC : TransportDetailController!
                            destVC = self.storyboard?.instantiateViewController(withIdentifier: "TransportDetailController") as! TransportDetailController
                            if let str_bus = dict_temp .value(forKey: "bus_id") as? String {
                                destVC.str_bus_id = str_bus
                            }
                            self.navigationController?.pushViewController(destVC, animated: true)
                        }
                    }
                   
                } else {
                    self.loading.dismiss()
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: (data as? String)!, showInView: self.view)
                }
            self.chosenImage = nil
        }

        
//                AppTheme().callPostService(String(format: "%@add_transport_bus?", AppUrl), param: parameters)
//                {(result, data) -> Void in                             ///Call services
//                    
//                    if result == "success" {
//                        let dict : NSDictionary = data as! NSDictionary
//                        var arr_busDetail : NSArray! = NSArray()
//                        var dict_temp : NSDictionary! = NSDictionary()
//                        if let arr = dict .valueForKey("details")?.valueForKey("bus_details") as? NSArray{
//                            arr_busDetail = arr
//                            for element in arr_busDetail{
//                                dict_temp = element as! NSDictionary
//                            }
//                            if dict_temp.count > 0{
//                                let destVC : TransportDetailController!
//                                destVC = self.storyboard?.instantiateViewControllerWithIdentifier("TransportDetailController") as! TransportDetailController
//                                if let str_bus = dict_temp .valueForKey("bus_id") as? String {
//                                    destVC.str_bus_id = str_bus
//                                }
//                                self.navigationController?.pushViewController(destVC, animated: true)
//                            }
//                        }
//                    }else {
//                        self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
//                    }
//                }
            }
    }
    
     //MARK: Cancel action method for select city view
    @IBAction func btnCancelSelect(_ sender: AnyObject) {
        viewForCity.isHidden = true
    }
    @IBAction func btn_addBus(_ sender: AnyObject) {
        viewFor_bg_popup.isHidden = false
        view_container_for_review_popup.isHidden = false
        view_container_for_checkInCheckOut.isHidden = false
        self.viewForDatePicker.isHidden = true
        self.viewFor_bg_popup.isUserInteractionEnabled = true
    }
    
    @IBAction func btn_image_select(_ sender: AnyObject) {
        let alertController = UIAlertController(title: "Please Select", message: "", preferredStyle: .alert)
        
        let galleryAction = UIAlertAction(title: "Image from gallery", style: .default) { (action) -> Void in
            
            self.galleryAction()
        }
        let cameraAction = UIAlertAction(title: "Image from camera", style: .default) {
            (action) -> Void in
            self.cameraAction()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        alertController.addAction(galleryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(cancelAction)
        
        alertController.view.backgroundColor = UIColor.white
        alertController.view.tintColor = UIColor.gray
        alertController.view.layer.cornerRadius = 5
        alertController.view.layer.masksToBounds = true
        
        present(alertController, animated: true, completion: nil)
    }
    
    func galleryAction()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary))
        {
            isCamera = false
            self.picker?.delegate = self
            picker!.allowsEditing = true
            picker!.mediaTypes = [kUTTypeImage as String]
            picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
            
            present(picker!, animated: true, completion: nil)
            self.viewFor_bg_popup.isHidden = false
            self.view_container_for_review_popup.isHidden = false
            self.view_container_for_checkInCheckOut.isHidden = false
        }
    }
    func cameraAction()
    {
        isCamera = true
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            self.picker?.delegate = self
            picker!.allowsEditing = true
            picker!.mediaTypes = [kUTTypeImage as String]
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
            self.viewFor_bg_popup.isHidden = false
            self.view_container_for_review_popup.isHidden = false
            self.view_container_for_checkInCheckOut.isHidden = false
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    //===========================================
    //MARK: UIImagePickerview delegate methods
    //===========================================
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.viewFor_bg_popup.isHidden = false
        self.view_container_for_review_popup.isHidden = false
        self.view_container_for_checkInCheckOut.isHidden = false
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        let mediaType:AnyObject? = info[UIImagePickerControllerMediaType] as AnyObject
        if let type:AnyObject = mediaType {
            if type is String {
                self.viewFor_bg_popup.isHidden = false
                self.view_container_for_review_popup.isHidden = false
                self.view_container_for_checkInCheckOut.isHidden = false
                    self.chosenImage = self.rotateCameraImageToProperOrientation(info[UIImagePickerControllerOriginalImage] as! UIImage, maxResolution: 1024)
                if isCamera == false {
                    let tempImage  = info[UIImagePickerControllerReferenceURL] as! URL
                    self.lbl_select_image.text =  String(describing: tempImage)
                    imageVideoData = UIImageJPEGRepresentation(chosenImage, 1.0)!
                }else{
                    self.lbl_select_image.text = "image select successfully."
                }
                imageVideoData = UIImageJPEGRepresentation(chosenImage, 1.0)!
            }
        }
        dismiss(animated: true, completion:{
            self.viewFor_bg_popup.isHidden = false
            self.view_container_for_review_popup.isHidden = false
            self.view_container_for_checkInCheckOut.isHidden = false
        })
    }
    
    // Fix image orientation rotate in right direction
    func rotateCameraImageToProperOrientation(_ imageSource : UIImage, maxResolution : CGFloat) -> UIImage {
        
        let imgRef = imageSource.cgImage;
        
        let width = CGFloat(imgRef!.width);
        let height = CGFloat(imgRef!.height);
        
        var bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        var scaleRatio : CGFloat = 1
        if (width > maxResolution || height > maxResolution) {
            
            scaleRatio = min(maxResolution / bounds.size.width, maxResolution / bounds.size.height)
            bounds.size.height = bounds.size.height * scaleRatio
            bounds.size.width = bounds.size.width * scaleRatio
        }
        
        var transform = CGAffineTransform.identity
        let orient = imageSource.imageOrientation
        let imageSize = CGSize(width: CGFloat(imgRef!.width), height: CGFloat(imgRef!.height))
        
        
        switch(imageSource.imageOrientation) {
        case .up :
            transform = CGAffineTransform.identity
            
        case .upMirrored :
            transform = CGAffineTransform(translationX: imageSize.width, y: 0.0);
            transform = transform.scaledBy(x: -1.0, y: 1.0);
            
        case .down :
            transform = CGAffineTransform(translationX: imageSize.width, y: imageSize.height);
            transform = transform.rotated(by: CGFloat(M_PI));
            
        case .downMirrored :
            transform = CGAffineTransform(translationX: 0.0, y: imageSize.height);
            transform = transform.scaledBy(x: 1.0, y: -1.0);
            
        case .left :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(translationX: 0.0, y: imageSize.width);
            transform = transform.rotated(by: 3.0 * CGFloat(M_PI) / 2.0);
            
        case .leftMirrored :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(translationX: imageSize.height, y: imageSize.width);
            transform = transform.scaledBy(x: -1.0, y: 1.0);
            transform = transform.rotated(by: 3.0 * CGFloat(M_PI) / 2.0);
            
        case .right :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(translationX: imageSize.height, y: 0.0);
            transform = transform.rotated(by: CGFloat(M_PI) / 2.0);
            
        case .rightMirrored :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
            transform = transform.rotated(by: CGFloat(M_PI) / 2.0);
            
            
        }
        
        UIGraphicsBeginImageContext(bounds.size)
        let context = UIGraphicsGetCurrentContext()
        
        if orient == .right || orient == .left {
            context!.scaleBy(x: -scaleRatio, y: scaleRatio);
            context!.translateBy(x: -height, y: 0);
        } else {
            context!.scaleBy(x: scaleRatio, y: -scaleRatio);
            context!.translateBy(x: 0, y: -height);
        }
        
        context!.concatenate(transform);
        UIGraphicsGetCurrentContext()!.draw(imgRef!, in: CGRect(x: 0, y: 0, width: width, height: height));
        
        let imageCopy = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return imageCopy!;
    }
}
