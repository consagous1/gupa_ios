//
//  HashTagController.swift
//  Gupa Network
//
//  Created by MWM23 on 12/23/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher
import QuartzCore
import AVKit
import AVFoundation
import MediaPlayer

class HashTagController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var loading : JGProgressHUD! = JGProgressHUD()
    
    @IBOutlet weak var tblHashTagList: UITableView!
    @IBOutlet var userPostArray: NSMutableArray! = NSMutableArray()
    
    //Video player controller===============
    var player_video: AVPlayer!
    var avpController : AVPlayerViewController! = AVPlayerViewController()
    ///////----===============
    
    static var isUserDetailLike : Bool = false
    var arrOfResponseKey: NSMutableArray = NSMutableArray()
    var sr_search_word : String! // = <#value#>
    
    var arr_total_comment : NSMutableArray!
    var likeByMe : NSInteger!
    var flight_Id : NSInteger!
    var post_Id : NSInteger!
    
    var player = AVPlayer ()
    var playerLayer = AVPlayerLayer()
    var isPlaying: Bool!
    var btnPlayPause: UIButton!
    var userIdForSingleUserDetail : String = String()
    var strUserId : String!
    
    //MARK: Controller life cycle method
     override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startProgressBar()
        appDelegate.centerNav = self.navigationController
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(HashTagController.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        self.tblHashTagList.estimatedRowHeight = 100
        self.tblHashTagList.rowHeight = UITableViewAutomaticDimension
        self.tblHashTagList.setNeedsLayout()
        self.tblHashTagList.layoutIfNeeded()
        self.tblHashTagList.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if sr_search_word != nil {
            self.title = sr_search_word
            if let str : String =  AppTheme.getLoginDetails().value(forKey: "id") as? String{
                strUserId = str
                 self.getHashUserList(sr_search_word)
            }
        }
    }
    @objc func leftBarBackButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
        if self.arr_total_comment != nil {
            row =  self.arr_total_comment.count * 2
            return row
        } else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        self.loading.dismiss()
        let temp_dic : NSDictionary  = self.arr_total_comment.object(at: indexPath.row/2) as! NSDictionary
        if indexPath.row % 2 == 1{
           
               var cell : WriteStatusCell! = tableView.dequeueReusableCell(withIdentifier: "WriteStatusCell", for: indexPath) as! WriteStatusCell
                    if cell == nil {
                        cell = WriteStatusCell(style: UITableViewCellStyle.default, reuseIdentifier: "WriteStatusCell")
                    }
                    if let str_user_name : String = temp_dic .value(forKey: "username") as? String {
                        cell.lblUserName.text = str_user_name
                    }
            cell.btn_image_post.isHidden = true
            cell.btn_video_post.isHidden = true
                    if let str_posts : String = temp_dic .value(forKey: "posts") as? String{
                        cell.lblStatus.text = str_posts
                        cell.lblStatus.hashtagLinkTapHandler = {(label:KILabel,string:String?,range: NSRange?) -> ()
                            in
                            self.getHashUserList(string!)
                        }
                        cell.lblStatus.userHandleLinkTapHandler = {(label:KILabel,string:String?,range: NSRange?) -> ()
                            in
                            let objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "HashTagController") as! HashTagController
                            objFlightDetailVC.sr_search_word = string
                            self.navigationController?.pushViewController(objFlightDetailVC, animated: true)
                        }
                    }
                    
                    if let str_total_like : String = temp_dic .value(forKey: "total_like") as? String {
                        cell.lblTotalLike.text = str_total_like
                    }
                    if let str_total_comment : String = temp_dic .value(forKey: "total_comment") as? String{
                        cell.lblTotalComments.text = str_total_comment
                    }
                    cell.layer.cornerRadius=10 //set corner radius here
                    cell.layer.borderColor = UIColor.lightGray.cgColor//set cell border color here
                    cell.layer.borderWidth = 2
                    cell.btnLike.tag = indexPath.row
                    
                    if let strRating: String = temp_dic["like_by_me"] as? String {
                        likeByMe = NSInteger(strRating)!
                    }else if let strRating: NSInteger = temp_dic["like_by_me"] as? NSInteger{
                        likeByMe = strRating
                    }
                    if likeByMe == 1{
                        cell.btnLike.setImage(UIImage(named: "img_like_on.png"), for: UIControlState())
                    }else{
                        cell.btnLike.setImage(UIImage(named: "img_dislike.png"), for: UIControlState())
                    }
                    if let str_flight_id: String = temp_dic .value(forKey: "flight_id") as? String{
                        flight_Id = NSInteger(str_flight_id)
                    }
                    if let str_post_Id : String = temp_dic .value(forKey: "post_id") as? String{
                        post_Id = NSInteger(str_post_Id)
                    }
                    cell.btnShare.addTarget(self, action: #selector(HashTagController.shareAction(_:)), for: UIControlEvents.touchUpInside)
                    cell.btnShare.tag = indexPath.row
                    
                    cell.btnLike.addTarget(self, action: #selector(HashTagController.likeDislikeAction(_:)), for: UIControlEvents.touchUpInside)
                    
                    cell.btnUserFollowUnfollow.addTarget(self, action: #selector(HashTagController.btnUserFollowUnfollow(_:)), for: .touchUpInside)
                    cell.btnUserFollowUnfollow.tag = indexPath.row
            
                    cell.btnComment.addTarget(self, action: #selector(HashTagController.commentAction(_:)), for: UIControlEvents.touchUpInside)
                    
                    cell.btnComment.tag = indexPath.row
                    if let str_user_id : String = temp_dic["user_id"] as? String {
                        
                        if let str_logged_user_id : String = AppTheme.getLoginDetails().value(forKey: "id") as? String{
                            if (str_user_id == str_logged_user_id) {
                                cell.deleteButton.isHidden = false
                                cell.deleteButton.isUserInteractionEnabled = true
                                cell.deleteButton.addTarget(self, action: #selector(HashTagController.deletePost(_:)), for: .touchUpInside)
                                cell.deleteButton.tag = indexPath.row
                            } else{
                                cell.deleteButton.isHidden = true
                                cell.deleteButton.isUserInteractionEnabled = false
                            }
                        }
                    }
                    if let str_profile_pic : String = temp_dic .value(forKey: "profile_pic") as? String
                    {
                        let url = URL(string:str_profile_pic)
                        
                        cell.imgUser.kf.setImage(with: url, placeholder: UIImage(named: "addprofilepic.png"), options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
                            print(image?.description ?? "")
                                                  })
                        
                        
//                        cell.imgUser.kf_setImageWithURL(url, placeholderImage: UIImage(named: "addprofilepic.png"), optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
//                            print(image?.description)
//                        })
                    }
                    var str_time_server : String! // = <#value#>
                    var str_date_server : String!
                    if let str_time : String = temp_dic .value(forKey: "time") as? String {
                        str_time_server = str_time
                    }
                    if let str_date : String = temp_dic .value(forKey: "date") as? String {
                        str_date_server = str_date
                    }
                    cell.lblDateTime.text = "\(str_time_server)  \(str_date_server)"
                    
                    if let str_date : String = temp_dic .value(forKey: "date") as? String {
                        str_date_server = str_date
                    }
                    
            if let str_f_type =  temp_dic .value(forKey: "file_type") as? String{
                if str_f_type == "text" {
                    cell.imageHeight.constant = 0
                    cell.viewHeightConstraint.constant = 0
                }else{
                    if let str_file_url : String = temp_dic .value(forKey: "file_name") as? String{
                        let url = URL(string:str_file_url)
                        if verifyUrl(str_file_url) == true {
                            if str_f_type == "video"{
                                cell.viewPlayer.isHidden = false
                                cell.imgAdd.isHidden = true
                                cell.viewHeightConstraint.constant = 81
                                cell.btn_image_post.isHidden = true
                                cell.btn_video_post.isHidden = false
                                cell.btn_video_post.addTarget(self, action: #selector(HashTagController.videoTapped(_:)), for: .touchUpInside)
                                cell.btn_video_post.tag = indexPath.row
                                //                            cell.viewPlayer.setVideoURL(videoURL!)
                            } else{
                                cell.viewPlayer.isHidden = true
                                cell.imgAdd.isHidden = false
                                cell.btn_image_post.isHidden = false
                                cell.btn_video_post.isHidden = true
                                cell.imageHeight.constant = 81
                                cell.btn_image_post.addTarget(self, action:  #selector(HashTagController.imageTapped(_:)), for: .touchUpInside)
                                cell.btn_image_post.tag = indexPath.row
                                let url = url
                                
                                cell.imgAdd.kf.setImage(with: url, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler:{ (image, error, cacheType, imageURL) in
                                    print(image?.description ?? "")
                                     } )
                                
//                                cell.imgAdd.kf_setImageWithURL(url!, placeholderImage: UIImage(named: ""), optionsInfo: nil , progressBlock: nil , completionHandler: { (image, error, cacheType, imageURL) in
//                                    print(image?.description)
//                                } )
                            }
                        }
                    }else{
                        cell.imageHeight.constant = 0
                        cell.viewHeightConstraint.constant = 0
                    }
                }
            }
                  /*  if let str_file_type : String = temp_dic .valueForKey("file_name") as? String
                    {
                        let url = NSURL(string:str_file_type)
                        if verifyUrl(str_file_type) == true
                        {
                            if str_file_type == "video"{
                                cell.viewPlayer.hidden = false
                                cell.btn_image_post.hidden = true
                                cell.btn_video_post.hidden = false
                                cell.imgAdd.hidden = true
                                cell.viewHeightConstraint.constant = 81
                                cell.btn_video_post.addTarget(self, action: #selector(HomeVC.videoTapped(_:)), forControlEvents: .TouchUpInside)
                                cell.btn_video_post.tag = indexPath.row
                            } else{
                                cell.viewPlayer.hidden = true
                                cell.imgAdd.hidden = false
                                cell.btn_image_post.hidden = false
                                cell.btn_video_post.hidden = true
                                cell.imageHeight.constant = 81
                                cell.btn_image_post.addTarget(self, action: #selector(imageTapped(_:)), forControlEvents: .TouchUpInside)
                                cell.btn_image_post.tag = indexPath.row
                                let url = url
                                cell.imgAdd.kf_setImageWithURL(url!, placeholderImage: UIImage(named: ""), optionsInfo: nil , progressBlock: nil , completionHandler: { (image, error, cacheType, imageURL) in
                                    print(image?.description)
                                } )
                            }
                        }else{
                            cell.btn_image_post.hidden = true
                            cell.btn_video_post.hidden = true
                            cell.imageHeight.constant = 0
                            cell.viewHeightConstraint.constant = 0
                        }
                    } */
                    var rating : NSInteger = 0
                    if let strRating: String = temp_dic["rating"] as? String
                    {
                        if strRating == ""{
                            
                        } else{
                            rating = NSInteger(strRating)!
                        }
                    }
                    else if let strRating: NSInteger = temp_dic["rating"] as? NSInteger{
                        rating = strRating
                    }
                    if rating == 0 {
                        cell.imgSmileStatus.image = UIImage(named: "")
                    }
                    if rating == 1
                    {
                        cell.imgSmileStatus.image = UIImage(named: "good_on.png")
                    }
                    else if rating == 2
                    {
                        cell.imgSmileStatus.image = UIImage(named: "bad_on.png")
                    }
                    else if rating == 3
                    {
                        cell.imgSmileStatus.image = UIImage(named: "bad.png")
                    }
                    return cell
    
        } else {
            var cell = tableView .dequeueReusableCell(withIdentifier: "cell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            }
            cell!.isUserInteractionEnabled = false
            cell?.backgroundColor = UIColor.clear
            return cell!
        }
    }

    //Check url nil or not
    func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if urlString == "" || urlString.isEmpty{
                return false
            }else{
                if let url = URL(string: urlString) {
                    // check if your application can open the NSURL instance
                    return UIApplication.shared.canOpenURL(url)
                }
            }
        }
        return false
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let dict_temp : NSDictionary = self.arr_total_comment.object(at: indexPath.row/2) as? NSDictionary{
            if indexPath.row % 2 == 1 {
                if let strTag : String = dict_temp["tag"] as? String{
                    if strTag == "advertise"{
                        
                    }else{
                        let objCommentVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedsCommentVC") as! FeedsCommentVC
                        objCommentVC.dicData = dict_temp
                        objCommentVC.strPostsId = dict_temp .value(forKey: "post_id") as? String
                        self.navigationController?.pushViewController(objCommentVC, animated: true)
                    }
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
            var cell: WriteStatusCell! = tableView.dequeueReusableCell(withIdentifier: "WriteStatusCell") as? WriteStatusCell
            if cell == nil {
                cell = WriteStatusCell(style: UITableViewCellStyle.default, reuseIdentifier: "WriteStatusCell")
            }
            tblHashTagList.backgroundColor = UIColor.clear
            tblHashTagList.separatorStyle = UITableViewCellSeparatorStyle.none
            tblHashTagList.estimatedRowHeight = 100
            tblHashTagList.rowHeight = UITableViewAutomaticDimension
            if indexPath.row%2 == 0 {
                return 10
            }
            return  tblHashTagList.rowHeight
    }
    //===================================================
    //MARK: Action for the Like, comment and share
    //===================================================
  @objc  func videoTapped(_ sender: UIButton!) {
        print("index: %@", sender.tag)
        let dicData: NSDictionary = self.arrOfResponseKey.object(at: sender.tag/2) as! NSDictionary
        print(dicData)
        if let img_post = dicData .value(forKey: "file_name") as? String{
            let popupView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 160))
            popupView.backgroundColor = UIColor.clear
            let url_video = URL(string: img_post)
            let player = AVPlayer(url: url_video!)
            let playerController = AVPlayerViewController()
            playerController.player = player
            present(playerController, animated: true) {
                player.play()
            }
            //            present(playerController, animated: true) {
            //                player.play()
            //            }
            popupView.addSubview(playerController.view)
        }
        
    }
   @objc func imageTapped(_ sender: UIButton!) {
        print("index: %@", sender.tag)
        let dicData: NSDictionary = self.arrOfResponseKey.object(at: sender.tag/2) as! NSDictionary
        print(dicData)
        var popupVC: ImageVideoPlayerController! = ImageVideoPlayerController()
        popupVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageVideoPlayerController") as! ImageVideoPlayerController
        if let img_post = dicData .value(forKey: "file_name") as? String{
            let url_image = URL(string: img_post)
            popupVC.img_url = (url_image! as NSURL) as URL!
        }
        if let str_userName = dicData .value(forKey: "username") as? String{
            popupVC.str_username = str_userName
        }
        let popupDestVC = STPopupController(rootViewController: popupVC)
        popupDestVC.present(in: self)
    }
    @objc func btnUserFollowUnfollow(_ sender: UIButton)
    {
        // print("index: %@", sender.tag )
        let dicData: NSDictionary = self.arr_total_comment.object(at: sender.tag/2) as! NSDictionary
        print(dicData)
        self.navigationController?.isNavigationBarHidden = true
        if let str_user_id : String = dicData .value(forKey: "user_id") as? String{
            userIdForSingleUserDetail = str_user_id
            UserDefaults.standard.setValue(str_user_id, forKey: "otherId")
            UserDefaults.standard.synchronize()
            self.getSingleUserInfo(userIdForSingleUserDetail)
        }
    }
    @objc func likeDislikeAction(_ sender: UIButton!)
    {
        print("index: %@", sender.tag )
        let dicData: NSDictionary!
        if HashTagController.isUserDetailLike == true {
            dicData = self.userPostArray.object(at: sender.tag) as! NSDictionary
        } else{
            dicData = self.arr_total_comment.object(at: sender.tag/2) as! NSDictionary
        }
//        let parameters : [String: AnyObject]!
        
        let paraDict = NSMutableDictionary()
        
        
        var strFlighId : String! = String()
        var strPostsId : String! = String()
        
        if let str_flight_id : String = dicData["flight_id"]! as? String{
            strFlighId = str_flight_id
        }
        if let str_post_id : String = dicData["post_id"]! as? String{
            strPostsId = str_post_id
        }
        if let str_likeByMe : String = dicData["like_by_me"]! as? String{
            likeByMe = NSInteger(str_likeByMe)!
        }
        else if let str_likeByMe_int: NSInteger = dicData["like_by_me"]! as?  NSInteger{
            likeByMe = str_likeByMe_int
        }
        if likeByMe == 1{
//            parameters = ["flight_id" : strFlighId as AnyObject,
//                          "posts_id" : strPostsId as AnyObject,
//                          "user_id" : strUserId as AnyObject,
//                          "status" : "0" as AnyObject]
            
            paraDict.setValue(strFlighId, forKey:"flight_id")
            paraDict.setValue(strPostsId, forKey:"posts_id")
            paraDict.setValue(strUserId, forKey:"user_id")
            paraDict.setValue("0", forKey:"status")
            
        } else{
//            parameters = ["flight_id" : strFlighId as AnyObject,
//                          "posts_id" : strPostsId as AnyObject,
//                          "user_id" : strUserId as AnyObject,
//                          "status" : "1" as AnyObject]
            
            paraDict.setValue(strFlighId, forKey:"flight_id")
            paraDict.setValue(strPostsId, forKey:"posts_id")
            paraDict.setValue(strUserId, forKey:"user_id")
            paraDict.setValue("1", forKey:"status")
            
        }
        AppTheme().callPostService(String(format: "%@posts_like", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                print(data)
                self.loading.dismiss()
                if HashTagController.isUserDetailLike == true {
                    self.getHashUserList(self.sr_search_word)
                } else{
                    self.getHashUserList(self.sr_search_word)
                }
            }
        }
    }
    func getSingleUserInfo(_ otherId : String)
    {
        var destVC = UserChatOnCommentController()
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
        destVC.str_other_id = otherId
        destVC.isFromTrip = "true"
        //        destVC.str_ = self.arr_like.objectAtIndex(indexPath.row) as! NSDictionary
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    @objc func commentAction(_ sender: UIButton!)
    {
        //        print("index: %@", sender.tag )
        //        let dicData: NSDictionary! = NSDictionary()
        //         let objCommentVC = self.storyboard?.instantiateViewControllerWithIdentifier("FeedsCommentVC") as! FeedsCommentVC
        //        if sender.tag >= 1000 {
        //            sender.tag = sender.tag - 1000
        //            FeedsCommentVC.isFromUser = true
        //
        //        }else{
        //
        //            FeedsCommentVC.isFromUser = false
        //        }
        //        if HomeVC.isUserDetailLike == true {
        //            if let dict_temp : NSDictionary = self.userPostArray.objectAtIndex(sender.tag) as? NSDictionary{
        //                 objCommentVC.dicData = dict_temp
        //            }//            dicData = self.userPostArray.objectAtIndex(sender.tag) as! NSMutableDictionary
        //        } else
        //        {
        //            if let dict_temp : NSDictionary = self.arrOfResponseKey.objectAtIndex(sender.tag/2) as? NSDictionary{
        //                 objCommentVC.dicData = dict_temp
        //            }//            dicData = self.arrOfResponseKey.objectAtIndex(sender.tag/2) as! NSMutableDictionary
        //        }
        //        print(dicData)
        //        self.navigationController?.pushViewController(objCommentVC, animated: true)
    }
    
    
    @objc  func shareAction(_ sender: UIButton!)
    {
        print("index: %@", sender.tag )
        let dicData: NSDictionary!
        if HomeVC.isUserDetailLike == true {
            dicData = self.userPostArray.object(at: sender.tag) as! NSDictionary
        } else {
            dicData = self.arr_total_comment.object(at: sender.tag/2) as! NSDictionary
        }
        var data : Data = Data()
        var imgShare : UIImage = UIImage()
        var textToShare : String = String()
        
        
        if let str_textToShare : String = dicData["posts"]! as? String{
            textToShare = str_textToShare
            if let str_img_share : String = dicData["file_name"]! as? String {
                if str_img_share != "" {
                    let urlNew:String = str_img_share.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    let url  = URL(string: urlNew)
                    data = try! Data(contentsOf: url!)
                    imgShare = UIImage(data: data)!
                }
                else{
                }
            }
        }
        if textToShare != "" && imgShare != nil {
            shareImageText(textToShare, image: imgShare)
        }
        if textToShare == "" && imgShare != nil {
            shareImageText(textToShare, image: imgShare)
        }
        if textToShare != "" && imgShare == nil {
            shareImageText(textToShare, image: imgShare)
        }
        
        
    }
    func shareImageText(_ text: String, image: UIImage){
        let objects2Share = [text, image] as [Any]
        let activityVC = UIActivityViewController(activityItems: objects2Share , applicationActivities: nil)
        
        let excludeActivities = [UIActivityType.airDrop, UIActivityType.copyToPasteboard , UIActivityType.mail, UIActivityType.addToReadingList]
        
        
        activityVC.excludedActivityTypes = excludeActivities
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func PlayPause(_ sender: UIButton!)
    {
        print("index: %@", sender.tag )
        
        btnPlayPause.setImage(UIImage(named: "pause.png"), for: UIControlState())
        player.play()
    }
    
    @objc func deletePost(_ sender: UIButton!)
    {
        print("index: %@", sender.tag )
        
        let refreshAlert = UIAlertController(title: "Delete post", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            let dicData: NSDictionary!
            if HomeVC.isUserDetailLike == true {
                dicData = self.userPostArray.object(at: sender.tag) as! NSDictionary
            } else
            {
                dicData = self.arr_total_comment.object(at: sender.tag/2) as! NSDictionary
            }
            if let strPostsId : String = dicData["post_id"]! as? String {
                self.deleteSelectedPost(strPostsId)
            }
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }
    
    func deleteSelectedPost(_ post_id : String)  {
        
           if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
       
//        let parameters : [String: AnyObject] = ["post_id" : post_id as AnyObject]
        
          let paraDict = NSMutableDictionary()
          paraDict.setValue(post_id, forKey:"post_id")
        
        AppTheme().callPostService(String(format: "%@post_delete", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Post deleted!", showInView: self.view )
                self.getUserStatusList()
            }else{
                self.loading.dismiss()
              //  self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: data as! String, showInView: self.view )
            }
        }
    }
    
    func getUserStatusList()
    {
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
        let currentTimeZone: String = (TimeZone.current as NSTimeZone).description
        let arrayTime: NSArray = currentTimeZone .components(separatedBy: " ") as NSArray
        let zoneString : String = arrayTime .object(at: 0) as! String
        
        //        if let flightId: NSInteger = AppTheme.getUserFlight() {
        //            let currentFlightId = String(flightId)
        //        }
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
     
        let parameters : [String: AnyObject] = ["flight_id" : "" as AnyObject,
                                                "user_id" : strUserId as AnyObject,
                                                "timezone" : zoneString as AnyObject]  //dictForFlight["FlightNumber"]! as AnyObject ]
        
        AppTheme().callGetService(String(format: "%@get_posts", AppUrl), param: parameters) {(result, data) -> Void in                                   ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                if let arrResponse: NSMutableArray = data as? NSMutableArray
                {
                    print("arrResponse: @%", arrResponse)
                }else if let dicRes: NSDictionary = data as? NSDictionary {
                    //self.respDict = dicRes
                    if let tempArr: NSArray = dicRes["response"] as? NSArray
                    {
                        self.arr_total_comment = tempArr.mutableCopy() as! NSMutableArray
                        if self.arr_total_comment.count == 0 || self.arr_total_comment.isEqual(nil)
                        {
                            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry there is no data found!", showInView: self.view)
                        }else{
                            self.arr_total_comment = tempArr.mutableCopy() as! NSMutableArray
                        }
                    }else{
                        self.arr_total_comment = dicRes["response"] as! NSMutableArray
                    }
                }
                self.tblHashTagList.reloadData()
            } else{
                self.loading.dismiss()
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry there is some network error", showInView: self.view)
            }
        }
    }
    //==============================
    //MARK: Loader method
    //==============================
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    func getHashUserList(_ str_hash_word : String) {
        
           if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
      
//        let parameters : [String: AnyObject] = ["hashtag" : str_hash_word as AnyObject,
//                                                "user_id" : strUserId as AnyObject]
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(str_hash_word, forKey:"hashtag")
        paraDict.setValue(strUserId, forKey:"user_id")
        
        AppTheme().callPostService(String(format: "%@search_posts", AppUrl), param: paraDict) { (result, data) -> Void in
                if result == "success"{
                self.loading.dismiss()
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arr_total_comment = array_response.mutableCopy() as! NSMutableArray
                }else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                    self.arr_total_comment = array_Response_mutable
                }
                self.tblHashTagList.reloadData()
            }
        }
    }
}

