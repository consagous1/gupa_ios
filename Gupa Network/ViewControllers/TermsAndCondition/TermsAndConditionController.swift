//
//  TermsAndConditionController.swift
//  Gupa Network
//
//  Created by MWM23 on 11/16/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class TermsAndConditionController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    var appDelegate: AppDelegate = AppDelegate()
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var onSignUp : String! // = <#value#>
    
    // MARK: Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 66.0/255.0, green: 199.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if let str_check : String = appDelegate.strURL {
            
            if str_check == "OnSignUp"{
                
                appDelegate.strLink = "http://gupanetwork.com/#/toc"
                
                let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
                btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
                btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
                btnLeftBar.addTarget(self, action: #selector(TermsAndConditionController.backButton), for: .touchUpInside)
                
                let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
                leftBarButton.customView = btnLeftBar
                self.navigationItem.leftBarButtonItem = leftBarButton
                
            } else if str_check == "OnSideBar" {
                
                self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
                let btnRighBar: UIButton = UIButton ()
                btnRighBar.setImage(UIImage(named: "Home-icon.png"), for: UIControlState())
                btnRighBar.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
                btnRighBar.addTarget(self, action: #selector(TermsAndConditionController.rightBarBackButton), for: .touchUpInside)
                
                //.....Set right button
                let rightBarButton:UIBarButtonItem = UIBarButtonItem()
                rightBarButton.customView = btnRighBar
                self.navigationItem.rightBarButtonItem = rightBarButton
            }
        }
       
        webView.delegate = self
       // startProgressBar()
        
        if appDelegate.strLink == "http://gupanetwork.com/#/toc" {
             self.title = "Terms & Condition"
        } else {
             //self.title = "Flight Booking"
            self.title = "About Us"
        }
        let url = URL (string: appDelegate.strLink)
        let requestObj = URLRequest(url: url!);
        webView.loadRequest(requestObj)
    }
    
    @objc func backButton()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func rightBarBackButton(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Webview delegate methods

    func webViewDidStartLoad(_ webView: UIWebView)
    {
        activity.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        activity.stopAnimating()
        activity.isHidden = true
    }

}
