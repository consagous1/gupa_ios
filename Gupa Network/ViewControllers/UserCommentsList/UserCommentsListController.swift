//
//  UserCommentsListController.swift
//  Gupa Network
//
//  Created by MWM23 on 12/1/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher

class UserCommentsListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblCommentsList: UITableView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnPostComment: UIButton!
    @IBOutlet weak var txtCommentField: UITextField!
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var dicData : NSDictionary!
    var strFlighId : String!
    var strPostsId : String!
    var strUserId : String!
    var respDict : NSDictionary = NSDictionary()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    
    var arr_like : NSMutableArray! // = NSMutableArray()
    
    var isFollow : NSInteger!
    var userIdForSingleUserDetail : String = String()
    static var isFromUser : Bool = false
    
    let identifier = "CommentCell"
    
    //-===============================
    //MARK: Life cycle methods
    //-===============================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // self.startProgressBar()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        appDelegate.centerNav = self.navigationController
        
        let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(UserCommentsListController.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        self.title = "Users"
        UserDefaults.standard.set(1, forKey: "isComment")
        
       self.tblCommentsList.estimatedRowHeight = 100
       self.tblCommentsList.rowHeight = UITableViewAutomaticDimension
//        self.tblCommentsList.layoutIfNeeded()
       self.tblCommentsList.setNeedsLayout()
       tblCommentsList.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: identifier)
        
//        self.tblCommentsList.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        if arr_like == nil {
            let alert : UIAlertView = UIAlertView(title: "Sorry", message: "Record not found!", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
        }
//        self.imgUser.layer.cornerRadius = 22 //self.imgUser.frame.height/2
//        self.imgUser.clipsToBounds = true
        if UserDefaults.standard.object(forKey: kPrefrenceLoginDetails) != nil
        {
//            let url = NSURL(string:AppTheme.getLoginDetails().objectForKey("profile_pic") as! String)
//            let data = NSData(contentsOfURL:url!)
//            if data != nil {
//                imgUser?.image = UIImage(data:data!)
//            }
        } else
        {
            print("Test Blank")
        }
       // self.getCommentsList()
    }
    
    @objc func leftBarBackButton(){
        
        if FeedsCommentVC.isFromUser == true {
            HomeVC.shouldHideNavBar = true
        }else{
            HomeVC.shouldHideNavBar = false
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
    }
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
        if self.arr_like.count != 0
        {
            row =  self.arr_like.count
            return row
        } else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        self.loading.dismiss()
        let dic: NSDictionary = self.arr_like.object(at: indexPath.row) as! NSDictionary
        
        var cell: CommentCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? CommentCell
        cell.lbl_date.isHidden = true
        if cell == nil {
            cell = CommentCell(style: UITableViewCellStyle.default, reuseIdentifier: identifier)
        }
        if let str_second  = dic["username"] as? String {
            cell.lblSubName.text =  str_second as String
        }
        
        if let str_comment : String = dic["username"] as? String{
            cell.lbl_post.text = str_comment
        }
        if let str_profile_pic : String = dic["profile_pic"] as? String {
            let url = URL(string:str_profile_pic)
            
            cell?.imgUser.kf.setImage(with: url, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: nil)
            
//            cell.imgUser.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var destVC = UserChatOnCommentController()
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
        destVC.isFromUserComment = "true"
        destVC.isFromTrip = "false"
        destVC.dic = self.arr_like.object(at: indexPath.row) as! NSDictionary
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    
//==============================
//MARK: Loader method
//==============================
func startProgressBar()
{
    self.loading = JGProgressHUD(style: .dark)
    self.loading.textLabel.text = "Loading.."
    self.loading.show(in: self.view)
}
}
