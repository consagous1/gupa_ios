//
//  TransportRatingsController.swift
//  Gupa Network
//
//  Created by MWM23 on 12/15/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher
import CoreData

class TransportRatingsController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet var tbl_transportList : UITableView!
    @IBOutlet var searchbar_transport : UISearchBar!
    
    var arrOfResponseKey: NSMutableArray = NSMutableArray()
    var loading : JGProgressHUD!
    var strGoodRating: NSInteger = 0
    var strBadRating: NSInteger = 0
    var strUglyRating: NSInteger = 0
    var searchActive : Bool = false
    var filtered : NSArray = NSArray()
    
    var appDelegate: AppDelegate = AppDelegate()
    var totalBuses : NSMutableArray = NSMutableArray()
    var totalTransport : NSMutableArray = NSMutableArray()
    var totalBusId : NSMutableArray = NSMutableArray()
    
    var checkTableCell : NSInteger = 0
    
    var transportSearch : String!
    var bus_id : String!
    var reversed: NSArray = NSArray()
    var arrOfDestinatioStation: NSMutableArray = NSMutableArray()
    var arrOfDestinatioStationCode: NSMutableArray = NSMutableArray()
    
    var btnSearchUser:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    var isSearchBarActive : Bool = false
    
    //-===============================
    //MARK: Life cycle methods
    //-===============================
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        appDelegate.centerNav = self.navigationController
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.title = "Transport Ratings"
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        showRightNavigationBarButtonHide()
        tbl_transportList.dataSource = self
        tbl_transportList.delegate = self
        self.tbl_transportList.estimatedRowHeight = 50
        self.tbl_transportList.rowHeight = UITableViewAutomaticDimension
        self.tbl_transportList.setNeedsLayout()
        self.tbl_transportList.layoutIfNeeded()
        self.tbl_transportList.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        searchbar_transport.delegate = self
        
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
        checkTableCell = 0
    }
    
    func showRightNavigationBarButtonHide()                               //RightNavigationBarButton hide unhide
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnSearchUser = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnSearchUser.setImage(imgChat, for: UIControlState())
        btnSearchUser.addTarget(self, action: #selector(TransportRatingsController.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnSearchUser)
        
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(TransportRatingsController.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add th@objc e rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    
   @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        searchActive = false
        searchbar_transport.text = nil
        transportSearch = ""
         getBusList("", search: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //=====================================
    //MARK: UISearchBar delegate methods
    //=====================================
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchActive = true;
         checkTableCell = 0
         searchActive = false;
        ///flightList = getFlights()
      //   tbl_transportList.isHidden = false
      //   tbl_transportList.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        searchbar_transport.resignFirstResponder()
        searchActive = false;
//        tbl_transportList.isHidden = true
//
//        if totalBuses.count > 0  || arrOfResponseKey.count > 0 {
//            tbl_transportList.isHidden = false
//        } else {
//            tbl_transportList.isHidden = true
//            startActivity()
//            getBusList("", search: "")
//        }
//
//        startActivity()
//        getBusList("", search: "")
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchbar_transport.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchbar_transport.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //searchbar_transport.resignFirstResponder()
        let resultPredicate = NSPredicate(format: "transport_name contains[cd] %@", searchText)
        filtered = arrOfResponseKey.filtered(using: resultPredicate) as NSArray
        
        if(filtered.count == 0){
            searchActive = false;
            checkTableCell = 1
            transportSearch = ""
            getBusList("", search: "")
            tbl_transportList.isHidden = false
            self.tbl_transportList.reloadData()
        } else{
            searchActive = true;
            checkTableCell = 0
            tbl_transportList.isHidden = false
            self.tbl_transportList.reloadData()
        }
    }
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
       
        if checkTableCell == 1{
            if self.arrOfResponseKey.count != 0{
                row = arrOfResponseKey.count
                return row
            }
            return 0
        } else {
            if(searchActive) {
                row = filtered.count
                return row
            }else  if totalBuses.count != 0{
                row = totalBuses.count
                return row
            }
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  checkTableCell == 1 {
            
            var cell : FlightRatingCell! = tableView.dequeueReusableCell(withIdentifier: "FlightRatingCell", for: indexPath) as! FlightRatingCell
            if cell == nil {
                cell = FlightRatingCell(style: UITableViewCellStyle.default, reuseIdentifier: "FlightRatingCell")
            }
            
            let view : UIView = cell.viewWithTag(111)!
            view.layer.cornerRadius = 5
            var dic: NSDictionary = NSDictionary()
            cell.lblFlighNumber.isHidden = false
            
            if(searchActive){
                dic = filtered.object(at: indexPath.row) as! NSDictionary
            } else {
                dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
            }
            if let str_transport_name : String = dic["transport_name"] as? String{
                cell.lblAirlinesName.text = str_transport_name
            }
            if let str_destination : String = dic["to"] as? String{
                cell.lblDestStation.text = str_destination
            }
            if let str_source : String = dic["from"] as? String{
                cell.lblSourceStation.text = str_source
            }
            
            if let str_BusID : String = dic["bus_id"] as? String{
                cell.lblFlighNumber.text = str_BusID
            }
            if let str_flight_id : String = dic["transport_name"] as? String{
                 let myStringArr: [String] = str_flight_id.components(separatedBy: " ")
                cell.lblFlightName.text = myStringArr.first
            }
           
            if let strRating: String = dic["bus_good"] as? String{
                strGoodRating = NSInteger(strRating)!
            }else if let strRating: NSInteger = dic["bus_good"] as? NSInteger {
                strGoodRating = strRating
            }
            cell.lblGoodRating.text = String(strGoodRating)
            
            if let strRating: String = dic["bus_bad"] as? String{
                strBadRating = NSInteger(strRating)!
            }else if let strRating: NSInteger = dic["bus_bad"] as? NSInteger{
                strBadRating = strRating
            }
            cell.lblBadRating.text = String(strBadRating)
            
            if let strRating: String = dic["bus_ugly"] as? String {
                strUglyRating = NSInteger(strRating)!
            }else if let strRating: NSInteger = dic["bus_ugly"] as? NSInteger {
                strUglyRating = strRating
            }
            cell.lblUglyRating.text = String(strUglyRating)
            
            if let str_profile_pic : String = dic["bus_image"] as? String {
                let url = URL(string: str_profile_pic)
                if url == nil{
                    cell.imgTitle.image = UIImage(named: "img_bus.png")
                }else{
                    
                    cell.imgTitle.kf.setImage(with: url, placeholder: UIImage(named: "img_bus.png"), options: nil, progressBlock: nil, completionHandler: nil)
                    
//                    cell.imgTitle.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "img_bus.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
                }
            }
            return cell
        } else if checkTableCell == 0
        {
            var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
            var dic: NSDictionary = NSDictionary()
            
            if cell == nil {
                cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
            }
            if(searchActive){
                if filtered.count > 0 {
                    dic = filtered.object(at: indexPath.row) as! NSDictionary
                    if let str_title = dic .value(forKey: "transport_name") as? String {
                        cell.titleLabel.text = str_title
                    }
                    if let str_destination : String = dic["to"] as? String{
                        if let str_source : String = dic["from"] as? String{
                            cell.cityLabel.text = "\(str_source)  TO  \(str_destination)"
                        }
                    }
                }
            } else {
                if arrOfResponseKey.count > 0 {
                    dic = arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
                    if let str_title =  dic .value(forKey: "transport_name") as? String{
                        cell.titleLabel.text = str_title
                    }
                    if let str_destination : String = dic["to"] as? String{
                        if let str_source : String = dic["from"] as? String{
                            cell.cityLabel.text = "\(str_source)  TO  \(str_destination)"
                        }
                    }
                }
            }
            return cell
        } else {
            var cell = tableView .dequeueReusableCell(withIdentifier: "cell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            }
            cell?.backgroundColor = UIColor.clear
            cell!.isUserInteractionEnabled = false
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
         var dic: NSDictionary = NSDictionary()
          if checkTableCell == 1 {
           searchbar_transport.resignFirstResponder()
            
            if(searchActive){
                dic = filtered.object(at: indexPath.row) as! NSDictionary
            } else {
                dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
            }
            let barRatingVC = self.storyboard?.instantiateViewController(withIdentifier: "FlightTransportGraphController") as! FlightTransportGraphController
            barRatingVC.flightHotelDetailDict = dic
            if let str_fligh_id : String = dic["bus_id"] as? String{
                barRatingVC.strFlightId = str_fligh_id
            }
            barRatingVC.strCheck = "comeFromTransportRating"
            self.navigationController?.pushViewController(barRatingVC, animated: true)
            
         } else {
//           searchbar_transport.resignFirstResponder()
            tbl_transportList.isHidden = true
            if(searchActive){
                if filtered.count > 0 {
                    dic = filtered.object(at: indexPath.row) as! NSDictionary
                    if let str_bus_id = dic .value(forKey: "bus_id") as? String {
                        bus_id = str_bus_id
                    }
                    if let str_title = dic .value(forKey: "transport_name") as? String {
                        searchbar_transport.text = str_title
                        transportSearch = searchbar_transport.text
                        getBusList(bus_id, search: transportSearch)
                    }
                    //  filtered.objectAtIndex(indexPath.row) as? String
                }
            } else {
                if arrOfResponseKey.count > 0 {
                    dic = arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
                    if let str_bus_id = dic .value(forKey: "bus_id") as? String {
                        bus_id = str_bus_id
                    }
                    if let str_title =  dic .value(forKey: "transport_name") as? String{
                       searchbar_transport.text = str_title
                         transportSearch = searchbar_transport.text
                        getBusList(bus_id, search: transportSearch)
                        
                    }
                    //                    cell.titleLabel.text = arrOfResponseKey.objectAtIndex(indexPath.row) as? String
                }
            }

           /* if(searchActive){
                if filtered.count > indexPath.row {
                    searchbar_transport.text = filtered.objectAtIndex(indexPath.row) as? String
                    transportSearch = searchbar_transport.text
                    getBusList(transportSearch)
                }
            } else {
                if totalBuses.count > indexPath.row {
                    searchbar_transport.text = totalBuses.objectAtIndex(indexPath.row) as? String
                    transportSearch = searchbar_transport.text
                    getBusList(transportSearch)
                    //tbl_transportList.reloadData()
                }
            } */
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if checkTableCell == 1 {
            return 120
        } else{
            return 45
        }
    }
    
    
    func getBusList(_ busNumber : String, search : String)  {
        
      //  self.startActivity()
        
        let separators = CharacterSet(charactersIn: " ")
        // Split based on characters.
        let parts = search.components(separatedBy: separators)
        
        // Print result array.
        print("print parts == \(parts)")
       // let stringFlightId : String = parts.last!
      //  let firstIndex : String = parts.first!
        
        // Service call here
        let parameter : [String : AnyObject]!
        parameter = ["search" : search as AnyObject,
                     "bus_id" : busNumber as AnyObject]
        
        AppTheme().callGetService(String(format: "%@BusTransportRating", AppUrl), param: parameter) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                self.tbl_transportList.isHidden = false
                self.checkTableCell = 1
                var respDict : NSDictionary = NSDictionary()
                var dictFlightId : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
//                 AppTheme.setTransportRatingData(data)
                AppTheme.setTransportRatingData(data)
                AppTheme.setFirstTimeTransport(false)
                print(respDict)
                
                self.searchActive = false
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                    self.arrOfResponseKey = array_Response_mutable
                }
                
                self.totalBusId.removeAllObjects()
                 self.totalBuses.removeAllObjects()
                 self.totalTransport.removeAllObjects()
                if self.arrOfResponseKey.count > 0 {
                    for element in self.arrOfResponseKey
                    {
                        let str_airline_name : String! = String()
                        dictFlightId = element as! NSDictionary
                        if let str_airline_name = dictFlightId .value(forKey: "transport_name") as? String{
                            self.totalBusId.add(str_airline_name)
                        }
                        if let str_flight_id : String = dictFlightId .value(forKey: "transport_name") as? String{
                            let flightId = "\(str_airline_name)  \(str_flight_id)"
                            self.totalBuses.add(str_flight_id)
                            self.totalTransport.add(str_flight_id)
                        }
                    }
                    self.tbl_transportList.reloadData()
                  //  self.searchbar_transport.resignFirstResponder()
                }
            }
            else
            {
                self.loading.dismiss()
//                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry there is no data found.", showInView: (self.view))
            }
        }
    }
    
   /* override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        searchbar_transport.resignFirstResponder()
    } */
    func startActivity()  {
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
    }
}
