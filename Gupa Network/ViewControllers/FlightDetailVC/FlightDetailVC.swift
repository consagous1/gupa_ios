//
//  FlightDetailVC.swift
//  Gupa Network
//
//  Created by mac on 6/6/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher


class FlightDetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate{

    var loading : JGProgressHUD! = JGProgressHUD()
    
    @IBOutlet weak var lblRecordNotFound: UILabel!
    @IBOutlet weak var imgRecordNotFound: UIImageView!
    @IBOutlet weak var viewBGForAlert: UIView!
    @IBOutlet weak var viewForAlert: UIView!    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtCommentReview: UITextView!
    @IBOutlet weak var btnGreen: UIButton!
    @IBOutlet weak var btnRed: UIButton!
    @IBOutlet weak var btnGrey: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var viewPopUpForCheckInCheckOut: UIView!
    @IBOutlet weak var txtNameOnTicket: UITextField!
    @IBOutlet weak var txtBookingReference: UITextField!
    @IBOutlet weak var txtBookingNumber: UITextField!
    @IBOutlet weak var btnCheckForConnectHotel: UIButton!
    @IBOutlet weak var btnCancelCheckInCheckOut: UIButton!
    @IBOutlet weak var btnSubmitCheckInCheckOut: UIButton!
    
    @IBOutlet weak var viewForAirlinesDetail: UIView!
    @IBOutlet weak var viewForFlightDetail: UIView!
    
    @IBOutlet var objAirlineModel : AirLinesDetails?
    
    @IBOutlet weak var txtAirlinesDescription: UITextView!
    
    @IBOutlet weak var imgFlightLogo: UIImageView!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var imgComment: UIImageView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgReview: UIImageView!
    
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnCheckUncheck: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var btnReviews: UIButton!
    
    @IBOutlet weak var lblAirlinesName: UILabel!
    @IBOutlet weak var lblFlightNameAndNumber: UILabel!
    @IBOutlet weak var lblFlightSourceStationAndTime: UILabel!
    @IBOutlet weak var lblFlightDestinatoinNameAndTime: UILabel!
    @IBOutlet weak var lblCheckUncheck: UILabel!
    
    @IBOutlet weak var tblCommUsrReviewList: UITableView!
    @IBOutlet weak var viewForButtons: UIView!
    
    @IBOutlet var arrForAirlines: NSMutableArray!
    @IBOutlet var arrForFlight: NSMutableArray!
    @IBOutlet var dictForUser: NSDictionary!
    
    var dictForFlight: NSDictionary!
    var dictForAirlines: NSDictionary!
    var respDict : NSDictionary = NSDictionary()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var strTextCommentReview : String = String()
    
    var rating: NSInteger = 0
    var status: NSInteger!
    var connectedLike: NSInteger = 0
    var isCheck: NSInteger = 0
    
    var btnCheck: UIButton = UIButton()
    var btnWriteComment:UIButton = UIButton()
    var btnWriteReview:UIButton = UIButton()
    var isChecked: Bool = true
    var airlineName: String! = String()
    var airlineCode: String! = String()
    var str_flighID : String! = String()
    var str_userID : String! = String()
    
    var str_other_id : String! = String()
    
    var arrOfComments : NSMutableArray = NSMutableArray()
    var arrOfUserReview : NSMutableArray = NSMutableArray()
    var arrOfUserList : NSMutableArray = NSMutableArray()
    
    var strDepartTime: String = String()
    var strArrivalTime: String = String()
    
    var buttonSelected : UserDefaults!
    
    var isBackFromChat : Bool = false
    
    var isGoodRating : Bool = false
    var isBadRating : Bool = false
    var isUglyRating : Bool = false
//===================================================
    //MARK: View Controllers Life cycle methods
//===================================================
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
        self.viewForAlert.isHidden = true
        self.viewBGForAlert.isHidden = true
        self.imgRecordNotFound.isHidden = true
        self.lblRecordNotFound.isHidden = true
        self.viewPopUpForCheckInCheckOut.isHidden = true
        
        self.viewForFlightDetail.layer.borderWidth = 1
        self.viewForFlightDetail.layer.borderColor = UIColor.gray.cgColor
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.title = "Flight"
        self.navigationController?.isNavigationBarHidden = false
       
        
        let btnLeftBar: UIButton = UIButton ()                     // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(FlightDetailVC.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()        //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        txtCommentReview.delegate = self
        
        updateFields()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        if status == 0 {
            updateFields()
        }else{
            
        }
       
        if isBackFromChat == true {
            if str_other_id != "" {
                if let str_myID  = AppTheme.getLoginDetails().value(forKey: "id") {
                    userOnChat(str_myID as! String, str_others_id: str_other_id)
                }
            }
        }
    }
    
    //MARK: User check for online/offline
    func userOnChat(_ str_my_id: String, str_others_id: String) {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
//        let parameters : [String: AnyObject] = ["from_id" : str_my_id  as AnyObject,
//                                                "to_id" :  str_others_id as AnyObject,
//                                                "status" : "0" as AnyObject]
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(str_my_id, forKey:"from_id")
        paraDict.setValue(str_others_id, forKey:"to_id")
        paraDict.setValue("0", forKey:"status")
        
        AppTheme().callPostService(String(format: "%@user_online", AppUrl), param: paraDict) {(result, data) -> Void in
            if result == "success"
            {
                print("Change status")
            }
        }
    }
    
    func uiUpdate() {
        self.viewPopUpForCheckInCheckOut.layer.cornerRadius = 2
        self.viewPopUpForCheckInCheckOut.layer.masksToBounds = true
        self.viewPopUpForCheckInCheckOut.layer.borderWidth = 1
        self.viewPopUpForCheckInCheckOut.layer.borderColor = UIColor.lightGray.cgColor
        
        self.viewForAlert.layer.cornerRadius = 2
        self.viewForAlert.layer.masksToBounds = true
        self.viewForAlert.layer.borderWidth = 1
        self.viewForAlert.layer.borderColor = UIColor.lightGray.cgColor
        
        self.btnSubmitCheckInCheckOut.layer.cornerRadius = 2
        self.btnSubmitCheckInCheckOut.layer.masksToBounds = true
        self.btnSubmitCheckInCheckOut.layer.borderWidth = 1
        self.btnSubmitCheckInCheckOut.layer.borderColor = UIColor.lightGray.cgColor
        
        self.btnCancelCheckInCheckOut.layer.cornerRadius = 2
        self.btnCancelCheckInCheckOut.layer.masksToBounds = true
        self.btnCancelCheckInCheckOut.layer.borderWidth = 1
        self.btnCancelCheckInCheckOut.layer.borderColor = UIColor.lightGray.cgColor
        
        self.btnSubmit.layer.cornerRadius = 2
        self.btnSubmit.layer.masksToBounds = true
        self.btnSubmit.layer.borderWidth = 1
        self.btnSubmit.layer.borderColor = UIColor.lightGray.cgColor
        
        self.btnCancel.layer.cornerRadius = 2
        self.btnCancel.layer.masksToBounds = true
        self.btnCancel.layer.borderWidth = 1
        self.btnCancel.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func updateFields()  {        
        
        self.tblCommUsrReviewList.estimatedRowHeight = 100
        self.tblCommUsrReviewList.rowHeight = UITableViewAutomaticDimension
        self.tblCommUsrReviewList.setNeedsLayout()
        self.tblCommUsrReviewList.layoutIfNeeded()
        self.tblCommUsrReviewList.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        //.....
        
        for element in arrForFlight
        {
            self.dictForFlight  = element as! NSDictionary
        }
        for element in arrForAirlines{
            self.dictForAirlines  = element as! NSDictionary
        }
        if dictForFlight.count != 0 {
            if let str: String = self.dictForFlight["check_status"] as? String{
                AppTheme.setUserStatus(str as NSString)
            }
            else if let statusInt: NSInteger = self.dictForFlight["check_status"] as? NSInteger{
                AppTheme.setUserStatusInt(statusInt)
            }
            
            if let strSourceTime = dictForFlight["FLSDepartureDateTime"] as? String  {
                
                let strSourcTimeSplit = strSourceTime.characters.split(separator: "T")
                self.strDepartTime = String(strSourcTimeSplit.last!)
                
                if let str_depar_station = self.dictForFlight["departure_station"] as? String {
                     self.lblFlightDestinatoinNameAndTime.text   = "\(str_depar_station)  \(String(strSourcTimeSplit.last!))"
                }
            }
            if let str_user_id =  dictForUser["user_id"] as? String{
                str_userID = str_user_id
            }
            if let int_user_id =  dictForUser["user_id"] as? NSInteger{
                str_userID = String(int_user_id)
            }
           
            if let strDestTime = dictForFlight["FLSArrivalDateTime"] as? String  {
                let strSplit = strDestTime.characters.split(separator: "T")
                
                self.strArrivalTime = String(strSplit.last!)
                
                if let str_flightNumber_id =  dictForFlight["flight_name"] as? String{
                    str_flighID = str_flightNumber_id
                }
                if let int_FlighNum_id =  dictForFlight["flight_name"] as? NSInteger{
                    str_flighID = String(int_FlighNum_id)
                }
                 self.lblFlightNameAndNumber.text = " FLIGHT \n \(str_flighID!)"
                 if let str_arri_sta_name = self.dictForFlight["arrival_station_name"] as? String {
                     self.lblFlightSourceStationAndTime.text = "\(str_arri_sta_name)  \(String(strSplit.last!))"
                }
            }
            // var checkingStatus : NSInteger = 0         //Check user status for checking checkout
            if let strStatus: String = dictForFlight["check_status"] as? String {
                status = NSInteger(strStatus)!
            }else if let strStatus: NSInteger = dictForFlight["check_status"] as? NSInteger{
                status = strStatus
            }
            if status == 1{
                self.showRightNavigationBarButtonHide()
                self.imgRecordNotFound.isHidden = true
                self.lblRecordNotFound.isHidden = true
                self.btnCheckUncheck.setImage(UIImage(named: "img_check.png")! as UIImage, for: UIControlState())
                self.lblCheckUncheck.text = "Check In"
                self.getFlightUserList()
            }else{
                self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
                self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
                self.imgRecordNotFound.isHidden = false
                self.lblRecordNotFound.isHidden = false
                self.btnCheckUncheck.setImage(UIImage(named: "img_uncheck.png")! as UIImage, for: UIControlState())
                self.tblCommUsrReviewList.isHidden = true
            }
        }
        
        //Populate Airlines detail Flight detail and Call w_s for comment
        if dictForAirlines.count != 0 {
            if let str_air_name =  self.dictForAirlines["airline_name"] as? String{
                self.lblAirlinesName.text = str_air_name
            }
            if let str_air_id = self.dictForAirlines["airline_id"] as? String {
                airlineName = str_air_id
            }
            if let str_disc_airline = self.dictForAirlines["about"] as? String {
                 self.txtAirlinesDescription.text = str_disc_airline
            }
            if let str_airline_code = dictForFlight.value(forKey: "airline_code") as? String {
                airlineCode = str_airline_code
            }
        }
    }
//Helper method for right navigationbar button
    func rightBarBackButton(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @objc func leftBarBackButton(){                //Helper method for right navigationbar button
        self.navigationController?.popViewController(animated: true)
    }
    
    func showRightNavigationBarButtonHide()           //RightNavigationBarButton hide unhide
    {
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ic_review_white.png")!
        
        // Create the second button
        self.btnWriteReview = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnWriteReview.setImage(imgWriteReview, for: UIControlState())
        btnWriteReview.addTarget(self, action: #selector(FlightDetailVC.writeReview), for: UIControlEvents.touchUpInside)
        // Create two UIBarButtonItems
        //let item1:UIBarButtonItem = UIBarButtonItem(customView: btnWriteComment)
        let item2:UIBarButtonItem = UIBarButtonItem(customView: btnWriteReview)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [negativeSpace, item2]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
//===================================================
    //MARK: Action for the Comment and Review
//===================================================
    
    func writeComment()                                  // Method for the right navigationbar button comment
    {
        self.txtCommentReview.text = ""
        self.viewForAlert.isHidden = false
        viewBGForAlert.isHidden = false
        self.viewForAlert.layer.borderWidth = 1
        self.viewForAlert.layer.borderColor = UIColor.gray.cgColor
        self.viewForAlert.layer.cornerRadius = 5
        self.viewForAlert.layer.masksToBounds = true
        btnGreen.isHidden = true
        btnGrey.isHidden = true
        btnRed.isHidden = true
        self.lblTitle.text = "Please write your comment."
        self.txtCommentReview.text = ""
        self.txtCommentReview.layer.borderWidth = 1
        self.txtCommentReview.layer.borderColor = UIColor.gray.cgColor
        self.btnSubmit.tag = 10
    }
    
    
    @objc func writeReview() {                                                                    //Review Action
        tblCommUsrReviewList.isHidden = true
        self.viewForAlert.isHidden = false
        viewBGForAlert.isHidden = false
        self.viewForAlert.layer.borderWidth = 1
        self.viewForAlert.layer.borderColor = UIColor.gray.cgColor
        self.viewForAlert.layer.cornerRadius = 5
        self.viewForAlert.layer.masksToBounds = true
        btnGreen.isHidden = false
        btnGrey.isHidden = false
        btnRed.isHidden = false
        self.lblTitle.text = "Please write your Review."
        self.txtCommentReview.text = "Please write your review..."
        self.txtCommentReview.layer.borderWidth = 1
        self.txtCommentReview.layer.borderColor = UIColor.gray.cgColor
        self.btnSubmit.tag = 11
    }
func cancelAction()
{
    print("check for review")
    }
   
    // MARK: -  UItextView Delegate
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == txtCommentReview
        {
            if txtCommentReview.text == "Please write your review..." {
                txtCommentReview.text = ""
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtCommentReview
        {
            if txtCommentReview.text == "Please write your review..." {
                txtCommentReview.text = ""
            }
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == txtCommentReview{
            if txtCommentReview.text == "" {
                txtCommentReview.text = "Please write your review..."
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.txtCommentReview.resignFirstResponder()
    }
    
//========================================================
   //MARK: UITableview Datasource and delegate methods
//========================================================
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
        if btnCheck.tag == 3{
            if self.arrOfResponseKey.count != 0 {
                row =  self.arrOfResponseKey.count
                return row
            }else {
                return 0
            }
        }else if btnCheck.tag == 2{
            if self.arrOfResponseKey.count != 0{
                row =  self.arrOfResponseKey.count
                self.loading.dismiss()
                return row
            } else {
                self.loading.dismiss()
                return 0
            }
        }else{
            if self.arrOfResponseKey.count != 0{
                row =  self.arrOfResponseKey.count
                return row
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        
        ///////....20/07/16
        self.tblCommUsrReviewList.estimatedRowHeight = 100
        self.tblCommUsrReviewList.rowHeight = UITableViewAutomaticDimension
        self.tblCommUsrReviewList.setNeedsLayout()
        self.tblCommUsrReviewList.layoutIfNeeded()
        self.tblCommUsrReviewList.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        //.....
        isBackFromChat = true
        if btnCheck.tag == 1{
            self.loading.dismiss()
            loading.dismiss()
            let cell : CommentCell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
            if let str_comment =  dic["comment"] as? String{
                cell.lblUserComments.text = str_comment
            }
            if let str_profile_pic = dic["profile_pic"] as? String {
                let url = URL(string: str_profile_pic)
                
                cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, imageURL) in
                    print(image?.description ?? "")
                })
                
//                cell.imgUser.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
            }
            view.layer.cornerRadius = 5
            return cell
        }else if btnCheck.tag == 2{
            self.loading.dismiss()
            loading.dismiss()
            var cell : UserChatCell! = tableView.dequeueReusableCell(withIdentifier: "UserChatCell", for: indexPath) as! UserChatCell
            if cell == nil {
                cell = UserChatCell(style: UITableViewCellStyle.default, reuseIdentifier: "UserChatCell")
            }
            if let str_user_name : String = dic["username"] as? String{
                cell.lblUserComments.text  = str_user_name
            }
            if let str_user_profile : String = dic["profile_pic"] as? String{
                let url = URL(string:str_user_profile)
                
                cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, imageURL) in
                    print(image?.description ?? "")
                })
                
//                cell.imgUser.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
            }
            if  let str_otherId = dic["user_id"] as? String {
                
                self.str_other_id = str_otherId
            }
            cell.btnChat.addTarget(self, action: #selector(FlightDetailVC.chat(_:)), for: UIControlEvents.touchUpInside)
            cell.btnChat.tag = indexPath.row
            
            return cell

        }else if btnCheck.tag == 3{
            self.loading.dismiss()
            loading.dismiss()
            let cell : ReviewCell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
            
            var rating : NSInteger = 0
            
            if let strRating: String = dic["rating"] as? String {
                if strRating == "" {
                  rating = 0 // NSInteger(strRating)!
                }else{
                     rating =  NSInteger(strRating)!
                }
                
            }else if let strRating: NSInteger = dic["rating"] as? NSInteger{
                rating = strRating
            }
            if rating == 0 {
                cell.imgReview.image = UIImage(named: "")
            }
               if rating == 1 {
                cell.imgReview.image = UIImage(named: "good_on.png")
               }else if rating == 2{
                cell.imgReview.image = UIImage(named: "bad_on.png")
            }else if rating == 3{
                cell.imgReview.image = UIImage(named: "bad.png")
            }
            
            //cell.imgReview.image
            if let str_user_name : String = dic["review"] as? String{
                 cell.lblUserReview.text   = str_user_name
            }
            
            if let str_user_profile : String = dic["profile_pic"] as? String{
                let url = URL(string:str_user_profile)
                
                cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, imageURL) in
                    print(image?.description ?? "")
                })
                
//                cell.imgUser.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
            }
            if let str_date : String = dic["date_time"] as? String{
                cell.lblDate.text   = str_date
            }
            loading.dismiss()
            return cell
        }else {
            loading.dismiss()
            loading.dismiss()
            let cell : CommentCell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
            cell.lblUserComments.text = dic["comment"] as? String
            let url = URL(string:(dic["profile_pic"] as? String)!)
            
            cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                (image, error, cacheType, imageURL) in
                print(image?.description ?? "")
            })
            
//            cell.imgUser.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
            view.layer.cornerRadius = 5
            return cell
        }
    }

    @objc func chat(_ sender: UIButton!)
     {
        var destVC = ChatViewController()
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//        if let str_myId =  AppTheme.getLoginDetails().value(forKey: "id") {
//            destVC.str_others_id = str_other_id
//            destVC.str_my_id = str_myId as! String
//        }
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    
//==================================================================================
   //MARK: Action for the Check-in check-out, view comment, view user, view review
   //MARK: show graph for single flight
//==================================================================================
    
    @IBAction func btnCheckUncheck(_ sender: AnyObject)
    {
       if status == 0 {
        self.viewBGForAlert.isHidden = false
        self.viewPopUpForCheckInCheckOut.bringSubview(toFront: self.viewBGForAlert)
        self.viewPopUpForCheckInCheckOut.isHidden = false
        uiUpdate()
       }else{
        status = 0
                    self.imgRecordNotFound.isHidden = false
                    self.lblRecordNotFound.isHidden = false
                    self.btnCheckUncheck.setImage(UIImage(named: "img_uncheck.png") as UIImage?, for: UIControlState())
                    self.lblCheckUncheck.text = "check Out"
        
//                    let parameters : [String: AnyObject] = ["user_id" : dictForUser["user_id"]! as AnyObject,
//                        "flight_id" : dictForFlight["FlightNumber"]! as AnyObject,
//                    "check_status": status as AnyObject,
//                    "from": dictForFlight["departure_station_code"]! as AnyObject,
//                    "to": dictForFlight["arrival_station_code"]! as AnyObject,
//                    "airline_code" : dictForFlight["airline_code"]! as AnyObject]
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(dictForUser["user_id"]!, forKey:"user_id")
        paraDict.setValue(dictForFlight["FlightNumber"], forKey:"flight_id")
        paraDict.setValue(status, forKey:"check_status")
        paraDict.setValue(dictForFlight["departure_station_code"]!, forKey:"from")
        paraDict.setValue(dictForFlight["arrival_station_code"]!, forKey:"to")
        paraDict.setValue(dictForFlight["airline_code"]!, forKey:"airline_code")
        
                    AppTheme().callPostService(String(format: "%@user_check?", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
        
                        if result == "success"
                        {
                            self.loading.dismiss()
                            AppTheme.setUserFlight(0)
                            self.btnWriteComment.isHidden = true
                            self.btnWriteReview.isHidden = true
                            self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
                            self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
                            self.loading.textLabel.text = "Check out successfully!"
                            self.loading.show(in: self.view)
                            self.loading.dismiss(afterDelay: 3.2)
                            self.tblCommUsrReviewList.isHidden = true
                        }else{
                            self.loading.dismiss()
                            self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
                            self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
                            self.loading.textLabel.text = "Sorry there is no Air Services between entered station"
                            self.loading.show(in: self.view)
                            self.loading.dismiss(afterDelay: 3.2)
                            self.tblCommUsrReviewList.isHidden = true
                        }
                    }
        }
    }

    @IBAction func btnComentUserReviews(_ sender: UIButton)
    {
        let button: UIButton = sender as UIButton
        buttonSelected = UserDefaults.standard
        buttonSelected.set(button.tag, forKey: "buttonTag")
        
        if button.tag == 1
        {
           if status == 1
           {
            self.getCommentList()
           }else{
            self.loading.dismiss()
            self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
            self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
//            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry your not check In", showInView: self.view)
            self.imgRecordNotFound.isHidden = false
            self.lblRecordNotFound.isHidden = false
            }
           
        }else if button.tag == 2{
            if status == 1 {
               self.getFlightUserList()
            }else{
                self.loading.dismiss()
                self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
                self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
//                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry your not check In", showInView: self.view)
            }
        }else if button.tag == 3  {
            if status == 1  {
             self.getFlightReview()
            }else{
                self.loading.dismiss()
                self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
                self.imgReview.image = UIImage(named : "ic_review_on.png")! as UIImage
//                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry your not check In", showInView: self.view)
            }
        }
}
    
    func getCommentList()
    {
        btnCheck.tag = 1
        self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
        self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
        
        let parameters : [String: AnyObject] = ["user_id" : dictForUser["user_id"]! as AnyObject,
                                                "flight_id" : dictForFlight["FlightNumber"]! as AnyObject ]
        
        AppTheme().callGetService(String(format: "%@get_comment", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.imgRecordNotFound.isHidden = true
                self.lblRecordNotFound.isHidden = true
                self.respDict = data as! NSMutableDictionary
                print(self.respDict)
                self.arrOfResponseKey = self.respDict["response"] as! NSMutableArray
                // print(self.arrOfResponseKey)
                self.tblCommUsrReviewList.reloadData()
            }
            else
            {
                self.loading.dismiss()
//                self.loading = JGProgressHUD(style: JGProgressHUDStyle.Dark)
//                self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
////                self.loading.textLabel.text = "Sorry there is no record found"
//                self.loading.showInView(self.view)
//                self.loading.dismissAfterDelay(3.2)
                // self.tblCommUsrReviewList.hidden = true
            }
            self.tblCommUsrReviewList.reloadData()
        }
    }
    
    func getFlightUserList()
    {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        btnCheck.tag = 2
        self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
        self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
        self.tblCommUsrReviewList.isHidden = false
        
        let parameters : [String: AnyObject] = ["user_id" : dictForUser["user_id"]! as AnyObject,
                                                "flight_id" : dictForFlight["FlightNumber"]! as AnyObject]
        
        AppTheme().callGetService(String(format: "%@user_flight", AppUrl), param: parameters) {(result, data) -> Void in                 ///Call services
            
            if result == "success"
            {
                self.imgRecordNotFound.isHidden = true
                self.lblRecordNotFound.isHidden = true
                self.respDict = data as! NSDictionary
                if let tempArr: NSArray = self.respDict["response"] as? NSArray
                {
                    if tempArr.count == 0
                    {
                        self.arrOfResponseKey = tempArr.mutableCopy() as! NSMutableArray
                        if self.arrOfResponseKey.count == 0 || self.arrOfResponseKey.isEqual(nil ){
                            self.loading.dismiss()
                        }
                    }else {
                        self.arrOfResponseKey = tempArr.mutableCopy() as! NSMutableArray
                    }
                }else{
                    self.arrOfResponseKey = self.respDict["response"] as! NSMutableArray
                }
                self.tblCommUsrReviewList.isHidden = false
                self.tblCommUsrReviewList.reloadData()
            }
            else
            {
                self.loading.dismiss()
                self.imgRecordNotFound.isHidden = false
                self.lblRecordNotFound.isHidden = false
                self.tblCommUsrReviewList.isHidden = true
            }
        }
    }
    
    func getFlightReview()
    {
        btnCheck.tag = 3
        self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
        self.imgReview.image = UIImage(named : "ic_review_on.png")! as UIImage
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        let parameters : [String: AnyObject] = ["flight_id" : dictForFlight["FlightNumber"]! as AnyObject ]
        AppTheme().callGetService(String(format: "%@get_reviews", AppUrl), param: parameters) {(result, data) -> Void in                 ///Call services
            
            if result == "success"{
                self.respDict = data as! NSDictionary
                print(self.respDict)
                if let arrRes: NSMutableArray = self.respDict["response"] as? NSMutableArray{
                    self.arrOfResponseKey = arrRes
                }else if let arrRes: NSArray = self.respDict["response"] as? NSArray{
                    self.arrOfResponseKey = NSMutableArray(array: arrRes)
                }
                self.tblCommUsrReviewList.isHidden = false
                self.tblCommUsrReviewList.reloadData()
            }
            else
            {
                self.loading.dismiss()
                self.tblCommUsrReviewList.isHidden = true
            }
            self.tblCommUsrReviewList.reloadData()
        }
    }
    
    func getFlightRating() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        if let str_flight_id = dictForFlight["FlightNumber"] as? String {
            let parameters : [String: AnyObject] = ["flight_id" : str_flight_id  as AnyObject]
            AppTheme().callGetService(String(format: "%@SingleFlightRating", AppUrl), param: parameters) {(result, data) -> Void in                 ///Call services
                
                if result == "success"   {
                    self.loading.dismiss()
                    self.respDict = data as! NSDictionary
                    var dic_temp : NSDictionary! = NSDictionary()
                    if let dic_Res: NSMutableDictionary = self.respDict["response"] as? NSMutableDictionary {
                       dic_temp = dic_Res
                    }else if let dict_Res: NSDictionary = self.respDict["response"] as? NSDictionary {
                       dic_temp = dict_Res
                    }
                    let barRatingVC = self.storyboard?.instantiateViewController(withIdentifier: "FlightTransportGraphController") as! FlightTransportGraphController
                        barRatingVC.strCheck = "comeFromFlightDetail"
                        if dic_temp.count != 0 {
                            barRatingVC.flightHotelDetailDict = dic_temp
                        }
                        self.navigationController?.pushViewController(barRatingVC, animated: true)
                    } else  {
                    self.loading.dismiss()
                    let alertView = UIAlertView(title: "Message", message: "This flight has no rating", delegate: nil, cancelButtonTitle: "Ok")
                    alertView.show()
//                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                }
            }
        }
    }
    
//======================================================
    // Action for the show graph for single flight
//======================================================
    @IBAction func btnCheck(_ sender: AnyObject)
    {
           getFlightRating()
    }
    
    
//=====================================================================================
    //MARK: Actions for buttons on navigationbar- Rating, Comment-post, Review- post
//=====================================================================================
    
    @IBAction func btnRating(_ sender: UIButton)
    {
        if sender.tag == 100
        {
            isUglyRating = false
            isBadRating = false
            if isGoodRating == false {
                self.rating = 1
                isGoodRating = true
                self.btnGreen.setImage(UIImage(named: "good_on.png")! as UIImage, for: UIControlState())
            }else{
                self.rating = 0
                isGoodRating = false
                self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            }
            self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())
            self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
            //            self.rating = 1
            //            self.btnGreen.setImage(UIImage(named: "good_on.png")! as UIImage, forState: .Normal)
            //            self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, forState: .Normal)
            //            self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, forState: .Normal)
        }
        else if sender.tag == 101
        {
            isGoodRating = false
            isUglyRating = false
            if isBadRating == false {
                self.rating = 2
                isBadRating = true
                self.btnRed.setImage(UIImage(named: "bad_on.png")! as UIImage, for: UIControlState())
            }else{
                self.rating = 0
                isBadRating = false
                self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())
            }
            self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
            /*  self.rating = 2
             self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, forState: .Normal)
             self.btnRed.setImage(UIImage(named: "bad_on.png")! as UIImage, forState: .Normal)
             self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, forState: .Normal) */
        }
        else if sender.tag == 102
        {
            isGoodRating = false
            isBadRating = false
            if isUglyRating == false {
                self.rating = 3
                isUglyRating = true
                self.btnGrey.setImage(UIImage(named: "bad.png")! as UIImage, for: UIControlState())
            }else{
                isUglyRating = false
                self.rating = 0
                self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
            }
            self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())
            /* self.rating = 3
             self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, forState: .Normal)
             self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, forState: .Normal)
             self.btnGrey.setImage(UIImage(named: "bad.png")! as UIImage, forState: .Normal) */
        }
    }

    @IBAction func btnSumit(_ sender: AnyObject) {
       self.startProgressBar()
       if btnSubmit.tag == 10 {
                if self.txtCommentReview.text != "" {
                    if  txtCommentReview.text != "Please write your review" ||  txtCommentReview.text != "Please write your review." || txtCommentReview.text != "Please write your review.."  || txtCommentReview.text != "Please write your review..." {
                        txtCommentReview.text = ""
                    }
//                   let parameters : [String: AnyObject] = ["user_id" : self.dictForUser["user_id"]! as AnyObject,
//                    "flight_id" : dictForFlight["FlightNumber"]! as AnyObject,
//                    "comment" :  self.txtCommentReview.text as AnyObject]
                    
                    let paraDict = NSMutableDictionary()
                    paraDict.setValue(self.dictForUser["user_id"]!, forKey:"user_id")
                    paraDict.setValue(dictForFlight["FlightNumber"]!, forKey:"flight_id")
                    paraDict.setValue(self.txtCommentReview.text, forKey:"comment")
                    
                AppTheme().callPostService(String(format: "%@comment", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
                    if result == "success" {
                        self.respDict = data as! NSDictionary
                        self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Post sent", showInView: self.view)
                        self.viewBGForAlert.isHidden = true
                        self.viewForAlert.isHidden = true
                        self.tblCommUsrReviewList.reloadData()
                    }else{
                        self.loading.dismiss()
                        self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                    }
                }
            }else{
                    self.loading.dismiss()
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please write comment!", showInView: self.view)
                }
        }else if btnSubmit.tag == 11
        {
            if txtCommentReview.text != "Please write your review..." && txtCommentReview.text != "" || rating == 0 || rating == 1 || rating == 2 || rating == 3
            {
                if txtCommentReview.text != "Please write your review..."  && txtCommentReview.text != "" ||  rating == 0 || rating == 1 || rating == 2 || rating == 3{
                    if txtCommentReview.text == "Please write your review..." && rating == 0{
                        self.loading.dismiss()
                        self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter the review or rating.", showInView: self.view)
                    }else{
                        if txtCommentReview.text == "Please write your review..."{
                            txtCommentReview.text = ""
                        }
                        if str_flighID != nil || str_flighID != "" || str_userID != nil || str_userID != "" {
//                            let parameters : [String: AnyObject] = ["user_id" : str_userID as AnyObject,
//                                                                    "flight_id" : str_flighID as AnyObject,
//                                                                    "review" :  self.txtCommentReview.text as AnyObject,
//                                                                    "rating" : rating as AnyObject,
//                                                                    "airline_id": airlineName as AnyObject,
//                                                                    "airline_code": airlineCode as AnyObject]
                            
                            
                            let paraDict = NSMutableDictionary()
                            paraDict.setValue(str_userID, forKey:"user_id")
                            paraDict.setValue(str_flighID, forKey:"flight_id")
                            paraDict.setValue(self.txtCommentReview.text, forKey:"review")
                            paraDict.setValue(rating, forKey:"rating")
                            paraDict.setValue(airlineName, forKey:"airline_id")
                            paraDict.setValue(airlineCode, forKey:"airline_code")
                            
                            AppTheme().callPostService(String(format: "%@review", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
                                
                                if result == "success"
                                {
                                    self.respDict = data as! NSDictionary
                                    self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
                                    self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())
                                    self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
                                    self.rating = 0
                                    self.loading.dismiss()
                                    self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Post Sent", showInView: self.view)
                                    self.viewBGForAlert.isHidden = true
                                    self.viewForAlert.isHidden = true
                                    self.getFlightReview()
                                    self.tblCommUsrReviewList.reloadData()
                                }else {
                                    self.loading.dismiss()
                                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                                    self.viewBGForAlert.isHidden = true
                                    self.viewForAlert.isHidden = true
                                }
                            }
                        }
                    }
                }else{
                    self.loading.dismiss()
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter the review or rating.", showInView: self.view)
                }
            }else{
                self.loading.dismiss()
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter the review or rating.", showInView: self.view)
            }
       /* if  rating != 0
        {
            if txtCommentReview.text != "Please write your review" ||  txtCommentReview.text != "Please write your review." || txtCommentReview.text != "Please write your review.."  || txtCommentReview.text != "Please write your review..." {
                txtCommentReview.text = ""
            }
            if str_flighID != nil || str_flighID != "" || str_userID != nil || str_userID != "" {
                let parameters : [String: AnyObject] = ["user_id" : str_userID as AnyObject,
                                                        "flight_id" : str_flighID as AnyObject,
                                                        "review" :  self.txtCommentReview.text as AnyObject,
                                                        "rating" : rating as AnyObject,
                                                        "airline_id": airlineName as AnyObject,
                                                        "airline_code": airlineCode as AnyObject]
                
                AppTheme().callPostService(String(format: "%@review", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
                    
                    if result == "success"
                    {
                        self.respDict = data as! NSDictionary
                        print(self.respDict)
                        self.loading.dismiss()
                        self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Post Sent", showInView: self.view)
                        self.viewBGForAlert.hidden = true
                        self.viewForAlert.hidden = true
                        self.tblCommUsrReviewList.reloadData()
                    }else {
                        self.loading.dismiss()
                        self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                        self.viewBGForAlert.hidden = true
                        self.viewForAlert.hidden = true
                    }
                }
            }
    }else{
            self.loading.dismiss()
            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter the review or rating.", showInView: self.view)
        } */
     }
    }
    @IBAction func btnCancel(_ sender: AnyObject) {
        self.viewBGForAlert.isHidden = true
    }
    
    
    func startProgressBar(){
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    func callUserComment()         //Call user comment to show
    {
        if str_userID != "" || str_userID != nil || str_flighID != "" || str_flighID != nil {
             let parameters : [String: AnyObject] = ["user_id" : str_userID as AnyObject,
             "flight_id" : str_flighID as AnyObject ]
             
             AppTheme().callGetService(String(format: "%@get_comment", AppUrl), param: parameters) {(result, data) -> Void in                                   ///Call services
             
             if result == "success"{
             self.respDict = data as! NSDictionary
             print(self.respDict)
             self.arrOfResponseKey = self.respDict["response"] as! NSMutableArray
             self.tblCommUsrReviewList.reloadData()
             }else{
             self.loading.dismiss()
//             self.loading = JGProgressHUD(style: JGProgressHUDStyle.Dark)
//             self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
//             self.loading.textLabel.text = "No record found!"
//             self.loading.showInView(self.view)
//             self.loading.dismissAfterDelay(3.2)
             }
           }
        }
    }
    
//========================================================
    //MARK: Actions for Cancel, Submit on alert view
//========================================================
    
    @IBAction func btnCancelForCheckInCheckOut(_ sender: AnyObject)
    {
        self.viewBGForAlert.isHidden = true
        self.viewPopUpForCheckInCheckOut.isHidden = true
    }
    @IBAction func btnSubmitForCheckInCheckOut(_ sender: AnyObject)
    {
        
       if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if self.txtNameOnTicket.text != ""  && txtBookingReference.text != "" && txtBookingNumber.text != ""{
            self.viewBGForAlert.isHidden = true
            self.viewPopUpForCheckInCheckOut.isHidden = true
            btnCheckUncheck.setImage(UIImage(named:"checked"),for:UIControlState())
                    if status == 0
                    {
                     connectedLike = 1
                     self.showRightNavigationBarButtonHide()
                     self.imgRecordNotFound.isHidden = true
                     self.lblRecordNotFound.isHidden = true
                     status = 1
                     self.btnCheckUncheck.setImage(UIImage(named: "img_check.png") as UIImage?, for: UIControlState())
                     self.lblCheckUncheck.text = "check In"
                        if str_userID != "" || str_userID != nil || str_flighID != "" || str_flighID != nil {
                            if let str_depart_sta_code =  dictForFlight["departure_station_code"] as? String {
                                if let str_arriv_sta_code = dictForFlight["arrival_station_code"] as? String {
                                    if let str_airline_name = dictForFlight["airline_name"] as? String {
                                        if let str_air_code = dictForFlight .value(forKey: "airline_code") as? String {
                                            
                                            
//                                            let parameters : [String: AnyObject] = ["user_id" : str_userID as AnyObject,
//                                                                                    "flight_id" : str_flighID as AnyObject,
//                                                                                    "check_status": status as AnyObject,
//                                                                                    "from": str_depart_sta_code as AnyObject,
//                                                                                    "to": str_arriv_sta_code as AnyObject,
//                                                                                    "airline_name": str_airline_name as AnyObject,
//                                                                                    "departure_time": self.strDepartTime as AnyObject,
//                                                                                    "arrival_time": self.strArrivalTime as AnyObject,
//                                                                                    "ticket_name": self.txtNameOnTicket as AnyObject,
//                                                                                    "booking_reference": self.txtBookingReference as AnyObject,
//                                                                                    "ticket_number": self.txtBookingNumber as AnyObject,
//                                                                                    "connected_like": connectedLike as AnyObject,
//                                                                                    "airline_code":str_air_code as AnyObject]
                                            
                                            let paraDict = NSMutableDictionary()
                                            paraDict.setValue(str_userID, forKey:"user_id")
                                            paraDict.setValue(str_flighID, forKey:"flight_id")
                                            paraDict.setValue(status, forKey:"check_status")
                                            paraDict.setValue(str_depart_sta_code, forKey:"from")
                                            paraDict.setValue(str_arriv_sta_code, forKey:"to")
                                            paraDict.setValue(str_airline_name, forKey:"airline_name")
                                            paraDict.setValue(self.strDepartTime, forKey:"departure_time")
                                            paraDict.setValue(self.strArrivalTime, forKey:"arrival_time")
                                            paraDict.setValue(self.txtBookingReference, forKey:"ticket_name")
                                            paraDict.setValue(self.txtBookingNumber, forKey:"booking_reference")
                                            paraDict.setValue(self.txtBookingNumber, forKey:"ticket_number")
                                            paraDict.setValue(connectedLike, forKey:"connected_like")
                                            paraDict.setValue(str_air_code, forKey:"airline_code")
                                            
                                            AppTheme().callPostService(String(format: "%@user_check?", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
                                                
                                                if result == "success"
                                                {
                                                    self.btnWriteComment.isHidden = false
                                                    self.btnWriteReview.isHidden = false
                                                    self.loading.dismiss()
                                                    if let key = UserDefaults.standard.value(forKey: "buttonTag") as? NSInteger
                                                    {
                                                        if  key == 1{
                                                            self.getCommentList()
                                                        }else if  key == 2{
                                                            self.getFlightUserList()
                                                        } else if  key == 3  {
                                                            self.getFlightReview()
                                                        }
                                                        self.tblCommUsrReviewList.isHidden = false
                                                    }
                                                }else{
                                                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry no data found", showInView: self.view)
                                                    self.tblCommUsrReviewList.isHidden = true
                                                }
                                            }
                                            self.tblCommUsrReviewList.isHidden = false
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        status = 0
                        self.imgRecordNotFound.isHidden = false
                        self.lblRecordNotFound.isHidden = false
                        self.btnCheckUncheck.setImage(UIImage(named: "img_uncheck.png") as UIImage?, for: UIControlState())
                        self.lblCheckUncheck.text = "check Out"
                        
                        let parameters : [String: AnyObject] = ["user_id" : dictForUser["user_id"]! as AnyObject,
                            "flight_id" : dictForFlight["FlightNumber"]! as AnyObject,
                        "check_status": status as AnyObject,
                        "from": dictForFlight["departure_station_code"]! as AnyObject,
                        "to": dictForFlight["arrival_station_code"]! as AnyObject]
            
                        AppTheme().callGetService(String(format: "%@user_check", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
            
                        if result == "success"
                          {
                            self.btnWriteComment.isHidden = true
                            self.btnWriteReview.isHidden = true
                            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Check Out Successful.!", showInView: self.view)
                            self.imgRecordNotFound.isHidden = false
                            self.lblRecordNotFound.isHidden = false
                          }
                         else
                          {
                            self.btnWriteComment.isHidden = true
                            self.btnWriteReview.isHidden = true
                            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Check Out Successful.!", showInView: self.view)
                            self.imgRecordNotFound.isHidden = false
                            self.lblRecordNotFound.isHidden = false
                            }
                        }
                        self.tblCommUsrReviewList.isHidden = true
                    }
             }else{
                self.loading.dismiss()
            if txtNameOnTicket.text == "" || txtNameOnTicket.text == nil {
                 self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter name on ticket", showInView: self.view)
            }else if txtBookingReference.text == "" || txtBookingReference.text == nil{
                 self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter booking reference", showInView: self.view)
            }else if txtBookingNumber.text == "" || txtBookingNumber.text == nil{
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter ticket number", showInView: self.view)
            }
//                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please fill all the fields.", showInView: self.view)
                self.tblCommUsrReviewList.isHidden = true
             }
    }
    @IBAction func btnCheckForConnectHotel(_ sender: AnyObject)
    {
        if isCheck == 0{
            isCheck = 1
            connectedLike = 1
            self.btnCheckForConnectHotel.setImage(UIImage(named: "img_check.png") as UIImage?, for: UIControlState())
        }else {
            isCheck = 0
            connectedLike = 0
            self.btnCheckForConnectHotel.setImage(UIImage(named: "img_uncheck.png") as UIImage?, for: UIControlState())
        }
    }
}
