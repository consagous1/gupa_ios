//
//  RaiseAQureyVC.swift
//  Gupa Network
//
//  Created by Apple on 18/01/18.
//  Copyright © 2018 Consagous. All rights reserved.
//

import UIKit
import JGProgressHUD

class RaiseAQureyVC: UIViewController {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var txtEmail         : UITextField!
    @IBOutlet var txtTitle         : UITextField!
    @IBOutlet var txtViewMessage   : UITextView!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var loading : JGProgressHUD! = JGProgressHUD()
    
    //****************************************************
    // MARK: - Life Cycle Methods
    //****************************************************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Raise A Query"
        setNavigationController()
        
        appDelegate.centerNav = self.navigationController
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func canSubmit() -> Bool {
        
        if txtEmail.text == "" || !AppTheme().isValidEmail(txtEmail.text as String!)
        {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter valid Email.", showInView: self.view)
            return false
        } else if txtTitle.text == ""
        {
            _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Title.", showInView: self.view)
            return false
        } else if txtViewMessage.text == ""
        {
            _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter your message.", showInView: self.view)
            return false
        }
        return true
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func btnSend_Action(_: UIButton) {
        
        resignFirstResponder()
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if canSubmit() {
           sendfeedback_ApiCall()
        }
    }
    
    //****************************************************
    // MARK: - API Methods
    //****************************************************
    
    /**
     **   Submit Qurey API Call
     **/
    
    func sendfeedback_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"user_id")
        paraDict.setValue(txtEmail.text, forKey:"user_email")
        paraDict.setValue(txtTitle.text, forKey:"title")
        paraDict.setValue(txtViewMessage.text, forKey:"msg")

        AppTheme().callPostService(String(format: "%@feedback", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
        if result == "success"
        {
            self.loading.dismiss()
                
            let alertVC = UIAlertController(title: "Message sent successfully.", message: "", preferredStyle: .alert)
                
            let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                let centerNavCont = UINavigationController(rootViewController: homeVC)
                centerNavCont.setViewControllers([homeVC], animated: true)
                self.revealViewController().setFront(centerNavCont, animated: true)
                self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
            }
                
                alertVC .addAction(actionOk)
                self .present(alertVC, animated: true, completion: nil)
            
//                self.txtEmail.text = ""
//                self.txtTitle.text = ""
//                self.txtViewMessage.text = ""
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
}
