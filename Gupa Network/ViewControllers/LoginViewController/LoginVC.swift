//
//  LoginVC.swift
//  Gupa Network
//
//  Created by mac on 5/2/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import FacebookLogin

class LoginVC: UIViewController {
    
    @IBOutlet var vwContainer : UIView!
    
    @IBOutlet var txtUserName : UITextField!
    @IBOutlet var txtPassword : UITextField!
    @IBOutlet weak var alertView : UIView!
    @IBOutlet weak var subAlertView : UIView!
    @IBOutlet weak var emailTextView : UITextField!
    @IBOutlet weak var cancelButtonActionOnAlert : UIButton!
    @IBOutlet weak var sendActionOnAlert : UIButton!
    @IBOutlet weak var fbLoginBtn: UIButton!
    var databaseManager = DatabaseManager.sharedDBInterface // Local database

    var loading : JGProgressHUD = JGProgressHUD(style: JGProgressHUDStyle.dark)
    
    //==================================
    // MARK: - VC life cycle method
    //==================================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        alertView.isHidden = true

        cancelButtonActionOnAlert.layer.borderColor = UIColor.lightGray.cgColor
        cancelButtonActionOnAlert.layer.borderWidth = 0.5;
        cancelButtonActionOnAlert.layer.cornerRadius = 4
        
        sendActionOnAlert.layer.borderColor = UIColor.lightGray.cgColor
        sendActionOnAlert.layer.borderWidth = 0.5;
        sendActionOnAlert.layer.cornerRadius = 4
        
        if let isLogin = AppTheme.getIsLogin() as? Bool {
            if isLogin == true{
                let newRegiVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(newRegiVC, animated: true)
            }
        }
        self.txtUserName.text = ""
        self.txtPassword.text = ""
        
        for view in self.vwContainer.subviews {
            
            if let txtField : UITextField = view as? UITextField
            {
                let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 35))
                paddingView.backgroundColor = UIColor.clear
                txtField.leftView = paddingView
                txtField.leftViewMode = .always
                
                txtField.layer.borderColor = UIColor.lightGray.cgColor
                txtField.layer.borderWidth = 1.0;
                txtField.layer.cornerRadius = 2
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }

    //==========================================================
    // MARK: - IBActions for the Login and API calling
    //==========================================================
    
    @IBAction func loginAction (_ sender: UIButton)
    {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if txtUserName.text == "" //|| !AppTheme().isValidEmail(txtUserName.text as String!)
        {
           _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Email.", showInView: self.view)
            
        } else if txtPassword.text == ""
        {
           _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Password.", showInView: self.view)
        } else
        {
            self.loading = JGProgressHUD(style: .dark)
            self.loading.textLabel.text = "Loading.."
            self.loading.show(in: self.view)
            var deviceT = ""
            
            if let deviceToken = UserDefaults.standard.value(forKey: "dt") as? String {
                deviceT = deviceToken
            } else {
                if deviceT == "" || deviceT.isEmpty == true {
                    deviceT = "" // AppTheme.deviceToken
                }
            }
            
            let parameters : [String: AnyObject] = ["username" : txtUserName.text as AnyObject,
                                                    "password" : txtPassword.text as AnyObject,
                                                    "device_id" : deviceT as AnyObject,
                                                    "device_type" : AppTheme.deviceType as AnyObject]   //Create param here
            
            
            AppTheme.setUserEmail_Login(txtUserName.text! as NSString)
            
            //call api using alamofire
            AppTheme().callGetService(String(format: "%@doLogin", AppUrl), param: parameters) { (result, data) -> Void in
                
                if(result == "success")
                {
                    DispatchQueue.main.async {
                    
                        self.loading.dismiss()
                    
                        var departments : NSDictionary = NSDictionary()
                        
                        departments = data as! NSDictionary
                        
                        if let str_active = data.value(forKey: "is_active") as? NSInteger{
                            if str_active == 1 {
                                
                                print("Department \(departments)")
                                AppTheme.setLoginDetails(departments)
                                
                                 if !(data.value(forKey: "mongo_obj") is NSNull) {
                                    ApplicationPreference.saveMongoUserId(userId: (data.value(forKey: "mongo_obj") as? String)!)
                                }
                                
                                self.databaseManager.initDatabaseManager()
                                
                                _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: departments.value(forKey: "message") as! String, showInView: self.view)
                                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                            
                        } else {
                            var destVC : VerificationControllerl = VerificationControllerl()
                            destVC = self.storyboard?.instantiateViewController(withIdentifier: "VerificationControllerl") as! VerificationControllerl
                            self.navigationController?.pushViewController(destVC, animated: true)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        
                        self.loading.dismiss()
                        
                        if data as! String == "Your Account Is Inactive. Please Active Your Account" {
                        
                            let alertVC = UIAlertController(title: "Alert", message: data as? String, preferredStyle: .alert)
                            let actionCancel = UIAlertAction(title: "Cancel", style: .default){ __ in
                                self .dismiss(animated: true, completion: nil)
                            }
                            let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                                let destVC = self.storyboard?.instantiateViewController(withIdentifier: "VerificationControllerl") as! VerificationControllerl
                                self.navigationController?.pushViewController(destVC, animated: true)
                            }
                            
                            alertVC .addAction(actionCancel)
                            alertVC .addAction(actionOk)
                            self .present(alertVC, animated: true, completion: nil)
                        } else {
                            self.loading.dismiss()

                            let alertVC = UIAlertController(title: "Alert", message: data as? String, preferredStyle: .alert)
                            let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                            }
                            
                            alertVC .addAction(actionOk)
                            self .present(alertVC, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    
    func faceBookLogin_APICall(faceBookId :String ,userName : String ,userEmail : String) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        var firstName : String?
        var lastName : String?
        
        if userName.isEmpty {
              firstName = ""
              lastName = ""
        } else {
             let fullNameArr = userName.components(separatedBy: " ")
                 firstName = fullNameArr[0]
                lastName  = fullNameArr[1]
        }
        
        let paraDict = NSMutableDictionary()
        
        paraDict.setValue(faceBookId, forKey:"fb_id")
        paraDict.setValue(userName, forKey:"fb_name")
        paraDict.setValue(userEmail, forKey:"fb_email")
        paraDict.setValue(firstName, forKey:"fb_first_name")
        paraDict.setValue(lastName, forKey:"fb_last_name")

        AppTheme().callPostService(String(format: "%@FbLogin/fb_login","https://gupanetwork.com/backend/"), param: paraDict) {(result, data) -> Void in                                                                                  ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                
                var departments : NSDictionary = NSDictionary()
                departments = data as! NSDictionary
                
                if let str_active = data.value(forKey: "is_active") as? NSInteger
                {
                    if str_active == 1 {
                        
                        print("Department \(departments)")
                        
                        AppTheme.setLoginDetails(departments)
                        if !(data.value(forKey: "mongo_obj") is NSNull) {
                            ApplicationPreference.saveMongoUserId(userId: (data.value(forKey: "mongo_obj") as? String)!)
                        }

                        _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: departments.value(forKey: "message") as! String, showInView: self.view)
                        
                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                    
                } else {
                    var destVC : VerificationControllerl = VerificationControllerl()
                    destVC = self.storyboard?.instantiateViewController(withIdentifier: "VerificationControllerl") as! VerificationControllerl
                    self.navigationController?.pushViewController(destVC, animated: true)
                }
            } else
            {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
    
    @IBAction func forgotButtonAction(_ sender: AnyObject) {
        alertView.isHidden = false
    }
    
    @IBAction func newSignUpAction(_ sender: AnyObject) {
        let newRegiVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterationVC") as! RegisterationVC
        self.navigationController?.pushViewController(newRegiVC, animated: true)
    }
    
    @IBAction func sendActionOnAlert(_ sender: AnyObject) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if emailTextView.text == "" || !AppTheme().isValidEmail(emailTextView.text as String!)
        {
            if  !AppTheme().isValidEmail(emailTextView.text as String!) == true {
                
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter right email in format.", showInView: self.view)
            } else
            {
               _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Email.", showInView: self.view)
            }
        }
        else
        {
            let parameters : [String: AnyObject] = ["email" : emailTextView.text as AnyObject]
            AppTheme().callGetService(String(format: "%@forget_password", AppUrl), param: parameters) { (result, data) -> Void in
                print(data)
                if(result == "success")
                {
                    self.loading.dismiss()
                    var departments : NSDictionary = NSDictionary()
                    departments = data as! NSDictionary
                    self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: departments .value(forKey: "message") as! String, showInView: self.view)
                    
                } else
                {
                    self.loading.dismiss()
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                }
                self.alertView.isHidden = true
            }
        }
    }
    
    @IBAction func cancelActionOnAlert(_ sender: AnyObject) {
        alertView.isHidden = true
    }
    
    func endUpEveryThing()
    {
        self.performSegue(withIdentifier: "log_home_seg", sender: self)
    }
}

extension LoginVC {
    
    @IBAction func facebookLoginMethod(_ sender: Any) {
        
        var strID = ""
        var strName = ""
        var strEmail = ""
        
        let loginManager = LoginManager()
        
        loginManager.logIn(readPermissions: [.publicProfile], viewController: self) { (loginResult) in
            
        FBSDKGraphRequest(graphPath:"me", parameters: ["fields":"id,email,name,picture.width(480).height(480)"]).start(completionHandler: { (connection, result, error) in
                    if error == nil {
                        
                        if let jsonResult = result as? Dictionary<String, AnyObject> {

                            print("Json Result \(jsonResult)")
                            
                            if jsonResult["id"] != nil{
                                strID = jsonResult["id"] as! String
                                print(strID)
                            }
                            
                            if jsonResult["name"] != nil{
                                strName = jsonResult["name"] as! String
                                print(strName)
                            }
                            
                            if jsonResult["email"] != nil{
                                strEmail = jsonResult["email"] as! String
                                print(strEmail)
                            }
                        }
                        self.faceBookLogin_APICall(faceBookId: strID ,userName: strName , userEmail:  strEmail)
                        
                    } else {
                        print("Error Getting Info \(String(describing: error))");
                    }
                })
            }
        }
}
