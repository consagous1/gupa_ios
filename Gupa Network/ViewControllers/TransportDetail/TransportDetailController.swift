//
//  TransportDetailController.swift
//  Gupa Network
//
//  Created by MWM23 on 12/8/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher

class TransportDetailController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UITextFieldDelegate {

    
    @IBOutlet weak var img_checkin_checkout: UIImageView!
    @IBOutlet weak var view_trans_detail: UIView!
    @IBOutlet weak var btn_Graph: UIButton!
    @IBOutlet weak var img_trans: UIImageView!
    @IBOutlet weak var lbl_tranp_name: UILabel!
    @IBOutlet weak var img_trans_type: UILabel!
    
    @IBOutlet weak var lbl_depart_City: UILabel!
    @IBOutlet weak var lbl_depart_time: UILabel!
    
    @IBOutlet weak var lbl_arrival_city: UILabel!
    @IBOutlet weak var lbl_arrival_time: UILabel!
    @IBOutlet weak var lbl_trans_detail: UILabel!    
    @IBOutlet weak var btn_more_trans_detail: UIButton!

    
    @IBOutlet weak var textview_call: UITextView!
    
    @IBOutlet weak var view_review_user_container: UIView!
    @IBOutlet weak var btn_user_list: UIButton!
    @IBOutlet weak var view_user_bottom: UIView!
    @IBOutlet weak var btn_review: UIButton!
    @IBOutlet weak var view_review_bottom: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgReview: UIImageView!
    
    @IBOutlet weak var view_table_list: UIView!
    @IBOutlet weak var tblReiviewUserList: UITableView!
    @IBOutlet weak var lblRecordNotFound: UILabel!
    @IBOutlet weak var imgRecordNotFound: UIImageView!
    @IBOutlet weak var btnCheckInCheckOut: UIButton!
  
    //Review Pop Up Elements=============================
    @IBOutlet weak var view_container_for_review_popup: UIView!
    @IBOutlet weak var view_container_for_checkInCheckOut: UIView!
    @IBOutlet weak var viewForDatePicker: UIView!
    @IBOutlet weak var timePicker:UIDatePicker!
    @IBOutlet weak var txtNameOnTicket: UITextField!
    @IBOutlet weak var txtBusNumber: UITextField!
    @IBOutlet weak var txtBusName: UITextField!
    @IBOutlet weak var txtTicketNumber: UITextField!
    @IBOutlet weak var txt_arrival_time: UITextField!
    @IBOutlet weak var txt_departure_time: UITextField!
    @IBOutlet weak var btnCheckForConnectToBusCompany: UIButton!
    @IBOutlet weak var btnCancelCheckInCheckOut: UIButton!
    @IBOutlet weak var btnSubmitCheckInCheckOut: UIButton!
    
    var connectedLike: NSInteger = 0
    var buttonSelected : UserDefaults!
    var timeString : String = String()
    //==================================================== View for Send Review
    @IBOutlet weak var view_container_write_review: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var txtCommentReview : UITextView!
    @IBOutlet weak var btnGreen: UIButton!
    @IBOutlet weak var btnRed: UIButton!
    @IBOutlet weak var btnGrey: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var rating: NSInteger = 0
    var isGoodRating : Bool = false
    var isBadRating : Bool = false
    var isUglyRating : Bool = false
    var str_trans_detail : String! // = <#value#>
    
    
    //=====================================================
    var str_transport_name : String!//= <#value#>
    var str_bus_num : String! // = <#value#>
    
    //=====================================================
    
    var loading : JGProgressHUD! = JGProgressHUD()
    
    var str_bus_id : String!
    var btnWriteComment:UIButton = UIButton()
    var btnWriteReview:UIButton = UIButton()
    var btnCheck: UIButton = UIButton()
    
    var status: NSInteger!
    var dic : NSDictionary! = NSDictionary()
   
    var arr_transportDetail : NSMutableArray! = NSMutableArray()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var respDict : NSDictionary = NSDictionary()
    
    //MARK: Viewcontrollsers life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
        self.title = "Ratings & Review"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(TransportDetailController.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
      
        //Set initial tag for user list first time
        buttonSelected = UserDefaults.standard
        buttonSelected.set(2, forKey: "buttonTag")
        buttonSelected.synchronize()
        
        if str_bus_id != nil || str_bus_id != "" {
            getTransportDetail(str_bus_id)
        }
        self.updateUI()
    }
    @objc func leftBarBackButton(){
        self.navigationController?.popViewController(animated: true)
    }

    func showRightNavigationBarButtonHide()                         //RightNavigationBarButton hide unhide
    {
        let imgWriteReview:UIImage = UIImage(named: "ic_review_white.png")!
        
        self.btnWriteReview = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnWriteReview.setImage(imgWriteReview, for: UIControlState())
        btnWriteReview.addTarget(self, action: #selector(TransportDetailController.writeReview), for: UIControlEvents.touchUpInside)
        
        let item2:UIBarButtonItem = UIBarButtonItem(customView: btnWriteReview)
        
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        navigationItem.rightBarButtonItems = [negativeSpace, item2]
    }
    
    @objc func writeReview()  {
        view_container_for_review_popup.isHidden = false
        view_container_for_checkInCheckOut.isHidden = true
        view_container_write_review.isHidden = false
        viewForDatePicker.isHidden = true
        self.txtCommentReview.layer.cornerRadius = 5
        self.txtCommentReview.layer.borderColor = UIColor.lightGray.cgColor
        self.txtCommentReview.layer.borderWidth = 1
        self.view.layer.borderWidth = 1
        self.view_container_write_review.layer.borderColor = UIColor.gray.cgColor
        self.view_container_write_review.layer.cornerRadius = 5
        self.view_container_write_review.layer.masksToBounds = true
        self.lbl_Title.text = "Please submit your review."
        self.txtCommentReview.text = "Please write your review..."
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
    }

    func updateUI() {
        //Popup view design for review============
        self.view_container_for_review_popup.isHidden = true
        self.view_container_for_checkInCheckOut.layer.cornerRadius = 5
        self.view_container_for_checkInCheckOut.layer.masksToBounds = true
        
        self.btnCancelCheckInCheckOut.layer.cornerRadius = 2
        self.btnCancelCheckInCheckOut.layer.masksToBounds = true
        self.btnCancelCheckInCheckOut.layer.borderColor = UIColor.lightGray.cgColor
        self.btnCancelCheckInCheckOut.layer.borderWidth = 1
        
        self.btnSubmitCheckInCheckOut.layer.cornerRadius = 2
        self.btnSubmitCheckInCheckOut.layer.masksToBounds = true
        self.btnSubmitCheckInCheckOut.layer.borderColor = UIColor.lightGray.cgColor
        self.btnSubmitCheckInCheckOut.layer.borderWidth = 1
        //=========================================
        
        self.tblReiviewUserList.estimatedRowHeight = 100
        self.tblReiviewUserList.rowHeight = UITableViewAutomaticDimension
        self.tblReiviewUserList.setNeedsLayout()
        self.tblReiviewUserList.layoutIfNeeded()
        
        //Detail view design===================
        self.view_container_for_review_popup.isHidden = true
        view_user_bottom.isHidden = true
        view_review_bottom.isHidden = false
        self.view_trans_detail.layer.cornerRadius = 5
        self.view_trans_detail.layer.masksToBounds = true
        self.img_trans.layer.cornerRadius = 15
        self.img_trans.layer.masksToBounds = true
        self.view_trans_detail.layer.borderColor = UIColor.lightGray.cgColor
        self.view_trans_detail.layer.borderWidth = 1
        
        self.view_review_user_container.layer.cornerRadius = 5
        self.view_review_user_container.layer.masksToBounds = true
        self.view_review_user_container.layer.borderColor = UIColor.lightGray.cgColor
        self.view_review_user_container.layer.borderWidth = 1
        //=========================================
        
        btnCheckInCheckOut.layer.cornerRadius = 25
        btnCheckInCheckOut.layer.masksToBounds = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btn_graph_action(_ sender: AnyObject) {
        if status == 0{
            self.getTransportSingleRating()
//            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry you are not checked in.", showInView: self.view)
        }else{
            self.getTransportSingleRating()
        }
    }
    
    func getTransportDetail(_ str_busID : String){
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if let str_user_id = AppTheme.getLoginDetails().value(forKey: "id") {
          let parameters : [String: AnyObject] = ["bus_id" : str_busID as AnyObject,
                                                "user_id" : str_user_id as AnyObject]
        AppTheme().callGetService(String(format: "%@transports_details", AppUrl), param: parameters) { (result, data) -> Void in
            
            if result == "success"{
                self.loading.dismiss()
                
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arr_transportDetail = array_response.mutableCopy() as! NSMutableArray
                }else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                    self.arr_transportDetail = array_Response_mutable
                }
                if self.arr_transportDetail.count != 0{
                    self.updateDetail(self.arr_transportDetail)
                }
            }
        }
      }
    }
    
    
    func updateDetail(_ arr_detail : NSMutableArray) {
//        var dic : NSDictionary! = NSDictionary()
        for element in arr_transportDetail {
            dic = element as! NSDictionary
        }
        
        if let strStatus: String = dic["checked_in"] as? String{
            status = NSInteger(strStatus)!
        }else if let strStatus: NSInteger = dic["checked_in"] as? NSInteger{
            status = strStatus
        }
        if let str_title = dic .value(forKey: "transport_name") as? String {
            //self.title = str_title
            str_transport_name = str_title
        }
        if status == 1 {
            showRightNavigationBarButtonHide()
            self.view_user_bottom.isHidden = false
            self.view_review_bottom.isHidden = true
            self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
            self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
            self.imgRecordNotFound.isHidden = true
            self.lblRecordNotFound.isHidden = true
            self.img_checkin_checkout.image = UIImage(named: "img_check_white.png")
           
            self.getUserList()
        }else{
            self.view_user_bottom.isHidden = true
            self.view_review_bottom.isHidden = true
            self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
            self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
            self.view_review_bottom.isHidden = true
            self.view_user_bottom.isHidden = true
            self.imgRecordNotFound.isHidden = false
            self.lblRecordNotFound.isHidden = false
            self.img_checkin_checkout.image = UIImage(named: "img_box_white.png")
            self.tblReiviewUserList.isHidden = true
        }
        lbl_arrival_time.isHidden = true
        lbl_depart_time.isHidden = true
        if let str_transport_pic : String = dic["image"] as? String {
            let url = URL(string:str_transport_pic)
            
            
             img_trans.kf.setImage(with: url, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: nil)
            
//            img_trans.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
        }
        if let str_trans_name = dic .value(forKey: "transport_name") as? String {
            lbl_tranp_name.text = str_trans_name
            str_transport_name = str_trans_name
        }
        if let str_trans_type = dic .value(forKey: "address") as? String {
            img_trans_type.text = str_trans_type
        }
        if let str_trans_depart_city = dic .value(forKey: "source_station") as? String {
            lbl_depart_City.text = str_trans_depart_city
        }
        if let str_trans_depart_time = dic .value(forKey: "departure_time") as? String {
            lbl_depart_time.text = str_trans_depart_time
            
        }
        if let str_trans_arrival_city = dic .value(forKey: "destination_station") as? String {
            lbl_arrival_city.text = str_trans_arrival_city
        }
        if let str_trans_arrival_time = dic .value(forKey: "arrival_time") as? String {
            lbl_arrival_time.text = str_trans_arrival_time
            
        }
        if let str_trans_detail = dic .value(forKey: "description") as? String {
            self.str_trans_detail = str_trans_detail
            lbl_trans_detail.text = self.str_trans_detail
        }
        if let str_trans_call = dic .value(forKey: "phone_no") as? String {
            textview_call.text = str_trans_call
        }
        if let str_bus_number = dic .value(forKey: "bus_number") as? String {
            str_bus_num = str_bus_number
        }
    }
    
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
        if btnCheck.tag == 3 {
            if self.arrOfResponseKey.count != 0 {
                row =  self.arrOfResponseKey.count
                return row
            }else {
                return 0
            }
        }else {
            if self.arrOfResponseKey.count != 0{
                row =  self.arrOfResponseKey.count
                return row
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
       
         if btnCheck.tag == 2 {
            
            var cell : UserChatCell! = tableView.dequeueReusableCell(withIdentifier: "UserChatCell", for: indexPath) as! UserChatCell
            if cell == nil {
                cell = UserChatCell(style: UITableViewCellStyle.default, reuseIdentifier: "UserChatCell")
            }
            if let str_user_name : String = dic["username"] as? String{
                cell.lblUserComments.text  = str_user_name
            }
            if let str_user_profile : String = dic["profile_pic"] as? String{
                let url = URL(string:str_user_profile)
                
                 cell.imgUser.kf.setImage(with: url, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: nil)
                
//                cell.imgUser.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
            }
            return cell
            
        }else if btnCheck.tag == 3 {
            
            var cell : ReviewCell! = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
            if cell == nil {
                cell = ReviewCell(style: UITableViewCellStyle.default, reuseIdentifier: "ReviewCell")
            }
            if let str_hotel_review : String = dic["review"] as? String{
                cell.lblUserReview.text = str_hotel_review
            }
            var rating : NSInteger = 0
            if let strRating: String = dic["rating"] as? String
            {
                if strRating == "" {
                    rating = 0
                } else{
                    rating = NSInteger(strRating)!
                }
            } else if let strRating: NSInteger = dic["rating"] as? NSInteger {
                rating = strRating
            }
            if rating == 1{
                cell.imgReview.image = UIImage(named: "good_on.png")
            }else if rating == 2 {
                cell.imgReview.image = UIImage(named: "bad_on.png")
            }else if rating == 3{
                cell.imgReview.image = UIImage(named: "red_on.png")
            } else if rating == 0 {
                cell.imgReview.image = UIImage(named: "")
            }
            if let str_user_profile : String = dic["profile_pic"] as? String{
                let url = URL(string:str_user_profile)
                
                cell.imgUser.kf.setImage(with: url, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: nil)
                
//                cell.imgUser.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
            }
            if let str_user_name : String = dic["date_time"] as? String{
                cell.lblDate.text  = str_user_name
            }
            return cell
            
        }else {
            var cell : ReviewCell! = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
            if cell == nil {
                cell = ReviewCell(style: UITableViewCellStyle.default, reuseIdentifier: "ReviewCell")
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if btnCheck.tag == 2 {
            var destVC = UserChatOnCommentController()
            destVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
            destVC.dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
            self.navigationController?.pushViewController(destVC, animated: true)
        }        
    }
  func chatAction(_ sender: AnyObject){
        
    }
    
    
    func getUserList() {
        self.tblReiviewUserList.isHidden = false
        btnCheck.tag = 2
        self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
        self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
        //str_bus_id
        if let str_user_id = AppTheme.getLoginDetails().value(forKey: "id") {
            let parameters : [String: AnyObject] = ["bus_id" : str_bus_id as AnyObject,
                                                    "user_id" : str_user_id as AnyObject]
            AppTheme().callGetService(String(format: "%@transports_bus_user_online", AppUrl), param: parameters) {(result, data) -> Void in                 ///Call services
                
                if result == "success"{
                    self.imgRecordNotFound.isHidden = true
                    self.lblRecordNotFound.isHidden = true
                    self.respDict = data as! NSDictionary
                    print(self.respDict)
                    if let arrRes: NSMutableArray = self.respDict["response"] as? NSMutableArray {
                        self.arrOfResponseKey = arrRes
                    }else if let arrRes: NSArray = self.respDict["response"] as? NSArray {
                        self.arrOfResponseKey = NSMutableArray(array: arrRes)
                    }
                    self.tblReiviewUserList.reloadData()
                }else {
                    self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
                    self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
                    self.view_user_bottom.isHidden = false
                    self.imgRecordNotFound.isHidden = false
                    self.lblRecordNotFound.isHidden = false
                    self.tblReiviewUserList.isHidden = true
                }
            }
        }
    }
    
    func getReviewList() {
        
        self.startProgressBar()
        
        if (!isNetworkAvailable) {
            self.loading.dismiss()
            Utility.showNetWorkAlert()
            return
        }
        
        btnCheck.tag = 3
        self.view_user_bottom.isHidden = true
        self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
        self.imgReview.image = UIImage(named : "ic_review_on.png")! as UIImage
        //str_bus_id
        let parameters : [String: AnyObject] = ["bus_id" : str_bus_id  as AnyObject ]
        AppTheme().callGetService(String(format: "%@get_reviews_transport_bus", AppUrl), param: parameters) {(result, data) -> Void in                                                                                    ///Call services
            if result == "success"{
                self.loading.dismiss()
                self.imgRecordNotFound.isHidden = true
                self.lblRecordNotFound.isHidden = true
                self.respDict = data as! NSDictionary
                print(self.respDict)
                if let arrRes: NSMutableArray = self.respDict["response"] as? NSMutableArray  {
                    self.arrOfResponseKey = arrRes
                } else if let arrRes: NSArray = self.respDict["response"] as? NSArray{
                    self.arrOfResponseKey = NSMutableArray(array: arrRes)
                }
                self.tblReiviewUserList.isHidden = false
                self.tblReiviewUserList.reloadData()
            }else {
                self.loading.dismiss()
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view )
                self.imgRecordNotFound.isHidden = false
                self.lblRecordNotFound.isHidden = false
                self.tblReiviewUserList.isHidden = true
            }
        }
    }
    
    func checkInCheckOut() {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if status == 0 {
                connectedLike = 1
                self.showRightNavigationBarButtonHide()
                self.imgRecordNotFound.isHidden = true
                self.lblRecordNotFound.isHidden = true
                status = 1
                if let str_userID = AppTheme.getLoginDetails().object(forKey: "id") {
//                    let parameters : [String: AnyObject] = ["user_id" : str_userID as AnyObject,
//                        "bus_id" : str_bus_id as AnyObject,
//                        "check_status": status as AnyObject,
//                    "ticket_name": self.txtNameOnTicket.text  as AnyObject,
//                    "ticket_number": self.txtTicketNumber.text as AnyObject,
//                    "bus_company_name": "" as AnyObject,
//                    "bus_number": "" as AnyObject,
//                    "connected_like": connectedLike as AnyObject,
//                    "bus_arrival_datetime": txt_arrival_time.text as AnyObject,
//                    "bus_departure_datetime": txt_departure_time.text as AnyObject]
                    
                    
                    let paraDict = NSMutableDictionary()
                    paraDict.setValue(str_userID, forKey:"user_id")
                    paraDict.setValue(str_bus_id, forKey:"bus_id")
                    paraDict.setValue(status, forKey:"check_status")
                    paraDict.setValue(self.txtNameOnTicket.text, forKey:"ticket_name")
                    paraDict.setValue(self.txtTicketNumber.text, forKey:"ticket_number")
                    paraDict.setValue("", forKey:"bus_company_name")
                    paraDict.setValue("", forKey:"bus_number")
                    paraDict.setValue(connectedLike, forKey:"connected_like")
                    paraDict.setValue(txt_arrival_time.text, forKey:"bus_arrival_datetime")
                    paraDict.setValue(txt_departure_time.text, forKey:"bus_departure_datetime")
                    
                    AppTheme().callPostService(String(format: "%@transport_bus_checkinout?", AppUrl), param: paraDict)
                    {(result, data) -> Void in                             ///Call services
                        
                        if result == "success"
                        {
                            let dict : NSDictionary = data as! NSDictionary
                            let str_msg  = dict .value(forKey: "message") as? String
                            self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: str_msg!, showInView: self.view)
                            self.btnWriteComment.isHidden = false
                            self.btnWriteReview.isHidden = false
                            if let key = UserDefaults.standard.value(forKey: "buttonTag") as? NSInteger   {
                            if  key == 2{
                                    self.view_user_bottom.isHidden = false
                                    self.getUserList()
                                }
                            else if  key == 3 {
                                    print("Method for review")
                                    self.view_review_bottom.isHidden = false
                                    self.getReviewList()
                                }
                                self.tblReiviewUserList.isHidden = false
                            }
                        }else {
                            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                            self.tblReiviewUserList.isHidden = true
                        }
                    }
                    self.tblReiviewUserList.isHidden = false
                }
            }else{
            if AppTheme.getLoginDetails().object(forKey: "id") != nil {
                status = 0
//                let parameters : [String: AnyObject] = ["bus_id" :str_bus_id as AnyObject,
//                                                        "user_id" :AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject,
//                                                        "check_status": status as AnyObject]
                
                let paraDict = NSMutableDictionary()
                paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")!, forKey:"user_id")
                paraDict.setValue(str_bus_id, forKey:"bus_id")
                paraDict.setValue(status, forKey:"check_status")
                
                AppTheme().callPostService(String(format: "%@transport_bus_checkinout?", AppUrl), param: paraDict)
                {(result, data) -> Void in                               ///Call services
                    
                    if result == "success" {
                        let dict : NSDictionary = data as! NSDictionary
                        let str_msg  = dict .value(forKey: "message") as? String
                        self.view_review_bottom.isHidden = true
                        self.view_user_bottom.isHidden = true
                        self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
                        self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
                        
                        self.btnWriteComment.isHidden = true
                        self.btnWriteReview.isHidden = true
                        self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: str_msg! , showInView: self.view)
                        self.imgRecordNotFound.isHidden = false
                        self.lblRecordNotFound.isHidden = false
                    }else {
                        self.btnWriteComment.isHidden = true
                        self.btnWriteReview.isHidden = true
                        self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                        self.imgRecordNotFound.isHidden = false
                        self.lblRecordNotFound.isHidden = false
                    }
                }
                self.tblReiviewUserList.isHidden = true
            }
        }
    }
    
    //MARK: - UITextfield delegate methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == txt_arrival_time) || (textField == txt_departure_time ) {
            txt_departure_time.resignFirstResponder()
            txt_arrival_time.resignFirstResponder()
            if textField == txt_arrival_time {
                txt_departure_time.resignFirstResponder()
                txt_arrival_time.resignFirstResponder()
                txt_arrival_time.tag = 5
                txt_departure_time.tag = 101
            }else{
                txt_departure_time.resignFirstResponder()
                txt_arrival_time.resignFirstResponder()
                txt_departure_time.tag = 6
                txt_arrival_time.tag = 100
            }
            self.viewForDatePicker.isHidden = false
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            var timeString: String! = String()
            timeString = dateFormatter.string(from: timePicker.date)
            timePicker.datePickerMode = UIDatePickerMode.dateAndTime
             timePicker.addTarget(self, action: #selector(TransportDetailController.dueDateChanged(_:)), for: UIControlEvents.valueChanged)
        }
    }
    @objc func dueDateChanged(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
       
        if txt_arrival_time.tag == 5 {
           txt_arrival_time.text = dateFormatter.string(from: sender.date)
           
        }else if txt_departure_time.tag == 6{
            txt_departure_time.text = dateFormatter.string(from: sender.date)
        }
        self.viewForDatePicker.isHidden = true
    }
    
    // MARK: -  UItextView Delegate
    func textViewDidChange(_ textView: UITextView) {
        if textView == txtCommentReview{
            if txtCommentReview.text == "Please write your review..." {
                txtCommentReview.text = ""
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtCommentReview  {
            if txtCommentReview.text == "Please write your review..." {
                txtCommentReview.text = ""
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtCommentReview{
            if txtCommentReview.text == "" {
                txtCommentReview.text = "Please write your review..."
            }
        }
    }
    
    //MARK:- User List And Review List Action
    @IBAction func btnCheckInCheckOut(_ sender: AnyObject) {
        
        if status == 0 {
            self.view_container_for_review_popup.isHidden = false
            self.view_container_for_checkInCheckOut.isHidden = false
            self.viewForDatePicker.isHidden = true
            self.view_container_write_review.isHidden = true
            if str_transport_name != "" {
               self.txtBusName.text = str_transport_name
            }
            if str_bus_num != "" {
            }
        }else{
            self.view_container_for_review_popup.isHidden = true
            self.view_container_for_checkInCheckOut.isHidden = true
            self.view_container_write_review.isHidden = true
           self.img_checkin_checkout.image = UIImage(named: "img_box_white.png")
            checkInCheckOut()
        }
    }
    
    
    @IBAction func btnReviewAction(_ sender: AnyObject) {
        if status == 1 {
            self.view_review_bottom.isHidden = false
            self.view_user_bottom.isHidden = true
            buttonSelected = UserDefaults.standard
            buttonSelected.set(sender.tag, forKey: "buttonTag")
            buttonSelected.synchronize()
            getReviewList()
        }else{
        }
    }
    
    @IBAction func btnUserListAction(_ sender: AnyObject) {
        if status == 1 {
            self.view_review_bottom.isHidden = true
            self.view_user_bottom.isHidden = false
            buttonSelected = UserDefaults.standard
            buttonSelected.set(sender.tag, forKey: "buttonTag")
            buttonSelected.synchronize()
            getUserList()
        }else{
        }
    }
    
    
    //MARK: Actions for Check-in check out pop up
    @IBAction func btn_sendOnReviewAlert(_ sender: AnyObject) {
        if txtNameOnTicket.text == "" || txtTicketNumber.text == "" || txt_departure_time.text == "" || txt_arrival_time.text == "" || self.txtBusName.text == ""{
            if txtNameOnTicket.text == "" {
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Name on Ticket.", showInView: self.view)
            }else if txtBusName.text == "" {
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Bus Number.", showInView: self.view)
            } else if txtTicketNumber.text == ""{
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Ticker Number.", showInView: self.view)
            }else if txt_arrival_time.text == ""{
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter arrival time.", showInView: self.view)
            }else if txt_departure_time.text == ""{
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter departure time.", showInView: self.view)
            }
        }else {
            self.txtNameOnTicket.text = ""
            self.txtTicketNumber.text = ""
            txtBusName.text = ""
            
            self.view_container_for_review_popup.isHidden = true
            self.view_container_for_checkInCheckOut.isHidden = true
            self.view_container_write_review.isHidden = true
            
            if status == 0 {
                self.img_checkin_checkout.image = UIImage(named: "img_check_white.png")
                checkInCheckOut()
            }else{
                self.img_checkin_checkout.image = UIImage(named: "img_box_white.png")
                checkInCheckOut()
            }
        }
    }
    
    @IBAction func btn_CancelOnAlert(_ sender: AnyObject) {
        self.view_container_for_review_popup.isHidden = true
    }
    
    @IBAction func btn_checkForConnection(_ sender: AnyObject) {
        if connectedLike == 0 {
            connectedLike = 1
            btnCheckForConnectToBusCompany.setImage(UIImage(named: "img_check.png"), for: UIControlState())
        }else{
            connectedLike = 0
            btnCheckForConnectToBusCompany.setImage(UIImage(named: "img_uncheck.png"), for: UIControlState())
        }
    }
    
    func startProgressBar(){
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    
    //MARK: Write review action, rating selection,  methods
    @IBAction func btnCancelActionWriteReview(_ sender: AnyObject) {
        self.view_container_for_review_popup.isHidden = true
    }
    @IBAction func btnSubmitWriteReview(_ sender: AnyObject) {
        sendReview()
    }
    @IBAction func btnRatingGood(_ sender: AnyObject) {
        if isGoodRating == false {
            self.rating = 1
            isGoodRating = true
            self.btnGreen.setImage(UIImage(named: "good_on.png")! as UIImage, for: UIControlState())
        }else{
            self.rating = 0
            isGoodRating = false
            self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
        }
        self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())
        self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
    }
    
    @IBAction func btnRatingBad(_ sender: AnyObject) {
        if isBadRating == false {
            self.rating = 2
            isBadRating = true
            self.btnRed.setImage(UIImage(named: "bad_on.png")! as UIImage, for: UIControlState())
        }else{
            self.rating = 0
            isBadRating = false
            self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())
        }
        self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
        self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
    }
    
    @IBAction func btnRatingUgly(_ sender: AnyObject) {
        if isUglyRating == false {
            self.rating = 3
            isUglyRating = true
            self.btnGrey.setImage(UIImage(named: "bad.png")! as UIImage, for: UIControlState())
        }else{
            isUglyRating = false
            self.rating = 0
            self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
        }
        self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
        self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())        
    }

    
    func sendReview() {
        
        self.startProgressBar()
        
        if (!isNetworkAvailable) {
            self.loading.dismiss()
            Utility.showNetWorkAlert()
            return
        }
        
        if self.txtCommentReview.text != "Please write your review..." && self.txtCommentReview.text != "" || self.rating != 0  {
            if self.txtCommentReview.text == "Please write your review..." {
                txtCommentReview.text = ""
            }
                if let str_user_id =  AppTheme.getLoginDetails().object(forKey: "id"){
                    if let str_trans_id = self.dic .value(forKey: "transport_id") as? String {
                        
//                        let parameters : [String: AnyObject] = ["user_id" : str_user_id as AnyObject,
//                                                                "bus_id" :  str_bus_id as AnyObject,
//                                                                "review" :  self.txtCommentReview.text as AnyObject,
//                                                                "rating" : rating as AnyObject,
//                                                                "transport_id" : str_trans_id as AnyObject]
                        
                        
                        let paraDict = NSMutableDictionary()
                        paraDict.setValue(str_user_id, forKey:"user_id")
                        paraDict.setValue(str_bus_id, forKey:"bus_id")
                        paraDict.setValue(self.txtCommentReview.text, forKey:"review")
                        paraDict.setValue(rating, forKey:"rating")
                        paraDict.setValue(str_trans_id, forKey:"transport_id")
                        
                        AppTheme().callPostService(String(format: "%@transport_bus_review", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
                            
                            if result == "success" {
                                self.loading.dismiss()
                                self.view_review_bottom.isHidden = false
                                self.view_user_bottom.isHidden = true
                                self.respDict = data as! NSDictionary
                                self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Post Sent", showInView: self.view)
                                self.getReviewList()
                                self.view_container_for_review_popup.isHidden = true
                                self.view_container_write_review.isHidden = true
                            }else{
                                self.loading.dismiss()
                                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                                self.view_container_for_review_popup.isHidden = true
                                self.view_container_write_review.isHidden = true
                            }
                        }
                    }
                }
            }else{
                self.loading.dismiss()
                if self.txtCommentReview.text == nil || self.txtCommentReview.text == "" || self.txtCommentReview.text == "Please write your review..."{
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter review or rating.", showInView: self.view)
                }
            }
        }
    @IBAction func btn_expand_company_detail(_ sender: AnyObject) {
        if  self.str_trans_detail != ""  {
            let destVC : TransportDescriptionDetailController!
            destVC =  self.storyboard?.instantiateViewController(withIdentifier: "TransportDescriptionDetailController") as! TransportDescriptionDetailController
            destVC.str_transport_detail = self.str_trans_detail
            self.navigationController?.pushViewController(destVC, animated: true)
        }
    }
    
    func getTransportSingleRating() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if let str_flight_id = self.dic["bus_id"] as? String {
            let parameters : [String: AnyObject] = ["bus_id" : str_flight_id  as AnyObject]
            AppTheme().callGetService(String(format: "%@BusTransportRating", AppUrl), param: parameters) {(result, data) -> Void in                 ///Call services
                
                if result == "success"   {
                    self.loading.dismiss()
                    self.respDict = data as! NSDictionary
                    var arr_bus_rating : NSMutableArray! = NSMutableArray()
                    var dic_temp : NSDictionary! = NSDictionary()
                    if let arr_Res: NSMutableArray = self.respDict["response"] as? NSMutableArray {
                        arr_bus_rating = arr_Res
                    }else if let arr_Res: NSArray = self.respDict["response"] as? NSArray {
                        arr_bus_rating = arr_Res.mutableCopy() as! NSMutableArray
                    }
                    for element in arr_bus_rating{
                        dic_temp = element as! NSDictionary
                    }
                    let barRatingVC = self.storyboard?.instantiateViewController(withIdentifier: "FlightTransportGraphController") as! FlightTransportGraphController
                    barRatingVC.strCheck = "comeFromTransportRating"  //let str_hotel_id : String =
                   if  self.str_bus_id != nil {
                    barRatingVC.strBusId = self.str_bus_id as! NSString
                    }
                   barRatingVC.flightHotelDetailDict = dic_temp
                   self.navigationController?.pushViewController(barRatingVC, animated: true)
                } else  {
                    self.loading.dismiss()
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                }
            }
        }
    }
}
