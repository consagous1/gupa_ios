//
//  HotailDetailVC.swift
//  Gupa Network
//
//  Created by mac on 6/18/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher

class HotailDetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {

    var loading : JGProgressHUD! = JGProgressHUD()
    @IBOutlet weak var lblRecordNotFound: UILabel!
    @IBOutlet weak var imgRecordNotFound: UIImageView!
    @IBOutlet weak var viewBGForAlert: UIView!
    @IBOutlet weak var viewForAlert: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtCommentReview: UITextView!
    @IBOutlet weak var btnGreen: UIButton!
    @IBOutlet weak var btnRed: UIButton!
    @IBOutlet weak var btnGrey: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnHotelDetail: UIButton!
    
    @IBOutlet weak var viewForButtons: UIView!
    
    @IBOutlet weak var viewForHotelDetail: UIView!
    @IBOutlet weak var imgHotel: UIImageView!
    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnChat: UIButton!
    
    @IBOutlet weak var viewForCheckInCheckOut: UIView!
    
    @IBOutlet weak var viewPopUpForCheckInCheckOut: UIView!
    @IBOutlet weak var txtNameAtBooking: UITextField!
    @IBOutlet weak var txtConfirmationNum: UITextField!
   
    @IBOutlet weak var btnCheckForConnectHotel: UIButton!
    @IBOutlet weak var btnCancelCheckInCheckOut: UIButton!
    @IBOutlet weak var btnSubmitCheckInCheckOut: UIButton!
    
    @IBOutlet weak var lblDetailAddress: UILabel!
    @IBOutlet weak var lblStateAddress: UILabel!
    @IBOutlet weak var btnCheckInCheckOut: UIButton!
    @IBOutlet weak var lblCheckInCheckOutStatus: UILabel!
    
    @IBOutlet weak var imgComment: UIImageView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgReview: UIImageView!
    
    @IBOutlet weak var tblCommUsrReviewList: UITableView!
    
    @IBOutlet var arrForUserDetail: NSArray? = NSArray()
    @IBOutlet var arrForHotel: NSArray!
    @IBOutlet var arrForUser: NSArray!

    var str_hotel_id : String! = String()
    var str_hotel_city : String! = String()
    
    var dictForHotel: NSDictionary!
    var dictForUserDetail = NSDictionary()
    var respDict : NSDictionary = NSDictionary()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    
    
    var arrOfComments : NSMutableArray = NSMutableArray()
    var arrOfUserReview : NSMutableArray = NSMutableArray()
    var arrOfUserList : NSMutableArray = NSMutableArray()

    var str_other_id : String! = String()
    
    var btnCheck: UIButton = UIButton()
    var btnWriteComment:UIButton = UIButton()
    var btnWriteReview:UIButton = UIButton()
    
    var status: NSInteger!
    var connectedLike: NSInteger = 0
    var rating: NSInteger = 0
    var isCheck: NSInteger = 0
    var isGoodRating : Bool = false
    var isBadRating : Bool = false
    var isUglyRating : Bool = false
    
    var buttonSelected : UserDefaults!
    var isBackFromChat : Bool = false
    
    
  //===================================================
    //MARK: View Controllers Life cycle methods
  //===================================================
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.viewForAlert.isHidden = true
        self.viewBGForAlert.isHidden = true
        self.imgRecordNotFound.isHidden = true
        self.lblRecordNotFound.isHidden = true
        self.viewPopUpForCheckInCheckOut.isHidden = true
       
        appDelegate.centerNav = self.navigationController
        
        self.title = "Hotel"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 66.0/255.0, green: 199.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       
        txtCommentReview.delegate = self
        tblCommUsrReviewList.dataSource = self
        tblCommUsrReviewList.delegate = self
        
        let btnLeftBar: UIButton = UIButton ()                     // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(HotailDetailVC.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()        //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        self.viewForButtons.layer.borderColor = UIColor.gray.cgColor
        self.viewForButtons.layer.borderWidth = 1.0
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        txtCommentReview.text = ""
        txtNameAtBooking.text = ""
        txtConfirmationNum.text = ""
        
        
        if str_hotel_id != ""  {
         self.getSelectedHotelDetail(str_hotel_id, city_name: str_hotel_city)
        }
        // updateDetail()
        if isBackFromChat == true {
        if str_other_id != "" {
                if let str_myID  = AppTheme.getLoginDetails().value(forKey: "id") {
                    userOnChat(str_myID as! String, str_others_id: str_other_id)
                }
            }
        }
    }
    
    
//    func updateDetail() {
//
//        if arrForHotel.count != 0 {
//            for element in arrForHotel       //.....Handle details for Airline, Flight and User
//            {
//                self.dictForHotel  = element as! NSDictionary
//            }
//            print(self.dictForHotel)
//
//            let userDetailArray: NSMutableArray = NSMutableArray()
//
//            if (!arrForUserDetail!.isEqual(nil))
//            {
//                userDetailArray.addingObjects(from: arrForUserDetail! as [AnyObject])
//                for element in arrForUserDetail!
//                {
//                    self.dictForUserDetail  = element as! NSMutableDictionary
//                }
//            }
//
//            print(self.dictForUserDetail)
//
//            let str: String = (self.dictForHotel["hotel_Id"] as? String)!
//            AppTheme.setHotelId(NSInteger(str)! )
//
//            //Populate Airlines detail Flight detail and Call w_s for comment
//            if let str_hotel_name : String = self.dictForHotel["hotel_name"] as? String{
//                self.lblHotelName.text = str_hotel_name
//            }
//            if let str_hotel_city : String = self.dictForHotel["city"] as? String{
//                self.lblAddress.text = str_hotel_city
//            }
//            if let str_hotel_address : String = self.dictForHotel["address"] as? String{
//                self.lblDetailAddress.text = str_hotel_address
//            }
//
//            var str_state : String!
//            var str_country_code : String!
//
//
//            if let str_hotel_detail_address_state  : String = self.dictForHotel["state"] as? String{
//                str_state = str_hotel_detail_address_state
//            }
//            if let str_hotel_detail_add_countryCode : String = self.dictForHotel["countryCode"] as! String!{
//                str_country_code = str_hotel_detail_add_countryCode
//            }
//            self.lblStateAddress.text = "\(str_state!)  \(str_country_code!)"
//
////            if let str_hotel_image_string : String = self.dictForHotel["profile_pic"] as? String{
////                let url = URL(string:str_hotel_image_string)
////
////                self.imgHotel.kf.setImage(with: url!, placeholder: UIImage(named: "ic_hotel_orange.png"), options: nil, progressBlock: nil, completionHandler: {
////                    (image, error, cacheType, imageURL) in
////                    print(image?.description ?? "")
////                })
////            }
//
//
//            //Check user status for checking checkout
//            if let strStatus: String = dictForHotel["check_status"] as? String{
//                status = NSInteger(strStatus)!
//            }
//            else if let strStatus: NSInteger = dictForHotel["check_status"] as? NSInteger{
//                status = strStatus
//            }
//            if status == 1{
//                self.showRightNavigationBarButtonHide()
//                self.imgRecordNotFound.isHidden = true
//                self.lblRecordNotFound.isHidden = true
//                self.btnCheckInCheckOut.setImage(UIImage(named: "img_check.png")! as UIImage, for: UIControlState())
//                self.lblCheckInCheckOutStatus.text = "Check In"
//                self.getUserList()
//            }else{
//                self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
//                self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
//                self.imgRecordNotFound.isHidden = false
//                self.lblRecordNotFound.isHidden = false
//                self.btnCheckInCheckOut.setImage(UIImage(named: "img_uncheck.png")! as UIImage, for: UIControlState())
//                self.tblCommUsrReviewList.isHidden = true
//            }
//        }
//
//    }
//
    @objc func leftBarBackButton(){                              //Helper method for right navigationbar button
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    
    func showRightNavigationBarButtonHide()                   //RightNavigationBarButton hide unhide
    {
        let imgWriteReview:UIImage = UIImage(named: "ic_review_white.png")!
        
        // Create the second button
        self.btnWriteReview = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnWriteReview.setImage(imgWriteReview, for: UIControlState())
        btnWriteReview.addTarget(self, action: #selector(HotailDetailVC.writeReview), for: UIControlEvents.touchUpInside)
        let item2:UIBarButtonItem = UIBarButtonItem(customView: btnWriteReview)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [negativeSpace, item2]
    }

    
   //========================================================
    //MARK: UITableview Datasource and delegate methods
  //========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
        if btnCheck.tag == 3
        {
            if self.arrOfResponseKey.count != 0
            {
                row =  self.arrOfResponseKey.count
                return row
            }
            else
            {
                return 0
            }
        }else if btnCheck.tag == 2
        {
            if self.arrOfResponseKey.count != 0
            {
                row =  self.arrOfResponseKey.count
                return row
            }
            else
            {
                return 0
            }
        }
        else{
            if self.arrOfResponseKey.count != 0
            {
                row =  self.arrOfResponseKey.count
                return row
            }
            else
            {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
   
        self.tblCommUsrReviewList.estimatedRowHeight = 100
        self.tblCommUsrReviewList.rowHeight = UITableViewAutomaticDimension
        self.tblCommUsrReviewList.setNeedsLayout()
        self.tblCommUsrReviewList.layoutIfNeeded()
        self.tblCommUsrReviewList.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        //.....
        isBackFromChat = true
        if btnCheck.tag == 1{
            var cell : CommentCell! = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
            if cell == nil {
                cell = CommentCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommentCell")
            }
            if let str_comment : String = dic["comment"] as? String{
                cell.lblUserComments.text = str_comment
            }
            
            if let str_user_profile : String = dic["profile_pic"] as? String{
                let url = URL(string:str_user_profile)
                
                cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, imageURL) in
                    print(image?.description ?? "")
                })
                
            }
            view.layer.cornerRadius = 5
            return cell
            
        } else if btnCheck.tag == 2 {
            var cell : UserChatCell! = tableView.dequeueReusableCell(withIdentifier: "UserChatCell", for: indexPath) as! UserChatCell
            if cell == nil {
                cell = UserChatCell(style: UITableViewCellStyle.default, reuseIdentifier: "UserChatCell")
            }
            if let str_user_name : String = dic["username"] as? String{
                cell.lblUserComments.text  = str_user_name
            }
            if let str_user_profile : String = dic["profile_pic"] as? String{
                let url = URL(string:str_user_profile)
                
                cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, imageURL) in
                    print(image?.description ?? "")
                })
                
            }
            if  let str_otherId = dic["user_id"] as? String {
                self.str_other_id = str_otherId
            }
            cell.btnChat.addTarget(self, action: #selector(HotailDetailVC.chat(_:)), for: UIControlEvents.touchUpInside)
            cell.btnChat.tag = indexPath.row
            return cell
            
        }else if btnCheck.tag == 3{
            
            var cell : ReviewCell! = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
            if cell == nil {
                cell = ReviewCell(style: UITableViewCellStyle.default, reuseIdentifier: "ReviewCell")
            }
            if self.arrOfResponseKey.count > indexPath.row {
                if let str_hotel_review : String = dic["review"] as? String{
                    cell.lblUserReview.text = str_hotel_review
                }
                
                var rating : NSInteger = 0
                if let strRating: String = dic["rating"] as? String
                {
                    if strRating == ""
                    {
                        rating = 0
                    } else
                    {
                        rating = NSInteger(strRating)!
                    }
                } else if let strRating: NSInteger = dic["rating"] as? NSInteger
                {
                    rating = strRating
                }
                if rating == 1
                {
                    cell.imgReview.image = UIImage(named: "good_on.png")
                }
                else if rating == 2
                {
                    cell.imgReview.image = UIImage(named: "bad_on.png")
                }
                else if rating == 3
                {
                    cell.imgReview.image = UIImage(named: "bad.png")
                } else if rating == 0
                {
                    cell.imgReview.image = UIImage(named: "")
                }
                if let str_user_profile : String = dic["profile_pic"] as? String{
                    let url = URL(string:str_user_profile)
                    
                    cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                        (image, error, cacheType, imageURL) in
                        print(image?.description ?? "")
                    })
                    
                }
                if let str_date : String = dic["date_time"] as? String{
                    cell.lblDate.text   = str_date
                }
            }
            
            return cell
            
        }else
        {
            var cell : ReviewCell! = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
            if cell == nil {
                cell = ReviewCell(style: UITableViewCellStyle.default, reuseIdentifier: "ReviewCell")
            }
            return cell
        }
    }

    @objc func chat(_ sender: UIButton!)
    {
        print("index: %@", sender.tag )
        var destVC = ChatViewController()
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        if let str_myId =  AppTheme.getLoginDetails().value(forKey: "id") {
//            destVC.str_others_id = str_other_id
//            destVC.str_my_id = str_myId as! String
        }
        self.navigationController?.pushViewController(destVC, animated: true)
    }
   
    
//==================================================================================
    //MARK: Action for the Check-in check-out, view comment, view user, view review
    //MARK: show graph for single flight
//==================================================================================
    
    @IBAction func btnComentUserReviews(_ sender: UIButton)
    {
        let button: UIButton = sender as UIButton
        buttonSelected = UserDefaults.standard
        buttonSelected.set(button.tag, forKey: "buttonTag")
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if button.tag == 1{
            if status == 1{
                btnCheck.tag = 1
                self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
                self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
                let parameters : [String: AnyObject] = ["hotel_id" : dictForHotel["hotel_Id"]! as AnyObject ]
                
                AppTheme().callGetService(String(format: "%@get_hotel_comment", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
                    
                    if result == "success"
                    {
                        self.imgRecordNotFound.isHidden = true
                        self.lblRecordNotFound.isHidden = true
                        self.respDict = data as! NSDictionary
                        print(self.respDict)
                        if let arrRes: NSMutableArray = self.respDict["response"] as? NSMutableArray
                        {
                            self.arrOfResponseKey = arrRes
                        }
                        else if let arrRes: NSArray = self.respDict["response"] as? NSArray
                        {
                            self.arrOfResponseKey = NSMutableArray(array: arrRes)
                        }
                       // self.arrOfResponseKey = self.respDict["response"] as! NSMutableArray
                        self.tblCommUsrReviewList.isHidden = false
                        self.tblCommUsrReviewList.reloadData()
                    }else
                    {
                        self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:data as! String, showInView: self.view)
                        self.imgRecordNotFound.isHidden = false
                        self.lblRecordNotFound.isHidden = false
                    }
                }
            }
            else
            {
                self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
                self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
//                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry your not check In", showInView: self.view)
                self.imgRecordNotFound.isHidden = false
                self.lblRecordNotFound.isHidden = false
            }
        }
        else if button.tag == 2
        {
            if status == 1
            {
             self.getUserList()
            }
            else
            {
                self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
                self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
//                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry your not check In", showInView: self.view)
                self.imgRecordNotFound.isHidden = false
                self.lblRecordNotFound.isHidden = false
            }
        }
        else if button.tag == 3
        {
            if status == 1
            {
               self.getReviewList()
            }
            else
            {
                self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
                self.imgReview.image = UIImage(named : "ic_review_on.png")! as UIImage
//                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry your not check In", showInView: self.view)
                self.imgRecordNotFound.isHidden = false
                self.lblRecordNotFound.isHidden = false
            }
        }
    }
    
    @IBAction func btnCheckUncheck(_ sender: AnyObject)
    {
        if status == 0
        {
            self.viewBGForAlert.isHidden = false
            self.viewPopUpForCheckInCheckOut.isHidden = false
            viewForAlert.isHidden = true
        }
        else
        {
            status = 0
            self.btnCheckInCheckOut.setImage(UIImage(named: "img_uncheck.png") as UIImage?, for: UIControlState())
            self.lblCheckInCheckOutStatus.text = "check Out"
            
//            let parameters : [String: AnyObject] = ["hotel_Id" : dictForHotel["hotel_Id"]! as AnyObject,
//                                                    "user_id" :AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject,
//                                                    "check_status": status as AnyObject]
            
            let paraDict = NSMutableDictionary()
            paraDict.setValue(dictForHotel["hotel_Id"]!, forKey:"hotel_Id")
            paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")!, forKey:"user_id")
            paraDict.setValue(status, forKey:"check_status")
            
            AppTheme().callPostService(String(format: "%@hotel_log?", AppUrl), param: paraDict)
            {(result, data) -> Void in                               ///Call services
                
                if result == "success"
                {
                    self.btnWriteComment.isHidden = true
                    self.btnWriteReview.isHidden = true
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Check Out Successful.!", showInView: self.view)
                    self.imgRecordNotFound.isHidden = false
                    self.lblRecordNotFound.isHidden = false
                }
                else
                {
                    self.btnWriteComment.isHidden = true
                    self.btnWriteReview.isHidden = true
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Check Out Successful.!", showInView: self.view)
                    self.imgRecordNotFound.isHidden = false
                    self.lblRecordNotFound.isHidden = false
                }
            }
            self.tblCommUsrReviewList.isHidden = true
        }
    }
    
    
    @IBAction func btnCheck(_ sender: AnyObject)
    {
        if dictForHotel != nil {
      getHotelRating()
        }
    }
    
    func getHotelRating() {
        self.startProgressBar()
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if let str_hotel_id = dictForHotel["hotel_Id"] as? String {
            let parameters : [String: AnyObject] = ["hotel_id" : str_hotel_id  as AnyObject]
            
            AppTheme().callGetService(String(format: "%@SingleHotelRating", AppUrl), param: parameters) {(result, data) -> Void in
                
                if result == "success"
                {
                    self.loading.dismiss()
                    self.respDict = data as! NSDictionary
                    if let arrRes: NSMutableArray = self.respDict["response"] as? NSMutableArray
                    {
                        self.arrOfResponseKey = arrRes
                    }
                    else if let arrRes: NSArray = self.respDict["response"] as? NSArray
                    {
                        self.arrOfResponseKey = NSMutableArray(array: arrRes)
                    }
                    for dic in self.arrOfResponseKey{
                        let dict_detail : NSDictionary = dic as! NSDictionary
                        let barRatingVC = self.storyboard?.instantiateViewController(withIdentifier: "FlightTransportGraphController") as! FlightTransportGraphController
                        barRatingVC.strCheck = "comeFromHotel"
                        
                        if dict_detail.count != 0 {
                            barRatingVC.flightHotelDetailDict = dict_detail
                        }
                        self.navigationController?.pushViewController(barRatingVC, animated: true)
                    }
                } else  {
                    self.loading.dismiss()
                    let alertView = UIAlertView(title: "Message", message: "Hotel has no user ratings", delegate: nil, cancelButtonTitle: "Ok")
                            alertView.show()
//                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Hotel has no user ratings", showInView: self.view)
                }
            }
        }
    }
//===================================================
    //MARK: Action for the Comment and Review
//===================================================
    func writeComment()               // Method for the right navigationbar button comment
    {
        self.txtCommentReview.text = ""
        self.viewForAlert.isHidden = false
        self.txtCommentReview.layer.cornerRadius = 5
        self.txtCommentReview.layer.borderColor = UIColor.lightGray.cgColor
        self.txtCommentReview.layer.borderWidth = 1
        viewBGForAlert.isHidden = false
        self.viewForAlert.layer.borderWidth = 1
        self.viewForAlert.layer.borderColor = UIColor.gray.cgColor
        self.viewForAlert.layer.cornerRadius = 5
        self.viewForAlert.layer.masksToBounds = true
        btnGreen.isHidden = true
        btnGrey.isHidden = true
        btnRed.isHidden = true
        self.lblTitle.text = "Please write your comment."
        self.btnSubmit.tag = 10
    }
    
    
    @objc func writeReview() {                                             //Review Action
        self.txtCommentReview.text = ""
        self.viewForAlert.isHidden = false
        self.txtCommentReview.layer.cornerRadius = 5
        self.txtCommentReview.layer.borderColor = UIColor.lightGray.cgColor
        self.txtCommentReview.layer.borderWidth = 1
        viewBGForAlert.isHidden = false
        self.viewForAlert.layer.borderWidth = 1
        self.viewForAlert.layer.borderColor = UIColor.gray.cgColor
        self.viewForAlert.layer.cornerRadius = 5
        self.viewForAlert.layer.masksToBounds = true
        btnGreen.isHidden = false
        btnGrey.isHidden = false
        btnRed.isHidden = false
        self.lblTitle.text = "Please submit your review."
        self.txtCommentReview.text = "Please write your review..."
        self.btnSubmit.tag = 11
    }
    
    
 //Call for user comment
    func callUserComment()                                                                  //Call user comment to show
    {        
        let parameters : [String: AnyObject] = ["hotel_id" : dictForHotel["hotel_Id"]! as AnyObject ]
        
        AppTheme().callGetService(String(format: "%@get_hotel_comment", AppUrl), param: parameters) {(result, data) -> Void in                                                                                   ///Call services
            
            if result == "success"
            {
                self.imgRecordNotFound.isHidden = true
                self.lblRecordNotFound.isHidden = true
                self.respDict = data as! NSDictionary
                print(self.respDict)
                if let arrRes: NSMutableArray = self.respDict["response"] as? NSMutableArray
                {
                    self.arrOfResponseKey = arrRes
                }
                else if let arrRes: NSArray = self.respDict["response"] as? NSArray
                {
                    self.arrOfResponseKey = NSMutableArray(array: arrRes)
                }
                //self.arrOfResponseKey = self.respDict["response"] as! NSMutableArray
                self.tblCommUsrReviewList.reloadData()
            }
            else
            {
//                self.loading = JGProgressHUD(style: JGProgressHUDStyle.Dark)
//                self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
//                self.loading.textLabel.text = "No record found!"
//                self.loading.showInView(self.view)
//                self.loading.dismissAfterDelay(3.2)
                self.imgRecordNotFound.isHidden = false
                self.lblRecordNotFound.isHidden = false
            }
        }
    }
    
    func getUserList()
    {
        self.tblCommUsrReviewList.isHidden = false
        btnCheck.tag = 2
        ///self.imgComment.image = UIImage(named: "ic_comment_off.png")! as UIImage
        self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
        self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
        
        let parameters : [String: AnyObject] = ["user_id" : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject,
                                                "hotel_id" : dictForHotel["hotel_Id"]! as AnyObject]
        AppTheme().callGetService(String(format: "%@user_hotels", AppUrl), param: parameters) {(result, data) -> Void in                 ///Call services
            
            if result == "success"
            {
                self.imgRecordNotFound.isHidden = true
                self.lblRecordNotFound.isHidden = true
                self.respDict = data as! NSDictionary
                print(self.respDict)
                if let arrRes: NSMutableArray = self.respDict["response"] as? NSMutableArray
                {
                    self.arrOfResponseKey = arrRes
                }
                else if let arrRes: NSArray = self.respDict["response"] as? NSArray
                {
                    self.arrOfResponseKey = NSMutableArray(array: arrRes)
                }
                self.tblCommUsrReviewList.reloadData()
            }else{
               // self.imgComment.image = UIImage(named: "ic_comment_off.png")! as UIImage
                self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
                self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
//                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                self.imgRecordNotFound.isHidden = false
                self.lblRecordNotFound.isHidden = false
                self.tblCommUsrReviewList.isHidden = true
            }
        }
    }
    
    
    func getReviewList()
    {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        btnCheck.tag = 3
        self.imgUser.image = UIImage(named : "ic_user_off.png")! as UIImage
        self.imgReview.image = UIImage(named : "ic_review_on.png")! as UIImage
        
        let parameters : [String: AnyObject] = ["hotel_id" : dictForHotel["hotel_Id"]! as AnyObject ]
        AppTheme().callGetService(String(format: "%@get_reviews_hotels", AppUrl), param: parameters) {(result, data) -> Void in                                                                                    ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                self.imgRecordNotFound.isHidden = true
                self.lblRecordNotFound.isHidden = true
                self.respDict = data as! NSDictionary
                print(self.respDict)
                if let arrRes: NSMutableArray = self.respDict["response"] as? NSMutableArray
                {
                    self.arrOfResponseKey = arrRes
                }
                else if let arrRes: NSArray = self.respDict["response"] as? NSArray{
                    self.arrOfResponseKey = NSMutableArray(array: arrRes)
                }

               // self.arrOfResponseKey = self.respDict["response"] as! NSMutableArray
                self.tblCommUsrReviewList.isHidden = false
                self.tblCommUsrReviewList.reloadData()
            }
            else
            {
                self.loading.dismiss()
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view )
                self.imgRecordNotFound.isHidden = false
                self.lblRecordNotFound.isHidden = false
                self.tblCommUsrReviewList.isHidden = true
            }
        }
    }
    
    
   func getHotelComment()
   {
    let parameters : [String: AnyObject] = ["hotel_id" : dictForHotel["hotel_Id"]! as AnyObject ]
    
    AppTheme().callGetService(String(format: "%@get_hotel_comment", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
        
        if result == "success"
        {
            self.imgRecordNotFound.isHidden = true
            self.lblRecordNotFound.isHidden = true
            self.respDict = data as! NSDictionary
            print(self.respDict)
            if let arrRes: NSMutableArray = self.respDict["response"] as? NSMutableArray
            {
                self.arrOfResponseKey = arrRes
            }
            else if let arrRes: NSArray = self.respDict["response"] as? NSArray
            {
                self.arrOfResponseKey = NSMutableArray(array: arrRes)
            }
           // self.arrOfResponseKey = self.respDict["response"] as! NSMutableArray
            self.tblCommUsrReviewList.reloadData()
        }
        else
        {
            self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
            self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
            self.loading.textLabel.text = "Check In Successful."
            self.loading.show(in: self.view)
            self.loading.dismiss(afterDelay: 3.2)
            self.imgRecordNotFound.isHidden = false
            self.lblRecordNotFound.isHidden = false
        }
        self.tblCommUsrReviewList.reloadData()
    }
}
    
    // MARK: -  UItextView Delegate
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == txtCommentReview
        {
            if txtCommentReview.text == "Please write your review..." {
                txtCommentReview.text = ""
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtCommentReview
        {
            if txtCommentReview.text == "Please write your review..." {
                txtCommentReview.text = ""
            }
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == txtCommentReview
        {
            if txtCommentReview.text == "" {
                txtCommentReview.text = "Please write your review..."
            }
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.txtCommentReview.resignFirstResponder()
    }
    
//=====================================================================================
    //MARK: Actions for buttons on navigationbar- Rating, Comment-post, Review- post
//=====================================================================================
    @IBAction func btnRating(_ sender: UIButton)
    {
        if sender.tag == 100
        {
            isUglyRating = false
            isBadRating = false
            if isGoodRating == false {
                self.rating = 1
                isGoodRating = true
                self.btnGreen.setImage(UIImage(named: "good_on.png")! as UIImage, for: UIControlState())
            }else{
                self.rating = 0
                isGoodRating = false
                self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            }
            self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())
            self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
//            self.rating = 1
//            self.btnGreen.setImage(UIImage(named: "good_on.png")! as UIImage, forState: .Normal)
//            self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, forState: .Normal)
//            self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, forState: .Normal)
        }
        else if sender.tag == 101
        {
            isGoodRating = false
             isUglyRating = false
            if isBadRating == false {
                self.rating = 2
                isBadRating = true
                self.btnRed.setImage(UIImage(named: "bad_on.png")! as UIImage, for: UIControlState())
            }else{
                self.rating = 0
                isBadRating = false
                self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())
            }
            self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
          /*  self.rating = 2
            self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, forState: .Normal)
            self.btnRed.setImage(UIImage(named: "bad_on.png")! as UIImage, forState: .Normal)
            self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, forState: .Normal) */
        }
        else if sender.tag == 102
        {
            isGoodRating = false
            isBadRating = false
            if isUglyRating == false {
                self.rating = 3
                isUglyRating = true
                self.btnGrey.setImage(UIImage(named: "bad.png")! as UIImage, for: UIControlState())
            }else{
                isUglyRating = false
                self.rating = 0
                self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
            }
            self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())
           /* self.rating = 3
            self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, forState: .Normal)
            self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, forState: .Normal)
            self.btnGrey.setImage(UIImage(named: "bad.png")! as UIImage, forState: .Normal) */
        }
    }
    
    
    @IBAction func btnSubmit(_ sender: AnyObject) {
//        self.startProgressBar()
        txtCommentReview.resignFirstResponder()
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        if btnSubmit.tag == 10
        {
            if self.txtCommentReview.text != "" && self.txtCommentReview.text != "Please write your review..." && self.txtCommentReview.text != "Please write your review.." && self.txtCommentReview.text != "Please write your review." || rating != 0
            {
//                let parameters : [String: AnyObject] = ["user_id" : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject,
//                    "hotel_id" : dictForHotel["hotel_Id"]! as AnyObject,
//                    "comment" :  self.txtCommentReview.text as AnyObject]
                
                let paraDict = NSMutableDictionary()
                paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")!, forKey:"user_id")
                paraDict.setValue(dictForHotel["hotel_Id"]!, forKey:"hotel_Id")
                paraDict.setValue(self.txtCommentReview.text, forKey:"comment")
                
                AppTheme().callPostService(String(format: "%@hotel_comment", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
                    
                    if result == "success"{
                        self.loading.dismiss()
                        self.respDict = data as! NSDictionary
                        print(self.respDict)
                        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
                        self.loading.indicatorView = JGProgressHUDSuccessIndicatorView()
                        self.loading.textLabel.text = "Post sent"
                        self.loading.show(in: self.view)
                        self.loading.dismiss(afterDelay: 3.2)
                        self.viewBGForAlert.isHidden = true
                        self.viewForAlert.isHidden = true
                        self.tblCommUsrReviewList.reloadData()
                    } else{
                        self.loading.dismiss()
                        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
                        self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.loading.textLabel.text = "Sorry no data found."
                        self.viewBGForAlert.isHidden = false
                        self.loading.show(in: self.view)
                        self.loading.dismiss(afterDelay: 3.2)
                    }
                }
            }else {
                self.loading.dismiss()
                self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
                self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
                self.loading.textLabel.text = "Please write comment!"
                self.viewBGForAlert.isHidden = false
                self.loading.show(in: self.view)
                self.loading.dismiss(afterDelay: 3.2)
            }
        }
        else if btnSubmit.tag == 11
        {
            if txtCommentReview.text != "Please write your review..." && txtCommentReview.text != "" || rating == 0 || rating == 1 || rating == 2 || rating == 3
            {
                if txtCommentReview.text != "Please write your review..."  && txtCommentReview.text != "" ||  rating == 0 || rating == 1 || rating == 2 || rating == 3{
                    if txtCommentReview.text == "Please write your review..." && rating == 0{
                        self.loading.dismiss()
                         _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please send enter your review or rating.", showInView: self.view)
                    } else {
                        if txtCommentReview.text == "Please write your review..."{
                           txtCommentReview.text = ""
                        }
                        
                        let paraDict = NSMutableDictionary()
                        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")!, forKey:"user_id")
                        paraDict.setValue(dictForHotel["hotel_Id"]!, forKey:"hotel_id")
                        paraDict.setValue(self.txtCommentReview.text, forKey:"review")
                        paraDict.setValue(rating, forKey:"rating")
                        
                        AppTheme().callPostService(String(format: "%@hotel_review", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
                            
                            if result == "success" {
                                self.loading.dismiss()
                                self.respDict = data as! NSDictionary
                                if let str_msg = self.respDict.value(forKey: "message") as? String{
                                    _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: str_msg, showInView: self.view)
                                }
                                self.btnGreen.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
                                self.btnRed.setImage(UIImage(named: "bad_off.png")! as UIImage, for: UIControlState())
                                self.btnGrey.setImage(UIImage(named: "ugly_off.png")! as UIImage, for: UIControlState())
                                self.rating = 0
                                self.getReviewList()
                                self.viewBGForAlert.isHidden = true
                                self.viewForAlert.isHidden = true
                            }else{
                                self.loading.dismiss()
                                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                                self.viewBGForAlert.isHidden = true
                                self.viewForAlert.isHidden = true
                            }
                        }
                    }
                }else{
                    self.loading.dismiss()
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please send enter your review or rating.", showInView: self.view)
                }
            }else{
                self.loading.dismiss()
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please send enter your review or rating.", showInView: self.view)
            }
        }
    }
    @IBAction func btnCancel(_ sender: AnyObject) {
        self.viewBGForAlert.isHidden = true
    }
    
    
    
//========================================================
    //MARK: Actions for Cancel, Submit on alert view
//========================================================
   
    @IBAction func btnCancelForCheckInCheckOut(_ sender: AnyObject)
    {
       self.viewBGForAlert.isHidden = true
       self.viewPopUpForCheckInCheckOut.isHidden = true
    }
    @IBAction func btnSubmitForCheckInCheckOut(_ sender: AnyObject)
    {
           if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
         if self.txtNameAtBooking.text != ""  && txtConfirmationNum.text != ""
          {
            self.viewBGForAlert.isHidden = true
            self.viewPopUpForCheckInCheckOut.isHidden = true
            
            if status == 0
             {
                connectedLike = 1
                self.showRightNavigationBarButtonHide()
                self.imgRecordNotFound.isHidden = true
                self.lblRecordNotFound.isHidden = true
                status = 1
                self.btnCheckInCheckOut.setImage(UIImage(named: "img_check.png") as UIImage?, for: UIControlState())
                self.lblCheckInCheckOutStatus.text = "check In"

                
                let paraDict = NSMutableDictionary()
                paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")!, forKey:"user_id")
                paraDict.setValue(dictForHotel["hotel_Id"]!, forKey:"hotel_id")
                paraDict.setValue(dictForHotel["hotel_pic"]!, forKey:"hotel_image")
                paraDict.setValue(dictForHotel["address"]!, forKey:"hotel_address")
                paraDict.setValue(dictForHotel["hotel_name"]!, forKey:"hotel_name")
                paraDict.setValue(dictForHotel["1"]!, forKey:"register_no")
                paraDict.setValue(self.txtNameAtBooking.text, forKey:"booking_name")
                paraDict.setValue(self.txtConfirmationNum.text, forKey:"confirmation_number")
                paraDict.setValue(connectedLike, forKey:"connected_like")
                paraDict.setValue(status, forKey:"check_status")
                
                
                
                AppTheme().callPostService(String(format: "%@hotel_log?", AppUrl), param: paraDict)
                    {(result, data) -> Void in                             ///Call services
                    
                      if result == "success"
                       {
                         self.btnWriteComment.isHidden = false
                         self.btnWriteReview.isHidden = false
                    
                        
                        if let key = UserDefaults.standard.value(forKey: "buttonTag") as? NSInteger
                        {
                            if  key == 1
                            {
                                print("Method for comment")
                                self.callUserComment()
                            }
                            else if  key == 2
                            {
                                print("Method for user")
                                self.getUserList()
                            }
                            else if  key == 3
                            {
                                print("Method for review")
                                self.getReviewList()
                            }
                            self.tblCommUsrReviewList.isHidden = false
                        }
                       }
                       else
                       {
                        self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry no data found", showInView: self.view)
                         self.tblCommUsrReviewList.isHidden = true
                       }
                     }
                    self.tblCommUsrReviewList.isHidden = false
               }
               else
               {
                 status = 0
                 self.btnCheckInCheckOut.setImage(UIImage(named: "img_uncheck.png") as UIImage?, for: UIControlState())
                 self.lblCheckInCheckOutStatus.text = "check Out"
                
//                let parameters : [String: AnyObject] = ["hotel_Id" : dictForHotel["hotel_Id"]! as AnyObject,
//                "user_id" :AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject,
//                "check_status": status as AnyObject]
//
                let paraDict = NSMutableDictionary()
                paraDict.setValue(dictForHotel["hotel_Id"]!, forKey:"hotel_Id")
                paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")!, forKey:"user_id")
                paraDict.setValue(status, forKey:"check_status")
                
                 AppTheme().callPostService(String(format: "%@hotel_log?", AppUrl), param: paraDict)
                  {(result, data) -> Void in                               ///Call services
            
                   if result == "success"
                     {
                       
                       self.btnWriteComment.isHidden = true
                       self.btnWriteReview.isHidden = true
                       self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Check Out Successful.!", showInView: self.view)
                       self.imgRecordNotFound.isHidden = false
                       self.lblRecordNotFound.isHidden = false
                      }
                      else
                      {
                       self.btnWriteComment.isHidden = true
                       self.btnWriteReview.isHidden = true
                       self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Check Out Successful.!", showInView: self.view)
                       self.imgRecordNotFound.isHidden = false
                       self.lblRecordNotFound.isHidden = false
                      }
                   }
                        self.tblCommUsrReviewList.isHidden = true
                }
           }
           else
           {
            self.loading.dismiss()
            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please fill all the fields.", showInView: self.view)
            self.tblCommUsrReviewList.isHidden = true
           }
     }
    @IBAction func btnCheckForConnectHotel(_ sender: AnyObject)
    {
        if isCheck == 0
        {
            isCheck = 1
            connectedLike = 1
             self.btnCheckForConnectHotel.setImage(UIImage(named: "img_check.png") as UIImage?, for: UIControlState())
        }
        else
        {
            isCheck = 0
            connectedLike = 0
            self.btnCheckForConnectHotel.setImage(UIImage(named: "img_uncheck.png") as UIImage?, for: UIControlState())
        }
    }
    
    //============================
    //MARK: Hotail detail action
    //============================
     @IBAction func btnHotelDetail(_ sender: AnyObject)
     {
//         let objHotelDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "IndivisualHotelDetailViewController") as! IndivisualHotelDetailViewController
//        self.navigationController?.pushViewController(objHotelDetailVC, animated: true)
    }
    
    //MARK: User check for online/offline
    func userOnChat(_ str_my_id: String, str_others_id: String) {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
//        let parameters : [String: AnyObject] = ["from_id" : str_my_id  as AnyObject,
//                                                "to_id" :  str_others_id as AnyObject,
//                                                "status" : "0" as AnyObject]
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(str_my_id, forKey:"from_id")
        paraDict.setValue(str_others_id, forKey:"to_id")
        paraDict.setValue("0", forKey:"status")
        
        AppTheme().callPostService(String(format: "%@user_online", AppUrl), param: paraDict) {(result, data) -> Void in
            if result == "success"
            {
                print("Change status")
            }
        }
    }
    
    func getSelectedHotelDetail(_ hotel_Id: String, city_name: String) {
        
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let parameters : [String: AnyObject] =
            [  "hotel_id" : hotel_Id as AnyObject,
               "user_id" : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject
            ]
        
        AppTheme().callGetService(String(format: "%@GetHotel", AppUrl), param: parameters) {(result, data) -> Void in                                            ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                
                var dataForAirlineFlightUser : NSDictionary = NSDictionary()
                dataForAirlineFlightUser = data as! NSDictionary
                
                if let dicData = dataForAirlineFlightUser["response"] as? NSDictionary
                {
                    self.arrForHotel = Utility.getValueForObject(dicData["hotel"] as! NSArray) as! NSArray

                    if let tempDic: NSDictionary = dicData["users"] as?  NSDictionary
                    {
                        self.dictForUserDetail = tempDic
                    }
                    print("4444\( self.dictForUserDetail)")

//                     if let tempArr: NSMutableArray = dicData["users"] as?  NSMutableArray
//                    {
//                        self.arrForUserDetail = Utility.getValueForObject(tempArr) as? NSArray
//                    }

                    if self.arrForHotel.count != 0 {
                        
                        for element in self.arrForHotel       //.....Handle details for Airline, Flight and User
                        {
                            self.dictForHotel  = element as! NSDictionary
                        }
                        

                        let str: String = (self.dictForHotel["hotel_Id"] as? String)!
                         AppTheme.setHotelId(NSInteger(str)! )
                        
                        if let str_hotel_name : String = self.dictForHotel["hotel_name"] as? String{
                            self.lblHotelName.text = str_hotel_name
                        }
                        if let str_hotel_city : String = self.dictForHotel["city"] as? String{
                            self.lblAddress.text = str_hotel_city
                        }
                        if let str_hotel_address : String = self.dictForHotel["address"] as? String{
                            self.lblDetailAddress.text = str_hotel_address
                        }
                        
                        var str_state : String!
                        var str_country_code : String!
                        
                        if let str_hotel_detail_address_state  : String = self.dictForHotel["state"] as? String{
                            str_state = str_hotel_detail_address_state
                        }
                        if let str_hotel_detail_add_countryCode : String = self.dictForHotel["countryCode"] as! String!{
                            str_country_code = str_hotel_detail_add_countryCode
                        }
                        self.lblStateAddress.text = "\(str_state!)  \(str_country_code!)"
                        
                        if let str_hotel_image_string : String = self.dictForHotel["hotel_pic"] as? String {
                            
                            let url = URL(string:str_hotel_image_string)
                            if url != nil {
                            self.imgHotel.kf.setImage(with: url!, placeholder: UIImage(named: "ic_hotel_orange.png"), options: nil, progressBlock: nil, completionHandler: {
                                (image, error, cacheType, imageURL) in
                                print(image?.description ?? "")
                            })
                            }
                        }
                        
                        //Check user status for checking checkout
                        if let strStatus: String = self.dictForHotel["check_status"] as? String{
                            self.status = NSInteger(strStatus)!
                        }
                        else if let strStatus: NSInteger = self.dictForHotel["check_status"] as? NSInteger{
                           self.status = strStatus
                        }
                        if self.status == 1{
                            self.showRightNavigationBarButtonHide()
                            self.imgRecordNotFound.isHidden = true
                            self.lblRecordNotFound.isHidden = true
                            self.btnCheckInCheckOut.setImage(UIImage(named: "img_check.png")! as UIImage, for: UIControlState())
                            self.lblCheckInCheckOutStatus.text = "Check In"
                            self.getUserList()
                        }else{
                            self.imgUser.image = UIImage(named : "ic_user.png")! as UIImage
                            self.imgReview.image = UIImage(named : "ic_review_off.png")! as UIImage
                            self.imgRecordNotFound.isHidden = false
                            self.lblRecordNotFound.isHidden = false
                            self.btnCheckInCheckOut.setImage(UIImage(named: "img_uncheck.png")! as UIImage, for: UIControlState())
                            self.tblCommUsrReviewList.isHidden = true
                        }
                    }
                }
            }else{
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:  "Sorry there is no hotel details available!", showInView: self.view)
                self.navigationController?.presentedViewController
                let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    
    
}
