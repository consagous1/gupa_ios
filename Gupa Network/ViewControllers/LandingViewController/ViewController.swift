//
//  ViewController.swift
//  Gupa Network
//
//  Created by mac on 4/27/16.
//  Copyright © 2016 mobiweb. All rights reserved.

import UIKit
import FacebookLogin
import JGProgressHUD

class ViewController: UIViewController {

    @IBOutlet var vwContainer : UIView!
    @IBOutlet weak var btnFacebook: UIButton!
    
    var loading : JGProgressHUD = JGProgressHUD(style: JGProgressHUDStyle.dark)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true

        appDelegate.centerNav = self.navigationController
        
        for view in self.vwContainer.subviews {
            if let btn : UIButton = view as? UIButton
            {
                btn.layer.borderWidth = 1.5
                btn.layer.borderColor = UIColor.lightGray.cgColor
                btn.layer.cornerRadius = 2
            }
        }
        let myLoginButton = UIButton(type: .custom)
        btnFacebook = myLoginButton
        myLoginButton.frame = btnFacebook.frame
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

 //===========================================================================
 // MARK: - Button actions for the LoginViewController and RegistrationVC
 //===========================================================================
    
    @IBAction func goToLogin (_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "goToLoginVC", sender: self)
    }
    
    @IBAction func goToRegistration (_ sender: UIButton) {
        
      //  self.performSegueWithIdentifier("goToRegisterationVC", sender: self)
    }

    // MARK: - Facebook login Api call

    func faceBookLogin_APICall(faceBookId :String ,userName : String ,userEmail : String) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        var firstName : String?
        var lastName : String?
        
        if userName.isEmpty {
            firstName = ""
            lastName = ""
        } else {
            let fullNameArr = userName.components(separatedBy: " ")
        
            firstName = fullNameArr[0]
            lastName  = fullNameArr[1]
        }
        
        let paraDict = NSMutableDictionary()
        
        paraDict.setValue(faceBookId, forKey:"fb_id")
        paraDict.setValue(userName, forKey:"fb_name")
        paraDict.setValue(userEmail, forKey:"fb_email")
        paraDict.setValue(firstName, forKey:"fb_first_name")
        paraDict.setValue(lastName, forKey:"fb_last_name")
        
        AppTheme().callPostService(String(format: "%@FbLogin/fb_login","https://gupanetwork.com/backend/"), param: paraDict) {(result, data) -> Void in                                                                                  ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                
                var departments : NSDictionary = NSDictionary()
                departments = data as! NSDictionary
                
                if let str_active = data.value(forKey: "is_active") as? NSInteger
                {
                    if str_active == 1 {
                        
                        print("Department \(departments)")
                        
                        AppTheme.setLoginDetails(departments)
                     
                        if !(data.value(forKey: "mongo_obj") is NSNull) {
                            ApplicationPreference.saveMongoUserId(userId: (data.value(forKey: "mongo_obj") as? String)!)
                        }

                        _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: departments.value(forKey: "message") as! String, showInView: self.view)
                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                    
                } else {
                    var destVC : VerificationControllerl = VerificationControllerl()
                    destVC = self.storyboard?.instantiateViewController(withIdentifier: "VerificationControllerl") as! VerificationControllerl
                    self.navigationController?.pushViewController(destVC, animated: true)
                }
            } else
            {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
}

extension ViewController {
    
    @IBAction func facebookLoginAction(_ sender: Any) {
        
        var strID = ""
        var strName = ""
        var strEmail = ""
        
        let loginManager = LoginManager()
        
        loginManager.logIn(readPermissions: [.publicProfile], viewController: self) { (loginResult) in
            
            FBSDKGraphRequest(graphPath:"me", parameters: ["fields":"id,email,name,picture.width(480).height(480)"]).start(completionHandler: { (connection, result, error) in
                if error == nil {
                    
                    if let jsonResult = result as? Dictionary<String, AnyObject> {
                        
                        print("Json Result \(jsonResult)")
                        
                        if jsonResult["id"] != nil{
                            strID = jsonResult["id"] as! String
                            print(strID)
                        }
                        
                        if jsonResult["name"] != nil{
                            strName = jsonResult["name"] as! String
                        }
                        
                        if jsonResult["email"] != nil{
                            strEmail = jsonResult["email"] as! String
                        }
                    }
                    self.faceBookLogin_APICall(faceBookId: strID ,userName: strName , userEmail:  strEmail)
                    
                } else {
                    print("Error Getting Info \(String(describing: error))");
                }
            })
            
        }
    }
    
}
