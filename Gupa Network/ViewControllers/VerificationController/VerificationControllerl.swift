//
//  VerificationControllerl.swift
//  Gupa Network
//
//  Created by MWM23 on 12/20/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD


class VerificationControllerl: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textfield_verification_code: UITextField!
    @IBOutlet weak var btn_resend_mail: UIButton!
    @IBOutlet weak var btn_go: UIButton!
    
    var loading : JGProgressHUD! = JGProgressHUD()
    
    //MARK:- Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.title = "Verification"
        let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(VerificationControllerl.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        btn_go.layer.cornerRadius = 3.0
        btn_go.layer.masksToBounds = true
        btn_go.layer.borderColor = UIColor.lightGray.cgColor
        btn_go.layer.borderWidth = 1
        
        
        btn_resend_mail.layer.cornerRadius = 3.0
        btn_resend_mail.layer.masksToBounds = true
        btn_resend_mail.layer.borderColor = UIColor.lightGray.cgColor
        btn_resend_mail.layer.borderWidth = 1
        
    }
    @objc func leftBarBackButton(){
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Resend button action and Go action methods
    @IBAction func btn_resendMail(_ sender: AnyObject) {
        resendEmail()
    }
    
    @IBAction func btn_Go(_ sender: AnyObject) {
        go_action()
    }
    
    
    func resendEmail() {
        
        if (!isNetworkAvailable) {
            self.loading.dismiss()
            return
        }
//        if isRechableToInternet() == false{
//            return
//        }
        if let str_user_id : String = AppTheme.getUserEmail_Login() as String {
            let parameters : [String: AnyObject] = ["id" : str_user_id as AnyObject,
                                                    "type" : "app" as AnyObject]
            AppTheme().callGetService(String(format: "%@resendVerification", AppUrl), param: parameters) { (result, data) -> Void in
                
                if result == "success"{
                    self.loading.dismiss()
                    var respDict : NSDictionary = NSDictionary()
                    respDict = data as! NSDictionary
                    if let str_msg = respDict .value(forKey: "message") as? String{
                      _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: str_msg, showInView: self.view)
                    }
                    
                }
            }
        }
    }

    func go_action(){
        
        if (!isNetworkAvailable) {
            self.loading.dismiss()
            return
        }
        
//        if isRechableToInternet() == false{
//            return
//        }
        if let str_user_id : String = AppTheme.getUserEmail_Login() as String {
            
            if textfield_verification_code.text != "" || textfield_verification_code.text != nil {
                let parameters : [String: AnyObject] = ["id" : str_user_id as AnyObject,
                                                        "token" : self.textfield_verification_code.text as AnyObject]
                AppTheme().callGetService(String(format: "%@active_user", AppUrl), param: parameters) { (result, data) -> Void in
                    
                    if result == "success"{
                        self.loading.dismiss()
                        var respDict : NSDictionary = NSDictionary()
                        respDict = data as! NSDictionary
                        print(respDict)
                        let objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                         self.navigationController?.pushViewController(objFlightDetailVC, animated: true)

//                    self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: (data as? String)!, showInView: self.view)
                    }else{
                         self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: (data as? String)!, showInView: self.view)
                    }
                }
            }else{
                print("Please enter code")
            }
            }
           
           
    }
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    @IBAction func btn_go_action(_ sender: AnyObject) {
         go_action()
    }
    
      
}
