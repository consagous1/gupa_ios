//
//  EditViewController.swift
//  Gupa Network
//
//  Created by mac on 5/24/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class EditViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet var scrollVw : UIScrollView!
    @IBOutlet var vwContainer : UIView!
    
    @IBOutlet var btnUpdate: UIButton!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var txtFirstName : UITextField!
    @IBOutlet var txtLastName : UITextField!
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet var txtBirthday: UITextField!
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtCity: UITextField!
    
    @IBOutlet var leftBarBtn : UIBarButtonItem!
    var loading : JGProgressHUD = JGProgressHUD(style: JGProgressHUDStyle.dark)
    var imagePicker = UIImagePickerController()
    var countyId: NSMutableArray = NSMutableArray()
    var stateId: NSMutableArray = NSMutableArray ()
    var cityId: NSMutableArray = NSMutableArray ()
    var countryName: NSMutableArray = NSMutableArray()
    var stateName: NSMutableArray = NSMutableArray ()
    var cityName: NSMutableArray = NSMutableArray ()
    var cityIdSelected : String = String()
    var id : String = String()
    var imageData : Data = Data()
    var pickerCountStatCity = UIPickerView()
    var txtCheck : UITextField = UITextField ()
    
    
    var arrOfCountry : NSMutableArray = NSMutableArray()
    var arrOfState: NSMutableArray = NSMutableArray()
    var arrOfCity: NSMutableArray = NSMutableArray()

//MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Edit Profile"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 66.0/255.0, green: 199.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        
        self.navigationItem.rightBarButtonItem = AppTheme.AddNavigationRightBarButton(HomeVC())
        
        
        //picker view delegate
        pickerCountStatCity.delegate = self
        pickerCountStatCity.dataSource = self
        
        self.btnUpdate.layer.borderWidth = 2;
        
        self.txtCountry.inputView = pickerCountStatCity
        self.txtState.inputView = pickerCountStatCity
        self.txtCity.inputView = pickerCountStatCity
        
        //Call services and set response for edit
        let parameters : [String: AnyObject] = ["id" : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject]
        AppTheme().callGetService(String(format: "%@user_info", AppUrl), param: parameters) { (result, data) -> Void in
            print(data)//manage response in dictionary and array
            var respDict : NSMutableDictionary = NSMutableDictionary()
            respDict = data as! NSMutableDictionary
            print(respDict)
            let arrOfResponseKey = respDict["response"] as! NSMutableArray
            var dictOfResponseKey : NSMutableDictionary = NSMutableDictionary()
            print(arrOfResponseKey)
            for element in arrOfResponseKey
            {
                dictOfResponseKey  = element as! NSMutableDictionary
            }
            print(dictOfResponseKey)
            let url = URL(string:(dictOfResponseKey["profile_pic"] as? String)!)
            let data = try? Data(contentsOf: url!)
            if data != nil {
                self.imgProfile?.image = UIImage(data:data!)
            }
            self.txtFirstName.text = dictOfResponseKey["first_name"] as? String
            self.txtLastName.text = dictOfResponseKey["last_name"] as? String
            // self.txtEmail.text = dictOfResponseKey["email"] as? String
            //self.txtBirthday.text = (dictOfResponseKey["dob"] as? String)!
            let strCountry: String = (dictOfResponseKey["country_name"] as? String)!
            let strState: String = (dictOfResponseKey["state_name"] as? String)!
            let strCity: String = (dictOfResponseKey["city_name"] as? String)!
            self.txtCountry.text = strCountry
            //self.txtCountry.text = strState //(strCountry, animated: false)
            self.txtState.text = strState //(strState, animated: false)
            self.txtCity.text = strCity //(strCity, animated: false)
            
        }
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        imgProfile.layer.masksToBounds = true
        imgProfile.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action:#selector(EditViewController.handleTapOnProfileImg(_:)))
        tap.delegate = self
        imgProfile.addGestureRecognizer(tap)
        //        }else if appDelegate.strURL == "editVC"
        //        {
        //}
        
//        for   view in self.vwContainer.subviews {
//            if let txtField : UITextField = view as? UITextField
//            {
//                let paddingView: UIView = UIView(frame: CGRectMake(0, 0, 10, 35))
//                paddingView.backgroundColor = UIColor.clearColor()
//                txtField.leftView = paddingView
//                txtField.leftViewMode = .Always
//                
//                txtField.layer.borderColor = UIColor.lightGrayColor().CGColor
//                txtField.layer.borderWidth = 1.0;
//                txtField.layer.cornerRadius = 2
//                txtField.delegate = self
//            }
        
            //            if let dropDownField : IQDropDownTextField = view as? IQDropDownTextField
            //            {
            //
            //                let imageView1 = UIImageView(image: UIImage(named: "Drop-down"))
            //                imageView1.contentMode = UIViewContentMode.Center
            //                imageView1.frame = CGRectMake(0, 0, imageView1.image!.size.width + 20.0, imageView1.image!.size.height)
            //                dropDownField.rightViewMode = UITextFieldViewMode.Always
            //                dropDownField.rightView = imageView1
            //
            //                dropDownField.addTarget(self, action: "textFieldDidChange:", forControlEvents: .EditingDidEnd)
            //                dropDownField.delegate = self
            //
            //            }
       // }
        
        //txtBirthday.dropDownMode = .DatePicker
        
        self.countryName = NSMutableArray()
        self.countyId = NSMutableArray()
                getCountryStateCity("country", searchId: "") { (result, data) -> Void in
                    if result == "success"
                    {
                        self.arrOfCountry.add(data as AnyObject as! [String])
                       // print(self.arrOfCountry)
                        //print(self.arrOfCountry.count)
                   }
               }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //==================================================
    // MARK: - UIPickerView delegate datasource method
    //==================================================
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        
        var row : Int = Int()
        
        if txtCheck.tag == 1
        {
            row = self.countryName.count
        }
        if txtCheck.tag == 2
        {
            row = self.arrOfState.count
        }
        if txtCheck.tag == 4
        {
            row = self.arrOfCity.count
        }
        
        return row
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var str: String = String()
       // self.pickerCountStatCity.reloadAllComponents()
        if txtCheck.tag == 1
        {
            let strArr : NSArray = (self.arrOfCountry[row] as? NSArray)!
            //self.pickerCountStatCity.reloadAllComponents()
            str =  (strArr.object(at: row) as? String)!
            return str
            
        }
        if txtCheck.tag == 2
        {
            let arrState  = (self.arrOfState[row] as? NSArray)!
            
            str = (arrState.object(at: row) as? String)!
            
        }
        if txtCheck.tag == 4
        {
                let cityArr: NSArray = (self.arrOfCity[row] as? NSArray)!
             str = (cityArr.object(at: row) as? String)!
        }
        return str
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if txtCountry.isFirstResponder
        {
            let str : NSArray = (self.arrOfCountry[row] as? NSArray)!
            
            self.txtCountry.text = str.object(at: row) as? String
        }
        if txtState.isFirstResponder
        {
            let str : NSArray = (self.arrOfState[row] as? NSArray)!
            
            self.txtState.text = str.object(at: row) as? String
        }
        if txtCity.isFirstResponder
        {
            let str : NSArray = (self.arrOfCity[row] as? NSArray)!
            let strText : String = (str.object(at: row) as? String)!
            self.txtCity.text = strText
        }
        
        
    }
    
    
    //============================================
    // MARK: - method for image action and send
    //============================================
    @objc func handleTapOnProfileImg(_ sender: UITapGestureRecognizer) {
        // handling code
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion:
                { () -> Void in
            })
        }
        print("Profile iamge selected")
    }
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
    {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        imgProfile.image = image
        imageData = UIImageJPEGRepresentation(image, 0.5)!
    }
    
    
    //==================================
    // MARK: - textField Delegates
    //==================================
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtCountry
        {
            txtCheck.tag = 1
        }
        if textField == txtState
        {
            txtCheck.tag = 2
        }
        if textField == txtCity
        {
            txtCheck.tag = 4
           // print(self.arrOfCity)
           // print(self.arrOfCity.count)
        }
       
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtCountry
        {
            if self.countryName.count == 0 {
                print("Please select county")
            } else if self.countryName.count != 0
            {
                getCountryStateCity("state", searchId: self.countyId.object(at: self.countryName.index(of: txtCountry.text!)) as! String)  {(result, data) -> Void in
                    
                    if result == "success"
                    {
                        self.arrOfState.add(data as AnyObject as! [String])
                        //print(self.arrOfState)
                    }
                }
               // print("you are here")
                self.arrOfState.removeAllObjects()
            }
            
        }
        if textField == txtState
        {
          // txtCheck.tag = 3
            getCountryStateCity("city", searchId: self.stateId.object(at: self.stateName.index(of: txtState.text!)) as! String) {(result, data) -> Void in
                if result == "success"
                {
                    self.arrOfCity.add(data as AnyObject as! [String])
                   // print(self.arrOfCity)
                }
            }
            //self.arrOfCity.removeAllObjects()
        }
        
        self.arrOfCity.removeAllObjects()
    }
    
    
    
    func getCountryStateCity(_ SubURl: String, searchId: String, completion:@escaping (_ result: String, _ data: AnyObject)-> Void )
    {
        var parameters : [String: AnyObject] = [String: AnyObject]()
        if SubURl == "state" {
            parameters  = ["country_id" : searchId as AnyObject]
            self.stateName = NSMutableArray()
            self.stateId = NSMutableArray()
            
        } else if SubURl == "city"
        {
            parameters  = ["state_id" : searchId as AnyObject]
            self.cityName = NSMutableArray()
            self.cityId = NSMutableArray()
        } else {
            parameters  = [String: AnyObject]()
            self.countryName = NSMutableArray()
            self.countyId = NSMutableArray()
        }
        
        AppTheme().callGetService(String(format: "%@%@", AppUrl,SubURl), param: parameters) { (result, data) -> Void in
            print(data)
            if(result == "success") {
                if let aryData : NSMutableArray = data.value(forKey: "response") as? NSMutableArray
                {
                    for dict in aryData
                    {
                        
                        let dic: NSMutableDictionary = dict as! NSMutableDictionary
                        
                        if SubURl == "state" {
                            self.stateId.add(dic["id"] as! String)
                            self.stateName.add(dic["name"] as! String)
                            completion("success", self.stateName)
                            
                        } else if SubURl == "city" {
                            
                            self.cityId.add(dic["id"] as! String)
                            self.cityName.add(dic["name"] as! String)
                            completion("success", self.cityName)
                            
                            
                        } else {
                            self.countyId.add(dic["id"] as! String)
                            self.countryName.add(dic["name"] as! String)
                            completion("success", self.countryName)
                            
                        }
                    }
                }
                print(data["response"])
            } else {
                completion("error", "error Found" as AnyObject)
            }
        }
    }



}
