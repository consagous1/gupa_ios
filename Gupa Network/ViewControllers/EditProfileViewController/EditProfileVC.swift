//
//  EditProfileVC.swift
//  Gupa Network
//
//  Created by mac on 4/28/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import IQDropDownTextField
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}



class EditProfileVC: UIViewController ,UIScrollViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate, IQDropDownTextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource, UITableViewDelegate {
    var appDelegate: AppDelegate = AppDelegate()
    
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet var scrollVw : UIScrollView!
    @IBOutlet var vwContainer : UIView!
    @IBOutlet weak var viewForCity: UIView!
    @IBOutlet weak var tbl_city: UITableView!
    
    @IBOutlet var btnUpdate: UIButton!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var txtFirstName : UITextField!
    @IBOutlet var txtLastName : UITextField!
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet var txtBirthday: IQDropDownTextField!
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtState: UITextField!
    //  @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtInterests: UITextField!
    
    @IBOutlet var leftBarBtn : UIBarButtonItem!
    
    var loading : JGProgressHUD = JGProgressHUD(style: JGProgressHUDStyle.dark)
    var imagePicker = UIImagePickerController()
    var countyId: NSMutableArray = NSMutableArray()
    var stateId: NSMutableArray = NSMutableArray ()
    var cityId: NSMutableArray = NSMutableArray ()
    var countryName: NSMutableArray = NSMutableArray()
    var stateName: NSMutableArray = NSMutableArray ()
    var cityName: NSMutableArray = NSMutableArray ()
    var cityIdSelected : String = String()
    var id : String = String()
    var imageData : Data = Data()
    var pickerCountStatCity = UIPickerView()
    var txtCheck : UITextField = UITextField ()
    
    var arrOfCountry : NSMutableArray = NSMutableArray()
    var arrOfState: NSMutableArray = NSMutableArray()
    var arrOfCity: NSMutableArray = NSMutableArray()
    var arrOfInterest: NSMutableArray = NSMutableArray()
    var dictOfResponseKey : NSDictionary = NSDictionary()
    var arrOfGender: NSArray = NSArray()
    
    var isCitySearching : Bool = false
    var strTableType : String = ""
    var str_id_city : String!
    var chosenImage : UIImage!

    @IBOutlet weak var lblSelectListTitle: UILabel!
    
    var btnSearchUser:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    //MARK:- Life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        appDelegate.centerNav = self.navigationController
        
        self.title = "Edit Profile"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        
        showRightNavigationBarButtonHide()
        
        //Alert for value popup
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
        viewForCity.isHidden = true
        updateFunction()
        
        pickerCountStatCity.delegate = self
        pickerCountStatCity.dataSource = self
        
        self.btnUpdate.layer.borderWidth = 1;
        
        self.txtState.inputView = pickerCountStatCity
        self.txtInterests.inputView = pickerCountStatCity
        
        self.txtState.isEnabled = true
        getUserDeatil()
        
    }
    
    func showRightNavigationBarButtonHide()                               //RightNavigationBarButton hide unhide
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnSearchUser = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnSearchUser.setImage(imgChat, for: UIControlState())
        btnSearchUser.addTarget(self, action: #selector(EditProfileVC.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnSearchUser)
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(EditProfileVC.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    func updateFunction(){
        
//        imgProfile.layer.cornerRadius = 45 //imgProfile.frame.height/2
//        imgProfile.layer.masksToBounds = true
        imgProfile.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action:#selector(EditProfileVC.handleTapOnProfileImg(_:)))
        tap.delegate = self
        imgProfile.addGestureRecognizer(tap)
        
        for   view in self.vwContainer.subviews {
            if let txtField : UITextField = view as? UITextField{
                let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 35))
                paddingView.backgroundColor = UIColor.clear
                txtField.leftView = paddingView
                txtField.leftViewMode = .always
                
                txtField.layer.borderColor = UIColor.lightGray.cgColor
                txtField.layer.borderWidth = 1.0;
                txtField.layer.cornerRadius = 2
                txtField.delegate = self
            }
        }
        
        txtBirthday.dropDownMode = .datePicker
        
        arrOfGender = ["Female", "Male", "Other"]
        self.arrOfInterest =  ["Animals", "Arts & Humanities", "Automotive", "Fitness", "Business", "Computers & Electronics", "Entertainment", "Food & Drink", "Games",  "Home & Garden", "Industries", "Internet", "Lifestyles", "News", "Photo & Video", "Science", "Shopping", "Social Networks", "Society", "Sports", "Telecommunications", "Travel"]
        
        self.countryName = NSMutableArray()
        self.countyId = NSMutableArray()
    }
    
    func getUserDeatil() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }

        let parameters : [String: AnyObject] = ["id" : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject]
        
        AppTheme().callGetService(String(format: "%@user_info", AppUrl), param: parameters) { (result, data) -> Void in
            
            if result == "success"{
                
                self.loading.dismiss()
                var respDict : NSDictionary = NSDictionary()
                var arrOfResponseKey : NSMutableArray = NSMutableArray()
                respDict = data as! NSDictionary
                
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                } else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                    arrOfResponseKey = array_Response_mutable
                }
                
                if arrOfResponseKey.count > 0 {
                    
                    for element in arrOfResponseKey
                    {
                        self.dictOfResponseKey  = element as! NSDictionary
                    }
                    
                    print(self.dictOfResponseKey)
                    
                    if let str_user_name : String = self.dictOfResponseKey["username"] as? String{
                        self.userLabel.text = "Username: \(str_user_name)"
                    }
                    if let str_first_name : String = self.dictOfResponseKey["first_name"] as? String{
                        self.txtFirstName.text = str_first_name
                    }
                    if let str_last_name : String = self.dictOfResponseKey["last_name"] as? String{
                        self.txtLastName.text = str_last_name
                    }
                    if let str_dob : String = self.dictOfResponseKey["dob"] as? String {
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy"
                        let date = dateFormatter.date(from: str_dob)
                        
                        let dateFormatterRight = DateFormatter()
                        dateFormatterRight.dateFormat = "dd MMM yyyy"
                        dateFormatterRight.dateStyle = DateFormatter.Style.long
                        let timeStamp = dateFormatterRight.string(from: date!)
                        self.txtBirthday.setSelectedItem(timeStamp, animated: true)
                    }
                    if let str_country_name : String = self.dictOfResponseKey["country_name"] as? String{
                        if let str_city_name = self.dictOfResponseKey .value(forKey: "city_name") as? String{
                            self.txtCountry.text = "\(str_city_name), \(str_country_name)"
                        }
                    }
                    if let str_city_id = self.dictOfResponseKey.value(forKey: "city_id") as? String{
                        self.str_id_city = str_city_id
                    }
                    if let str_state_name : String = self.dictOfResponseKey["gender"] as? String{
                        self.txtState.text = str_state_name
                    }
                    
                    if let str_interest_name : String = self.dictOfResponseKey["interests"] as? String{
                        self.txtInterests.text = str_interest_name
                    }
                    if let str_user_image : String = self.dictOfResponseKey["profile_pic"] as? String{
                        let url = URL(string:str_user_image)
                        if url != nil {
                        self.imgProfile.kf.setImage(with: url!, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: {
                            (image, error, cacheType, imageURL) in
                            print(image?.description ?? "")
                         })
                      }
                    }
                }
            } else {
                self.loading.dismiss()
               _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let date = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat = "yyyy MMM EEEE HH:mm"///this is you want to convert format
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let timeStamp = dateFormatter.string(from: date!)
        
        return timeStamp
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        //        getUserDeatil()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func homeButton()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //==================================================
    // MARK: - UIPickerView delegate datasource method
    //==================================================
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        var row : Int = Int()
        if txtCheck.tag == 1{
            if self.countryName.count > 0  {
                row = self.countryName.count
                return row
            }
            return 0
        }
        if txtCheck.tag == 2
        {
            if self.arrOfGender.count > 0 {
                row = self.arrOfGender.count
                return row
            }
            return 0
        }
        if txtCheck.tag == 5
        {
            if self.arrOfInterest.count > 0 {
                row = self.arrOfInterest.count
                return row
            }
            return 0
        }
        return row
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var str: String = String()
        str = ""
        if txtCheck.tag == 1
        {
            if self.arrOfCountry.count > row {
                let strArr : NSArray = (self.arrOfCountry[row] as? NSArray)!
                str =  (strArr.object(at: row) as? String)!
                return str
            }
            return str
        }
        if txtCheck.tag == 2
        {
            if self.arrOfGender.count > row{
                str = (self.arrOfGender.object(at: row) as? String)!
                return str
            }
            return str
        }
        
        if txtCheck.tag == 5
        {
            if self.arrOfInterest.count > row {
                str = (self.arrOfInterest.object(at: row) as? String)!
                return str
            }
            return str
        }
        return str
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if txtCountry.isFirstResponder
        {
            let str : NSArray = (self.arrOfCountry[row] as? NSArray)!
            self.txtCountry.text = str.object(at: row) as? String
        }
        if txtState.isFirstResponder
        {
            let strText : String = (self.arrOfGender.object(at: row) as? String)!
            self.txtState.text = strText
        }
        if txtInterests.isFirstResponder
        {
            let strText : String = (self.arrOfInterest.object(at: row) as? String)!
            self.txtInterests.text = strText
        }
    }
    
    //============================================
    // MARK: - method for image action and send
    //============================================
    
    @objc func handleTapOnProfileImg(_ sender: UITapGestureRecognizer) {
        
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertControllerStyle.actionSheet)
        
        settingsActionSheet.addAction(UIAlertAction(title:"Choose Photo", style:UIAlertActionStyle.default, handler:
            {
                action in self.takeImageFromGallery()
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Take Photo", style:UIAlertActionStyle.default, handler:
            {
                action in self.takeImageFromCamera()
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Cancel", style:UIAlertActionStyle.cancel, handler:nil))
        present(settingsActionSheet, animated:true, completion:nil)
    }
    
    func takeImageFromGallery () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            imagePicker.delegate      = self
            imagePicker.sourceType    = UIImagePickerControllerSourceType.savedPhotosAlbum
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func takeImageFromCamera () {
        
        if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
            imagePicker.delegate             = self
            imagePicker.sourceType           = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing        = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    
    //****************************************************
    // MARK: - Image Picker Delegates
    //****************************************************
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){

        dismiss(animated: true, completion: { () -> Void in

            self.chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            self.imgProfile.contentMode = .scaleAspectFit
            self.imgProfile.image = self.chosenImage
            self.imageData = UIImageJPEGRepresentation(self.chosenImage, 0.5)!
     })
    }

    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    //==================================
    // MARK: - textField Delegates
    //==================================
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtCountry
        {
            //self.txtCity.enabled = true
            self.txtState.isEnabled = true
            txtCheck.tag = 1
        }
        if textField == txtState
        {
            txtCheck.tag = 2
        }
        if textField == txtInterests
        {
            txtCheck.tag = 5
        }
        if textField == txtBirthday
        {
            if (txtBirthday.selectedItem != nil)
            {
                txtBirthday.selectedItem = ""
                txtBirthday.maximumDate = Date()
            }
            else
            {
                print("null value")
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtCountry {
            if(txtCountry.text?.characters.count > 0) {
                //deSelectionView()
                viewForCity.isHidden = true
            }
        } else if textField == txtBirthday{
            let date2 : Date =  (Calendar.current as NSCalendar).date(byAdding: .year, value: -13, to: Date(), options: [])!
            let date_current = Date()
            let deFormatter = DateFormatter()
            let date_final = txtBirthday.date
            
            deFormatter.dateFormat = "MMM dd, YYYY"
            let dateAfter = date_final!.add(-13, months: 0, weeks: 0, days: 0, hours: 0, minutes: 0, seconds: 0)
            print(dateAfter)
            
            if date2 <= date_current {
                if  date_final <=  date2 {
                    print("Date is presented")
                }  else  {
                     _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "The user should be 13+ years old to use this app.", showInView: self.view)
                    
                }
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCountry{
            if(txtCountry.text?.characters.count > 0 || string.characters.count > 0)
            {
                if !isCitySearching
                {
                    isCitySearching = true
                    strTableType = "City"
                    arrOfCity.removeAllObjects()
                    tbl_city.reloadData()
                    showSelectionView()
                }
                var strSearch : String = ""
                if(txtCountry.text?.characters.count > 0){
                    strSearch =  txtCountry.text! + string // "\(txtCity.text)\(string)"
                }else{
                    strSearch = string
                }
                callSearchCity(strSearch)
            }
        }
        return true
    }
    
    //Search city with autocomplete
    func callSearchCity(_ searchText: String)  {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        print(searchText)
        let parameters : [String: AnyObject] = ["search" : searchText as AnyObject ]
        
        AppTheme().callGetService(String(format: "%@city_search", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if let array_temp : NSArray = respDict["response"] as? NSArray{
                    self.arrOfCity = array_temp.mutableCopy() as! NSMutableArray
                }
                self.view.bringSubview(toFront: self.viewForCity)
                self.viewForCity.isHidden = false
                self.tbl_city.reloadData()
                
                // self.tbl_city_list.reloadData()
                
            } else  {
                self.loading.dismiss()
                // self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                // self.tbl_city_list.hidden = true
            }
        }
    }
    
    func textFieldDidChange(_ theTextField: IQDropDownTextField) {
        if (theTextField.selectedItem != nil)
        {
            NSLog("text changed: %@", theTextField.selectedItem!)
        }
        else
        {
            print("null value")
        }
    }
    
    @IBAction func btnUpdate(_ sender: UIButton){
        
        var parameters : [String: AnyObject] = [String: AnyObject]()
        if self.txtFirstName.text == ""
        {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter First Name.", showInView: self.view)
        }
        else if self.txtLastName.text == ""
        {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Last Name.", showInView: self.view)
        }
            
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        startProgressBar()
        
        if txtBirthday.selectedItem != nil || txtBirthday.selectedItem == nil
        {
            self.loading.dismiss()
            let date2 : Date =  (Calendar.current as NSCalendar).date(byAdding: .year, value: -13, to: Date(), options: [])!
            let date_current = Date()
            let deFormatter = DateFormatter()
            let date_final = txtBirthday.date
            
            deFormatter.dateFormat = "MMM dd, YYYY"
            if date_final != nil {
            let dateAfter = date_final!.add(-13, months: 0, weeks: 0, days: 0, hours: 0, minutes: 0, seconds: 0)
             print(dateAfter)
            }
            
            if date2 <= date_current {
                if  date_final <=  date2 {
                    
                    if (!isNetworkAvailable) {
                        return
                    }
                    
                    if let str_userId =  AppTheme.getLoginDetails().object(forKey: "id") as? String{
                        parameters = ["first_name" : self.txtFirstName.text as AnyObject,
                                      "last_name"  : self.txtLastName.text as AnyObject,
                                      "dob"        : self.txtBirthday.selectedItem as AnyObject,
                                      "city_id"    : str_id_city as AnyObject,
                                      "id"         : str_userId as AnyObject,
                                      "interests"  : self.txtInterests.text as AnyObject,
                                      "gender"     : self.txtState.text as AnyObject]
                        
                        AppTheme().callPostWithMultipartServices(String(format: "%@update_profile", AppUrl), param: parameters, imageData: imageData, file_type: "image")
                        { (result, data) -> Void in
                            
                            if(result == "success")
                            {
                                self.loading.dismiss()

                                if self.appDelegate.isEqual(nil){
                                    self.appDelegate = UIApplication.shared.delegate as! AppDelegate
                                }
                                
                                UserDefaults.standard.set("\(String(describing: self.txtFirstName.text!)) \(String(describing: self.txtLastName.text!))", forKey: "UserName")

                                self.appDelegate.isEditProfile = true
                                _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Successfully Updated", showInView: self.view)
                            } else {
                                self.loading.dismiss()
                                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                            }
                        }
                    }
                } else  {
                    self.loading.dismiss()

                    _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "The user should be 13+ years old to use this app.", showInView: self.view)
                }
            }
            
        }
        else
        {
            if (!isNetworkAvailable) {
                Utility.showNetWorkAlert()
                return
            }
            if let str_userId =  AppTheme.getLoginDetails().object(forKey: "id") as? String {
                
                parameters = ["first_name" : self.txtFirstName.text as AnyObject,
                              "last_name"  : self.txtLastName.text as AnyObject,
                              "dob"        : self.txtBirthday.selectedItem as AnyObject,
                              "city_id"    : str_id_city as AnyObject,
                              "id"         : str_userId as AnyObject,
                              "interests"  : self.txtInterests.text as AnyObject,
                              "gender"     : self.txtState.text as AnyObject]
                
                AppTheme().callPostWithMultipartService(String(format: "%@update_profile", AppUrl), param: parameters, imageData: imageData )
                { (result, data) -> Void in
                    
                    if(result == "success")
                    {
                        self.loading.dismiss()

                        if self.appDelegate.isEqual(nil){
                            self.appDelegate = UIApplication.shared.delegate as! AppDelegate
                        }
                        self.appDelegate.isEditProfile = true
                        _  = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Successfully Updated!", showInView: self.view)
                    }
                    else
                    {
                        self.loading.dismiss()
                        _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                    }
                }
            }
            
        }
    }
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    func rightBarBackButton()
    {
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    func showSelectionView()
    {
        //viewCityContainer.hidden = false
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
            
            //self.viewTblPickerBottomLayout.constant = 0
            self.view
                .layoutIfNeeded()
            
            })
        { (finished) -> Void in
            //self.playVideo(strDefaultUrl)
        }
    }
    
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if  arrOfCity.count > 0
        {
            return arrOfCity.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
        
        if cell == nil {
            cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
        }
        let temp_dic: NSDictionary = self.arrOfCity.object(at: indexPath.row) as! NSDictionary
        if arrOfCity.count > indexPath.row {
            if let str_city_name : String = temp_dic .value(forKey: "location") as? String{
                cell.titleLabel.text = str_city_name
            }
            cell.cityLabel.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let dic: NSDictionary = self.arrOfCity.object(at: indexPath.row) as! NSDictionary
        
        if let str_city_name  = dic .value(forKey: "location") as? String{
            txtCountry.text = str_city_name
        }
        if let str_city_id : String = dic .value(forKey: "id") as? String{
            str_id_city = str_city_id
        }
        viewForCity.isHidden = true
        // hideSelectionView()
        isCitySearching = false
    }
    @IBAction func btnCancelOnCityPopup(_ sender: AnyObject) {
        self.viewForCity.isHidden = true
    }
}


public func <(left: Date, right: Date) -> Bool {
    return left.compare(right) == ComparisonResult.orderedAscending
}
