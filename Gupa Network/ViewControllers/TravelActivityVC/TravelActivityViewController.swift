//
//  TravelActivityViewController.swift
//  Gupa Network
//
//  Created by mac on 5/20/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher
import CoreData

class TravelActivityViewController: UIViewController, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    
    @IBOutlet weak var arrivalButton: UIButton!
    @IBOutlet weak var departureButton: UIButton!
    @IBOutlet weak var arrivalBottomView: UIView!
    @IBOutlet weak var departureBottomView: UIView!
    
    @IBOutlet var tblActivity: UITableView!
    @IBOutlet var leftBarBtn : UIBarButtonItem!
    
    @IBOutlet var HotelSearchBar : UISearchBar!
    
    var searchActive : Bool = false
    var filtered : NSArray = NSArray()
   
    var loading : JGProgressHUD!
    var flightList : NSArray!
    let identifier = "TravelActivityTableCell"
    var isDepartureSelect: Bool = false
    var apiString: String!
    var airportCodeString : String!
    
    var appDelegate: AppDelegate = AppDelegate()
    var airportList : NSArray!
    var checkTableCell : NSInteger = 0
    
    var reversed: NSArray = NSArray()
    var arrOfDestinatioStation: NSMutableArray = NSMutableArray()
    var arrOfDestinatioStationCode: NSMutableArray = NSMutableArray()
    var btnChat:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    var isGPSOff : Bool = false
    
    
    // MARK: - ViewControllers life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
      //  startActivity()
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 440, height: 44))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.text = "Travel Information Display"
        label.textColor = UIColor.white
        self.navigationItem.titleView = label
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 66.0/255.0, green: 199.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        
        showRightNavigationBarButtonHide()
        
         appDelegate = UIApplication.shared.delegate as! AppDelegate
        tblActivity.isHidden = false
        checkTableCell = 0
        tblActivity.separatorStyle = UITableViewCellSeparatorStyle.none
        tblActivity.estimatedRowHeight = 300
        tblActivity.rowHeight = UITableViewAutomaticDimension
        tblActivity.setNeedsLayout()
        tblActivity.layoutIfNeeded()
        tblActivity.delegate = self
        tblActivity.dataSource = self
        arrivalBottomView.isHidden = false
        departureBottomView.isHidden = true
        isDepartureSelect = false
        
        if airportCodeString == nil{
            if isGPSOn() == false{
                isGPSOff = true
               // self.loading.dismiss()
                return
            }
        }
        if let codeAirport : NSString  = AppTheme.getAirportCode()  {
                airportCodeString = String(codeAirport as NSString)
                //Call airport activity services
                startActivity()
                getAirportActivity(airportCodeString)
            }
    }
    
    func showRightNavigationBarButtonHide()                               //RightNavigationBarButton hide unhide
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnChat = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnChat.setImage(imgChat, for: UIControlState())
        btnChat.addTarget(self, action: #selector(TravelActivityViewController.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnChat)        
        
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(TravelActivityViewController.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    
    @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getAirportActivity(_ airport : String) {
        
        if (!isNetworkAvailable) {
            self.loading.dismiss()

            Utility.showNetWorkAlert()
            return
        }
        
        let parameters : [String: AnyObject] = ["airport" : airport as AnyObject]
        print(parameters)
        
        if isDepartureSelect == false {
            apiString =  "airport_arrival_info?"
           
        } else{
            apiString = "airport_departure_info?"
        }
        AppTheme().callGetCode(String(format: "%@%@", AppUrl, self.apiString), param: parameters) {(result, data) -> Void in
            
         if result == "success"
            {
                self.loading.dismiss()

                self.tblActivity.isHidden = false
                self.checkTableCell = 1
                var departments : NSDictionary = NSDictionary()
                departments = data as! NSDictionary
                if let array_response : NSArray = departments["response"] as? NSArray{
                    self.flightList = array_response
                }
                self.tblActivity.reloadData()
            } else
            {
                  self.loading.dismiss()
                _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: (data as? String)!, showInView: self.view)
            }
        }
    }
    
    
    
//========================================================
    //MARK: UITableview Datasource and delegate methods
//========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if checkTableCell == 1{
        if flightList != nil {
            return flightList.count * 2
         }
          return 0
        } else {
            if filtered.count != 0{
                return  filtered.count
            } else if airportList != nil
            {
                return airportList.count
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if checkTableCell == 1 {
        var cell: TravelActivityTableCell! = tableView.dequeueReusableCell(withIdentifier: "TravelActivityTableCell") as? TravelActivityTableCell
        
        if cell == nil {
            cell = TravelActivityTableCell(style: UITableViewCellStyle.default, reuseIdentifier: "TravelActivityTableCell")
        }
        cell.layer.cornerRadius=10 //set corner radius here
        cell.layer.borderColor = UIColor.lightGray.cgColor  // set cell border color here
        cell.layer.borderWidth = 2
        
        
        if indexPath.row % 2 == 1 {
            if flightList.count > indexPath.row/2 {
            
            if  let dataDict = flightList .object(at: indexPath.row/2) as? NSDictionary
            {
                if let str_depart_time : String = dataDict["arrival_time"] as? String{
                     let strSplit = str_depart_time.characters.split(separator: "T")
                     cell.lblFlightDate.text = String(strSplit.first!)
                }
                if let str_airline_code : String = dataDict .value(forKey: "airline_code")! as? String{
                    let strAirlineCode : String = str_airline_code
                    if let str_airline_number : String = dataDict .value(forKey: "number")! as? String{
                        let strAirlineNumber : String = str_airline_number
                        cell.lblFlightNameNumber.text = "Flight " + strAirlineCode + strAirlineNumber
                    }
                }
                if let str_airline_name : String = dataDict .value(forKey: "airline_name") as? String{
                   cell.lblFlightTime.text = str_airline_name
                }
                if let str_departure_code : String = dataDict .value(forKey: "departure_code") as? String{
                    cell.lblSorceStationNameCode.text = str_departure_code
                }
                if let str_arival_code : String = dataDict .value(forKey: "arrival_code") as? String{
                    cell.lblDestinationStationNameCode.text = str_arival_code
                }
                if let str_arival_city : String = dataDict .value(forKey: "arrival_city") as? String{
                    cell.lblFlightArrivalCityName.text = str_arival_city
                }
                if let str_departure_city : String = dataDict .value(forKey: "departure_city") as? String{
                    cell.lblFlightDepartureCityName.text = str_departure_city
                }
                var delayed : NSInteger = 0
                if let flighDelayTime: String = dataDict .value(forKey: "delayed") as? String{
                    if flighDelayTime == "" {
                        
                    } else{
                        delayed = NSInteger(flighDelayTime)!
                    }
                }
                else if let flighDelayTime: NSInteger = dataDict["delayed"] as? NSInteger {
                    delayed = flighDelayTime
        
                }
                if delayed <= 0{
                    cell.lblFlightStatus.text = "On time"
                } else {
                    cell.viewForFlightActualTime.backgroundColor = UIColor(red: 221.0/255.0, green: 100.0/255.0, blue: 101/255.0, alpha: 1.0)
                    if let str_delay : String = String(dataDict .value(forKey: "delayed") as! NSInteger){
                        cell.lblFlightStatus.text = "Delayed by " + str_delay + "min"
                    }
                }
                if let str_departure_terminal : String = dataDict .value(forKey: "departure_terminal") as? String{
                     cell.lblActualTimeIn.text = "Departs terminal \n" + str_departure_terminal
                }
                if let str_arival_terminal : String = dataDict .value(forKey: "arrival_terminal") as? String{
                    cell.lblActualTimeOut.text = "Arives terminal \n" + str_arival_terminal
                }
                if let str_departure_time : String = dataDict["departure_time"] as? String{
                    let strSplitTime = str_departure_time.characters.split(separator: "T")
                    cell.lblScheduleTimeOut.text =  String(strSplitTime.last!)
                }
                
                if let str_arrival_time : String = dataDict["arrival_time"] as? String{
                    let strSplitTimeArr = str_arrival_time.characters.split(separator: "T")
                    cell.lblScheduleTimeIn.text =  String(strSplitTimeArr.last!)
                }
            }
            }
            return cell
        }else{
            var cell = tableView .dequeueReusableCell(withIdentifier: "cell")
            if cell == nil {
                
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            }
            cell?.backgroundColor = UIColor.clear
            cell!.isUserInteractionEnabled = false
            return cell!
        }
        } else if checkTableCell == 0
        {
            
            var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
            
            if cell == nil {
                cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
            }
            if(searchActive){
                if filtered.count > indexPath.row {
                 cell.titleLabel.text = filtered.object(at: indexPath.row) as? String
                }
            } else {
                if airportList.count > indexPath.row {
                 cell.titleLabel.text = airportList.object(at: indexPath.row) as? String
                }
            }
           
            return cell
        } else
        {
            var cell = tableView .dequeueReusableCell(withIdentifier: "cell")
            if cell == nil {
                
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            }
            cell?.backgroundColor = UIColor.clear
            cell!.isUserInteractionEnabled = false
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if checkTableCell == 1 {
            var cell: TravelActivityTableCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? TravelActivityTableCell
            
            if cell == nil {
                cell = TravelActivityTableCell(style: UITableViewCellStyle.default, reuseIdentifier: identifier)
            }
            tblActivity.backgroundColor = UIColor.clear
            tblActivity.separatorStyle = UITableViewCellSeparatorStyle.none
            tblActivity.estimatedRowHeight = 100
            tblActivity.rowHeight = UITableViewAutomaticDimension
            if indexPath.row%2 == 0 {
                return 5
            }
            return 200
        } else{
            return 45
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if checkTableCell == 0 {
            startActivity()
            
            if(searchActive){
                
                if filtered.count > indexPath.row {
                HotelSearchBar.text = filtered.object(at: indexPath.row) as? String
                 airportCodeString = (self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.HotelSearchBar.text!)) as! String)
                   getAirportActivity(self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.HotelSearchBar.text!)) as! String)
                }
            } else {
                if airportList.count > indexPath.row {
                HotelSearchBar.text = airportList.object(at: indexPath.row) as? String
                }
            }
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
//MARK: Top bar button actions
    func startActivity() {
        
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
    }
    
    @IBAction func arrivalDepartureButtonAction(_ sender: UIButton) {
        
        if sender.tag == 1 {
            self.startActivity()
            arrivalBottomView.isHidden = false
            departureBottomView.isHidden = true
            isDepartureSelect = false
            if isGPSOff == true {
                self.loading.dismiss()
            } else {
                if airportCodeString != "" {
                     isGPSOff = false
                    getAirportActivity(airportCodeString)
                }
            }
        } else if sender.tag == 2{
            startActivity()
            arrivalBottomView.isHidden = true
            departureBottomView.isHidden = false
            isDepartureSelect = true
            if isGPSOff == true {
                self.loading.dismiss()
            }else{
                if airportCodeString != "" || airportCodeString != nil {
                    isGPSOff = false
                    getAirportActivity(airportCodeString)
                }
            }
        }
    }
    
    
    
    //=====================================
    //MARK: UISearchBar delegate methods
    //=====================================
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        checkTableCell = 0
        searchActive = false;
        airportList = getAirportDetail()
        tblActivity.isHidden = false
        tblActivity.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        HotelSearchBar.resignFirstResponder()
        searchActive = false;
        tblActivity.isHidden = true
//        startActivity()
        if airportCodeString == nil{
            if isGPSOn() == false{
                self.loading.dismiss()
                return
            }
        }
        airportCodeString = AppTheme.getAirportCode() as String
        getAirportActivity(airportCodeString)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        HotelSearchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        HotelSearchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let resultPredicate = NSPredicate(format: "SELF CONTAINS[c] %@",  searchText)
        filtered = airportList.filtered(using: resultPredicate) as NSArray
        
        if(filtered.count == 0){
            checkTableCell = 1
            searchActive = false;
            HotelSearchBar.canResignFirstResponder
            tblActivity.reloadData()
        } else {
            checkTableCell = 0
            searchActive = true;
            tblActivity.reloadData()
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       // getAirportActivity()
        HotelSearchBar.resignFirstResponder()
    }
    
    
    func getAirportDetail() -> NSArray {
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Table_travel", in: self.appDelegate.managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        var result : NSArray!
        
        do {
            result = try appDelegate.managedObjectContext.fetch(fetchRequest) as NSArray
            
            
            if (result.count > 0) {
                
                for i in 0 ..< result.count {
                    let airportDetail = result[i] as! NSManagedObject
                    let strCityNameCode = (airportDetail.value(forKey: "airport_city") as! String) + " " + (airportDetail.value(forKey: "airport_code") as! String)
                    self.arrOfDestinatioStation.add(strCityNameCode as String)
                    self.arrOfDestinatioStationCode.add(airportDetail.value(forKey: "airport_code") as! String)
                }
                
                //sorted array for destination stations
                self.reversed = (self.arrOfDestinatioStation as NSArray as! [String]).sorted(by: { (s1: String, s2: String) -> Bool in
                    return s1 < s2
                }) as NSArray
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return self.reversed
    }

    
}
