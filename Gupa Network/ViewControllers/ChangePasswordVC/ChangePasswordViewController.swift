//
//  ChangePasswordViewController.swift
//  Gupa Network
//
//  Created by mac on 5/26/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD


class ChangePasswordViewController: UIViewController {

    @IBOutlet var txtOldPwd: UITextField!
    @IBOutlet var txtNewPwd: UITextField!
    @IBOutlet var txtConfirmPwd: UITextField!
    @IBOutlet var btnUpdate: UIButton!
    
    var loading : JGProgressHUD = JGProgressHUD(style: JGProgressHUDStyle.dark)
    var btnSearchUser:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    
   //==============================
    //MARK: Life cycle
   //==============================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
         self.title = "Change Password"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 66.0/255.0, green: 199.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        showRightNavigationBarButtonHide()
        self.btnUpdate.layer.borderWidth = 1
    }
    
    func showRightNavigationBarButtonHide()                               //RightNavigationBarButton hide unhide
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnSearchUser = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnSearchUser.setImage(imgChat, for: UIControlState())
        btnSearchUser.addTarget(self, action: #selector(ChangePasswordViewController.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnSearchUser)
        
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(ChangePasswordViewController.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    
    @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnUpdate(_ sender: AnyObject)
    {
        if txtOldPwd.text == ""
        {
            self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
            self.loading.show(in: self.view )
            self.loading.textLabel.text = " Please enter Old password."
            self.loading.dismiss(afterDelay: 1.8)
            
        }  else if txtNewPwd.text == ""
        {
            self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
            self.loading.show(in: self.view )
            self.loading.textLabel.text = "Please enter New Password."
            self.loading.dismiss(afterDelay: 1.8)
            
        } else if txtConfirmPwd.text != txtNewPwd.text || txtConfirmPwd.text == ""
        {
            self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
            self.loading.show(in: self.view )
            self.loading.textLabel.text = "Passwords does not Match"
            self.loading.dismiss(afterDelay: 1.8)
            
        } else {

            let paraDict = NSMutableDictionary()
            paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")!, forKey:"user_id")
            paraDict.setValue(self.txtOldPwd.text, forKey:"old_password")
            paraDict.setValue(self.txtNewPwd.text, forKey:"new_password")
            
            AppTheme().callPostService(String(format: "%@change_password", AppUrl), param: paraDict) {
                (result, data) -> Void in
                if result == "success"
                {
                    if data.value(forKey: "status") as! Int == 0 {
                        self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.loading.show(in: self.view )
                        self.loading.textLabel.text = data.value(forKey: "message")! as? String
                        self.loading.dismiss(afterDelay: 1.2)
                    } else
                    {
                        self.txtOldPwd.text = ""
                        self.txtNewPwd.text = ""
                        self.txtConfirmPwd.text = ""
                        
                        self.loading.indicatorView = JGProgressHUDSuccessIndicatorView()
                        self.loading.show(in: self.view )
                        self.loading.textLabel.text = "Passwords change successfully!"
                        self.loading.dismiss(afterDelay: 1.2)
                    }
                    
                } else
                {
                    self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
                    self.loading.show(in: self.view )
                    self.loading.textLabel.text = data as? String
                    self.loading.dismiss(afterDelay: 1.2)
                }
            }
        }
    }
}
