//
//  ExtensionTableView.swift
//  Ikan
//
//  Created by RajeshYadav on 29/10/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import Foundation

public extension UITableView {
    
    var indexesOfVisibleSections: [Int] {
        // Note: We can't just use indexPathsForVisibleRows, since it won't return index paths for empty sections.
        var visibleSectionIndexes = [Int]()
        
        for i in 0..<numberOfSections {
            var headerRect: CGRect?
            // In plain style, the section headers are floating on the top, so the section header is visible if any part of the section's rect is still visible.
            // In grouped style, the section headers are not floating, so the section header is only visible if it's actualy rect is visible.
            if (self.style == .plain) {
                headerRect = rect(forSection: i)
            } else {
                headerRect = rectForHeader(inSection: i)
            }
            if headerRect != nil {
                // The "visible part" of the tableView is based on the content offset and the tableView's size.
                let visiblePartOfTableView: CGRect = CGRect(x: contentOffset.x, y: contentOffset.y, width: bounds.size.width, height: bounds.size.height)
                if (visiblePartOfTableView.intersects(headerRect!)) {
                    visibleSectionIndexes.append(i)
                }
            }
        }
        return visibleSectionIndexes
    }
    
    internal var visibleSectionHeaders: [LogSectionHeaderView] {
        var visibleSects = [LogSectionHeaderView]()
        for sectionIndex in indexesOfVisibleSections {
            if let sectionHeader = headerView(forSection: sectionIndex) {
                visibleSects.append(sectionHeader as! LogSectionHeaderView)
            }
        }
        
        return visibleSects
    }
    
    func scrollToBottom() {
        
        let lastSection: Int = self.numberOfSections - 1
        if lastSection < 0 {
            return
        }
        let lastRow: Int = self.numberOfRows(inSection: lastSection) - 1
        if lastRow < 0 {
            return
        }
        let ip = IndexPath(row: lastRow, section: lastSection)
        self.scrollToRow(at: ip, at: .top, animated: true)
        
//        let rows = self.numberOfRows(inSection: 0)
//        let indexPath = IndexPath(row: rows - 1, section: 0)
//        if(indexPath.row > 0){
//            self.scrollToRow(at: indexPath, at: .top, animated: true)
       // }
    }
}
