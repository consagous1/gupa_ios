//
//  MessagesBubbleImageFactory.swift
//  Flum
//
//  Created by RajeshYadav on 23/12/16.
//  Copyright © 2016 47billion. All rights reserved.
//

import UIKit

/**
 *  `MessagesBubbleImageFactory` is a factory that provides a means for creating and styling
 *  `MessagesBubbleImage` objects to be displayed in a `MessagesCollectionViewCell` of a `MessagesCollectionView`.
 */
class MessagesBubbleImageFactory: NSObject{
    
    var bubbleImage: UIImage!
    
    var capInsets = UIEdgeInsets()
    
    /**
     * Specifies the layout direction of the message bubble. The default value is initialized
     * from `[UIApplication sharedApplication]`.
     */
    var layoutDirection = UIUserInterfaceLayoutDirection(rawValue: 0)
    
    /**
     *  Creates and returns a new instance of `MessagesBubbleImageFactory` that uses the
     *  default bubble image assets, cap insets, and layout direction.
     *
     *  @return An initialized `MessagesBubbleImageFactory` object.
     */
    convenience override init() {
        self.init(bubble: UIImage.msg_bubbleCompactImage(), capInsets: UIEdgeInsets.zero, layoutDirection: UIApplication.shared.userInterfaceLayoutDirection)
    }
    
    /**
     *  Creates and returns a new instance of `MessagesBubbleImageFactory` having the specified
     *  bubbleImage and capInsets. These values are used internally in the factory to produce
     *  `MessagesBubbleImage` objects.
     *
     *  @param bubbleImage A template bubble image from which all images will be generated.
     *  The image should represent the *outgoing* message bubble image, which will be flipped
     *  horizontally for generating the corresponding *incoming* message bubble images. This value must not be `nil`.
     *
     *  @param capInsets   The values to use for the cap insets that define the unstretchable regions of the image.
     *  Specify `UIEdgeInsetsZero` to have the factory create insets that allow the image to stretch from its center point.
     *
     *  @return An initialized `MessagesBubbleImageFactory`.
     */
    
    required init(bubble bubbleImage: UIImage, capInsets: UIEdgeInsets, layoutDirection: UIUserInterfaceLayoutDirection) {
        super.init()
        
        self.bubbleImage = bubbleImage
        self.capInsets = capInsets
        self.layoutDirection = layoutDirection
        if UIEdgeInsetsEqualToEdgeInsets(capInsets, UIEdgeInsets.zero) {
            self.capInsets = self.msg_centerPointEdgeInsets(forImageSize: bubbleImage.size)
        }
    }
    
    /**
     *  Creates and returns a `MessagesBubbleImage` object with the specified color for *outgoing* message image bubbles.
     *  The `messageBubbleImage` property of the `MessagesBubbleImage` is configured with a flat bubble image, masked to the given color.
     *  The `messageBubbleHighlightedImage` property is configured similarly, but with a darkened version of the given color.
     *
     *  @param color The color of the bubble image in the image view. This value must not be `nil`.
     *
     *  @return An initialized `MessagesBubbleImage` object.
     */
    func outgoingMessagesBubbleImage(with color: UIColor) -> MessagesBubbleImage {
        return self.msg_messagesBubbleImage(with: color, flippedForIncoming: (false != self.isRightToLeftLanguage()))
    }
    
    /**
     *  Creates and returns a `MessagesBubbleImage` object with the specified color for *incoming* message image bubbles.
     *  The `messageBubbleImage` property of the `MessagesBubbleImage` is configured with a flat bubble image, masked to the given color.
     *  The `messageBubbleHighlightedImage` property is configured similarly, but with a darkened version of the given color.
     *
     *  @param color The color of the bubble image in the image view. This value must not be `nil`.
     *
     *  @return An initialized `MessagesBubbleImage` object.
     */
    func incomingMessagesBubbleImage(with color: UIColor) -> MessagesBubbleImage {
        return self.msg_messagesBubbleImage(with: color, flippedForIncoming: (true != self.isRightToLeftLanguage()))
    }
}

//MARK:- Private method
extension MessagesBubbleImageFactory{
    func isRightToLeftLanguage() -> Bool {
        return (self.layoutDirection == .rightToLeft)
    }
    
    func msg_centerPointEdgeInsets(forImageSize bubbleImageSize: CGSize) -> UIEdgeInsets {
        // make image stretchable from center point
        let center = CGPoint(x: CGFloat(bubbleImageSize.width / 2.0), y: CGFloat(bubbleImageSize.height / 2.0))
        return UIEdgeInsetsMake(center.y, center.x, center.y, center.x)
    }
    
    func msg_messagesBubbleImage(with color: UIColor, flippedForIncoming: Bool) -> MessagesBubbleImage {
        var normalBubble = self.bubbleImage.msg_imageMasked(with: color)
        var highlightedBubble = self.bubbleImage.msg_imageMasked(with: color.msg_colorByDarkeningColor(withValue: 0.12))
        if flippedForIncoming {
            normalBubble = self.msg_horizontallyFlippedImage(from: normalBubble)
            highlightedBubble = self.msg_horizontallyFlippedImage(from: highlightedBubble)
        }
        
        normalBubble = self.msg_stretchableImage(from: normalBubble, withCapInsets: self.capInsets)
        highlightedBubble = self.msg_stretchableImage(from: highlightedBubble, withCapInsets: self.capInsets)
        
        // Test  on image
//        normalBubble = normalBubble.tint(tintColor: UIColor.black)
        
        let messageBubble:MessagesBubbleImage! = MessagesBubbleImage(messageBubble: normalBubble, highlightedImage: highlightedBubble)
        return messageBubble
    }
    
    func msg_horizontallyFlippedImage(from image: UIImage) -> UIImage {
        return UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: .upMirrored)
    }
    
    func msg_stretchableImage(from image: UIImage, withCapInsets capInsets: UIEdgeInsets) -> UIImage {
        return image.resizableImage(withCapInsets: capInsets, resizingMode: .stretch)
    }
}
