//
//  BaseApp.swift
//
//

import Foundation
import UIKit
import Reachability
//import NotificationBannerSwift
import CoreLocation
import Photos

enum LoginType:String{
    case NONE
    case FACEBOOK
    case GMAIL
    case REGISTER_USER
}

class BaseApp : NSObject{
    
    //Variable declaration
    static let sharedInstance = BaseApp()
    internal static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var networkAlert:UIAlertController?
    var jobManager:OperationQueue?
    var isCommonAlertControllerAlreadyPresented = false
    var loginType = LoginType.NONE
    
    // Reachability instance for Network status monitoring
    let reachability = Reachability()!
    
    var DIGIT_PATTERN = "01234567890"
    
    var isNetworkConnected = false
    var isInfoLoaded = false
    var currentChatRoomId:String?
    
    //For location update
    fileprivate var scheduler:Scheduler?
    
    //MARK: - Basic Init setup
    override init () {
        super.init()
        initJobManager()
    }
    
    func initJobManager() {
        jobManager = OperationQueue()
        jobManager?.maxConcurrentOperationCount = 50
        jobManager?.qualityOfService = QualityOfService.background
    }
    
    /// Called whenever there is a change in NetworkReachibility Status
    ///
    /// — parameter notification: Notification with the Reachability instance
    @objc func handleNetworkChangeEvent(_ notification: Foundation.Notification!) -> Void {
        
        let reachability = notification.object as! Reachability
        
        switch reachability.connection {
        case .none:
            debugPrint("Network became unreachable")
            isNetworkConnected = false
            showNetworkNotAvailableAlertController()
        case .wifi:
            debugPrint("Network reachable through WiFi")
            isNetworkConnected = true
            BaseApp.appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            showScreenInfo()
            
        case .cellular:
            debugPrint("Network reachable through Cellular Data")
            isNetworkConnected = true
            BaseApp.appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func showScreenInfo(){
        if(!isInfoLoaded){
            isInfoLoaded = true
            SwiftEventBus.post(AppConstant.kRefreshChatList)
           // self.refreshLocation()
        }
    }
    
    func showNetworkNotAvailableAlertController(){
        //if(networkAlert == nil){
            let errorTitle = getMessageForCode("Network not avaiable.", fileName: "Strings")
            let errorMessage = getMessageForCode("Please check your connection and try again.", fileName: "Strings")
            networkAlert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
            let defaultAction = UIAlertAction(title: getMessageForCode("ok", fileName: "Strings")
                , style: .default, handler: nil)
            networkAlert?.addAction(defaultAction)
            BaseApp.appDelegate.window?.rootViewController?.present(networkAlert!, animated: true, completion: nil)
        //}
    }
    
    func showVersionUpdateAvailableAlertController(){
        OperationQueue.main.addOperation() {
            self.hideProgressHudView()
            if(self.networkAlert == nil){
                let errorTitle = self.getMessageForCode("appUpdateTitle", fileName: "Strings")
                let errorMessage = self.getMessageForCode("newVersion", fileName: "Strings")
                self.networkAlert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
                BaseApp.appDelegate.window?.rootViewController?.present(self.networkAlert!, animated: true, completion: nil)
            }
        }
        
    }
    
    func showTokenExpireAlertController(_ errorView:APIResponseParam.Error?){
        OperationQueue.main.addOperation() {
            self.hideProgressHudView()
            if(self.networkAlert == nil){
               // self.openLoginViewController()
                let errorTitle = self.getMessageForCode("sessionExpireTitle", fileName: "Strings")
                let errorMessage = errorView?.message!
                //                self.networkAlert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
                //                BaseApp.appDelegate.window?.rootViewController?.present(self.networkAlert!, animated: true, completion: nil)
                self.showAlertViewControllerWith(title: errorTitle, message: errorMessage!, buttonTitle: nil, controller: nil)
                
                //                UIView.animate(withDuration: 0.5, animations: {}, completion: { status in
                //
                //                })
                
            }
        }
    }
    
    //MARK:- Check Network status
    /// Starts monitoring the network availability status
    func startMonitoring() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleNetworkChangeEvent),
                                               name: Notification.Name.reachabilityChanged,
                                               object: reachability)
        do{
            try reachability.startNotifier()

            // reconnect socket
            if(reachability.connection == .cellular || reachability.connection == .wifi){
                //SocketManager.sharedInstance.reConnectSocket()
                connectSocket()
            }
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }
    
    /// Stops monitoring the network availability status
    func stopMonitoring(){
        
        reachability.stopNotifier()
        disconnectSocket()
        NotificationCenter.default.removeObserver(self,
                                                  name: Notification.Name.reachabilityChanged,
                                                  object: reachability)
    }
    
    func connectSocket(){
        print("Socket is conneccted")
        
        /*if !SocketManager.sharedInstance.isConnected() {
         SocketManager.sharedInstance.connectSocket([SocketEvents.COMMON_CHAT_ALERT])
         }*/
        
        if !SocketManager.sharedInstance.isConnected() {
           
            SocketManager.sharedInstance.connectSocket([SocketEvents.COMMON_ON_UPDATE_CHAT, SocketEvents.COMMON_ON_SINGLE_CHAT, SocketEvents.COMMON_ON_LAST_CHAT, SocketEvents.COMMON_ON_CLEAR_CHAT, SocketEvents.COMMON_ON_MSG_DELETE_CHAT, SocketEvents.COMMON_ON_NOTIFY_LAST_MSG, SocketEvents.COMMON_ON_GET_PRIVATE_USER_INFO, SocketEvents.COMMON_ON_PREVOIUS_MORE, SocketEvents.COMMON_ON_GET_CHAT_USER_GROUP_INFO, SocketEvents.COMMON_ON_NOTIFY_LAST_SEEN_STATUS, SocketEvents.COMMON_ON_USER_ONLINE_OFFLINE_STATUS, SocketEvents.COMMON_ON_TALK_ROOM_CONNECTED, SocketEvents.COMMON_ON_NOTIFY_INCOMING_CALL , SocketEvents.COMMON_ON_GROUP_DELETETED])
            
            //For Location Update
         //   self.scheduler = Scheduler.init(delay: 3600, callBackMethod: refreshLocation)
            
            let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                //Added on 4Jan2018 for online offline status
                
                if let isLogin = AppTheme.getIsLogin() as? Bool {
                    if isLogin == true {
                        if ApplicationPreference.getMongoUserId() != nil {
                            let userid = ApplicationPreference.getMongoUserId()!
                            SocketManager.sharedInstance.emitData(SocketEvents.COMMON_ON_ONLINE, data: "\(userid)" as AnyObject)
                        }
                    }
                }
            }
        }
    }
    
    func disconnectSocket(){
        //For Location Update
        scheduler?.stopScheduler()
        scheduler = nil
        //Added on 4Jan2018 for online offline status
        if ApplicationPreference.getMongoUserId() != nil {
            let userid = ApplicationPreference.getMongoUserId()!
            SocketManager.sharedInstance.emitData(SocketEvents.COMMON_ON_OFFLINE, data: "\(userid)" as AnyObject)
        }
        SocketManager.sharedInstance.disconnectSocket()
    }
    
    //MARK:- Show common alert view controller
    func showAlertViewControllerWith(title:String?, message:String, buttonTitle:String?, controller:UIViewController?){ //TODO
                // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
//        let banner = NotificationBanner(title: title!, subtitle: message, style: .success)
//        banner.backgroundColor = UIColor.white
//        banner.titleLabel?.textColor = UIColor(hexString: ColorCode.titleTextColor)
//        banner.subtitleLabel?.textColor = UIColor(hexString: ColorCode.descTextColor)
//        banner.show()
    }
    
    /**
     * For showing error dialog if result from an event is false
     * @param title Dialog Title
     * @param error Error message
     */
    func showRestErrorNeutralDialog(title:String, error:ErrorModel?) {
        let errorTitle = title
        let errorMessage = error?.errorMsg
        networkAlert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
        let defaultAction = UIAlertAction(title: getMessageForCode("ok", fileName: "Strings")
            , style: .default, handler: nil)
        networkAlert?.addAction(defaultAction)
        BaseApp.appDelegate.window?.rootViewController?.present(networkAlert!, animated: true, completion: nil)
    }
    
    //MARK: - Show/Hide HudView(loader)
    func showProgressHudViewWithTitle(title:String?){
        if( reachability.isReachable){
            let loadingNotification = MBProgressHUD.showAdded(to: BaseApp.appDelegate.window!, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text =  title!
            loadingNotification.label.textColor = UIColor.black
            loadingNotification.alpha = 1.0
            loadingNotification.label.font = FontsConfig.FontHelper.defaultLightFontWithSize(FontsConfig.FONT_SIZE_NORMAL)
        }
    }
    
    func hideProgressHudView(){
        MBProgressHUD.hide(for: BaseApp.appDelegate.window!, animated: true)
    }
    
    func removeOptionalString(_ convertText:String) -> String{
        var convertedString = convertText
        convertedString = convertedString.replacingOccurrences(of: "Optional(", with: "")
        convertedString = convertedString.replacingOccurrences(of: ")", with: "")
        return convertedString
    }
    
    func convertDicToStr(_ dict: NSDictionary) -> String{
        do{
            if let data = try JSONSerialization.data(withJSONObject: dict, options: []) as? Data {
                if let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return json as String
                }
            }
        } catch let error as NSError {
            AppConstant.klogString(error.localizedDescription)
        }
        
        return ""
    }
    
    //MARK:- Load value from string file to show text in diffrent language
    func getMessageForCode(_ constantName:String, fileName:String) -> String? {
        return NSLocalizedString(constantName, tableName: fileName, bundle: Bundle.main, value: "", comment: "")
    }
    
    //    func getViewController(storyboardName:String, viewControllerName:String) -> BaseViewController {
    //
    //        //            let appDelegate  = UIApplication.shared.delegate
    //        //            let viewController = appDelegate?.window??.rootViewController
    //        //
    //        //
    //        //            let storyBoard = UIStoryboard (
    //        //                name: "ChallengeStoryboard", bundle: Bundle(for: CardholderAuthenticationViewController.self)
    //        //            )
    //        //            let cardHolderAuthenticationViewController = storyBoard.instantiateInitialViewController() as! CardholderAuthenticationViewController
    //
    //
    //        //let storyBoard = UIStoryboard (name: storyboardName, bundle: Bundle(for: viewController))
    //        let storyBoard = UIStoryboard(name: storyboardName, bundle: nil)
    //        let viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerName) as! BaseViewController
    //        return viewController
    //    }
    
    
//    func getSideMenuAfterLogin() -> NSMutableArray {
//
//        var arrayController : NSMutableArray =  []
//
//        let dict1: NSMutableDictionary = ["Name": "Home", "image": (UIImage(named: "Home"))! as UIImage, "ControllerName": "DashboardViewController"]
//        let dict2: NSMutableDictionary = ["Name": "Search Panditg", "image": (UIImage(named: "Home"))! as UIImage, "ControllerName": "SearchPanditGViewController"]
//        let dict3: NSMutableDictionary = ["Name": "My Favourite", "image": (UIImage(named: "Home"))! as UIImage, "ControllerName": "MyFavouriteViewController"]
//        let dict4: NSMutableDictionary = ["Name": "Articles", "image": UIImage(named: "Home")! as UIImage, "ControllerName": "ArticleViewController"]
//        let dict5: NSMutableDictionary = ["Name": "My Account", "image": UIImage(named: "Home")! as UIImage, "ControllerName": "ProfileViewController"]
//
//        arrayController = [dict1,dict2,dict3,dict4,dict5]
//        return arrayController
//    }
    
    //MARK:- Customize Navigation bar
    func setUpNavigationBarCustomViews(buttonImage:String,iconImage:String?, title:String, description:String?,navigationItem:UINavigationItem,navigationBarLeftButtonAction:Selector, navigationBarDetailButtonAction:Selector?, navigationBarRightButtonTitle:String?, navigationBarRightButtonAction:Selector?, target:UIViewController, navigationHide: Bool) {
        
        //Left view setup
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
        // TODO    // statusBar.backgroundColor = UIColor.init(colorLiteralRed: 255/255.0, green: 193/255.0, blue: 7/255.0, alpha: 1.0)
        }
        UIApplication.shared.statusBarStyle = .lightContent
        
        
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: (BaseApp.appDelegate.window?.frame.size.width)!, height: 44))
   // TODO  //  customView.backgroundColor = UIColor.init(colorLiteralRed: 255/255.0, green: 193/255.0, blue: 7/255.0, alpha: 1.0)
        target.navigationController?.isNavigationBarHidden = navigationHide
        
        //button-back
        let buttonBack = UIButton(type: UIButtonType.custom)
        buttonBack.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        buttonBack.setImage(UIImage(named:buttonImage), for: .normal)
        buttonBack.addTarget(target, action:Selector(NSStringFromSelector(navigationBarLeftButtonAction)), for: UIControlEvents.touchUpInside)
        buttonBack.backgroundColor = UIColor.clear
        customView.addSubview(buttonBack)
        
        // button- detail
        if(navigationBarDetailButtonAction != nil){
            let recognizer = UITapGestureRecognizer(target: target, action: Selector(NSStringFromSelector(navigationBarDetailButtonAction!)))
            customView.isUserInteractionEnabled = true
            customView.addGestureRecognizer(recognizer)
        }
        
        //icon image
        var marginX:CGFloat?
        if(iconImage != nil){
            let imageViewIcon = UIImageView(frame: CGRect(x: buttonBack.frame.size.width + 3, y: 4, width: 36, height: 36))
            imageViewIcon.backgroundColor = UIColor.gray
            //imageViewIcon.image = UIImage(named: iconImage!)
            //  imageViewIcon.setImageWith(NSURL(string: (iconImage!)) as! URL)
            imageViewIcon.layer.cornerRadius = imageViewIcon.frame.size.width/2
            imageViewIcon.clipsToBounds = true
            customView.addSubview(imageViewIcon)
            marginX = CGFloat(imageViewIcon.frame.origin.x + imageViewIcon.frame.size.width + 8)
        }else{
            marginX = CGFloat(buttonBack.frame.origin.x + buttonBack.frame.size.width + 3)
        }
        
        // add right navigation text button
        var rightButtonWidth:CGFloat = 0
        if(navigationBarRightButtonTitle != nil && navigationBarRightButtonAction != nil){
            let buttonRight = UIButton(type: UIButtonType.custom)
            let stringRect = navigationBarRightButtonTitle?.boundingRect(with: CGSize(width: CGFloat(CGFloat.greatestFiniteMagnitude), height:44), options: ([.usesLineFragmentOrigin, .usesFontLeading]), attributes: [NSAttributedStringKey.font: FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_NORMAL)], context: nil)
            rightButtonWidth = ceil(stringRect?.width ?? 0) + 10
            let rectFrame = customView.frame
            buttonRight.frame = CGRect(x: rectFrame.size.width - (rightButtonWidth + 10), y: 0, width: rightButtonWidth, height: 44)
            buttonRight.addTarget(target, action:Selector(NSStringFromSelector(navigationBarRightButtonAction!)), for: UIControlEvents.touchUpInside)
            buttonRight.titleLabel?.font =  FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_NORMAL)
            buttonRight.setTitle(navigationBarRightButtonTitle, for: UIControlState.normal)
            buttonRight.setTitleColor(UIColor.blue , for: UIControlState.normal) //TODO color chnage
            customView.addSubview(buttonRight)
        }
        
        //lable for title and description
        if(description != nil){
            let labelTitle = UILabel(frame: CGRect(x: marginX!, y: 8, width: (customView.frame.size.width - marginX! - 25) - rightButtonWidth, height: 20))
            labelTitle.text = title
            labelTitle.font = FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_MEDIUM)
            labelTitle.textColor = UIColor.black // TODO
            labelTitle.textAlignment = NSTextAlignment.left
            labelTitle.backgroundColor = UIColor.clear
            customView.addSubview(labelTitle)
            
            let labelDescription = UILabel(frame: CGRect(x: labelTitle.frame.origin.x, y: 24, width: labelTitle.frame.size.width, height: 15))
            labelDescription.text = description
            labelDescription.font = FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_SMALL)
            labelDescription.textColor = UIColor.black // TODO
            labelDescription.textAlignment = NSTextAlignment.left
            labelDescription.backgroundColor = UIColor.clear
            customView.addSubview(labelDescription)
        }else{
            
            let labelTitle = UILabel(frame: CGRect(x: marginX!, y: 13, width: (customView.frame.size.width - marginX! - 25) - rightButtonWidth, height: 20))
            labelTitle.text = title
            labelTitle.font = FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_MEDIUM)
            labelTitle.textColor = UIColor.black // TODO
            labelTitle.textAlignment = NSTextAlignment.left
            labelTitle.backgroundColor = UIColor.clear
            customView.addSubview(labelTitle)
        }
        
        let leftButton = UIBarButtonItem(customView: customView)
        
        //left side blank spave filler
        let leftSidePaddingFiller = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        leftSidePaddingFiller.width = -16
        navigationItem.leftBarButtonItems = [leftSidePaddingFiller,leftButton]
    }
    
    //MARK:- Date/Time related methods
    func getCurrentTimeMilliSeconds() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
    
    func getTimeStampFromSecond(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3600
        
        var timeStamp = ""
        if(hours != 0 && minutes != 0){
            timeStamp = String(format: "%02d h:%02d m", hours, minutes)
        }else if(hours == 0 && minutes != 0){
            timeStamp = String(format: "%02d m",minutes)
        }else if(minutes == 0 && seconds != 0){
            timeStamp = String(format: "%02d s",seconds)
        }else if(seconds == 0){
            timeStamp = String(format: "0 s")
        }
        return timeStamp
    }
    
    /**
     * @param phone number to verify and validate
     * @return Mobile number if able to parse and its type of a valid MOBILE including +91, otherwise null
     */
//    func parseAndReturnValidMobileNumber(phone:String) -> String?{
//        let phoneUtil = PhoneNumberKit()
//        let iso = getDeviceCountryISO() // Default ISO (e.g.,"IN" for India)
//        do{
//            let number:PhoneNumber = try phoneUtil.parse(phone, withRegion: iso!)
//            let numberType:PhoneNumberType = number.type
//            if(numberType == PhoneNumberType.mobile || numberType == PhoneNumberType.fixedOrMobile){
//                return phoneUtil.format(number, toType: PhoneNumberFormat.e164)
//            }
//
//        } catch let error as NSError {
//            AppConstant.kLogString(error)
//        }
//
//        return nil
//    }
    
   // func getDeviceCountryISO() -> String?{// TODO getDeviceCountryISO
        
        //fetch the device locality info and get the Country code detailed info
       // let countryDetails = getDeviceLocality()
        
        //set fetched country info as by default to UI
       // let countryName = countryDetails.object(forKey: AppConstant.localCountryCode) as? String
        
       // return countryName
  //  }
    
    //MARK:- Country Code Fetching methods
  //  func getDeviceLocality() -> NSDictionary{ //TODO
//        let locale = Locale.current
//        var countryShortCode = "IN"
//        if(locale.regionCode != nil){
//            countryShortCode = locale.regionCode!
//        }
//       // let countryDetails = BaseApp.sharedInstance.getCountryLocalityDetailsFor(countryCode: countryShortCode)
//        return countryDetails
   // }
    
//    func getCountryLocalityDetailsFor(countryCode:String) -> NSDictionary{
//        let countties  = self.getCountryListWithCountryCode()
//        var defaultCountry = ["name":"India","dial_code":"+91","code":"IN"]
//        for country in countties{
//            if((country as! NSDictionary).value(forKey: "code") as! String == countryCode){
//                defaultCountry = country as! [String : String]
//            }
//        }
//        AppConstant.kLogString(defaultCountry)
//        return defaultCountry as NSDictionary
//    }
//
   
    
    //MARK:- Remove Optional from string
    func removeOptionalWordFromString(_ convertText:String) -> String{
        var convertedString = convertText
        convertedString = convertedString.replacingOccurrences(of: "Optional(", with: "")
        convertedString = convertedString.replacingOccurrences(of: ")", with: "")
        return convertedString
    }
    
    func parsePhoneString(phone:String) -> Int64{
        //        try {
        //        String validDigits = OTHER_THAN_DIGIT_PATTERN.matcher(phone.trim()).replaceAll("");
        //        return Long.parseLong(validDigits);
        //        } catch (Exception ignore) {
        //        log.warn("parsePhoneString failed: " + phone, ignore);
        //        }
        //        return -1L;
        
        let result = String(phone.characters.filter { DIGIT_PATTERN.characters.contains($0) })
        return Int64(result) ?? 0
    }
    
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    func showShortToast(_ message:String, onView:UIView){ //TODO
        // default percentage
//        ToastManager.shared.style.maxWidthPercentage = 1.0
//        ToastManager.shared.style.messageFont = FontsConfig.FontHelper.defaultLightFontWithSize(FontsConfig.FONT_SIZE_NORMAL)
//        onView.makeToast(message, duration: 0.9, position: CGPoint(x:onView.center.x, y: 90))
       // BaseApp.appDelegate.window?.rootViewController?.navigationController?.view.makeToast(message, duration: 0.9, position: .top)
    }
    
    
    //MARK:- Check if user if already logged in to application
//    func isUserAlreadyLoggedInToApplication() -> Bool{
//        let loginInfo = ApplicationPreference.getLoginInfo()
//        if(loginInfo != nil){
//            return true
//        }
//        return false
//    }
    
    //MARK:- Calling
    func phoneCall(phone:String){
        
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: "Your device doesn't support calling.", buttonTitle: "OK", controller: BaseApp.appDelegate.window?.rootViewController?.navigationController)
        }
    }
    
    // MARK:- Update Total chat count
    func updateChatCount(roomId:String){
        var chatCountList:[String:String]? = ApplicationPreference.getChatCountInfo()
        if(chatCountList != nil){
            var chatCount:String? = chatCountList?[roomId]
            if(chatCount == nil){
                chatCount = "1"
            } else {
                chatCount = "\(Int(chatCount!)!+1)"
            }
            
            chatCountList?.updateValue(chatCount!, forKey: roomId)
            
        } else { // first time adding info
            chatCountList = [:]
            chatCountList?.updateValue("1", forKey: roomId)
        }
        
        ApplicationPreference.saveChatCountInfo(userInfo: chatCountList!)
        
        // update Chat count total
        if(chatCountList != nil){
            var totalChatCount = 0
            for (_,value) in chatCountList!{
                totalChatCount += Int(value)!
            }
            
            if(totalChatCount > 0){
                ApplicationPreference.saveTotalChatCount(totalCout: "\(totalChatCount)")
            }
        }
        
        SwiftEventBus.postToMainThread(AppConstant.kTotalChatCount)
    }
    
    func removeChatCount(roomId:String){
        var chatCountList:[String:String]? = ApplicationPreference.getChatCountInfo()
        if(chatCountList != nil){
            var chatCount:String? = chatCountList?[roomId]
            if(chatCount != nil){
                chatCount = "0"
                chatCountList?.updateValue(chatCount!, forKey: roomId)
            }
            ApplicationPreference.saveChatCountInfo(userInfo: chatCountList!)
        }
        
        // update Chat count total
        if(chatCountList != nil){
            var totalChatCount = 0
            for (_,value) in chatCountList!{
                totalChatCount += Int(value)!
            }
            
            if(totalChatCount > 0){
                ApplicationPreference.saveTotalChatCount(totalCout: "\(totalChatCount)")
            } else {
                ApplicationPreference.removeTotalChatCount()
            }
        }
        
        SwiftEventBus.postToMainThread(AppConstant.kTotalChatCount)
    }
    
    func getChatCount(roomId:String) -> String?{
        
        var chatCountList:[String:String]? = ApplicationPreference.getChatCountInfo()
        if(chatCountList != nil){
            let chatCount:String? = chatCountList?[roomId]
            if(chatCount != nil){
                return chatCount
            }
        }
        return nil
    }
    
    // customize label to show green icon on it, instead of using uiimageview control
    func convertSimpleTextToAttributedTextIcon(_ textStr:String, isIconLeftSide:Bool, isIconAdd:Bool, iconName:String? = nil, iconOffset:CGPoint? = CGPoint.zero, iconFixedSize:CGSize? = CGSize.zero) -> NSMutableAttributedString?{
        
        let attachment = NSTextAttachment()
        let attributeStr = NSMutableAttributedString(string:textStr)
        var attachmentString:NSMutableAttributedString? = nil
        var myImage:UIImage? = nil
        var myImageSize:CGSize = CGSize.zero
        
        if(iconName != nil){
            myImage = UIImage(named: iconName!)
        }
        
        if(myImage != nil){
            attachment.image = myImage
            
            if((iconFixedSize?.width ?? 0) > 0 || (iconFixedSize?.height ?? 0) > 0){
                myImageSize = iconFixedSize!
            }else{
                myImageSize = myImage!.size
            }
            attachment.bounds = CGRect(origin: iconOffset!, size: myImageSize)
        }
        
        if(isIconAdd){
            if(isIconLeftSide){
                attachmentString = NSMutableAttributedString(attributedString: NSMutableAttributedString(string: "   "))
                attachmentString?.append(NSAttributedString(attachment: attachment))
                attachmentString!.append(NSMutableAttributedString(string: " "))
                attachmentString!.append(attributeStr)
            }else{
                attachmentString = NSMutableAttributedString(attributedString: attributeStr)
                attachmentString!.append(NSMutableAttributedString(string: "  "))
                attachmentString?.append(NSAttributedString(attachment: attachment))
            }
            
        }else{
            attachmentString = NSMutableAttributedString(attributedString: attributeStr)
        }
        
        return attachmentString ?? nil
    }
    
    //View Controller
    func openDashboardViewControllerOnSkip(){
        //    TODO -   BaseApp.appDelegate.isAllowSkip = true
     //   BaseApp.appDelegate.setUpApplicationRootView()
    }
    
    
    func getViewController<T>(storyboardName:String, viewControllerName:String) -> T {
        let storyBoard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerName) as! T
        return viewController
    }
    
    //
    func getShortName(strFullName: String) -> String {
        let nameData = strFullName.capitalized
        let nameArr = nameData.components(separatedBy: " ")
        let firstName = nameArr[0]
        var lastName = ""
        if nameArr.count > 1 {
            lastName = nameArr[1]
        }
        let strName = (firstName.first?.description ?? "") + (lastName.first?.description ?? "")
        return strName
    }
    
    func getOnlineUserNameWithImage(strFullName: String) -> NSMutableAttributedString {
        // create an NSMutableAttributedString that we'll append everything to
        let fullString = NSMutableAttributedString(string: strFullName.capitalized)
        fullString.append(NSAttributedString(string: "\n"))
        // create our NSTextAttachment
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = #imageLiteral(resourceName: "green-bullet")
        // wrap the attachment in its own attributed string so we can append it
        let image1String = NSAttributedString(attachment: image1Attachment)
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        fullString.append(image1String)
        fullString.append(NSAttributedString(string: " Online"))
        return fullString
    }
    
    func getMsgTextWithImage(strFullName: String, msgText: String, msgType: String) -> NSMutableAttributedString {
        // create an NSMutableAttributedString that we'll append everything to
        let fullString = NSMutableAttributedString(string: strFullName.capitalized)
        // create our NSTextAttachment
        let image1Attachment = NSTextAttachment()
        if msgType == "text" {
            image1Attachment.image = UIImage(named: "")
        }
//        else if msgType == "image" {
//            image1Attachment.image = #imageLiteral(resourceName: "ic-picture")
//        } else if msgType == "location" {
//            image1Attachment.image = #imageLiteral(resourceName: "location")
//        } else if msgType == "audio" {
//            image1Attachment.image = #imageLiteral(resourceName: "ic-earphones")
//        }
//
        else {
            image1Attachment.image = UIImage(named: "")
        }
//        Roboto-Regular
//        fullString.append(NSAttributedString(string: " "))
        if msgType != "text" {
            image1Attachment.bounds = CGRect(x: 0, y: -03, width: 15, height: 15)
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
        }
        fullString.append(NSAttributedString(string: " "))
        fullString.append(NSAttributedString(string: msgText))
        return fullString
    }
    
   /* func getMsgTextWithSeenStatusAndImage(strTime: String, msgType: Bool, isGroup: Bool, msgStatus: String) -> NSMutableAttributedString {
        // create an NSMutableAttributedString that we'll append everything to
        let attr: [NSAttributedStringKey: AnyObject] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Helvetica", size: 10)!]
        let fullString = NSMutableAttributedString(string: strTime, attributes: attr)
        
        //let fullString = NSMutableAttributedString(string: strTime)
        // create our NSTextAttachment
        let image1Attachment = NSTextAttachment()
        if msgStatus == "send" {
            image1Attachment.image = #imageLiteral(resourceName: "mark_gray")
        } else if msgStatus == "delivered" {
            image1Attachment.image = #imageLiteral(resourceName: "mark_black")
        } else {
            image1Attachment.image = #imageLiteral(resourceName: "mark_blue")
        }
        
        if isGroup == false {
            fullString.append(NSAttributedString(string: " "))
            image1Attachment.bounds = CGRect(x: 0, y: 0, width: 15, height: 15)
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
        }
        return fullString
    }*/
    
    func getMsgTextWithSeenStatusAndImage(strTime: String, isGroup: Bool) -> NSMutableAttributedString {
        
        let attr: [NSAttributedStringKey: AnyObject] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont(name: "Helvetica", size: 10)!]
        let fullString = NSMutableAttributedString(string: strTime, attributes: attr)
     
        return fullString
    }
}

extension BaseApp{
    //View Controller
    func openDashboardViewController(){
       // BaseApp.appDelegate.setUpApplicationRootView()
    }

    func openLoginViewController(){
       // ApplicationPreference.clearAllData()
       // BaseApp.appDelegate.setUpApplicationRootView()
    }
    
    
    func getCurentTime() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.hour,.minute,.second], from: date)
        
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
        
    }
    
    
    // SAVE IMAGE
    
    func getVideo(filepath:String) -> String {

        let fileManager = FileManager.default
         if fileManager.fileExists(atPath: filepath){
            return String("\(filepath)")
        }else{
            print("No Video")
            return String("\(filepath)")
        }
    }
    
    func getVideoData(filepath:String) -> NSData {
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filepath){
            return NSData(contentsOfFile: filepath)!
        }else{
            print("No Image")
            return NSData()
        }
    }
    
    func getImage(filepath:String) -> UIImage {
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filepath){
            return UIImage(contentsOfFile: filepath)!
        }else{
            print("No Image")
            return UIImage()
        }
    }
    
    func getDefaultFolderPath() -> URL?{
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let path = URL(string: documentsDirectory)
        
        return path ?? nil
    }
    
    func getLocalMediaFileFolderPath(imageName:String) -> URL?{
        let documentsDirectory = getDefaultFolderPath()?.appendingPathComponent(imageName).path
        let path = URL(string: documentsDirectory!)
        return path ?? nil
    }
    
    func saveImageToDocumentDirectory(_ chosenImage: UIImage) -> String {
        
        let filename = getCurentTime().appending(".jpg")
        
        let directoryPath = getDefaultFolderPath()
        
        let filepath = directoryPath?.appendingPathComponent(filename).path
        let url = NSURL.fileURL(withPath: filepath!)
        do {
            try UIImageJPEGRepresentation(chosenImage, 1.0)?.write(to: url, options: .atomic)
            return String("\(filename)")
            
        } catch {
            print(error)
            print("file cant not be save at path \(filename), with error : \(error)");
            return (directoryPath?.path)!
        }
    }
    
    func saveVideoToDocumentDirectory(urlString :URL) -> String {
        
        let filename = getCurentTime().appending(".mov")

        let videoData = NSData.init(contentsOf: urlString as URL)
        
        let directoryPath = getDefaultFolderPath()
        
        let filepath = directoryPath?.appendingPathComponent(filename).path
        
        let url = NSURL.fileURL(withPath: filepath!)
        
        do {
            try videoData?.write(to: url, options: .atomic)
            return String("\(filename)")
        } catch {
            print(error)
            print("file cant not be save at path \(filename), with error : \(error)");
            return (directoryPath?.path)!
        }
    }
    
}

//extension BaseApp {
//    func refreshLocation(){
//        if(BaseApp.sharedInstance.isNetworkConnected){
//            getCurrentLocation()
//        }
//    }
//    
//    func getCurrentLocation() {
//        if CLLocationManager.locationServicesEnabled() {
//            switch(CLLocationManager.authorizationStatus()) {
//            case .notDetermined, .restricted, .denied:
//                print("No access")
//                BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: "Location services not allowed. Please allow from settings.", buttonTitle: "OK", controller: BaseApp.appDelegate.window?.rootViewController?.navigationController)
//                
//            case .authorizedAlways, .authorizedWhenInUse:
//                print("Access")
//                
//                if ApplicationPreference.getUserId() != nil {
//                    let currentLoc = GetLocation()
//                    currentLoc.getCurrentLocation { result in
//                        
//                        UIView.animate(withDuration: 0.5) {
//                            
//                            print(result.addressDictionary as Any)
//                            
//                            let lat = (result.location?.coordinate.latitude)!
//                            let lang = (result.location?.coordinate.longitude)!
//                            
//                            let geocoder = CLGeocoder()
//                            let location = CLLocation(latitude: lat, longitude: lang)
//                            geocoder.reverseGeocodeLocation(location) {
//                                (placemarks, error) -> Void in
//                                if let placemarks = placemarks, placemarks.count > 0 {
//                                    let placemark = placemarks[0]
//                                    
//                                    var addressString : String = ""
//                                    if placemark.subLocality != nil {
//                                        addressString = addressString + placemark.subLocality! + ", "
//                                    }
//                                    if placemark.thoroughfare != nil {
//                                        addressString = addressString + placemark.thoroughfare! + ", "
//                                    }
//                                    if placemark.locality != nil {
//                                        addressString = addressString + placemark.locality! + ", "
//                                    }
//                                    if placemark.country != nil {
//                                        addressString = addressString + placemark.country! + ", "
//                                    }
//                                    if placemark.postalCode != nil {
//                                        addressString = addressString + placemark.postalCode! + " "
//                                    }
//                                    
//                                    print(addressString)
//                                    if ApplicationPreference.getUserId() != nil {
//                                        
//                                        BaseApp.sharedInstance.showAlertViewControllerWith(title: "Location Updated", message: addressString, buttonTitle: "OK", controller: BaseApp.appDelegate.window?.rootViewController?.navigationController)
//                                        
//                                        let userid = ApplicationPreference.getUserId()!//self.userID!
//                                        
//                                        let dictLocation = NSMutableDictionary()
//                                        dictLocation.setValue("\(lat)", forKey: "lat")
//                                        dictLocation.setValue("\(lang)", forKey: "lang")
//                                        dictLocation.setValue("\(addressString)", forKey: "location")
//                                        dictLocation.setValue("\(userid)", forKey: "userid")
//                                        
//                                        if SocketManager.sharedInstance.isConnected() {
//                                            SocketManager.sharedInstance.emitData(SocketEvents.COMMON_ON_USER_ONLINE_STATUS, data: "\(userid)" as AnyObject)
//                                            SocketManager.sharedInstance.emitDataList(SocketEvents.COMMON_ON_UPDATE_LOCATION, data: [dictLocation])
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        } else {
//            print("Location services are not enabled")
//            BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: "Location services not allowed.Please allow from settings.", buttonTitle: "OK", controller: BaseApp.appDelegate.window?.rootViewController?.navigationController)
//        }
//    }
//}

