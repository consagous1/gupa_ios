//
//  GroupTableViewCell.swift
//  TextApp
//
//  Created by Apple on 11/10/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {

    // All Connections and IBOutlets
    @IBOutlet weak var btnForTitleImg: UIButton!

    @IBOutlet weak var imgIconForTitle: UIImageView!
    
    @IBOutlet weak var lblTitleName: UILabel!
    
    @IBOutlet weak var imgForToggle: UIImageView!
    @IBOutlet weak var btnForToggle: UIButton!
    
    @IBOutlet weak var imgStatus: UIImageView!
    
    var isCellSelected : Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
