//
//  NewGroupViewController.swift
//  TextApp
//
//  Created by Apple on 11/8/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import JGProgressHUD
import Alamofire

class NewGroupViewController: BaseViewController, UISearchBarDelegate, UISearchDisplayDelegate {
    
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet weak var btnForCreateGroup: UIButton!
    @IBOutlet weak var searchBarText: UISearchBar!
    @IBOutlet weak var tblForGroup: UITableView!
    @IBOutlet weak var txtForGrpName: UITextField!
    @IBOutlet weak var bottomConstForTblView: NSLayoutConstraint!
    @IBOutlet weak var btnSelectImage: UIButton!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var arrForNewGroup         = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
    var arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
    
    var arrForSelectedUserChat = NSMutableSet()
    var imagePicker = UIImagePickerController()

    var isSearching = false
    var integerCount = 0
    var chosenImage : UIImage!
    var strGroupImage = ""

    fileprivate var chatViewController:ChatViewController?
    
    //****************************************************
    // MARK: - Life Cycle Methods
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Called Group View Controller")

        self.serverAPI()
        searchBarText.delegate = self
        searchBarText.returnKeyType = UIReturnKeyType.done
        tblForGroup.tableFooterView = UIView()
        
        appDelegate.centerNav = self.navigationController
        
        self.tblForGroup.allowsMultipleSelection = true
        
        //SearchBar Placeholder
        if let txfSearchField = searchBarText.value(forKey: "_searchField") as? UITextField {
            txfSearchField.borderStyle = .none
            txfSearchField.backgroundColor = .clear
        }
        searchBarText.searchTextPositionAdjustment = UIOffsetMake(10.0, 0.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        chatViewController = nil
        
        self.navigationController?.isNavigationBarHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewGroupViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewGroupViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func methodBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func methodNewChatAction(_ sender: Any) {
        
        txtForGrpName.resignFirstResponder()
        
        if arrForSelectedUserChat.count > 0 {
            
            let strText = txtForGrpName.text?.trimmingCharacters(in: CharacterSet.whitespaces)
            if (strText?.isEmpty)! {
                _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter group name.", showInView: self.view)

            } else {
                let arrForUser = self.arrForSelectedUserChat.allObjects as! [APIResponseParam.AllUserListInfo.AllUserListInfoData]

                if(arrForUser.count > 0){
                    var userlist:[APIRequestParam.GroupUserData] = []
                    var userData:APIRequestParam.GroupUserData?
                    for userInfo in arrForUser {
                        userData = APIRequestParam.GroupUserData(_id: userInfo._id ?? "")
                        userlist.append(userData!)
                    }
                    self.serApiForCreateSingleChatGroup(userList: userlist)
                }
            }
        } else {
            BaseApp.sharedInstance.showAlertViewControllerWith(title: "", message: "Please select atleast one person.", buttonTitle: "OK", controller: self)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false;
            
            self.arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
            self.arrForFilteredUserChat = self.arrForNewGroup
            self.tblForGroup.reloadData()
            
        } else {
            isSearching = true
            
            self.arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
            
            for i in 0..<self.arrForNewGroup.count {
                let userData = self.arrForNewGroup[i]
                var strFullName = ""
                if userData.last_name != "" {
                    strFullName = "\(userData.first_name!)" + "\(userData.last_name!)"
                } else {
                    strFullName = "\(userData.first_name!)"
                }
                if (strFullName.lowercased().contains(searchText.lowercased())) {
                    self.arrForFilteredUserChat.append(userData)
                }
            }
            self.tblForGroup.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    //MARK: Keyboard Methods
    @objc func keyboardWillHide(_ sender: Notification) {
        
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                //key point 0,
                self.bottomConstForTblView.constant =  0
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardWillShow(_ sender: Notification) {
        
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.bottomConstForTblView.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
}

extension NewGroupViewController : UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrForFilteredUserChat.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupUserCell", for: indexPath) as! GroupTableViewCell
        
        let userData = arrForFilteredUserChat[indexPath.row]
        
        var strFullName = ""
        var strFullNameImg = ""
        
        if userData.last_name != "" {
            strFullName = "\(userData.first_name!)" + " " + "\(userData.last_name!)"
            strFullNameImg = (userData.first_name?.first?.description ?? "") + (userData.last_name?.first?.description ?? "")
        } else {
            strFullName = "\(userData.first_name!)"
            strFullNameImg = (userData.first_name?.first?.description ?? "")
        }
        cell.lblTitleName.text = strFullName.capitalized
        cell.btnForTitleImg.setTitle(strFullNameImg.uppercased(), for: .normal)
        
        if arrForSelectedUserChat.contains(userData) {
            cell.btnForToggle.setImage(#imageLiteral(resourceName: "radioselect"), for: .normal)
        } else {
            cell.btnForToggle.setImage(#imageLiteral(resourceName: "radio_normal"), for: .normal)
        }
       
//        if userData.online == true {
//            cell.imgStatus.alpha = 1
//        } else {
//            cell.imgStatus.alpha = 0
//        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Row is \(indexPath.row)")
        
        let item = arrForFilteredUserChat[indexPath.row]
        if arrForSelectedUserChat.contains(item) {
            arrForSelectedUserChat.remove(item)
        } else {
            arrForSelectedUserChat.add(item)
        }
        
        if arrForSelectedUserChat.count > 0 {
            btnForCreateGroup.isEnabled = true
        } else {
            btnForCreateGroup.isEnabled = false
        }
        self.tblForGroup.reloadData()
    }
}

extension NewGroupViewController : UINavigationControllerDelegate , UIImagePickerControllerDelegate  {
    
    @IBAction func selectImage_Action(_ sender: Any) {

        presentActionSheet()
    }
    
    //**>> Open Action Sheet On tap |Camera|
    func presentActionSheet() {
        
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertControllerStyle.actionSheet)
        
        settingsActionSheet.addAction(UIAlertAction(title:"Choose Photo", style:UIAlertActionStyle.default, handler:
            {
                action in self.takeImageFromGallery()
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Take Photo", style:UIAlertActionStyle.default, handler:
            {
                action in self.takeImageFromCamera()
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Cancel", style:UIAlertActionStyle.cancel, handler:nil))
        present(settingsActionSheet, animated:true, completion:nil)
    }
    
    func takeImageFromGallery () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            imagePicker.delegate      = self
            imagePicker.sourceType    = UIImagePickerControllerSourceType.savedPhotosAlbum
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func takeImageFromCamera () {
        
        if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
            imagePicker.delegate             = self
            imagePicker.sourceType           = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing        = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //****************************************************
    // MARK: - Image Picker Delegates
    //****************************************************
    
   @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){

        dismiss(animated: true, completion: { () -> Void in

            self.chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            self.btnSelectImage.contentMode = .scaleAspectFit
            self.btnSelectImage.setImage(self.chosenImage, for: .normal)
            print(self.chosenImage)
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")

            self.serverApiForUploadImage()
        })
    }
    
    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK:- API calling
extension NewGroupViewController {
    
    func serverAPI(){
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            
            let allUserRequestParam = APIRequestParam.AllUserListInfo()
            
            let allUserList = AllUserListRequest(token: ApplicationPreference.getAppToken(), userId: ApplicationPreference.getMongoUserId()!, userData: allUserRequestParam, onSuccess: {
                response in
                
                print(response.toJSON())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    var arrUserResponse = response.allUserListInfoData!
                    if let index = arrUserResponse.index(where: { $0._id == ApplicationPreference.getMongoUserId()}) {
                        arrUserResponse.remove(at: index)
                    }
                    
                    self.arrForNewGroup = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
                    self.arrForNewGroup = arrUserResponse
                    self.arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
                    self.arrForFilteredUserChat = arrUserResponse
                    
                    BaseApp.sharedInstance.hideProgressHudView()
                    self.tblForGroup.reloadData()
                }
            }, onError: {
                error in
                print(error.toString())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: "OK", controller: self)
                }
            })
            BaseApp.sharedInstance.jobManager?.addOperation(allUserList)
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }
    
    func serApiForCreateSingleChatGroup(userList: [APIRequestParam.GroupUserData]) {
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            
            let createSingleAndGroupChatRequestParam = APIRequestParam.CreateSingleAndGroupChatInfo(group_icon: strGroupImage, group_name:txtForGrpName.text , is_group: true, addedasfreind: "", lastmsgsentby: AppTheme.getUserFullName(), last_msg: "", friend_name: "", created_by: ApplicationPreference.getMongoUserId()!, timestamp: "\(BaseApp.sharedInstance.getCurrentTimeMilliSeconds())",  status: "1", created_id : "", friendId : "", userLists: userList)
            
            let createSingleAndGroupChat = CreateSingleAndGroupChatRequest(token: ApplicationPreference.getAppToken(), userId: ApplicationPreference.getMongoUserId()!, createSingleAndGroupChatData: createSingleAndGroupChatRequestParam, onSuccess: {
                response in
                
               // print(response.toJSON())
                
                OperationQueue.main.addOperation() {
                    BaseApp.sharedInstance.hideProgressHudView()
                    
                    if(self.chatViewController == nil){
                        let dict = response.toJSON() as NSDictionary
                        self.chatViewController = BaseApp.sharedInstance.getViewController(storyboardName: AppConstant.chatVCStoryboard, viewControllerName: ChatViewController.nameOfClass) as ChatViewController
                        self.chatViewController?.blockId = "\(dict.value(forKey: "data")!)"
                        self.chatViewController?.message_by  = ApplicationPreference.getMongoUserId()
                        self.chatViewController?.sender_name = AppTheme.getUserFullName()
                        self.chatViewController?.userID = ""
                        self.chatViewController?.is_group = true
                        self.chatViewController?.strViewComeFrom = "GROUPCHAT"
                        self.chatViewController?.is_ShowButton = true
                        self.chatViewController?.userName = (self.txtForGrpName.text?.trimmingCharacters(in: CharacterSet.whitespaces))!
                        self.navigationController?.isNavigationBarHidden = true
                        self.navigationController?.pushViewController(self.chatViewController!, animated: true)
                    }
                }
            }, onError: {
                error in
                print(error.toString())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: "OK", controller: self)
                }
            })
            BaseApp.sharedInstance.jobManager?.addOperation(createSingleAndGroupChat)
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }
    
    func serverApiForUploadImage(){
        
        if(BaseApp.sharedInstance.isNetworkConnected){
           //BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            
            OperationQueue.main.addOperation() {
                BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            }
            
            let strUrl = URL(string: "http://52.38.159.211:8080/mobileUpload")
            
            let params = NSMutableDictionary()
            Alamofire.upload(multipartFormData: {  (multipartFormData) in
                
                multipartFormData.append(UIImageJPEGRepresentation(self.chosenImage!, 1.0)!, withName: "file", fileName: "file.jpeg", mimeType: "image/jpeg")
                for (key, value) in params
                {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                }
                
            }, to:strUrl!,headers:nil)
            { (result) in
            switch result {
                
                case .success(let upload,_,_ ):
                    
                    upload.uploadProgress(closure: { (progress) in
                //       BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
                    })
                    
                    upload.responseJSON
                        { response in
                            print("\(response.result)")
                            //print response.result

                            if response.result.value != nil
                            {
                                let dict :NSDictionary = response.result.value! as! NSDictionary
                                let status = "\(dict.value(forKey: "error")!)"
                                if status == "0"
                                {
                                    BaseApp.sharedInstance.hideProgressHudView()

                                    print("DATA UPLOAD SUCCESSFULLY")
                                    self.strGroupImage = "\(dict.value(forKey: "data")!)"
                                    print(self.strGroupImage)
                                }
                            }
                    }
                    BaseApp.sharedInstance.hideProgressHudView()
                case .failure(let encodingError):
                    break
                }
            }
            
        } else {
            BaseApp.sharedInstance.hideProgressHudView()
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }
}



