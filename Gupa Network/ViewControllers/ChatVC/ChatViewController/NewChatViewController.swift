//  NewChatViewController.swift
//  TextApp
//
//  Created by Apple on 11/8/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit

class NewChatViewController: BaseViewController, UISearchBarDelegate, UISearchDisplayDelegate, UITextFieldDelegate {
    
    // All IBOutlets Connections 
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgMarkDOne: UIImageView!
    @IBOutlet weak var markDone: UIButton!
    @IBOutlet weak var tblForPeople: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //****************************************************
    // MARK: - Initialization
    //****************************************************

    fileprivate var chatViewController:ChatViewController?
    var arrForSelectedUserChat = NSMutableSet()
    
    var arrForUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData] ()
    var arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData] ()
    var isSearching = false
    
    //****************************************************
    // MARK: - Life Cycle Methods
    //****************************************************
    
    @IBOutlet weak var bottomConstForTblView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        tblForPeople.tableFooterView = UIView()
        appDelegate.centerNav = self.navigationController
        
        //SearchBar Placeholder
        if let txfSearchField = searchBar.value(forKey: "_searchField") as? UITextField {
            txfSearchField.borderStyle = .none
            txfSearchField.backgroundColor = .clear
        }
        searchBar.searchTextPositionAdjustment = UIOffsetMake(10.0, 0.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        chatViewController = nil
        self.serverAPI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewChatViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewChatViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK: Search bar delegates
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false;
            
            self.arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
            self.arrForFilteredUserChat = self.arrForUserChat
            self.tblForPeople.reloadData()
        } else {
            isSearching = true
            
            self.arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
            for i in 0..<self.arrForUserChat.count {
                let userData = self.arrForUserChat[i]
                var strFullName = ""
                if userData.last_name != "" {
                    strFullName = "\(userData.first_name!)" + "\(userData.last_name!)"
                } else {
                    strFullName = "\(userData.first_name!)"
                }
                if (strFullName.lowercased().contains(searchText.lowercased())) {
                    self.arrForFilteredUserChat.append(userData)
                }
            }
            self.tblForPeople.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    //MARK: Keyboard Methods
    @objc func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                //key point 0,
                self.bottomConstForTblView.constant =  0
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    @objc func keyboardWillShow(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.bottomConstForTblView.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func methodBackAction(_ sender: Any) {
        print("Back Action Called");
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
}

extension NewChatViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrForFilteredUserChat.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatUserCell", for: indexPath) as! chatViewCell
        let userData = arrForFilteredUserChat[indexPath.row]
        
        var strFullName = ""
        var strFullNameImg = ""
        
        if userData.last_name != "" {
            
            strFullName = "\(userData.first_name!)" + " " + "\(userData.last_name!)"
            strFullNameImg = (userData.first_name?.first?.description ?? "") + (userData.last_name?.first?.description ?? "")
        } else {
            strFullName = "\(userData.first_name!)"
            strFullNameImg = (userData.first_name?.first?.description ?? "")
        }
        
//        if userData.online == true {
//            cell.imgaeForStatus.alpha = 1
//        } else {
//            cell.imgaeForStatus.alpha = 0
//        }
        
        cell.lblForTitle.text = strFullName.capitalized
        cell.btnForTitleImg.setTitle(strFullNameImg.uppercased(), for: .normal)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Row is \(indexPath.row)")
        let userDataInfo = self.arrForFilteredUserChat[indexPath.row]

        var userlist:[APIRequestParam.GroupUserData] = []
        var userData:APIRequestParam.GroupUserData?
        
        userData = APIRequestParam.GroupUserData(_id: userDataInfo._id ?? "")
        userlist.append(userData!)
        
        self.serApiForCreateSingleChatGroup(inderPathRow: indexPath.row, userList: userlist)
    }
}

// MARK:- API calling
extension NewChatViewController {
    
    func serverAPI(){
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            
            let allUserRequestParam = APIRequestParam.AllUserListInfo()
            
            let allUserList = AllUserListRequest(token: ApplicationPreference.getAppToken(), userId: ApplicationPreference.getMongoUserId()!, userData: allUserRequestParam, onSuccess: {
                response in
                
                print(response.toJSON())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    self.arrForUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
                    var arrUserResponse = response.allUserListInfoData!
                    
                    if let index = arrUserResponse.index(where: { $0._id == ApplicationPreference.getMongoUserId()}) {
                        arrUserResponse.remove(at: index)
                    }
                   
                    self.arrForUserChat = arrUserResponse
                    self.arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
                    self.arrForFilteredUserChat = arrUserResponse
                    BaseApp.sharedInstance.hideProgressHudView()
                    self.tblForPeople.reloadData()
                }
            }, onError: {
                error in
                print(error.toString())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: "OK", controller: self)
                }
            })
            BaseApp.sharedInstance.jobManager?.addOperation(allUserList)
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }
    
    func serApiForCreateSingleChatGroup(inderPathRow: Int, userList: [APIRequestParam.GroupUserData]) {
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            let userData = self.arrForFilteredUserChat[inderPathRow]
            
            var strFullName = ""
            if userData.last_name! != "" {
                strFullName = "\(userData.first_name ?? "")" + " " + "\(userData.last_name ?? "")"
            } else {
                strFullName = "\(userData.first_name ?? "")"
            }
            
            let createSingleAndGroupChatRequestParam = APIRequestParam.CreateSingleAndGroupChatInfo(group_icon: "", group_name: strFullName, is_group: false, addedasfreind: "", lastmsgsentby: "", last_msg: "", friend_name: AppTheme.getUserFullName(), created_by: ApplicationPreference.getMongoUserId()!, timestamp: "\(BaseApp.sharedInstance.getCurrentTimeMilliSeconds())", status: "1", created_id : "", friendId : "", userLists: userList)
            
            let createSingleAndGroupChat = CreateSingleAndGroupChatRequest(token: ApplicationPreference.getAppToken(), userId: ApplicationPreference.getMongoUserId()!, createSingleAndGroupChatData: createSingleAndGroupChatRequestParam, onSuccess: {
                response in
                
                print(response.toJSON())
                
                OperationQueue.main.addOperation() {
                    BaseApp.sharedInstance.hideProgressHudView()
                    
                    if(self.chatViewController == nil){
                        let dict = response.toJSON() as NSDictionary
                        self.chatViewController = BaseApp.sharedInstance.getViewController(storyboardName: AppConstant.chatVCStoryboard, viewControllerName: ChatViewController.nameOfClass) as ChatViewController
                        self.chatViewController?.blockId = "\(dict.value(forKey: "data")!)"
                        self.chatViewController?.message_by = ApplicationPreference.getMongoUserId()
                        self.chatViewController?.userID = userData._id!
                        self.chatViewController?.sender_name = AppTheme.getUserFullName()
                        self.chatViewController?.is_group = false
                        self.chatViewController?.strViewComeFrom = "SINGLECHAT"
                        self.chatViewController?.strTitle = ""
                        self.navigationController?.pushViewController(self.chatViewController!, animated: true)
                    }
                }
            }, onError: {
                error in
                print(error.toString())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: "OK", controller: self)
                }
            })
            BaseApp.sharedInstance.jobManager?.addOperation(createSingleAndGroupChat)
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }
}

