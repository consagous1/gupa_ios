//
//  chatViewCell.swift
//  TextApp
//
//  Created by Apple on 11/9/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit

class chatViewCell: UITableViewCell {

    @IBOutlet weak var btnForTitleImg: UIButton!

    @IBOutlet weak var imageForTitle: ImageLayerSetup!
    
    @IBOutlet weak var imgaeForStatus: ImageLayerSetup!
    
    @IBOutlet weak var lblForTitle: UILabel!
    
    @IBOutlet weak var imageForSelect: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
