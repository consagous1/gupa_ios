//
//  LogSectionHeaderView.swift
//  Ikan
//
//  Created by Apple on 18/10/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import Foundation

class LogSectionHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var labelTitle:UILabel!
    @IBOutlet weak var labelSeparatorLeft:UILabel!
    @IBOutlet weak var labelSeparatorRight:UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
