//
//  MessagesToolbarButtonFactory.swift
//  TextApp
//

import Foundation

class MessagesToolbarButtonFactory: NSObject{
    
    private(set) var buttonFont: UIFont!
    
    /**
     *  Creates and returns a new instance of `MessagesToolbarButtonFactory` that uses
     *  the default font for creating buttons.
     *
     *  @return An initialized `MessagesToolbarButtonFactory` object.
     */
    //- (instancetype)init;
    convenience override init() {
        self.init(font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline))
    }
    
    /**
     *  Creates and returns a new instance of `MessagesToolbarButtonFactory` that uses
     *  the specified font for creating buttons.
     *
     *  @param A font that will be used for the buttons produced by the factory.
     *
     *  @return An initialized `MessagesToolbarButtonFactory` object.
     */
    //- (instancetype)initWithFont:(UIFont *)font NS_DESIGNATED_INITIALIZER;
    init(font: UIFont) {
        super.init()
        self.buttonFont = font
    }
}

// MARK:- public method
extension MessagesToolbarButtonFactory{
    /**
     *  Creates and returns a new button that is styled as the default accessory button.
     *  The button has a paper clip icon image and no text.
     *
     *  @return A newly created button.
     */
    func defaultAccessoryButtonItem() -> UIButton {
        var accessoryImage = UIImage(named: "add-7")
        
        accessoryImage = accessoryImage?.resizeImage(targetSize: CGSize(width: 20, height: 20))
        //let normalImage = accessoryImage?.msg_imageMasked(with: UIColor(hexString: ColorCode.blue)!)
        let normalImage = accessoryImage?.maskWithColor(UIColor.black) //TODo
        //let normalImage = accessoryImage?.maskWithColor(UIColor(hexString: "#406FE9FF")!)
        //let normalImage = accessoryImage?.maskWithColor(UIColor.blue)
        let highlightedImage = accessoryImage?.msg_imageMasked(with: UIColor.black) //TODo
        //let accessoryButton = UIButton(frame: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat((accessoryImage?.size.width)!), height: CGFloat(32.0)))
        let accessoryButton = UIButton(frame: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(32.0), height: CGFloat(32.0)))
        accessoryButton.setImage(normalImage, for: .normal)
        accessoryButton.setImage(highlightedImage, for: .highlighted)
        //accessoryButton.contentMode = .scaleAspectFit
        accessoryButton.backgroundColor = UIColor.clear
        //accessoryButton.backgroundColor = UIColor(hexString: ColorCode.textColorRed)!
        //accessoryButton.tintColor = UIColor.lightGray
        accessoryButton.titleLabel!.font = self.buttonFont
        accessoryButton.accessibilityLabel = Bundle.msg_localizedString(forKey: "accessory_button_accessibility_label")
        //accessoryButton.addObserver(self, forKeyPath: "attachment", options: [.New, .Old], context: nil);
        return accessoryButton
        
    }
    
    /**
     *  Creates and returns a new button that is styled as the default send button.
     *  The button has title text `@"Send"` and no image.
     *
     *  @return A newly created button.
     */
    func defaultSendButtonItem() -> UIButton {
        let sendTitle = Bundle.msg_localizedString(forKey: "send")
        let sendButton = UIButton(frame: CGRect.zero)
//        var image = UIImage(named: "ForwardArrowIcon")
        var image = UIImage(named: "send")
        image = image?.resizeImage(targetSize: CGSize(width: 20, height: 20))
        sendButton.setImage(image, for: UIControlState.normal)
        
        sendButton.contentMode = .center
        //sendButton.backgroundColor = UIColor.clear
        sendButton.backgroundColor = UIColor.red //TODO
        sendButton.tintColor = UIColor.msg_messageBubbleBlueColor()
        let maxHeight: CGFloat = 32.0
        let sendTitleRect = sendTitle.boundingRect(with: CGSize(width: CGFloat(CGFloat.greatestFiniteMagnitude), height: maxHeight), options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedStringKey.font: sendButton.titleLabel!.font], context: nil)
        sendButton.frame = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(sendTitleRect.integral.width), height: maxHeight)
        return sendButton
    }
}
