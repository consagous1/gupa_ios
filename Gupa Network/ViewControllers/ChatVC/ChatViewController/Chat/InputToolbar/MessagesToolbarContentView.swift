//
//  MessagesToolbarContentView.swift
//  TextApp
//


import Foundation

/**
 *  A constant value representing the default spacing to use for the left and right edges
 *  of the toolbar content view.
 */
//let kMessagesToolbarContentViewHorizontalSpacingDefault:CGFloat = 8.0
let kMessagesToolbarContentViewHorizontalSpacingDefault:CGFloat = 0.0

/**
 *  A constant value representing the default spacing right width
 */
//let kMessagesToolbarContentViewRightButtonWidthDefault:CGFloat = 44.0
let kMessagesToolbarContentViewRightButtonWidthDefault:CGFloat = 75.0

/**
 *  A constant value representing the default spacing right width
 */
//let kMessagesToolbarContentViewLeftButtonWidthDefault:CGFloat = 44.0
let kMessagesToolbarContentViewLeftButtonWidthDefault:CGFloat = 0.0

/**
 *  A `MessagesToolbarContentView` represents the content displayed in a `MessagesInputToolbar`.
 *  These subviews consist of a left button, a text view, and a right button. One button is used as
 *  the send button, and the other as the accessory button. The text view is used for composing messages.
 */
class MessagesToolbarContentView: UIView {
    
    @IBOutlet weak var textView: MessagesComposerTextView? = nil
    
    @IBOutlet weak var leftBarButtonContainerView: UIView?
    @IBOutlet weak var leftBarButtonContainerViewWidthConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var rightBarButtonContainerView: UIView?
    @IBOutlet weak var rightBarButtonContainerViewWidthConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var leftHorizontalSpacingConstraint: NSLayoutConstraint?
    @IBOutlet weak var rightHorizontalSpacingConstraint: NSLayoutConstraint?
    
    /**
     *  Returns the text view in which the user composes a message.
     */
    
    /**
     *  A custom button item displayed on the left of the toolbar content view.
     *
     *  @discussion The frame height of this button is ignored. When you set this property, the button
     *  is fitted within a pre-defined default content view, the leftBarButtonContainerView,
     *  whose height is determined by the height of the toolbar. However, the width of this button
     *  will be preserved. You may specify a new width using `leftBarButtonItemWidth`.
     *  If the frame of this button is equal to `CGRectZero` when set, then a default frame size will be used.
     *  Set this value to `nil` to remove the button.
     */
    @objc weak var leftBarButtonItem: UIButton?
    
    func setLeftBarButtonItem(button:UIButton?){
        if (self.leftBarButtonItem != nil) {
            self.leftBarButtonItem?.removeFromSuperview()
        }
        if (button == nil) {
            self.leftBarButtonItem = nil
            self.leftHorizontalSpacingConstraint?.constant = 0.0
            self.setLeftBarButtonItemWidth(leftBarButtonItemWidth: 0.0)
            self.leftBarButtonContainerView?.isHidden = true
            return
        }
        if (button?.frame.equalTo(CGRect.zero))! {
            button?.frame = (self.leftBarButtonContainerView?.bounds)!
        }
        self.leftBarButtonContainerView?.isHidden = false
        self.leftHorizontalSpacingConstraint?.constant = kMessagesToolbarContentViewHorizontalSpacingDefault
//        self.setLeftBarButtonItemWidth(leftBarButtonItemWidth: (button?.frame.width)!)
        
        button?.translatesAutoresizingMaskIntoConstraints = false
        
        self.leftBarButtonContainerView?.addSubview(button!)
        self.leftBarButtonContainerView?.msg_pinAllEdgesOfSubview(button!)
        self.setNeedsUpdateConstraints()
        self.leftBarButtonItem = button
    }
    
    /**
     *  Specifies the width of the leftBarButtonItem.
     *
     *  @discussion This property modifies the width of the leftBarButtonContainerView.
     */
    var leftBarButtonItemWidth: CGFloat = 0.0
    
    func setLeftBarButtonItemWidth(leftBarButtonItemWidth:CGFloat){
        self.leftBarButtonContainerViewWidthConstraint?.constant = leftBarButtonItemWidth
        self.setNeedsUpdateConstraints()
    }
    
    func getLeftBarButtonItemWidth() -> CGFloat{
        return self.leftBarButtonContainerViewWidthConstraint!.constant
    }
    
    /**
     *  Specifies the amount of spacing between the content view and the leading edge of leftBarButtonItem.
     *
     *  @discussion The default value is `8.0f`.
     */
    var leftContentPadding: CGFloat = 0.0
    
    func setLeftContentPadding(leftContentPadding: CGFloat){
        self.leftHorizontalSpacingConstraint?.constant = leftContentPadding
        self.setNeedsUpdateConstraints()
    }
    
    /**
     *  The container view for the leftBarButtonItem.
     *
     *  @discussion
     *  You may use this property to add additional button items to the left side of the toolbar content view.
     *  However, you will be completely responsible for responding to all touch events for these buttons
     *  in your `MessagesViewController` subclass.
     */
    //weak private(set) var leftBarButtonContainerView: UIView?
    
    /**
     *  A custom button item displayed on the right of the toolbar content view.
     *
     *  @discussion The frame height of this button is ignored. When you set this property, the button
     *  is fitted within a pre-defined default content view, the rightBarButtonContainerView,
     *  whose height is determined by the height of the toolbar. However, the width of this button
     *  will be preserved. You may specify a new width using `rightBarButtonItemWidth`.
     *  If the frame of this button is equal to `CGRectZero` when set, then a default frame size will be used.
     *  Set this value to `nil` to remove the button.
     */
   @objc weak var rightBarButtonItem: UIButton?
    func setRightBarButtonItem(button:UIButton?){
        if self.rightBarButtonItem != nil{
            self.rightBarButtonItem?.removeFromSuperview()
        }
        if (button == nil) {
            self.rightBarButtonItem = nil
            self.rightHorizontalSpacingConstraint?.constant = 0.0
            self.setRightBarButtonItemWidth(rightBarButtonItemWidth: 0.0)
            self.rightBarButtonContainerView?.isHidden = true
            return
        }
        if (button?.frame.equalTo(CGRect.zero))! {
            button?.frame = (self.rightBarButtonContainerView?.bounds)!
        }
        self.rightBarButtonContainerView?.isHidden = false
        //self.rightHorizontalSpacingConstraint?.constant = kMessagesToolbarContentViewHorizontalSpacingDefault
        self.setRightBarButtonItemWidth(rightBarButtonItemWidth: (button?.frame.width)!)
        
        button?.translatesAutoresizingMaskIntoConstraints = false
        self.rightBarButtonContainerView?.addSubview(button!)
        self.rightBarButtonContainerView?.msg_pinAllEdgesOfSubview(button!)
        self.setNeedsUpdateConstraints()
        self.rightBarButtonItem = button
        self.rightBarButtonItem?.layer.cornerRadius = (self.rightBarButtonItem?.frame.size.width)! / 2
        self.rightBarButtonItem?.layer.masksToBounds = false
    }
    
    /**
     *  Specifies the width of the rightBarButtonItem.
     *
     *  @discussion This property modifies the width of the rightBarButtonContainerView.
     */
    var rightBarButtonItemWidth: CGFloat = 0.0
    
    func setRightBarButtonItemWidth(rightBarButtonItemWidth:CGFloat){
        self.rightBarButtonContainerViewWidthConstraint?.constant = rightBarButtonItemWidth
        self.setNeedsUpdateConstraints()
    }
    
    func getRightBarButtonItemWidth() -> CGFloat {
        return self.rightBarButtonContainerViewWidthConstraint!.constant
    }
    
    /**
     *  Specifies the amount of spacing between the content view and the trailing edge of rightBarButtonItem.
     *
     *  @discussion The default value is `8.0f`.
     */
    var rightContentPadding: CGFloat = 0.0
    
    func setRightContentPadding(rightContentPadding: CGFloat){
        self.rightHorizontalSpacingConstraint?.constant = rightContentPadding
        self.setNeedsUpdateConstraints()
    }
    
    /**
     *  The container view for the rightBarButtonItem.
     *
     *  @discussion
     *  You may use this property to add additional button items to the right side of the toolbar content view.
     *  However, you will be completely responsible for responding to all touch events for these buttons
     *  in your `MessagesViewController` subclass.
     */
    //weak private(set) var rightBarButtonContainerView: UIView?
    
    /**
     *  Returns the `UINib` object initialized for a `MessagesToolbarContentView`.
     *
     *  @return The initialized `UINib` object.
     */
    class func instanceFromNib() -> UINib {
        return UINib(nibName: self.nameOfClass, bundle: Bundle(identifier: self.nameOfClass))
    }
    
    //MARK - Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leftHorizontalSpacingConstraint?.constant = kMessagesToolbarContentViewHorizontalSpacingDefault
        self.rightHorizontalSpacingConstraint?.constant = kMessagesToolbarContentViewHorizontalSpacingDefault
        //self.setBackgroundColor(backgroundColor: UIColor.clear)
        self.setBackgroundColor(backgroundColor: UIColor.white)
    }
}

//MARK:- Setters
extension MessagesToolbarContentView{
    func setBackgroundColor(backgroundColor: UIColor) {
        super.backgroundColor = backgroundColor
        self.leftBarButtonContainerView?.backgroundColor = backgroundColor
        self.rightBarButtonContainerView?.backgroundColor = backgroundColor
    }
}

//MARK:- UIView overrides
extension MessagesToolbarContentView{
    override func setNeedsDisplay() {
        super.setNeedsDisplay()
        self.textView?.setNeedsDisplay()
    }
}

