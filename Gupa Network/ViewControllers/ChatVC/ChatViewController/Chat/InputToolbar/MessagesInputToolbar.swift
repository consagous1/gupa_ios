//
//  MessagesInputToolbar.swift
//  TextApp
//


import Foundation

//static void * kMessagesInputToolbarKeyValueObservingContext = &kMessagesInputToolbarKeyValueObservingContext;
// for testing purpose
var kMessagesInputToolbarKeyValueObservingContext:UnsafeMutableRawPointer = UnsafeMutableRawPointer(bitPattern: 1001)!

enum MessagesInputSendButtonLocation : Int {
    case none
    case right
    case left
}
/**
 *  The `MessagesInputToolbarDelegate` protocol defines methods for interacting with
 *  a `MessagesInputToolbar` object.
 */
protocol MessagesInputToolbarDelegate: UIToolbarDelegate {
    /**
     *  Tells the delegate that the toolbar's `rightBarButtonItem` has been pressed.
     *
     *  @param toolbar The object representing the toolbar sending this information.
     *  @param sender  The button that received the touch event.
     */
    func messagesInputToolbar(_ toolbar: MessagesInputToolbar, didPressRightBarButton sender: UIButton)
    /**
     *  Tells the delegate that the toolbar's `leftBarButtonItem` has been pressed.
     *
     *  @param toolbar The object representing the toolbar sending this information.
     *  @param sender  The button that received the touch event.
     */
    func messagesInputToolbar(_ toolbar: MessagesInputToolbar, didPressLeftBarButton sender: UIButton)
}

class MessagesInputToolbar: UIToolbar {
    
    var isMsg_isObserving = false
    
    /**
     *  The object that acts as the delegate of the toolbar.
     */
    weak var messageDelegate: MessagesInputToolbarDelegate?
    
    /**
     *  Returns the content view of the toolbar. This view contains all subviews of the toolbar.
     */
    var contentView: MessagesToolbarContentView?
    
    /**
     *  Indicates the location of the send button in the toolbar.
     *
     *  @discussion The default value is `MessagesInputSendButtonLocationRight`, which indicates that the send button is the right-most subview of
     *  the toolbar's `contentView`. Set to `MessagesInputSendButtonLocationLeft` to specify that the send button is on the left. Set to 'MessagesInputSendButtonLocationNone' if there is no send button or if you want to take control of the send button actions. This
     *  property is used to determine which touch events correspond to which actions.
     *
     *  @warning Note, this property *does not* change the positions of buttons in the toolbar's content view.
     *  It only specifies whether the `rightBarButtonItem` or the `leftBarButtonItem` is the send button or there is no send button.
     *  The other button then acts as the accessory button.
     */
    var sendButtonLocation:MessagesInputSendButtonLocation = MessagesInputSendButtonLocation(rawValue: 1)!
    /**
     *  Specify if the send button should be enabled automatically when the `textView` contains text.
     *  The default value is `YES`.
     *
     *  @discussion If `YES`, the send button will be enabled if the `textView` contains text. Otherwise,
     *  you are responsible for determining when to enable/disable the send button.
     */
    var isEnablesSendButtonAutomatically:Bool = false
    
    /**
     *  Specifies the default (minimum) height for the toolbar. The default value is `44.0f`. This value must be positive.
     */
    var preferredDefaultHeight: CGFloat = 0.0
    
    /**
     *  Specifies the maximum height for the toolbar. The default value is `NSNotFound`, which specifies no maximum height.
     */
    
    //MARK:- Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.white
        self.isMsg_isObserving = false
        self.sendButtonLocation = MessagesInputSendButtonLocation.right
        self.setEnablesSendButtonAutomatically(enableStatus: true)
        self.setPreferredDefaultHeight(preferredDefaultHeight: 44.0)
        let toolbarContentView = self.loadToolbarContentView()
        toolbarContentView.frame = self.frame
        self.addSubview(toolbarContentView)
        self.msg_pinAllEdgesOfSubview(toolbarContentView)
        self.setNeedsUpdateConstraints()
        self.contentView = toolbarContentView
        
        let toolbarButtonFactory = MessagesToolbarButtonFactory(font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline))
        self.contentView?.setLeftBarButtonItem(button: toolbarButtonFactory.defaultAccessoryButtonItem())
        self.contentView?.setRightBarButtonItem(button: toolbarButtonFactory.defaultSendButtonItem())
        
        // add observer
        self.msg_addObservers()
        
        self.updateSendButtonEnabledState()
        NotificationCenter.default.addObserver(self, selector: #selector(self.textViewTextDidChange), name: NSNotification.Name.UITextViewTextDidChange, object: contentView?.textView)
    }
    
    func loadToolbarContentView() -> MessagesToolbarContentView {
        
        let nibViews = Bundle(for: MessagesInputToolbar.classForCoder()).loadNibNamed(MessagesToolbarContentView.nameOfClass, owner: self, options: nil)
        return nibViews!.first! as! MessagesToolbarContentView
    }
 
    func setEnablesSendButtonAutomatically(enableStatus:Bool){
        self.isEnablesSendButtonAutomatically = enableStatus
        self.updateSendButtonEnabledState()
    }
    
    func setPreferredDefaultHeight(preferredDefaultHeight:CGFloat){
        self.preferredDefaultHeight = preferredDefaultHeight
    }
}

//MARK:- - Actions
extension MessagesInputToolbar{
    @objc func msg_leftBarButtonPressed(_ sender: UIButton) {
        self.messageDelegate?.messagesInputToolbar(self, didPressLeftBarButton: sender)
    }
    @objc func msg_rightBarButtonPressed(_ sender: UIButton) {
        self.messageDelegate?.messagesInputToolbar(self, didPressRightBarButton: sender)
    }
}

//MARK:- - Input toolbar
extension MessagesInputToolbar{
    func updateSendButtonEnabledState() {
        if !self.isEnablesSendButtonAutomatically {
            return
        }
        var enabled:Bool = false
        if(self.contentView != nil && self.contentView?.textView != nil){
            enabled = (self.contentView?.textView?.messageHasText())!
        }
        switch self.sendButtonLocation {
        case MessagesInputSendButtonLocation.right:
            if(self.contentView != nil && self.contentView?.rightBarButtonItem != nil){
                //self.contentView?.rightBarButtonItem!.isEnabled = enabled
                if(enabled){
                    self.contentView?.rightBarButtonContainerViewWidthConstraint?.constant = kMessagesToolbarContentViewRightButtonWidthDefault
                }else{
                    self.contentView?.rightBarButtonContainerViewWidthConstraint?.constant = 0.0
                }
            }
        case MessagesInputSendButtonLocation.left:
            if(self.contentView != nil && self.contentView?.leftBarButtonItem != nil){
                self.contentView?.leftBarButtonItem!.isEnabled = enabled
            }
        default:
            break
        }
    }
}

//MARK:- Notifications
extension MessagesInputToolbar{
    @objc func textViewTextDidChange(_ notification: Notification) {
        self.updateSendButtonEnabledState()
    }
}

//MARK:- - Key-value observing
extension MessagesInputToolbar{
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if context == kMessagesInputToolbarKeyValueObservingContext {
            
            let convObject = object as? MessagesToolbarContentView
            if convObject != nil && convObject == self.contentView {
                if(keyPath == NSStringFromSelector(#selector(getter: self.contentView?.leftBarButtonItem))){
                    self.contentView?.leftBarButtonItem!.removeTarget(self, action: nil, for: .touchUpInside)
                    self.contentView?.leftBarButtonItem!.addTarget(self, action: #selector(self.msg_leftBarButtonPressed), for: .touchUpInside)
                }
                else if (keyPath == NSStringFromSelector(#selector(getter: self.contentView?.rightBarButtonItem))) {
                    self.contentView?.rightBarButtonItem!.removeTarget(self, action: nil, for: .touchUpInside)
                    self.contentView?.rightBarButtonItem!.addTarget(self, action: #selector(self.msg_rightBarButtonPressed), for: .touchUpInside)
                }
                
                self.updateSendButtonEnabledState()
            }
        }
    }
    
    func msg_addObservers() {
        if self.isMsg_isObserving {
            return
        }
        
        // observeValue(forkeypath method is not called
//        self.contentView?.addObserver(self, forKeyPath: NSStringFromSelector(#selector(getter: self.contentView?.leftBarButtonItem)), options: [], context: kMessagesInputToolbarKeyValueObservingContext)
//        self.contentView?.addObserver(self, forKeyPath: NSStringFromSelector(#selector(getter: self.contentView?.rightBarButtonItem)), options: [], context: kMessagesInputToolbarKeyValueObservingContext)
        
        self.contentView?.leftBarButtonItem!.removeTarget(self, action: nil, for: .touchUpInside)
        self.contentView?.leftBarButtonItem!.addTarget(self, action: #selector(self.msg_leftBarButtonPressed), for: .touchUpInside)
        
        self.contentView?.rightBarButtonItem!.removeTarget(self, action: nil, for: .touchUpInside)
        self.contentView?.rightBarButtonItem!.addTarget(self, action: #selector(self.msg_rightBarButtonPressed), for: .touchUpInside)
        
        self.updateSendButtonEnabledState()
        self.isMsg_isObserving = true
    }
    
    func msg_removeObservers() {
        
        if !isMsg_isObserving {
            return
        }
        contentView?.removeObserver(self, forKeyPath: NSStringFromSelector(#selector(getter: self.contentView?.leftBarButtonItem)), context: kMessagesInputToolbarKeyValueObservingContext)
        contentView?.removeObserver(self, forKeyPath: NSStringFromSelector(#selector(getter: self.contentView?.rightBarButtonItem)), context: kMessagesInputToolbarKeyValueObservingContext)
        
        self.isMsg_isObserving = false
    }
}
