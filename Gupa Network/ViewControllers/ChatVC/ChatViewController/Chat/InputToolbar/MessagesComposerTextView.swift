//
//  MessagesComposerTextView.swift
//  TextApp
//


import Foundation

/**
 *  A delegate object used to notify the receiver of paste events from a `MessagesComposerTextView`.
 */
protocol MessagesComposerTextViewPasteDelegate {
    /**
     *  Asks the delegate whether or not the `textView` should use the original implementation of `-[UITextView paste]`.
     *
     *  @discussion Use this delegate method to implement custom pasting behavior.
     *  You should return `NO` when you want to handle pasting.
     *  Return `YES` to defer functionality to the `textView`.
     */
    func composerTextView(_ textView: MessagesComposerTextView, shouldPasteWithSender sender: Any) -> Bool
    
}

class MessagesComposerTextView: UITextView {
    
    weak var heightConstraint: NSLayoutConstraint?
    weak var minHeightConstraint: NSLayoutConstraint?
    weak var maxHeightConstraint: NSLayoutConstraint?
    
    /**
     *  The text to be displayed when the text view is empty. The default value is `nil`.
     */
    var placeHolder: String?
    
    func setPlaceHolder(placeHolder: String){
        if (self.placeHolder == placeHolder) {
            return
        }
        self.placeHolder = placeHolder
        self.setNeedsDisplay()
    }
    
    /**
     *  The color of the place holder text. The default value is `[UIColor lightGrayColor]`.
     */
    var placeHolderTextColor: UIColor = UIColor.lightGray
    
    func setPlaceHolderTextColor(placeHolderTextColor: UIColor){
        if self.placeHolderTextColor.isEqual(placeHolderTextColor) {
            return
        }
        self.placeHolderTextColor = placeHolderTextColor
        self.setNeedsDisplay()
    }
    
    /**
     *  The insets to be used when the placeholder is drawn. The default value is `UIEdgeInsets(5.0, 7.0, 5.0, 7.0)`.
     */
    var placeHolderInsets: UIEdgeInsets = UIEdgeInsets.zero
    
    func setPlaceHolderInsets(placeHolderInsets: UIEdgeInsets){
        if UIEdgeInsetsEqualToEdgeInsets(self.placeHolderInsets, placeHolderInsets) {
            return
        }
        self.placeHolderInsets = placeHolderInsets
        self.setNeedsDisplay()
    }
    
    /**
     *  The object that acts as the paste delegate of the text view.
     */
    //var pasteDelegate: MessagesComposerTextViewPasteDelegate?
    var messagesComposerTextViewPasteDelegate: MessagesComposerTextViewPasteDelegate?
    
    // MARK: - Initialization
    func msg_configureTextView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        let cornerRadius: CGFloat = 6.0
        self.backgroundColor = UIColor.white
        //self.layer.borderWidth = 0.5
        //self.layer.borderColor = UIColor.lightGray.cgColor
        //self.layer.cornerRadius = cornerRadius
        self.scrollIndicatorInsets = UIEdgeInsetsMake(cornerRadius, 0.0, cornerRadius, 0.0)
        self.textContainerInset = UIEdgeInsetsMake(4.0, 2.0, 4.0, 2.0)
        self.contentInset = UIEdgeInsetsMake(1.0, 0.0, 1.0, 0.0)
        self.isScrollEnabled = true
        self.scrollsToTop = false
        self.isUserInteractionEnabled = true
        self.font = FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_NORMAL)
        self.textColor = UIColor.black
        self.textAlignment = .natural
        self.contentMode = .redraw
        //self.dataDetectorTypes = .none
        //self.dataDetectorTypes = UIDataDetectorTypes.all
        self.keyboardAppearance = .default
        self.keyboardType = .default
        self.returnKeyType = UIReturnKeyType.default
        self.text = nil
        self.placeHolder = nil
        self.setPlaceHolderTextColor(placeHolderTextColor: UIColor.lightGray)
        self.setPlaceHolderInsets(placeHolderInsets: UIEdgeInsetsMake(5.0, 7.0, 5.0, 7.0))
        self.associateConstraints()
        self.msg_addTextViewNotificationObservers()
        
        //
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1.0
        //self.layer.masksToBounds = false
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        self.msg_configureTextView()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.msg_configureTextView()
    }
    
    
    //MARK - UIMenuController
//    override func canBecomeFirstResponder() -> Bool {
//        return super.canBecomeFirstResponder
//    }
//    override func becomeFirstResponder() -> Bool {
//        return super.becomeFirstResponder()
//    }
//    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        UIMenuController.shared.menuItems = nil
//        return super.canPerformAction(action, withSender: sender)
//    }
}

// TODO: we should just set these from the xib
extension MessagesComposerTextView{
    func associateConstraints() {
        // iterate through all text view's constraints and identify
        // height, max height and min height constraints.
        for constraint: NSLayoutConstraint in self.constraints {
            if constraint.firstAttribute == .height {
                if constraint.relation == .equal {
                    self.heightConstraint = constraint
                }
                else if constraint.relation == .lessThanOrEqual {
                    self.maxHeightConstraint = constraint
                }
                else if constraint.relation == .greaterThanOrEqual {
                    self.minHeightConstraint = constraint
                }
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // calculate size needed for the text to be visible without scrolling
        let sizeThatFits = self.sizeThatFits(self.frame.size)
        var newHeight: Float = Float(sizeThatFits.height)
        // if there is any minimal height constraint set, make sure we consider that
        if (self.maxHeightConstraint != nil) {
            newHeight = min(newHeight, Float(self.maxHeightConstraint!.constant))
        }
        // if there is any maximal height constraint set, make sure we consider that
        if (self.minHeightConstraint != nil) {
            newHeight = max(newHeight, Float(self.minHeightConstraint!.constant))
        }
        // update the height constraint
        self.heightConstraint?.constant = CGFloat(newHeight)
    }
}

//MARK - Composer text view
extension MessagesComposerTextView{
    /**
     *  Determines whether or not the text view contains text after trimming white space
     *  from the front and back of its string.
     *
     *  @return `YES` if the text view contains text, `NO` otherwise.
     */
    func messageHasText() -> Bool {
        return (self.text.length > 0)
    }
}

//MARK - UITextView overrides
extension MessagesComposerTextView{
    
    func setBounds(bounds: CGRect) {
        super.bounds = bounds
        if self.contentSize.height <= self.bounds.size.height + 1 {
            self.contentOffset = CGPoint.zero
            // Fix wrong contentOfset
        }
    }
    func setText(text: String) {
        super.text = text
        self.setNeedsDisplay()
    }
    func setAttributedText(attributedText: NSAttributedString?) {
        super.attributedText = attributedText
        self.setNeedsDisplay()
    }
    func setFont(font: UIFont) {
        super.font = font
        self.setNeedsDisplay()
    }
    func setTextAlignment(textAlignment: NSTextAlignment) {
        super.textAlignment = textAlignment
        self.setNeedsDisplay()
    }
    func paste(sender: Any) {
        if !(self.messagesComposerTextViewPasteDelegate != nil) || (self.messagesComposerTextViewPasteDelegate?.composerTextView(self, shouldPasteWithSender: sender))! {
            super.paste(sender)
        }
    }
}

//MARK - Notifications
extension MessagesComposerTextView{
    
override func draw(_ rect: CGRect) {
    super.draw(rect)
    if self.text.length == 0 && (self.placeHolder != nil) {
        self.placeHolderTextColor.set()
        self.placeHolder?.draw(in: UIEdgeInsetsInsetRect(rect, self.placeHolderInsets), withAttributes: self.msg_placeholderTextAttributes())
    }
}
}

//MARK - Drawing
extension MessagesComposerTextView{
    
    func msg_addTextViewNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.msg_didReceiveTextViewNotification), name: NSNotification.Name.UITextViewTextDidChange, object: self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.msg_didReceiveTextViewNotification), name: NSNotification.Name.UITextViewTextDidBeginEditing, object: self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.msg_didReceiveTextViewNotification), name: NSNotification.Name.UITextViewTextDidEndEditing, object: self)
    }
    func msg_removeTextViewNotificationObservers() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextViewTextDidChange, object: self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextViewTextDidBeginEditing, object: self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextViewTextDidEndEditing, object: self)
    }
    @objc func msg_didReceiveTextViewNotification(_ notification: Notification) {
        self.setNeedsDisplay()
    }
}

//MARK - Utilities
extension MessagesComposerTextView{
    func msg_placeholderTextAttributes() -> [NSAttributedStringKey: Any] {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = NSLineBreakMode.byTruncatingTail
        paragraphStyle.alignment = self.textAlignment
        return [NSAttributedStringKey.font: self.font ?? UIFont(), NSAttributedStringKey.foregroundColor: self.placeHolderTextColor, NSAttributedStringKey.paragraphStyle: paragraphStyle]
    }
}
