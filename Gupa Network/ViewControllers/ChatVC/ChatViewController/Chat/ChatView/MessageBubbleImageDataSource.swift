//
//  MessageBubbleImageDataSource.swift
//  Flum
//
//  Created by RajeshYadav on 23/12/16.
//  Copyright © 2016 47billion. All rights reserved.
//

import UIKit

protocol MessageBubbleImageDataSource {
    
    /**
     *  @return The message bubble image for a regular display state.
     *
     *  @warning You must not return `nil` from this method.
     */
    func messageBubbleImage() -> UIImage
    
    /**
     *  @return The message bubble image for a highlighted display state.
     *
     *  @warning You must not return `nil` from this method.
     */
    func messageBubbleHighlightedImage() -> UIImage
    
}
