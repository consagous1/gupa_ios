//
//  ExtensionNSBundle.swift
//  Flum
//
//  Created by RajeshYadav on 21/12/16.
//  Copyright © 2016 47billion. All rights reserved.
//

import Foundation

extension Bundle {
    
    /**
     *  @return The bundle for MessagesViewController.
     */
    class func msg_messagesBundle() -> Bundle {
        return Bundle(for: ChatViewController.self)
    }
    
    /**
     *  @return The bundle for assets in MessagesViewController.
     */

    class func msg_messagesAssetBundle() -> Bundle {
//        let bundleResourcePath = Bundle.msg_messagesBundle().resourcePath!
//        let assetPath = URL(string: bundleResourcePath)?.appendingPathComponent("MessagesAssets.bundle").absoluteString
//        return Bundle(path: assetPath!)!
        
        return Bundle.main
    }
    
    /**
     *  Returns a localized version of the string designated by the specified key and residing in the Messages table.
     *
     *  @param key The key for a string in the Messages table.
     *
     *  @return A localized version of the string designated by key in the Messages table.
     */
    class func msg_localizedString(forKey key: String) -> String {
        return NSLocalizedString(key, tableName: "Messages", bundle: Bundle.msg_messagesAssetBundle(), value: "", comment: "")
    }
}
