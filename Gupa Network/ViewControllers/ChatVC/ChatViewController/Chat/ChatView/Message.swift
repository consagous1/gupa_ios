//
//  Message.swift
//  DrApp
//

import Foundation
import UIKit


class Message {
    
    //MARK: Properties
    var owner: MessageOwner
    var type: MessageType
    var content: Any
    var timestamp: Int?
    var isRead: Bool
    var image: UIImage?
    var id:String?
    var msg_status:String?
    var showTimestamp: String?
    var msg_id: String?

//    class func send(message: Message, toID: String, completion: @escaping (Bool) -> Swift.Void)  {
//        let currentUserID = "123"
//            switch message.type {
//            case .text:
//                let values = ["type": "text", "content": message.content, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false]
//                
//            default: break
//            }
//   }

    //MARK: Inits
    init(type: MessageType, content: Any, owner: MessageOwner, timestamp: Int?, isRead: Bool, id:String?, msg_status: String?, showTimestamp: String?, msg_id: String?) {
        
        self.type = type
        self.content = content
        self.owner = owner
        self.timestamp = timestamp
        self.isRead = isRead
        self.id = id
        self.msg_status = msg_status
        self.showTimestamp = showTimestamp
        self.msg_id = msg_id

    }
}
