//
//  ExtensionUIImage.swift
//  DrApp
//
//  Created by RajeshYadav on 21/12/16.
//  Copyright © 2016 47billion. All rights reserved.
//

import UIKit

extension UIImage {
    /**
     *  Creates and returns a new image object that is masked with the specified mask color.
     *
     *  @param maskColor The color value for the mask. This value must not be `nil`.
     *
     *  @return A new image object masked with the specified color.
     */
    func msg_imageMasked(with maskColor: UIColor) -> UIImage {
        
        let imageRect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        var newImage: UIImage? = nil
        UIGraphicsBeginImageContextWithOptions(imageRect.size, false, scale)
        do {
            let context: CGContext? = UIGraphicsGetCurrentContext()
            context?.scaleBy(x: 1.0, y: -1.0)
            context?.translateBy(x: 0.0, y: -imageRect.size.height)
            context?.clip(to: imageRect, mask: cgImage!)
            context?.setFillColor(maskColor.cgColor)
            context?.fill(imageRect)
            newImage = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
    
    /**
     *  @return The regular message bubble image.
     */
    class func msg_bubbleImageFromBundle(withName name: String) -> UIImage {
        let bundle = Bundle.msg_messagesAssetBundle()
//        let path = bundle.path(forResource: name, ofType: "png", inDirectory: "Images")!
//        var path = ""
//        if let resourcePath = bundle.resourcePath {
//            path = resourcePath + "/" + name
//        }
//        return UIImage(contentsOfFile: path)!
        return UIImage(named: name)!
    }
//
    /**
     *  @return The regular message bubble image.
     */
    class func msg_bubbleRegularImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "bubble_regular")
    }
    
    /**
     *  @return The regular message bubble image without a tail.
     */
    class func msg_bubbleRegularTaillessImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "bubble_tailless")
    }
    
    /**
     *  @return The regular message bubble image stroked, not filled.
     */
    class func msg_bubbleRegularStrokedImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "bubble_stroked")
    }
    
    /**
     *  @return The regular message bubble image stroked, not filled and without a tail.
     */
    class func msg_bubbleRegularStrokedTaillessImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "bubble_stroked_tailless")
    }
    
    /**
     *  @return The compact message bubble image.
     *
     *  @discussion This is the default bubble image used by `MessagesBubbleImageFactory`.
     */
    class func msg_bubbleCompactImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "bubble_min")
    }
    
    /**
     *  @return The compact message bubble image without a tail.
     */
    class func msg_bubbleCompactTaillessImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "bubble_min_tailless")
    }
    
    /**
     *  @return The default input toolbar accessory image.
     */
    class func msg_defaultAccessoryImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "clip")
    }
    
    /**
     *  @return The default typing indicator image.
     */
    class func msg_defaultTypingIndicatorImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "typing")
    }
    
    /**
     *  @return The default play icon image.
     */
    class func msg_defaultPlayImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "play")
    }
    
    /**
     *  @return The default pause icon image.
     */
    class func msg_defaultPauseImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "pause")
    }
    
    /**
     *  @return The standard share icon image.
     *
     *  @discussion This is the default icon for the message accessory button.
     */
    class func msg_shareActionImage() -> UIImage {
        return UIImage.msg_bubbleImageFromBundle(withName: "share")
    }
    
    func maskWithColor(_ color: UIColor) -> UIImage? {
        
        let maskImage = self.cgImage
        let width = self.size.width
        let height = self.size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let bitmapContext = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue) //needs rawValue of bitmapInfo
        
        bitmapContext?.clip(to: bounds, mask: maskImage!)
        bitmapContext?.setFillColor(color.cgColor)
        bitmapContext?.fill(bounds)
        
        //is it nil?
        if let cImage = bitmapContext?.makeImage() {
            let coloredImage = UIImage(cgImage: cImage)
            
            return coloredImage
            
        } else {
            return nil
        }
    }
    
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / self.size.width
        let heightRatio = targetSize.height / self.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            //newSize = CGSize(size.width * heightRatio, size.height * heightRatio)
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            //newSize = CGSize(size.width * widthRatio,  size.height * widthRatio)
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        //let rect = CGRect(0, 0, newSize.width, newSize.height)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    // colorize image with given tint color
    // this is similar to Photoshop's "Color" layer blend mode
    // this is perfect for non-greyscale source images, and images that have both highlights and shadows that should be preserved
    // white will stay white and black will stay black as the lightness of the image is preserved
    func tint(tintColor: UIColor) -> UIImage {
        
        return modifiedImage { context, rect in
            // draw black background - workaround to preserve color of partially transparent pixels
            context.setBlendMode(.normal)
            UIColor.black.setFill()
            context.fill(rect)
            
            // draw original image
            context.setBlendMode(.normal)
            context.draw(self.cgImage!, in: rect)
            
            
            // tint image (loosing alpha) - the luminosity of the original image is preserved
            context.setBlendMode(.color)
            tintColor.setFill()
            context.fill(rect)
            
            // mask by alpha values of original image
            context.setBlendMode(.destinationIn)
            context.draw(self.cgImage!, in: rect)
        }
    }
    
    // fills the alpha channel of the source image with the given color
    // any color information except to the alpha channel will be ignored
    func fillAlpha(fillColor: UIColor) -> UIImage {
        
        return modifiedImage { context, rect in
            // draw tint color
            context.setBlendMode(.normal)
            fillColor.setFill()
            context.fill(rect)
            
            // mask by alpha values of original image
            context.setBlendMode(.destinationIn)
            context.draw(self.cgImage!, in: rect)
        }
    }
    
    private func modifiedImage( draw: (CGContext, CGRect) -> ()) -> UIImage {
        
        // using scale correctly preserves retina images
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        let context: CGContext! = UIGraphicsGetCurrentContext()
        assert(context != nil)
        
        // correctly rotate image
        context.translateBy(x: 0, y: size.height);
        context.scaleBy(x: 1.0, y: -1.0);
        
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        
        draw(context, rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
