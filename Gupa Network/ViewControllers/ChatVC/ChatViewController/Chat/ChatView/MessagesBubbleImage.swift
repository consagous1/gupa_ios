//
//  MessagesBubbleImage.swift
//  Flum
//
//  Created by RajeshYadav on 23/12/16.
//  Copyright © 2016 47billion. All rights reserved.
//

import UIKit

class MessagesBubbleImage: NSObject, MessageBubbleImageDataSource, NSCopying{
    
    /**
     *  Returns the message bubble image for a regular display state.
     */
    fileprivate var fileMessageBubbleImage: UIImage! = nil
    
    /**
     *  Returns the message bubble image for a highlighted display state.
     */
    fileprivate var fileMessageBubbleHighlightedImage: UIImage! = nil
   
    /**
     *  Initializes and returns a message bubble image object having the specified regular image and highlighted image.
     *
     *  @param image            The regular message bubble image. This value must not be `nil`.
     *  @param highlightedImage The highlighted message bubble image. This value must not be `nil`.
     *
     *  @return An initialized `MessagesBubbleImage` object.
     *
     *  @see MessagesBubbleImageFactory.
     */
    convenience required init(messageBubble image: UIImage, highlightedImage: UIImage) {
        self.init()
        
        self.fileMessageBubbleImage = image
        self.fileMessageBubbleHighlightedImage = highlightedImage
    }
    
    override init(){
        super.init()
    }
    
    // MARK: - NSObject
    override var description: String {
        return "<\(self.self): messageBubbleImage=\(self.messageBubbleImage), messageBubbleHighlightedImage=\(self.messageBubbleHighlightedImage)>"
    }
    
    func debugQuickLookObject() -> Any {
        return UIImageView(image: self.messageBubbleImage(), highlightedImage: self.messageBubbleHighlightedImage())
    }
    
    // MARK: - NSCopying
    func copy(with zone: NSZone?) -> Any {
        return type(of:self).init(messageBubble: UIImage(cgImage: self.messageBubbleImage().cgImage!), highlightedImage: UIImage(cgImage: self.messageBubbleHighlightedImage().cgImage!))
    }
    
    func messageBubbleImage() -> UIImage{
        return self.fileMessageBubbleImage
    }
    
    func messageBubbleHighlightedImage() -> UIImage{
        return self.fileMessageBubbleHighlightedImage
    }
}
