
import UIKit
import CoreLocation
import MapKit
import Alamofire
import IQKeyboardManagerSwift

class ChatViewController: BaseViewController {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var inputContainerView: UIView!
    @IBOutlet weak var inputContainerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var growingTextView: NextGrowingTextView!
    @IBOutlet weak var btnSendMessage: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnForAddOption: UIButton!
    //For Delete Functionality
    @IBOutlet weak var viewForDelete : UIView!
    @IBOutlet weak var lblForDeleteCount : UILabel!
    @IBOutlet weak var leadingConstForDeleteView : NSLayoutConstraint!
    @IBOutlet weak var lblMessage: UILabel!
    
    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    fileprivate var items = [Message]()
    fileprivate let barHeight: CGFloat = 50
    fileprivate var currentUser: User?
    var senderName:String?
    var onlineStatus:Bool = false
    fileprivate var canSendLocation = true
    
    var maxHeaderHeight: CGFloat = 88;
    let minHeaderHeight: CGFloat = 44;
    var previousScrollOffset: CGFloat = 0;
    
    var outgoingBubbleImageData:MessagesBubbleImage?
    var incomingBubbleImageData:MessagesBubbleImage?
    
    fileprivate var popUpVC:PopUpVC?
    
    var message:String?
    var message_type:String?
    var userID:String?
    var blockId:String? = ""
    var message_by:String?
    var sender_name: String?
    var otherUserId: String?
    var createdID: String?
    var strAlertMessage: String?
    
    var is_group: Bool?
    var is_ShowButton = false
    
    var groupUsersArray: [String]?
    var arrGroupUsers: NSMutableArray = NSMutableArray()
    
    class ChatModel{
        var title:String?
        var displayTitle:String?
        var messageList:[Message]?
    }
    
    let rightBarDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.rightBarDropDown
        ]
    }()
    
    var arrUserTemp = [String]()
    
    fileprivate var chatList:[ChatModel]?
    var strViewComeFrom: String = ""
    fileprivate var actionSheetControllerIOS8: UIAlertController!
    fileprivate var profileImageData:Data?
    fileprivate var imgChat: UIImage?
    fileprivate var isScreenDisplayFromImageController = true
    
    var arrForSelectedChatForDelete = NSMutableSet()
    var arrFullChatData:[SocketPayloadView.ChatReceive] = []
    var intTotalChatCount : Int = 0
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var respDict : NSDictionary = NSDictionary()
    
    var strTitle    : String = ""
    var strCallFrom : String = ""
    var userName    : String?
    var isSendMessage   = false
    var isGuidedBy   = false
    
    //****************************************************
    // MARK: - Life Cycle Methods
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
        if userName == "" {
            self.lblTitle.text = strTitle.capitalized
        } else {
            self.lblTitle.text = userName?.capitalized ?? "Chats"
        }
        
        self.arrFullChatData = []
        
        self.leadingConstForDeleteView.constant = AppConstant.ScreenSize.SCREEN_WIDTH
        self.viewForDelete.alpha = 0.0
        self.viewForDelete.layoutIfNeeded()
        self.view.layoutIfNeeded()
        
        BaseApp.sharedInstance.currentChatRoomId = blockId!
        BaseApp.sharedInstance.removeChatCount(roomId: blockId!)
        
        print("block id === \(blockId!)")
        if SocketManager.sharedInstance.isConnected() {
            SocketManager.sharedInstance.emitData(SocketEvents.COMMON_ON_CHAT_USER_GROUP_INFO, data: (blockId ?? "") as AnyObject)
        }
        self.customization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.isNavigationBarHidden = true
        
        self.lblMessage.isHidden = true
        self.tableView.isUserInteractionEnabled = true
        if is_group == true {
            if is_ShowButton {
                btnForAddOption.isHidden = false
                self.setUPDropDown()
            } else {
                btnForAddOption.isHidden = true
            }
        } else {
            btnForAddOption.isHidden = true
        }
        
        BaseApp.sharedInstance.currentChatRoomId = blockId
        BaseApp.sharedInstance.removeChatCount(roomId: blockId!)
        
        SwiftEventBus.onMainThread(self, name:AppConstant.kGetGroupInfoOnChatList,handler: handleUserGroupInfo)
        SwiftEventBus.onMainThread(self, name:SocketEvents.COMMON_ON_UPDATE_CHAT,handler: handleUpdateChat)
        SwiftEventBus.onMainThread(self, name:SocketEvents.COMMON_ON_MSG_DELETE_CHAT,handler:  handleDeleteChatChat)
        SwiftEventBus.onMainThread(self, name:SocketEvents.COMMON_ON_NOTIFY_LAST_MSG,handler:  handleLastChat)
        SwiftEventBus.onMainThread(self, name:SocketEvents.COMMON_ON_LAST_CHAT,handler: handleLastChat)
        SwiftEventBus.onMainThread(self, name:SocketEvents.COMMON_ON_PREVOIUS_MORE,handler:  handleLoadMoreChat)
        SwiftEventBus.onMainThread(self, name:SocketEvents.COMMON_ON_NOTIFY_LAST_SEEN_STATUS,handler:  handleNotifySeenChat)
        SwiftEventBus.onMainThread(self, name:SocketEvents.COMMON_ON_GROUP_DELETETED,handler:  handleGroupDelete)
        
        IQKeyboardManager.sharedManager().enable = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if is_group == false {
            
            //**API Call to get Block List
            getBlockList_ApiCall()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layoutIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
        BaseApp.sharedInstance.currentChatRoomId = nil
        IQKeyboardManager.sharedManager().enable = false
        
        NotificationCenter.default.removeObserver(self)
        SwiftEventBus.unregister(self)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    @IBAction func backButton(sender : UIButton) {
        
        if strViewComeFrom == "GROUPCHAT" {
            let newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.storyboardMain, viewControllerName: HomeViewController.nameOfClass) as HomeViewController
            self.navigationController?.pushViewController(newGroupViewController, animated: true)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func methodForShowPopUp(_ sender: Any) {
        
        rightBarDropDown.show()
    }
    
    func setUPDropDown() {
        
        rightBarDropDown.anchorView = btnForAddOption
        rightBarDropDown.bottomOffset = CGPoint(x: 0, y: btnForAddOption.frame.size.height - 5)
        rightBarDropDown.width = 140
        
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 40
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.cornerRadius = 5
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        appearance.selectionBackgroundColor = .clear
        
        if is_group == true {
            rightBarDropDown.dataSource = [
                "Add Participate",
                "Delete Group"
            ]
        }
        
        // Action triggered on selection
        rightBarDropDown.selectionAction = { [unowned self] (index, item) in
            
            switch index {
            case 0:
                let participateVC = self.storyboard?.instantiateViewController(withIdentifier: "AddParticipateController") as! AddParticipateController
                participateVC.strGroupName = self.strTitle
                participateVC.strRoomId  = self.blockId!
                self.navigationController?.pushViewController(participateVC, animated: true)
                
                break
                
            case 1:
                
                let actionSheetController = UIAlertController(title: "Message", message: "Are you sure you want to delete group?", preferredStyle: .alert)
                
                let cancelActionButton = UIAlertAction(title: "Cancel", style: .default) { action -> Void in
                }
                
                let clearChatActionButton = UIAlertAction(title: "Delete", style: .default) { action -> Void in
                    if SocketManager.sharedInstance.isConnected() {
                        let roomid = self.blockId ?? ""
                        self.deleteGroup_APICall(groupId: roomid) //GroupId
                    }
                }
                actionSheetController.addAction(cancelActionButton)
                actionSheetController.addAction(clearChatActionButton)
                self.present(actionSheetController, animated: true, completion: nil)
                
                break
                
            default:
                break
            }
        }
    }
}

//****************************************************
// MARK:- Handle notification
//****************************************************

extension ChatViewController {
    
    func handleUserGroupInfo(notification: Notification!) -> Void {
        
        self.updateUserInfo(notification: notification)
        if SocketManager.sharedInstance.isConnected() && strCallFrom != "ONLINEOFFLINESTATUS" {
            SocketManager.sharedInstance.emitData(SocketEvents.COMMON_ON_ADD_ROOM_ID, data: (blockId ?? "") as AnyObject)
        }
    }
    
    func handleOnlineOfflineStatus(notification: Notification!) -> Void {
        
        if is_group == false {
            let object:[SocketPayloadView.OnlineOfflineStatus]? = notification.object as? [SocketPayloadView.OnlineOfflineStatus]
            
            if(object != nil && (object?.count)! > 0){
                
                let userid = object![0].id
                
                for i in 0..<arrGroupUsers.count {
                    
                    let dict = arrGroupUsers.object(at: i) as! NSDictionary
                    
                    if (userid?.caseInsensitiveCompare("\(dict.value(forKey: "userId")!)") == ComparisonResult.orderedSame){
                        if SocketManager.sharedInstance.isConnected() {
                            SocketManager.sharedInstance.emitData(SocketEvents.COMMON_ON_CHAT_USER_GROUP_INFO, data: (blockId ?? "") as AnyObject)
                            strCallFrom = "ONLINEOFFLINESTATUS"
                        }
                        break
                    }
                }
            }
        }
    }
    
    func handleUpdateChat(notification: Notification!) -> Void {
        
        let object:[SocketPayloadView.ChatReceive]? = notification.object as? [SocketPayloadView.ChatReceive]
        //Intcount for loadmore
        self.intTotalChatCount = (object?.count)!
        
        BaseApp.sharedInstance.hideProgressHudView()
        if(object != nil && (object?.count)! > 0){
            self.arrFullChatData = object!
            self.updateDataAndReloadTable(self.arrFullChatData)
        }
        
        if(chatList != nil && (chatList?.count)! > 0){
            tableView.scrollToBottom()  //TODO Scroll
        }
    }
    
    func handleLastChat(notification: Notification!) -> Void {
        
        let object:[SocketPayloadView.ChatReceive]? = notification.object as? [SocketPayloadView.ChatReceive]
        
        if (object != nil && (object?.count)! > 0) {
            
            for objectInfo in object! {
                self.arrFullChatData.append(objectInfo)
                
                let dict = objectInfo.toJSON()
                if (ApplicationPreference.getMongoUserId()?.caseInsensitiveCompare(objectInfo.receiver_id!) == ComparisonResult.orderedSame) {
                    if SocketManager.sharedInstance.isConnected() {
                        SocketManager.sharedInstance.emitDataList(SocketEvents.COMMON_ON_UPDATE_LAST_SEEN_STATUS, data: [dict , self.blockId ?? "" ])
                    }
                }
            }
            self.updateDataAndReloadTable(self.arrFullChatData)
        }
        BaseApp.sharedInstance.hideProgressHudView()
        
        if(chatList != nil && (chatList?.count)! > 0){
            
            self.tableView.scrollToBottom()
            //  tableView.reloadData()
        }
    }
    
    func handleNotifySeenChat(notification: Notification!) -> Void {
        
        let object:[SocketPayloadView.ChatReceive]? = notification.object as? [SocketPayloadView.ChatReceive]
        if (object != nil && (object?.count)! > 0) {
            for objectInfo in object! {
                if let index = self.arrFullChatData.index(where: { $0._id == objectInfo._id}) {
                    self.arrFullChatData.remove(at: index)
                    self.arrFullChatData.append(objectInfo)
                }
            }
            self.updateDataAndReloadTable(self.arrFullChatData)
        }
        BaseApp.sharedInstance.hideProgressHudView()
        if(chatList != nil && (chatList?.count)! > 0){
            tableView.scrollToBottom()
        }
    }
    
    func handleGroupDelete(notification: Notification!) -> Void {
        
        BaseApp.sharedInstance.hideProgressHudView()
        
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: "Alert!", message: "This Group has been deleted by Admin.", preferredStyle: .alert)
        
        let cancelActionButton = UIAlertAction(title: "OK", style: .default) { action -> Void in
            self.navigationController?.popViewController(animated: true)
        }
        actionSheetController.addAction(cancelActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func handleDeleteChatChat(notification: Notification!) -> Void {
        
        let responseJson = notification.object as? NSDictionary
        let strError = "\(responseJson?.value(forKey: "error")! ?? "")"
        if strError == "false" {
            let arrForDelete = self.arrForSelectedChatForDelete.allObjects as! [Message]
            var arrTemp = self.arrFullChatData
            for i in 0..<arrForDelete.count {
                let deleteDataId = arrForDelete[i].msg_id
                if let index = arrTemp.index(where: { $0._id == deleteDataId}) {
                    arrTemp.remove(at: index)
                }
            }
            self.arrFullChatData = []
            self.arrFullChatData = arrTemp
            self.updateDataAndReloadTable(self.arrFullChatData)
        } else {
        }
    }
    
    func handleLoadMoreChat(notification: Notification!) -> Void {
        
        let object:[SocketPayloadView.ChatReceive]? = notification.object as? [SocketPayloadView.ChatReceive]
        //Intcount for loadmore
        self.intTotalChatCount += (object?.count)!
        print(self.intTotalChatCount)
        
        BaseApp.sharedInstance.hideProgressHudView()
        
        if (object != nil && (object?.count)! > 0) {
            for objectInfo in object! {
                if self.arrFullChatData.count > 0 {
                    self.arrFullChatData.append(objectInfo)
                }
            }
            self.updateDataAndReloadTable(self.arrFullChatData)
        }
    }
    
    func updateDataAndReloadTable(_ object: [SocketPayloadView.ChatReceive]?) {
        
        print("All chat data = \(self.arrFullChatData)")
        
        if(object != nil && (object?.count)! > 0){
            let objectData = object?.sorted(by: {$0.sendtimestamp?.compare($1.sendtimestamp!) == .orderedAscending })
            
            chatList = []
            
            for objectInfo in objectData!{
                
                var messageSenderType = MessageOwner.sender
                
                if (objectInfo.sender_id?.caseInsensitiveCompare(ApplicationPreference.getMongoUserId()!) == ComparisonResult.orderedSame){
                    messageSenderType = MessageOwner.receiver
                }
                
                if (!isMessageExits(id: objectInfo._id ?? "")){ //&& objectInfo.is_deleted?.intValue == 0
                    
                    // filter message as per date
                    var chatModel:ChatModel?
                    if chatList != nil {
                        
                        for chatListInfo in chatList!{
                            if(chatListInfo.title?.caseInsensitiveCompare(DateTimeUtils.sharedInstance.getDeviceTimeZoneDateStrFromUnixFormat((objectInfo.sendtimestamp?.doubleValue)!)!) == ComparisonResult.orderedSame){
                                chatModel = chatListInfo
                                break
                            }
                        }
                    }
                    if(chatModel == nil){
                        chatModel = ChatModel()
                        chatModel?.title = DateTimeUtils.sharedInstance.getDeviceTimeZoneDateStrFromUnixFormat((objectInfo.sendtimestamp?.doubleValue)!)!
                        
                        chatModel?.displayTitle = DateTimeUtils.sharedInstance.formatSimpleData(date: DateTimeUtils.sharedInstance.getDateFromTimeInterval(timeInterval: (objectInfo.sendtimestamp?.doubleValue)!))
                        chatModel?.messageList = []
                        chatList?.append(chatModel!)
                    }
                    var msg_status = false
                    
                    var displayTimestamp = DateTimeUtils.sharedInstance.formatSimpleData(date: DateTimeUtils.sharedInstance.getDateFromTimeInterval(timeInterval: (objectInfo.sendtimestamp?.doubleValue)!))
                    
                    if (objectInfo.msg_status != nil) {
                        
                        if objectInfo.msg_status == "send" {
                            
                            msg_status = false
                            displayTimestamp = DateTimeUtils.sharedInstance.formatSimpleData(date: DateTimeUtils.sharedInstance.getDateFromTimeInterval(timeInterval: (objectInfo.sendtimestamp?.doubleValue)!))
                            
                        } else if objectInfo.msg_status == "delivered" {
                            msg_status = false
                            displayTimestamp = DateTimeUtils.sharedInstance.formatSimpleData(date: DateTimeUtils.sharedInstance.getDateFromTimeInterval(timeInterval: (objectInfo.recievetimestamp?.doubleValue)!))
                        } else {
                            msg_status = true
                            displayTimestamp = DateTimeUtils.sharedInstance.formatSimpleData(date: DateTimeUtils.sharedInstance.getDateFromTimeInterval(timeInterval: (objectInfo.readtimestamp?.doubleValue)!))
                        }
                    } else {
                        msg_status = false
                        displayTimestamp = DateTimeUtils.sharedInstance.formatSimpleData(date: DateTimeUtils.sharedInstance.getDateFromTimeInterval(timeInterval: (objectInfo.sendtimestamp?.doubleValue)!))
                    }
                    if objectInfo.message_type == "text" {
                        chatModel?.messageList?.append(Message.init(type: .text, content: objectInfo.text ?? "", owner: messageSenderType, timestamp: objectInfo.sendtimestamp != nil ? (objectInfo.sendtimestamp?.intValue)! : 0, isRead: msg_status, id: objectInfo.sender_id, msg_status: "\(objectInfo.msg_status!)", showTimestamp: displayTimestamp, msg_id: objectInfo._id))//objectInfo._id
                        
                    }
                }
            }
        } else {
            if(chatList != nil){
                chatList = []
            }
        }
        tableView.reloadData()
        
        if self.leadingConstForDeleteView.constant == 0 {
            self.arrForSelectedChatForDelete = []
            self.leadingConstForDeleteView.constant = AppConstant.ScreenSize.SCREEN_WIDTH
            self.viewForDelete.alpha = 0.0
            self.viewForDelete.layoutIfNeeded()
            self.view.layoutIfNeeded()
            print(chatList?.count as Any)
        }
    }
    
    func updateUserInfo(notification: Notification!) -> Void {
        
        if(BaseApp.sharedInstance.currentChatRoomId != blockId){
            blockId = BaseApp.sharedInstance.currentChatRoomId
        }
        
        let object:[SocketPayloadView.PrivateGroupUserInfo]? = notification.object as? [SocketPayloadView.PrivateGroupUserInfo]
        
        if(object != nil && (object?.count)! > 0) {
            
            //                if is_group == false {
            //
            //                    //Online Status
            //                    let onlineStatus = object?[0].online
            //                    if onlineStatus == true {
            //                        let strName = "\(object![0].first_name ?? "")" + " " + "\(object![0].last_name ?? "")"
            //                       // let strNameWithImage = BaseApp.sharedInstance.getOnlineUserNameWithImage(strFullName: strName)
            //
            //                    } else {
            //                        let strName = "\(object![0].first_name ?? "")" + " " + "\(object![0].last_name ?? "")"
            //                        var strNameWithTime = ""
            //                        if "\(object![0].onlineTime!)" != "" {
            //                            let date = DateTimeUtils.sharedInstance.getDateFromTimeInterval(timeInterval: (object![0].onlineTime!.doubleValue))
            //                            let strNameWithTimeAgo = DateTimeUtils.sharedInstance.convertTimeToEasyString(sourceDate: date)
            //                            strNameWithTime = strName.capitalized + "\n" + strNameWithTimeAgo!
            //                        } else {
            //                            strNameWithTime = strName.capitalized
            //                        }
            //
            //                    }
            //                } else {
            //                 //   self.heightConstraintForBtnLocation.constant = 0
            //                   // self.btnForLocation.alpha = 0.0
            //                   // self.labelNavigation.text = strTitle.capitalized
            //                  //  self.btnForCall.isHidden = true
            //                }
            
            self.groupUsersArray = []
            self.arrGroupUsers = NSMutableArray()
            
            for i in 0..<(object?.count)! {
                let dictTemp = NSMutableDictionary()
                let userId = object?[i]._id
                let first_name = object?[i].first_name
                self.groupUsersArray?.append(userId!)
                
                dictTemp.setValue(userId, forKey: "userId")
                dictTemp.setValue(first_name, forKey: "first_name")
                self.arrGroupUsers.add(dictTemp)
            }
        } else {
            self.groupUsersArray = []
            self.arrGroupUsers = NSMutableArray()
        }
        self.view.layoutIfNeeded()
    }
}



//MARK:- ChatViewController
extension ChatViewController{
    
    fileprivate func isMessageExits(id:String) -> Bool{
        var isFound = false
        
        if(chatList != nil && (chatList?.count)! > 0){
            
            for chatListInfo in chatList!{
                for chatModel in chatListInfo.messageList!{
                    if chatModel.id?.caseInsensitiveCompare(id) == ComparisonResult.orderedSame {
                        isFound = true
                        break
                    }
                }
            }
        }
        
        //if let index = self.arrFullChatData.index(where: { $0._id == id}) {
        //            if let index = self.arrFullChatData.index(where: { $0._id?.caseInsensitiveCompare(id) == ComparisonResult.orderedSame}) {
        //                isFound = true
        //            }
        
        return isFound
    }
    
    fileprivate func filterMessageList(){
        //chatList
    }
}

extension ChatViewController {
    
    //MARK: Methods
    func customization() {
        
        //self.growingTextView.maxNumberOfLines = 1
        self.growingTextView.layer.cornerRadius = 10
        self.growingTextView.textView.autocorrectionType = .no
        self.growingTextView.textView.textContainerInset = UIEdgeInsets(top: 10, left: 5, bottom: 4, right: 40)
        self.growingTextView.placeholderAttributedText = NSAttributedString(string: "Type Message here...", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray]
        )
        
        let image = UIImage(named: "send_chat")?.withRenderingMode(.alwaysTemplate)
        btnSendMessage.setImage(image, for: .normal)
        btnSendMessage.tintColor = UIColor(red: 0.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        
        tableView.allowsMultipleSelection = true
        tableView.sectionHeaderHeight = 40
        tableView.register(UINib(nibName: LogSectionHeaderView.nameOfClass, bundle: nil), forHeaderFooterViewReuseIdentifier: LogSectionHeaderView.nameOfClass)
        
        /**
         *  Create message bubble images objects.
         *
         *  Be sure to create your bubble images one time and reuse them for good performance.
         *
         */
        let bubbleFactory = MessagesBubbleImageFactory()
        self.outgoingBubbleImageData = bubbleFactory.outgoingMessagesBubbleImage(with: UIColor.msg_messageBubbleLightGrayColor())
        self.incomingBubbleImageData = bubbleFactory.incomingMessagesBubbleImage(with: UIColor.msg_messageBubbleLightGreenColor())
        
        self.tableView.estimatedRowHeight = self.barHeight
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset.bottom = self.barHeight
        self.tableView.scrollIndicatorInsets.bottom = self.barHeight
        self.tableView.backgroundColor = UIColor.clear
        self.navigationItem.title = self.currentUser?.name
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        if SocketManager.sharedInstance.isConnected() {
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
        }
    }
    
    //MARK: NotificationCenter handlers
    func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.tableView.contentInset.bottom = height
            self.tableView.scrollIndicatorInsets.bottom = height
            if self.items.count > 0 {
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
            }
        }
    }
}

extension ChatViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        _ = scrollView.contentOffset.y - self.previousScrollOffset
        
        let _: CGFloat = 0;
        let _: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height;
        
        if(scrollView.contentOffset.y > AppConstant.headerColorChangeConstant){
            updateSectionOnScroll()
        } else { // clear all color of section header
            removeSectionHeaderBackground()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollViewDidStopScrolling(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.scrollViewDidStopScrolling(scrollView)
        }
    }
    
    func scrollViewDidStopScrolling(_ scrollView: UIScrollView) {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let midPoint = self.minHeaderHeight + (range / 2)
        
        print(midPoint)
        if scrollView.contentOffset.y == 0 {
            if SocketManager.sharedInstance.isConnected() {
                let count = intTotalChatCount + 1
                let roomid = self.blockId ?? ""
                SocketManager.sharedInstance.emitDataList(SocketEvents.COMMON_ON_LOAD_MORE, data: [count, roomid])
            }
        }
    }
    
    func collapseHeader() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func expandHeader() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func setScrollPosition(_ position: CGFloat) {
        self.tableView.contentOffset = CGPoint(x: self.tableView.contentOffset.x, y: position)
    }
    
    // update tableview section
    func updateSectionOnScroll(){
        
        if(tableView.visibleSectionHeaders.count > 0){
            for headerView in tableView.visibleSectionHeaders{
                headerView.labelTitle.backgroundColor = UIColor.clear
            }
        }
        
        if((self.tableView.indexPathsForVisibleRows?.count)! > 0){
            
            guard let topVisibleIndexPath:IndexPath = self.tableView.indexPathsForVisibleRows?[0] else {
                return
            }
            guard let headerView = tableView.headerView(forSection: (topVisibleIndexPath.section)) as? LogSectionHeaderView else{
                return
            }
            headerView.labelTitle.backgroundColor = UIColor.white // TODO Uper Label text color
            headerView.labelTitle.cornerRadius = headerView.labelTitle.frame.height / 2
            headerView.labelTitle.borderWidth = 0.5
            headerView.labelTitle.layer.borderColor = UIColor(red: 0.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
            headerView.labelTitle.textColor = UIColor(red: 0.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            headerView.labelSeparatorLeft.isHidden = true
            headerView.labelSeparatorRight.isHidden = true
        }
    }
    
    func removeSectionHeaderBackground(){
        
        if(tableView.visibleSectionHeaders.count > 0){
            for headerView in tableView.visibleSectionHeaders {
                
                headerView.labelTitle.textColor = UIColor(hexString: "#555555FF")
                headerView.labelTitle.backgroundColor   = UIColor.clear
                headerView.labelTitle.layer.borderColor = UIColor.clear.cgColor
                headerView.labelSeparatorLeft.isHidden  = false
                headerView.labelSeparatorRight.isHidden = false
            }
        }
    }
}

// MARK:- UITableViewDelegate and DataSource method implementation
extension ChatViewController: UITableViewDelegate, UITableViewDataSource{
    
    //MARK: Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.chatList != nil ? (self.chatList?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        if(chatList != nil && section < (chatList?.count)!){
            let chatModel = chatList?[section]
            rowCount = chatModel?.messageList != nil ? (chatModel?.messageList?.count)! : 0
        }
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: LogSectionHeaderView.nameOfClass) as! LogSectionHeaderView
        
        if(chatList != nil && section < (chatList?.count)!){
            let chatModel = chatList?[section]
            headerView.labelTitle.text = "" + (chatModel?.displayTitle)! + "      "
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging {
            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, animations: {
                cell.transform = CGAffineTransform.identity
            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var rowData:Message?
        if(chatList != nil && indexPath.section < (chatList?.count)!) {
            let chatModel = chatList?[indexPath.section]
            if(chatModel != nil && chatModel?.messageList != nil && indexPath.row < (chatModel?.messageList?.count)!){
                rowData = chatModel?.messageList?[indexPath.row]
            }
            
            switch rowData?.type {
            case .text?:
                return UITableViewAutomaticDimension
                
            default:
                return UITableViewAutomaticDimension
            }
        } else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var rowData:Message?
        
        if(chatList != nil && indexPath.section < (chatList?.count)!){
            let chatModel = chatList?[indexPath.section]
            if(chatModel != nil && chatModel?.messageList != nil && indexPath.row < (chatModel?.messageList?.count)!){
                rowData = chatModel?.messageList?[indexPath.row]
            }
        }
        
        switch rowData?.owner {
        case .receiver?:
            
            switch rowData?.type {
            case .text?:
                let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
                cell.clearCellData()
                
                cell.message.text = rowData?.content as! String
                cell.labelTitle.text = "Me"
                
                if((rowData?.timestamp)! > 0){
                    
                    let startDateTimeStamp = rowData?.timestamp
                    let time = DateTimeUtils.sharedInstance.getDeviceTimeZoneTimeStrFromUnixFormat(Double(startDateTimeStamp!))
                    if is_group == false {
                        //                            cell.labelDateTime.attributedText = BaseApp.sharedInstance.getMsgTextWithSeenStatusAndImage(strTime: time!, msgType: (rowData?.isRead)!, isGroup: false, msgStatus: (rowData?.msg_status)!)
                        cell.labelDateTime.attributedText = BaseApp.sharedInstance.getMsgTextWithSeenStatusAndImage(strTime: time!, isGroup: false)
                    } else {
                        //                            cell.labelDateTime.attributedText = BaseApp.sharedInstance.getMsgTextWithSeenStatusAndImage(strTime: time!, msgType: (rowData?.isRead)!, isGroup: true, msgStatus: (rowData?.msg_status)!)
                        cell.labelDateTime.attributedText = BaseApp.sharedInstance.getMsgTextWithSeenStatusAndImage(strTime: time!, isGroup: true)
                    }
                } else {
                    cell.labelDateTime.attributedText = BaseApp.sharedInstance.getMsgTextWithSeenStatusAndImage(strTime: "", isGroup: false)
                }
                cell.messageBackground.image = outgoingBubbleImageData?.messageBubbleImage()
                
                if arrForSelectedChatForDelete.contains(rowData as Any) {
                    cell.backgroundColor = UIColor.lightGray
                } else {
                    cell.backgroundColor = UIColor.clear
                }
                return cell
                
            default: break
            }
            
        case .sender?:
            var strSenderName = ""
            if let index = groupUsersArray?.index(of: (rowData?.id)!) {
                let dict = self.arrGroupUsers.object(at: index) as! NSDictionary
                strSenderName = "\(dict.value(forKey: "first_name")!)".capitalized
            }
            
            //switch self.items[indexPath.row].type {
            switch rowData?.type {
                
            case .text?:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
                cell.clearCellData()
                
                cell.message.text = rowData?.content as! String
                if is_group == true {
                    cell.labelTitle.text = strSenderName
                    cell.labelTitle.textColor = UIColor.darkGray
                    cell.labelTitleHeight.constant = 15.0
                } else {
                    cell.labelTitle.text = ""
                    cell.labelTitleHeight.constant = 0.0
                }
                cell.labelTitle.backgroundColor = UIColor.clear
                
                if((rowData?.timestamp)! > 0){
                    let startDateTimeStamp = rowData?.timestamp
                    let time = DateTimeUtils.sharedInstance.getDeviceTimeZoneTimeStrFromUnixFormat(Double(startDateTimeStamp!))
                    cell.labelDateTime.text = time
                } else {
                    cell.labelDateTime.text = ""
                }
                
                cell.messageBackground.image = incomingBubbleImageData?.messageBubbleImage()
                
                if arrForSelectedChatForDelete.contains(rowData as Any) {
                    cell.backgroundColor = UIColor.darkGray
                } else {
                    cell.backgroundColor = UIColor.clear
                }
                return cell
                
            default: break
                
            }
        case .none:
            break
        }
        
        // Default cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        let isDeleteChat = ApplicationPreference.getPermissionDeleteChat()
        //        if isDeleteChat == true {
        var rowData:Message?
        if(chatList != nil && indexPath.section < (chatList?.count)!){
            let chatModel = chatList?[indexPath.section]
            
            if(chatModel != nil && chatModel?.messageList != nil && indexPath.row < (chatModel?.messageList?.count)!){
                rowData = chatModel?.messageList?[indexPath.row]
                
                if arrForSelectedChatForDelete.contains(rowData as Any) {
                    arrForSelectedChatForDelete.remove(rowData as Any)
                } else {
                    arrForSelectedChatForDelete.add(rowData as Any)
                }
                self.tableView.reloadData()
                
                if arrForSelectedChatForDelete.count > 0 {
                    self.lblForDeleteCount.text = "\(self.arrForSelectedChatForDelete.count)"
                    self.leadingConstForDeleteView.constant = 0
                    self.viewForDelete.alpha = 1.0
                    self.viewForDelete.layoutIfNeeded()
                    self.view.layoutIfNeeded()
                } else {
                    self.leadingConstForDeleteView.constant = AppConstant.ScreenSize.SCREEN_WIDTH
                    self.viewForDelete.alpha = 0.0
                    self.viewForDelete.layoutIfNeeded()
                    self.view.layoutIfNeeded()
                }
            }
            // }
        }
    }
}

extension ChatViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ChatViewController {
    
    @IBAction func btn_CrossClicked(_ sender: UIButton) {
        
        self.arrForSelectedChatForDelete = NSMutableSet()
        self.tableView.reloadData()
        
        self.leadingConstForDeleteView.constant = AppConstant.ScreenSize.SCREEN_WIDTH
        self.viewForDelete.alpha = 0.0
        self.viewForDelete.layoutIfNeeded()
        self.view.layoutIfNeeded()
    }
    
    @IBAction func btn_DeleteClicked(_ sender: UIButton) {
        
        let arrForDelete = self.arrForSelectedChatForDelete.allObjects as! [Message]
        
        if((arrForDelete as AnyObject).count > 0){
            var arrTempDelete = [String]()
            
            for i in 0..<arrForDelete.count {
                
                let deleteData = arrForDelete[i].msg_id
                arrTempDelete.append("\(deleteData!)")
            }
            
            let dict = NSMutableDictionary()
            dict.setValue(arrTempDelete, forKey: "msgIds")
            
            
            // Create the AlertController and add its actions like button in ActionSheet
            let actionSheetController = UIAlertController(title: "Message", message: "Are you sure you want to delete chat?", preferredStyle: .alert)
            
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .default) { action -> Void in
                
            }
            actionSheetController.addAction(cancelActionButton)
            
            let clearChatActionButton = UIAlertAction(title: "OK", style: .default) { action -> Void in
                if SocketManager.sharedInstance.isConnected() {
                    let roomid = self.blockId ?? ""
                    
                    let msgs = dict.mutableCopy() as! NSMutableDictionary
                    SocketManager.sharedInstance.emitDataList(SocketEvents.COMMON_ON_DELETE_CHAT, data: [msgs, roomid])
                    print("44444===== \( [msgs, roomid])")
                    
                }
            }
            actionSheetController.addAction(clearChatActionButton)
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
}

extension ChatViewController: MessagesComposerTextViewPasteDelegate{
    func composerTextView(_ textView: MessagesComposerTextView, shouldPasteWithSender sender: Any) -> Bool{
        return false
    }
}

extension ChatViewController {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func handleSendButton(_ sender: AnyObject) {
        
        if isSendMessage == true {
            
            let refreshAlert = UIAlertController(title: strAlertMessage, message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
        } else {
            
            if let text = self.growingTextView.textView.text {
                if text.count > 0 {
                    if SocketManager.sharedInstance.isConnected() {
                        
                        self.message_type = "text"
                        self.message = text.trimmingCharacters(in: CharacterSet.whitespaces)
                        SocketManager.sharedInstance.emitDataList(SocketEvents.COMMON_ON_SEND_CHAT, data: [self.message ?? "", self.message_type ?? "", self.userID ?? "" , self.blockId ?? "" , self.message_by ?? "", self.sender_name ?? "", self.is_group as Any, self.groupUsersArray as Any])
                    }
                    self.growingTextView.textView.text = ""
                }
            }
        }
    }
    
    @objc func keyboardWillHide(_ sender: Notification) {
        
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                
                self.inputContainerViewBottom.constant =  0
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardWillShow(_ sender: Notification) {
        
        if let userInfo = (sender as NSNotification).userInfo {
            
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                
                self.inputContainerViewBottom.constant = keyboardHeight
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    
                    if(self.chatList != nil && (self.chatList?.count)! > 0){
                        self.tableView.scrollToBottom()
                        //     self.tableView.reloadData()
                    }
                    
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func getBlockList_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(createdID, forKey:"receiver_id")
        paraDict.setValue(otherUserId, forKey:"sender_id")
        
        AppTheme().callPostService(String(format: "%@get_block_status", AppUrlNew), param: paraDict) {(result, data) -> Void in
            if result == "success"
            {
                self.respDict = data as! NSDictionary
                
                let strBlockedBy : String = self.respDict.value(forKey: "blockedBy") as! String
                let strGuideStatus : Bool = self.respDict.value(forKey: "guideStatus") as! Bool
                //  let strGuidedBy   = self.respDict.value(forKey: "guidedBy") as! String
                let strBlockStatus  : Bool   = self.respDict.value(forKey: "blockStatus") as! Bool
                print(strBlockStatus)
                print(strGuideStatus)
                if strBlockStatus == true {
                    
                    if ((AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject).caseInsensitiveCompare(strBlockedBy) == ComparisonResult.orderedSame){
                        
                        self.strAlertMessage = "Unable to send a message.First unblock this user to send message."
                    } else {
                        self.strAlertMessage = "Unable to send message to this user."
                    }
                    self.lblMessage.isHidden = true
                    self.isSendMessage = true
                    self.tableView.isUserInteractionEnabled = true
                } else {
                    
                    if strBlockStatus == false && strGuideStatus == false  {
                        
                        let image = UIImage(named: "send_chat")?.withRenderingMode(.alwaysTemplate)
                        self.btnSendMessage.setImage(image, for: .normal)
                        self.btnSendMessage.tintColor = UIColor(red: 0.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 0.5)
                        
                        self.btnSendMessage.isUserInteractionEnabled = false
                        self.growingTextView.isUserInteractionEnabled = false
                        
                        self.lblMessage.isHidden = false
                        self.tableView.isUserInteractionEnabled = false
                        
                    } else if strGuideStatus == true {
                        
                        let image = UIImage(named: "send_chat")?.withRenderingMode(.alwaysTemplate)
                        self.btnSendMessage.setImage(image, for: .normal)
                        self.btnSendMessage.tintColor = UIColor(red: 0.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                        self.btnSendMessage.isUserInteractionEnabled = true
                        self.lblMessage.isHidden = true
                        self.tableView.isUserInteractionEnabled = true
                        self.growingTextView.isUserInteractionEnabled = true
                        
                    }
                }
                
                
                //                if strGuideStatus == true {
                //
                //                    let image = UIImage(named: "send_chat")?.withRenderingMode(.alwaysTemplate)
                //                    self.btnSendMessage.setImage(image, for: .normal)
                //                    self.btnSendMessage.tintColor = UIColor(red: 0.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                //                    self.btnSendMessage.isUserInteractionEnabled = true
                //                    self.lblMessage.isHidden = true
                //  self.tableView.isUserInteractionEnabled = true
                //                } else {
                //                    let image = UIImage(named: "send_chat")?.withRenderingMode(.alwaysTemplate)
                //                    self.btnSendMessage.setImage(image, for: .normal)
                //                    self.btnSendMessage.tintColor = UIColor(red: 0.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 0.5)
                //                    self.btnSendMessage.isUserInteractionEnabled = false
                //                    self.lblMessage.isHidden = false
                //                    self.tableView.isUserInteractionEnabled = false
                //                }
                
                print("44444===== \(String(describing: strBlockedBy))")
                print("3333===== \(String(describing: strGuideStatus))")
                // print("666===== \(String(describing: strGuidedBy))")
                print("99===== \(String(describing: strBlockStatus))")
                
                //                if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray{
                //                    self.arrOfResponseKey = array_Response_mutable
                //                    print("44444===== \(self.arrOfResponseKey)")
                
                //                     self.arrUserTemp = [String]()
                //
                //                    for i in 0..<self.arrOfResponseKey.count {
                //
                //                        let dict = self.arrOfResponseKey.object(at: i) as! NSDictionary
                //
                //                        if (self.userID!.caseInsensitiveCompare("\(dict.value(forKey: "mongo_obj")!)") == ComparisonResult.orderedSame){
                //                            print("contain that id")
                //                            self.isSendMessage = true
                //                        } else {
                //                            self.isSendMessage  = false
                //                        }
                //                    }
                //    }
            } else {
                //                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Server Error! Please check again later.", showInView: self.view)
            }
        }
    }
    
    func deleteGroup_APICall(groupId : String) {
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            
            let createSingleAndGroupChat = DeleteGroupRequest(userId: groupId, onSuccess: {
                response in
                
                print("response data = \(response.toJSON())")
                
                OperationQueue.main.addOperation() {
                    BaseApp.sharedInstance.hideProgressHudView()
                    
                    if SocketManager.sharedInstance.isConnected() {
                        SocketManager.sharedInstance.emitData(SocketEvents.COMMON_ON_GROUP_DELETE_BYADMIN, data: (self.blockId ?? "") as AnyObject)
                    }
                    
                    let newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.storyboardMain, viewControllerName: HomeViewController.nameOfClass) as HomeViewController
                    self.navigationController?.pushViewController(newGroupViewController, animated: true)
                }
                
            }, onError: {
                error in
                print(error.toString())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: "OK", controller: self)
                }
            })
            BaseApp.sharedInstance.jobManager?.addOperation(createSingleAndGroupChat)
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }
}


