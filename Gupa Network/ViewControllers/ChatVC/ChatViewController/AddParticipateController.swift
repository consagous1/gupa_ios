//
//  AddParticipateController.swift
//  Gupa Network
//
//  Created by Apple on 22/01/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit

class AddParticipateController: UIViewController , UISearchBarDelegate, UISearchDisplayDelegate {

    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet weak var btnForCreateGroup: UIButton!
    @IBOutlet weak var searchBarText: UISearchBar!
    @IBOutlet weak var tblForGroup: UITableView!
    @IBOutlet weak var bottomConstForTblView: NSLayoutConstraint!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var arrForNewGroup         = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
    var arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
    
    var arrForSelectedUserChat = NSMutableSet()
    
    var isSearching = false
    var integerCount = 0
    var strGroupName = ""
    var strRoomId = ""

    fileprivate var chatViewController : ChatViewController?
    
    //****************************************************
    // MARK: - Life Cycle Methods
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Called Group View Controller")
        
        self.title = strGroupName
        searchBarText.delegate = self
        searchBarText.returnKeyType = UIReturnKeyType.done
        tblForGroup.tableFooterView = UIView()
        
        self.tblForGroup.allowsMultipleSelection = true
        
        appDelegate.centerNav = self.navigationController
        
        //SearchBar Placeholder
        if let txfSearchField = searchBarText.value(forKey: "_searchField") as? UITextField {
            txfSearchField.borderStyle = .none
            txfSearchField.backgroundColor = .clear
        }
        searchBarText.searchTextPositionAdjustment = UIOffsetMake(10.0, 0.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.serverAPI()
        self.navigationController?.isNavigationBarHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewGroupViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewGroupViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func methodBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func addNewParticipateAction(_ sender: Any) {
        
        if arrForSelectedUserChat.count > 0 {
        
        let arrForUser = self.arrForSelectedUserChat.allObjects as! [APIResponseParam.AllUserListInfo.AllUserListInfoData]
                
          if(arrForUser.count > 0){
                var userlist:[APIRequestParam.AddMemberData] = []
                var userData:APIRequestParam.AddMemberData?
                for userInfo in arrForUser {
                    userData = APIRequestParam.AddMemberData(_id: userInfo._id ?? "")
                    userlist.append(userData!)
                }
                    self.serApiForAddNewparticipate(userList: userlist)
                }
        } else {
            BaseApp.sharedInstance.showAlertViewControllerWith(title: "", message: "Please select atleast one person.", buttonTitle: "OK", controller: self)
        }
    }
   
    //****************************************************
    // MARK: - Search Methods
    //****************************************************
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            
            self.arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
            self.arrForFilteredUserChat = self.arrForNewGroup
            self.tblForGroup.reloadData()
            
        } else {
            isSearching = true
            
            self.arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
            
            for i in 0..<self.arrForNewGroup.count {
                let userData = self.arrForNewGroup[i]
                var strFullName = ""
                if userData.last_name != "" {
                    strFullName = "\(userData.first_name!)" + "\(userData.last_name!)"
                } else {
                    strFullName = "\(userData.first_name!)"
                }
                if (strFullName.lowercased().contains(searchText.lowercased())) {
                    self.arrForFilteredUserChat.append(userData)
                }
            }
            self.tblForGroup.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    //MARK: Keyboard Methods
    @objc func keyboardWillHide(_ sender: Notification) {
        
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                //key point 0,
                self.bottomConstForTblView.constant =  0
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardWillShow(_ sender: Notification) {
        
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.bottomConstForTblView.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
}

extension AddParticipateController : UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrForFilteredUserChat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupUserCell", for: indexPath) as! GroupTableViewCell
        
        let userData = arrForFilteredUserChat[indexPath.row]
        
        var strFullName = ""
        var strFullNameImg = ""
        
        if userData.last_name != "" {
            strFullName = "\(userData.first_name!)" + " " + "\(userData.last_name!)"
            strFullNameImg = (userData.first_name?.first?.description ?? "") + (userData.last_name?.first?.description ?? "")
        } else {
            strFullName = "\(userData.first_name!)"
            strFullNameImg = (userData.first_name?.first?.description ?? "")
        }
        cell.lblTitleName.text = strFullName.capitalized
        cell.btnForTitleImg.setTitle(strFullNameImg.uppercased(), for: .normal)
        
        if arrForSelectedUserChat.contains(userData) {
            cell.btnForToggle.setImage(#imageLiteral(resourceName: "radioselect"), for: .normal)
        } else {
            cell.btnForToggle.setImage(#imageLiteral(resourceName: "radio_normal"), for: .normal)
        }
    
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Row is \(indexPath.row)")
        
        let item = arrForFilteredUserChat[indexPath.row]
        if arrForSelectedUserChat.contains(item) {
            arrForSelectedUserChat.remove(item)
        } else {
            arrForSelectedUserChat.add(item)
        }
        
        if arrForSelectedUserChat.count > 0 {
            btnForCreateGroup.isEnabled = true
        } else {
            btnForCreateGroup.isEnabled = false
        }
        self.tblForGroup.reloadData()
    }
}


// MARK:- API calling
extension AddParticipateController {
    
    func serverAPI(){
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            
            let allUserRequestParam = APIRequestParam.AllUserListInfo()
            
            let allUserList = AddParticipateInGroup(token: "" , roomId: strRoomId, userData: allUserRequestParam, onSuccess: {
                response in
                
                print(response.toJSON())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    var arrUserResponse = response.allUserListInfoData!
                    if let index = arrUserResponse.index(where: { $0._id == ApplicationPreference.getMongoUserId()}) {
                        arrUserResponse.remove(at: index)
                    }
                    
                    self.arrForNewGroup = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
                    self.arrForNewGroup = arrUserResponse
                    self.arrForFilteredUserChat = [APIResponseParam.AllUserListInfo.AllUserListInfoData]()
                    self.arrForFilteredUserChat = arrUserResponse
                    
                    BaseApp.sharedInstance.hideProgressHudView()
                    self.tblForGroup.reloadData()
                }
            }, onError: {
                error in
                print(error.toString())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: "OK", controller: self)
                }
            })
            BaseApp.sharedInstance.jobManager?.addOperation(allUserList)
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }
    
    func serApiForAddNewparticipate(userList: [APIRequestParam.AddMemberData]) {
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            
            let requestparam = APIRequestParam.Addparticiaptes(group_Id: strRoomId, userLists: userList)
            
            let createSingleAndGroupChat = AddNewParticipates(createSingleAndGroupChatData : requestparam, onSuccess: {
                response in
                
                print(response.toJSON())
                
                OperationQueue.main.addOperation() {
                    BaseApp.sharedInstance.hideProgressHudView()
                   self.navigationController?.popViewController(animated: true)
                }
            }, onError: {
                error in
                print(error.toString())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: "OK", controller: self)
                }
            })
            BaseApp.sharedInstance.jobManager?.addOperation(createSingleAndGroupChat)
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }

}
