//
//  PopUpVC.swift
//  TextApp
//
//  Created by Apple on 11/8/17.
//  Copyright © 2017 Consagous. All rights reserved.
//


enum OptionType {
    case NewChat
    case NewGroup
}

protocol popUpViewControllerDelegate {
    func removePopupViewControllerScr()
    func openNewChatViewController()
    func openNewGroupViewController()
}


import UIKit

class PopUpVC: UIViewController {

    @IBOutlet weak var viewForPopUp: UIView!
    @IBOutlet weak var tableViewForPopUp: UITableView!
    
    var mainDataforArray = [chatData] ()
    var delegate:popUpViewControllerDelegate?

    var dataForChat : chatData?
    var dataForChat1 : chatData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            dataForChat = chatData(id: "1", title: "New Chat", status: "ON", dateString: "Nil", labelCount: "NA")
            dataForChat1 = chatData(id: "1", title: "New Group", status: "ON", dateString: "Nil", labelCount: "NA")
        
            mainDataforArray.append(dataForChat!)
            mainDataforArray.append(dataForChat1!)
        
         appDelegate.centerNav = self.navigationController
        
            self.tableViewForPopUp.register(UITableViewCell.self, forCellReuseIdentifier: "popUpCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func methodBackAction(_ sender: Any) {
        
        print("Back Action Called");
        removePopUpView()
    }
    
    func removePopUpView(){
        if(delegate != nil){
            delegate?.removePopupViewControllerScr()
        }
    }
    
    
    func openViewControllerWithOptionType(optionType:OptionType) {
    
        
        switch optionType {
            case .NewChat:
              if(delegate != nil){
                delegate?.openNewChatViewController()
              }
            break
            case .NewGroup:
                if(delegate != nil){
                    delegate?.openNewGroupViewController()
                }
            break
        }
    }
}

extension PopUpVC : UITableViewDelegate , UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainDataforArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "popUpCell", for: indexPath)
        
        let valForChatData = mainDataforArray[indexPath.row]
        cell.textLabel?.text = valForChatData.title
        cell.backgroundColor = UIColor.clear
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch indexPath.row {
        case 0:
            openViewControllerWithOptionType(optionType: .NewChat)
            break
        case 1:
            openViewControllerWithOptionType(optionType: .NewGroup)
            break
        default:
            break
        }
        
        removePopUpView()

    }
}
