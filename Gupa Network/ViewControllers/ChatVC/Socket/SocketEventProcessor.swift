//
//  SocketEventProcessor.swift
//  TextApp
//


import Foundation

class SocketEventProcessor{
    /**
     * Process Events which are received on Socket
     * @param eventName Event Name
     * @param response Response Object
     */
    func processEvent(_ eventName: String, response: AnyObject?){}
    
    /**
     * Process Emit Acknowledgement after emitting event on Socket
     * @param eventName Event Name
     * @param request Request Object sent
     * @param error Error Object in case of emit failed
     * @param response Response Object in case of successful Emits
     */
    func processEventAck(_ eventName: String, request: AnyObject, error: SocketPayloadView.CommonEmitErrorView?, response: AnyObject?){}
}
