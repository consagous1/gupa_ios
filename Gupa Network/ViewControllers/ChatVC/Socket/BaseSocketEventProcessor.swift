//
//  BaseSocketEventProcessor.swift
//  TextApp
//


import Foundation

class BaseSocketEventProcessor: SocketEventProcessor {
    /**
     * Process events, called from Socket Listener call()
     * @param eventName on event
     * @param response response object to be processed, null in case of error
     */
    override func processEvent(_ eventName: String, response: AnyObject?){
        AppConstant.kLogString("Base processEvent", eventName, response as Any);
        if ApplicationPreference.getMongoUserId() != nil {
            switch (eventName) {
                
            case SocketEvents.COMMON_CHAT_ALERT:
                AppConstant.klogString(response as! String)
                break
            case SocketEvents.COMMON_ON_CHAT_MESSAGE:
                // TODO: Manage code
                AppConstant.klogString(response as! String)
                break;
                
            case SocketEvents.COMMON_ON_UPDATE_CHAT:
                //print(response ?? "")
                
                var chatList:[SocketPayloadView.ChatReceive] = []
                
                if(response is NSArray){
                    let responseList = response as? NSArray
                    for jsonResultValue in responseList!{
                        let reqResponse = SocketPayloadView.ChatReceive().getModelObjectFromServerResponse(jsonResponse: jsonResultValue as AnyObject)
                        
                        chatList.append(reqResponse!)
                    }
                }
                
                SwiftEventBus.post(SocketEvents.COMMON_ON_UPDATE_CHAT, sender: chatList as AnyObject)
                
                break
                
            case SocketEvents.COMMON_ON_LAST_CHAT:
                print(response ?? "")
                var chatList:[SocketPayloadView.ChatReceive] = []
                
                if(response is NSDictionary){
                    let responseJson = response as? NSDictionary
                    let reqResponse = SocketPayloadView.ChatReceive().getModelObjectFromServerResponse(jsonResponse: responseJson as AnyObject)
                    
                    if(reqResponse != nil){
                        
                        //if(reqResponse?.receiver_id != nil && reqResponse?.receiver_id?.caseInsensitiveCompare(ApplicationPreference.getUserId()!) == ComparisonResult.orderedSame) || (reqResponse?.sender_id != nil && reqResponse?.sender_id?.caseInsensitiveCompare(ApplicationPreference.getUserId()!) == ComparisonResult.orderedSame) {
                            if(BaseApp.sharedInstance.currentChatRoomId != nil && (BaseApp.sharedInstance.currentChatRoomId?.caseInsensitiveCompare((reqResponse?.chatroom_id!)!) == ComparisonResult.orderedSame)){
                                
                                chatList.append(reqResponse!)
                                SwiftEventBus.post(SocketEvents.COMMON_ON_LAST_CHAT, sender: chatList as AnyObject)
                                SwiftEventBus.post(AppConstant.kRefreshHomeList, sender: chatList as AnyObject)
                            } else {
                               // BaseApp.sharedInstance.updateChatCount(roomId: (reqResponse?.chatroom_id!)!)
                            //    BaseApp.sharedInstance.showAlertViewControllerWith(title: "", message: reqResponse?.text ?? "", buttonTitle: nil, controller: nil)

                               // chatList.append(reqResponse!)
                               // SwiftEventBus.post(AppConstant.kRefreshHomeList, sender: chatList as AnyObject)
                            }
                        //}
                    }
                }
                break
                
            case SocketEvents.COMMON_ON_NOTIFY_LAST_MSG:
                print(response ?? "")
                var chatList:[SocketPayloadView.ChatReceiveWithExtraParamters] = []
                
                if(response is NSDictionary){
                    let responseJson = response as? NSDictionary
                    //let reqResponse = SocketPayloadView.SingleChat().getModelObjectFromServerResponse(jsonResponse: responseJson as AnyObject)
                    let reqResponse = SocketPayloadView.ChatReceiveWithExtraParamters().getModelObjectFromServerResponse(jsonResponse: responseJson as AnyObject)
                    if(reqResponse != nil) {
                        //TODO commented all header notifications
                        if reqResponse?.is_group == false {
                            if(reqResponse?.receiver_id != nil && reqResponse?.receiver_id?.caseInsensitiveCompare(ApplicationPreference.getMongoUserId()!) == ComparisonResult.orderedSame) || (reqResponse?.sender_id != nil && reqResponse?.sender_id?.caseInsensitiveCompare(ApplicationPreference.getMongoUserId()!) == ComparisonResult.orderedSame) {
                                if(BaseApp.sharedInstance.currentChatRoomId != nil && (BaseApp.sharedInstance.currentChatRoomId?.caseInsensitiveCompare((reqResponse?.chatroom_id!)!) == ComparisonResult.orderedSame)){
                                    
                                    chatList.append(reqResponse!)
                                    SwiftEventBus.post(AppConstant.kRefreshHomeList, sender: chatList as AnyObject)
                                } else {
                                    BaseApp.sharedInstance.updateChatCount(roomId: (reqResponse?.chatroom_id!)!)
                                    //BaseApp.sharedInstance.showAlertViewControllerWith(title: "", message: reqResponse?.text ?? "", buttonTitle: nil, controller: nil)
                                    
                                    chatList.append(reqResponse!)
                                    SwiftEventBus.post(SocketEvents.COMMON_ON_NOTIFY_LAST_MSG, sender: chatList as AnyObject)
                                    SwiftEventBus.post(AppConstant.kRefreshHomeList, sender: chatList as AnyObject)
                                }
                            } else {
                                //TODO commented all header notifications
                                BaseApp.sharedInstance.updateChatCount(roomId: (reqResponse?.chatroom_id!)!)
                                //BaseApp.sharedInstance.showAlertViewControllerWith(title: "", message: reqResponse?.text ?? "", buttonTitle: nil, controller: nil)
                                
                                chatList.append(reqResponse!)
                                SwiftEventBus.post(AppConstant.kRefreshHomeList, sender: chatList as AnyObject)
                            }
                        } else {
                            let groupIds = reqResponse?.groupUsersArray
                            if (groupIds?.contains(ApplicationPreference.getMongoUserId()!))! {
                                BaseApp.sharedInstance.updateChatCount(roomId: (reqResponse?.chatroom_id!)!)
                                //BaseApp.sharedInstance.showAlertViewControllerWith(title: "", message: reqResponse?.text ?? "", buttonTitle: nil, controller: nil)
                                
                                chatList.append(reqResponse!)
                                SwiftEventBus.post(SocketEvents.COMMON_ON_NOTIFY_LAST_MSG, sender: chatList as AnyObject)
                                SwiftEventBus.post(AppConstant.kRefreshHomeList, sender: chatList as AnyObject)
                            }
                        }
                    }
                }
                
                break
                
            case SocketEvents.COMMON_ON_CLEAR_CHAT:
                // TODO: Manage code
                AppConstant.klogString(response as! String)
                break;
                
            case SocketEvents.COMMON_ON_MSG_DELETE_CHAT:
                // TODO: Manage code
                if(response is NSDictionary){
                    let responseJson = response as? NSDictionary
                    SwiftEventBus.post(SocketEvents.COMMON_ON_MSG_DELETE_CHAT, sender: responseJson as AnyObject)
                }
                break;
                
            //Private grp data
            case SocketEvents.COMMON_ON_GET_PRIVATE_USER_INFO:
                //TODO: Manage code
                var chatList:[SocketPayloadView.PrivateGroupUserInfo] = []
                
                if(response is NSArray){
                    let responseList = response as? NSArray
                    for jsonResultValue in responseList!{
                        let reqResponse = SocketPayloadView.PrivateGroupUserInfo().getModelObjectFromServerResponse(jsonResponse: jsonResultValue as AnyObject)
                        
                        chatList.append(reqResponse!)
                    }
                }
                SwiftEventBus.post(AppConstant.kRefreshHomeListFromSocket, sender: chatList as AnyObject)
                break
                
            case SocketEvents.COMMON_ON_PREVOIUS_MORE:
                //TODO: Manage code
                var chatList:[SocketPayloadView.ChatReceive] = []
                
                if(response is NSArray){
                    let responseList = response as? NSArray
                    for jsonResultValue in responseList!{
                        let reqResponse = SocketPayloadView.ChatReceive().getModelObjectFromServerResponse(jsonResponse: jsonResultValue as AnyObject)
                        
                        chatList.append(reqResponse!)
                    }
                }
                SwiftEventBus.post(SocketEvents.COMMON_ON_PREVOIUS_MORE, sender: chatList as AnyObject)
                break
                
            //
            case SocketEvents.COMMON_ON_GET_CHAT_USER_GROUP_INFO:
                var userList:[SocketPayloadView.PrivateGroupUserInfo] = []
                
                if(response is NSArray){
                    let responseList = response as? NSArray
                    for jsonResultValue in responseList!{
                        let reqResponse = SocketPayloadView.PrivateGroupUserInfo().getModelObjectFromServerResponse(jsonResponse: jsonResultValue as AnyObject)
                        if (reqResponse?._id != nil && reqResponse?._id?.caseInsensitiveCompare(ApplicationPreference.getMongoUserId()!) != ComparisonResult.orderedSame) {
                            userList.append(reqResponse!)
                        }
                    }
                }
                SwiftEventBus.post(AppConstant.kGetGroupInfoOnChatList, sender: userList as AnyObject)
                break
                
            case SocketEvents.COMMON_ON_NOTIFY_LAST_SEEN_STATUS:
                print(response ?? "")
                var chatList:[SocketPayloadView.ChatReceive] = []
                
                if(response is NSDictionary){
                    let responseJson = response as? NSDictionary
                    let reqResponse = SocketPayloadView.ChatReceive().getModelObjectFromServerResponse(jsonResponse: responseJson as AnyObject)
                    
                    if(reqResponse != nil){
         
                        if(BaseApp.sharedInstance.currentChatRoomId != nil && (BaseApp.sharedInstance.currentChatRoomId?.caseInsensitiveCompare((reqResponse?.chatroom_id!)!) == ComparisonResult.orderedSame)){
                            
                            chatList.append(reqResponse!)
                            SwiftEventBus.post(SocketEvents.COMMON_ON_NOTIFY_LAST_SEEN_STATUS, sender: chatList as AnyObject)
                        }
                    }
                }
                break
                
            case SocketEvents.COMMON_ON_USER_ONLINE_OFFLINE_STATUS:
                print(response ?? "")
                var onlineOfflineStatus:[SocketPayloadView.OnlineOfflineStatus] = []
                
                if(response is NSDictionary){
                    let responseJson = response as? NSDictionary
                    let reqResponse = SocketPayloadView.OnlineOfflineStatus().getModelObjectFromServerResponse(jsonResponse: responseJson as AnyObject)
                    
                    if(reqResponse != nil){
                        onlineOfflineStatus.append(reqResponse!)
                        SwiftEventBus.post(SocketEvents.COMMON_ON_USER_ONLINE_OFFLINE_STATUS, sender: onlineOfflineStatus as AnyObject)
                    }
                }
                break
                
            case SocketEvents.COMMON_ON_TALK_ROOM_CONNECTED:
                print(response ?? "")
                // TODO: Manage code
                if(response is NSDictionary){
                    let responseJson = response as? NSDictionary
                    SwiftEventBus.post(SocketEvents.COMMON_ON_TALK_ROOM_CONNECTED, sender: responseJson as AnyObject)
                }
                break
                
            case SocketEvents.COMMON_ON_GROUP_DELETETED:
                
                print("2222222 response==== \(response ?? "" as AnyObject)")
                
                if(BaseApp.sharedInstance.currentChatRoomId != nil && (BaseApp.sharedInstance.currentChatRoomId?.caseInsensitiveCompare(response as! String) == ComparisonResult.orderedSame)){
                    
                    SwiftEventBus.post(SocketEvents.COMMON_ON_GROUP_DELETETED, sender: response as AnyObject)
                }
                
//                var chatList:[SocketPayloadView.ChatReceive] = []
//
//                if(response != nil){
//
//                    if(BaseApp.sharedInstance.currentChatRoomId != nil && (BaseApp.sharedInstance.currentChatRoomId?.caseInsensitiveCompare(response.chatroom_id!) == ComparisonResult.orderedSame)){
//
//                        chatList.append(response! as! SocketPayloadView.ChatReceive)
//                        SwiftEventBus.post(SocketEvents.COMMON_ON_GROUP_DELETETED, sender: response as AnyObject)
//                    }
//                }
//
//                if(response is NSDictionary){
//                    let responseJson = response as? NSDictionary
//                    SwiftEventBus.post(SocketEvents.COMMON_ON_GROUP_DELETETED, sender: responseJson as AnyObject)
//                }
                break
                
            default:
                AppConstant.klogString("processEvent: NO CASE TO HANDLE EVENT: "+eventName);
                break;
            }
        }
    }
    
    /**
     * Process events, called from Socket emit -> ack -> call()
     * @param eventName on event
     * @param request request object of which ACK it is
     * @param error error object if any, null in case of successful response
     * @param response response object to be processed, null in case of error
     */
    override func processEventAck(_ eventName: String, request: AnyObject, error: SocketPayloadView.CommonEmitErrorView?, response: AnyObject?){
        AppConstant.kLogString("Base processEventAck", eventName, error, response);
        var success = false
        
        if(error == nil){
            success = true
        }
        
        //        switch (eventName) {
        //        case SocketEvents.: // TODO: specify ack event name
        //            if(success) {
        //                }
        //            }else{
        //
        //            }
        //            return;
        //
        //        default:
        //            AppConstant.kLogString("processEventAck: NO CASE TO HANDLE EVENT: "+eventName, request, error, response)
        //            break;
        //        }
    }
}
