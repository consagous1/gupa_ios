//
//  DefaultSocketAckListener.swift
//  TextApp
//

import Foundation

class DefaultSocketAckListener  {
    private var eventName:String? = ""
    private var request:AnyObject? = nil
    private var eventProcessor:SocketEventProcessor? = nil
    
    init(eventName: String, request: AnyObject, eventProcessor: SocketEventProcessor){
        self.eventName = eventName
        self.request = request
        self.eventProcessor = eventProcessor
        SocketManager.sharedInstance.addToOnGoingSockAckListenerList(self)
    }
    
    func call(args: [AnyObject], emitter: SocketAckEmitter) -> Void {
        AppConstant.kLogString(args, self.eventName as Any)
        
        SocketManager.sharedInstance.removeFromOnGoingSockAckListenerList(self) //we have got the ack for it, remove it from pending list.
        var responseData:AnyObject? = nil
        var errorData:SocketPayloadView.CommonEmitErrorView? = nil
        var ack:AnyObject? = nil
        AppConstant.kLogString("DefaultSocketAckListener: call",eventName as Any, args)
        if(args.count > 0) {
            ack = args[0]
            if (ack != nil) {
                errorData = SocketPayloadView.CommonEmitErrorView().getObjectFromString(jsonResponse: ack!)
            } else if(args.count > 1) {
                responseData = args[1]
            }
        }else{ //assume success
            AppConstant.kLogString("Empty response from server in callback")
        }
        if(eventProcessor != nil){
            eventProcessor!.processEventAck(eventName!, request: request!, error: errorData!, response: responseData)
        }
    }
    
    func callAck(args: [AnyObject]) -> Void {
        AppConstant.kLogString(args, self.eventName)
        
        SocketManager.sharedInstance.removeFromOnGoingSockAckListenerList(self) //we have got the ack for it, remove it from pending list.
        var responseData:AnyObject! = nil
        var errorData:SocketPayloadView.CommonEmitErrorView! = nil
        var ack:AnyObject? = nil
        var dataArray:[AnyObject]? = nil
        //arg[0] error //arg[1] data
        AppConstant.kLogString("DefaultSocketAckListener: call",eventName, args)
        if(args.count > 0) {
            dataArray = args[0] as? [AnyObject]
            if(dataArray != nil && dataArray!.count > 0){
                ack = dataArray![0]
                if(ack is NSNull){
                    responseData = dataArray![1]
                }else if(ack != nil){
                    errorData = SocketPayloadView.CommonEmitErrorView().getObjectFromString(jsonResponse: ack!)
                }
            }
        }else{ //assume success
            AppConstant.kLogString("Empty response from server in callback")
        }
        if(eventProcessor != nil){
            eventProcessor!.processEventAck(eventName!, request: request!, error: errorData, response: responseData)
        }
    }
    
    /**
     * Calling forced error ack, useful for socket disconnected without getting a callback call on this.
     * @param errorData error data
     */
    func forcedCallWithError(errorData: SocketPayloadView.CommonEmitErrorView?){
        if(eventProcessor != nil){
            eventProcessor!.processEventAck(eventName!, request: request!, error: errorData!, response: nil);
        }
    }
}
