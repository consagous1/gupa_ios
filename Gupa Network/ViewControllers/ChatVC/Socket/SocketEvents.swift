//
//  SocketEvents.swift
//  TextApp
//

import Foundation

public class SocketEvents {
    /*------------------ Common Socket.IO Events ---------------------*/
    //Receive Events: (Handled in Socket Manager only)
    internal static var COMMON_SOCKET_CUSTOM_PING_EVENT = "t_ping"
    internal static var COMMON_ON_SOCKET_CONNECT_EVENT = "connect"
    internal static var COMMON_ON_SOCKET_CONNECTING_EVENT = "connecting"
    internal static var COMMON_ON_SOCKET_RECONNECT_EVENT = "reconnect"
    internal static var COMMON_ON_SOCKET_DISCONNECT_EVENT = "disconnect"
    internal static var COMMON_ON_SOCKET_ERROR_EVENT = "error"
    /*=============================================================*/
    
    /*------------------ Common Socket.IO Chat Events ---------------------*/
    //Emit Events: (Handled in Socket Manager only)
    internal static var COMMON_CHAT_ROOM_ID = "room_id"
    internal static var COMMON_CHAT_MESSAGE = "message"
    internal static var COMMON_CHAT_SEEN = "seen"
    internal static var COMMON_CHAT_ALERT = "alert"
    
    ////Receive events
    internal static var COMMON_ON_CHAT_MESSAGE = "message"
    internal static var COMMON_ON_UPDATE_CHAT = "updatechat"
    internal static var COMMON_ON_SEND_CHAT = "sendchat"
    internal static var COMMON_ON_ADD_ROOM_ID = "addUser"
    internal static var COMMON_ON_SINGLE_CHAT = "singleChat"
    internal static var COMMON_ON_LAST_CHAT = "updateLastMessage"
    internal static var COMMON_ON_NOTIFY_LAST_MSG = "notifyNewMsg"
    internal static var COMMON_ON_CLEAR_CHAT = "clearChat"
    internal static var COMMON_ON_DELETE_CHAT = "deleteMessages"
    internal static var COMMON_ON_MSG_DELETE_CHAT = "messageDeleteResponse"
    internal static var COMMON_ON_UPDATE_LOCATION = "updateLocation"
    internal static var COMMON_ON_GROUP_DELETE_BYADMIN = "groupDelete"
    internal static var COMMON_ON_GROUP_DELETETED = "groupDeleted"
    

    //Get user info
    internal static var COMMON_ON_GET_USER_INFO = "getPrivateGroupUserInfo"
    internal static var COMMON_ON_GET_PRIVATE_USER_INFO = "privateGroupUsersData"
    internal static var COMMON_ON_USER_ONLINE_STATUS = "onlineUsers"

    internal static var COMMON_ON_LOAD_MORE = "loadMoreMessage"
    internal static var COMMON_ON_PREVOIUS_MORE = "previousMessages"
    
    //
    internal static var COMMON_ON_CHAT_USER_GROUP_INFO = "getGroupUsers"
    internal static var COMMON_ON_GET_CHAT_USER_GROUP_INFO = "groupUsersInfo"

    //
    internal static var COMMON_ON_UPDATE_LAST_SEEN_STATUS = "updateLastMsgSeen"
    internal static var COMMON_ON_NOTIFY_LAST_SEEN_STATUS = "notifySeenStatus"

    //Talk box related
    internal static var COMMON_ON_CREATE_TALK_ROOM = "creatTalkRoom"
    internal static var COMMON_ON_TALK_ROOM_CONNECTED = "talkRoomConnected"
    internal static var COMMON_ON_CONNECT_CALL = "connectCall"
    internal static var COMMON_ON_NOTIFY_INCOMING_CALL = "notifyIncomingCall"

    //Online Offline Status
    internal static var COMMON_ON_OFFLINE = "imOffline"
    internal static var COMMON_ON_ONLINE = "imOnline"
    internal static var COMMON_ON_USER_ONLINE_OFFLINE_STATUS = "userOnlineStatus"
    /*=============================================================*/
}
