//
//  SocketPacketProcessor.swift
//  TextApp
//


import Foundation
/**
 * Socket Message Payload Processor
 */
class SocketPacketProcessor {
    static let sharedInstance = SocketPacketProcessor()
    
    init(){}
    
    /**
     * Process Socket actual Packet Message Payload, Tokenize and handle accordingly.
     * @param payload Message Packet payload.
     */
    func processPacket(payload:String?, fromSocket:Bool){
        if(payload == nil){
            return
        }
        
        AppConstant.kLogString("processPacket called payload: %@", payload!)
        
        let token = ";"
        
        //pattern is id;action;actual-payload
        let payloadItemList = payload?.components(separatedBy: token)
        if(payloadItemList?.count == 3){
            let id = payloadItemList?[0]
            let action = payloadItemList?[1]
            let actualPayload = payloadItemList?[2]
            //FlumMessageProcessor.sharedInstance.processPacketData(id: id, action: action, payload: actualPayload, fromSocket: fromSocket)
            
        }else if(payloadItemList?.count == 2){
            AppConstant.kLogString("processPacket unable to process - firstTokenIndex: %@  secondTokenIndex: %@", payloadItemList?[0] ?? "", payloadItemList?[1] ?? "");
        }
    }
}
