//
//  SocketErrorCodes.swift
//  TextApp
//

import Foundation

class SocketErrorCodes {
    static var INTERNAL_CODE_SOCKET_NOT_CONNECTED = 1000; /*In case of any emit on non connected socket*/
}
