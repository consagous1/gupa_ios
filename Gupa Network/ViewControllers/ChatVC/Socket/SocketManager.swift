//
//  SocketManager.swift
//  TextApp
//

import Foundation

class SocketManager{
    
    internal enum SocketStatus{
        case NONE,
        NO_NETWORK,
        CONNECTING,
        CONNECTED,
        DISCONNECTED,
        UNAUTHORIZED,
        ERROR
    }
    
    let CLOSE_CODE = 1010
    static let sharedInstance = SocketManager()
//    var webSocketInstance:WebSocket?

    //let socket = SocketIOClient(socketURL: URL(string:"http://localhost:8900")!)
    var socket:SocketIOClient?
    
    let pollingInterval = 1 // seconds
    
    var isSocketForceFullyClosed = false
    
    private var requestStateFlag:Bool? = false
    private var socketStatus:SocketStatus? = SocketStatus.NONE
    
    private var eventProcessor:BaseSocketEventProcessor? = nil
    private var eventListForDefaultListener:NSMutableArray? = nil
    private var socketAckListenerList:NSMutableArray? = nil
    
    /**
     * Connect Socket on given Url
     */
    
    init () {
        requestStateFlag = true
        socketAckListenerList = NSMutableArray()
        eventProcessor = BaseSocketEventProcessor()
    }
    
    func setEventProcessor(eventProcessor: BaseSocketEventProcessor) {
        self.eventProcessor = eventProcessor;
        AppConstant.kLogString("setEventProcessor", eventProcessor)
    }
    
    /**
     * @return get the current socket event processor
     */
    func getEventProcessor() -> BaseSocketEventProcessor {
        return eventProcessor!
    }
    
    func setSocketStatus(status: SocketStatus) {
        let statusChanged = (socketStatus != status) || status == SocketStatus.CONNECTED; //force on Connected.
        socketStatus = status;
        if(statusChanged) {
            //BaseApp.getInstance().getEventBus().post(new Events.SocketStatusChangedEvent(socketStatus));
        }
    }
    
    /**
     * Socket reconnect required, due to token changed.
     */
    func reConnectSocket(){
        AppConstant.kLogString("reConnectSocket called on socket:"+socket.debugDescription);
        disconnectSocket();//try disconnecting first if any
        if(eventListForDefaultListener != nil){
            connectSocket(eventListForDefaultListener!, forceNew: true/*Force New*/)
        }
    }
    
    /**
     * Reconnect socket with flag to request state.
     */
    func reConnectSocketForState() {
        requestStateFlag = true;
        reConnectSocket();
    }
    
    /**
     * This will be called from Socket Service for Fresh connection.
     * @param eventListForDefaultListener list of event listeners
     */
    func connectSocket(_ eventListForDefaultListener: NSMutableArray){
        disconnectSocket();//try disconnecting first if any
        connectSocket(eventListForDefaultListener, forceNew: false);
    }
    
    /**
     *
     * @param eventListForDefaultListener list of event listeners
     * @param forceNew force new, in case of reconnect socket specially
     */
    private func connectSocket(_ eventListForDefaultListener: NSMutableArray, forceNew: Bool){
        
        self.eventListForDefaultListener = eventListForDefaultListener;
        
        if (socket == nil){
            let url = APIParamConstants.WebSocket.URL
            socket = SocketIOClient(socketURL: URL(string:url)!)
            
            //register generic listeners
            registerSocketListener(SocketEvents.COMMON_ON_SOCKET_CONNECT_EVENT, callback: onConnect)
            registerSocketListener(SocketEvents.COMMON_ON_SOCKET_DISCONNECT_EVENT, callback: onDisconnect)
            registerSocketListener(SocketEvents.COMMON_ON_SOCKET_ERROR_EVENT, callback: onError)
            registerSocketListener(SocketEvents.COMMON_ON_SOCKET_RECONNECT_EVENT, callback: onReconnect)
            registerSocketListener(SocketEvents.COMMON_ON_SOCKET_CONNECTING_EVENT, callback: onConnecting)
            
            // TODO: when implement common chat events
            for event in eventListForDefaultListener  {
                registerSocketListener(event as! String, callback: DefaultSocketEventListener(eventName: event as! String, eventProcessor: eventProcessor!).call)
                AppConstant.kLogString("Adding Default Event Listener for: "+(event as! String))
            }
            
            socket?.connect()
        }
        
    }
    
    func registerSocketListener(_ event: String, callback: @escaping NormalCallback){
        if(socket != nil){
            socket!.on(event, callback: callback)
        }
    }
    
    func unregisterSocketListener(event: String){
        if(socket != nil){
            socket!.off(event)
        }
    }
    
    func disconnectSocket() {
        if (socket != nil) {
            AppConstant.kLogString("disconnectSocket called")
            socket?.disconnect()
            socket?.removeAllHandlers()
            socket = nil
        }
    }
    
    private func onConnect(args: [Any], emitter: SocketAckEmitter){
        AppConstant.klogString("SocketService:handleConnect: socket Connect")
    }
    
    private func onDisconnect(args: [Any], emitter: SocketAckEmitter){
        AppConstant.klogString("SocketService:handleDisconnect: socket disconnect")
    }
    
    private func onError(args: [Any], emitter: SocketAckEmitter){
        AppConstant.klogString("SocketService:handleError:")
    }
    
    private func onReconnect(args: [Any], emitter: SocketAckEmitter){
        AppConstant.klogString("SocketService:handleReconnect:")
    }
    
    private func onConnecting(args: [Any], emitter: SocketAckEmitter){
        AppConstant.klogString("SocketService:handleAuthenticate:")
    }
    
    /**
     * This will emit data on Socket, if socket is in connected mode.
     * @param eventName event name
     * @param data Object data to be emit for the eventName
     */
    func emitData(_ eventName:String, data: AnyObject){
        emitData(eventName, data: data, processor: eventProcessor!)
    }
    
    func emitDataList(_ eventName:String, data: [Any]){
        emitDataList(eventName, dataList: data, processor: eventProcessor!)
    }
    
    func emitDataAck(_ eventName:String, data: AnyObject){
        emitDataAck(eventName, data: data, processor: eventProcessor!)
    }
    
    /**
     * Common method to Emit data on Socket
     * @param eventName Event Name
     * @param data Object data to be emit for the eventName
     * @param processor Process to Process Ack
     */
    private func emitData(_ eventName:String, data: AnyObject, processor: SocketEventProcessor){
        if(socket?.status == SocketIOClientStatus.connected){
            AppConstant.kLogString("emitData: type String", eventName, data)
            //socket!.emit(eventName, data, DefaultSocketAckListener(eventName: eventName, request: data, eventProcessor: processor)) // Android code
            socket!.emit(eventName, data as! SocketData)
            
        } else {
            handleSocketNotConnected(eventName, requestData: data);
        }
    }
    
    private func emitDataList(_ eventName:String, dataList: [Any], processor: SocketEventProcessor){
        if(socket?.status == SocketIOClientStatus.connected){
            AppConstant.kLogString("emitData: type String", eventName)
            socket!.emit(eventName, with: dataList)
            
        } else {
            handleSocketNotConnected(eventName, requestData: dataList as AnyObject);
        }
    }
    
    private func emitDataAck(_ eventName:String, data: AnyObject, processor: SocketEventProcessor){
        if(socket?.status == SocketIOClientStatus.connected){
            let eventAckListener = DefaultSocketAckListener(eventName: eventName, request: data, eventProcessor: processor)
//            socket!.emitWithAck(eventName, data as! SocketData)(timeoutAfter: 5){ ackData in
//                eventAckListener.callAck([ackData])
//            }
            
            socket?.emitWithAck(eventName, data as! SocketData).timingOut(after: 5, callback: { ackData in
                eventAckListener.callAck(args: ackData as [AnyObject])
            })
        }else {
            handleSocketNotConnected(eventName, requestData: data);
        }
    }
    
    /**
     * Adding listener to on going listeners to determine pending ack on socket disconnect
     * @param listener listener
     */
    func addToOnGoingSockAckListenerList(_ listener: DefaultSocketAckListener){
        socketAckListenerList?.add(listener)
    }
    
    /**
     * Removing listener from on going listeners list, as they got call from socket.
     * @param listener listener
     */
    func removeFromOnGoingSockAckListenerList(_ listener: DefaultSocketAckListener){
        socketAckListenerList?.remove(listener)
    }
    
    /**
     * This will flush pending (On going listeners did no receive ack callback before disconnection) with forced error callback.
     * It will allow every socket event emitter listener lifecycle complete, even socket gets disconnected before giving ack to them.
     */
    private func flushPendingAckListenerList(){
        let pendingListenersSize = socketAckListenerList?.count
        AppConstant.kLogString("flushPendingAckListenerList called pendingListenersSize: " + String(describing: pendingListenersSize))
        if(pendingListenersSize! > 0) {
            let errorView = SocketPayloadView.CommonEmitErrorView()
            errorView.code = SocketErrorCodes.INTERNAL_CODE_SOCKET_NOT_CONNECTED
            errorView.message = "Forced error ack for Pending listeners after Socket disconnected" //internal debug purpose only
            for listener in socketAckListenerList! {
                (listener as! DefaultSocketAckListener).forcedCallWithError(errorData: errorView)
            }
            socketAckListenerList?.removeAllObjects()
        }
    }
    
    /**
     * Handle case when Socket is not in connected state and we got emit request
     * @param eventName Event Name
     * @param requestData Request Data.
     */
    private func handleSocketNotConnected(_ eventName: String, requestData: AnyObject) {
        AppConstant.kLogString("emitData: SOCKET IS NOT IN CONNECTED STATE", socket, eventName)
        let error = SocketPayloadView.CommonEmitErrorView()
        error.code = SocketErrorCodes.INTERNAL_CODE_SOCKET_NOT_CONNECTED
        if(eventProcessor != nil) {
            eventProcessor!.processEventAck(eventName, request: requestData, error: error, response: nil)
        }
        if(socketStatus == SocketStatus.DISCONNECTED || socketStatus == SocketStatus.ERROR){
            AppConstant.kLogString("emitData: FORCE RECONNECT SOCKET for status: "+socketStatus.debugDescription)
            reConnectSocket();
        } else if (socketStatus == SocketStatus.UNAUTHORIZED){
            AppConstant.kLogString("emitData: FORCE RECONNECT SOCKET for status: "+socketStatus.debugDescription)
            //BaseApp.getInstance().getEventBus().postSticky(new Events.UserUnAuthorizedEvent());
        }
    }
    
    /**
     * @return true if status is CONNECTED, otherwise false
     */
    func isConnected() -> Bool{
        //return socketStatus == SocketStatus.CONNECTED;
        return socket?.status.rawValue == SocketStatus.CONNECTED.hashValue
    }
    
    /**
     * This will reset request state on connect flag to force new current_state update on onConnect
     */
    func resetCurrentStateFlag() {
        requestStateFlag = true;
    }
}


