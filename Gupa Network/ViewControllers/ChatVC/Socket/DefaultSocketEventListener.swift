//
//  DefaultSocketEventListener.swift
//  TextApp
//


import Foundation

class DefaultSocketEventListener {
    private var eventName:String? = nil
    private var eventProcessor:SocketEventProcessor? = nil
    
    init(eventName: String, eventProcessor: SocketEventProcessor){
        self.eventName = eventName;
        self.eventProcessor = eventProcessor;
    }
    
    func call(args: [Any], emitter: SocketAckEmitter) -> Void {
        
        
        AppConstant.kLogString(args, self.eventName as Any)
        var responseData:AnyObject? = nil
        //Added by Rahul for handlling of extra parameters
        let responseExtraData: NSMutableDictionary = NSMutableDictionary()
        
        // arg[0] data //arg[1] ack
        AppConstant.kLogString("DefaultSocketEventListener: call",eventName as Any, args)
        if args.count > 0 {
            if eventName == SocketEvents.COMMON_ON_NOTIFY_LAST_MSG {
                print(args)
                for i in 0..<args.count {
                    if i == 0 {
                        //responseData = args[0] as AnyObject
                        let resDict = args[0] as AnyObject
                        let msgData = resDict as! NSDictionary
                        for (key, value) in msgData {
                            responseExtraData.setValue(value, forKey: key as! String)
                        }
                    } else if i == 1 {
                        responseExtraData.setValue(args[1], forKey: "roomID")
                    } else if i == 2 {
                        responseExtraData.setValue(args[2], forKey: "sender_name")
                    } else if i == 3 {
                        responseExtraData.setValue(args[3], forKey: "is_group")
                    } else if i == 4 {
                        responseExtraData.setValue(args[4], forKey: "groupUsersArray")
                    }
                }
                print(responseExtraData)
                responseData = responseExtraData as NSDictionary
            } else {
                responseData = args[0] as AnyObject
            }

        }else{
            //TODO error in response format.
         //   AppConstant.kLogString("Invalid response from server")
        }
        if(eventProcessor != nil){
            eventProcessor!.processEvent(eventName!, response: responseData)
        }
    }
}
