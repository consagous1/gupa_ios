//
//  SocketPayloadView.swift
//  TextApp
//

import Foundation
import ObjectMapper

/**
 * Socket Payload Views
 */
class SocketPayloadView {
    
    class MessageAckView:Mappable {
        var ack = "ack"
        //@SerializedName("p")
        var messageId:String?
        
        init(messageId:String){
            self.messageId = messageId
        }
        
        // Convert Json string to class object
        func getObjectFromString(jsonResponse: AnyObject)->MessageAckView{
            var getDriverMarkersView:MessageAckView?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<MessageAckView>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        getDriverMarkersView = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<MessageAckView>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            getDriverMarkersView = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return getDriverMarkersView!
        }
        
        required init?(map: Map){
        }
        
        func mapping(map: Map){
            ack             <- map["a"]
            messageId       <- map["p"]
        }
    }
    
    class CommonEmitErrorView : Mappable{
        var code = 0 // Int
        var message = "" // String
        
        init(){
            
        }
        
        // Convert Json string to class object
        func getObjectFromString(jsonResponse: AnyObject)->CommonEmitErrorView{
            var commonEmitErrorView:CommonEmitErrorView?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<CommonEmitErrorView>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        commonEmitErrorView = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<CommonEmitErrorView>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            commonEmitErrorView = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return commonEmitErrorView!
        }
        
        func toString()->String{
            return "{code:"+String(code)+"msg: "+message+"}"
        }
        
        // MARK:- Mappable delegate method implementation
        required init?(map: Map) {
        }
        
        func mapping(map: Map){
            // TODO: Update key-value pair when socket response fetch
            code         <- map["code"]
            message      <- map["message"]
        }
    }
    
    class ChatReceive : Mappable{
        
        var _id:String?
        var chatroom_id:String?
        var is_deleted:NSNumber?
        var message_type:String?
        var msg_status:String?
        var readtimestamp:NSNumber?
        var receiver_id:String?
        var recievetimestamp:NSNumber?
        var sender_id:String?
        var sendtimestamp:NSNumber?
        var text:String?
        var userName:String?
        
        init(){
            
        }
        
        // Convert Json response to class object
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ChatReceive?{
            var chatReceive:ChatReceive?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<ChatReceive>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        chatReceive = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ChatReceive>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            chatReceive = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return chatReceive ?? nil
        }
        
        // MARK:- Mappable delegate method implementation
        required init?(map: Map) {
        }
        
        func mapping(map: Map){
            // TODO: Update key-value pair when socket response fetch
            _id                 <- map["_id"]
            chatroom_id         <- map["chatroom_id"]
            is_deleted          <- map["is_deleted"]
            message_type        <- map["message_type"]
            msg_status          <- map["msg_status"]
            readtimestamp       <- map["readtimestamp"]
            receiver_id         <- map["receiver_id"]
            recievetimestamp    <- map["recievetimestamp"]
            sender_id           <- map["sender_id"]
            sendtimestamp       <- map["sendtimestamp"]
            text                <- map["text"]
            userName            <- map["userName"]
        }
    }
    
    class ChatReceiveWithExtraParamters : Mappable{
        
        var _id:String?
        var chatroom_id:String?
        var is_deleted:NSNumber?
        var is_group: Bool?
        var message_type:String?
        var msg_status:String?
        var readtimestamp:NSNumber?
        var receiver_id:String?
        var recievetimestamp:NSNumber?
        var roomID: String?
        var sender_id:String?
        var sender_name:String?
        var sendtimestamp:NSNumber?
        var text:String?
        var userName:String?
        var groupUsersArray: [String]?
        
        init(){
            
        }
        
        // Convert Json response to class object
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ChatReceiveWithExtraParamters?{
            var chatReceiveWithExtraParamters:ChatReceiveWithExtraParamters?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<ChatReceiveWithExtraParamters>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        chatReceiveWithExtraParamters = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ChatReceiveWithExtraParamters>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            chatReceiveWithExtraParamters = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return chatReceiveWithExtraParamters ?? nil
        }
        
        // MARK:- Mappable delegate method implementation
        required init?(map: Map) {
        }
        
        func mapping(map: Map){
            // TODO: Update key-value pair when socket response fetch
            _id                 <- map["_id"]
            chatroom_id         <- map["chatroom_id"]
            groupUsersArray     <- map["groupUsersArray"]
            is_deleted          <- map["is_deleted"]
            is_group            <- map["is_group"]
            message_type        <- map["message_type"]
            msg_status          <- map["msg_status"]
            readtimestamp       <- map["readtimestamp"]
            receiver_id         <- map["receiver_id"]
            recievetimestamp    <- map["recievetimestamp"]
            roomID              <- map["roomID"]
            sender_id           <- map["sender_id"]
            sender_name         <- map["sender_name"]
            sendtimestamp       <- map["sendtimestamp"]
            text                <- map["text"]
            userName            <- map["userName"]
        }
    }
    
    //
    class OnlineOfflineStatus : Mappable{
        
        var id:String?
        var status:NSNumber?
        
        init(){
            
        }
        
        // Convert Json response to class object
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->OnlineOfflineStatus?{
            var onlineOfflineStatus:OnlineOfflineStatus?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<OnlineOfflineStatus>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        onlineOfflineStatus = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<OnlineOfflineStatus>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            onlineOfflineStatus = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return onlineOfflineStatus ?? nil
        }
        
        // MARK:- Mappable delegate method implementation
        required init?(map: Map) {
        }
        
        func mapping(map: Map){
            // TODO: Update key-value pair when socket response fetch
            id                 <- map["id"]
            status         <- map["status"]
        }
    }
    
    class SingleChat : Mappable{
        
        var chatroom_id:String?
        var receiver_id:String?
        var senderFullName:String?
        var sender_id:String?
        var text:String?
        var userName:String?
        
        init(){
            
        }
        
        // Convert Json string to class object
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->SingleChat?{
            var singleChat:SingleChat?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<SingleChat>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        singleChat = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<SingleChat>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            singleChat = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return singleChat ?? nil
        }
        
        // MARK:- Mappable delegate method implementation
        required init?(map: Map) {
        }
        
        func mapping(map: Map){
            // TODO: Update key-value pair when socket response fetch
            chatroom_id         <- map["chatroom_id"]
            receiver_id         <- map["receiver_id"]
            senderFullName       <- map["senderFullName"]
            sender_id           <- map["sender_id"]
            text                <- map["text"]
            userName            <- map["userName"]
        }
    }
    
    //
    class PrivateGroupUserInfo : Mappable{
        
        var _id:String?
        var email:String?
        var first_name:String?
        var image_path:String?
        var isContact:NSNumber?
        var lang:String?
        var last_name:String?
        var lat:String?
        var location:String?
        var mobile:String?
        var online:Bool?
        var onlineTime:NSNumber?
        var password:String?
        var status:NSNumber?
        var type:String?
        var userToken:String?
        var userName:String?
     //   var id:String?

        init(){
            
        }
        
        // Convert Json response to class object
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PrivateGroupUserInfo?{
            var userInfo:PrivateGroupUserInfo?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<PrivateGroupUserInfo>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        userInfo = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<PrivateGroupUserInfo>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            userInfo = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return userInfo ?? nil
        }
        
        // MARK:- Mappable delegate method implementation
        required init?(map: Map) {
        }
        
        func mapping(map: Map){
            // TODO: Update key-value pair when socket response fetch
            _id           <- map["_id"]
            email         <- map["email"]
            first_name    <- map["first_name"]
            image_path    <- map["image_path"]
            isContact     <- map["isContact"]
            lang          <- map["lang"]
            last_name     <- map["last_name"]
            lat           <- map["lat"]
            location      <- map["location"]
            mobile        <- map["mobile"]
            online        <- map["online"]
            onlineTime    <- map["onlineTime"]
            password      <- map["password"]
            status        <- map["status"]
            type          <- map["type"]
            userToken     <- map["userToken"]
            userName      <- map["userName"]
         //   id            <- map["id"]

            
        }
    }
}
