//
//  APIParamConstants.swift
//  MyPanditJi
//

import Foundation
import UIKit

class APIParamConstants: NSObject {
    
    //internal static let API_VERSION = "1.0"
    internal static let API_VERSION = "1"

    // Api enviornment
    enum Env: String {
        case PRODUCTION = "_"
        case DEVELOPMENT = ""
    }
    
    
    //**********DEV***********
    //static let DEFAULT_ENV = Env.DEVELOPMENT.rawValue
    static let DEFAULT_ENV = Env.DEVELOPMENT.rawValue
    //
    //    #else
    //        //******** PROD *********
    //        static let DEFAULT_ENV = Env.PRODUCTION.rawValue
    //    #endif
    
    //TODO :- Need to change API  here
    
    internal static let kSERVER_END_POINT = BaseApi.getSelectedHttpEnvironment(DEFAULT_ENV).value(forKey: "SERVER_END_POINT") as! String
    internal static let kSERVER_INDEX_END_POINT = BaseApi.getSelectedHttpEnvironment(DEFAULT_ENV).value(forKey: "SERVER_INDEX_END_POINT") as! String
    
    
    //REST or SOCKET Headers
    class HEADERS {
        internal static let AUTHORIZATION        = "Authorization"
        internal static let CONTENT_TYPE         = "content-type"
        
    }
    
    //    class URLParam {
    //        internal static let ID = "id"
    //    }
    
    class WebSocket {
        
        //internal static let URL = "https://textingapp.herokuapp.com" // TODO: Specific socket base url
        //internal static let URL = "http://52.38.159.211:8080" // TODO: Specific socket base url
        //internal static let URL = "http://192.168.1.185:8080" // TODO: Specific socket base url
      //  internal static let URL = "http://52.25.119.166:8080"
      //  internal static let URL = "https://gupaapp.tk:8080"
        internal static let URL = "https://www.gupaapp.xyz:8080"


        internal static let TOKEN_PARAM = "tk"
    }
    
    class DIR {
        internal static let IMAGE_DIRECTORY_NAME = "textApp_image"
        internal static let PROFILE_IMAGE_FILE_NAME = "profile.tmp"
    }
    
    class MIME {
        internal static let IMAGE_JPEG = "image/jpeg"
        internal static let APP_PDF = "application/pdf"
        internal static let AUDIO_MP4 = "audio/mp4"
    }
    
    class NetworkAPIStatusCode{
        internal static let SUCCESS  = 200
    }
    
    //Image base url 
    //internal static let BASE_IMAGEURL = String(format: "https://textingapp.herokuapp.com/images/")
    internal static let BASE_MEDIAURL = String(format: "https://www.gupaapp.xyz:8080/images/")
    //internal static let BASE_MEDIAURL = String(format: "http://192.168.1.185:8080/images/")

    //User Login
    internal static let kLogin = "userlogin"
    //Forgot Password
    internal static let kForgotPassword = "api/v1/forgotpassword"
    //Reset Password
    internal static let kResetPassword = "api/v1/setpassword"
    //Change Password
    internal static let kChangePassword = "api/v1/changepassword"
    //Contact list for New Group Chat
    internal static let kContactList = "api/v1/contact"
    //User list for New chat
    internal static let kUserList = "api/v1/user/"
    //Home List
    internal static let kHomeList = "api/v1/privategroups/"
    
    //Create Room for single and group chat
    internal static let kCreateGroup = "api/v1/creategroup"
    
    //All User list for both chat
    internal static let kAllUserList = "api/v1/allusers"
    
    //Upload images ,audio
    internal static let kUploadFiles = "mobileUpload"
    
    internal static let kDeleteGroup = "api/v1/group/"
    
    internal static let KOtherUserList = "api/v1/getOtherUsers/"
    
    internal static let KAddNewMember = "api/v1/addparticipant/"


}
