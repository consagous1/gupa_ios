//
//  StructSwift.swift
//  TextApp
//
//  Created by Apple on 11/7/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import Foundation

struct MainData{
    
    var id:String?
    var title:String?
    var titleDescription:String?
    var status:String?
    var dateString : String?
    var labelCount : String?
    
    init(id:String,title : String , titleDescription : String , status : String , dateString : String , labelCount : String) {
        self.id = id
        self.title = title
        self.titleDescription = titleDescription
        self.status = status
        self.dateString = dateString
        self.labelCount = labelCount
    }
}

struct chatData{
    
    var id:String?
    var title:String?
    var status:String?
    var dateString : String?
    var labelCount : String?
    
    init(id:String,title : String, status : String , dateString : String , labelCount : String) {
        self.id = id
        self.title = title
        self.status = status
        self.dateString = dateString
        self.labelCount = labelCount
    }
}

struct newChatUserData {
    var _id:String?
    var email:String?
    var first_name:String?
    var image_path:String?
    var isContact:String?
    var lang:String?
    var last_name:String?
    var lat:String?
    var mobile:String?
    var online:String?
    var onlineTime:String?
    var password:String?
    var status:String?
    var type:String?
    var userToken:String?
    var user_group:String?
    var username:String?
    var fullName:String?
    var fullNameImg:String?
    var otherUserrId :String?

    init(_id:String,email : String, first_name : String, image_path : String, isContact : String, lang : String, last_name : String, lat : String, mobile : String, online : String, onlineTime : String, password : String, status : String, type : String, userToken : String, user_group : String, username : String , otherUserId : String) {
        self._id = _id
        self.email = email
        self.first_name = first_name
        self.image_path = image_path
        self.isContact = isContact
        self.lang = lang
        self.last_name = last_name
        self.lat = lat
        self.mobile = mobile
        self.online = online
        self.onlineTime = onlineTime
        self.password = password
        self.status = status
        self.type = type
        self.userToken = userToken
        self.user_group = user_group
        self.username = username
        self.otherUserrId = otherUserId

        var strFullName = ""
        var strFullNameImg = ""
        if last_name != "" {
            strFullName = first_name + last_name
            strFullNameImg = (first_name.first?.description ?? "") + (last_name.first?.description ?? "")
        } else {
            strFullName = first_name
            strFullNameImg = (first_name.first?.description ?? "")
        }
        self.fullName = strFullName
        self.fullNameImg = strFullNameImg
    }

}

struct newGroupUserData {
    var _id:String?
    var email:String?
    var first_name:String?
    var image_path:String?
    var isContact:String?
    var lang:String?
    var last_name:String?
    var lat:String?
    var mobile:String?
    var online:String?
    var onlineTime:String?
    var password:String?
    var status:String?
    var type:String?
    var userToken:String?
    var user_group:String?
    var username:String?
    var fullName:String?
    var fullNameImg:String?

    init(_id:String,email : String, first_name : String, image_path : String, isContact : String, lang : String, last_name : String, lat : String, mobile : String, online : String, onlineTime : String, password : String, status : String, type : String, userToken : String, user_group : String, username : String) {
        self._id = _id
        self.email = email
        self.first_name = first_name
        self.image_path = image_path
        self.isContact = isContact
        self.lang = lang
        self.last_name = last_name
        self.lat = lat
        self.mobile = mobile
        self.online = online
        self.onlineTime = onlineTime
        self.password = password
        self.status = status
        self.type = type
        self.userToken = userToken
        self.user_group = user_group
        self.username = username
        
        var strFullName = ""
        var strFullNameImg = ""
        if last_name != "" {
            strFullName = first_name + last_name
            strFullNameImg = (first_name.first?.description ?? "") + (last_name.first?.description ?? "")
        } else {
            strFullName = first_name
            strFullNameImg = (first_name.first?.description ?? "")
        }
        self.fullName = strFullName
        self.fullNameImg = strFullNameImg
    }

}

/*
 struct newChatUserData {
 var userid:String?
 var userName:String?
 var userStatus:String?
 
 init(userid:String,userName : String, userStatus : String) {
 self.userid = userid
 self.userName = userName
 self.userStatus = userStatus
 }
 
 }
 
 struct newGroupUserData {
 var userid:String?
 var userName:String?
 var userStatus:String?
 
 init(userid:String,userName : String, userStatus : String) {
 self.userid = userid
 self.userName = userName
 self.userStatus = userStatus
 }
 }

*/

