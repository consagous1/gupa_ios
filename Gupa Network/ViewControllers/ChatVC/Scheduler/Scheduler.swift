//
//  Scheduler.swift
//  TextApp
//


import Foundation

class Scheduler{
    
    var timer:Timer?
    var callBackMethod:(() -> Void)?
    
    init(delay:Double, callBackMethod:(() -> Void)?){
        self.callBackMethod = callBackMethod
        if(self.timer == nil){
            self.timer = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(schedulerSelectorAction), userInfo: nil, repeats: true)
        }
    }
    
    func stopScheduler(){
        if(self.timer != nil){
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    func isSchedulerIsOn() -> Bool{
        var isFound = false
        if(self.timer != nil){
            isFound = true
        }
        
        return isFound;
    }
    
    @objc func schedulerSelectorAction(){
        self.callBackMethod!()
    }
}
