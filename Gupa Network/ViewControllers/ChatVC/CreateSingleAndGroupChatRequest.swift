//
//  CreateSingleAndGroupChatRequest.swift
//  TextApp
//
//  Created by iMac on 12/8/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import Foundation

class CreateSingleAndGroupChatRequest: BaseRequest<APIResponseParam.BaseResponse> {
    
    private var token: String?
    private var userId: String?
    private var createSingleAndGroupChatData:APIRequestParam.CreateSingleAndGroupChatInfo?
    private var callBackSuccess:((_ response:APIResponseParam.BaseResponse)->Void)?
    private var callBackError:((_ response:ErrorModel)->Void)?
    
    init(token:String?, userId: String, createSingleAndGroupChatData:APIRequestParam.CreateSingleAndGroupChatInfo, onSuccess:((_ response:APIResponseParam.BaseResponse)->Void)?, onError:((_ response:ErrorModel)->Void)?){
        self.token = token
        self.userId = userId
        self.createSingleAndGroupChatData = createSingleAndGroupChatData
        self.callBackSuccess = onSuccess
        self.callBackError = onError
    }
    
    override func main() {
        
        //Prepare URL String
        let urlParameter = String(format:APIParamConstants.kCreateGroup)
        urlString = BaseApi().urlEncodedString(nil, restUrl: urlParameter, baseUrl: APIParamConstants.kSERVER_END_POINT)
        
        //TODO: Change with actual user current token
        header = BaseApi().getDefaultHeaders() as? [String : String]
        
        //Conver data to JSON String
        let validateOtpReqViewStr = createSingleAndGroupChatData?.toJSONString()
        
        //Set Method Type
        methodType = .POST
        
        //Set Post Data
        postData = validateOtpReqViewStr!.data(using: String.Encoding.utf8)!
        super.main()
    }
    
    override func onSuccess(_ responseView:APIResponseParam.BaseResponse?){
        if(callBackSuccess != nil){
            callBackSuccess!(responseView!)
        }
    }
    
    override func onError(_ response:ErrorModel) {
        if(callBackError != nil){
            callBackError!(response)
        }
    }
}
