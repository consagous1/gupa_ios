//
//  AddNewParticipates.swift
//  Gupa Network
//
//  Created by Apple on 23/01/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit

class AddNewParticipates: BaseRequest<APIResponseParam.BaseResponse> {
   
    private var createSingleAndGroupChatData:APIRequestParam.Addparticiaptes?
    private var callBackSuccess:((_ response:APIResponseParam.BaseResponse)->Void)?
    private var callBackError:((_ response:ErrorModel)->Void)?
    
    init(createSingleAndGroupChatData: APIRequestParam.Addparticiaptes ,onSuccess:((_ response:APIResponseParam.BaseResponse)->Void)?, onError:((_ response:ErrorModel)->Void)?){
        
        self.createSingleAndGroupChatData = createSingleAndGroupChatData
        self.callBackSuccess = onSuccess
        self.callBackError = onError
    }
    
    override func main() {
        
        //Prepare URL String
        let urlParameter = String(format:APIParamConstants.KAddNewMember)
        urlString = BaseApi().urlEncodedString(nil, restUrl: urlParameter, baseUrl: APIParamConstants.kSERVER_END_POINT)
        
        //TODO: Change with actual user current token
        header = BaseApi().getDefaultHeaders() as? [String : String]
        
        //Conver data to JSON String
        let validateOtpReqViewStr = createSingleAndGroupChatData?.toJSONString()
        
        //Set Method Type
        methodType = .POST
        
        //Set Post Data
        postData = validateOtpReqViewStr!.data(using: String.Encoding.utf8)!
        super.main()
    }
    
    override func onSuccess(_ responseView:APIResponseParam.BaseResponse?){
        AppConstant.kLogString(responseView ?? "")
        
        if(callBackSuccess != nil && responseView != nil){
            callBackSuccess!(responseView!)
        }
    }
    
    override func onError(_ response:ErrorModel) {
        AppConstant.kLogString(response)
        
        if(callBackError != nil){
            callBackError!(response)
        }
    }

}
