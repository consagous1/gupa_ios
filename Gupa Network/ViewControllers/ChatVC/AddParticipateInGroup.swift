//
//  AddParticipateInGroup.swift
//  Gupa Network
//
//  Created by Apple on 22/01/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit

class AddParticipateInGroup: BaseRequest<APIResponseParam.AllUserListInfo> {

    private var userListData:APIRequestParam.AllUserListInfo?
    private var callBackSuccess:((_ response:APIResponseParam.AllUserListInfo)->Void)?
    private var callBackError:((_ response:ErrorModel)->Void)?
    private var token: String?
    private var roomId: String?
    
    init(token:String?, roomId: String, userData:APIRequestParam.AllUserListInfo, onSuccess:((_ response:APIResponseParam.AllUserListInfo)->Void)?, onError:((_ response:ErrorModel)->Void)?){
        
        self.token = token
        self.roomId = roomId
        self.userListData = userData
        self.callBackSuccess = onSuccess
        self.callBackError = onError
    }
    
    override func main() {
        
        //Prepare URL String
        let urlParameter = String(format:APIParamConstants.KOtherUserList)
        urlString = BaseApi().urlEncodedString(nil, restUrl: urlParameter, baseUrl: APIParamConstants.kSERVER_END_POINT)
        
        //Get Header Parameters
        let extraParameter:[String:String] = [APIConfigConstants.kToken: "", APIConfigConstants.kGroupID: self.roomId!]
        header = BaseApi().getCustomHeaders(extraParameter: extraParameter) as? [String: String]
        
        //Set Method Type
        methodType = .POST
        
        super.main()
    }
    
    override func onSuccess(_ responseView:APIResponseParam.AllUserListInfo?){
        AppConstant.kLogString(responseView ?? "")
        
        if(callBackSuccess != nil && responseView != nil){
            callBackSuccess!(responseView!)
        }
    }
    
    override func onError(_ response:ErrorModel) {
        AppConstant.kLogString(response)
        
        if(callBackError != nil){
            callBackError!(response)
        }
    }
    
}
