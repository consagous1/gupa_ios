//
//  DeleteGroupRequest.swift
//  Gupa Network
//
//  Created by Apple on 22/01/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import Foundation

class DeleteGroupRequest: BaseRequest<APIResponseParam.BaseResponse> {
    
    private var userId: String?
    private var callBackSuccess:((_ response:APIResponseParam.BaseResponse)->Void)?
    private var callBackError:((_ response:ErrorModel)->Void)?
    
    init(userId: String, onSuccess:((_ response:APIResponseParam.BaseResponse)->Void)?, onError:((_ response:ErrorModel)->Void)?){
        self.userId = userId
        self.callBackSuccess = onSuccess
        self.callBackError = onError
    }
    
    override func main() {
    
        //Prepare URL String
        let urlParameter = String(format:APIParamConstants.kDeleteGroup)
        
        urlString = BaseApi().urlEncodedString(nil, restUrl: "\(urlParameter)\(String(describing: self.userId!))", baseUrl: APIParamConstants.kSERVER_END_POINT)
        
         header = BaseApi().getDefaultHeaders() as? [String : String]
        
        //Set Method Type
        methodType = .DELETE
        super.main()
    }
    
    override func onSuccess(_ responseView:APIResponseParam.BaseResponse?){
        AppConstant.kLogString(responseView ?? "")
        
        if(callBackSuccess != nil && responseView != nil){
            callBackSuccess!(responseView!)
        }
    }
   
    override func onError(_ response:ErrorModel) {
        AppConstant.kLogString(response)
        
        if(callBackError != nil){
            callBackError!(response)
        }
    }
}
