//
//  ApplicationPreference.swift
//  MyPanditJi
//

import Foundation
import UIKit

class ApplicationPreference{
    
    fileprivate static let defaults = UserDefaults.standard
    
    //////////////// Remove all info //////
    class func clearAllData(){
        defaults.removeObject(forKey: AppConstant.userDefaultAppTokenKey)
        defaults.removeObject(forKey: AppConstant.userDefaultUserNameKey)
        defaults.removeObject(forKey: AppConstant.userDefaultUserLoginInfoKey)
        //
        defaults.removeObject(forKey: AppConstant.userDefaultUserIdKey)
        defaults.removeObject(forKey: AppConstant.userDefaultUserFullNameKey)
        defaults.removeObject(forKey: AppConstant.userPermissionisFileManager)
        defaults.removeObject(forKey: AppConstant.userPermissionisClearChat)
        defaults.removeObject(forKey: AppConstant.userPermissionisDeleteChat)

        UserDefaults.standard.synchronize()
    } 
    
    //////////////// GET & SAVE APP TOKEN //////
    class func saveAppToken(appToken: String){
        defaults.set(appToken, forKey: AppConstant.userDefaultAppTokenKey)
        UserDefaults.standard.synchronize()
    }
    
    class func getAppToken()->String?{
        let appToken:String?
        appToken = defaults.object(forKey: AppConstant.userDefaultAppTokenKey) as? String
        return appToken ?? nil
    }
    
    ///////////////// GET & SAVE UserName //////
    class func saveUserName(userName: String){
        defaults.set(userName, forKey: AppConstant.userDefaultUserNameKey)
        UserDefaults.standard.synchronize()
    }
    
    class func getUserName()->String?{
        let userName:String?
        userName = defaults.object(forKey: AppConstant.userDefaultUserNameKey) as? String
        return userName ?? nil
    }
    
    ///////////////// GET & SAVE UserId //////
    
    class func saveMongoUserId(userId: String){
        defaults.set(userId, forKey: AppConstant.userDefaultUserIdKey)
        UserDefaults.standard.synchronize()
    }

    class func getMongoUserId()->String?{
        let userId:String?
        userId = defaults.object(forKey: AppConstant.userDefaultUserIdKey) as? String
        return userId ?? nil
    }
//
    ///////////////// GET & SAVE UserName //////
//    class func saveFcmToken(token: String){
//        defaults.set(token, forKey: AppConstant.kFcmToken)
//        UserDefaults.standard.synchronize()
//    }
//
//    class func getFcmToken()->String?{
//        let token:String?
//        token = defaults.object(forKey: AppConstant.kFcmToken) as? String
//        return token ?? nil
//    }
    
//    class func removeFcmToken(){
//        defaults.removeObject(forKey: AppConstant.kFcmToken)
//        UserDefaults.standard.synchronize()
//    }
//    
    ///////////////// GET & SAVE Login Info //////
    class func saveLoginInfo(loginInfo: String){
        defaults.set(loginInfo, forKey: AppConstant.userDefaultUserLoginInfoKey)
        UserDefaults.standard.synchronize()
    }
    
    class func getLoginInfo()->String?{
        let loginInfo:String?
        loginInfo = defaults.object(forKey: AppConstant.userDefaultUserLoginInfoKey) as? String
        return loginInfo ?? nil
    }
    
    class func removeLoginInfo(){
        defaults.removeObject(forKey: AppConstant.userDefaultUserLoginInfoKey)
        UserDefaults.standard.synchronize()
    }
    
    ///
    class func textToImage(text: String, image: UIImage, point: CGPoint) -> UIImage? {
        let textColor = UIColor.white
        let textFont = UIFont(name: "Helvetica Bold", size: 12)!
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedStringKey.font: textFont,
            NSAttributedStringKey.foregroundColor: textColor,
            ] as [NSAttributedStringKey : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let rect = CGRect(origin: point, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes as [NSAttributedStringKey : Any]?)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    ///////////////// GET & SAVE Notification Count Info //////
    class func saveNotificationCountInfo(notificationCount: String){
        defaults.set(notificationCount, forKey: AppConstant.kNotificationCount)
        UserDefaults.standard.synchronize()
    }
    
    class func getNotificationCountInfo()->String?{
        let notificationCount:String?
        notificationCount = defaults.object(forKey: AppConstant.kNotificationCount) as? String
        return notificationCount ?? nil
    }
    
    class func updateNotificationCount(){
        let notificationCount = getNotificationCountInfo()
        var count = 0
        
        if notificationCount != nil && Int(notificationCount!)! > 0{
            count = Int(notificationCount!)! + 1
        } else{
            count = 1
        }
        
        saveNotificationCountInfo(notificationCount: "\(count)")
    }
    
    class func removeNotificationCountInfo(){
        defaults.removeObject(forKey: AppConstant.kNotificationCount)
    }
    
    ///////////////// GET & SAVE Chat count //////
    
    class func saveChatCountInfo(userInfo: [String:String]){
        // Storing the model dictionary
        let data = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        defaults.set(data, forKey: AppConstant.kChatCount)
        UserDefaults.standard.synchronize()
    }
    
    class func getChatCountInfo()-> [String:String]?{
        
        if(defaults.object(forKey: AppConstant.kChatCount) != nil){
            let userInfo:NSDictionary?
            let outData:AnyObject = defaults.object(forKey: AppConstant.kChatCount)! as AnyObject
            userInfo = NSKeyedUnarchiver.unarchiveObject(with: outData as! Data) as! NSDictionary?
            return userInfo as? [String:String]
        }else{
            return nil
        }
    }
    
    class func removeChatCount(){
        defaults.removeObject(forKey: AppConstant.kChatCount)
    }
    
    ///////////////// GET & SAVE Total Chat count //////
    class func saveTotalChatCount(totalCout: String){
        defaults.set(totalCout, forKey: AppConstant.kTotalChatCount)
        UserDefaults.standard.synchronize()
    }
    
    class func getTotalChatCount()->String?{
        let totalCout:String?
        totalCout = defaults.object(forKey: AppConstant.kTotalChatCount) as? String
        return totalCout ?? nil
    }
    
    class func removeTotalChatCount(){
        defaults.removeObject(forKey: AppConstant.kTotalChatCount)
    }
    
    //
    ///////////////// GET & SAVE permissions //////
    class func savePermissionFileManager(isFileManager: Bool){
        defaults.set(isFileManager, forKey: AppConstant.userPermissionisFileManager)
        UserDefaults.standard.synchronize()
    }
    
    class func getPermissionFileManager()->Bool?{
        let isFileManager:Bool?
        isFileManager = defaults.object(forKey: AppConstant.userPermissionisFileManager) as? Bool
        return isFileManager ?? nil
    }
    
    //Clear chat
    class func savePermissionClearChat(isClearChat: Bool){
        defaults.set(isClearChat, forKey: AppConstant.userPermissionisClearChat)
        UserDefaults.standard.synchronize()
    }
    
    class func getPermissionClearChat()->Bool?{
        let isClearChat:Bool?
        isClearChat = defaults.object(forKey: AppConstant.userPermissionisClearChat) as? Bool
        return isClearChat ?? nil
    }
    
    //Delete chat
    class func savePermissionDeleteChat(isDeleteChat: Bool){
        defaults.set(isDeleteChat, forKey: AppConstant.userPermissionisDeleteChat)
        UserDefaults.standard.synchronize()
    }
    
    class func getPermissionDeleteChat()->Bool?{
        let isDeleteChat:Bool?
        isDeleteChat = defaults.object(forKey: AppConstant.userPermissionisDeleteChat) as? Bool
        return isDeleteChat ?? nil
    }
    
    ///////////////// GET & SAVE User Name //////
//    class func saveUserFullName(userFullName: String){
//        defaults.set(userFullName, forKey: AppConstant.userDefaultUserFullNameKey)
//        UserDefaults.standard.synchronize()
//    }
    
//    class func getUserFullName()->String?{
//        let userFullName:String?
//        userFullName = AppTheme.getLoginDetails().object(forKey: "full_name")! as? String
//        return userFullName ?? nil
//    }
}
