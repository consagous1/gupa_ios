//
//  BaseViewController.swift
//  DrApp
//
//  Created by Apple on 26/09/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import Foundation

class BaseViewController: UIViewController {
    
    @IBOutlet weak var constraintNavigationHeight:NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.constraintNavigationHeight != nil){
            if AppConstant.DeviceType.IS_IPHONE_X{
                self.constraintNavigationHeight?.constant = AppConstant.IPHONE_X_DEFAULT_NAVIGATION_BAR_HEIGHT
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.navigationController?.isNavigationBarHidden = fa
        
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
//            statusBar.backgroundColor = UIColor(hexString: ColorCode.statusBgColor)
//        }
//        UIApplication.shared.statusBarStyle = .lightContent
//        
//        self.navigationController?.navigationBar.tintColor = UIColor(hexString: ColorCode.navigationBgColor)
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
}
