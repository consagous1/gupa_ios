//
//  ViewLayerSetup.swift
//  TextApp
//


import Foundation

class ViewLayerSetup: UIView{
    @IBInspectable var cornerRadiusView: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadiusView
            layer.masksToBounds = cornerRadiusView > 0
        }
    }
    @IBInspectable var borderWidthView : CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidthView
        }
    }
    @IBInspectable var borderColorNewView : UIColor? {
        didSet {
            layer.borderColor = borderColorNewView?.cgColor
        }
    }
    
    //    @IBInspectable var shouldRasterize: Bool = false{
    //        didSet{
    //            layer.shouldRasterize = shouldRasterize
    //        }
    //    }
    
    @IBInspectable var shadowRadiusView : CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadiusView
        }
    }
    
    @IBInspectable var shadowOpacityViewNew: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacityViewNew
        }
    }
    
    @IBInspectable var shadowColorView11: UIColor? {
        didSet {
            layer.shadowColor = shadowColorView11?.cgColor
        }
    }
    
    @IBInspectable var masksToBoundsView11: Bool = false{
        didSet {
            layer.masksToBounds = masksToBoundsView11
        }
    }
    
    @IBInspectable var shadowOffsetView11: CGSize = CGSize.zero {
        didSet {
            layer.shadowOffset = shadowOffsetView11
        }
    }
}


class ButtonLayerSetup: UIButton{
    
    @IBInspectable var cornerRadiusButton : CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadiusButton
            layer.masksToBounds = cornerRadiusButton > 0
        }
    }
    @IBInspectable var borderWidthButton: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidthButton
        }
    }
    @IBInspectable var borderColorButton: UIColor? {
        didSet {
            layer.borderColor = borderColorButton?.cgColor
        }
    }
    
    //    @IBInspectable var shouldRasterize: Bool = false{
    //        didSet{
    //            layer.shouldRasterize = shouldRasterize
    //        }
    //    }
    
    @IBInspectable var shadowRadiusButton: CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadiusButton
        }
    }
    
    @IBInspectable var shadowOpacityButton: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacityButton
        }
    }
    
    @IBInspectable var shadowColorButton: UIColor? {
        didSet {
            layer.shadowColor = shadowColorButton?.cgColor
        }
    }
    
    @IBInspectable var masksToBoundsButton: Bool = false{
        didSet {
            layer.masksToBounds = masksToBoundsButton
        }
    }
    
    @IBInspectable var shadowOffsetButton: CGSize = CGSize.zero{
        didSet {
            layer.shadowOffset = shadowOffsetButton
        }
    }
    
   // TODO
//    @IBInspectable var imageHighlightColor: UIColor? {
//        didSet {
//            self.setImageColorForState(image: (self.imageView?.image!)!, color: imageHighlightColor!, forState: UIControlState.highlighted)
//        }
//    }
}

class LabelLayerSetup: UILabel {
    
    @IBInspectable var cornerRadiusNew: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadiusNew
            layer.masksToBounds = cornerRadiusNew > 0
        }
    }
    
    @IBInspectable var borderWidthNew: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidthNew
        }
    }
    @IBInspectable var borderColorNew: UIColor? {
        didSet {
            layer.borderColor = borderColorNew?.cgColor
        }
    }
    
    //    @IBInspectable var shouldRasterize: Bool = false{
    //        didSet{
    //            layer.shouldRasterize = shouldRasterize
    //        }
    //    }
    
    @IBInspectable var shadowRadiusNew: CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadiusNew
        }
    }
    
    @IBInspectable var shadowOpacityNew: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacityNew
        }
    }
    
    @IBInspectable var masksToBoundsNew: Bool = false{
        didSet {
            layer.masksToBounds = masksToBoundsNew
        }
    }
    
}

class ImageLayerSetup: UIImageView{
    
    @IBInspectable var cornerRadiusImage : CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidthImage: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColorImage: UIColor? {
        didSet {
            layer.borderColor = borderColorImage?.cgColor
        }
    }
    
    @IBInspectable var shadowRadiusImage : CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacityImage: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacityImage
        }
    }
    
    @IBInspectable var shadowColorImage: UIColor? {
        didSet {
            layer.shadowColor = shadowColorImage?.cgColor
        }
    }
    
    @IBInspectable var masksToBoundsImage : Bool = false{
        didSet {
            layer.masksToBounds = masksToBoundsImage
        }
    }
    
    @IBInspectable var shadowOffsetImage: CGSize = CGSize.zero{
        didSet {
            layer.shadowOffset = shadowOffsetImage
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let radius: CGFloat = self.bounds.size.width / 2.0
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}
