 //
//  MainTableViewCell.swift
//  TextApp
//
//  Created by Apple on 11/7/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var titleIconImg: UIImageView!
    @IBOutlet weak var imageforStatus: UIImageView!
    @IBOutlet weak var labelforTitle: UILabel!
    @IBOutlet weak var labelForDescription: UILabel!
    
    @IBOutlet weak var viewForName: UIView!
    @IBOutlet weak var viewForDescription: UIView!
    @IBOutlet weak var viewForAddress: UIView!
    @IBOutlet weak var btnForAddress: UIButton!
    @IBOutlet weak var btnForStatus: UIButton!
    
    @IBOutlet weak var btnForUserImg: UIButton!
    @IBOutlet weak var btnForDescription: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if selected {
            
        } else {
            
        }
    }
    
    func updateScreenInfo(homeListInfo:APIRequestParam.HomeListData?){
        
        if(homeListInfo?.lastmsgtime != nil){
            let startDateTimeStamp = homeListInfo?.lastmsgtime
            let time = DateTimeUtils.sharedInstance.getDeviceTimeZoneTimeStrFromUnixFormat(Double(truncating: startDateTimeStamp!))
            lblDate.text = time
        } else {
            lblDate.text = ""
        }
        
        let chatCount = BaseApp.sharedInstance.getChatCount(roomId: (homeListInfo?._id)!)
        if(chatCount != nil && (Int(chatCount!) ?? 0) > 0){
            lblCount.text = chatCount
            lblCount.isHidden = false
        } else {
            lblCount.text = ""
            lblCount.isHidden = true
        }
        
        if homeListInfo?.is_group == false {
            
            if ApplicationPreference.getMongoUserId() != nil {
                
                if homeListInfo?.createdby?.caseInsensitiveCompare(ApplicationPreference.getMongoUserId()!) != ComparisonResult.orderedSame {
                    let strFullName = BaseApp.sharedInstance.getShortName(strFullName: "\(homeListInfo?.friend_name! ?? "")")
                    labelforTitle.text = "\(homeListInfo?.friend_name! ?? "")".capitalized
                    btnForUserImg.setTitle(strFullName.uppercased(), for: .normal)
                } else {
                    let strFullName = BaseApp.sharedInstance.getShortName(strFullName: "\(homeListInfo?.group_name! ?? "")")
                    labelforTitle.text = "\(homeListInfo?.group_name! ?? "")".capitalized
                    btnForUserImg.setTitle(strFullName.uppercased(), for: .normal)
                }
                
                if homeListInfo?.last_msg_type == "text" {
                    let strText = BaseApp.sharedInstance.getMsgTextWithImage(strFullName: "", msgText: "\(homeListInfo?.last_msg! ?? "")".capitalized, msgType: "text")
                    btnForDescription.setAttributedTitle(strText, for: .normal)
                }  else {
                    let strText = BaseApp.sharedInstance.getMsgTextWithImage(strFullName: "", msgText: "", msgType: "")
                    btnForDescription.setAttributedTitle(strText, for: .normal)
                }
                
//                if homeListInfo?.online == true {
//                    imageforStatus.alpha = 1
//                } else {
//                    imageforStatus.alpha = 0
//                }
                
            }
        } else {
           // imageforStatus.alpha = 0
          //  viewForAddress.isHidden = true
            let strFullName = BaseApp.sharedInstance.getShortName(strFullName: "\(homeListInfo?.group_name! ?? "")")
            
            labelforTitle.text = "\(homeListInfo?.group_name! ?? "")".capitalized
            btnForUserImg.setTitle(strFullName.uppercased(), for: .normal)
            
            if homeListInfo?.last_msg_type == "text" {
                let name = "\(homeListInfo?.last_msg_sender_name! ?? "")".capitalized + ":"
                let strText = BaseApp.sharedInstance.getMsgTextWithImage(strFullName: name, msgText: (homeListInfo?.last_msg! ?? ""), msgType: "text")
                btnForDescription.setAttributedTitle(strText, for: .normal)
            } else {
                let strText = BaseApp.sharedInstance.getMsgTextWithImage(strFullName: "", msgText: "", msgType: "")
                btnForDescription.setAttributedTitle(strText, for: .normal)
            }
        }
        
//        if btnForDescription.titleLabel?.attributedText != nil {
//            self.btnForStatus.alpha = 1
//        } else {
//            self.btnForStatus.alpha = 0
//        }
    }
}
