//
//  ChatViewController.swift
//  TextApp
//
//  Created by Apple on 11/7/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var tblForMainChat: UITableView!
    
    fileprivate var chatViewController:ChatViewController?
    fileprivate var newGroupViewController:NewGroupViewController?

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var heightForSearchView: NSLayoutConstraint!
    @IBOutlet weak var bottomConstForTblView: NSLayoutConstraint!
    
    var arrForHomeList         = [APIResponseParam.HomeListInfo.HomeListInfoData] ()
    var arrFilteredForHomeList = [APIRequestParam.HomeListData]()//[APIResponseParam.HomeListInfo.HomeListInfoData]()
    var arrPrivateGroupInfo    = [SocketPayloadView.PrivateGroupUserInfo]()
    var arrLastChatInfo        = [SocketPayloadView.ChatReceiveWithExtraParamters]()

    var isSearching = false
    var isSearchClicked = false
    var respDict : NSDictionary = NSDictionary()

    //For user info update
    fileprivate var scheduler:Scheduler?
    var arrUserTemp = [String]()
    var strUserID = ""
    
    //****************************************************
    // MARK: - Life Cycle Methods
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        appDelegate.centerNav = self.navigationController
        
        searchBar.isHidden = true
        heightForSearchView.constant = 0.0
        searchBar.delegate = self
        tblForMainChat.tableFooterView = UIView()

         //SearchBar Placeholder
        if let txfSearchField = searchBar.value(forKey: "_searchField") as? UITextField {
            txfSearchField.borderStyle = .none
            txfSearchField.backgroundColor = .clear
        }
        searchBar.searchTextPositionAdjustment = UIOffsetMake(10.0, 0.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        newGroupViewController = nil
        chatViewController = nil
        
        //**>> Manage Navigation Bar
        self.title = "Chats"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        SwiftEventBus.onMainThread(self, name:AppConstant.kNotificationCount, handler: handleUpdateNotificationCount)
        SwiftEventBus.onMainThread(self, name:AppConstant.kTotalChatCount, handler: handleUpdateTotalChatCount)
        SwiftEventBus.onMainThread(self, name:AppConstant.kRefreshChatList, handler: handleUViewAds)
        SwiftEventBus.onMainThread(self, name:AppConstant.kRefreshHomeList, handler: handleLastChat)
        SwiftEventBus.onMainThread(self, name:AppConstant.kRefreshHomeListFromSocket, handler: handlePrivateGroupData)
      //  SwiftEventBus.onMainThread(self, name:SocketEvents.COMMON_ON_USER_ONLINE_OFFLINE_STATUS, handler: handleOnlineOfflineStatus)

        if !SocketManager.sharedInstance.isConnected() {
            //For User info Update
            self.scheduler = Scheduler.init(delay: 3600, callBackMethod: self.refreshUserInfo)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //TODO: remove after build
        if(BaseApp.sharedInstance.isNetworkConnected){
            isSearchClicked = false
            UIView.animate(withDuration: 0.5, animations: {
                self.searchBar.resignFirstResponder()
                self.heightForSearchView.constant = 0.0
                self.tblForMainChat.reloadData()
                self.view.layoutIfNeeded()
            })
            serverAPI()
        }
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        BaseApp.sharedInstance.currentChatRoomId = nil
        self.scheduler?.timer?.invalidate()
        
        SwiftEventBus.unregister(self)
        NotificationCenter.default.removeObserver(self)
    }
    
//    //Setting Floating View
//    fileprivate func setupFab() {
//
//        var fabConfig = MIFab.Config()
//
//        fabConfig.buttonImage = UIImage(named: "plus_menu")
//        fabConfig.buttonBackgroundColor = UIColor(hexString: ColorCode.buttonBgColor)!
//
//        fab = MIFab(
//            parentVC: self,
//            config: fabConfig,
//            options: [
//                MIFabOption(
//                    title: "New Chat",
//                    image: #imageLiteral(resourceName: "ic-chat") ,//UIImage(named: "plus_menu"),
//                    backgroundColor: UIColor(hexString: ColorCode.buttonBgColor),
//                    tintColor: UIColor.white,
//                    actionClosure: {
//                        self.openNewChatViewController()
//                }
//                ),
//                MIFabOption(
//                    title: "New Group",
//                    image: #imageLiteral(resourceName: "ic-group-chat") , //UIImage(named: "plus_menu"),
//                    backgroundColor: UIColor(hexString: ColorCode.buttonBgColor),
//                    tintColor: UIColor.white,
//                    actionClosure: {
//                        self.openNewGroupViewController()
//                }
//                )
//            ]
//        )
//
//        fab.showButton(animated: true)
//    }
//
    //=================================================
    //MARK: Button Clicked Methods
    //=================================================

    @IBAction func btnSearchClicked(_ sender: UIBarButtonItem) {
        
        if isSearchClicked == false {
            isSearchClicked = true
            UIView.animate(withDuration: 0.5, animations: {
                self.searchBar.becomeFirstResponder()
                self.heightForSearchView.constant = 55.0
                self.tblForMainChat.reloadData()
                self.view.layoutIfNeeded()
            })
        } else {
            
            isSearchClicked = false
            UIView.animate(withDuration: 0.5, animations: {
                self.searchBar.resignFirstResponder()
                self.heightForSearchView.constant = 0.0
                self.tblForMainChat.reloadData()
                self.view.layoutIfNeeded()
            })
        }
    }

    @IBAction func btnAddGroup(_ sender: UIBarButtonItem) {
        self.openNewGroupViewController()
    }
    
    func openNewGroupViewController(){
        
        isSearchClicked = false
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar.resignFirstResponder()
            self.heightForSearchView.constant = 0.0
            self.tblForMainChat.reloadData()
            self.view.layoutIfNeeded()
        })
        if(newGroupViewController == nil){
            newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: AppConstant.chatVCStoryboard, viewControllerName: NewGroupViewController.nameOfClass) as NewGroupViewController
            self.navigationController?.pushViewController(newGroupViewController!, animated: true)
        }
    }

    
    //=================================================
    //MARK: Search bar delegates
    //=================================================

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false;
        } else {
            isSearching = true
        }
        self.handleDataForHomeList(callFrom: "SEARCH", searchText: searchText , userId: "", status: false)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    //MARK: Keyboard Methods
    @objc func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                //key point 0,
                self.bottomConstForTblView.constant =  0
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardWillShow(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.bottomConstForTblView.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func setUPDropDown() {
//        rightBarDropDown.anchorView = btnForAddOption
//        rightBarDropDown.width = 150
//
//        let appearance = DropDown.appearance()
//
//        appearance.cellHeight = 40
//        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
//        appearance.cornerRadius = 5
//        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
//        appearance.shadowOpacity = 0.9
//        appearance.shadowRadius = 25
//        appearance.animationduration = 0.25
//        appearance.textColor = .darkGray
//        appearance.selectionBackgroundColor = .clear
//
//        // You can also use localizationKeysDataSource instead. Check the docs.
//        rightBarDropDown.dataSource = [
//            "Change Password",
//            "Logout"
//        ]
//
//        // Action triggered on selection
//        rightBarDropDown.selectionAction = { [unowned self] (index, item) in
//            switch index {
//            case 0:
//                self.openChangePasswordViewController()
//                break
//
//            case 1:
//                // Create the AlertController and add its actions like button in ActionSheet
//                let actionSheetController = UIAlertController(title: "Message", message: "Are you sure you want to logout?", preferredStyle: .alert)
//
//                let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
//
//                }
//                actionSheetController.addAction(cancelActionButton)
//
//                let logoutActionButton = UIAlertAction(title: "OK", style: .default) { action -> Void in
//
//                    self.arrFilteredForHomeList = []
//                    self.tblForMainChat.reloadData()
//                    // Stops monitoring network reachability status changes
//                    BaseApp.sharedInstance.stopMonitoring()
//                    BaseApp.sharedInstance.openLoginViewController()
//                }
//                actionSheetController.addAction(logoutActionButton)
//                self.present(actionSheetController, animated: true, completion: nil)
//                break
//            default:
//                break
//            }
//        }
//    }
}

// MARK:- Handle notification
extension HomeViewController {
    
    func handleUpdateNotificationCount(notification: Notification!) -> Void{
       // updateNotificationBadgeCount()
    }
    
    func handleUpdateTotalChatCount(notification: Notification!) -> Void{
     //   updateTotalChatBadgeCount()
    }
    
    func handleUViewAds(notification: Notification!) -> Void{
       refreshAds()
    }
    
    func handleLastChat(notification: Notification!) -> Void {
        
        let object:[SocketPayloadView.ChatReceiveWithExtraParamters]? = notification.object as? [SocketPayloadView.ChatReceiveWithExtraParamters]
        
        if(object != nil && (object?.count)! > 0){
           
            let dict = object!.toJSON()

            self.arrLastChatInfo = [SocketPayloadView.ChatReceiveWithExtraParamters]()
            self.arrLastChatInfo = (object)!
            self.handleDataForHomeList(callFrom: "LASTMSG", searchText: "" , userId: "", status: false)
            
            //TODO: need to manage chat delivary status
            if SocketManager.sharedInstance.isConnected() {
                SocketManager.sharedInstance.emitDataList(SocketEvents.COMMON_ON_UPDATE_LAST_SEEN_STATUS, data: [dict , "" ])
            }
        }
        BaseApp.sharedInstance.hideProgressHudView()
        self.tblForMainChat.reloadData()
    }
    
    func handlePrivateGroupData(notification: Notification!) -> Void {
        let object:[SocketPayloadView.PrivateGroupUserInfo]? = notification.object as? [SocketPayloadView.PrivateGroupUserInfo]
        
        if(object != nil && (object?.count)! > 0){
            self.arrPrivateGroupInfo = [SocketPayloadView.PrivateGroupUserInfo]()
            self.arrPrivateGroupInfo = (object)!
            self.handleDataForHomeList(callFrom: "PRIVATEINFO", searchText: "" , userId: "", status: false)
        }
        BaseApp.sharedInstance.hideProgressHudView()
        self.tblForMainChat.reloadData()
    }
    
//    func handleOnlineOfflineStatus(notification: Notification!) -> Void {
//        let object:[SocketPayloadView.OnlineOfflineStatus]? = notification.object as? [SocketPayloadView.OnlineOfflineStatus]
//        
//        if(object != nil && (object?.count)! > 0){
//            let status = object![0].status
//            let userid = object![0].id
//            var statusOnelineOffline = false
//            if status == 0 {
//                statusOnelineOffline = false
//            } else {
//                statusOnelineOffline = true
//            }
//            self.handleDataForHomeList(callFrom: "ONLINEOFFLINE", searchText: "" , userId: userid!, status: statusOnelineOffline)
//        }
//        BaseApp.sharedInstance.hideProgressHudView()
//        self.tblForMainChat.reloadData()
//    }
    
    func refreshAds() {
        if(BaseApp.sharedInstance.isNetworkConnected){
            serverAPI()
        }
    }
    
    func handleDataForHomeList(callFrom: String, searchText: String, userId: String, status: Bool) {
        
        var strlat = ""
        var strLang = ""
        var strLocation = ""
        var strLastMsg = ""
        var strLastMsgSenderName = ""
        var strLastMsgType = ""
        var strLastMsgSentBy = ""
        var strLastMsgTime = 0
        var strOnline = false


        if(self.arrForHomeList.count > 0){
            var filteredlist:[APIRequestParam.HomeListData] = []
            var filteredlistData:APIRequestParam.HomeListData?
            
            for filterInfo in self.arrForHomeList {
                
                strlat = ""
                strLang = ""
                strLocation = ""
                strLastMsg = filterInfo.last_msg!
                strLastMsgSenderName = filterInfo.last_msg_sender_name!
                strLastMsgType = filterInfo.last_msg_type!
                strLastMsgTime = Int(truncating: filterInfo.lastmsgtime! as NSNumber)
                strLastMsgSentBy = filterInfo.lastmsgsentby!
//                strCreatedID = filterInfo.created_Id!
//                strFreindID = filterInfo.friend_Id!

                strOnline = false

                if arrPrivateGroupInfo.count > 0 {
                    for privateInfo in arrPrivateGroupInfo {
                        let userList = filterInfo.userLists
                        for userInfo in userList! {
                            if (userInfo._id?.caseInsensitiveCompare(privateInfo._id!) == ComparisonResult.orderedSame){
                                if callFrom == "ONLINEOFFLINE" && (userId.caseInsensitiveCompare(privateInfo._id!) == ComparisonResult.orderedSame){
                                 //   strOnline = status
                                    strLocation = privateInfo.location!
                                  //  strUserID = privateInfo.id!
                                    print ("3333333 == \(strUserID)")

                                    break
                                } else {
                                 //   strOnline = privateInfo.online!
                                    strLocation = privateInfo.location!
                                  //  strUserID = privateInfo.id!
                                    print ("777777== \(strUserID)")
                                }
                            }
                        }
                    }
                }
                
                if callFrom == "LASTMSG" {
                    for lastMsgInfo in arrLastChatInfo {
                        if (filterInfo._id?.caseInsensitiveCompare(lastMsgInfo.roomID!) == ComparisonResult.orderedSame){
                            strLastMsg = lastMsgInfo.text!
                            strLastMsgSenderName = lastMsgInfo.sender_name!
                            strLastMsgType = lastMsgInfo.message_type!
                            strLastMsgTime = Int(truncating: lastMsgInfo.sendtimestamp!)
                            strLastMsgSentBy = lastMsgInfo.sender_id!
                            break
                        } else {
                            refreshAds()
                            break
                        }
                    }
                }
                
                if callFrom == "SEARCH" {
                    if (filterInfo.friend_name?.lowercased().contains(searchText.lowercased()))! {
                        filteredlistData = APIRequestParam.HomeListData(_id: filterInfo._id, createdby: filterInfo.createdby, friend_name: filterInfo.friend_name, group_icon: filterInfo.group_icon, group_name: filterInfo.group_name, is_group: filterInfo.is_group, lang: strLang, lat: strlat, location: strLocation, last_msg: strLastMsg, last_msg_sender_name: strLastMsgSenderName, last_msg_type: strLastMsgType, lastmsgsentby: strLastMsgSentBy, lastmsgtime: strLastMsgTime as NSNumber, online: strOnline, status: "1", createdBy_Id: filterInfo.created_Id ,friend_Id : filterInfo.friend_Id, userLists: filterInfo.userLists!)
                        filteredlist.append(filteredlistData!)
                    }
                } else {
                    filteredlistData = APIRequestParam.HomeListData(_id: filterInfo._id, createdby: filterInfo.createdby, friend_name: filterInfo.friend_name, group_icon: filterInfo.group_icon, group_name: filterInfo.group_name, is_group: filterInfo.is_group, lang: strLang, lat: strlat, location: strLocation, last_msg: strLastMsg, last_msg_sender_name: strLastMsgSenderName, last_msg_type: strLastMsgType, lastmsgsentby: strLastMsgSentBy, lastmsgtime: strLastMsgTime as NSNumber, online: strOnline, status: "1", createdBy_Id :filterInfo.created_Id ,friend_Id: filterInfo.friend_Id  ,userLists: filterInfo.userLists!)
                    filteredlist.append(filteredlistData!)
                }
            }
            
            if filteredlist.count > 0 {
                filteredlist = filteredlist.sorted(by: {$0.lastmsgtime?.compare($1.lastmsgtime!) == .orderedDescending })
                arrFilteredForHomeList = [APIRequestParam.HomeListData]()
                arrFilteredForHomeList = filteredlist
            }
        } else {
            refreshAds()
        }
        
        BaseApp.sharedInstance.hideProgressHudView()
        self.tblForMainChat.reloadData()
    }
}


extension HomeViewController : UITableViewDataSource , UITableViewDelegate , UISearchBarDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print ("arr count == \(arrFilteredForHomeList.count)")

        if arrFilteredForHomeList.count > 0 {
            return arrFilteredForHomeList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath) as! MainTableViewCell

        let homeListInfo = arrFilteredForHomeList[indexPath.row]

        if(homeListInfo.lastmsgtime != nil){
            let startDateTimeStamp = homeListInfo.lastmsgtime
                let time = DateTimeUtils.sharedInstance.getDeviceTimeZoneTimeStrFromUnixFormat(Double(truncating: startDateTimeStamp!))
                cell.lblDate.text = time
            } else {
                cell.lblDate.text = ""
        }
        
        
        let chatCount = BaseApp.sharedInstance.getChatCount(roomId: (homeListInfo._id)!)

            if(chatCount != nil && (Int(chatCount!) ?? 0) > 0){
                cell.lblCount.text = chatCount
                cell.lblCount.isHidden = false
            } else {
                cell.lblCount.text = ""
                cell.lblCount.isHidden = true
            }

        if homeListInfo.is_group == false {

                if ApplicationPreference.getMongoUserId() != nil {
                    
                    cell.btnForUserImg.setImage(nil, for: .normal)

                    if homeListInfo.createdby?.caseInsensitiveCompare(ApplicationPreference.getMongoUserId()!) != ComparisonResult.orderedSame {
                        let strFullName = BaseApp.sharedInstance.getShortName(strFullName: "\(homeListInfo.friend_name!)")
                        cell.labelforTitle.text = "\(homeListInfo.friend_name! )".capitalized
                        cell.btnForUserImg.setTitle(strFullName.uppercased(), for: .normal)
                    } else {
                        let strFullName = BaseApp.sharedInstance.getShortName(strFullName: "\(homeListInfo.group_name! )")
                        cell.labelforTitle.text = "\(homeListInfo.group_name! )".capitalized
                        cell.btnForUserImg.setTitle(strFullName.uppercased(), for: .normal)
                    }

                    if homeListInfo.last_msg_type == "text" {
                        let strText = BaseApp.sharedInstance.getMsgTextWithImage(strFullName: "", msgText: "\(homeListInfo.last_msg! )".capitalized, msgType: "text")
                        cell.btnForDescription.setAttributedTitle(strText, for: .normal)
                    }  else {
                        let strText = BaseApp.sharedInstance.getMsgTextWithImage(strFullName: "", msgText: "", msgType: "")
                        cell.btnForDescription.setAttributedTitle(strText, for: .normal)
                    }
                }
            } else {
            
            let strFullName = BaseApp.sharedInstance.getShortName(strFullName: "\(homeListInfo.group_name! )")
            cell.labelforTitle.text = "\(homeListInfo.group_name!)".capitalized
            
            if  homeListInfo.group_icon == "" {
                cell.btnForUserImg.setImage(nil, for: .normal)
                cell.btnForUserImg.setTitle(strFullName.uppercased(), for: .normal)
            } else {
              //  cell.btnForUserImg.setImage(UIImage(named: "user"), for: .normal)
                let strUrl = "\(APIParamConstants.BASE_MEDIAURL)" + homeListInfo.group_icon!
                cell.btnForUserImg.setImageFor(.normal, with: URL(string: strUrl)!)
                print("1111 = \(strUrl)")
            }

            if homeListInfo.last_msg_type == "text" {
                
               // let name = "\(homeListInfo.last_msg_sender_name ?? "")".capitalized

                if(homeListInfo.lastmsgsentby == ApplicationPreference.getMongoUserId()){
                let strText = BaseApp.sharedInstance.getMsgTextWithImage(strFullName: "", msgText: (homeListInfo.last_msg! ), msgType: "text")

//                    let strText = BaseApp.sharedInstance.getMsgTextWithImage(strFullName: "Me: ", msgText: (homeListInfo.last_msg! ), msgType: "text")
                    cell.btnForDescription.setAttributedTitle(strText, for: .normal)
                } else {
                    let strText = BaseApp.sharedInstance.getMsgTextWithImage(strFullName: "", msgText: (homeListInfo.last_msg! ), msgType: "text")
//                    let strText = BaseApp.sharedInstance.getMsgTextWithImage(strFullName: name, msgText: (homeListInfo.last_msg! ), msgType: "text")
                    cell.btnForDescription.setAttributedTitle(strText, for: .normal)
                  }
                }
            }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if arrFilteredForHomeList.count > 0 {
            return 90
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as? MainTableViewCell
                
        if(chatViewController == nil){
            isSearchClicked = false
            
            UIView.animate(withDuration: 0.5, animations: {
                self.searchBar.resignFirstResponder()
                self.heightForSearchView.constant = 0.0
                self.tblForMainChat.reloadData()
                self.view.layoutIfNeeded()
            })
            
           // let dictID = arrPrivateGroupInfo[indexPath.row]
            let userData = arrFilteredForHomeList[indexPath.row]
            
            chatViewController = BaseApp.sharedInstance.getViewController(storyboardName: AppConstant.chatVCStoryboard, viewControllerName: ChatViewController.nameOfClass) as ChatViewController
            chatViewController?.blockId = userData._id!
            chatViewController?.message_type = "text"
            chatViewController?.message_by = ApplicationPreference.getMongoUserId()
            chatViewController?.sender_name = AppTheme.getUserFullName()
            chatViewController?.is_group = userData.is_group!
            chatViewController?.otherUserId = userData.friend_Id!
            chatViewController?.createdID = userData.createdBy_Id!

            let strName = cell?.labelforTitle.text
            chatViewController?.userName = strName
            
            chatViewController?.strViewComeFrom = ""
            let strTrim = userData.group_name!.trimmingCharacters(in: .whitespacesAndNewlines)
            chatViewController?.strTitle = strTrim
            
            if userData.is_group == false {
                
                if (ApplicationPreference.getMongoUserId()?.caseInsensitiveCompare(userData.createdby!) == ComparisonResult.orderedSame) {
                    let arrForUser = userData.userLists![0]
                    chatViewController?.userID = arrForUser._id!
                    chatViewController?.is_ShowButton = true
                } else {
                    chatViewController?.userID = userData.createdby!
                    chatViewController?.is_ShowButton = false
                }
            } else {
                if (ApplicationPreference.getMongoUserId()?.caseInsensitiveCompare(userData.createdby!) == ComparisonResult.orderedSame) {
                    chatViewController?.is_ShowButton = true
                } else {
                    chatViewController?.is_ShowButton = false
                }
                chatViewController?.userID = ""
            }
            self.navigationController?.pushViewController(chatViewController!, animated: true)
        }
    }
}

// MARK:- API calling
extension HomeViewController {
    
    func serverAPI(){
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
            
            let userRequestParam = APIRequestParam.HomeListInfo()
            
            let userList = HomeListRequest(userId: ApplicationPreference.getMongoUserId()!, userData: userRequestParam, onSuccess: {
                response in
                
                print(response.toJSON())
                
                OperationQueue.main.addOperation() {
                    
                    var object = response.homeListInfoData!
                    object = object.sorted(by: {$0.lastmsgtime?.compare($1.lastmsgtime!) == .orderedDescending })
                    
                    // when done, update your UI and/or model on the main queue
                    self.arrForHomeList = [APIResponseParam.HomeListInfo.HomeListInfoData]()
                    self.arrForHomeList = object
                    
                    let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        
                        //Recivers for Socket
                        if SocketManager.sharedInstance.isConnected() {
                            
                            self.arrUserTemp = [String]()
                            for i in 0..<object.count {
                                let isGroup = object[i].is_group
                                let createdBy = object[i].createdby
                                
                                  if isGroup == false {
                                    if createdBy?.caseInsensitiveCompare(ApplicationPreference.getMongoUserId()!) != ComparisonResult.orderedSame {
                                        self.arrUserTemp.append("\(createdBy!)")
                                    } else {
                                        let userList = object[i].userLists
                                        for userInfo in userList! {
                                            let userId = userInfo._id
                                            self.arrUserTemp.append("\(userId!)")
                                        }
                                    }
                               }
                            }
                            print("Arr Temp ===\(self.arrUserTemp)")
                           // self.tblForMainChat.reloadData()
                          SocketManager.sharedInstance.emitDataList(SocketEvents.COMMON_ON_GET_USER_INFO, data: [self.arrUserTemp])
                        } else {
                            self.handleDataForHomeList(callFrom: "API", searchText: "" , userId: "", status: false)
                        }
                    }
                }
            }, onError: {
                error in
                print(error.toString())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: "OK", controller: self)
                }
            })
            BaseApp.sharedInstance.jobManager?.addOperation(userList)
            
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }
        
    func refreshUserInfo(){
        if SocketManager.sharedInstance.isConnected() {
            if arrUserTemp.count > 0 {
                SocketManager.sharedInstance.emitDataList(SocketEvents.COMMON_ON_GET_USER_INFO, data: [arrUserTemp])
            }
        }
    }
}


