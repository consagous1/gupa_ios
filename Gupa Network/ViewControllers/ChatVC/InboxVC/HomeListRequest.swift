//
//  HomeListRequest.swift
//  TextApp
//
//  Created by iMac on 12/4/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import Foundation

class HomeListRequest: BaseRequest<APIResponseParam.HomeListInfo> {
    
    private var userListData:APIRequestParam.HomeListInfo?
    private var callBackSuccess:((_ response:APIResponseParam.HomeListInfo)->Void)?
    private var callBackError:((_ response:ErrorModel)->Void)?
    private var userId: String?

    init(userId: String, userData:APIRequestParam.HomeListInfo, onSuccess:((_ response:APIResponseParam.HomeListInfo)->Void)?, onError:((_ response:ErrorModel)->Void)?){

        self.userId = userId
        self.userListData = userData
        self.callBackSuccess = onSuccess
        self.callBackError = onError
    }
    
    override func main() {
        
        //Prepare URL String
        let urlParameter = String(format:APIParamConstants.kHomeList)
        urlString = BaseApi().urlEncodedString(nil, restUrl: urlParameter, baseUrl: APIParamConstants.kSERVER_END_POINT)
        
        //Get Header Parameters
        let extraParameter:[String:String] = [APIConfigConstants.kUserId: self.userId!]
        header = BaseApi().getCustomHeaders(extraParameter: extraParameter) as? [String: String]
        
        //Set Method Type
        methodType = .GET
        
        super.main()
    }
    
    override func onSuccess(_ responseView:APIResponseParam.HomeListInfo?){
        AppConstant.kLogString(responseView ?? "")
        
        if(callBackSuccess != nil && responseView != nil){
            callBackSuccess!(responseView!)
        }
    }
    
    override func onError(_ response:ErrorModel) {
        AppConstant.kLogString(response)
        
        if(callBackError != nil){
            callBackError!(response)
        }
    }
}
