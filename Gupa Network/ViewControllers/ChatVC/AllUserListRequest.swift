//
//  AllUserListRequest.swift
//  TextApp
//
//  Created by iMac on 12/10/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import Foundation

class AllUserListRequest: BaseRequest<APIResponseParam.AllUserListInfo> {
    
    private var userListData:APIRequestParam.AllUserListInfo?
    private var callBackSuccess:((_ response:APIResponseParam.AllUserListInfo)->Void)?
    private var callBackError:((_ response:ErrorModel)->Void)?
    private var token: String?
    private var userId: String?
    
    init(token:String?, userId: String, userData:APIRequestParam.AllUserListInfo, onSuccess:((_ response:APIResponseParam.AllUserListInfo)->Void)?, onError:((_ response:ErrorModel)->Void)?){
        
        self.token = token
        self.userId = userId
        self.userListData = userData
        self.callBackSuccess = onSuccess
        self.callBackError = onError
    }
    
    override func main() {
        
        //Prepare URL String
        let urlParameter = String(format:APIParamConstants.kAllUserList)
        urlString = BaseApi().urlEncodedString(nil, restUrl: urlParameter, baseUrl: APIParamConstants.kSERVER_END_POINT)
        
        //Get Header Parameters
        let extraParameter:[String:String] = [APIConfigConstants.kToken: "", APIConfigConstants.kUserId: self.userId!]
        header = BaseApi().getCustomHeaders(extraParameter: extraParameter) as? [String: String]
        
        //Set Method Type
        methodType = .GET
        
        super.main()
    }
    
    override func onSuccess(_ responseView:APIResponseParam.AllUserListInfo?){
        AppConstant.kLogString(responseView ?? "")
        
        if(callBackSuccess != nil && responseView != nil){
            callBackSuccess!(responseView!)
        }
    }
    
    override func onError(_ response:ErrorModel) {
        AppConstant.kLogString(response)
        
        if(callBackError != nil){
            callBackError!(response)
        }
    }
}
