//
//  FontsConfig.swift
// TextApp
//


import Foundation

class FontsConfig: NSObject {
    
    //Android Text Size
    //!-- Text sizes -->
//    <dimen name="text_size_extra_small">10sp</dimen>
//    <dimen name="text_size_small">14sp</dimen>
//    <dimen name="text_size_medium">18sp</dimen>
//    <dimen name="text_size_large">22sp</dimen>
    

    static let FONT_SIZE_SMALL:CGFloat = 10.0
    static let FONT_SIZE_NORMAL:CGFloat = 14.0
    static let FONT_SIZE_MEDIUM:CGFloat = 18.0
    static let FONT_SIZE_MEDIUM20:CGFloat = 20.0
    static let FONT_SIZE_LARGE:CGFloat = 22.0
    
    static let FONT_SIZE_16:CGFloat = 16.0
    static let FONT_SIZE_14:CGFloat = 14.0
  
    static let FONT_TYPE_NunitoSansLight:String = "NunitoSans-Light"
    static let FONT_TYPE_NunitoSansRegular:String = "Roboto-Regular"
    static let FONT_TYPE_NunitoSansSemiBold:String = "Roboto-Medium"
    
    struct FontHelper {
        static func defaultRegularFontWithSize(_ size: CGFloat) -> UIFont {
            return UIFont(name: FONT_TYPE_NunitoSansRegular as String, size: size)!
        }
        
        static func defaultLightFontWithSize(_ size: CGFloat) -> UIFont {
            return UIFont(name: FONT_TYPE_NunitoSansRegular, size: size)!
        }
        
        static func defaultSemiBoldFontWithSize(_ size: CGFloat) -> UIFont {
            return UIFont(name: FONT_TYPE_NunitoSansSemiBold, size: size)!
        }
    }
}
