//
//  NotificationListController.swift
//  Gupa Network
//
//  Created by MWM23 on 12/3/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher


class NotificationListController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblNotificationList: UITableView!
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var arr_notification : NSMutableArray! = NSMutableArray()
    
    var btnSearchUser:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    
     let identifier = "NotificationCell"
    
    //-===============================
    //MARK: Life cycle methods
    //-===============================
    
    override func viewDidLoad() {
        super.viewDidLoad()

        appDelegate.centerNav = self.navigationController
        
        self.title = "Notifications"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        
        showRightNavigationBarButtonHide()
        
        self.tblNotificationList.estimatedRowHeight = 100
        self.tblNotificationList.rowHeight = UITableViewAutomaticDimension
        self.tblNotificationList.setNeedsLayout()
        self.tblNotificationList.layoutIfNeeded()
        self.tblNotificationList.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        tblNotificationList.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: identifier)
    }

    func showRightNavigationBarButtonHide()                               //RightNavigationBarButton hide unhide
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnSearchUser = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnSearchUser.setImage(imgChat, for: UIControlState())
        btnSearchUser.addTarget(self, action: #selector(NotificationListController.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnSearchUser)
        
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(NotificationListController.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    
   @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         getNotificationList()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var row : Int = Int()
        if self.arr_notification.count != 0 {
            row =  self.arr_notification.count
            return row
        } else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        self.loading.dismiss()
        var cell: NotificationCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? NotificationCell
        
        if cell == nil {
            cell = NotificationCell(style: UITableViewCellStyle.default, reuseIdentifier: identifier)
        }
        let dic: NSDictionary = self.arr_notification.object(at: indexPath.row) as! NSDictionary
        
        
        if let str_title : String = dic["title"] as? String{
            cell?.lbl_post_type.text = str_title
        }
        if let str_time = dic["datetime"] as? String {
            //let str_time = AppTheme().UTCtoLocalTimeConvert(str_time )
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            let date = dateFormatter.date(from: str_time)  // create   date from string
            
            // change to a readable time format and change to local time zone
            dateFormatter.dateFormat = "MMM dd YYYY HH:mm"
            dateFormatter.timeZone = TimeZone.autoupdatingCurrent
            let timeStamp = dateFormatter.string(from: date!)
//            AppTheme().UTCtoLocalTimeConvert(str_time)
            
        cell?.lbl_noti_time.text = timeStamp //AppTheme().UTCtoLocalTimeConvert(str_time)
        }
        if let str_msg : String = dic["message"] as? String{
            cell?.lbl_noti_msg.text = str_msg
        }
        if let str_profile_pic : String = dic["icon"] as? String {
            let url = URL(string:str_profile_pic)
            
            cell?.img_user.kf.setImage(with: url, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: nil)
            
//            cell?.img_user.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var destVC = UserChatOnCommentController()
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
        //destVC.isFromUserComment = "true"
        destVC.isFromTrip = "Notification"
        destVC.dic = self.arr_notification.object(at: indexPath.row) as! NSDictionary
        if let str_noti_id =  (self.arr_notification.object(at: indexPath.row) as AnyObject).value(forKey: "id") as? String{
            callNotificationReadUnread(str_noti_id)
        }
        self.navigationController?.pushViewController(destVC, animated: true)
    }

    //==============================
    //MARK: Loader method
    //==============================
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    func getNotificationList() {
        
        if (!isNetworkAvailable) {
            return
        }
        
        if let user_id = AppTheme.getLoginDetails().object(forKey: "id") {
            let parameters : [String: AnyObject] = ["user_id" : user_id as AnyObject]
            AppTheme().callGetService(String(format: "%@get_notification", AppUrl), param: parameters) { (result, data) -> Void in
                
                if result == "success"{
                    self.loading.dismiss()
                    var respDict : NSDictionary = NSDictionary()
                    var arrOfResponseKey : NSMutableArray = NSMutableArray()
                    respDict = data as! NSDictionary
                    if let array_response : NSArray = respDict["response"] as? NSArray{
                        //arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                        self.arr_notification = array_response.mutableCopy() as! NSMutableArray
                    }else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                        self.arr_notification = array_Response_mutable
                    }
                    self.tblNotificationList.reloadData()
                }
            }
        }
 }
    
    func callNotificationReadUnread(_ noti_id : String) {
        
        if (!isNetworkAvailable) {
            return
        }
        
        let parameters : [String: AnyObject] = ["notification_id" : noti_id as AnyObject]
        AppTheme().callGetService(String(format: "%@read_notification", AppUrl), param: parameters) { (result, data) -> Void in
            
            if result == "success"{
                self.loading.dismiss()
                var respDict : NSDictionary = NSDictionary()
                var arrOfResponseKey : NSMutableArray = NSMutableArray()
                respDict = data as! NSDictionary

            }
        }

    }
}
