//
//  TransportDescriptionDetailController.swift
//  Gupa Network
//
//  Created by MWM23 on 12/14/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class TransportDescriptionDetailController: UIViewController {
 
    @IBOutlet weak var textview_detail_descri: UITextView!
    
    var str_transport_detail : String! // = <#value#>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(TransportDescriptionDetailController.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        if str_transport_detail != nil {
            self.textview_detail_descri.text = str_transport_detail
        }
        // Do any additional setup after loading the view.
    }

    @objc func leftBarBackButton(){
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
