//
//  AirportServiceReviewsController.swift
//  Gupa Network
//
//  Created by MWM23 on 9/21/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher
import IQKeyboardManagerSwift

class AirportServiceReviewsController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    @IBOutlet weak var servicesSegmentList: SDSegmentedControl!
    @IBOutlet weak var tblCommentsList: UITableView!
    @IBOutlet weak var reviewButton: UIButton!
    @IBOutlet weak var btnPostComment: UIButton!
    @IBOutlet weak var txtCommentField: UITextView!
    @IBOutlet weak var serviceImage: UIImageView!   
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var ViewForRating: UIView!
        
    @IBOutlet weak var inputContainerViewBottom: NSLayoutConstraint!

    var loading : JGProgressHUD! = JGProgressHUD()
    var dicData : NSDictionary!
    var iataCode : String!
    var strUserId : String!
    var service_code : String!
    var respDict : NSDictionary = NSDictionary()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var serviceDataDic : NSDictionary = NSDictionary()
    var arrForCollectionView: NSMutableArray = NSMutableArray()
    var dictForServicesDetail : NSMutableDictionary = NSMutableDictionary()
    
    var rating = 0
    var isActive: NSInteger = 0
    var isSelected: Bool = false
    var btnChat:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    
    var isSmileButtonGoodSelect : Bool = false
    var isSmileButtonBadSelect : Bool = false
    var isSmileButtonUglySelect : Bool = false
    
    var databaseManager = DatabaseManager.sharedDBInterface // Local database
    
    static var isFromUser : Bool = false
    
        //-===============================
        //MARK: Life cycle methods
        //-===============================
    
        override func viewDidLoad() {
            
            super.viewDidLoad()
            
           appDelegate.centerNav = self.navigationController
            
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            
            let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
            btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
            btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
            btnLeftBar.addTarget(self, action: #selector(AirportServiceReviewsController.leftBarBackButton), for: .touchUpInside)
            
            let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
            leftBarButton.customView = btnLeftBar
            self.navigationItem.leftBarButtonItem = leftBarButton
            
            self.title = "Review"
            self.txtCommentField.delegate = self
            ViewForRating.isHidden = true
            if let segment_count = serviceDataDic .value(forKey: "ServiceCode") as? String{
                servicesSegmentList.selectedSegmentIndex = Int(segment_count)! - 1
            }
//            servicesSegmentList.selectedSegmentIndex = Int (serviceDataDic .valueForKey("ServiceCode") as! String)! - 1
            if let str_service_code =  serviceDataDic .value(forKey: "ServiceCode") as? String{
                service_code = str_service_code
            }
            if let str_pic =  serviceDataDic .value(forKey: "ServiceImage") as? String{
                let url  = URL(string: str_pic)
                if url != nil {
                serviceImage.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: nil)
                }
            }
            if let str_service_name = serviceDataDic .value(forKey: "ServiceName") as? String {
                serviceTitle.text = str_service_name
            }            
            
            self.tblCommentsList.estimatedRowHeight = 100
            self.tblCommentsList.rowHeight = UITableViewAutomaticDimension
            self.tblCommentsList.setNeedsLayout()
            self.tblCommentsList.layoutIfNeeded()
            self.tblCommentsList.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
           
            //Get review list
            if iataCode != "" {
                self.getReviewsList(iataCode, service: service_code)
            }
            
            
    }
    
    
    @objc func applicationDidBecomeActive(notification: NSNotification) {
        
        self.txtCommentField.resignFirstResponder()
    }
        
    @objc func leftBarBackButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func showRightNavigationBarButtonHide()                               //RightNavigationBarButton hide unhide
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnChat = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnChat.setImage(imgChat, for: UIControlState())
        btnChat.addTarget(self, action: #selector(AirportServiceReviewsController.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnChat)
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "img_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(AirportActivityViewController.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        IQKeyboardManager.sharedManager().enable = false
        
         NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.getAllPostDFromNotificationPostMethod), name: NSNotification.Name(rawValue: "ReviewDatabaseData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive(notification:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AirportServiceReviewsController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AirportServiceReviewsController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReviewDatabaseData") , object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive , object: nil)

        IQKeyboardManager.sharedManager().enable = true
    }
    
    @objc func getAllPostDFromNotificationPostMethod()  {
        
        //Get review list
        if iataCode != "" {
            self.getReviewsList(iataCode, service: service_code)
        }
    }
    
    @objc  func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    @objc func keyboardWillHide(_ sender: Notification) {
        
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                
                self.inputContainerViewBottom.constant =  8
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardWillShow(_ sender: Notification) {
        
        if let userInfo = (sender as NSNotification).userInfo {
            
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                
                self.inputContainerViewBottom.constant = keyboardHeight
                
                    self.view.layoutIfNeeded()
            }
        }
    }
    
    func getReviewsList(_ iataCode: String, service: String)
        {
            if (!isNetworkAvailable) {
                Utility.showNetWorkAlert()
                return
            }
   
            let paraDict = NSMutableDictionary()
            paraDict.setValue(iataCode, forKey:"iata_code")
            paraDict.setValue(service, forKey:"service")
            
            AppTheme().callPostService(String(format: "%@airport_services_review_get", AppUrl), param: paraDict) {(result, data) -> Void in
                
                if result == "success"
                {
                    self.loading.dismiss()
                    self.respDict = data as! NSDictionary
                    if let array_response : NSArray = self.respDict["response"] as? NSArray{
                        self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                    }else if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray{
                        self.arrOfResponseKey = array_Response_mutable
                    }
                    self.tblCommentsList.reloadData()
                } else
                {
                    self.loading.dismiss()
                  //  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                    self.tblCommentsList.reloadData()
                }
            }
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        //========================================================
        //MARK: UITableview Datasource and delegate methods
        //========================================================
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            var row : Int = Int()
            if self.arrOfResponseKey.count != 0
            {
                row =  self.arrOfResponseKey.count
                return row
            } else
            {
                return 0
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            self.loading.dismiss()
            let dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
            
            let cell : CommentCell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
            if let str_userFull =  dic["full_name"] as? String{
                cell.lblSubName.text = str_userFull
            }
            
            if let str_comment =  dic["review"] as? String{
                cell.lblUserComments.text = str_comment
            }
            
            if let str_profile_pic = dic["profile_pic"] as? String {
                let url = URL(string: str_profile_pic)
                if url != nil {
                cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: nil)
                }
            }
            
            var rating : NSInteger = 0
            if let strRating: String = dic["rating"] as? String
            {
                rating = NSInteger(strRating)!

            } else if let strRating: NSInteger = dic["rating"] as? NSInteger{
                rating = strRating
            }
            if rating == 1 {
                 cell.img_rating.image = #imageLiteral(resourceName: "good_on")

            } else if rating == 2
            {
                cell.img_rating.image = #imageLiteral(resourceName: "bad_on")

            } else if rating == 3 {
                cell.img_rating.image = #imageLiteral(resourceName: "bad")
                
            } else  {
                cell.img_rating.image = #imageLiteral(resourceName: "good_off")
            }

            if let str_date = dic["create_date"] as? String {
                cell.lbl_date.text = str_date
            }
            return cell
        }
        
        //==========================================
        //MARK: Post comment action & rating action
        //==========================================
        
        @IBAction func btnPostComment(_ sender: AnyObject)
        {
            self.startProgressBar()
            
                if (!isNetworkAvailable) {
                    
                    self.loading.dismiss()

                if let str_user_ID = AppTheme.getLoginDetails().value(forKey: "id") {
                        
                    self.strUserId =  str_user_ID as! String
                        
                    if txtCommentField.text != "Write review" && txtCommentField.text != "" || rating == 0
                    {
                        if txtCommentField.text != "Write review"  && txtCommentField.text != "" ||  rating == 0  {
                                
                        if txtCommentField.text == "Write review" && rating == 0 {
                            self.loading.dismiss()
                            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter the review or rating.", showInView: self.view)
                        } else {
                            if txtCommentField.text == "Write review" {
                            txtCommentField.text = ""
                        }
                            
                        let paraDict = NSMutableDictionary()

                        if strUserId != nil || strUserId != ""  {
                            
                            paraDict.setValue(self.strUserId, forKey:"user_id")
                            paraDict.setValue(iataCode, forKey:"iata_code")
                            paraDict.setValue(service_code, forKey:"service")
                            paraDict.setValue(txtCommentField.text, forKey:"review")
                            paraDict.setValue(self.rating, forKey:"rating")
                        }
                                        
                    
                    var theJSONText = String()
                    if let theJSONData = try? JSONSerialization.data(
                        withJSONObject: paraDict,
                        options: []) {
                        theJSONText = String(data: theJSONData,
                                             encoding: .ascii)!
                        print("JSON string = \(theJSONText)")
                    }
                    
                    let reviewDetail = Review(postIntId: 0, strAppUrl: "Review", strAppUrlType: "post", strParamData:
                        theJSONText)
                    
                    let strFullName = AppTheme.getUserFullName()
                    print("UserFullName = \(strFullName)")
                    
                    var urlImage: URL!
                    
                    if let str_profile_pic : String = AppTheme.getLoginDetails().value(forKey: "profile_pic") as? String{
                        urlImage = URL(string: str_profile_pic)!
                        print("str_profile_pic = \(urlImage!)")
                    }
                    
                    //self.loading.dismiss()
                    
                    let todayDate : String!
                    let todayTime : String!
                    
                    todayDate = getTodayDate()
                    todayTime = getCurentTime()
                    
                    print("todayDate = \(todayDate!)")
                    print("todayTime = \(todayTime!)")
                            
                    let createdDate = "\(todayDate!) \(todayTime!)"
                    
                    var username = ""
                    if let str_user_name : String = UserDefaults.standard.string(forKey: "UserName") {
                        username = str_user_name
                    }
                    let tempDict  = ["user_id" : self.strUserId , "review_id": "" , "iata_code": iataCode , "services": service_code ,"review": txtCommentField.text, "create_date": createdDate , "full_name": strFullName , "username": username, "profile_pic": urlImage, "rating": self.rating] as [String : Any]
                            
                    self.arrOfResponseKey.add(tempDict)
                    self.tblCommentsList.reloadData()
                    
                    self.txtCommentField.text = ""
                    populateCategoryDetails(reviewDetails: reviewDetail)
                  }
                }
               }
            }
        } else {
                
            if let str_user_ID = AppTheme.getLoginDetails().value(forKey: "id") {
                
                self.strUserId =  str_user_ID as! String
                
                if txtCommentField.text != "Write review" && txtCommentField.text != "" || rating == 0
                {
                    if txtCommentField.text != "Write review"  && txtCommentField.text != "" ||  rating == 0  {
                        
                        if txtCommentField.text == "Write review" && rating == 0 {
                            self.loading.dismiss()
                           _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter the review or rating.", showInView: self.view)
                        } else {
                            if txtCommentField.text == "Write review" {
                                txtCommentField.text = ""
                            }
                            
                            if strUserId != nil || strUserId != ""  {

                                
                                let paraDict = NSMutableDictionary()
                                paraDict.setValue(self.strUserId, forKey:"user_id")
                                paraDict.setValue(iataCode, forKey:"iata_code")
                                paraDict.setValue(service_code, forKey:"service")
                                paraDict.setValue(txtCommentField.text, forKey:"review")
                                paraDict.setValue(self.rating, forKey:"rating")
                                
                                AppTheme().callPostService(String(format: "%@airport_services_review?", AppUrl), param: paraDict) {(result, data) -> Void in                                                                  ///Call services
                                    
                                    if result == "success"
                                    {
                                        
                                        self.loading.dismiss()
                                        self.txtCommentField.text = ""
                                        self.rating = 0
                                        _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Comment sent", showInView: self.view)
                                        self.tblCommentsList.isHidden = false
                                        self.getReviewsList(self.iataCode, service: self.service_code)
                                    } else{
                                        self.loading.dismiss()
                                        _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                                    }
                                }
                            }
                        }
                    } else {
                        self.loading.dismiss()
                        _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter the review or rating.", showInView: self.view)
                    }
            } else {
                    self.loading.dismiss()
                    _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter the review or rating.", showInView: self.view)
            }
          }
        }
    }
                    
    ///** To Insert INTO Database
    func populateCategoryDetails(reviewDetails : Review?){
        
        databaseManager.addReviewToDatabase(review: reviewDetails!)
    }
    
    func getTodayDate() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
        
        let today_string = String(year!) + "-" + String(month!) + "-" + String(day!)
        
        return today_string
        
    }
    
    func getCurentTime() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.hour,.minute,.second], from: date)
        
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
        
    }
    
    @IBAction func ratingButtonAction(_ sender: AnyObject) {
       }
    
    @IBAction func changeSegmantValue(_ sender: SDSegmentedControl) {
        updateSegmentValue()
    }
    
    // MARK: -  UItextView Delegate
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == txtCommentField
        {
            if txtCommentField.text == "Write review" {
                txtCommentField.text = ""
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtCommentField
        {
            if txtCommentField.text == "Write review" {
                txtCommentField.text = ""
            }
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == txtCommentField{
            if txtCommentField.text == "" {
                txtCommentField.text = "Write review"
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.txtCommentField.resignFirstResponder()
    }
        //======================================
        //MARK: Loader method, Get Review list
        //======================================
        func startProgressBar()
        {
            self.loading = JGProgressHUD(style: .dark)
            self.loading.textLabel.text = "Loading.."
            self.loading.show(in: self.view)
        }
     @IBAction func btnSmile(_ sender: UIButton) {
        
        if isActive == 0 {
           isActive = 1
           self.ViewForRating.isHidden = false
           self.txtCommentField.isUserInteractionEnabled = false
         }else{
           isActive = 0
           self.ViewForRating.isHidden = true
           self.txtCommentField.isUserInteractionEnabled = true
         }
    }
    
    @IBAction func btnSmileAction(_ sender: UIButton) {
        
        isActive = 0
        if sender.tag == 1 {
            if isSmileButtonGoodSelect == false{
                isSmileButtonGoodSelect = true
                isSmileButtonUglySelect = false
                isSmileButtonBadSelect = false
                self.rating = 1
                self.reviewButton.setImage(UIImage(named: "good_on.png")! as UIImage, for: UIControlState())
            }
        }
        else if sender.tag == 2
        {
            if isSmileButtonBadSelect == false {
                isSmileButtonGoodSelect = false
                isSmileButtonUglySelect = false
                isSmileButtonBadSelect = true
                self.rating = 2
                self.reviewButton.setImage(UIImage(named: "bad_on.png")! as UIImage, for: UIControlState())
            }

         } else if sender.tag == 3 {
            if isSmileButtonUglySelect == false {
                isSmileButtonUglySelect = true
                isSmileButtonGoodSelect = false
                isSmileButtonBadSelect = false
                self.rating = 3
                self.reviewButton.setImage(UIImage(named: "bad.png")! as UIImage, for: UIControlState())
            }

        }
        self.txtCommentField.isUserInteractionEnabled = true
        self.ViewForRating.isHidden = true
    }
    
    
    func updateSegmentValue() {
        
        dictForServicesDetail = arrForCollectionView.object(at: servicesSegmentList.selectedSegmentIndex) as! NSMutableDictionary
        self.rating = 0
        isSmileButtonUglySelect = false
        isSmileButtonGoodSelect = false
        isSmileButtonBadSelect = false
        serviceImage.image = UIImage(named: dictForServicesDetail .value(forKey: "ServiceImage") as! String )
        serviceTitle.text = dictForServicesDetail .value(forKey: "ServiceName") as? String
        service_code = dictForServicesDetail.value(forKey: "ServiceCode") as! String
        arrOfResponseKey.removeAllObjects()
        getReviewsList(iataCode, service: service_code)
    }
}
