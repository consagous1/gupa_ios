//
//  HotelRatingBarVC.swift
//  Gupa Network
//
//  Created by mac on 6/4/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import PNChartSwift
import JGProgressHUD



class HotelRatingBarVC: UIViewController, SimpleBarChartDataSource, SimpleBarChartDelegate
{
    
    var loading : JGProgressHUD = JGProgressHUD(style: JGProgressHUDStyle.dark)
    var barChartForFlight: SimpleBarChart!
    var barChartForAirWays: SimpleBarChart!
    var barChartForHotel: SimpleBarChart!
    var barChartForGroupHotel: SimpleBarChart!
    
    var valuesFlighHotel : NSArray! // = ()
    var valuesAirwaysGroup : NSArray!
    var barColors: NSArray = NSArray()
    var textArr: NSArray!
    var hotelFlightDic: NSDictionary = NSDictionary()
    var currentBarColors : NSInteger = NSInteger()
    var flightHotelDetailDict : NSDictionary = NSDictionary()
    
    
    var arrOfResponseKey: NSMutableArray = NSMutableArray()
    var dicOfResponseKey: NSMutableDictionary = NSMutableDictionary()
    
    var strCheck: NSString?
    var strHotelId: NSString?
    var strFlightId: NSString?
    var strBusId: NSString?
    var imgViewRating: UIImageView!
    
    
    //-===============================
    //MARK: Life cycle methods
    //-===============================
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    
        let btnLeftBar: UIButton = UIButton ()                     // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnLeftBar.addTarget(self, action: #selector(HotelRatingBarVC.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()        //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        let btnRighBar: UIButton = UIButton ()
        btnRighBar.setImage(UIImage(named: "Home-icon.png"), for: UIControlState())
        btnRighBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnRighBar.addTarget(self, action: #selector(HotelRatingBarVC.rightBarBackButton), for: .touchUpInside)
        let rightBarButton:UIBarButtonItem = UIBarButtonItem()       //.....Set right button
        rightBarButton.customView = btnRighBar
        self.navigationItem.rightBarButtonItem = rightBarButton        
       
        
        barChartForFlight = SimpleBarChart()                   // Create SimpleBarChart here
        barChartForAirWays = SimpleBarChart()
        barChartForHotel = SimpleBarChart()
        barChartForGroupHotel = SimpleBarChart()
        textArr = ["Excellent", "Good", "Bad"]
        
       
        
        if strCheck == "comeFromHotel"{                                  //Check for Hotel
                   self.title = "Hotel Ratings Chart"
                    var ratingGood : NSInteger = 0
                    var ratingBad : NSInteger = 0
                    var ratingUgly : NSInteger = 0
                    
                    if let strGoodRating: String = self.flightHotelDetailDict["Good"] as? String                                       //Check for string/int
                    {
                        ratingGood = NSInteger(strGoodRating)!
                    }
                    else if let strGoodRating: NSInteger = self.flightHotelDetailDict["Good"] as? NSInteger
                    {
                        ratingGood = strGoodRating
                    }
                    if let strBadRating: String = self.flightHotelDetailDict["Bad"] as? String
                    {
                        ratingBad = NSInteger(strBadRating)!
                    }
                    else if let strBadRating: NSInteger = self.flightHotelDetailDict["Bad"] as? NSInteger
                    {
                        ratingBad = strBadRating
                    }
                    if let strUglyRating: String = self.flightHotelDetailDict["Ugly"] as? String
                    {
                        ratingUgly = NSInteger(strUglyRating)!
                    }
                    else if let strUglyRating: NSInteger = self.flightHotelDetailDict["Ugly"] as? NSInteger
                    {
                        ratingUgly = strUglyRating
                    }
                    
                    self.valuesFlighHotel = [ratingGood , ratingBad, ratingUgly]     //Create chart for Hotel indivisual
                    let colorCodeForGood = UIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1)
                    let colorCodeForBad = UIColor(red: 0, green: 128/255, blue: 0, alpha: 1)
                    // let colorCodeForUgly = UIColor(red: 1, green: 215/255, blue: 0, alpha: 1)
                    self.barColors = [colorCodeForGood, colorCodeForBad, UIColor.red]
                    
                    let frameBarHotel: CGRect = CGRect(x: 20, y: 100, width: self.view.frame.size.width-50, height: 100)                                                        //Set Frame here
                    self.barChartForHotel = SimpleBarChart.init(frame: frameBarHotel)
                    self.barChartForHotel.backgroundColor = UIColor.clear
                    self.barChartForHotel.animationDuration = 2.0
                    self.barChartForHotel.xLabelType = SimpleBarChartXLabelTypeHorizontal
                    self.barChartForHotel.barWidth = 28.0
                    self.barChartForHotel.incrementValue = 0.5
                    self.barChartForHotel.barTextColor = UIColor.black
                    
                    self.view.addSubview(self.barChartForHotel)
                    self.barChartForHotel.delegate = self
                    self.barChartForHotel.dataSource = self
                    
                    let imageName = "imgRating.png"
                    let image = UIImage(named: imageName)
                    let imageView = UIImageView(image: image!)
                    imageView.frame = CGRect(x: 25, y: 198, width: 20, height: 20)
                    self.view.addSubview(imageView)
                    
                    let label = UILabel(frame: CGRect(x: 50, y: 204, width: 50, height: 8))
                    label.textAlignment = NSTextAlignment.left
                    label.font = UIFont.boldSystemFont(ofSize: 8)
                    label.text = "Rating"
                    self.view.addSubview(label)
                    
                    let labelHotel = UILabel(frame: CGRect(x: 55, y: 220, width: 250, height: 13))
                    labelHotel.textAlignment = NSTextAlignment.left
                    if let str_hotel_name : String = self.flightHotelDetailDict["hotel_name"] as? String {
                    labelHotel.text = " HOTEL: \(str_hotel_name)"
                     }
                    //labelHotel.text = " HOTEL: \(self.flightHotelDetailDict["hotel_name"] as! String)" // "Hotel:%@",self.hotelFlightDic["hotel_name"] as? String
                    labelHotel.font = labelHotel.font.withSize(12)
                    self.view.addSubview(labelHotel)
                    self.barChartForHotel.reloadData()
                    
                    self.valuesAirwaysGroup = [0,  0, 1]
        }else if strCheck == "comeFromTransportRating"{
            self.title = "Road Transport Ratings Chart"
            var ratingGood : NSInteger = 0
            var ratingBad : NSInteger = 0
            var ratingUgly : NSInteger = 0
            
            if let strGoodRating: String = self.flightHotelDetailDict["bus_good"] as? String    //Check for string/int
            {
                ratingGood = NSInteger(strGoodRating)!
            }
            else if let strGoodRating: NSInteger = self.flightHotelDetailDict["bus_good"] as? NSInteger
            {
                ratingGood = strGoodRating
            }
            if let strBadRating: String = self.flightHotelDetailDict["bus_bad"] as? String
            {
                ratingBad = NSInteger(strBadRating)!
            }
            else if let strBadRating: NSInteger = self.flightHotelDetailDict["bus_bad"] as? NSInteger
            {
                ratingBad = strBadRating
            }
            if let strUglyRating: String = self.flightHotelDetailDict["bus_ugly"] as? String
            {
                ratingUgly = NSInteger(strUglyRating)!
            }
            else if let strUglyRating: NSInteger = self.flightHotelDetailDict["bus_ugly"] as? NSInteger
            {
                ratingUgly = strUglyRating
            }
            //Create chart for Flight indivisual
            self.valuesFlighHotel = [ratingGood, ratingBad, ratingUgly]
            let colorCodeForGood2 = UIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1)
            let colorCodeForBad2 = UIColor(red: 0, green: 128/255, blue: 0, alpha: 1)
            // let colorCodeForUgly = UIColor(red: 1, green: 215/255, blue: 0, alpha: 1)
            self.barColors = [colorCodeForGood2, colorCodeForBad2, UIColor.red]
            
            let frameBarHotel: CGRect = CGRect(x: 20, y: 100, width: self.view.frame.size.width-50, height: 100)
            self.barChartForFlight = SimpleBarChart.init(frame: frameBarHotel)
            self.barChartForFlight.backgroundColor = UIColor.clear
            self.barChartForFlight.animationDuration = 2.0
            self.barChartForFlight.xLabelType = SimpleBarChartXLabelTypeHorizontal
            self.barChartForFlight.barWidth = 28.0
            self.barChartForFlight.incrementValue = 0.5
            self.barChartForFlight.barTextColor = UIColor.black
            
            self.view.addSubview(self.barChartForFlight)
            self.barChartForFlight.delegate = self
            self.barChartForFlight.dataSource = self
            
            let imageName = "imgRating.png"
            let image = UIImage(named: imageName)
            let imageView = UIImageView(image: image!)
            imageView.frame = CGRect(x: 25, y: 205, width: 20, height: 20)
            self.view.addSubview(imageView)
            
            let label = UILabel(frame: CGRect(x: 50, y: 211, width: 50, height: 8))
            label.textAlignment = NSTextAlignment.left
            label.font = UIFont.boldSystemFont(ofSize: 8)
            label.text = "Rating"
            self.view.addSubview(label)
            
            let lblFlight = UILabel(frame: CGRect(x: 50, y: 220, width: 250, height: 13))
            lblFlight.textAlignment = NSTextAlignment.left
            lblFlight.text = "BUS:  \(self.flightHotelDetailDict["transport_name"] as! String) \(self.flightHotelDetailDict["bus_id"] as! String)"
            lblFlight.font = lblFlight.font.withSize(12)
            self.view.addSubview(lblFlight)
            
            self.barChartForFlight.reloadData()
            
            //////////////////////////////////////
            
            var ratingGoodAirline : NSInteger = 0
            var ratingBadAirline : NSInteger = 0
            var ratingUglyAirline : NSInteger = 0
            
            if let strGoodRatingAirline: String = self.flightHotelDetailDict["transport_Good"] as? String    //Check for string/int
            {
                ratingGoodAirline = NSInteger(strGoodRatingAirline)!
            }
            else if let strGoodRatingAirline: NSInteger = self.flightHotelDetailDict["transport_Good"] as? NSInteger
            {
                ratingGoodAirline = strGoodRatingAirline
            }
            if let strBadRatingAirline: String = self.flightHotelDetailDict["transport_Bad"] as? String
            {
                ratingBadAirline = NSInteger(strBadRatingAirline)!
            }
            else if let strBadRatingAirline: NSInteger = self.flightHotelDetailDict["transport_Bad"] as? NSInteger
            {
                ratingBadAirline = strBadRatingAirline
            }
            if let strUglyRatingAirline: String = self.flightHotelDetailDict["transport_Ugly"] as? String
            {
                ratingUglyAirline = NSInteger(strUglyRatingAirline)!
            }
            else if let strUglyRatingAirline: NSInteger = self.flightHotelDetailDict["transport_Ugly"] as? NSInteger
            {
                ratingUglyAirline = strUglyRatingAirline
            }
            
            self.valuesAirwaysGroup = [ratingGoodAirline, ratingBadAirline, ratingUglyAirline]       //Create chart for Airways group
            let colorCodeForGood3 = UIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1)
            let colorCodeForBad3 = UIColor(red: 0, green: 128/255, blue: 0, alpha: 1)
            // let colorCodeForUgly = UIColor(red: 1, green: 215/255, blue: 0, alpha: 1)
            self.barColors = [colorCodeForGood3, colorCodeForBad3, UIColor.red]
            
            let frameBarAirways: CGRect = CGRect(x: 20, y: 300, width: self.view.frame.size.width-50, height: 100)
            self.barChartForAirWays = SimpleBarChart.init(frame: frameBarAirways)
            self.barChartForAirWays.backgroundColor = UIColor.clear
            self.barChartForAirWays.animationDuration = 2.0
            self.barChartForAirWays.xLabelType = SimpleBarChartXLabelTypeHorizontal
            self.barChartForAirWays.barWidth = 28.0
            self.barChartForAirWays.incrementValue = 0.5
            self.barChartForAirWays.barTextColor = UIColor.black
            
            self.view.addSubview(self.barChartForAirWays)
            self.barChartForAirWays.delegate = self
            self.barChartForAirWays.dataSource = self
            
            let imageName1 = "imgRating.png"
            let image1 = UIImage(named: imageName1)
            let imageView1 = UIImageView(image: image1!)
            imageView1.frame = CGRect(x: 25, y: 405, width: 20, height: 20)
            self.view.addSubview(imageView1)
            
            let labelHotelGrp = UILabel(frame: CGRect(x: 50, y: 411, width: 50, height: 8))
            labelHotelGrp.textAlignment = NSTextAlignment.left
            labelHotelGrp.text = "Rating"
            labelHotelGrp.font =  UIFont.boldSystemFont(ofSize: 8)
            self.view.addSubview(labelHotelGrp)
            
            let labelAirjet = UILabel(frame: CGRect(x: 50, y: 420, width: 250, height: 13))
            labelAirjet.textAlignment = NSTextAlignment.left
            labelAirjet.text = "TRANSPORT COMPANY: \(self.flightHotelDetailDict["transport_name"] as! String)"
            labelAirjet.font = labelAirjet.font.withSize(12)
            self.view.addSubview(labelAirjet)
            
            self.barChartForAirWays.reloadData()
        }else{
                self.title = "Flight Ratings Chart"
                var ratingGood : NSInteger = 0
                var ratingBad : NSInteger = 0
                var ratingUgly : NSInteger = 0
            
                if let strGoodRating: String = self.flightHotelDetailDict["Good"] as? String    //Check for string/int
                {
                    ratingGood = NSInteger(strGoodRating)!
                }
                else if let strGoodRating: NSInteger = self.flightHotelDetailDict["Good"] as? NSInteger
                {
                    ratingGood = strGoodRating
                }
                if let strBadRating: String = self.flightHotelDetailDict["Bad"] as? String
                {
                    ratingBad = NSInteger(strBadRating)!
                }
                else if let strBadRating: NSInteger = self.flightHotelDetailDict["Bad"] as? NSInteger
                {
                    ratingBad = strBadRating
                }
                if let strUglyRating: String = self.flightHotelDetailDict["Ugly"] as? String
                {
                    ratingUgly = NSInteger(strUglyRating)!
                }
                else if let strUglyRating: NSInteger = self.flightHotelDetailDict["Ugly"] as? NSInteger
                {
                    ratingUgly = strUglyRating
                }
                                                        //Create chart for Flight indivisual
                self.valuesFlighHotel = [ratingGood, ratingBad, ratingUgly]
                let colorCodeForGood2 = UIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1)
                let colorCodeForBad2 = UIColor(red: 0, green: 128/255, blue: 0, alpha: 1)
                // let colorCodeForUgly = UIColor(red: 1, green: 215/255, blue: 0, alpha: 1)
                self.barColors = [colorCodeForGood2, colorCodeForBad2, UIColor.red]
                
                let frameBarHotel: CGRect = CGRect(x: 20, y: 100, width: self.view.frame.size.width-50, height: 100)
                self.barChartForFlight = SimpleBarChart.init(frame: frameBarHotel)
                self.barChartForFlight.backgroundColor = UIColor.clear
                self.barChartForFlight.animationDuration = 2.0
                self.barChartForFlight.xLabelType = SimpleBarChartXLabelTypeHorizontal
                self.barChartForFlight.barWidth = 28.0
                self.barChartForFlight.incrementValue = 0.5
                self.barChartForFlight.barTextColor = UIColor.black
                
                self.view.addSubview(self.barChartForFlight)
                self.barChartForFlight.delegate = self
                self.barChartForFlight.dataSource = self
                
                let imageName = "imgRating.png"
                let image = UIImage(named: imageName)
                let imageView = UIImageView(image: image!)
                imageView.frame = CGRect(x: 25, y: 205, width: 20, height: 20)
                self.view.addSubview(imageView)
                
                let label = UILabel(frame: CGRect(x: 50, y: 211, width: 50, height: 8))
                label.textAlignment = NSTextAlignment.left
                label.font = UIFont.boldSystemFont(ofSize: 8)
                label.text = "Rating"
                self.view.addSubview(label)
                
                let lblFlight = UILabel(frame: CGRect(x: 50, y: 220, width: 250, height: 13))
                lblFlight.textAlignment = NSTextAlignment.left
                lblFlight.text = "FLIGHT:  \(self.flightHotelDetailDict["airline_name"] as! String) \(self.flightHotelDetailDict["flight_id"] as! String)"
                lblFlight.font = lblFlight.font.withSize(12)
                self.view.addSubview(lblFlight)
                
                self.barChartForFlight.reloadData()
                
              //////////////////////////////////////
                
                var ratingGoodAirline : NSInteger = 0
                var ratingBadAirline : NSInteger = 0
                var ratingUglyAirline : NSInteger = 0
                
                if let strGoodRatingAirline: String = self.flightHotelDetailDict["Airline_Good"] as? String    //Check for string/int
                {
                    ratingGoodAirline = NSInteger(strGoodRatingAirline)!
                }
                else if let strGoodRatingAirline: NSInteger = self.flightHotelDetailDict["Airline_Good"] as? NSInteger
                {
                    ratingGoodAirline = strGoodRatingAirline
                }
                if let strBadRatingAirline: String = self.flightHotelDetailDict["Airline_Bad"] as? String
                {
                    ratingBadAirline = NSInteger(strBadRatingAirline)!
                }
                else if let strBadRatingAirline: NSInteger = self.flightHotelDetailDict["Airline_Bad"] as? NSInteger
                {
                    ratingBadAirline = strBadRatingAirline
                }
                if let strUglyRatingAirline: String = self.flightHotelDetailDict["Airline_Ugly"] as? String
                {
                    ratingUglyAirline = NSInteger(strUglyRatingAirline)!
                }
                else if let strUglyRatingAirline: NSInteger = self.flightHotelDetailDict["Airline_Ugly"] as? NSInteger
                {
                    ratingUglyAirline = strUglyRatingAirline
                }
            
                self.valuesAirwaysGroup = [ratingGoodAirline, ratingBadAirline, ratingUglyAirline]       //Create chart for Airways group
                let colorCodeForGood3 = UIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1)
                let colorCodeForBad3 = UIColor(red: 0, green: 128/255, blue: 0, alpha: 1)
                // let colorCodeForUgly = UIColor(red: 1, green: 215/255, blue: 0, alpha: 1)
                self.barColors = [colorCodeForGood3, colorCodeForBad3, UIColor.red]
                
                let frameBarAirways: CGRect = CGRect(x: 20, y: 300, width: self.view.frame.size.width-50, height: 100)
                self.barChartForAirWays = SimpleBarChart.init(frame: frameBarAirways)
                self.barChartForAirWays.backgroundColor = UIColor.clear
                self.barChartForAirWays.animationDuration = 2.0
                self.barChartForAirWays.xLabelType = SimpleBarChartXLabelTypeHorizontal
                self.barChartForAirWays.barWidth = 28.0
                self.barChartForAirWays.incrementValue = 0.5
                self.barChartForAirWays.barTextColor = UIColor.black
                
                self.view.addSubview(self.barChartForAirWays)
                self.barChartForAirWays.delegate = self
                self.barChartForAirWays.dataSource = self
                
                let imageName1 = "imgRating.png"
                let image1 = UIImage(named: imageName1)
                let imageView1 = UIImageView(image: image1!)
                imageView1.frame = CGRect(x: 25, y: 405, width: 20, height: 20)
                self.view.addSubview(imageView1)
                
                let labelHotelGrp = UILabel(frame: CGRect(x: 50, y: 411, width: 50, height: 8))
                labelHotelGrp.textAlignment = NSTextAlignment.left
                labelHotelGrp.text = "Rating"
                labelHotelGrp.font =  UIFont.boldSystemFont(ofSize: 8)
                self.view.addSubview(labelHotelGrp)
                
                let labelAirjet = UILabel(frame: CGRect(x: 50, y: 420, width: 250, height: 13))
                labelAirjet.textAlignment = NSTextAlignment.left
                labelAirjet.text = "AIRLINE: \(self.flightHotelDetailDict["airline_name"] as! String)"
                labelAirjet.font = labelAirjet.font.withSize(12)
                self.view.addSubview(labelAirjet)
                
                self.barChartForAirWays.reloadData()
            
        }
        
    }
    
    //Helper method for right navigationbar button
    @objc func rightBarBackButton(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @objc  func leftBarBackButton(){                //Helper method for right navigationbar button
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //=================================
    //MARK: Chart Bar delegate methods
    //=================================
    
    func numberOfBars(in barChart: SimpleBarChart!) -> UInt {
        if strCheck == "comeFromHotel"
        {
            if barChart == barChartForHotel
            {
            let row: Int = self.valuesFlighHotel.count as Int
            return  UInt (row)
            }
            else
            {
            let row1: Int = self.valuesAirwaysGroup.count as Int
            return  UInt (row1)
            }
        }
        else
        {
            if barChart == barChartForFlight
            {
                let row: Int = self.valuesFlighHotel.count as Int
                return  UInt (row)
            }
            else
            {
                let row1: Int = self.valuesAirwaysGroup.count as Int
                return  UInt (row1)
            }
        }
    }
    
    func barChart(_ barChart: SimpleBarChart!, valueForBarAt index: UInt) -> CGFloat {
        if strCheck == "comeFromHotel"
        {
            if barChart == barChartForHotel
            {
                return CGFloat( self.valuesFlighHotel.object(at: Int(index)) as! NSNumber)
            }
            else
            {
                return CGFloat( self.valuesAirwaysGroup.object(at: Int(index)) as! NSNumber)
            }
        }
        else
        {
            if barChart == barChartForFlight
            {
                return CGFloat( self.valuesFlighHotel.object(at: Int(index)) as! NSNumber)
            }
            else
            {
               return CGFloat( self.valuesAirwaysGroup.object(at: Int(index)) as! NSNumber)
            }
        }
    }
    
    
    func barChart(_ barChart: SimpleBarChart!, textForBarAt index: UInt) -> String! {
       
        if strCheck == "comeFromHotel"
        {
            if barChart == barChartForHotel
            {
                let str: NSNumber = self.valuesFlighHotel.object(at: Int (index)) as! NSNumber
                return String(describing: str)
            }
            else
            {
                let str: NSNumber = self.valuesAirwaysGroup.object(at: Int (index)) as! NSNumber
                return String(describing: str)
            }
        }
        else
        {
            if barChart == barChartForFlight
            {
                let str: NSNumber = self.valuesFlighHotel.object(at: Int (index)) as! NSNumber
                return String(describing: str)
            }
            else
            {
                let str: NSNumber = self.valuesAirwaysGroup.object(at: Int (index)) as! NSNumber
                return String(describing: str)
            }
        }
    }
    
    
    func barChart(_ barChart: SimpleBarChart!, xLabelForBarAt index: UInt) -> String! {
       
        if strCheck == "comeFromHotel"
        {
               return String(self.textArr.object(at: Int(index)) as! String)

        }
        else
        {
               return String(self.textArr.object(at: Int(index)) as! String)
        }
    }
    
    func barChart(_ barChart: SimpleBarChart!, colorForBarAt index: UInt) -> UIColor! {
        return (self.barColors.object(at: Int(index)) as! UIColor)
    }
}
