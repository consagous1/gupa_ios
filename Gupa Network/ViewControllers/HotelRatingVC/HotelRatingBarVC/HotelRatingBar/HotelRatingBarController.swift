//
//  HotelRatingBarController.swift
//  Gupa Network
//
//  Created by MWM23 on 12/16/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
//import Charts

class HotelRatingBarController: UIViewController {
    
    struct Sale {
    var month: String
    var value: Double
}

    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var lbl_title : UILabel! // BarChartView!
    
    var flightHotelDetailDict: NSDictionary!
    var strCheck : String! // = <#value#>
    var strHotelId: NSString?
override func viewDidLoad() {
    super.viewDidLoad()
    super.viewDidLoad()
    self.navigationController?.isNavigationBarHidden = false
    self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    
    let btnLeftBar: UIButton = UIButton ()                     // Create bar button here
    btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
    btnLeftBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
    btnLeftBar.addTarget(self, action: #selector(HotelRatingBarController.leftBarBackButton), for: .touchUpInside)
    
    let leftBarButton:UIBarButtonItem = UIBarButtonItem()        //.....Set left button
    leftBarButton.customView = btnLeftBar
    self.navigationItem.leftBarButtonItem = leftBarButton
    
    let btnRighBar: UIButton = UIButton ()
    btnRighBar.setImage(UIImage(named: "Home-icon.png"), for: UIControlState())
    btnRighBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
    btnRighBar.addTarget(self, action: #selector(HotelRatingBarController.rightBarBackButton), for: .touchUpInside)
    let rightBarButton:UIBarButtonItem = UIBarButtonItem()       //.....Set right button
    rightBarButton.customView = btnRighBar
    self.navigationItem.rightBarButtonItem = rightBarButton
    
    self.title = "Hotel Ratings Chart"
    // Customization
    barChart.descriptionText = ""
    barChart.xAxis.labelPosition = .bottom
//    barChart.xAxis.setLabelsToSkip(0)
    barChart.leftAxis.axisMinValue = 0.0
    barChart.leftAxis.axisMaxValue = 10.0
    barChart.rightAxis.enabled = false
    barChart.xAxis.drawGridLinesEnabled = false
    barChart.legend.enabled = true
    barChart.scaleYEnabled = true
    barChart.scaleXEnabled = true
    barChart.pinchZoomEnabled = false
    barChart.doubleTapToZoomEnabled = false
    barChart.highlighter = nil
    
    
    
    let months = ["Exellent", "Good", "Bad"]
    var sales = [Sale]()
    var j : NSInteger = 0
    var ratingGood : NSInteger = 0
    var ratingBad : NSInteger = 0
    var ratingUgly : NSInteger = 0
    var values : NSMutableArray = NSMutableArray()
    
    if self.flightHotelDetailDict.count != 0 {
        if let strGoodRating: String = self.flightHotelDetailDict["Good"] as? String                                       //Check for string/int
        {
            ratingGood = NSInteger(strGoodRating)!
        }
        else if let strGoodRating: NSInteger = self.flightHotelDetailDict["Good"] as? NSInteger
        {
            ratingGood = strGoodRating
        }
        if let strBadRating: String = self.flightHotelDetailDict["Bad"] as? String
        {
            ratingBad = NSInteger(strBadRating)!
        }
        else if let strBadRating: NSInteger = self.flightHotelDetailDict["Bad"] as? NSInteger
        {
            ratingBad = strBadRating
        }
        if let strUglyRating: String = self.flightHotelDetailDict["Ugly"] as? String
        {
            ratingUgly = NSInteger(strUglyRating)!
        }
        else if let strUglyRating: NSInteger = self.flightHotelDetailDict["Ugly"] as? NSInteger
        {
            ratingUgly = strUglyRating
        }
        
        values = [ratingGood , ratingBad, ratingUgly]
        if let str_hotel_name : String = self.flightHotelDetailDict["hotel_name"] as? String {
            lbl_title.text = " HOTEL: \(str_hotel_name)"
        }
        
        
        for month in months {
            let sale = Sale(month: month, value:Double(values.object(at: j) as! NSNumber))
            sales.append(sale)
            j += 1
        }
        
        
        
        // Get and prepare the data
        //let sales = DataGenerator.data()
        
        // Initialize an array to store chart data entries (values; y axis)
        var salesEntries = [ChartDataEntry]()
        
        // Initialize an array to store months (labels; x axis)
        var salesMonths = [String]()
        
        var i = 0
        for sale in sales {
            // Create single chart data entry and append it to the array
//            let saleEntry = BarChartDataEntry(value: sale.value, xIndex: i)
            
            let saleEntry = BarChartDataEntry.init(x: Double(i), yValues: [sale.value])
            
            salesEntries.append(saleEntry)
            
            // Append the month to the array
            salesMonths.append(sale.month)
            
            i += 1
        }
        
        // Create bar chart data set containing salesEntries
        let chartDataSet = BarChartDataSet(values: salesEntries, label: "Profit")
        chartDataSet.colors = [NSUIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1),NSUIColor(red: 0, green: 128/255, blue: 0, alpha: 1), NSUIColor.red]
        
        // Create bar chart data with data set and array with values for x axis
//        let chartData = BarChartData(xVals: salesMonths, dataSets: [chartDataSet])
        
        let chartData = BarChartData.init(dataSets: [chartDataSet])
        
        // Set bar chart data to previously created data
        barChart.data = chartData
        
        // Animation
        barChart.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuart)
    }
    
}
    //Helper method for right navigationbar button
    @objc func rightBarBackButton(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @objc func leftBarBackButton(){                //Helper method for right navigationbar button
        self.navigationController?.popViewController(animated: true)
    }
}
