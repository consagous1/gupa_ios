//
//  HotelRatingVC.swift
//  Gupa Network
//
//  Created by mac on 6/2/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher

class HotelRatingVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
   
    @IBOutlet var tblHotelstLish: UITableView!
    @IBOutlet var leftBarBtn : UIBarButtonItem!
    @IBOutlet var rightBarBtn : UIBarButtonItem!
    @IBOutlet var HotelSearchBar : UISearchBar!
    
    var arrOfResponseKey: NSMutableArray = NSMutableArray()
    var loading : JGProgressHUD!
    var strGoodRating: NSInteger = 0
    var strBadRating: NSInteger = 0
    var strUglyRating: NSInteger = 0
    var searchActive : Bool = false
    var filtered : NSArray = NSArray()
    
    var btnSearchUser:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    
    
//-===============================
    //MARK: Life cycle methods
//-===============================
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        appDelegate.centerNav = self.navigationController
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.title = "Hotel Ratings"
        
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        
        showRightNavigationBarButtonHide()
        
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
        tblHotelstLish.dataSource = self
        tblHotelstLish.delegate = self
        HotelSearchBar.delegate = self

    }
    
    func showRightNavigationBarButtonHide()                               //RightNavigationBarButton hide unhide
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnSearchUser = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnSearchUser.setImage(imgChat, for: UIControlState())
        btnSearchUser.addTarget(self, action: #selector(HotelRatingVC.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnSearchUser)
        
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(HotelRatingVC.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        searchActive = false
        HotelSearchBar.text = nil
        HotelSearchBar.canResignFirstResponder
        self.getHotelList()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //=====================================
    //MARK: UISearchBar delegate methods
    //=====================================
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let resultPredicate = NSPredicate(format: "hotel_name contains[cd] %@", searchText)
        filtered = arrOfResponseKey.filtered(using: resultPredicate) as NSArray
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tblHotelstLish.reloadData()
    }
    
//========================================================
  //MARK: UITableview Datasource and delegate methods
//========================================================
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var row : Int = Int()
        if(searchActive) {
            row = filtered.count
            return filtered.count
        }

        if self.arrOfResponseKey.count != 0
        {
            if tableView == tblHotelstLish
            {
                row = self.arrOfResponseKey.count
                return row
            }
        }else
        {
            row = 0
        }
        return row
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // self.loading.dismiss()
        let cell : HotelRatingCell = tableView.dequeueReusableCell(withIdentifier: "HotelRatingCell", for: indexPath) as! HotelRatingCell
        
        let view : UIView = cell.viewWithTag(111)!
        view.layer.cornerRadius = 5
        var dic: NSDictionary = NSDictionary()
        
        if(searchActive){
            dic = filtered.object(at: indexPath.row) as! NSDictionary
        } else {
            dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        }
        if let str_hotel_name : String = dic["hotel_name"] as? String{
            cell.lblHotelName.text = str_hotel_name
        }
        if let str_hotel_address : String = dic["address"] as? String{
           cell.lblHotelAddress.text = str_hotel_address
        }
        if let str_hotel_city : String = dic["city"] as? String{
             cell.lblCity.text = str_hotel_city
        }
        
        if let strRating: String = dic["Good"] as? String{
            strGoodRating = NSInteger(strRating)!
        }
        else if let strRating: NSInteger = dic["Good"] as? NSInteger
        {
            strGoodRating = strRating
        }
        cell.lblGoodRating.text = String(strGoodRating)
        
        if let strRating: String = dic["Bad"] as? String
        {
            strBadRating = NSInteger(strRating)!
        }
        else if let strRating: NSInteger = dic["Bad"] as? NSInteger
        {
            strBadRating = strRating
        }
        cell.lblBadRating.text = String(strBadRating)
        
        if let strRating: String = dic["Ugly"] as? String
        {
            strUglyRating = NSInteger(strRating)!
        }
        else if let strRating: NSInteger = dic["Ugly"] as? NSInteger
        {
            strUglyRating = strRating
        }
        cell.lblUglyRating.text = String(strUglyRating)
        
        if let str_hotel_pic : String = dic["hotel_pic"] as? String {
            let url = URL(string: str_hotel_pic)
            if url == nil{
                cell.imgHotel.image = UIImage(named: "Plane.png")
            }else{
                
                cell.imgHotel.kf.setImage(with: url!, placeholder: UIImage(named: "Plane.png"), options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, imageURL) in
                    print(image?.description ?? "")
                })
                
//               cell.imgHotel.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "Plane.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
            }
        }
      return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var dic: NSDictionary = NSDictionary()
        let barRatingVC = self.storyboard?.instantiateViewController(withIdentifier: "HotelRatingBarController") as! HotelRatingBarController
        if(searchActive){
            if filtered.count > 0{
                dic = filtered.object(at: indexPath.row) as! NSDictionary
            }
        } else {
            dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
                    }
        barRatingVC.flightHotelDetailDict = dic

        barRatingVC.strCheck = "comeFromHotel"        
        self.navigationController?.pushViewController(barRatingVC, animated: true)
    }
    
    
    func getHotelList() {
        
        AppTheme().callGetService(String(format: "%@HotelRating", AppUrl), param: nil) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                print(respDict)
                self.searchActive = false
                    if respDict.count > 0{
                        if let array_response : NSArray = respDict["response"] as? NSArray{
                            self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                        }else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                            self.arrOfResponseKey = array_Response_mutable
                        }
                       self.tblHotelstLish.reloadData()
                    }
            }
            else {
                self.loading.dismiss()
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: (self.view))
            }
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        HotelSearchBar.resignFirstResponder()
    }
}
