//
//  FlightBarRatingVC.swift
//  Gupa Network
//
//  Created by mac on 6/2/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class FlightBarRatingVC: UIViewController,SimpleBarChartDataSource, SimpleBarChartDelegate
{

        var viewCont: SimpleBarChart!
        var values : NSArray! // = ()
        var barColors: NSArray = NSArray()
        var currentBarColors : NSInteger = NSInteger()
        
        
    //-===============================
    //MARK: Life cycle methods
    //-===============================
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        
        self.title = "Flight Rating"
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 66.0/255.0, green: 199.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        
        let btnLeftBar: UIButton = UIButton ()                     // Create bar button here
        btnLeftBar.setImage(UIImage(named: "ic_back_arrows.png"), forState: .Normal)
        btnLeftBar.frame = CGRectMake(0, 0, 30, 30)
        btnLeftBar.addTarget(self, action: Selector("leftBarBackButton"), forControlEvents: .TouchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()        //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        
        let btnRighBar: UIButton = UIButton ()
        btnRighBar.setImage(UIImage(named: "Home-icon.png"), forState: .Normal)
        btnRighBar.frame = CGRectMake(0, 0, 30, 30)
        btnRighBar.addTarget(self, action: Selector("rightBarBackButton"), forControlEvents: .TouchUpInside)
        let rightBarButton:UIBarButtonItem = UIBarButtonItem()       //.....Set right button
        rightBarButton.customView = btnRighBar
        
        self.navigationItem.rightBarButtonItem = rightBarButton
    
    
            viewCont = SimpleBarChart()                        // Create SimpleBarChart here
            values = [2,  4, 8]
            barColors = [UIColor.blueColor(), UIColor.redColor()]
            currentBarColors = 1
            let frameBar: CGRect = CGRectMake(20, 100, self.view.frame.size.width-50, self.view.frame.size.height-100)
            viewCont = SimpleBarChart.init(frame: frameBar)
            viewCont.backgroundColor = UIColor.clearColor()
            viewCont.animationDuration = 2.0
           // viewCont.center = CGPointMake(self.view.frame.size.width/(2.0), self.view.frame.size.height/(2.0))
            viewCont.xLabelType = SimpleBarChartXLabelTypeHorizontal
           // viewCont.yLabelType = SimpleBarChartYAxisLabelTypeInteger
            viewCont.barWidth = 28.0
            viewCont.incrementValue = 2.0
            viewCont.barTextColor = UIColor.whiteColor()
            viewCont.gridColor = UIColor.blueColor()
            
            self.view.addSubview(viewCont)
            viewCont.delegate = self
            viewCont.dataSource = self
            viewCont.reloadData()
            // Do any additional setup after loading the view, typically from a nib.
        }
    
    //Helper method for right navigationbar button
    func rightBarBackButton(){
        let homeVC = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    func leftBarBackButton(){                //Helper method for right navigationbar button
        self.navigationController?.popViewControllerAnimated(true)
    }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
 //===============================
    //MARK: Bar delegate methods
 //===============================
        
        func numberOfBarsInBarChart(barChart: SimpleBarChart!) -> UInt {
            let row: Int = self.values.count as Int
            return  UInt (row)
        }
        func barChart(barChart: SimpleBarChart!, valueForBarAtIndex index: UInt) -> CGFloat {
            // let indexValue: String = (self.values.objectAtIndex(Int (index)) as? String)!
            // let indexValue : Int = self.values[Int (index)]
            // let indexFloat = CGFloat (indexValue) as
            return CGFloat( self.values.objectAtIndex(Int(index)) as! NSNumber)
        }
        func barChart(barChart: SimpleBarChart!, textForBarAtIndex index: UInt) -> String! {
            let str: NSNumber = self.values.objectAtIndex(Int (index)) as! NSNumber
            // let str: String = self.values[Int(index)] as! String
            return String(str)
        }
        func barChart(barChart: SimpleBarChart!, xLabelForBarAtIndex index: UInt) -> String! {
            return String(self.values.objectAtIndex(Int(index)) as! NSNumber)
        }

}
