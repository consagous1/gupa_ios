//
//  FlightRatingVC.swift
//  Gupa Network
//
//  Created by mac on 6/2/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher
import CoreData

class FlightRatingVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet var tblFlightLish : UITableView!
    @IBOutlet var leftBarBtn : UIBarButtonItem!
    @IBOutlet var rightBarBtn : UIBarButtonItem!
    @IBOutlet var flightSearchBar : UISearchBar!
    
    var arrOfResponseKey: NSMutableArray = NSMutableArray()
    var loading : JGProgressHUD!
    var strGoodRating: NSInteger = 0
    var strBadRating: NSInteger = 0
    var strUglyRating: NSInteger = 0
    var searchActive : Bool = false
    var filtered : NSArray = NSArray()
    
    var appDelegate: AppDelegate = AppDelegate()
    var flightList : NSArray!
    var totalFlight : NSMutableArray = NSMutableArray()
    var totalAirline : NSMutableArray = NSMutableArray()
    var totalFlightId : NSMutableArray = NSMutableArray()
    
    var checkTableCell : NSInteger = 0
    
    var flightSearch : String!
    var reversed: NSArray = NSArray()
    var arrOfDestinatioStation: NSMutableArray = NSMutableArray()
    var arrOfDestinatioStationCode: NSMutableArray = NSMutableArray()

    var btnSearchUser:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    
//-===============================
    //MARK: Life cycle methods
//-===============================
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
//        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        self.title = "Flight Ratings"
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        showRightNavigationBarButtonHide()
        tblFlightLish.dataSource = self
        tblFlightLish.delegate = self
        self.tblFlightLish.estimatedRowHeight = 50
        self.tblFlightLish.rowHeight = UITableViewAutomaticDimension
        self.tblFlightLish.setNeedsLayout()
        self.tblFlightLish.layoutIfNeeded()
        self.tblFlightLish.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        flightSearchBar.delegate = self
        
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
        checkTableCell = 0
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        flightSearchBar.resignFirstResponder()
    }
    
    func showRightNavigationBarButtonHide()                               //RightNavigationBarButton hide unhide
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnSearchUser = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnSearchUser.setImage(imgChat, for: UIControlState())
        btnSearchUser.addTarget(self, action: #selector(FlightRatingVC.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnSearchUser)
        
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(FlightRatingVC.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    
    @objc  func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
   @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        searchActive = false
        flightSearchBar.text = nil
        flightSearchBar.resignFirstResponder()
        flightSearch = ""
        
        if let isFirstTime = AppTheme.getIsFirstTimeFlight() as? Bool {
            if isFirstTime == true{
                self.loading.dismiss()
                getFlightList(flightSearch)
                //let result =   getAirportDetail() as NSArray
            }else{
                AppTheme.setIsLogin(true)
                if let resDic = AppTheme.getFlightRatingData() as? NSDictionary {
                    if resDic.count > 0{
                        if let array_response : NSArray = resDic["response"] as? NSArray{
                            self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                        }else if let array_Response_mutable : NSMutableArray = resDic["response"] as? NSMutableArray{
                            self.arrOfResponseKey = array_Response_mutable
                        }
                        self.tblFlightLish.reloadData()
                    }
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //=====================================
      //MARK: UISearchBar delegate methods
    //=====================================
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchActive = true;
        checkTableCell = 0
        searchActive = false;
        ///flightList = getFlights()
//        tblFlightLish.hidden = false
//        tblFlightLish.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        flightSearchBar.resignFirstResponder()
        searchActive = false;
        tblFlightLish.isHidden = true
        startActivity()
        getFlightList(flightSearch)
       
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
         searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        flightSearchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         let resultPredicate = NSPredicate(format: "SELF CONTAINS[cd] %@", searchText)
         filtered = totalFlight.filtered(using: resultPredicate) as NSArray
        if(filtered.count == 0){
//            stopActivity()
            searchActive = false;
            checkTableCell = 1
            flightSearch = ""
            getFlightList(flightSearchBar.text!)
            self.tblFlightLish.reloadData()
        } else if (filtered.count != 0) {
             searchActive = true;
             checkTableCell = 0
            self.tblFlightLish.reloadData()
        }else{
            searchActive = true;
            checkTableCell = 0
            getFlightList(flightSearchBar.text!)
        }
    }
    
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
            if checkTableCell == 1{
                if self.arrOfResponseKey.count != 0{
                    row = arrOfResponseKey.count
                    return row
                }
                return 0
            }else{
                if filtered.count != 0{
                    return  filtered.count
                } else if totalFlight.count != 0{
                    return totalFlight.count
                }
                return 0
            }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  checkTableCell == 1 {
            
            var cell : FlightRatingCell! = tableView.dequeueReusableCell(withIdentifier: "FlightRatingCell", for: indexPath) as! FlightRatingCell
            if cell == nil {
                cell = FlightRatingCell(style: UITableViewCellStyle.default, reuseIdentifier: "FlightRatingCell")
            }
            let view : UIView = cell.viewWithTag(111)!
            view.layer.cornerRadius = 5
            var dic: NSDictionary = NSDictionary()
            
            if(searchActive){
                dic = filtered.object(at: indexPath.row) as! NSDictionary
            } else {
                dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
            }
            if let str_airline_name : String = dic["airline_name"] as? String{
                cell.lblAirlinesName.text = str_airline_name
            }
            if let str_destination : String = dic["to"] as? String{
                cell.lblDestStation.text = str_destination
            }
            if let str_source : String = dic["from"] as? String{
                cell.lblSourceStation.text = str_source
            }
            if let str_flight_id : String = dic["flight_name"] as? String{
                cell.lblFlighNumber.text = str_flight_id
            }
            if let strRating: String = dic["Good"] as? String{
                strGoodRating = NSInteger(strRating)!
            }else if let strRating: NSInteger = dic["Good"] as? NSInteger {
                strGoodRating = strRating
            }
            cell.lblGoodRating.text = String(strGoodRating)
            
            if let strRating: String = dic["Bad"] as? String{
                strBadRating = NSInteger(strRating)!
            }else if let strRating: NSInteger = dic["Bad"] as? NSInteger{
                strBadRating = strRating
            }
            cell.lblBadRating.text = String(strBadRating)
            
            if let strRating: String = dic["Ugly"] as? String {
                strUglyRating = NSInteger(strRating)!
            }else if let strRating: NSInteger = dic["Ugly"] as? NSInteger {
                strUglyRating = strRating
            }
            cell.lblUglyRating.text = String(strUglyRating)
            
            if let str_profile_pic : String = dic["airline_pic"] as? String {
                let url = URL(string: str_profile_pic)
                if url == nil{
                    cell.imgTitle.image = UIImage(named: "Plane.png")
                }else{
                    
                    cell.imgTitle.kf.setImage(with: url!, placeholder: UIImage(named: "Plane.png"), options: nil, progressBlock: nil, completionHandler: {
                        (image, error, cacheType, imageURL) in
                        print(image?.description ?? "")
                    })
                    
//                    cell.imgTitle.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "Plane.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
                }
            }
            return cell
        }else if checkTableCell == 0
        {
            var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
            
            if cell == nil {
                cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
            }
            if(searchActive){
                if filtered.count > indexPath.row {
                    cell.titleLabel.text = (filtered.object(at: indexPath.row) as? String)!
                }
            } else {
                if totalFlight.count > indexPath.row {
                    cell.titleLabel.text = totalFlight.object(at: indexPath.row) as? String
                }
            }
            return cell
        }else
        {
            var cell = tableView .dequeueReusableCell(withIdentifier: "cell")
            if cell == nil {
                
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            }
            cell?.backgroundColor = UIColor.clear
            cell!.isUserInteractionEnabled = false
            return cell!
        }        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if checkTableCell == 1{
            var dic: NSDictionary = NSDictionary()
            if(searchActive){
                dic = filtered.object(at: indexPath.row) as! NSDictionary
            } else {
                dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
            }
           let barRatingVC = self.storyboard?.instantiateViewController(withIdentifier: "FlightTransportGraphController") as! FlightTransportGraphController
            barRatingVC.flightHotelDetailDict = dic
            if let str_fligh_id : String = dic["flight_id"] as? String{
                barRatingVC.strFlightId = str_fligh_id
            }
            barRatingVC.strCheck = ""
            self.navigationController?.pushViewController(barRatingVC, animated: true)
        }else{
            startActivity()
            tblFlightLish.isHidden = true
            if(searchActive){
                if filtered.count > indexPath.row {
                    flightSearchBar.text = filtered.object(at: indexPath.row) as? String
                    flightSearch = flightSearchBar.text
                    getFlightList(flightSearch)
                }
            } else {
                if totalFlight.count > indexPath.row {
                    flightSearchBar.text = totalFlight.object(at: indexPath.row) as? String
                    flightSearch = flightSearchBar.text
                    getFlightList(flightSearch)
                    //tblFlightLish.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if checkTableCell == 1 {
            return 120
        } else{
              return 45
        }
    }
    
    
    func getFlightList(_ flightNumber : String)  {
        
        let separators = CharacterSet(charactersIn: " ")
        // Split based on characters.
        let parts = flightNumber.components(separatedBy: separators)
       
        // Print result array.
        print(parts)
        var stringFlightId : String = parts.last!
        let firstIndex : String = parts.first!
        // Service call here
        if parts.count == 1 {
            stringFlightId = ""
        }
        let parameter : [String : AnyObject]!
            parameter = ["search" : firstIndex as AnyObject,
                         "flight_id" : stringFlightId as AnyObject]
        
        AppTheme().callGetService(String(format: "%@FlightRating", AppUrl), param: parameter) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                self.tblFlightLish.isHidden = false
                self.checkTableCell = 1
                var respDict : NSDictionary = NSDictionary()
                var dictFlightId : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                AppTheme.setFlightRatingData(data)
                print(respDict)
                self.searchActive = false                
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }else if let array_Response_mutable : NSMutableArray = respDict["response"] as? NSMutableArray{
                    self.arrOfResponseKey = array_Response_mutable
                }
                self.totalFlightId.removeAllObjects()
                self.totalFlight.removeAllObjects()
                self.totalAirline.removeAllObjects()
                
                if self.arrOfResponseKey.count > 0 {
                    for element in self.arrOfResponseKey {
                       var str_airline_name : String! = String()
                        dictFlightId = element as! NSDictionary
                        if let str_airlineName = dictFlightId .value(forKey: "airline_name") as? String{
                            str_airline_name = str_airlineName
                            self.totalFlightId.add(str_airlineName)
                        }
                        if let str_flight_id : String = dictFlightId .value(forKey: "flight_id") as? String{
                            
                            let flightId = "\(str_airline_name)  \(str_flight_id)"
                            self.totalFlight.add(flightId as String)
                            self.totalAirline.add(str_flight_id)
                        }
                    }
                    self.tblFlightLish.reloadData()
                }
            }
            else
            {
                self.loading.dismiss()
                self.arrOfResponseKey.removeAllObjects()
                self.tblFlightLish.reloadData()
               // self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry there is no data found.", showInView: (self.view))
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        flightSearchBar.resignFirstResponder()
    }
    func startActivity()  {
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
    }
  }
