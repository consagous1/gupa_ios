//
//  AirportActivityViewController.swift
//  Gupa Network
//
//  Created by MWM23 on 9/16/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import CoreData
import IQKeyboardManagerSwift

class AirportActivityViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{

    @IBOutlet weak var collectionItems: UICollectionView!
    @IBOutlet weak var airportNameLabel: UILabel!
    @IBOutlet weak var airportCityNameLabel: UILabel!
    @IBOutlet weak var goodRatingLabel: UILabel!
    @IBOutlet weak var badRatingLabel: UILabel!
    @IBOutlet weak var ugliRatingLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeZoneLabel: UILabel!
    @IBOutlet weak var dropdownTable: UITableView!
    @IBOutlet weak var searchBar : UISearchBar!
    @IBOutlet weak var dropdownContainer : UIView!
    
    @IBOutlet weak var btnGraph: UIButton!
    
    var loading : JGProgressHUD!
    var arrOfResponseKey: NSMutableArray = NSMutableArray()
    var respDict: NSDictionary = NSDictionary()
    var arrForCollectionView: NSMutableArray = NSMutableArray()
    var dictForServicesDetail : NSMutableDictionary = NSMutableDictionary()
    var iataCode: String!
    var airportList : NSArray!
    var searchActive : Bool = false
    var filtered : NSArray = NSArray()
    var arrForCollImageCell : NSMutableArray = NSMutableArray()
    var reversed: NSArray = NSArray()
    var arrOfDestinatioStation: NSMutableArray = NSMutableArray()
    var arrOfDestinatioStationCode: NSMutableArray = NSMutableArray()
    var appDelegate: AppDelegate = AppDelegate()
    var btnChat:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    var isGPSOff : Bool = false
    
    
    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController

        self.title = "Airport"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 66.0/255.0, green: 199.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        
        showRightNavigationBarButtonHide()
        loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        loading.textLabel.text = "Loading"
        loading.show(in: self.view)

        appDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        dropdownContainer.isHidden = true
        
        updateDetail()
    }
    
    func updateDetail() {
        //Array For Login signup user
        self.arrForCollectionView = NSMutableArray()
        dictForServicesDetail = ["ServiceName": "Terminal", "ServiceImage": "terminal.png", "ServiceCode" : "1"]
        self.arrForCollectionView.add(dictForServicesDetail)
        dictForServicesDetail = ["ServiceName": "Access/Info", "ServiceImage": "building.png", "ServiceCode" : "2"]
        self.arrForCollectionView.add(dictForServicesDetail)
        dictForServicesDetail = ["ServiceName": "Parking", "ServiceImage": "parking.png", "ServiceCode" : "3"]
        self.arrForCollectionView.add(dictForServicesDetail)
        dictForServicesDetail = ["ServiceName": "Check in", "ServiceImage": "checkin.png", "ServiceCode" : "4"]
        self.arrForCollectionView.add(dictForServicesDetail)
        dictForServicesDetail = ["ServiceName": "Baggage Claim/ Handling", "ServiceImage": "experience.png", "ServiceCode" : "5"]
        self.arrForCollectionView.add(dictForServicesDetail)
        dictForServicesDetail = ["ServiceName": "Security", "ServiceImage": "scanner.png", "ServiceCode" : "6"]
        self.arrForCollectionView.add(dictForServicesDetail)
        dictForServicesDetail = ["ServiceName": "Lounges", "ServiceImage": "lounge.png", "ServiceCode" : "7"]
        self.arrForCollectionView.add(dictForServicesDetail)
        dictForServicesDetail = ["ServiceName": "Restaurants", "ServiceImage": "restaurent.png", "ServiceCode" : "8"]
        self.arrForCollectionView.add(dictForServicesDetail)
        dictForServicesDetail = ["ServiceName": "Restrooms", "ServiceImage": "restroom.png", "ServiceCode" : "9"]
        self.arrForCollectionView.add(dictForServicesDetail)
        dictForServicesDetail = ["ServiceName": "Shopping", "ServiceImage": "terminal.png", "ServiceCode" : "10"]
        self.arrForCollectionView.add(dictForServicesDetail)
        dictForServicesDetail = ["ServiceName": "Others", "ServiceImage": "others.png", "ServiceCode" : "11"]
        self.arrForCollectionView.add(dictForServicesDetail)
        
        if iataCode == nil{
            if isGPSOn() == false{
                 isGPSOff = true
                self.loading.dismiss()
                return
            }
        }
        if let str_aita_code : NSString =  AppTheme.getAirportCode() {
            iataCode = str_aita_code as String
             getAirportDetail(iataCode)
        }
        
        // self.startLoader("Loading", showInView: self.view)
        
    }
    
    func showRightNavigationBarButtonHide()                               //RightNavigationBarButton hide unhide
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnChat = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnChat.setImage(imgChat, for: UIControlState())
        btnChat.addTarget(self, action: #selector(AirportActivityViewController.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnChat)
        
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(AirportActivityViewController.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
     @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getAirportDetail(_ iata: String) {
        
        if (!isNetworkAvailable) {
            return
        }
        
        let parameters : [String: AnyObject] = ["iata_code" : iataCode as AnyObject]
        
        AppTheme().callGetService(String(format: "%@get_airport_detail", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }else if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray{
                    self.arrOfResponseKey = array_Response_mutable
                }
                if self.arrOfResponseKey.count > 0 {
                    for airlines in self.arrOfResponseKey{
                        self.respDict = airlines as! NSDictionary
                    }
                    if let str_name : String = self.respDict .value(forKey: "name") as? String{
                        self.airportNameLabel.text = "\(str_name) Airport"
                    }
                    if let str_city : String = self.respDict .value(forKey: "city") as? String{
                        var str_city_value : String! = String()
                        var str_countary_value : String! = String()
                        str_city_value = str_city
                        if let str_countary_code : String = self.respDict .value(forKey: "country_code") as? String{
                            str_countary_value = str_countary_code
                             self.airportCityNameLabel.text = " \(str_city_value!) \(str_countary_value!)"
                        }
                    }
                    if let str_rating_good : String = self.respDict .value(forKey: "good") as? String{
                        self.goodRatingLabel.text = str_rating_good
                    }
                    if let str_rating_bad : String = self.respDict .value(forKey: "bad") as? String{
                        self.badRatingLabel.text = str_rating_bad
                    }
                    if let str_rating_ugly : String = self.respDict .value(forKey: "ugly") as? String{
                        self.ugliRatingLabel.text = str_rating_ugly
                    }
                    if let str_current_time : String = self.respDict .value(forKey: "current_time") as? String{
                        let strSplitTime = str_current_time.characters.split(separator: "T")
                        self.timeLabel.text =  String(strSplitTime.last!)
                    }
                    if let str_timezone : String = self.respDict .value(forKey: "timezone") as? String{
                        self.timeZoneLabel.text = str_timezone
                    }
                }
            }
            else{
                self.loading.dismiss()
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:  "Sorry there is no Air Services between entered station", showInView: self.view)
            }
        }
    }
    
    
//MARK:- Collectionview delegate and data source methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrForCollectionView.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AirportCollCell! = collectionView.dequeueReusableCell(withReuseIdentifier: "AirportCollCell", for: indexPath) as! AirportCollCell
        if cell == nil{
            //cell = AirportCollCell(.uico)
        }
        let dicTest: NSDictionary! = arrForCollectionView[indexPath.row] as! NSDictionary
        
        cell.imgCollectionCell.image = UIImage(named:(dicTest["ServiceImage"] as! String))
        cell.titleLabel.text = String( dicTest["ServiceName"] as! String)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if iataCode == nil{
            if isGPSOn() == false{
                self.loading.dismiss()
                return
            }
        }
        if iataCode != "" {
            let airportReviewVC : AirportServiceReviewsController = self.storyboard?.instantiateViewController(withIdentifier: "AirportServiceReviewsController") as! AirportServiceReviewsController
            if searchActive {
                if let str_iata = self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.searchBar.text!)) as? String {
                    iataCode = str_iata
                    airportReviewVC.iataCode = iataCode
                }
            }else{
                airportReviewVC.iataCode = iataCode
            }
            
            if let dic = arrForCollectionView[indexPath.row] as? NSDictionary {
                airportReviewVC.serviceDataDic = dic
            }
            if self.arrForCollectionView.count != 0 {
                airportReviewVC.arrForCollectionView = self.arrForCollectionView
            }
            self.navigationController?.pushViewController(airportReviewVC, animated: true)
        }
    }
    
    
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
            if arrOfDestinatioStation.count != 0 {
                return arrOfDestinatioStation.count
            }
            return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
            var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
            
            if cell == nil {
                cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
            }
        if(searchActive){
            if filtered.count > indexPath.row {
                cell.titleLabel.text = filtered.object(at: indexPath.row) as? String
            }
        } else {
            if arrOfDestinatioStation.count > indexPath.row {
                cell.titleLabel.text = arrOfDestinatioStation.object(at: indexPath.row) as? String
            }
        }
          return cell
    }
    

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        
            if(searchActive){
                if filtered.count > indexPath.row {
                    searchBar.text = filtered.object(at: indexPath.row) as? String
                    iataCode = (self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.searchBar.text!)) as! String)
                    getAirportDetail(self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.searchBar.text!)) as! String)
                }
            } else {
                if airportList.count > indexPath.row {
                    searchBar.text = airportList.object(at: indexPath.row) as? String
                }
            }
            
        dropdownContainer.isHidden = true
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
//    func stopActivity() {
//        self.loading = JGProgressHUD(style: JGProgressHUDStyle.Dark)
//        self.loading.textLabel.text = "Loading"
//        self.loading.showInView(self.view)
//    }
    
    //=====================================
    //MARK: UISearchBar delegate methods
    //=====================================
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {    
        searchActive = false;
        airportList = getAirportCodeList()
        dropdownTable.isHidden = false
        dropdownTable.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
       // searchActive = false;
        dropdownTable.isHidden = true
        dropdownContainer.isHidden = true
        if isGPSOff == true {
            self.loading.dismiss()
        }else{
            if iataCode != "" {
                isGPSOff = false
                getAirportDetail(iataCode)
            }
        }
       
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let resultPredicate = NSPredicate(format: "SELF CONTAINS[c] %@",  searchText)
        if searchText == "" {
//            airportList.removeallobjects
        }
        filtered = arrOfDestinatioStation.filtered(using: resultPredicate) as NSArray
        if(filtered.count == 0){
            dropdownContainer.isHidden = true
            searchActive = false;
            dropdownTable.reloadData()
        } else {
           dropdownContainer.isHidden = false
            searchActive = true;
            dropdownTable.reloadData()
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // getAirportActivity()
        searchBar.resignFirstResponder()
    }

    func getAirportCodeList() -> NSArray {
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Table_travel", in: self.appDelegate.managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        var result : NSArray!
        
        do {
            result = try appDelegate.managedObjectContext.fetch(fetchRequest) as NSArray
            
            if (result.count > 0) {
                
                for i in 0 ..< result.count {
                    let airportDetail = result[i] as! NSManagedObject
                    let strCityNameCode = (airportDetail.value(forKey: "airport_city") as! String) + " " + (airportDetail.value(forKey: "airport_code") as! String)
                    self.arrOfDestinatioStation.add(strCityNameCode as String)
                    self.arrOfDestinatioStationCode.add(airportDetail.value(forKey: "airport_code") as! String)
                }
                
                //sorted array for destination stations
                self.reversed = (self.arrOfDestinatioStation as NSArray as! [String]).sorted(by: { (s1: String, s2: String) -> Bool in
                    return s1 < s2
                }) as NSArray
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return self.reversed
    }

    @IBAction func btnGraph(_ sender: AnyObject) {
        var destVC = FlightTransportGraphController()
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "FlightTransportGraphController") as! FlightTransportGraphController
        destVC.flightHotelDetailDict = self.respDict
        destVC.strCheck = "comeFromAirportService"
        self.navigationController?.pushViewController(destVC, animated: true)
        
    }
   

}
