//
//  IndivisualHotelDetailViewController.swift
//  Gupa Network
//
//  Created by MWM23 on 8/8/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher
import MapKit

class IndivisualHotelDetailViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var imageCollectionSlider: UICollectionView!
    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblHotelAddress: UILabel!
    @IBOutlet weak var lblShortDiscription: UILabel!
    @IBOutlet weak var btnMoreDiscription: UIButton!
    @IBOutlet weak var viewForAlert: UIView!
    @IBOutlet weak var btnCancelOnAlert: UIButton!
    @IBOutlet weak var txtLongDiscrip: UITextView!
    @IBOutlet weak var txtHotelAmenities: UITextView!
    @IBOutlet weak var img_hotel: UIImageView!
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var respDict : NSDictionary = NSDictionary()
    var dictTemp: NSDictionary = NSDictionary()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var arrOfHotelImage :  NSMutableArray = NSMutableArray()
    var arrOfHotelAmenities : NSMutableArray = NSMutableArray()
    var attrStr : NSAttributedString = NSAttributedString()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startProgressBar()
        self.getHotelDetail()
        
        appDelegate.centerNav = self.navigationController
        self.viewForAlert.isHidden = true
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 66.0/255.0, green: 199.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()                     // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(IndivisualHotelDetailViewController.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()        //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
    }
    
    @objc func leftBarBackButton(){
        //Helper method for right navigationbar button
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }

    func getHotelDetail()
    {
//        let parameters : [String: AnyObject] = ["hotel_id" : AppTheme.getHotelId() as AnyObject ]
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getHotelId(), forKey:"hotel_id")
        
        AppTheme().callPostService(String(format: "%@hotelDetails", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                self.respDict = data as! NSDictionary
                if let arrRes: NSMutableArray = self.respDict["response"] as? NSMutableArray{
                    self.arrOfResponseKey = arrRes
                }else if let arrRes: NSArray = self.respDict["response"] as? NSArray{
                    self.arrOfResponseKey = NSMutableArray(array: arrRes)
                }
                if self.arrOfResponseKey.count > 0{
                    
                    for hotelDetail in  self.arrOfResponseKey
                    {
                        self.dictTemp = hotelDetail as! NSDictionary
                    }
                    if let str_hotel_name : String = self.dictTemp["hotel_name"] as? String{
                       self.lblHotelName.text = str_hotel_name
                    }
                    if let str_hotel_addres : String = self.dictTemp["hotel_address"] as? String{
                        self.lblHotelAddress.text = str_hotel_addres
                    }
                    if let str_hotel_short_discription : String = self.dictTemp["short_Description"] as? String{
                        self.lblShortDiscription.text = str_hotel_short_discription
                    }
                    if let str_hotel_long_discription : String = self.dictTemp["long_Description"] as? String{
                        self.attrStr = try! NSAttributedString(
                            data: (str_hotel_long_discription).data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                            documentAttributes: nil)
                        
                        
                        //[NSAttributedString.DocumentAttributeKey.documentType: NSAttributedString.DocumentType.html]
                    }
                    
                    //...Hotel image array handle
                    if let str_photos : String = self.dictTemp["photos"] as? String{
                        let url = URL(string: str_photos)
                        
                        self.img_hotel.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                            (image, error, cacheType, imageURL) in
                            print(image?.description ?? "")
                        })
                        
//                        self.img_hotel.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
                    }
                    
                    //...Manage hotel-amenities
                    if let array_hotel_amenities : NSArray = self.dictTemp["hotel_Amenities"] as? NSArray{
                        var tempDicAmenities : NSDictionary = NSDictionary()
                        for hotelAmenities in array_hotel_amenities
                        {
                            tempDicAmenities = hotelAmenities as! NSMutableDictionary
                            if let str_description : String = tempDicAmenities["description"] as? String{
                                self.arrOfHotelAmenities.add(str_description)
                            }
                        }
                    self.txtHotelAmenities.text = self.arrOfHotelAmenities.componentsJoined(by: "\n. ")
                    }
                    //...Manage lat long
                    
//                    let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: Double((self.dictTemp["latitude"] as! NSString).doubleValue), longitude: Double((self.dictTemp["longitude"] as! NSString).doubleValue))
//                    let annotation = MKPointAnnotation()
//                    annotation.coordinate = location
//                    if let str_hotel_name : String = self.dictTemp["hotel_name"] as? String{
//                         annotation.title = str_hotel_name
//                    }
//                    if let str_hotel_city : String = self.dictTemp["hotel_city"] as? String{
//                       annotation.subtitle = str_hotel_city
//                    }
//                    self.mapView.addAnnotation(annotation)
                    
//                    let span = MKCoordinateSpanMake(0.05, 0.05)
//                    let region = MKCoordinateRegion(center: location, span: span)
//                    self.mapView.setRegion(region, animated: true)
                }
               // self.imageCollectionSlider.reloadData()
            }
            else
            {
                
            }
        }
    }

    
   //================================================
    //MARK: Collectionview datasource and delegate
   //================================================
/*    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
       return self.arrOfHotelImage.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("HotelImageCell", forIndexPath: indexPath) as! HotelImageCell
        let url = NSURL(string:self.arrOfHotelImage.objectAtIndex(indexPath.row) as! String)
        cell.imgHotel.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
        return cell
    }
    */
    
    @IBAction func btnCancelOnAlert(_ sender: AnyObject)
    {
        self.viewForAlert.isHidden = true
    }
   
    @IBAction func btnMore(_ sender: AnyObject)
    {
        self.viewForAlert.isHidden = false
        self.btnCancelOnAlert.isHidden = false
        self.txtLongDiscrip.isHidden = false
        self.txtLongDiscrip.attributedText = attrStr
    }
}
