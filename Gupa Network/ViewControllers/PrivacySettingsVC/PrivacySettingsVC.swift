//
//  PrivacySettingsVC.swift
//  Gupa Network
//
//  Created by Apple on 20/12/17.
//  Copyright © 2017 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class PrivacySettingsVC: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet var switchOnChat          : UISwitch!
    @IBOutlet var switchOFrndRequest    : UISwitch!
    @IBOutlet var switchOnSearch        : UISwitch!

    // MARK: - Initialization
    var loading : JGProgressHUD! = JGProgressHUD()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var respDict : NSDictionary = NSDictionary()
    var dicData : NSDictionary = NSDictionary()

    var btnSearchUser:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    
    var strChat  = ""
    var strSearch  = ""
    var strGuide  = ""

    var strValue = ""
    var strType = ""

    // MARK: - Lifecycle

        override func viewDidLoad() {
        super.viewDidLoad()
            
            appDelegate.centerNav = self.navigationController

        //**>> Manage Navigation Bar
        self.title = "Privacy Settings"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
            
        showRightNavigationBarButtonHide()

        //**>> API Call for Privacy Settings
            getPrivacySettings_ApiCall()
        }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
    // MARK: - Navigation Bar Button
    
    func showRightNavigationBarButtonHide()
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnSearchUser = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnSearchUser.setImage(imgChat, for: UIControlState())
        btnSearchUser.addTarget(self, action: #selector(NotificationListController.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnSearchUser)
        
         // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(NotificationListController.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    
    @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    

    @IBAction func switchOnoffAction1(settingSwitch: UISwitch) {
        
        if settingSwitch.isOn == true {
            strValue  = "1"
        }
        if settingSwitch.isOn == false {
            strValue  = "2"
        }
        strType = "1"

        changePrivacySettings_ApiCall(strValue, strType)
    }

    @IBAction func switchOnoffAction2(settingSwitch: UISwitch) {
        
        if settingSwitch.isOn == true {
            strValue  = "1"
        }
        if settingSwitch.isOn == false {
            strValue = "2"
        }
        strType = "2"

        changePrivacySettings_ApiCall(strValue, strType)
    }

    
    @IBAction func switchOnoffAction3(settingSwitch: UISwitch) {
        
        if settingSwitch.isOn == true {
            strValue  = "1"
        }
        if settingSwitch.isOn == false {
            strValue  = "2"
        }
        strType = "3"

        changePrivacySettings_ApiCall(strValue, strType)
    }
    
    func getPrivacySettings_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }

        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"ids")

        AppTheme().callPostService(String(format: "%@getprivacydata", AppUrlNew), param: paraDict) {(result, data) -> Void in                                                                                  ///Call services

            if result == "success"
            {
                self.loading.dismiss()
                self.respDict = data as! NSDictionary
                
                if self.respDict != nil {
                    
                    self.arrOfResponseKey = (self.respDict["response"] as? NSMutableArray)!
                    
                    if self.arrOfResponseKey.count > 0  {
                        for element in self.arrOfResponseKey {
                        self.dicData = element as! NSDictionary
                        
                            if let str_PrivacyChat =  self.dicData["privacy_chat"] as? String {
                                
                                self.strChat = str_PrivacyChat
                                
                                if self.strChat == "1" {
                                    self.switchOnChat.isOn = true
                                } else {
                                    self.switchOnChat.isOn = false
                                }
                                print(str_PrivacyChat)
                            }
                            if let str_Search =  self.dicData["privacy_search"] as? String {
                                
                                self.strSearch = str_Search
                                
                               if self.strSearch == "1" {
                                    self.switchOnSearch.isOn = true
                                } else {
                                    self.switchOnSearch.isOn = false
                                }
                                print(str_Search)
                            }
                            if let str_Request =  self.dicData["privacy_friend_request"] as? String {
                                
                                self.strGuide = str_Request
                                
                                if self.strGuide == "1" {
                                    self.switchOFrndRequest.isOn = true
                                } else {
                                    self.switchOFrndRequest.isOn = false
                                }
                                print(str_Request)
                            }
                               print(self.dicData)
                        }
                    }
                }
  
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
    
    func changePrivacySettings_ApiCall(_ strValue : String , _ strType : String) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
      
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"ids")
        paraDict.setValue(strValue, forKey:"val")
        paraDict.setValue(strType, forKey:"type")

        AppTheme().callPostService(String(format: "%@privacysettingpost", AppUrlNew), param: paraDict) {(result, data) -> Void in                                                                                  ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
               // self.getPrivacySettings_ApiCall()
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
}


