//
//  RegisterationVC.swift
//  Gupa Network
//
//  Created by mac on 5/2/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import IQDropDownTextField
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



class RegisterationVC: UIViewController , UIGestureRecognizerDelegate, UINavigationControllerDelegate ,UIImagePickerControllerDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var viewCityContainer: UIView!
    @IBOutlet weak var viewForCity: UIView!
    
    @IBOutlet weak var tbl_city_list: UITableView!
    @IBOutlet weak var tbl_city: UITableView!
    
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var date_picker: UIDatePicker!
    
    @IBOutlet var txtFirstNameLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var txtLastNameLeadingContraint: NSLayoutConstraint!
    @IBOutlet var txtEmailHeightLayout: NSLayoutConstraint!
    @IBOutlet var txtEmailVerticalContraint: NSLayoutConstraint!
    
    var loading : JGProgressHUD = JGProgressHUD(style: JGProgressHUDStyle.dark)
    @IBOutlet weak var viewGender: UIView!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    
    @IBOutlet var imgProfile : UIImageView!
    @IBOutlet var txtFirstName : UITextField!
    @IBOutlet var txtLastName : UITextField!
    @IBOutlet var txtUserName : UITextField!
    @IBOutlet var txtEmail : UITextField!
    @IBOutlet var txtPassword : UITextField!
    @IBOutlet var txtRePass : UITextField!
    @IBOutlet var txtBirthday : UITextField! = UITextField()
    var date : Date = Date()
    @IBOutlet var txtCity : UITextField! = UITextField()
    @IBOutlet var txtGender : UITextField! = UITextField()
    // @IBOutlet var txtGender :IQDropDownTextField!
    @IBOutlet var txtInterest :UITextField! = UITextField()
    @IBOutlet var leftBarBtn : UIBarButtonItem? = UIBarButtonItem()
    @IBOutlet var vwContainer : UIView!
    @IBOutlet weak var checkUncheckButton: UIButton!
    
    var arrOfInterest: NSMutableArray = NSMutableArray()
    var arrOfGender: NSMutableArray = NSMutableArray()
    var arr_cityList: NSMutableArray = NSMutableArray()
    var str_cityId : String! 
    
    
    var strCheckForEditVC: String = String()
    var appDelegate: AppDelegate = AppDelegate()
    
    var imagePicker = UIImagePickerController()
    var stateId : NSMutableArray = NSMutableArray()
    var countryId : NSMutableArray = NSMutableArray()
    var cityId : NSMutableArray = NSMutableArray()
    var stateName : NSMutableArray = NSMutableArray()
    var countryName : NSMutableArray = NSMutableArray()
    var cityName : NSMutableArray = NSMutableArray()
    var cityIdSelected : String = String()
    var imageData : Data = Data()
    var checkValue : String!
    var str_id_city : String! = String()
    
    static var isCheck : Bool = false
    
    //MARK: ---RV---
    @IBOutlet var viewTblPickerBottomLayout: NSLayoutConstraint!
    @IBOutlet weak var btnSelectCity: UIButton!
    @IBOutlet weak var btnCancelSelection: UIButton!
    var isCitySearching : Bool = false
    var strTableType : String = ""
    @IBOutlet weak var lblSelectListTitle: UILabel!
    //------
    
    //==================================
    // MARK: - VC life cycle method
    //==================================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        print(appDelegate.strURL)
        

        txtFirstName.layer.borderColor = UIColor.lightGray.cgColor
        txtFirstName.layer.borderWidth = 1.0;
        txtFirstName.layer.cornerRadius = 2
        txtLastName.layer.borderColor = UIColor.lightGray.cgColor
        txtLastName.layer.borderWidth = 1.0;
        txtLastName.layer.cornerRadius = 2
        
        imgProfile.layer.cornerRadius = 35 //imgProfile.frame.height/2
        imgProfile.layer.masksToBounds = true
        imgProfile.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action:#selector(RegisterationVC.handleTapOnProfileImg(_:)))
        tap.delegate = self
        imgProfile.addGestureRecognizer(tap)
    
        txtCity.delegate = self
       
        arrOfGender = ["Female", "Male", "Other"]
        self.arrOfInterest =  ["Animals", "Arts & Humanities", "Automotive", "Fitness", "Business", "Computers & Electronics", "Entertainment", "Food & Drink", "Games",  "Home & Garden", "Industries", "Internet", "Lifestyles", "News", "Photo & Video", "Science", "Shopping", "Social Networks", "Society", "Sports", "Telecommunications", "Travel"]
       
        self.countryName = NSMutableArray()
        self.countryId = NSMutableArray()
        
        //---RV---
        viewTblPickerBottomLayout.constant = -217
        viewCityContainer.isHidden = true
        viewForCity.isHidden = true

    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        viewCityContainer.isHidden = true
    }
    @IBAction func backButton(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //============================================
    // MARK: - method for image action and send
    //============================================
    
    @objc func handleTapOnProfileImg(_ sender: UITapGestureRecognizer) {
        
        // handling code
    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion:
                { () -> Void in
            })
        }
        print("Profile iamge selected")
    }
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
    {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        imgProfile.image = image
        imageData = UIImageJPEGRepresentation(image, 0.5)!
    }
    
    
    //==================================
    // MARK: - textField Delegates
    //==================================
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtBirthday{
            self.viewDatePicker.isHidden = false
            let datePickerView:UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.date
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(RegisterationVC.datePickerValueChanged), for: UIControlEvents.valueChanged)
            
        } else if textField == txtCity{
            hideSelectionView()
            
        } else if textField == txtGender{
            
        } else if textField == txtInterest{
            
        }
        
    }
    
    @objc func datePickerValueChanged(_ sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        date = sender.date
        txtBirthday.text = dateFormatter.string(from: sender.date)
        self.viewDatePicker.isHidden = true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtCity {
            if(txtCity.text?.characters.count > 0) {
                hideSelectionView()
                viewForCity.isHidden = true
            }
        }
        if textField == txtBirthday{
            self.viewDatePicker.isHidden = true
        }
    }
    
    //-- RV ---
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtCity{
            if(txtCity.text?.characters.count > 0 || string.characters.count > 0)
            {
                if !isCitySearching
                {
                    isCitySearching = true
                    strTableType = "City"
                    lblSelectListTitle.text = "Select City"
                    tbl_city_list.isHidden = true
                    arr_cityList.removeAllObjects()
                    tbl_city.reloadData()
                    showSelectionView()
                }
                var strSearch : String = ""
                if(txtCity.text?.characters.count > 0)
                {
                    strSearch =  txtCity.text! + string // "\(txtCity.text)\(string)"
                } else{
                    strSearch = string
                }
                callSearchCity(strSearch)
            }
        }
        return true
    }
   
    
    //Search city with autocomplete
    func callSearchCity(_ searchText: String)  {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let parameters : [String: AnyObject] = ["search" : searchText as AnyObject ]
        
        AppTheme().callGetService(String(format: "%@city_search", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.viewForCity.isHidden = false
                self.viewCityContainer.isHidden = true
                self.tbl_city_list.isUserInteractionEnabled = false
                self.loading.dismiss()
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if let array_temp : NSArray = respDict["response"] as? NSArray{
                    self.arr_cityList = array_temp.mutableCopy() as! NSMutableArray
                }
                self.view.bringSubview(toFront: self.tbl_city)
                //-- RV --
//                let tap_keyboard : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterationVC.dismissKeyboard))
//                self.txtCity.addGestureRecognizer(tap_keyboard)
                self.strTableType = "City"
                self.tbl_city.reloadData()
                
            }else  {
                self.loading.dismiss()
                // self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                //self.tbl_city_list.hidden = true
                //---RV ---
            }
        }
    }
    
    //===================================
    // MARK: - Gender action method
    //===================================
    
    @IBAction func btnGenderAction(_ sender: UIButton) {
        //        if sender.tag == 1{
        //            btnFemale.setImage(UIImage(named: "img_radio_select.png"), forState: .Normal)
        //            btnMale.setImage(UIImage(named: "img_radio_unselect.png"), forState: .Normal)
        //            btnOther.setImage(UIImage(named: "img_radio_unselect.png"), forState: .Normal)
        //
        //        }else if sender.tag == 2{
        //            btnFemale.setImage(UIImage(named: "img_radio_unselect.png"), forState: .Normal)
        //            btnMale.setImage(UIImage(named: "img_radio_select.png"), forState: .Normal)
        //            btnOther.setImage(UIImage(named: "img_radio_unselect.png"), forState: .Normal)
        //
        //        } else if sender.tag == 3{
        //            btnFemale.setImage(UIImage(named: "img_radio_unselect.png"), forState: .Normal)
        //            btnMale.setImage(UIImage(named: "img_radio_unselect.png"), forState: .Normal)
        //            btnOther.setImage(UIImage(named: "img_radio_select.png"), forState: .Normal)
        //
        //        }
    }
    
    
    
    //==================================
    // MARK: - Service Method
    //==================================
    
    @IBAction func registrationAction (_ sender: UIButton){
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
       
        if txtEmail.text == "" || !AppTheme().isValidEmail(txtEmail.text as String!)
        {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Email.", showInView: self.view)
        } else if txtPassword.text == ""
        {
            _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Password.", showInView: self.view)
        } else if txtRePass.text != txtPassword.text
        {
           _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Passwords are not Match", showInView: self.view)
        }
        else if txtEmail.text == "" || !AppTheme().isValidEmail(txtEmail.text as String!)
        {
          _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Valid Email.", showInView: self.view)
        } else if txtPassword.text == ""
        {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Password.", showInView: self.view)
        } else if txtRePass.text != txtPassword.text && txtRePass.text == ""
        {
           _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Password not Matched.", showInView: self.view)
        }
        
        else if RegisterationVC.isCheck == false{
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:"Please check terms and conditions", showInView: self.view)
        }
            
        else {
            var deviceT = ""
            
            if let deviceToken = UserDefaults.standard.value(forKey: "dt") as? String {
                deviceT = deviceToken
            }else{
                if deviceT == "" || deviceT.isEmpty == true {
                    deviceT = AppTheme.deviceToken
                }
            }
            
            let parameters : [String: AnyObject] = [//"username" : txtUserName.text as! AnyObject,
                                                    //"first_name" : txtUserName.text as! AnyObject,
                                                    //"last_name" : txtUserName.text as! AnyObject,
                                                    "email" : txtEmail.text as! AnyObject,
                                                    "password" : txtPassword.text as! AnyObject,
                                                    //"dob" : "",//txtBirthday.selectedItem as! AnyObject,
                //"city_id" : cityIdSelected as AnyObject,
                //"interests" :"", // txtInterest.selectedItem as! AnyObject,
                "t_c" : checkValue as AnyObject,
                "device_id" : deviceT as AnyObject,
                "device_type" : AppTheme.deviceType as AnyObject]
            
            AppTheme().callPostWithMultipartService(String(format: "%@signup", AppUrl), param: parameters, imageData: imageData ) { (result, data) -> Void in
                
                print(data)
                if(result == "success") {
                    var departments : NSDictionary = NSDictionary()
                    departments = data as! NSDictionary
                    AppTheme.setLoginDetails(departments)
                    let objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Signup Sucessfull", showInView: self.view)
                    self.navigationController?.pushViewController(objFlightDetailVC, animated: true)
                    
                } else {
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: (data as? String)!, showInView: self.view)
                }
            }
        }
    }
    
    //    func getCountryStateCity (SubURl: String, searchId:String , completion: (result: String, data : AnyObject) -> Void)
    //    {
    //        var parameters : [String: AnyObject] = [String: AnyObject]()
    //        if SubURl == "state" {
    //            parameters  = ["country_id" : searchId]
    //            self.stateName = NSMutableArray()
    //            self.stateId = NSMutableArray()
    //        } else if SubURl == "city"
    //        {
    //            parameters  = ["state_id" : searchId]
    //            self.cityName = NSMutableArray()
    //            self.cityId = NSMutableArray()
    //        } else {
    //            parameters  = [String: AnyObject]()
    //            self.countryName = NSMutableArray()
    //            self.countryId = NSMutableArray()
    //        }
    //
    //        AppTheme().callGetService(String(format: "%@%@", AppUrl, SubURl), param: parameters) { (result, data) -> Void in
    //            print(data)
    //            if(result == "success") {
    //                if let aryData : NSMutableArray = data.valueForKey("response") as? NSMutableArray
    //                {
    //                    for dic in aryData
    //                    {
    //                        if SubURl == "state" {
    //                            self.stateId.addObject(dic["id"] as! String)
    //                            self.stateName.addObject(dic["name"] as! String)
    //                            completion(result: "success", data: self.stateName)
    //
    //                        } else if SubURl == "city" {
    //
    //                            self.cityId.addObject(dic["id"] as! String)
    //                            self.cityName.addObject(dic["name"] as! String)
    //                            completion(result: "success", data: self.cityName)
    //
    //
    //                        } else {
    //                            self.countryId.addObject(dic["id"] as! String)
    //                            self.countryName.addObject(dic["name"] as! String)
    //                            completion(result: "success", data: self.countryName)
    //
    //                        }
    //                    }
    //                }
    //                print(data["response"])
    //            } else {
    //                completion(result: "error", data: "error Found")
    //            }
    //        }
    //    }
    //
    
    func endUpEveryThing()
    {
        self.performSegue(withIdentifier: "reg_home_seg", sender: self)
    }
    
    //MARK: - Check-uncheck and terms&condition button action
    
    @IBAction func btnCheckUncheckAction(_ sender: AnyObject)
    {
        if RegisterationVC.isCheck == false
        {
            RegisterationVC.isCheck = true
            checkValue = "1"
            checkUncheckButton.setImage(UIImage(named: "Check.png"), for: UIControlState())
        }else{
            RegisterationVC.isCheck = false
            checkValue = "0"
            checkUncheckButton.setImage(UIImage(named: "Uncheck.png"), for: UIControlState())
        }
    }
    
    @IBAction func btnSignUp(_ sender: AnyObject) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if txtEmail.text == "" || !AppTheme().isValidEmail(txtEmail.text as String!)
        {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Valid Email.", showInView: self.view)
            
        } else if txtPassword.text == ""
        {
            _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please enter Password.", showInView: self.view)
            
        } else if txtRePass.text != txtPassword.text
        {
            _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Password not Matched.", showInView: self.view)
        }

        else if txtBirthday.text != ""
        {
            let date2 : Date =  (Calendar.current as NSCalendar).date(byAdding: .year, value: -13, to: Date(), options: [])!
            let date_current = Date()
            let deFormatter = DateFormatter()
            let date_final = date
            
            deFormatter.dateFormat = "MMM dd, YYYY"
            let dateAfter = date_final.add(-13, months: 0, weeks: 0, days: 0, hours: 0, minutes: 0, seconds: 0)
            print(dateAfter)
            
            if date2 <= date_current {
                if  date_final <=  date2 {
                        var deviceT = ""
                        
                        if let deviceToken = UserDefaults.standard.value(forKey: "dt") as? String {
                            deviceT = deviceToken
                        }else{
                            if deviceT == "" || deviceT.isEmpty == true {
                                deviceT = "123456"
                            }else{
                                deviceT =  AppTheme.deviceToken
                            }
                        }
                    self.loading = JGProgressHUD(style: .dark)
                    self.loading.textLabel.text = "Loading.."
                    self.loading.show(in: self.view)
                    
                    AppTheme.setUserEmail_Login(txtEmail.text! as NSString)
                    
                    let paraDict = NSMutableDictionary()
                    paraDict.setValue(txtEmail.text!, forKey:"email")
                    paraDict.setValue(txtPassword.text!, forKey: "password")
                    paraDict.setValue(checkValue!, forKey:"t_c")
                    paraDict.setValue(deviceT, forKey: "device_id")
                    paraDict.setValue(AppTheme.deviceType, forKey: "device_type")
                    
                    AppTheme().callPostService(String(format: "%@signup", AppUrl), param: paraDict) { (result, data) -> Void in
                        
                        print(data)
                        DispatchQueue.main.async {
                            self.loading.dismiss()
                            if(result == "success") {
                                
                                var departments : NSDictionary = NSDictionary()
                                departments = data as! NSDictionary
                                
                                AppTheme.setLoginDetails(departments)
                                let objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "VerificationControllerl") as! VerificationControllerl
                                self.navigationController?.pushViewController(objFlightDetailVC, animated: true)
                                
                                
                            } else {
                                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: (data as? String)!, showInView: self.view)
                            }
                        }
                        
                        
                    }

                } else if  date_final >=  date2 {
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "The user should be 13+ years old to use this app.", showInView: self.view)
                }
                
            }
            
           
        }
        
        else if RegisterationVC.isCheck == false {
            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:"Please check terms and conditions", showInView: self.view)
        }
        else {
            
            var deviceT = ""
            
            if let deviceToken = UserDefaults.standard.value(forKey: "dt") as? String {
                deviceT = deviceToken
            } else {
                if deviceT == "" || deviceT.isEmpty == true {
                    deviceT = "123456"
                }else{
                    deviceT =  AppTheme.deviceToken
                }
            }
                self.loading = JGProgressHUD(style: .dark)
                self.loading.textLabel.text = "Loading.."
                self.loading.show(in: self.view)
                
                AppTheme.setUserEmail_Login(txtEmail.text! as NSString)
                
                let paraDict = NSMutableDictionary()
                paraDict.setValue(txtEmail.text!, forKey:"email")
                paraDict.setValue(txtPassword.text!, forKey: "password")
                paraDict.setValue( checkValue!, forKey:"t_c")
                paraDict.setValue(deviceT, forKey: "device_id")
                paraDict.setValue(AppTheme.deviceType, forKey: "device_type")
                
            AppTheme().callPostService(String(format: "%@signup", AppUrl), param: paraDict) { (result, data) -> Void in
                
                print(data)
                DispatchQueue.main.async {
                    self.loading.dismiss()
                    
                if(result == "success") {
                    
                        var departments : NSDictionary = NSDictionary()
                        departments = data as! NSDictionary
                        
                        AppTheme.setLoginDetails(departments)
                        let objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "VerificationControllerl") as! VerificationControllerl
                        self.navigationController?.pushViewController(objFlightDetailVC, animated: true)
                    
                } else {
                    self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: (data as? String)!, showInView: self.view)
                }
              }
            }
        }
    }
    
    
    
    @IBAction func btnTermsCondition(_ sender: AnyObject) {
        var destVC : TermsAndConditionController! = TermsAndConditionController()
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionController") as! TermsAndConditionController
        appDelegate.strURL = "OnSignUp"
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    
    //MARK: ---RV--- Button Actions ---
   
    @IBAction func btnShowViewForTable(_ sender: AnyObject)
    {
        let btn : UIButton = sender as! UIButton
        if(btn.tag == 123401)
        {
            lblSelectListTitle.text = "Select City"
            strTableType = "City"
        }
        else if(btn.tag == 123402)
        {
            lblSelectListTitle.text = "Select Gender"
            strTableType = "Gender"
        }
        else
        {
            lblSelectListTitle.text = "Select Interest"
            strTableType = "Interest"
        }
        self.tbl_city_list.isUserInteractionEnabled = true
        
        if isCitySearching
        {
            isCitySearching = false
            txtCity.resignFirstResponder()
        }
        tbl_city_list.isHidden = false
        //-- RV Future Scope --
        //if(btn.tag != 123401)
        //{
        tbl_city_list.reloadData()
        //}
        showSelectionView()
    }
    
    func hideSelectionView()
    {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations:
            {
                self.viewTblPickerBottomLayout.constant = -217
                self.view.layoutIfNeeded()
        }){ (finished) -> Void in
            self.viewCityContainer.isHidden = true
        }
        
    }
    
    func showSelectionView()
    {
        viewCityContainer.isHidden = false
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
            
            self.viewTblPickerBottomLayout.constant = 0
            self.view
                .layoutIfNeeded()
            
            })
        { (finished) -> Void in
            //self.playVideo(strDefaultUrl)
        }
        
    }
    
    //---
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tbl_city_list{
            
            if strTableType == "Gender"
            {
                return arrOfGender.count
            }
            else if strTableType == "Interest"
            {
                return arrOfInterest.count
            }
        }else {
            if  strTableType == "City"
            {
                return arr_cityList.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tbl_city_list
        {
            var cell = tableView .dequeueReusableCell(withIdentifier: "cell")
            if cell == nil
            {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            }
            cell?.textLabel?.textAlignment = NSTextAlignment.center
            if strTableType == "Gender"
            {
                cell?.textLabel?.text = arrOfGender.object(at: indexPath.row) as? String
                //cell!.userInteractionEnabled = false
                cell?.backgroundColor = UIColor.clear
                return cell!
            }
            else if strTableType == "Interest"
            {
                cell?.textLabel?.text = arrOfInterest.object(at: indexPath.row) as? String
                //cell!.userInteractionEnabled = false
                cell?.backgroundColor = UIColor.clear
                return cell!
            }
            else{
                cell?.textLabel?.text = "Demo Data"
                cell!.isUserInteractionEnabled = false
                cell?.backgroundColor = UIColor.clear
                return cell!
            }
        }
        else {
                var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
                
                if cell == nil {
                    cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
                }
                let temp_dic: NSDictionary = self.arr_cityList.object(at: indexPath.row) as! NSDictionary
                if arr_cityList.count > indexPath.row {
                    if let str_city_name : String = temp_dic .value(forKey: "location") as? String{
                        if let str_country = temp_dic .value(forKey: "country") as? String{
                           cell.titleLabel.text = "\(str_city_name), \(str_country)"
                        }
                    }
                    cell.cityLabel.isHidden = true
                }
                return cell
            }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tbl_city_list {
            if strTableType == "Gender"{
                txtGender.text = arrOfGender.object(at: indexPath.row) as? String
                hideSelectionView()
            }
            else if strTableType == "Interest"
            {
                txtInterest.text = arrOfInterest.object(at: indexPath.row) as? String
                hideSelectionView()
            }
        }else{
            let dic: NSDictionary = self.arr_cityList.object(at: indexPath.row) as! NSDictionary
            
            if let str_city_name  = dic .value(forKey: "location") as? String{
                txtCity.text = str_city_name
            }
            if let str_city_id : String = dic .value(forKey: "id") as? String{
                str_id_city = str_city_id
            }
            viewForCity.isHidden = true
            hideSelectionView()
            isCitySearching = false
        }
    }
    
    @IBAction func btnCancelOnGenderInterest(_ sender: AnyObject) {
        if isCitySearching{
            isCitySearching = false
            txtCity.resignFirstResponder()
            viewForCity.isHidden = true
        }
        hideSelectionView()
    }
    
    @IBAction func btnCancelOnCity(_ sender: AnyObject) {
        self.viewForCity.isHidden = true
    }
}
