//
//  SearchUserListController.swift
//  Gupa Network
//
//  Created by MWM23 on 11/28/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class SearchUserListController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var search_user: UISearchBar!
    @IBOutlet weak var tbl_search_user: UITableView!
    
    var titleString : String!
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var dicData : NSDictionary!
    var isFollowing : NSInteger!
    var strUserId : String!
    var apiFormat: String!
    
    var respDict : NSDictionary = NSDictionary()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    
    var searchActive : Bool = false
    var checkTableCell : NSInteger = 0
    
    let identifier = "CommentCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startProgressBar()
        updateNavigation()
        
        self.title = "Search User"
        appDelegate.centerNav = self.navigationController
        tbl_search_user.backgroundColor = UIColor.clear
        tbl_search_user.separatorStyle = UITableViewCellSeparatorStyle.none
        tbl_search_user.estimatedRowHeight = 100
        tbl_search_user.rowHeight = UITableViewAutomaticDimension
        
        tbl_search_user.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: identifier)
        tbl_search_user.isHidden = true
       // getSearchUserList()
    }

    func updateNavigation(){
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        btnLeftBar.addTarget(self, action: #selector(SearchUserListController.leftBarBackButton), for: .touchUpInside)
            
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        UserDefaults.standard.set(0, forKey: "isComment")

    }
    @objc func leftBarBackButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getSearchUserList(_ searchText: String) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let parameters : [String: String]!
        
        parameters = ["search"  : search_user.text! as String ,
                      "user_id" : AppTheme.getLoginDetails().object(forKey: "id") as! String]
        
        AppTheme().callGetService(String(format: "%@user_search", AppUrl), param: parameters! as [String : AnyObject]) {(result, data) -> Void in                                                                                ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                self.respDict = data as! NSDictionary
                print(self.respDict)
                self.tbl_search_user.isHidden = false
                if let array_response : NSArray = self.respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }else{
                    self.arrOfResponseKey =  self.respDict["response"] as! NSMutableArray
                }
                self.tbl_search_user.reloadData()
            } else{
                self.loading.dismiss()
               // self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
        self.loading.dismiss()
        if self.arrOfResponseKey.count != 0{
            row =  self.arrOfResponseKey.count
            return row
        } else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        self.loading.dismiss()
        var cell : CommentCell! = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        if cell == nil {
            cell = CommentCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommentCell")
        }
        
        cell.btnUserDwtail.isUserInteractionEnabled = false
        cell.contentView.layer.backgroundColor = UIColor.clear.cgColor
        let dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        cell.lbl_date.isHidden = true
        
        if let str_user_full_name : String = dic["interests"] as? String{
            cell.lbl_post.text = str_user_full_name
        }
        if let str_user_name : String = dic["name"] as? String{
            cell.lblSubName.text = str_user_name
        }
        if let str_profile_pic : String = dic["profile_pic"] as? String{
            let url = URL(string:str_profile_pic)
            cell.imgUser.setImageWith(url!, placeholderImage: UIImage(named: "ic_user.png"))
        }
        
//        cell.btnUserDwtail.addTarget(self, action: #selector(SearchUserListController.btnUserFollowUnfollow(_:)), for: .touchUpInside)
//        cell.btnUserDwtail.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        
        var destVC = UserChatOnCommentController()
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
        destVC.isFromTrip = "SearchUser"
        destVC.str_other_id = strUserId
        if let strId : String = dic["id"] as? String{
             destVC.str_other_id = strId
        }
        destVC.dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    @objc func btnUserFollowUnfollow(_ sender: UIButton)
    {
//        var destVC = UserChatOnCommentController()
//        destVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
//        destVC.dic = self.arrOfResponseKey.object(at: sender.tag) as! NSDictionary
//        self.navigationController?.pushViewController(destVC, animated: true)
//        // print("index: %@", sender.tag )
//        let dicData: NSDictionary = self.arrOfResponseKey.objectAtIndex(sender.tag) as! NSDictionary
//        print(dicData)
//        self.navigationController?.navigationBarHidden = true
//       
//        if let str_user_id : String = dicData .valueForKey("user_id") as? String{
//            userIdForSingleUserDetail = str_user_id
//            NSUserDefaults.standardUserDefaults().setValue(str_user_id, forKey: "otherId")
//            NSUserDefaults.standardUserDefaults().synchronize()
//            self.getSingleUserInfo(userIdForSingleUserDetail)
//        }
    }
    
    //==============================
    //MARK: Loader method
    //==============================
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    
    //=====================================
    //MARK: UISearchBar delegate methods
    //=====================================
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchActive = true;
        checkTableCell = 0
        searchActive = false;
        ///flightList = getFlights()
        tbl_search_user.isHidden = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        search_user.resignFirstResponder()
        searchActive = false;
        tbl_search_user.isHidden = true
        //startProgressBar()
        //getSearchUserList()//(flightSearch)
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        search_user.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        getSearchUserList(searchText)
//        let resultPredicate = NSPredicate(format: "SELF CONTAINS[cd] %@", searchText)
//        filtered = totalFlight.filteredArrayUsingPredicate(resultPredicate)
//        if(filtered.count == 0){
//            stopActivity()
//            searchActive = false;
//            checkTableCell = 1
//            flightSearch = ""
//            //getFlightList(flightSearch)
//            self.tblFlightLish.reloadData()
//        } else {
//            searchActive = true;
//            checkTableCell = 0
//            self.tblFlightLish.reloadData()
//        }
    }

}
