//
//  HomeVC.swift
//  Gupa Network
//
//  Created by mac on 5/12/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import CoreLocation
import Kingfisher
import QuartzCore
import AVKit
import AVFoundation
import MediaPlayer
import AssetsLibrary
import MobileCoreServices
import CoreData
import Photos

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UISearchBarDelegate{
    
    var captureSession = AVCaptureSession()
    var sessionOutput = AVCaptureStillImageOutput()
    var movieOutput = AVCaptureMovieFileOutput()
    var previewLayer = AVCaptureVideoPreviewLayer()
    
    var appDelegate: AppDelegate = AppDelegate()
    var picker:UIImagePickerController?=UIImagePickerController()
    weak var delegate: (UIImagePickerControllerDelegate & UINavigationControllerDelegate)?
    
    @IBOutlet weak var imgCameraGallary: UIImageView!
    @IBOutlet var viewBtnContainer: UIView!
    @IBOutlet var btnHotel: UIButton!
    @IBOutlet var btnTravelBoarding: UIButton!
    @IBOutlet var btnTrip: UIButton!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!

    //Video player controller===============
    
    var player_video: AVPlayer!
    var avpController : AVPlayerViewController! = AVPlayerViewController()
    
    //Add Flight Pop Up Elements=============================
    
    @IBOutlet weak var viewForCity_Flight_add: UIView!
    @IBOutlet weak var tbl_city_add_flight: UITableView!
    @IBOutlet weak var btnCancelSelection_add_flight: UIButton!
    @IBOutlet weak var lblSelectListTitle_add_flight: UILabel!
    
    @IBOutlet weak var viewFor_bg_popup_add_flight: UIView!
    @IBOutlet weak var view_container_for_review_popup_add_flight: UIView!
    @IBOutlet weak var view_container_for_checkInCheckOut_add_flight: UIView!
    @IBOutlet weak var viewForDatePicker_add_flight: UIView!
    @IBOutlet weak var viewFor_depart_arrival_time_add_flight: UIView!
    
    @IBOutlet weak var timePicker_add_flight:UIDatePicker!
    
    @IBOutlet weak var textview_arrival_time_add_flight: UITextView!
    @IBOutlet weak var textview_depart_time_add_flight: UITextView!
    
    @IBOutlet weak var txtAirlineName_add_flight: UITextField!
    @IBOutlet weak var txtFlightNumber_add_flight: UITextField!
    @IBOutlet weak var txtName_On_Ticket_add_flight: UITextField!
    @IBOutlet weak var txt_booking_reference_add_flight: UITextField!
    @IBOutlet weak var txtDescrption_ticket: UITextView!
    @IBOutlet weak var txtDepartureCity_add_flight: UITextField!
    @IBOutlet weak var txtArrivalCity_add_flight: UITextField!
    
    @IBOutlet weak var btnCheckForConnectToBusCompany_add_flight: UIButton!
    @IBOutlet weak var btnCancelCheckInCheckOut_add_flight: UIButton!
    @IBOutlet weak var btnSubmitCheckInCheckOut_add_flight: UIButton!
    
    var connectedLike_: String = "0"
    var textfield_common : UITextField = UITextField()
    var isTransportSearching : Bool = false
    var str_Transport_id : String = String()
    var airline_id_selected : String = String()
    var departure_city_airport_addFlight : String! = String()
    var arrival_city_airport_addFlight : String! = String()
    var departure_city_code_addFlight : String! = String()
    var arrival_city_code_addFlight : String! = String()
    
    
    //Hotel search option view container=====================
    
    @IBOutlet var viewTxtDestiContainer : UIView!
    @IBOutlet weak var btn_search_hotel: UIButton!
    @IBOutlet weak var flightSearchBar: UISearchBar!
    @IBOutlet weak var btn_addHotel_detail: UIButton!
    
    @IBOutlet weak var viewForCity_add_hotel: UIView!
    @IBOutlet weak var tbl_city: UITableView!
    @IBOutlet weak var btnCancelSelection: UIButton!
    @IBOutlet weak var lblSelectListTitle: UILabel!
    
    @IBOutlet weak var viewFor_bg_popup_add_hotel: UIView!
    @IBOutlet weak var lbl_addHotelFlight_title: UILabel!
    @IBOutlet weak var view_container_for_review_popup_add_hotel: UIView!
    @IBOutlet weak var view_container_for_checkInCheckOut_add_hotel: UIView!
    @IBOutlet weak var txt_city: UITextField!
    @IBOutlet weak var txt_Hotel_name: UITextField!
    @IBOutlet weak var btn_ImageCapture_hotelAdd: UIButton!
    @IBOutlet weak var lbl_select_image: UILabel!
    @IBOutlet weak var textview_description: UITextView!
    @IBOutlet weak var txt_Name_Used_Booking: UITextField!
   // @IBOutlet weak var txt_confirmation_num: UITextField!
    @IBOutlet weak var txt_booking_reference: UITextField!
    @IBOutlet weak var btnCheckForConnectToHotel_add_hotel: UIButton!
    @IBOutlet weak var btnCancelCheckInCheckOut_add_hotel: UIButton!
    @IBOutlet weak var btnSubmitCheckInCheckOut_add_hotel: UIButton!
    
    var arr_cityList : NSMutableArray! = NSMutableArray()
    var arr_airlineName : NSMutableArray! = NSMutableArray()
    var connectedLike_add_hotel: String = "0"
    var buttonSelected_add_hotel : UserDefaults!
    var timeString_add_hotel : String = String()
    var isHotelSearching : Bool = false
    var isHotelSearching_addDetail : Bool = false
    var str_Hotel_id : String = String()
    var isCamera : Bool = false
    var chosenImage_addDetail: UIImage!
    var imageVideoData_addDetail: Data = Data()
    
    //====================================================
    
    @IBOutlet var tblVwFlight : UITableView!
    @IBOutlet var viewForTravelActivity : UIView!
    
    //Write review on travel board view container=====================
    
    @IBOutlet weak var viewForWriteStatus: UIView!
    @IBOutlet weak var viewForSmilePopUp: UIView!
    @IBOutlet weak var imgUserOnStatusview: UIImageView!
    @IBOutlet weak var txtForWriteStatus: UITextView!
    @IBOutlet weak var btnGoodSmile: UIButton!
    @IBOutlet weak var btnCommentOnOff: UIButton!
    @IBOutlet weak var btnBadSmile: UIButton!
    @IBOutlet weak var btnUgliSmile: UIButton!
    @IBOutlet weak var btnRating: UIButton!
    @IBOutlet weak var btnCameraVideo: UIButton!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var tblWriteStatus: UITableView!
    @IBOutlet weak var timeSelectionButton: UIButton!
    
    //======================================================
    
    @IBOutlet weak var viewForAlertCamera: UIView!
    @IBOutlet weak var viewForImageSelect: UIView!
    @IBOutlet weak var viewForVideoSelect: UIView!
    @IBOutlet weak var viewForDatePicker: UIView!
    @IBOutlet weak var viewForSmilePopUpOnCameraAlert: UIView!
    @IBOutlet weak var imgUserOnAlert: UIImageView!
    @IBOutlet weak var txtViewForCaution: UITextView!
    @IBOutlet weak var btnImageFromGallery: UIButton!
    @IBOutlet weak var btnImageFromCamere: UIButton!
    @IBOutlet weak var btnRatingOnAlert: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSendOnAlert: UIButton!
    @IBOutlet weak var btnGoodOnAlert: UIButton!
    @IBOutlet weak var btnBadOnAlert: UIButton!
    @IBOutlet weak var btnUglyOnAlert: UIButton!
    
    @IBOutlet weak var viewForUserFollowUnfollow: UIView!
    @IBOutlet weak var viewFollowersFollowingPost : UIView!
    @IBOutlet weak var viewUserPostTbl : UIView!
    @IBOutlet weak var btnCancelOnFollowUnfollow: UIButton!
    @IBOutlet weak var userNameTitleBar: UILabel!
    
    @IBOutlet weak var userBgImage: UIImageView!
    @IBOutlet weak var userProfileImageOnUserDetail: UIImageView!
    @IBOutlet weak var userFirstLastNameUserDetail: UILabel!
    @IBOutlet weak var userAddressUserDetail: UILabel!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet var chatButtonOnNavigationUserDetail: UIButton!
    @IBOutlet var followUnfollowButtonOnNavigationOnUserDetail: UIButton!
    @IBOutlet weak var userTotalPostLabel: UILabel!
    @IBOutlet weak var userFollowingLabel: UILabel!
    @IBOutlet weak var userFollowersLabel: UILabel!
    
    @IBOutlet weak var tblUserDetailFollowUnfollow: UITableView!
    
    @IBOutlet var leftBarBtn : UIBarButtonItem!
    @IBOutlet var rightBarBtn : UIBarButtonItem!
    var btnChat:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    
    @IBOutlet var txtDestination: UITextField!
    
    @IBOutlet weak var timePicker:UIDatePicker!
    
    @IBOutlet var userDetailArray: NSArray? = NSArray()
    @IBOutlet var userPostArray: NSMutableArray! = NSMutableArray()
    @IBOutlet var userPostDict: NSDictionary!
    @IBOutlet var userDetailDict: NSDictionary!
    
    @IBOutlet  weak var btn_searchHotel: UIButton!
    
    var loading : JGProgressHUD!
    var arr_notification : NSArray!
    var str_notification_count : String!
    
    var locationManager : CLLocationManager = CLLocationManager()
    var lat : Double = 0.0
    var long : Double = 0.0
    var firstLoc : Bool = false
    var isTextfieldSelect : Bool = false
    var strSourceCode: String = String()
    var strDestinationCode: String = String()
    static var isUserDetailLike : Bool = false
    var pickerCountStatCity = UIPickerView()
    var DynamicView : UIView = UIView()
    var button : UIButton = UIButton()
    var filtered : NSArray = NSArray()
    var refreshControl: UIRefreshControl!
    let hotelModel : HotelDetail = HotelDetail()
    
    var arrOfResponseKey: NSMutableArray = NSMutableArray()
    var arrOfDestinatioStation: NSMutableArray = NSMutableArray()
    var arrOfRefreshed: NSMutableArray = NSMutableArray()
    
    var arrOfDestinatioStationCode: NSMutableArray = NSMutableArray()
    var arrOfDestinatioStationCity: NSMutableArray = NSMutableArray()
    var respoIsArray: NSArray = NSArray()
    var reversed: NSArray = NSArray()
    var searchActive : Bool = false
    var shouldCellBeExpanded : Bool = false
    
    var strParamLatLong: String = String()
    var strPresentLocName = ""
    var strCountry: String!
    
    var dicData : NSDictionary = NSDictionary()
    var respDict : NSDictionary = NSDictionary()
    
    var checkTableCell : NSInteger = 0
    var rating: NSInteger = 0
    var isActive: NSInteger = 0
    var isSmileButtonGoodSelect : Bool = false
    var isSmileButtonBadSelect : Bool = false
    var isSmileButtonUglySelect : Bool = false
    var timeString : String = String()
    var userIdForSingleUserDetail : String = String()
    var selectedUserIdOnFeed : String = String()
    
    var isUsrCheckIn: Bool = false
    var isCompressed: Bool = false
    var videoData_compress : Data! = Data()
    var chosenImage: UIImage!
    var selectedVideoPath: URL!
    var imageVideoData: Data!
    var imageVideoDataCompressed: Data!
    var videoData: Data!
    var player = AVPlayer ()
    var playerLayer = AVPlayerLayer()
    var isPlaying: Bool!
    var btnPlayPause: UIButton!
    var likeByMe : NSInteger!
    var flight_Id : NSInteger!
    var post_Id : NSInteger!
    var isFollow : NSInteger!
    var commentOnOffInt : NSInteger!
    
    fileprivate var objFlightDetailVC : FlightDetailVC?

    var strUserId : String!
    var dic: NSMutableDictionary!
    
    var isForLogout : NSInteger!
    static var shouldHideNavBar : Bool = false
    var searchTextLength : Int!
    
    var  queue : OperationQueue!
    var isSearchButtonClick : Bool = false
    var isScrolling: Bool = false
    var isAddCalled: NSInteger = 1
    var objMoviePlayerController: MPMoviePlayerController = MPMoviePlayerController()
    var urlVideo: URL!
    var flag = 1
    var strComment = "0"

    var databaseManager = DatabaseManager.sharedDBInterface // Local database

    //-===============================
    // MARK:- Life cycle methods
    //-===============================
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        checkTableCell = 105
        isTextfieldSelect = false
        self.tblWriteStatus.tableFooterView = UIView()
        isForLogout = 0
        button.tag = 1
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        tbl_city.delegate = self
        tbl_city.dataSource = self
        
        //appDelegate.window?.rootViewController = self
        appDelegate.centerNav = self.navigationController

        updateUI()
        showRightNavigationBarButtonHide()
        self.callLatLongSourceDestination()
        
        //** Check for Database insert data or call Destination station from Server
        if let isLogin = AppTheme.getIsLogin() as? Bool {
            if isLogin == true{
                _ = getAirportDetail()
            } else {
                AppTheme.setIsLogin(true)
                getAirDetail()
            }
        }
        self.btn_addHotel_detail.setTitle("Enter Flight Details", for: UIControlState())
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(HomeVC.refresh(_:)), for: UIControlEvents.valueChanged)
        tblWriteStatus.addSubview(refreshControl)
    
        self.lbl_select_image.text = "Select image (Optional)."
        viewFor_bg_popup_add_flight.isHidden = true
        viewFor_bg_popup_add_hotel.isHidden = true
        view_container_for_review_popup_add_hotel.isHidden = true
        view_container_for_checkInCheckOut_add_hotel.isHidden = true
        view_container_for_review_popup_add_flight.isHidden = true
        
        if appDelegate.isRemoteNotification == true {
            
            if appDelegate.dictAPNs.count > 0 {
                
               if let isLogin = AppTheme.getIsLogin() as? Bool {
                
                    if isLogin == true {
                        
                        if let aps: NSDictionary = appDelegate.dictAPNs["aps"] as? NSDictionary, let alert = aps["alert"] as? NSDictionary  {
                            
                           // print("11111100==\(alert)")
                            
                            // Notification for passenger
                            if let notification_id = alert.value(forKey: "notification_id") as? Int  {
                                
                                print("notification_id ==\(notification_id)")
                                
                                if notification_id == 1 {
                                    
                                    if let body = alert["body"] as? NSDictionary {
                                        
                                        let action_type  = body.value(forKey: "action_type") as? String
                                        let messsage     = body.value(forKey: "message") as? String
                                        let driverDetail = body.value(forKey: "driver_details") as? NSDictionary
                                        
                                        let arrDropOff  = body.value(forKey: "drop") as? NSArray
                                        let dictDropOff = arrDropOff![0] as! NSDictionary
                                        
                                        let arrpickup  = body.value(forKey: "pickup") as? NSArray
                                        let dictPickUp = arrpickup![0] as! NSDictionary
                                        
                                        let strAmount    = body.value(forKey: "amount") as? String
                                        let strPaymentType  = body.value(forKey: "payment_type") as? String
                                        let strBookingId  = body.value(forKey: "booking_id") as? String
                                        let strTitle     = alert.value(forKey: "title") as? String
                                        let strDistance  = body.value(forKey: "distance") as Any
                                        let strDateNTime  = body.value(forKey: "book_create_date_time") as! String
                                        
                                        if action_type == "1" {
                                            
                                            let alertVC = UIAlertController(title: strTitle, message: "Your booking is confirmed by driver! Driver will arrive soon.", preferredStyle: .alert)
                                            
                                            let actionOK = UIAlertAction(title: "Ok", style: .default){ __ in
                                                
                                               let tripDetailsVC = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.taxiVCStoryboard, viewControllerName: TripDetailsVC.nameOfClass) as TripDetailsVC
                                                tripDetailsVC.tripDetailDict = driverDetail!
                                                tripDetailsVC.str_SelectedAmount = strAmount!
                                                tripDetailsVC.str_SelectCardType = strPaymentType!
                                                tripDetailsVC.str_SelectedBookinID = strBookingId!
                                                tripDetailsVC.str_Distance = "\(String(describing: strDistance))!"
                                                tripDetailsVC.pickUpDict = dictPickUp
                                                tripDetailsVC.dropOffDict = dictDropOff
                                                
                                                self.navigationController?.pushViewController(tripDetailsVC, animated: false)
                                                
                                            }
                                            alertVC.addAction(actionOK)
                                            self.present(alertVC, animated: true, completion: nil)
                                            
                                        }  else if action_type == "2" {
                                            
                                            let alertVC = UIAlertController(title: "Booking Rejected!", message: "Your booking is rejected by driver! Please book for another driver.", preferredStyle: .alert)
                                            let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                                                
                                                let tripDetailsVC = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.taxiVCStoryboard, viewControllerName: PickupAndDropLocationVC.nameOfClass) as PickupAndDropLocationVC
                                                self.navigationController?.pushViewController(tripDetailsVC, animated: false)
                                                
                                            }
                                            alertVC.addAction(actionOk)
                                            self.present(alertVC, animated: true, completion: nil)
                                            
                                        } else if action_type == "4" {
                                            
                                            let alertVC = UIAlertController(title: messsage, message: "Driver is arriving soon! Please wait for some time.", preferredStyle: .alert)
                                            let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                                                
                                                let mainStoryboard : UIStoryboard = UIStoryboard(name: TaxiAppConstant.taxiVCStoryboard, bundle: nil)
                                                let tripDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "TripDetailsVC") as! TripDetailsVC
                                                if let wd = UIApplication.shared.delegate?.window {
                                                    var vc = wd!.rootViewController
                                                    if(vc is UINavigationController){
                                                        vc = (vc as! UINavigationController).visibleViewController
                                                    }
                                                    if !(vc is TripDetailsVC){
                                                        tripDetailsVC.tripDetailDict = driverDetail!
                                                        tripDetailsVC.str_SelectedAmount = strAmount!
                                                        tripDetailsVC.str_SelectCardType = strPaymentType!
                                                        tripDetailsVC.str_SelectedBookinID = strBookingId!
                                                        tripDetailsVC.str_Distance = "\(String(describing: strDistance))"
                                                        tripDetailsVC.pickUpDict = dictPickUp
                                                        tripDetailsVC.dropOffDict = dictDropOff
                                                        tripDetailsVC.str_BookingDateAndTime = strDateNTime
                                                        self.navigationController?.pushViewController(tripDetailsVC, animated: false)
                                                    }
                                                }
                                                
                                            }
                                            alertVC.addAction(actionOk)
                                            self.present(alertVC, animated: true, completion: nil)
                                            
                                        }  else if action_type == "5" {
                                            
                                            let alertVC = UIAlertController(title: messsage, message: "Your trip has been started.", preferredStyle: .alert)
                                            let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                                                
                                                let mainStoryboard : UIStoryboard = UIStoryboard(name: TaxiAppConstant.taxiVCStoryboard, bundle: nil)
                                                let tripDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "TripDetailsVC") as! TripDetailsVC
                                                
                                                if let wd = UIApplication.shared.delegate?.window {
                                                    var vc = wd!.rootViewController
                                                    if(vc is UINavigationController){
                                                        vc = (vc as! UINavigationController).visibleViewController
                                                    }
                                                if !(vc is TripDetailsVC){
                                                tripDetailsVC.tripDetailDict = driverDetail!
                                                tripDetailsVC.str_SelectedAmount = strAmount!
                                                tripDetailsVC.str_SelectCardType = strPaymentType!
                                                tripDetailsVC.str_SelectedBookinID = strBookingId!
                                                tripDetailsVC.str_Distance = "\(String(describing: strDistance))"
                                                tripDetailsVC.pickUpDict = dictPickUp
                                                tripDetailsVC.dropOffDict = dictDropOff
                                                tripDetailsVC.str_BookingDateAndTime = strDateNTime
                                                tripDetailsVC.isTripStarted = true
                                                 self.navigationController?.pushViewController(tripDetailsVC, animated: false)
                                                  }
                                                }
                                                
                                            }
                                            alertVC.addAction(actionOk)
                                            self.present(alertVC, animated: true, completion: nil)
                                            
                                        } else if action_type == "6" {
                                            
                                            let alertVC = UIAlertController(title: messsage, message: "", preferredStyle: .alert)
                                            let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                                                
                                                let mainStoryboard : UIStoryboard = UIStoryboard(name: TaxiAppConstant.taxiVCStoryboard, bundle: nil)
                                                let tripDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "CompletedTripVC") as! CompletedTripVC
                                                tripDetailsVC.driverDict = driverDetail!
                                                tripDetailsVC.strBookingId = strBookingId!
                                                tripDetailsVC.pickUpDict = dictPickUp
                                                tripDetailsVC.dropOffDict = dictDropOff
                                                self.navigationController?.pushViewController(tripDetailsVC, animated: false)
                                            }
                                            alertVC.addAction(actionOk)
                                            self.present(alertVC, animated: true, completion: nil)
                                        }
                                    }
                                } else if notification_id == 4 {
                                    
                                    if let body = alert["body"] as? NSDictionary {
                                        
                                        let strTitle     = alert.value(forKey: "title") as? String
                                        
                                        let action_type  = body.value(forKey: "action_type") as? String
                                        //  let dictPickUp   = body.value(forKey: "pickup") as? NSDictionary
                                        //  let dictDropOff  = body.value(forKey: "drop") as? NSDictionary
                                        
                                        let arrDropOff  = body.value(forKey: "drop") as? NSArray
                                        let dictDropOff = arrDropOff![0] as! NSDictionary
                                        
                                        let arrpickup  = body.value(forKey: "pickup") as? NSArray
                                        let dictPickUp = arrpickup![0] as! NSDictionary
                                        
                                        let strBookingId  = body.value(forKey: "booking_id") as? String
                                        let strDateNTime  = body.value(forKey: "book_create_date_time") as! String
                                        let driverDetail = body.value(forKey: "driver_details") as? NSDictionary
                                        
                                        if action_type == "1" {
                                            
                                            let alertVC = UIAlertController(title: strTitle, message: "Driver has confrimed your book later request.", preferredStyle: .alert)
                                            
                                            let actionOK = UIAlertAction(title: "Ok", style: .default){ __ in
                                                
                                                let mainStoryboard : UIStoryboard = UIStoryboard(name: TaxiAppConstant.taxiVCStoryboard, bundle: nil)
                                                let tripDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmationTaxiVC") as! ConfirmationTaxiVC
                                                tripDetailsVC.str_SelectedBookinID = strBookingId!
                                                tripDetailsVC.pickUpDict = dictPickUp
                                                tripDetailsVC.dropOffDict = dictDropOff
                                                tripDetailsVC.driverDict = driverDetail!
                                                tripDetailsVC.str_BookingDateAndTime = strDateNTime
                                                self.navigationController?.pushViewController(tripDetailsVC, animated: false)
                                            }
                                            alertVC.addAction(actionOK)
                                           self.present(alertVC, animated: true, completion: nil)
                                        }
                                    }
                                }
                                
                            } else if let body = alert.value(forKey: "body") as? String {
                                let alertVC = UIAlertController(title: "Notification", message: body, preferredStyle: .alert)
                                let actionCancel = UIAlertAction(title: "Ok", style: .default){ __ in
                                }
                                alertVC .addAction(actionCancel)
                                self.present(alertVC, animated: true, completion: nil)
                            }
                        }
                    }
                }
                
                appDelegate.isRemoteNotification = false
            }
        }
      //  NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.pushToTrpDetail(_:)), name:NSNotification.Name(rawValue: "11111"), object: nil);
    }
    
    @objc func applicationDidBecomeActive(notification: NSNotification) {
        
        self.txtForWriteStatus.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.getAllPostDFromNotificationPostMethod), name: NSNotification.Name(rawValue: "LocalDatabaseData"), object: nil)
        
          NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive(notification:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        getNotificationList()
        chekIsLogin()
        flightSearchBar.text = ""
        objFlightDetailVC = nil
        
        if let str : String =  AppTheme.getLoginDetails().value(forKey: "id") as? String{
            strUserId = str
        }
        chatButtonOnNavigationUserDetail.isHidden = true
        
        if button.tag == 1  {
            
            self.btn_addHotel_detail.setTitle("Enter Flight Details", for: UIControlState())
            self.viewFor_bg_popup_add_flight.isHidden = true
            self.view_container_for_review_popup_add_flight.isHidden = true
            self.txtDestination.text = ""
            self.tblVwFlight.isHidden = true
            tblVwFlight.reloadData()
            checkTableCell = 105
            flightSearchBar.resignFirstResponder()
            flightSearchBar.canResignFirstResponder
            self.callLatLongSourceDestination()
            
        } else if button.tag == 2 {
            
            self.tblWriteStatus.isHidden = false
        }

        txtArrivalCity_add_flight.text = ""
        txtDepartureCity_add_flight.text = ""
        txtAirlineName_add_flight.text = ""
        txtFlightNumber_add_flight.text = ""
        txtName_On_Ticket_add_flight.text = ""
        txtDestination.text = ""
       // txt_booking_reference.text = ""
        txtDescrption_ticket.text = ""
        txt_booking_reference_add_flight.text = ""
        textview_depart_time_add_flight.text = "Departure Time"
        textview_arrival_time_add_flight.text = "Arrival Time"
       
      //  self.lbl_select_image.text = "Select image (Optional)."
        viewFor_bg_popup_add_flight.isHidden = true
       // viewFor_bg_popup_add_hotel.isHidden = true
//        view_container_for_review_popup_add_hotel.isHidden = true
//        view_container_for_checkInCheckOut_add_hotel.isHidden = true
        view_container_for_review_popup_add_flight.isHidden = true
        
    }
    
   
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "LocalDatabaseData") , object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive , object: nil)

        self.txtForWriteStatus.resignFirstResponder()
        
    }
    
    @objc func pushToTrpDetail(_ notification: Foundation.Notification) {
        let newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.taxiVCStoryboard, viewControllerName: TripDetailsVC.nameOfClass) as TripDetailsVC
        self.navigationController?.pushViewController(newGroupViewController, animated: true)
    }
    
    @objc func refresh(_ sender:AnyObject) {
        
        self.getUserStatusList("0", str_bottom: "0", str_limit: "", str_offset: "", str_add_offset: 0)
    }
    
    func showRightNavigationBarButtonHide()
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnChat = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnChat.setImage(imgChat, for: UIControlState())
        btnChat.addTarget(self, action: #selector(HomeVC.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnChat)
        
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(HomeVC.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    
    @objc func homeAction(){
  
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        self.btn_addHotel_detail.setTitle("Enter Flight Details", for: UIControlState())
        self.viewForCity_add_hotel.isHidden = true
        self.viewFor_bg_popup_add_hotel.isHidden = true
        self.arrOfDestinatioStation.removeAllObjects()
        btn_search_hotel.isUserInteractionEnabled = false
        self.title = "Trip"
        flightSearchBar.tag = 1
        flightSearchBar.text = ""
        flightSearchBar.placeholder = "Select Destination"
        txtDestination.isHidden = false
        self.txtDestination.text = ""        //Set the image on the button select on it
        self.btnTrip.setImage(UIImage(named: "button_airline_active.png")! as UIImage, for: UIControlState())
        self.btnTravelBoarding.setImage(UIImage(named: "Button_trio-Inactive.png")! as UIImage, for: UIControlState())
        self.btnHotel.setImage(UIImage(named : "hotel_bed_off.png")! as UIImage, for: UIControlState())
        appDelegate.strURL = ""
        self.viewForTravelActivity.isHidden = true
        self.viewTxtDestiContainer.isHidden = false
        buttonFlightPress()
     
    }
    
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    func updateUI() {
        
        self.viewFor_bg_popup_add_flight.isHidden = true
        self.viewForCity_add_hotel.isHidden = true
       // self.viewFor_bg_popup_add_hotel.isHidden = true
        self.viewForUserFollowUnfollow.isHidden = true
        self.tblVwFlight.isHidden = true
        self.txtDestination.isHidden = false
        self.viewForTravelActivity.isHidden = true
        self.viewForAlertCamera.isHidden = true
        self.picker?.delegate = self
        self.txtViewForCaution.delegate = self
        self.txtForWriteStatus.delegate = self
        self.viewForDatePicker.isHidden = true
        self.viewForSmilePopUp.isHidden = true
        flightSearchBar.tag = 1
        flightSearchBar.placeholder = "Select Destination"
        
        self.viewForSmilePopUpOnCameraAlert.isHidden = true
        //self.viewCornerDesign(viewForWriteStatus, masksToBounds: true, cornerRadius: 8, showInView: self.view)
        self.viewForWriteStatus.layer.masksToBounds = true
        self.viewForWriteStatus.layer.cornerRadius = 4
        self.viewForWriteStatus.layer.borderWidth = 1
        self.viewForWriteStatus.layer.borderColor = UIColor.lightGray.cgColor
        self.viewForWriteStatus.layer.shadowColor = UIColor.lightGray.cgColor
       
        isPlaying = false
        self.tblWriteStatus.estimatedRowHeight = 50
        self.tblWriteStatus.rowHeight = UITableViewAutomaticDimension
        self.tblWriteStatus.setNeedsLayout()
        self.tblWriteStatus.layoutIfNeeded()
        self.tblWriteStatus.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        txtDestination.isUserInteractionEnabled = true
        
        self.viewFollowersFollowingPost.layer.masksToBounds = true
        self.viewFollowersFollowingPost.layer.cornerRadius = 4
        self.viewUserPostTbl.layer.masksToBounds = true
        self.viewUserPostTbl.layer.cornerRadius = 4
        
        tblUserDetailFollowUnfollow.layer.masksToBounds = true
        tblUserDetailFollowUnfollow.layer.cornerRadius = 4
        self.tblUserDetailFollowUnfollow.estimatedRowHeight = 50
        self.tblUserDetailFollowUnfollow.rowHeight = UITableViewAutomaticDimension
        self.tblUserDetailFollowUnfollow.setNeedsLayout()
        self.tblUserDetailFollowUnfollow.layoutIfNeeded()
        self.tblUserDetailFollowUnfollow.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        userProfileImageOnUserDetail.layer.masksToBounds = true
        userProfileImageOnUserDetail.layer.cornerRadius = 25 //self.imgUserOnAlert.frame.height/2
        self.viewBtnContainer.layer.shadowColor = UIColor.gray.cgColor
        self.viewBtnContainer.layer.shadowOffset = CGSize(width: 0, height: 2);
        self.viewBtnContainer.layer.shadowOpacity = 1;
        self.viewBtnContainer.layer.shadowRadius = 1.0;
        
        self.txtDestination.inputView = pickerCountStatCity
        // self.txtDestination.suggestions =         //Date:03/07/2016
        
        leftBarBtn.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func chekIsLogin() {
        
        if let isForLogout  = UserDefaults.standard.value(forKey: "ForLogout") as? NSInteger {
            if isForLogout == 1 {
                UserDefaults.standard.set(0, forKey: "ForLogout")
                UserDefaults.standard.synchronize()
                let refreshAlert = UIAlertController(title: "Alert", message: "Do you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    AppTheme.setIsLogin(false)
                    BaseApp.sharedInstance.stopMonitoring()
                    let destVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(destVC, animated: true)
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                    print("Handle Cancel Logic here")
                }))
                present(refreshAlert, animated: true, completion: nil)
            }
        }
    }
    
    func setNavBarHidden(_ hidden : Bool) {
        self.navigationController?.setNavigationBarHidden(hidden, animated: true)
    }
    
    func compareNames(_ s1:String, _ s2:String) -> Bool //Sorting function for array of string
    {
        return s1 < s2
    }
    
    func popUpForDestination()    /// call method after destination data load
    {
        self.loading.dismiss()
        _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Please enter destination for flight service", showInView: self.view)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //=====================================
    //MARK:- UISearchBar delegate methods
    //=====================================
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if flightSearchBar.tag == 1{
            checkTableCell = 105
            searchActive = false;
        } else{
            checkTableCell = 100
            searchActive = false;
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if flightSearchBar.tag == 1 {
            searchActive = false;
            searchBar.resignFirstResponder()
        } else{
            searchActive = false;
            searchBar.resignFirstResponder()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if flightSearchBar.tag == 1 {
            searchActive = false;
            searchBar.resignFirstResponder()
        } else{
            searchActive = false;
            searchBar.resignFirstResponder()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if flightSearchBar.tag == 1 {
            
//            let resultPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
//
//            let swiftArray = NSArray(array: arrOfDestinatioStation)
//            let searchResults = swiftArray.filtered(using: resultPredicate)
//            arr_cityList = NSMutableArray(array:searchResults)
//            print(arr_cityList)
            
            let resultPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
            let swiftArray = NSArray(array: arrOfDestinatioStation)
            let searchResults = swiftArray.filtered(using: resultPredicate)

            filtered = NSMutableArray(array:searchResults)
            
            searchActive = true;
            checkTableCell = 105
            tblVwFlight.isHidden = false
            self.tblVwFlight.reloadData()
        } else{
            searchTextLength = searchText.count
            if searchTextLength >= 4 {
            }
        }
        self.view .bringSubview(toFront: tblVwFlight)
        tblVwFlight.isUserInteractionEnabled = true
    }
    
    //Search Hotels with searchbar fuction
    
    func callSearchHotelService(_ searchText: String)  {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        let parameters : [String: AnyObject] = ["search" : searchText as AnyObject ]
        
        AppTheme().callGetService(String(format: "%@hotel_autocomplete", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                if self.isSearchButtonClick == true {
                    //self.checkTableCell = 101
                    return
                }
                self.tblVwFlight.isHidden = false
                self.tblVwFlight.isUserInteractionEnabled = true
                self.loading.dismiss()
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if let array_temp : NSArray = respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_temp.mutableCopy() as! NSMutableArray
                }
                
                // self.checkTableCell = 100
                self.tblVwFlight.isUserInteractionEnabled = true
                self.view.bringSubview(toFront: self.tblVwFlight)
                self.tblVwFlight.reloadData()
            } else  {
                self.loading.dismiss()
                //  self.checkTableCell = 101
                // self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                self.tblVwFlight.isHidden = true
            }
        }
        
       
    }
    
    
    //========================================================
    // MARK: UITableview Datasource and delegate methods
    //========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var row : Int = Int()
        
        if tableView == tblVwFlight
        {
            if self.arrOfResponseKey.count != 0 && checkTableCell == 106  {
                if tableView == tblVwFlight  {
                    if button.tag == 1 && checkTableCell == 106 {
                        row = self.arrOfResponseKey.count
                    }
                    if button.tag == 3 {
                        row = self.arrOfResponseKey.count
                    } else{
                        row = self.arrOfResponseKey.count
                    }
                    return row
                }
            } else if checkTableCell == 105 {
                if filtered.count != 0{
                    row = filtered.count
                    return row
                }else{
                    return 0
                }
                
            } else if checkTableCell == 106 {
                
                if tableView == tblVwFlight{
                    if button.tag == 1 {
                        row = self.arrOfResponseKey.count
                    }
                    if button.tag == 3{
                        row = self.arrOfResponseKey.count
                    }else {
                        row = self.arrOfResponseKey.count
                    }
                    return row
                }
            } else if checkTableCell == 100{
                row = self.arrOfResponseKey.count
            } else if checkTableCell == 101{
                row = self.arrOfResponseKey.count
            } else{
                row = 0
            }
            return row
        }
        else if tableView == tblWriteStatus {
            isScrolling = false
            if self.arrOfResponseKey.count != 0{
                row = self.arrOfResponseKey.count * 2
            }else {
                row = 0
            }
            return row
        }
        else if tableView == tbl_city {
            
            if arr_cityList.count > 0 {
                row = arr_cityList.count
            }else{
                row = 0
            }
        } else if tableView == tbl_city_add_flight{
            if arr_cityList.count > 0 {
                row = arr_cityList.count
            } else {
                row = 0
            }
        }
        return row
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.isScrolling = false
        if tableView == tblVwFlight {
            
            if button.tag == 1 && checkTableCell == 106{
                
                self.loading.dismiss()
                
                button.tag = 1
                var cell : FlightCell! = tableView.dequeueReusableCell(withIdentifier: "flightCell", for: indexPath) as! FlightCell
                if cell == nil {
                    cell = FlightCell(style: UITableViewCellStyle.default, reuseIdentifier: "flightCell")
                }
                
                let view : UIView = cell.viewWithTag(111)!
                
                view.layer.cornerRadius = 5
                let temp_dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
                
                if let str_arrival_date_time : String = temp_dic["FLSArrivalDateTime"] as? String{
                    let strDestTime: String = str_arrival_date_time
                    let strSplit = strDestTime.characters.split(separator: "T")
                    cell.lblDestTime.text = String(strSplit.last!)
                }
                if let str_diparture_date_time : String = temp_dic["FLSDepartureDateTime"] as? String{
                    let strSourceTime: String = str_diparture_date_time
                    let strSourcTimeSplit = strSourceTime.characters.split(separator: "T")
                    cell.lblSourceTime.text = String(strSourcTimeSplit.last!)
                }
                if let str_airline_name : String = temp_dic["airline_name"] as? String{
                    cell.lblFlightName.text = str_airline_name
                }
                if let str_arrivalStation_name : String = temp_dic["arrival_station_name"] as? String{
                    cell.lblDestStation.text = str_arrivalStation_name
                }
                if let str_departStation_name : String = temp_dic["departure_station_name"] as? String{
                    cell.lblSourceStation.text = str_departStation_name
                }
                if let str_flightNum : String = temp_dic["flight_name"] as? String{
                    cell.lblFlighNumber.text = " FLIGHT \n \(str_flightNum)"
                }
                return cell
            }
            if button.tag == 3 {
                
                if checkTableCell == 100 {
                    
                    var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
                    
                    if cell == nil {
                        cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
                    }
                    let temp_dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
                    if arrOfResponseKey.count > indexPath.row {
                        if let str_hotel_name : String = temp_dic .value(forKey: "hotel_name") as? String
                        {
                            cell.titleLabel.text = str_hotel_name
                        }
                        if let str_hotel_city : String = temp_dic .value(forKey: "city") as? String
                        {
                            cell.cityLabel.text = str_hotel_city
                        }
                    }
                    return cell
                } else if checkTableCell == 101{
                    self.loading.dismiss()
                    var cell : HotelCell! = tableView.dequeueReusableCell(withIdentifier: "HotelCell", for: indexPath) as! HotelCell
                    if cell == nil {
                        cell = HotelCell(style: UITableViewCellStyle.default, reuseIdentifier: "HotelCell")
                    }
                    
                    let temp_dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
                    if self.arrOfResponseKey.count > indexPath.row{
                        if let str_hotel_name : String = temp_dic .value(forKey: "hotel_name") as? String {
                            cell.lblHotelName.text = str_hotel_name
                        }
                        if let str_hotel_city : String = temp_dic .value(forKey: "city") as? String{
                            cell.lblDiscreption.text = str_hotel_city
                        }
                        if let str_hotel_address : String = temp_dic .value(forKey: "address") as? String{
                            cell.lblAddress.text = str_hotel_address
                        }
                        if let str_hotel_pic : String = temp_dic .value(forKey: "hotel_pic") as? String  {
                            let url = URL(string:str_hotel_pic)
                            
                            cell.imgHotelLogo.kf.setImage(with: url!, placeholder: UIImage(named: "ic_hotel_orange.png"), options: nil, progressBlock: nil, completionHandler: {
                                (image, error, cacheType, imageURL) in
                                print(image?.description ?? "")
                            })
                            
                        }
                        return cell
                    } else{
                        let cell : FlightCell = tableView.dequeueReusableCell(withIdentifier: "flightCell", for: indexPath) as! FlightCell
                        
                        return cell
                    }
                } else {
                    self.loading.dismiss()
                    var cell : HotelCell! = tableView.dequeueReusableCell(withIdentifier: "HotelCell", for: indexPath) as! HotelCell
                    if cell == nil {
                        cell = HotelCell(style: UITableViewCellStyle.default, reuseIdentifier: "HotelCell")
                    }
                    if self.arrOfResponseKey.count > indexPath.row{
                        if let temp_dict : NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as? NSDictionary {
                            if let str_hotel_name : String = temp_dict .value(forKey: "hotel_name") as? String {
                                cell.lblHotelName.text = str_hotel_name
                            }
                            if let str_hotel_city : String = temp_dict .value(forKey: "city") as? String {
                                cell.lblDiscreption.text = str_hotel_city
                            }
                            if let str_hotel_address : String = temp_dict .value(forKey: "address") as? String {
                                cell.lblAddress.text = str_hotel_address
                            }
                            if let str_hotel_pic : String = temp_dict .value(forKey: "hotel_pic") as? String {
                                let url = URL(string:str_hotel_pic)
                                
                                cell.imgHotelLogo.kf.setImage(with: url!, placeholder: UIImage(named: "ic_hotel_orange.png"), options: nil, progressBlock: nil, completionHandler: {
                                    (image, error, cacheType, imageURL) in
                                    print(image?.description ?? "")
                                })
                                
                                //                                cell.imgHotelLogo.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_hotel_orange.png"), optionsInfo: nil , progressBlock: nil , completionHandler: { (image, error, cacheType, imageURL) in
                                //                                    print(image?.description)
                                //                                } )
                            }
                            return cell
                        }
                    } else{
                        let cell : FlightCell = tableView.dequeueReusableCell(withIdentifier: "flightCell", for: indexPath) as! FlightCell
                        return cell
                    }
                }
            }else if button.tag == 1 || checkTableCell == 105 {
                var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
                
                if cell == nil {
                    cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
                }
                cell.cityLabel.isHidden = true
                if(searchActive){
                    if filtered.count > indexPath.row {
                        cell.titleLabel.text = filtered.object(at: indexPath.row) as? String
                    }
                } else {
                    if arrOfDestinatioStation.count > indexPath.row {
                        cell.titleLabel.text = arrOfDestinatioStation.object(at: indexPath.row) as? String
                    }
                }
                return cell
            }else if  checkTableCell == 106{
                var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
                if cell == nil {
                    cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
                }
                cell.cityLabel.isHidden = true
                if(searchActive){
                    if filtered.count > indexPath.row {
                        cell.titleLabel.text = filtered.object(at: indexPath.row) as? String
                    }
                } else {
                    if arrOfDestinatioStation.count > indexPath.row {
                        cell.titleLabel.text = arrOfDestinatioStation.object(at: indexPath.row) as? String
                    }
                }
                return cell
            } else {
                let cell : FlightCell = tableView.dequeueReusableCell(withIdentifier: "flightCell", for: indexPath) as! FlightCell
                return cell
            }
        }
        // Call Tableview for User wall Post detail=============
        
        if tableView == tblWriteStatus {
            
            self.tblVwFlight.isHidden = true
            HomeVC.isUserDetailLike = false
            view.layer.cornerRadius = 5
            let temp_dic : NSDictionary  = self.arrOfResponseKey.object(at: indexPath.row/2) as! NSDictionary
            if indexPath.row % 2 == 1 {
                if let strTag : String = temp_dic["tag"] as? String {
                    if strTag == "advertise" {
                        
                        var cell : AdvertiseCell! = tableView.dequeueReusableCell(withIdentifier: "AdvertiseCell", for: indexPath) as! AdvertiseCell
                        if cell == nil {
                            cell = AdvertiseCell(style: UITableViewCellStyle.default, reuseIdentifier: "AdvertiseCell")
                        }
                        cell.btn_image.isHidden = true
                        cell.btn_more.isHidden = true
                        cell.layer.cornerRadius = 10 //set corner radius here
                        cell.layer.borderColor = UIColor.lightGray.cgColor  // set cell border color here
                        cell.layer.borderWidth = 2
                        if let str_hotel_post : String = temp_dic .value(forKey: "posts") as? String{
                            cell.lblOffer.text = str_hotel_post
                        }
                        if let str_disc = temp_dic .value(forKey: "description") as? String {
                            if str_disc == "" {
                                cell.lbl_dis.isHidden = true
                                cell.lbl_dis.numberOfLines = 1
                            }else{
                                cell.btn_more.isHidden = false
                                cell.lbl_dis.isHidden = false
                                if shouldCellBeExpanded == false {
                                    cell.btn_more.setImage(UIImage(named: "img_expand.png"), for: UIControlState())
                                    cell.lbl_dis.numberOfLines = 2
                                    cell.lbl_dis.text = str_disc
                                }else{
                                    shouldCellBeExpanded = true
                                    cell.btn_more.setImage(UIImage(named: "img_short.png"), for: UIControlState())
                                    cell.lbl_dis.numberOfLines = 0
                                    cell.lbl_dis.text = str_disc
                                }
                            }
                        }
                        
                        cell.btn_more.addTarget(self, action: #selector(HomeVC.moreAction(_:)), for: UIControlEvents.touchUpInside)
                        cell.btn_more.tag = indexPath.row
                        if let str_profil = temp_dic .value(forKey: "profile_pic") as? String {
                            let url = URL(string:str_profil)
                            if verifyUrl(str_profil) == true {
                                
                                cell.img_profil_pic.kf.setImage(with: url!, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: {
                                    (image, error, cacheType, imageURL) in
                                    print(image?.description ?? "")
                                })
                            }
                        }
                        if let str_file_type = temp_dic .value(forKey: "file_type") as? String {
                            if str_file_type == "text" {
                                cell.imageHeight.constant = 0
                                cell.viewHeightConstraint.constant = 0
                                
                            } else {
                                if let str_file_url : String = temp_dic .value(forKey: "file_name") as? String{
                                    
                                    let url = URL(string:str_file_url)
                                    
                                    if verifyUrl(str_file_url) == true {
                                        
                                        if str_file_type == "video" {
                                            
                                            cell.imgAdd.isHidden = true
                                            cell.btn_image.isHidden = true
                                            cell.view_video.isHidden = false
                                            cell.btn_video.isHidden = false
                                            cell.btn_video.bringSubview(toFront: cell.view_video)
                                            cell.btn_video.addTarget(self, action: #selector(HomeVC.videoTapped(_:)), for: .touchUpInside)
                                            cell.btn_video.tag = indexPath.row
                                            
                                        } else{
                                            cell.view_video.isHidden = true
                                            cell.btn_image.isHidden = false
                                            cell.imgAdd.bringSubview(toFront: cell)
                                            cell.imgAdd.isHidden = false
                                            cell.imageHeight.constant = 81
                                            cell.btn_image.addTarget(self, action: #selector(imageTapped(_:)), for: .touchUpInside)
                                            cell.btn_image.tag = indexPath.row
                                            
                                            let url = url
                                            
                                            if str_file_url.contains(AppurlforImage) {
                                                
                                                cell.imgAdd.kf.setImage(with: url!, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: {
                                                    (image, error, cacheType, imageURL) in
                                                    print(image?.description ?? "")
                                                })
                                                
                                            } else {
                                                
                                                let filePath = BaseApp.sharedInstance.getLocalMediaFileFolderPath(imageName: str_file_url)?.path
                                                let imgAdd =  BaseApp.sharedInstance.getImage(filepath:  filePath!)
                                                cell.imgAdd.image = imgAdd
                                            }
                                            
                                        }
                                    } else {
                                        
                                        if str_file_type == "video" {
                                            
                                            cell.imgAdd.isHidden = true
                                            cell.btn_image.isHidden = true
                                            cell.view_video.isHidden = false
                                            cell.btn_video.isHidden = false
                                            cell.btn_video.bringSubview(toFront: cell.view_video)
                                            cell.btn_video.addTarget(self, action: #selector(HomeVC.videoTapped(_:)), for: .touchUpInside)
                                            cell.btn_video.tag = indexPath.row
                                            
                                        } else {
                                            
                                            cell.view_video.isHidden = true
                                            cell.btn_image.isHidden = false
                                            cell.imgAdd.bringSubview(toFront: cell)
                                            cell.imgAdd.isHidden = false
                                            cell.imageHeight.constant = 81
                                            cell.btn_image.addTarget(self, action: #selector(imageTapped(_:)), for: .touchUpInside)
                                            cell.btn_image.tag = indexPath.row
                                            
                                            let url = url
                                            
                                            if str_file_url.contains(AppurlforImage) {
                                                
                                                cell.imgAdd.kf.setImage(with: url!, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: {
                                                    (image, error, cacheType, imageURL) in
                                                    print(image?.description ?? "")
                                                })
                                                
                                            } else {
                                                let filePath = BaseApp.sharedInstance.getLocalMediaFileFolderPath(imageName: str_file_url)?.path
                                                let imgAdd =  BaseApp.sharedInstance.getImage(filepath:  filePath!)
                                                cell.imgAdd.image = imgAdd
                                                // cell.imgAdd.image = UIImage(contentsOfFile: str_file_url)
                                            }
                                        }
                                    }
                                } else {
                                    cell.imageHeight.constant = 0
                                    cell.viewHeightConstraint.constant = 0
                                }
                            }
                        }
                        return cell
                        
                    } else {
                        
                        // ** Table view cell for Post
                        var cell : WriteStatusCell! = tableView.dequeueReusableCell(withIdentifier: "WriteStatusCell", for: indexPath) as! WriteStatusCell
                        if cell == nil {
                            cell = WriteStatusCell(style: UITableViewCellStyle.default, reuseIdentifier: "WriteStatusCell")
                        }
                        if let str_user_name : String = temp_dic .value(forKey: "username") as? String {
                            cell.lblUserName.text = str_user_name
                        }
                        cell.btn_image_post.isHidden = true
                        cell.btn_video_post.isHidden = true
                        
                        if let str_posts : String = temp_dic .value(forKey: "posts") as? String{
                            cell.lblStatus.text = str_posts
                            cell.lblStatus.hashtagLinkTapHandler = {(label:KILabel,string:String?,range: NSRange?) -> ()
                                in
                                let objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "HashTagController") as! HashTagController
                                print(indexPath.row)
                                objFlightDetailVC.sr_search_word = string
                                self.navigationController?.pushViewController(objFlightDetailVC, animated: true)
                            }
                            
                            cell.lblStatus.userHandleLinkTapHandler = {(label:KILabel,string:String?,range: NSRange?) -> ()
                                in
                                if ((string?.contains("@")) != nil) {
                                    var str : String! = String()
                                    str = string!.replacingOccurrences(of: "@", with: "", options: NSString.CompareOptions.literal, range: nil)
                                    self.getUserInfoOnTag("", str_user_name: str)
                                }
                            }
                        }
                        if let str_total_like : String = temp_dic .value(forKey: "total_like") as? String {
                            cell.lblTotalLike.text = str_total_like
                        }
                        if let str_total_comment : String = temp_dic .value(forKey: "total_comment") as? String {
                            cell.lblTotalComments.text = str_total_comment
                        }
                        cell.layer.cornerRadius=10 //set corner radius here
                        cell.layer.borderColor = UIColor.lightGray.cgColor  // set cell border color here
                        cell.layer.borderWidth = 2
                        
                        cell.btnLike.tag = indexPath.row
                        cell.btnComment.tag = indexPath.row
                        cell.btnUserFollowUnfollow.tag = indexPath.row
                        cell.btnShare.tag = indexPath.row
                        
                        if let strRating: String = temp_dic["like_by_me"] as? String{
                            likeByMe = NSInteger(strRating)!
                        } else if let strRating: NSInteger = temp_dic["like_by_me"] as? NSInteger{
                            likeByMe = strRating
                        }
                        if likeByMe == 1 {
                            cell.btnLike.setImage(UIImage(named: "img_like_on.png"), for: UIControlState())
                        } else {
                            cell.btnLike.setImage(UIImage(named: "img_dislike.png"), for: UIControlState())
                        }
                        if let str_flight_id : String = temp_dic .value(forKey: "flight_id") as? String  {
                            flight_Id = NSInteger(str_flight_id)
                        }
                        if let str_post_Id : String = temp_dic .value(forKey: "post_id") as? String  {
                            post_Id = NSInteger(str_post_Id)
                        }
                        if let str_CommentOnOff : String = temp_dic.value(forKey: "commentsoff") as? String  {
                            commentOnOffInt = NSInteger(str_CommentOnOff)
                        }
                        
                        cell.btnShare.addTarget(self, action: #selector(HomeVC.shareAction(_:)), for: UIControlEvents.touchUpInside)
                        cell.btnLike.addTarget(self, action: #selector(HomeVC.likeDislikeAction(_:)), for: UIControlEvents.touchUpInside)
                        cell.btnUserFollowUnfollow.addTarget(self, action: #selector(HomeVC.btnUserFollowUnfollow(_:)), for: .touchUpInside)
                        cell.btnLike.addTarget(self, action: #selector(HomeVC.likeDislikeAction(_:)), for: UIControlEvents.touchUpInside)
                        
                        if let str_user_id : String = temp_dic["user_id"] as? String {
                            
                            if let str_logged_user_id : String = AppTheme.getLoginDetails().value(forKey: "id") as? String  {
                                
                                if (str_user_id == str_logged_user_id) {
                                    
                                    cell.btnCommentOnOff.isHidden = false
                                    cell.btnCommentOnOff.isUserInteractionEnabled = true
                                    
                                    cell.deleteButton.tag = indexPath.row
                                    cell.btnCommentOnOff.tag = indexPath.row
                                    
                                    cell.deleteButton.isHidden = false
                                    cell.deleteButton.isUserInteractionEnabled = true
                                    cell.deleteButton.addTarget(self, action: #selector(HomeVC.deletePost(_:)), for: .touchUpInside)
                                    
                                    if commentOnOffInt == 1 {
                                        cell.btnCommentOnOff.setImage(UIImage(named: "chatIconGrey") as UIImage!, for: .normal)
                                    } else {
                                        cell.btnCommentOnOff.setImage(UIImage(named: "chatIconYellow") as UIImage!, for: .normal)
                                    }
                                    cell.btnCommentOnOff.addTarget(self, action: #selector(HomeVC.commentActionTableCell(_:)), for: UIControlEvents.touchUpInside)
                                    
                                } else{
                                    cell.btnCommentOnOff.isHidden = true
                                    cell.deleteButton.isHidden = true
                                    cell.deleteButton.isUserInteractionEnabled = false
                                    cell.btnCommentOnOff.isUserInteractionEnabled = false
                                    
                                }
                            }
                        }
                        
                        if let str_profile_pic : String = temp_dic.value(forKey: "profile_pic") as? String {
                            let url = URL(string:str_profile_pic)
                            if url != nil{
                                cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "addprofilepic.png"), options: nil, progressBlock: nil, completionHandler: {
                                    (image, error, cacheType, imageURL) in
                                    print(image?.description ?? "")
                                })
                            }
                            
                        }
                        var str_time_server : String!
                        var str_date_server : String!
                        //   let strFileType: String!
                        if let str_time : String = temp_dic .value(forKey: "time") as? String {
                            str_time_server = str_time
                            if let str_date : String = temp_dic .value(forKey: "date") as? String {
                                str_date_server = str_date
                                cell.lblDateTime.text = "\(str_time_server!)  \(str_date_server!)"
                            }
                        }
                        if let str_f_type =  temp_dic .value(forKey: "file_type") as? String{
                            if str_f_type == "text" {
                                cell.imageHeight.constant = 0
                                cell.viewHeightConstraint.constant = 0
                            } else {
                                if let str_file_url : String = temp_dic.value(forKey: "file_name") as? String{
                                    let url = URL(string:str_file_url)
                                    if verifyUrl(str_file_url) == true {
                                        
                                        if str_f_type == "video" {
                                            cell.btn_video_post.isHidden = false
                                            cell.btn_image_post.isHidden = true
                                            cell.btn_video_post.addTarget(self, action: #selector(HomeVC.videoTapped(_:)), for: .touchUpInside)
                                            cell.btn_video_post.tag = indexPath.row
                                        } else{
                                            cell.viewPlayer.isHidden = true
                                            cell.imgAdd.isHidden = false
                                            cell.btn_video_post.isHidden = true
                                            cell.btn_image_post.isHidden = false
                                            cell.imageHeight.constant = 81
                                            cell.btn_image_post.addTarget(self, action: #selector(imageTapped(_:)), for: .touchUpInside)
                                            cell.btn_image_post.tag = indexPath.row
                                            
                                            let url = url
                                            
                                            if str_file_url.contains(AppurlforImage) {
                                                
                                                cell.imgAdd.kf.setImage(with: url!, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: {
                                                    (image, error, cacheType, imageURL) in
                                                    print(image?.description ?? "")
                                                })
                                                
                                            } else {
                                                let filePath = BaseApp.sharedInstance.getLocalMediaFileFolderPath(imageName: str_file_url)?.path
                                                let imgAdd =  BaseApp.sharedInstance.getImage(filepath:  filePath!)
                                                cell.imgAdd.image = imgAdd
                                                // cell.imgAdd.image = UIImage(contentsOfFile: str_file_url)
                                            }
                                        }
                                    } else {
                                        
                                        if str_f_type == "video" {
                                            
                                            cell.btn_video_post.isHidden = false
                                            cell.btn_image_post.isHidden = true
                                            cell.btn_video_post.addTarget(self, action: #selector(HomeVC.videoTapped(_:)), for: .touchUpInside)
                                            cell.btn_video_post.tag = indexPath.row
                                            
                                        } else {
                                            
                                            cell.viewPlayer.isHidden = true
                                            cell.imgAdd.isHidden = false
                                            cell.btn_video_post.isHidden = true
                                            cell.btn_image_post.isHidden = false
                                            cell.imageHeight.constant = 81
                                            cell.btn_image_post.addTarget(self, action: #selector(imageTapped(_:)), for: .touchUpInside)
                                            cell.btn_image_post.tag = indexPath.row
                                            
                                            let url = url
                                            
                                            if str_file_url.contains(AppurlforImage) {
                                                
                                                cell.imgAdd.kf.setImage(with: url!, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: {
                                                    (image, error, cacheType, imageURL) in
                                                    print(image?.description ?? "")
                                                })
                                                
                                            } else {
                                                let filePath = BaseApp.sharedInstance.getLocalMediaFileFolderPath(imageName: str_file_url)?.path
                                                let imgAdd =  BaseApp.sharedInstance.getImage(filepath:  filePath!)
                                                cell.imgAdd.image = imgAdd
                                            }
                                        }
                                    }
                                } else {
                                    cell.imageHeight.constant = 0
                                    cell.viewHeightConstraint.constant = 0
                                }
                            }
                        }
                        var rating : NSInteger = 0
                        if let strRating: String = temp_dic["rating"] as? String{
                            if strRating == ""{
                            } else{
                                rating = NSInteger(strRating)!
                            }
                        } else if let strRating: NSInteger = temp_dic["rating"] as? NSInteger{
                            rating = strRating
                        }
                        if rating == 0 {
                            cell.imgSmileStatus.image = UIImage(named: "")
                        }
                        if rating == 1    {
                            cell.imgSmileStatus.image = UIImage(named: "good_on.png")
                        }
                        else if rating == 2 {
                            cell.imgSmileStatus.image = UIImage(named: "bad_on.png")
                        } else if rating == 3 {
                            cell.imgSmileStatus.image = UIImage(named: "bad.png")
                        }
                        return cell
                    }
                }
                if indexPath.row == arrOfResponseKey.count - 1 { // last cell
                    if arrOfRefreshed.count > arrOfResponseKey.count { // more items to fetch
                        //                        loadItem() // increment `fromIndex` by 20 before server call
                    }
                }
                
            } else {
                var cell = tableView .dequeueReusableCell(withIdentifier: "cell")
                if cell == nil {
                    cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
                }
                cell!.isUserInteractionEnabled = false
                cell?.backgroundColor = UIColor.clear
                return cell!
            }
        }
        
        
        // Call Tableview for City selection for Departure Arrival detail=============
        if tableView == tbl_city{
            var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
            if cell == nil {
                cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
            }
            let dic: NSDictionary = self.arr_cityList.object(at: indexPath.row) as! NSDictionary
            if let str_city_name  = dic .value(forKey: "location") as? String{
                cell.titleLabel.text = str_city_name
            }
            return cell
        }
        if tableView == tbl_city_add_flight{
            var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
            if cell == nil {
                cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
            }
            if textfield_common.tag == 102 {
                let dic: NSDictionary = self.arr_cityList.object(at: indexPath.row) as! NSDictionary
                if let str_city_name  = dic .value(forKey: "name") as? String{
                    if let str_country_code = dic .value(forKey: "country_code") as? String {
                        cell.titleLabel.text = "\(str_city_name) \(str_country_code)"
                    }
                }
            }else{
                if(searchActive){
                    if arr_cityList.count > indexPath.row {
                        cell.titleLabel.text = arr_cityList.object(at: indexPath.row) as? String
                    }
                } else {
                    if arrOfDestinatioStation.count > indexPath.row {
                        cell.titleLabel.text = arrOfDestinatioStation.object(at: indexPath.row) as? String
                    }
                }
            }
            return cell
        } else {
            var cell = tableView .dequeueReusableCell(withIdentifier: "cell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            }
            cell!.isUserInteractionEnabled = false
            cell?.backgroundColor = UIColor.clear
            return cell!
        }
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblVwFlight {
            if button.tag == 1 && checkTableCell == 106 {
                var cell: FlightCell! = tableView.dequeueReusableCell(withIdentifier: "FlightCell") as? FlightCell
                if cell == nil {
                    cell = FlightCell(style: UITableViewCellStyle.default, reuseIdentifier: "FlightCell")
                }
                return tblVwFlight.rowHeight
            }
            if button.tag == 3 {
                if checkTableCell == 100 {
                    var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
                    if cell == nil {
                        cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
                    }
                    return 60.0
                }
                var cell: HotelCell! = tableView.dequeueReusableCell(withIdentifier: "HotelCell") as? HotelCell
                if cell == nil {
                    cell = HotelCell(style: UITableViewCellStyle.default, reuseIdentifier: "HotelCell")
                }
                return tblVwFlight.rowHeight
            }
            if button.tag == 4 &&  checkTableCell == 106{
                
                var cell: CommonSearchCell! = tableView.dequeueReusableCell(withIdentifier: "CommonSearchCell") as? CommonSearchCell
                if cell == nil {
                    cell = CommonSearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "CommonSearchCell")
                }
                return 45.0
            }
            if button.tag == 0 && checkTableCell == 106{
                
                var cell: FlightCell! = tableView.dequeueReusableCell(withIdentifier: "FlightCell") as? FlightCell
                if cell == nil {
                    cell = FlightCell(style: UITableViewCellStyle.default, reuseIdentifier: "FlightCell")
                }
                return tblVwFlight.rowHeight
            } else {
                return 45.0
            }
            
        } else if tableView == tblWriteStatus {
            
            var cell: WriteStatusCell! = tableView.dequeueReusableCell(withIdentifier: "WriteStatusCell") as? WriteStatusCell
            if cell == nil {
                cell = WriteStatusCell(style: UITableViewCellStyle.default, reuseIdentifier: "WriteStatusCell")
            }
            
            btnCommentOnOff.isHidden = false
            tblWriteStatus.backgroundColor = UIColor.clear
            tblWriteStatus.separatorStyle = UITableViewCellSeparatorStyle.none
            tblWriteStatus.estimatedRowHeight = 100
            tblWriteStatus.rowHeight = UITableViewAutomaticDimension
            
            if indexPath.row%2 == 0 {
                return 10
            }
            return  tblWriteStatus.rowHeight
        } else{
            var cell: WriteStatusCell! = tableView.dequeueReusableCell(withIdentifier: "WriteStatusCell") as? WriteStatusCell
            if cell == nil {
                cell = WriteStatusCell(style: UITableViewCellStyle.default, reuseIdentifier: "WriteStatusCell")
            }
            tblUserDetailFollowUnfollow.estimatedRowHeight = 100
            tblUserDetailFollowUnfollow.rowHeight = UITableViewAutomaticDimension
            return tblUserDetailFollowUnfollow.rowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if tableView == tblVwFlight {
            
            if button.tag == 1  && checkTableCell == 106 {
                
                if (!isNetworkAvailable) {
                    Utility.showNetWorkAlert()
                    return
                }
                
                self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
                self.loading.textLabel.text = "Loading"
                self.loading.show(in: self.view)
                
                let objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "FlightDetailVC") as! FlightDetailVC
                let dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
                if let str_flight_number = dic["FlightNumber"] as? String {
                    
                    let parameters : [String: AnyObject] =
                        [
                            "flight_id" : str_flight_number as AnyObject,
                            "from"      : self.strSourceCode as AnyObject,
                            "to"        : self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.flightSearchBar.text!)) as AnyObject,
                            "user_id"   : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject
                        ]
                    
                    AppTheme().callGetService(String(format: "%@getFlight", AppUrl), param: parameters) {(result, data) -> Void in
                        if result == "success" {
                            
                            self.flightSearchBar.resignFirstResponder()
                            self.loading.dismiss()
                            
                            var dictTemp: NSDictionary = NSDictionary()
                            var dataForAirlineFlightUser : NSDictionary = NSDictionary()
                            dataForAirlineFlightUser = data as! NSDictionary
                            var dicData: NSDictionary = NSDictionary()
                            dicData = dataForAirlineFlightUser["response"] as! NSDictionary
                            
                            var testArr : NSArray = NSArray()
                            if let array_airlines : NSArray = Utility.getValueForObject(dicData["airlines"] as! NSArray) as? NSArray{
                                testArr = array_airlines
                            }
                            
                            let objAirlineDetailModel: AirLinesDetails = AirLinesDetails()
                            for airlines in testArr
                            {
                                dictTemp = airlines as! NSDictionary
                            }
                            if let str_airline_name : String = dictTemp .value(forKey: "airline_name") as? String{
                                objAirlineDetailModel.airlineName = str_airline_name
                            }
                            if let str_airline_pic : String = dictTemp["airline_pic"] as? String{
                                objAirlineDetailModel.airlineImage = str_airline_pic as NSString
                            }
                            if let str_about : String = dictTemp["about"] as? String{
                                objAirlineDetailModel.about = str_about
                            }
                            if let arr_for_airline : NSArray = Utility.getValueForObject(dicData["airlines"] as! NSArray) as? NSArray {
                                objFlightDetailVC.arrForAirlines = arr_for_airline.mutableCopy() as! NSMutableArray
                            }
                            if let arr_for_flight : NSArray = Utility.getValueForObject(dicData["flights"] as! NSArray) as? NSArray {
                                objFlightDetailVC.arrForFlight = arr_for_flight.mutableCopy() as! NSMutableArray
                            }
                            if let dict_for_user_mutable : NSDictionary = Utility.getValueForObject(dicData["users"] as! NSDictionary) as? NSDictionary {
                                objFlightDetailVC.dictForUser = dict_for_user_mutable
                            }
                            var airLineId: NSInteger = 0
                            if let strID : String = dictTemp["airline_id"] as? String
                            {
                                airLineId = NSInteger(strID)!
                            }
                            else if let ailineID : NSInteger = dictTemp["airline_id"] as? NSInteger
                            {
                                airLineId = ailineID
                            }
                            self.navigationController?.pushViewController(objFlightDetailVC, animated: true)
                        }else {
                            self.loading.dismiss()

                            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:  "Sorry there is no flight details available!", showInView: self.view)
                            self.navigationController?.presentedViewController
                        }
                    }
                }
            } else if button.tag == 3{
                self.loading.dismiss()

                if checkTableCell == 100 {
                    
                    checkTableCell = 101
                    tblVwFlight.isHidden = true
                    if let dict_temp : NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as? NSDictionary{
                        var str_hotelName : String! // = <#value#>
                        var str_hotelCity : String!
                        if let str_hotel_name : String = dict_temp .value(forKey: "hotel_name") as? String{
                            str_hotelName = str_hotel_name
                            if let str_hotel_city : String = dict_temp .value(forKey: "city") as? String{
                                str_hotelCity = str_hotel_city
                                flightSearchBar.text = "\(str_hotelName) - \(str_hotelCity)"
                                getHotelList(str_hotelName, city_name: str_hotelCity)
                            }
                        }
                    }
                } else{
                    ///// ipoi
                    if let dict_temp : NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as? NSDictionary  {
                        
                        let str_hotelId : String!
                        let str_hotelCity : String!
                        
                        if let str_Hotel_Id : String = dict_temp .value(forKey: "hotel_Id") as? String{
                            str_hotelId = str_Hotel_Id
                            if let str_hotel_city : String = dict_temp .value(forKey: "city") as? String{
                                str_hotelCity = str_hotel_city
                                let objHotelDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "HotailDetailVC") as! HotailDetailVC
                                objHotelDetailVC.str_hotel_id = str_Hotel_Id
                                objHotelDetailVC.str_hotel_city = str_hotel_city
                                self.navigationController?.pushViewController(objHotelDetailVC, animated: true)
                                //                                getSelectedHotelDetail(str_hotelId , city_name: str_hotelCity)
                            }
                        }
                    }
                }
            } else if checkTableCell == 105 {
                
                tblVwFlight.isHidden = true
                self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
                self.loading.textLabel.text = "Loading"
                self.loading.show(in: self.view)
                
                if(searchActive){
                    if filtered.count > indexPath.row {
                        flightSearchBar.text = filtered.object(at: indexPath.row) as? String
                        
                        if self.flightSearchBar.text == "" {
                            self.loading.dismiss()
                            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select destination!", showInView: self.view)
                            self.tblVwFlight.isHidden = true
                        }else {
                            checkTableCell = 106
                            if (!isNetworkAvailable) {
                                self.loading.dismiss()
                                return
                            }
                            getFligtList()
                        }
                    }
                } else {
                    if arrOfDestinatioStation.count > indexPath.row {
                        flightSearchBar.text = arrOfDestinatioStation.object(at: indexPath.row) as? String
                        if self.flightSearchBar.text == ""
                        {
                            self.loading.dismiss()
                            _  = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select destination!", showInView: self.view)
                            self.tblVwFlight.isHidden = true
                        }else{
                            if (!isNetworkAvailable) {
                                Utility.showNetWorkAlert()
                                return
                            }
                            getFligtList()
                        }
                    } else{
                        loading.dismiss()
                    }
                }
            }
        } else if tableView == tblWriteStatus {
            
            self.loading.dismiss()

            var tag : Int!
            tag = indexPath.row
            
            if tag >= 1000 {
                tag = tag - 1000
                FeedsCommentVC.isFromUser = true
            } else {
                FeedsCommentVC.isFromUser = false
            }
                if let dict_temp : NSDictionary = self.arrOfResponseKey.object(at: indexPath.row/2) as? NSDictionary
                {
                    if indexPath.row % 2 == 1 {
                        
                        if let strTag : String = dict_temp["tag"] as? String{
                            if strTag == "advertise"{
                                if let url_open =  dict_temp .value(forKey: "url") as? String{
                                    if verifyUrl(url_open) == true {
                                        UIApplication.shared.openURL(URL(string: url_open)!)
                                    }
                                }
                            } else {
                                let objCommentVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedsCommentVC") as! FeedsCommentVC
                                objCommentVC.dicData = dict_temp
                                if let str_CommentOnOff : String = dict_temp.value(forKey: "commentsoff") as? String  {
                                    commentOnOffInt = NSInteger(str_CommentOnOff)
                                }
                                if commentOnOffInt == 1 {
                                    objCommentVC.isShowComment = false
                                } else {
                                    objCommentVC.isShowComment = true
                                }
                                objCommentVC.strPostsId = dict_temp .value(forKey: "post_id") as? String
                                self.navigationController?.pushViewController(objCommentVC, animated: true)
                            }
                        }
                    }
                }
          
        } else if tableView == tbl_city_add_flight {
            
            if  textfield_common.tag == 101 || textfield_common.tag == 100 {
                if(searchActive) {
                    
                    if arr_cityList.count > indexPath.row {
                        
                        if textfield_common.tag == 100 {
                            
                            txtDepartureCity_add_flight.text = arr_cityList.object(at: indexPath.row) as? String
                            departure_city_code_addFlight = self.arrOfDestinatioStationCode.object(at:self.arrOfDestinatioStation.index(of: self.txtDepartureCity_add_flight.text!)) as! String
                            var myStringArr =  txtDepartureCity_add_flight.text!.components(separatedBy: ",")
                            departure_city_airport_addFlight = myStringArr [0]
                            
                        } else if textfield_common.tag == 101 {
                            
                            txtArrivalCity_add_flight.text = arr_cityList.object(at: indexPath.row) as? String
                            arrival_city_code_addFlight = self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.txtArrivalCity_add_flight.text!)) as! String
                            var myStringArr =  txtArrivalCity_add_flight.text!.components(separatedBy: ",")
                            arrival_city_airport_addFlight = myStringArr [0]
                        }
                    }
                } else {
                    if arrOfDestinatioStation.count > indexPath.row {
                        
                        if textfield_common.tag == 100 {
                            
                            txtDepartureCity_add_flight.text = arrOfDestinatioStation.object(at: indexPath.row) as? String
                            departure_city_code_addFlight = self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.txtDepartureCity_add_flight.text!)) as! String
                            var myStringArr =  txtDepartureCity_add_flight.text!.components(separatedBy: ",")
                            departure_city_airport_addFlight = myStringArr [1]
                            
                        } else if textfield_common.tag == 101 {
                            
                            txtArrivalCity_add_flight.text = arrOfDestinatioStation.object(at: indexPath.row) as? String
                            var myStringArr =  txtDepartureCity_add_flight.text!.components(separatedBy: ",")
                            arrival_city_airport_addFlight = myStringArr [1]
                            arrival_city_code_addFlight = self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.txtArrivalCity_add_flight.text!)) as! String
                        }
                    }
                }
            } else if textfield_common.tag == 102 {
                
               // self.tbl_city.reloadData()
                let dic: NSDictionary = self.arr_cityList.object(at: indexPath.row) as! NSDictionary
                if let str_city_name  = dic .value(forKey: "name") as? String{
                    txtAirlineName_add_flight.text = str_city_name
                }
                if let str_airline_id = dic .value(forKey: "id") as? String{
                    airline_id_selected = str_airline_id
                }
            }
            viewForCity_Flight_add.isHidden = true
            
        } else  if tableView == tbl_city {
            
            let dic: NSDictionary = self.arr_cityList.object(at: indexPath.row) as! NSDictionary
            if let str_city_name  = dic .value(forKey: "location") as? String{
                txt_city.text = str_city_name
                viewForCity_add_hotel.isHidden = true
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == tblWriteStatus {
            
            let lastElement = (self.arrOfResponseKey.count * 2) - 1
            let firstElement = (self.arrOfResponseKey.count * 2) - (self.arrOfResponseKey.count * 2 - 1)
            if indexPath.row == lastElement {
                self.loading.dismiss()
                print(lastElement)
                let dic = self.arrOfResponseKey.object(at: self.arrOfResponseKey.count - 1) as! NSDictionary
                
                if let str_bottom_postId  = dic.value(forKey: "post_id") as? String{
                    isScrolling = true
                    getUserStatusList("", str_bottom: str_bottom_postId, str_limit: "", str_offset: "",str_add_offset: isAddCalled)
                    isAddCalled += 1
                }
            } else if indexPath.row == firstElement - 1 {
                
                let dic = self.arrOfResponseKey.object(at: firstElement - 1) as! NSDictionary
                print(dic)
            }
        }
    }
    
    // Mark - API Call
    
    func commentOnOffAction_ApiCall(status : String ,postID : String , selectedIndex: Int , dataDict : NSDictionary) {
        
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
        
        if (!isNetworkAvailable) {
            self.loading.dismiss()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(postID, forKey:"values")
        paraDict.setValue(status as AnyObject , forKey:"sts")
        print(paraDict)
        
        AppTheme().callPostService(String(format:"%@onoffcomments", AppUrlNew), param: paraDict) {(result, data) -> Void in                                                                                  ///Call services
            
            if result == "success" {
                
                self.loading.dismiss()
                
                let temp_Dict: NSMutableDictionary  = dataDict.mutableCopy() as! NSMutableDictionary
                
                if status == "0" {
                   temp_Dict.setValue("0", forKey: "commentsoff")
                } else {
                    temp_Dict.setValue("1", forKey: "commentsoff")
                }
                temp_Dict.setValue(postID, forKey: "post_id")

                self.arrOfResponseKey.replaceObject(at: selectedIndex, with: temp_Dict)
                self.tblWriteStatus.reloadData()

            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "", showInView: self.view)
            }
        }
    }
    
    func getSelectedHotelDetail(_ hotel_Id: String, city_name: String) {
        
        if (!isNetworkAvailable) {
            self.loading.dismiss()
            return
        }
       
        let objHotelDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "HotailDetailVC") as! HotailDetailVC
        
        let parameters : [String: AnyObject] =
                                                ["hotel_id" : hotel_Id as AnyObject,
                                                  "user_id" : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject ]
        
        AppTheme().callGetService(String(format: "%@GetHotel", AppUrl), param: parameters) {(result, data) -> Void in                                            ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                var dataForAirlineFlightUser : NSDictionary = NSDictionary()
                dataForAirlineFlightUser = data as! NSDictionary
                
                if let dicData = dataForAirlineFlightUser["response"] as? NSDictionary
                {
                    objHotelDetailVC.arrForHotel = Utility.getValueForObject(dicData["hotel"] as! NSArray) as! NSArray
                    
                    if let tempDic: NSMutableDictionary = dicData["users"] as?  NSMutableDictionary
                    {
                        objHotelDetailVC.dictForUserDetail = tempDic
                    }
                    else if let tempArr: NSMutableArray = dicData["users"] as?  NSMutableArray
                    {
                        objHotelDetailVC.arrForUserDetail = Utility.getValueForObject(tempArr) as? NSArray
                    }
                     objHotelDetailVC.str_hotel_id = hotel_Id
               
                    self.navigationController?.pushViewController(objHotelDetailVC, animated: true)
                    
                    self.txt_Name_Used_Booking.text = ""
                    self.txt_Hotel_name.text = ""
                    self.txt_booking_reference.text = ""
                    self.textview_description.text = ""
                    self.lbl_select_image.text = "Select image (Optional)."
                    self.viewFor_bg_popup_add_hotel.isHidden = true

                    self.view_container_for_review_popup_add_hotel.isHidden = true
                    self.view_container_for_checkInCheckOut_add_hotel.isHidden = true
                }
            } else {
                self.loading.dismiss()

                self.navigationController?.pushViewController(objHotelDetailVC, animated: true)
                
            }
        }
    }
    
    
    //Check url nil or not
    func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if urlString == "" || urlString.isEmpty{
                return false
            } else {
                if let url = URL(string: urlString) {
                    // check if your application can open the NSURL instance
                    return UIApplication.shared.canOpenURL(url)
                }
            }
        }
        return false
    }
    
    //==========================================
    // MARK: UITextfield delegate methods
    //==========================================
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtDestination {
            
            self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
            self.loading.textLabel.text = "Loading"
            self.loading.show(in: self.view)
            
            if self.txtDestination.text == "" {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select destination!", showInView: self.view)
                self.tblVwFlight.isHidden = true
                
            } else {
                button.tag = 1
                let parameters : [String: AnyObject] = ["from" : self.strSourceCode as AnyObject,
                                                        "to"   : self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.txtDestination.text!)) as AnyObject ]
                
                AppTheme().callGetService(String(format: "%@flights", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
                    
                    if result == "success"
                    {
                        var respDict : NSMutableDictionary = NSMutableDictionary()
                        respDict = data as! NSMutableDictionary
                        self.arrOfResponseKey = respDict["response"] as! NSMutableArray
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            
                            self.tblVwFlight.isHidden = false
                            self.tblVwFlight.isUserInteractionEnabled = true
                            self.loading.dismiss()
                            self.tblVwFlight.reloadData()
                            
                        })
                    } else {
                         self.loading.dismiss()
                        _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "No Flights Availabel For This Route.", showInView: self.view)
                        self.tblVwFlight.isHidden = true
                    }
                }
            }
        } else if textField == txt_city {
            self.loading.dismiss()
            //            viewForCity_add_hotel.hidden = true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //       self.loading.dismiss()
        if textField == txtDepartureCity_add_flight {
            searchActive = false
        }
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        
        let resultPredicate = NSPredicate(format: "SELF CONTAINS[c] %@",  textField.text!)
        filtered = self.reversed.filtered(using: resultPredicate) as NSArray
        
        if(filtered.count == 0){
            isTextfieldSelect = true
            searchActive = false;
            // pickerCountStatCity.reloadAllComponents()
            tblVwFlight.reloadData()
        } else {
            isTextfieldSelect = false
            searchActive = true;
            //  pickerCountStatCity.reloadAllComponents()
            tblVwFlight.reloadData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txt_city {
            
            if(txt_city.text?.count > 0 || string.characters.count > 0)
            {
                textfield_common = txt_city
                textfield_common.tag = 99
                
                var strSearch : String = ""
                if(txt_city.text?.count > 0) {
                    strSearch =  txt_city.text! + string // "\(txtCity.text)\(string)"
                } else{
                    strSearch = string
                }
                self.viewForCity_add_hotel.isHidden = false
                getcityList(strSearch)
            }
        } else  if textField == txtDepartureCity_add_flight {
            
            if(txtDepartureCity_add_flight.text?.count > 0 || string.characters.count > 0)  {
                textfield_common = txtDepartureCity_add_flight
                textfield_common.tag = 100
                var strSearch : String = ""
                if(txtDepartureCity_add_flight.text?.count > 0)
                {
                    strSearch =  txtDepartureCity_add_flight.text! + string // "\(txtCity.text)\(string)"
                } else{
                    strSearch = string
                }
                getAirportDetail(strSearch)
            }
            
        } else if textField == txtArrivalCity_add_flight {
            
            if(txtArrivalCity_add_flight.text?.characters.count > 0 || string.characters.count > 0)
            {
                textfield_common = txtArrivalCity_add_flight
                textfield_common.tag = 101
                var strSearch : String = ""
                if(txtArrivalCity_add_flight.text?.characters.count > 0)
                {
                    strSearch =  txtArrivalCity_add_flight.text! + string // "\(txtCity.text)\(string)"
                } else{
                    strSearch = string
                }
                let test_arr : NSMutableArray! = NSMutableArray()
                arr_cityList = test_arr
                getAirportDetail(strSearch)
            }
            
        } else if textField == txtAirlineName_add_flight {
            
            arr_cityList.removeAllObjects()
            
            self.viewForCity_add_hotel.isHidden = false
            
            let char = string.cString(using: String.Encoding.utf8)!
            
            let backSpace = strcmp(char, "\\b")
            
            var textEntered = "\(String(describing: textField.text!))\(string)"
            
            if (backSpace != -92) {
                
                if(txtAirlineName_add_flight.text?.characters.count > 0 || string.characters.count > 0)
                {
                    textfield_common = txtAirlineName_add_flight
                    textfield_common.tag = 102
                    var strSearch : String = ""
                    if(txtAirlineName_add_flight.text?.characters.count > 0)
                    {
                        strSearch =  txtAirlineName_add_flight.text! + string // "\(txtCity.text)\(string)"
                    } else{
                        strSearch = string
                    }
                    self.viewForCity_Flight_add.isHidden = true
                    getAirlineName(strSearch)
                }
            }
                
            else {
                
                textEntered = String(textEntered.dropLast())
                
                if textEntered.characters.count == 0 {
                    self.viewForCity_Flight_add.isHidden = true
                    arr_cityList.removeAllObjects()
                }
                
            }
            
            //            if(txtAirlineName_add_flight.text?.characters.count > 0 || string.characters.count > 0)
            //            {
            //                textfield_common = txtAirlineName_add_flight
            //                textfield_common.tag = 102
            //                var strSearch : String = ""
            //                if(txtAirlineName_add_flight.text?.characters.count > 0)
            //                {
            //                    strSearch =  txtAirlineName_add_flight.text! + string // "\(txtCity.text)\(string)"
            //                } else{
            //                    strSearch = string
            //                }
            //                self.viewForCity_Flight_add.isHidden = false
            //                getAirlineName(strSearch)
            //            }
            
        }
        return true
    }
    
    func getAirportDetail(_ searchText: String) {
        
//
//        let resultPredicate = NSPredicate(format: "name contains[c] %@", searchText)
//        let swiftArray = NSArray(array: arrOfDestinationStation)
//        let searchResults = swiftArray.filtered(using: resultPredicate)
//        arr_cityList = NSMutableArray(array:searchResults)
//
//        print(arr_cityList)
        
        self.viewForCity_Flight_add.isHidden = false
        let resultPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
        
        let swiftArray = NSArray(array: arrOfDestinatioStation)
        let searchResults = swiftArray.filtered(using: resultPredicate)
        arr_cityList = NSMutableArray(array:searchResults)
         print(arr_cityList)
      //  arr_cityList = arrOfDestinatioStation.filtered(using: resultPredicate) as! NSMutableArray
        searchActive = true
        self.tbl_city_add_flight.reloadData()
    }
    
    func getAirlineName(_ search_String: String){
        
        let test_arr : NSMutableArray! = NSMutableArray()
        arr_cityList = test_arr
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let parameters : [String: AnyObject]!
        parameters = ["search" : search_String as AnyObject]
        
        AppTheme().callGetService(String(format: "%@airline_autocomplete", AppUrl), param: parameters) { (result, data) -> Void in
            
            if result == "success"{
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                
                if let array_response : NSArray = respDict["response"] as? NSArray {
                    self.arr_cityList = array_response.mutableCopy() as! NSMutableArray
                    self.viewForCity_Flight_add.isHidden = false

                    if self.arr_cityList.count > 0 {
                        self.viewForCity_Flight_add.isHidden = false
                    } else {
                        self.viewForCity_Flight_add.isHidden = true
                    }
                    if self.textfield_common.tag == 100 || self.textfield_common.tag == 101 || self.textfield_common.tag == 102{
                        self.viewForDatePicker_add_flight.isHidden = true
                        self.tbl_city_add_flight.reloadData()
                    }else{
                        self.viewForCity_add_hotel.isHidden = false
                        self.tbl_city.reloadData()
                    }
                }
            } else {
                //                self.loading.dismiss()
                self.viewForCity_Flight_add.isHidden = true
            }
        }
    }
    
    func getcityList(_ search_String: String)  {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let parameters : [String: AnyObject]!
        parameters = ["search" : search_String as AnyObject]
        
        AppTheme().callGetService(String(format: "%@city_search", AppUrl), param: parameters) { (result, data) -> Void in
            if result == "success"{
                self.loading.dismiss()
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if let array_response : NSArray = respDict["response"] as? NSArray
                {
                    self.arr_cityList = array_response.mutableCopy() as! NSMutableArray
                    if self.textfield_common.tag == 100 || self.textfield_common.tag == 101{
                        self.tbl_city_add_flight.reloadData()
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            
                            self.viewForDatePicker_add_flight.isHidden = true
                            self.viewForCity_Flight_add.isHidden = false
                        })
                    } else {
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.viewForCity_add_hotel.isHidden = false
                            self.tbl_city.reloadData()
                        })
                    }
                }
            } else {
                self.loading.dismiss()
                self.viewForCity_Flight_add.isHidden = true
            }
        }
    }
    // MARK: -  UItextView Delegate
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == txtForWriteStatus {
            if txtForWriteStatus.text == "See Something. Say Something." {
                txtForWriteStatus.text = ""
            }
        } else if textView == txtViewForCaution{
            if txtViewForCaution.text == "Please enter your caption.." {
                txtViewForCaution.text = ""
            }
        }else if textView == textview_description{
            if textview_description.text == "Enter Description."   {
                textview_description.text = ""
            }
        }
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if (textView == textview_arrival_time_add_flight) || (textView == textview_depart_time_add_flight ) {
            if textView == textview_arrival_time_add_flight {
                if textview_arrival_time_add_flight.text == "Arrival Time" || textview_arrival_time_add_flight.text != "" {
                    textview_arrival_time_add_flight.resignFirstResponder()
                    textview_arrival_time_add_flight.text = ""
                    textview_arrival_time_add_flight.tag = 5
                    textview_depart_time_add_flight.tag = 101
                }
            }else if textView == textview_depart_time_add_flight  {
                if textview_depart_time_add_flight.text == "Departure Time" || textview_depart_time_add_flight.text != "" {
                    textview_depart_time_add_flight.resignFirstResponder()
                    textview_depart_time_add_flight.text = ""
                    textview_depart_time_add_flight.tag = 6
                    textview_arrival_time_add_flight.tag = 100
                }
            }
            self.viewForDatePicker_add_flight.isHidden = false
            self.timePicker_add_flight.bringSubview(toFront: self.viewForDatePicker_add_flight)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            self.timePicker_add_flight.backgroundColor = UIColor.white
            timePicker_add_flight.datePickerMode = UIDatePickerMode.dateAndTime
            textView.inputView = self.timePicker_add_flight
            
            // ToolBar
            let toolBar = UIToolbar()
            toolBar.frame = CGRect(x: 0, y: 0, width: self.timePicker_add_flight.frame.size.width, height: 44)
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
            toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
            
            // Adding Button ToolBar
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(HomeVC.doneClick(_:)))
            let flexibleSpaceLeft:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            flexibleSpaceLeft.width = 120
            
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(HomeVC.cancelClick(_:)))
            toolBar.setItems([cancelButton, flexibleSpaceLeft, doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            textView.inputAccessoryView = toolBar
            viewForDatePicker_add_flight.addSubview(toolBar)
            
        } else {
            if textView == txtForWriteStatus{
                if txtForWriteStatus.text == "See Something. Say Something." {
                    txtForWriteStatus.text = ""
                }
            } else if textView == txtViewForCaution{
                if txtViewForCaution.text == "Please enter your caption.." {
                    txtViewForCaution.text = ""
                }
        } else if textView == textview_description{
                if textview_description.text == "Enter Description." {
                    textview_description.text = ""
                }
            }
            return true
        }
        return false
    }
    
    @objc func cancelClick(_ sender:UIDatePicker){
        self.viewForDatePicker_add_flight.isHidden = true
    }
    
    @objc func doneClick(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if textview_arrival_time_add_flight.tag == 5 {
            textview_arrival_time_add_flight.text = dateFormatter.string(from: timePicker_add_flight.date)
        }else if textview_depart_time_add_flight.tag == 6{
            textview_depart_time_add_flight.text = dateFormatter.string(from: timePicker_add_flight.date)
        }
        timePicker_add_flight.resignFirstResponder()
        self.viewForDatePicker.resignFirstResponder()
        viewForDatePicker_add_flight.resignFirstResponder()
        
        self.viewForDatePicker.isHidden = true
        viewForDatePicker_add_flight.isHidden = true
        textview_arrival_time_add_flight.inputView = nil
        textview_arrival_time_add_flight.delegate = self
        textview_arrival_time_add_flight.endEditing(true)
        textview_depart_time_add_flight.inputView = nil
        textview_depart_time_add_flight.delegate = self
        textview_depart_time_add_flight.endEditing(true)
    }
    
    func dateChanged(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == txtForWriteStatus{
            if txtForWriteStatus.text == "" {
                txtForWriteStatus.text = "See Something. Say Something."
            }
        } else if textView == txtViewForCaution{
            if txtViewForCaution.text == "" {
                txtViewForCaution.text = "Please enter your caption.."
            }
        }else if textView == textview_description{
            if textview_description.text == "" {
                textview_description.text = "Enter Description."
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //=======================================
    //MARK: Get current location method
    //=======================================
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        lat = locValue.latitude
        let strLat: String = String(locValue.latitude)
        long = locValue.longitude
        let strLong: String = String(locValue.longitude)
        
        if (!isNetworkAvailable) {
            return
        }
        self.strParamLatLong = "\(strLat)/\(strLong)"
        
        if UIApplication.shared.applicationState == .active {
            areaCodeApi_Call() //TODO : NEED TO CHECK
        } else {
        }
       
    }
    
    
    // API call for area code
    func areaCodeApi_Call() {
       
        //Get current time zone
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        var timeString : String!
        
        
        //self.loading.dismiss()
        timeString =  dateFormatter.timeZone.identifier
        firstLoc = true
        
      //  let lat : Double = 0.0 //TODO : Need to comment this |for testing|
        //let long : Double = 0.0  //TODO : Need to comment this  |for testing|

        let parameters : [String: AnyObject] =
                                              ["lat" : "0.0"  as AnyObject,
                                               "long" : "0.0" as AnyObject,
                                               "user_id" : AppTheme.getLoginDetails().value(forKey: "id")! as AnyObject,
                                               "timezone" : timeString as AnyObject]
        
        AppTheme().callGetCode(String(format: "%@area_code?", AppUrl), param: parameters) {(result, data) -> Void in
            
            if result == "success" {
                
             //   self.loading.dismiss()
                
                var departments : NSDictionary = NSDictionary()
                departments = data as! NSDictionary
                
                print("departments  == \(departments)")

                self.locationManager.stopUpdatingLocation()
                
                if let tempDict : NSDictionary = departments["airport_record"] as? NSDictionary {
                    if let str_sourceCode : String = tempDict .value(forKey: "code") as? String{
                        self.strSourceCode = str_sourceCode
                    }
                }
                let strCode: NSString = NSString(string: self.strSourceCode)
                if strCode != ""{
                    AppTheme.setAirportCode(strCode)
                } else{
                    AppTheme.setAirportCode("")
                }
                
                CLGeocoder().reverseGeocodeLocation(self.locationManager.location!, completionHandler: {(placemarks, error) -> Void in
                    if  (error != nil)
                    {
                        print("Reverse geocoder failed with error" + error!.localizedDescription)
                        return
                    }
                    if placemarks?.count > 0
                    {
                        let pm = placemarks![0] as CLPlacemark
                        self.displayLocationInfo(pm)
                    }
                    else
                    {
                        print("Problem with the data received from geocoder")
                    }
                })
            } else
            {
                self.loading.dismiss()
            }
        }
        
    }
    
    func displayLocationInfo(_ placemark: CLPlacemark?){
        
        if let containsPlacemark = placemark {
            let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
            let country = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            
            self.strPresentLocName = locality!
            self.strCountry = country
            AppTheme.setPresentLocation(self.strPresentLocName as NSString)
            print(self.strPresentLocName)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print(error)
    }
    
    //==============================================================================================
    //MARK: Action for the Like, comment and share, Video play, image preview, more_option
    //==============================================================================================
    
    @objc func videoTapped(_ sender: UIButton!) {
        
        print("index: ", sender.tag)
        let dicData: NSDictionary = self.arrOfResponseKey.object(at: sender.tag/2) as! NSDictionary
        print(dicData)
        
        if let img_post = dicData .value(forKey: "file_name") as? String{
            let popupView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 160))
            popupView.backgroundColor = UIColor.clear
            
            var player = AVPlayer()
            
            let url_video = URL(string: img_post)

            if img_post.contains(AppurlforImage) {
                 player = AVPlayer(url: url_video!)
            } else {
                let filePath = BaseApp.sharedInstance.getLocalMediaFileFolderPath(imageName: img_post)?.path
                let imgAdd   =  BaseApp.sharedInstance.getVideo(filepath:  filePath!)
                let urlLink  =  NSURL(fileURLWithPath:imgAdd)
                  let imageData = imgAdd.data(using: .utf8)!
              //   let  imageData  = UIImageJPEGRepresentation(imgAdd, 1.0)!
                print("iriiriririirri ====\(imgAdd)")
                player = AVPlayer(url: urlLink as URL)
            }

            let playerController = AVPlayerViewController()
            playerController.player = player
            present(playerController, animated: true) {
                player.play()
            }
            popupView.addSubview(playerController.view)
        }
    }
    
    @objc func imageTapped(_ sender: UIButton!) {
        
        print("index: %@", sender.tag)
        let dicData: NSDictionary = self.arrOfResponseKey.object(at: sender.tag/2) as! NSDictionary
        print(dicData)
        var popupVC: ImageVideoPlayerController! = ImageVideoPlayerController()
        popupVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageVideoPlayerController") as! ImageVideoPlayerController
        if let img_post = dicData .value(forKey: "file_name") as? String{
            let url_image = URL(string: img_post)
            popupVC.img_url = url_image as! URL
        }
        if let str_userName = dicData .value(forKey: "username") as? String{
            popupVC.str_username = str_userName
        }
        let popupDestVC = STPopupController(rootViewController: popupVC)
        popupDestVC.present(in: self)
    }
    
    @objc func btnUserFollowUnfollow(_ sender: UIButton)
    {
        let dicData: NSDictionary = self.arrOfResponseKey.object(at: sender.tag/2) as! NSDictionary
        self.navigationController?.isNavigationBarHidden = true
        self.viewForUserFollowUnfollow.isHidden = false
        self.tblWriteStatus.isHidden = true
        self.tblVwFlight.isHidden = true
        self.tblUserDetailFollowUnfollow.delegate = self
        self.tblUserDetailFollowUnfollow.dataSource = self
        self.tblUserDetailFollowUnfollow.isHidden = false
        
        if let str_user_id : String = dicData .value(forKey: "user_id") as? String{
            userIdForSingleUserDetail = str_user_id
            UserDefaults.standard.setValue(str_user_id, forKey: "otherId")
            UserDefaults.standard.synchronize()
            self.getSingleUserInfo(userIdForSingleUserDetail)
        }
    }
    
    @IBAction func btn_Action(_ sender: UIButton!)  {

        if let str_user_id : String = dicData .value(forKey: "user_id") as? String{
            userIdForSingleUserDetail = str_user_id
            UserDefaults.standard.setValue(str_user_id, forKey: "otherId")
            UserDefaults.standard.synchronize()
            self.getSingleUserInfo(userIdForSingleUserDetail)
        }
    }
    
    @objc func likeDislikeAction(_ sender: UIButton!) {
        
        print("index: %@", sender.tag)
        var dicData: NSDictionary!
        
//        if HomeVC.isUserDetailLike == true {
//            dicData = self.userPostArray.object(at: sender.tag/2) as! NSDictionary
//        } else {
//            dicData = self.arrOfResponseKey.object(at: sender.tag/2) as! NSDictionary
//        }
        dicData = self.arrOfResponseKey.object(at: sender.tag/2) as! NSDictionary

        let paraDict = NSMutableDictionary()
        
        var strFlighId : String! = String()
        var strPostsId : String! = String()
        
        if let str_flight_id : String = dicData["flight_id"]! as? String{
            strFlighId = str_flight_id
        }
        if let str_post_id : String = dicData["post_id"]! as? String{
            strPostsId = str_post_id
        }
        if let str_likeByMe : String = dicData["like_by_me"]! as? String{
            likeByMe = NSInteger(str_likeByMe)!
        }
        else if let str_likeByMe_int: NSInteger = dicData["like_by_me"]! as?  NSInteger{
            likeByMe = str_likeByMe_int
        }
        
        if likeByMe == 1{
            
            paraDict.setValue(strFlighId, forKey:"flight_id")
            paraDict.setValue(strPostsId, forKey:"posts_id")
            paraDict.setValue(strUserId, forKey:"user_id")
            paraDict.setValue("0", forKey:"status")
            
        } else {
            
            paraDict.setValue(strFlighId, forKey:"flight_id")
            paraDict.setValue(strPostsId, forKey:"posts_id")
            paraDict.setValue(strUserId, forKey:"user_id")
            paraDict.setValue("1", forKey:"status")
        }
        
        AppTheme().callPostService(String(format: "%@posts_like", AppUrl), param: paraDict) {(result, data) -> Void in
            
            if result == "success"{
                self.loading.dismiss()
                
                self.getUserStatusList("", str_bottom: "", str_limit: "", str_offset: "", str_add_offset: 0)

            }
        }
    }
    
    @IBAction func commentonOff_Action(_ btnComment: UIButton!)  {
        
        if (btnCommentOnOff.currentImage?.isEqual(UIImage(named: "chatIconGrey")))! {
            btnCommentOnOff.setImage(UIImage(named: "chatIconYellow") as UIImage!, for: .normal)
            strComment = "0"
            
        } else if (btnCommentOnOff.currentImage?.isEqual(UIImage(named: "chatIconYellow")))! {
            btnCommentOnOff.setImage(UIImage(named: "chatIconGrey") as UIImage!, for: .normal)
            strComment = "1"
        }
    }
    
    @objc func commentActionTableCell(_ sender: UIButton!)  {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        
        print("index 111: %@", sender.tag/2 )

        let dicData: NSDictionary!
        var onOffComment : String!
        var postId : String!
        let strCommentOnOff : String!

        dicData = self.arrOfResponseKey.object(at: sender.tag/2) as! NSDictionary
        
        if let str_CommentOnOff : String = dicData.value(forKey: "commentsoff") as? String  {
            onOffComment = str_CommentOnOff
            print("comment status 111: %@", onOffComment)
        }
        
        if let str_PostID : String = dicData.value(forKey: "post_id") as? String  {
            postId = str_PostID
        }
        
        if onOffComment == "0"{
            onOffComment = "1"
            strCommentOnOff = "Do you want to disable comments?"
        } else {
            onOffComment = "0"
            strCommentOnOff = "Do you want to enable comments?"
        }
        
        //*> create the alert
        let alert = UIAlertController(title:strCommentOnOff, message: "", preferredStyle: UIAlertControllerStyle.alert)
        present(alert, animated: true, completion: nil)
        
        //*> add the actions (buttons)
        alert.addAction(UIAlertAction(title: "NO", style: .default, handler: { action in
        }))
        
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            
            self.commentOnOffAction_ApiCall(status: onOffComment , postID:  postId ,selectedIndex: sender.tag/2 , dataDict : dicData)
        }))
        
    }
    
    @objc func moreAction(_ sender: UIButton!)
    {
        self.tblWriteStatus.beginUpdates()
        self.tblWriteStatus.reloadRows(at: [IndexPath(row:sender.tag, section: 0)], with: UITableViewRowAnimation.automatic)
        if shouldCellBeExpanded == false {
            self.shouldCellBeExpanded = true
        } else {
            self.shouldCellBeExpanded = false
        }
        self.tblWriteStatus.endUpdates()
    }
    
    @objc func shareAction(_ sender: UIButton!){
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let dicData: NSDictionary!
//        if HomeVC.isUserDetailLike == true {
//            dicData = self.userPostArray.object(at: sender.tag) as! NSDictionary
//        } else {
//            dicData = self.arrOfResponseKey.object(at: sender.tag/2) as! NSDictionary
//        }
        
        dicData = self.arrOfResponseKey.object(at: sender.tag/2) as! NSDictionary

        var data : Data = Data()
        var imgShare : UIImage = UIImage()
        var textToShare : String = String()
        
        if let str_textToShare : String = dicData["posts"]! as? String{
            
            textToShare = str_textToShare + "\n" + "www.gupanetwork.com"
            
            if let str_img_share : String = dicData["file_name"]! as? String {
                if str_img_share != "" {
                    let urlNew:String = str_img_share.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    let url  = URL(string: urlNew)
                    data = try! Data(contentsOf: url!)
                    imgShare = UIImage(data: data)!
                }
                else{
                    
                }
            }
        }
        if textToShare != "" && imgShare != nil {
            shareImageText(textToShare, image: imgShare)
        }
        if textToShare == "" && imgShare != nil {
            shareImageText(textToShare, image: imgShare)
        }
        if textToShare != "" && imgShare == nil {
            shareImageText(textToShare, image: imgShare)
        }
    }
    
    func shareImageText(_ text: String, image: UIImage){
        let objects2Share = [text, image] as [Any]
        let activityVC = UIActivityViewController(activityItems: objects2Share , applicationActivities: nil)
        
        let excludeActivities = [UIActivityType.airDrop, UIActivityType.copyToPasteboard , UIActivityType.mail, UIActivityType.addToReadingList]
        
        activityVC.excludedActivityTypes = excludeActivities
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func PlayPause(_ sender: UIButton!){
        btnPlayPause.setImage(UIImage(named: "pause.png"), for: UIControlState())
        player.play()
    }
    
    @objc func deletePost(_ btnDelete: UIButton!){
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let refreshAlert = UIAlertController(title: "Delete post", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
       
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            
            let dicData = self.arrOfResponseKey.object(at: btnDelete.tag/2) as! NSDictionary

            if let strPostsId : String = dicData["post_id"]! as? String {
                self.deleteSelectedPost(strPostsId , selectedIndex: btnDelete.tag/2)
            }
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func btn_timer(_ sender: AnyObject) {
        
        self.timeString = ""
        self.viewForDatePicker.isHidden = false
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.timePicker_add_flight.frame.size.width, height: 44)
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(HomeVC.doneClick_selfTimer(_:)))
        let flexibleSpaceLeft:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        flexibleSpaceLeft.width = 120
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(HomeVC.cancelClick_selfTimer(_:)))
        toolBar.setItems([cancelButton, flexibleSpaceLeft, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        viewForDatePicker.addSubview(toolBar)
        
        var timeString: String!
        timeString = dateFormatter.string(from: timePicker.date)
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.addTarget(self, action: #selector(HomeVC.dueDateChanged(_:)), for: UIControlEvents.valueChanged)
    }
    
    @objc func cancelClick_selfTimer(_ sender:UIDatePicker){
        self.viewForDatePicker.isHidden = true
        self.viewForDatePicker_add_flight.isHidden = true
        self.timePicker_add_flight.isHidden = true
    }
    
    @objc func doneClick_selfTimer(_ sender:UIDatePicker){
        self.viewForDatePicker.isHidden = true
        self.viewForDatePicker_add_flight.isHidden = true
        self.timePicker_add_flight.isHidden = true
    }
    
    @objc  func dueDateChanged(_ sender:UIDatePicker){
        let timeInterval : NSInteger = Int(sender.date.timeIntervalSinceNow)
        timeString = "\(timeInterval/(60*60)):\((timeInterval/60)%60 + 1)"
        print("Time : \(timeString)")
    }
    
    //===================================================================
    //MARK: Tabbar button actions Trip, Activity-Boarding and Hotel
    //===================================================================
    
    @IBAction func btnTripHotelBoarding(_ sender: UIButton)
    {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        button = sender as UIButton
        self.tblWriteStatus.estimatedRowHeight = 50
        self.tblWriteStatus.rowHeight = UITableViewAutomaticDimension
        self.tblWriteStatus.setNeedsLayout()
        self.tblWriteStatus.layoutIfNeeded()
        self.tblWriteStatus.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
       
        if button.tag == 1
        {
            //self.loading.dismiss()

            self.btn_addHotel_detail.setTitle("Enter Flight Details", for: UIControlState())
            self.viewForCity_add_hotel.isHidden = true
            self.viewFor_bg_popup_add_hotel.isHidden = true
            self.arrOfDestinatioStation.removeAllObjects()
            btn_search_hotel.isUserInteractionEnabled = false
            self.title = "Trip"
            button.tag = 1
            flightSearchBar.tag = 1
            flightSearchBar.text = ""
            flightSearchBar.placeholder = "Select Destination"
            txtDestination.isHidden = false
            self.txtDestination.text = ""        //Set the image on the button select on it
            self.btnTrip.setImage(UIImage(named: "button_airline_active.png")! as UIImage, for: UIControlState())
            self.btnTravelBoarding.setImage(UIImage(named: "Button_trio-Inactive.png")! as UIImage, for: UIControlState())
            self.btnHotel.setImage(UIImage(named : "hotel_bed_off.png")! as UIImage, for: UIControlState())
            appDelegate.strURL = ""
            
            self.viewForTravelActivity.isHidden = true
            self.viewTxtDestiContainer.isHidden = false
            buttonFlightPress()
        }
        
        if button.tag == 2 {
            
          //  self.loading.dismiss()

            isUsrCheckIn = true
            self.viewForCity_add_hotel.isHidden = true
            self.viewFor_bg_popup_add_hotel.isHidden = true
            btn_search_hotel.isUserInteractionEnabled = false
            locationManager.stopUpdatingLocation()
            AppTheme.setUserFlight(0)
            
            if AppTheme.getUserFlight() == 0 {
              
                button.tag = 2
                viewForWriteStatus.isHidden = false
                self.tblWriteStatus.isHidden = false
                self.viewForTravelActivity.isHidden = false
                self.imgUserOnStatusview.clipsToBounds = true
                self.tblVwFlight.isHidden = true
                tblVwFlight.isUserInteractionEnabled = false
                self.viewTxtDestiContainer.isHidden = true
                //                self.txtEntercityName.isHidden = true
                self.txtDestination.isUserInteractionEnabled = false
                self.title = "Travel Board"
                self.btnTrip.setImage(UIImage(named: "button_airline_inactive.png")! as UIImage, for: UIControlState())
                self.btnTravelBoarding.setImage(UIImage(named: "Button_trip_active.png")! as UIImage, for: UIControlState())
                self.btnHotel.setImage(UIImage(named: "hotel_bed_off.png")! as UIImage, for: UIControlState())
                
                if let str_profile_pic : String = AppTheme.getLoginDetails().object(forKey: "profile_pic") as? String{
                    let url = URL(string:str_profile_pic)
                    
                    self.imgUserOnStatusview.kf.setImage(with: url!, placeholder: UIImage(named: "ic_hotel-1.png"), options: nil, progressBlock: nil, completionHandler: {
                        (image, error, cacheType, imageURL) in
                        print(image?.description ?? "")
                    })
            
                }
                
                self.imgUserOnStatusview.layer.cornerRadius = self.imgUserOnStatusview.frame.height/2
              
                if isGPSOn() == false {
                    self.getUserStatusList("0", str_bottom: "0", str_limit: "", str_offset: "", str_add_offset: 0)
                    return
                }
                self.getUserStatusList("0", str_bottom: "0", str_limit: "", str_offset: "", str_add_offset: 0)
            } else {
                
                self.loading.dismiss()
                self.tblWriteStatus.isHidden = true
                self.viewForTravelActivity.isHidden = false
                self.title = "Travel Board"
                self.btnTrip.setImage(UIImage(named: "Button_trio-Inactive.png")! as UIImage, for: UIControlState())
                self.btnTravelBoarding.setImage(UIImage(named: "Button_trip_active.png")! as UIImage, for: UIControlState())
                self.btnHotel.setImage(UIImage(named: "hotel_bed_off.png")! as UIImage, for: UIControlState())
                
                let url = URL(string:AppTheme.getLoginDetails().object(forKey: "profile_pic") as! String)
                
                self.imgUserOnStatusview.kf.setImage(with: url!, placeholder: UIImage(named: "ic_hotel-1.png"), options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, imageURL) in
                    print(image?.description ?? "")
                })
                
                self.imgUserOnStatusview.layer.cornerRadius = self.imgUserOnStatusview.frame.height/2
                self.imgUserOnStatusview.clipsToBounds = true
                self.txtForWriteStatus.layer.borderWidth = 1
                self.txtForWriteStatus.layer.borderColor = UIColor.lightGray.cgColor
                self.tblVwFlight.isHidden = true
                self.viewTxtDestiContainer.isHidden = true
                //                self.txtEntercityName.isHidden = true
                self.txtDestination.isUserInteractionEnabled = false
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:  "Sorry you are checked out!", showInView: self.view)
            }
        }
        if button.tag == 3 {
            
            self.btn_addHotel_detail.setTitle("Enter Hotel Detail.", for: UIControlState())
          //  self.loading.dismiss()

            self.viewForCity_add_hotel.isHidden = true
            self.viewFor_bg_popup_add_hotel.isHidden = true
            btn_search_hotel.isUserInteractionEnabled = true
            self.title = "Hotels"
            checkTableCell = 101
            flightSearchBar.tag = 2
            flightSearchBar.placeholder = "Enter Hotel Name or City"
            self.btnHotel.setImage(UIImage(named : "hotel_bed_on.png")! as UIImage, for: UIControlState())
            self.btnTrip.setImage(UIImage(named: "button_airline_inactive.png")! as UIImage , for: UIControlState())
            self.btnTravelBoarding.setImage(UIImage(named: "Button_trio-Inactive.png")! as UIImage, for: UIControlState())
            buttonHotelPress()
        }
    }
    
    func buttonFlightPress() {
        
        if isGPSOn() == false {
            self.loading.dismiss()
            return
        }
        self.callLatLongSourceDestination()
        
        if let isLogin = AppTheme.getIsLogin() as? Bool {
            if isLogin == true {
                //self.loading.dismiss()
                var filterArray : NSMutableArray =  NSMutableArray()
                filterArray = filtered.mutableCopy() as! NSMutableArray
                filtered = filterArray
                checkTableCell = 105
                getAirportDetail()
            } else {
                AppTheme.setIsLogin(true)
                getAirDetail()
            }
        }
        self.tblVwFlight.isHidden = true
        tblVwFlight.isUserInteractionEnabled = false
        self.txtDestination.isUserInteractionEnabled = false
    }
    
    func buttonHotelPress(){
        
        button.tag = 3
        callLatLongSourceDestination()
        self.tblWriteStatus.isHidden = true
        viewForWriteStatus.isHidden = true
        viewForAlertCamera.isHidden = true
        viewForUserFollowUnfollow.isHidden = true
        self.viewForTravelActivity.isHidden = true
        self.viewTxtDestiContainer.isHidden = false
        flightSearchBar.isHidden = false
        self.tblVwFlight.isHidden = true
        txtDestination.isHidden = true
        self.txtDestination.isUserInteractionEnabled = false
        self.flightSearchBar.text = strPresentLocName
        
        var strHotelId : String = String()
        strHotelId = ""
        
        if strPresentLocName == nil {
            if isGPSOn() == false{
                getHotelList(strHotelId, city_name: self.flightSearchBar.text!)
                self.loading.dismiss()
                return
            }
            callLatLongSourceDestination()
         //   strPresentLocName = "Indore"    //// 29-30 september 2016
        }
        if strPresentLocName != nil || strPresentLocName.isEmpty {
            
            getHotelList(strHotelId, city_name: strPresentLocName)
        }
    }
    
    func callLatLongSourceDestination()
    {
        if (!isNetworkAvailable) {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:  "Please check your network connection!", showInView: self.view)
            return
        }
        
        if isGPSOn() == false {
            return
        }
        /* Get User's Location */
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            print("Location is on")

            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }else{
            print("Location is off")
        }
    }
    
    func getAirDetail() {
        
        
        //button travel boarding action view design
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        AppTheme().callGetCode(String(format: "%@airports_records", AppUrl), param: nil)  { (result, data) -> Void in
            if result == "success"{
                
                self.loading.dismiss()

                var departments : NSDictionary = NSDictionary()
                departments = data as! NSDictionary
                
                print("Department is \(departments)")
                
                if let response_array : NSArray = departments .value(forKey: "response") as? NSArray{
                    
                    for value in response_array  {
                        
                        let dict = value as! [String : AnyObject]
                        
                        self.isTextfieldSelect = true
                        let strCityNameCode = (dict["city"] as! String) + "," + (dict["name"] as! String)
                        self.arrOfDestinatioStation.add(strCityNameCode as String)
                        self.arrOfDestinatioStationCode.add(dict["code"] as! String)
                        self.arrOfDestinatioStationCity.add(dict["name"] as! String)
                        self.setAirportDetail(dict as NSDictionary)
                    }
                    
                    self.popUpForDestination()  /// call method for destination data load
                    //sorted array for destination stations
                    self.reversed = (self.arrOfDestinatioStation as NSMutableArray as! [String]).sorted(by: { (s1: String, s2: String) -> Bool in
                        return s1 < s2
                    }) as NSArray
                } else {
                    
                    self.loading.dismiss()
                    _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:"Some thing wrong in service", showInView: self.view)
                }
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:  "Please check your network connection!", showInView: self.view)
            }
        }
    }
    
    @IBAction func btnCameraVideo(_ sender: UIButton)
    {
        self.picker?.delegate = self
        //        if txtViewForCaution.text == "" {
        //            txtViewForCaution.text = "Please enter your caption.."
        //        }else{
        alertVCForImage()
        //        }
    }
    
    func alertVCForImage() {
        
        let alertController = UIAlertController(title: "Please Select", message: "", preferredStyle: .alert)
        
        let galleryAction = UIAlertAction(title: "Image from gallery", style: .default){ (action) -> Void in
            self.galleryAction()
        }
        let cameraAction = UIAlertAction(title: "Image from camera", style: .default) {
            (action) -> Void in
            self.cameraAction()
        }
        let videoAction = UIAlertAction(title: "Video", style: .default) { (action) -> Void in
            self.videoAction()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addAction(galleryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(videoAction)
        alertController.addAction(cancelAction)
        
        alertController.view.backgroundColor = UIColor.white
        alertController.view.tintColor = UIColor.gray
        alertController.view.layer.cornerRadius = 5
        alertController.view.layer.masksToBounds = true
        
        present(alertController, animated: true, completion: nil)
    }
    
    func galleryAction()  {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)){
            self.picker?.delegate = self
            picker!.allowsEditing = true
            picker!.mediaTypes = [kUTTypeImage as String]
            picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
            present(picker!, animated: true, completion: nil)
        }
    }
    func cameraAction(){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = true
            picker!.mediaTypes = [kUTTypeImage as String]
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        } else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func videoAction(){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum)){
            picker!.allowsEditing = true
            picker!.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
            
            self.picker!.mediaTypes = [kUTTypeMovie as String]
            self.picker!.delegate = self
            //            self.picker!.videoMaximumDuration = 2.0
            present(picker!, animated: true, completion: nil)
        }
    }
    
    //===========================================
    //MARK: UIImagePickerview delegate methods
    //===========================================
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        dismiss(animated: true, completion: { () -> Void in

            if self.button.tag == 2 {
            self.viewForAlertCamera.isHidden = false
            
            let url = URL(string:AppTheme.getLoginDetails().object(forKey: "profile_pic") as! String)
            
            self.imgUserOnAlert.kf.setImage(with: url!, placeholder: UIImage(named: "ic_hotel-1.png"), options: nil, progressBlock: nil, completionHandler: {
                (image, error, cacheType, imageURL) in
                print(image?.description ?? "")
            })
       
            self.imgUserOnAlert.layer.cornerRadius = self.imgUserOnStatusview.frame.height/2
            self.imgUserOnAlert.clipsToBounds = true
            self.txtViewForCaution.layer.cornerRadius = 3
            self.txtViewForCaution.layer.masksToBounds = true
            self.txtViewForCaution.layer.borderWidth = 1
            self.txtViewForCaution.layer.shadowColor = UIColor.lightGray.cgColor
        }
        
        let mediaType:AnyObject? = info[UIImagePickerControllerMediaType] as AnyObject
        
        if let type:AnyObject = mediaType {
            
            if type is String {
                
                let stringType = type as! String
                if stringType == kUTTypeMovie as String {
                    
                    let urlOfVideo = info[UIImagePickerControllerMediaURL] as? URL
                    
                    self.selectedVideoPath = urlOfVideo!
                    self.imageVideoData = try? Data(contentsOf: self.selectedVideoPath)
                    
                    if let url = urlOfVideo
                    {
                        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
                        self.loading.textLabel.text = "Uploading..."
                        self.loading.show(in: self.view)
                        
                        if self.imageVideoData.count > 42603722 {
                            
                            
                            let VideoFilePath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("mergeVideo\(arc4random()%1000)d").appendingPathExtension("mp4").absoluteString
                            if FileManager.default.fileExists(atPath: VideoFilePath) {
                                do {
                                    try FileManager.default.removeItem(atPath: VideoFilePath)
                                     } catch {
                                }
                            }
                            
                            let savePathUrl =  URL(string: VideoFilePath)!
                            
                            let sourceAsset = AVURLAsset(url: urlOfVideo!, options: nil)
                            let assetExport: AVAssetExportSession = AVAssetExportSession(asset: sourceAsset, presetName: AVAssetExportPresetLowQuality)!
                            assetExport.outputFileType = AVFileType.mp4
                            assetExport.outputURL = savePathUrl
                            
                            assetExport.exportAsynchronously { () -> Void in
                                
                                switch assetExport.status {
                                case AVAssetExportSessionStatus.completed:
                                    DispatchQueue.main.async(execute: {
                                        do {
                                            self.loading.dismiss()
                                            self.isCompressed = true
                                            self.imageVideoDataCompressed = try! Data(contentsOf: savePathUrl)
                                            let videoData = try Data(contentsOf: savePathUrl, options: NSData.ReadingOptions())
                                            print("MB - \(videoData.count / (1024 * 1024))")
                                            self.imgCameraGallary.isHidden = true
                                            self
                                                .viewForVideoSelect.isHidden = false
                                            self.viewForVideoSelect.bringSubview(toFront: self.viewForImageSelect)
                                            let player = AVPlayer(url: savePathUrl)
                                            
                                            let playerLayer = AVPlayerLayer(player: player)
                                            var frameVideoLayer : CGRect = self.viewForVideoSelect.frame
                                            frameVideoLayer.origin.x = 0
                                            frameVideoLayer.origin.y = 0
                                            playerLayer.frame = frameVideoLayer
                                            
                                            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                                            self.viewForVideoSelect.layer.addSublayer(playerLayer)
                                            player.pause()
                                            self.loading.dismiss()
                                        } catch {
                                            print(error)
                                        }
                                    })
                                case AVAssetExportSessionStatus.failed:
                                    self.loading.dismiss()
                                    print("failed \(String(describing: assetExport.error))")
                                case AVAssetExportSessionStatus.cancelled:
                                    self.loading.dismiss()
                                    print("cancelled \(String(describing: assetExport.error))")
                                default:
                                    self.loading.dismiss()
                                    print("complete")
                                }
                            }
                        }else{
                            self.loading.dismiss()
                            self.isCompressed = false
                            self.imageVideoData = try? Data(contentsOf: urlOfVideo!)
                        }
                    }
                } else {
                    
                    self.chosenImage = self.rotateCameraImageToProperOrientation(info[UIImagePickerControllerOriginalImage] as! UIImage, maxResolution: 1024)
                    if self.button.tag == 1 {
                        print("Flight Select")
                    } else if self.button.tag == 2{
                        self.imgCameraGallary.isHidden = false
                        self.viewForVideoSelect.isHidden = true
                        self.imgCameraGallary.contentMode = .scaleAspectFit
                        self.imgCameraGallary.image = self.chosenImage
                        self.imgCameraGallary.bringSubview(toFront: self.viewForImageSelect)
                        
                    } else if self.button.tag == 3 {
                        if self.isCamera == false {
                            let tempImage  = info[UIImagePickerControllerReferenceURL] as! URL
                            self.lbl_select_image.text =  String(describing: tempImage)
                            self.imageVideoData = UIImageJPEGRepresentation(self.chosenImage, 1.0)!
                        } else if self.isCamera == true{
                            self.lbl_select_image.text = "image select successfully."
                        }
                    }
                    self.imageVideoData = UIImageJPEGRepresentation(self.chosenImage, 1.0)!
                }
            }
        }
        self.btnRating.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
        self.btnRatingOnAlert.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
    })
       // dismiss(animated: true, completion: nil)
    }
    
    
    // Fix image orientation rotate in right direction
    func rotateCameraImageToProperOrientation(_ imageSource : UIImage, maxResolution : CGFloat) -> UIImage {
        
        let imgRef = imageSource.cgImage;
        
        let width = CGFloat(imgRef!.width);
        let height = CGFloat(imgRef!.height);
        
        var bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        var scaleRatio : CGFloat = 1
        
        if (width > maxResolution || height > maxResolution) {
            
            scaleRatio = min(maxResolution / bounds.size.width, maxResolution / bounds.size.height)
            bounds.size.height = bounds.size.height * scaleRatio
            bounds.size.width = bounds.size.width * scaleRatio
        }
        
        var transform = CGAffineTransform.identity
        let orient = imageSource.imageOrientation
        let imageSize = CGSize(width: CGFloat(imgRef!.width), height: CGFloat(imgRef!.height))
        
        switch(imageSource.imageOrientation) {
        case .up :
            transform = CGAffineTransform.identity
            
        case .upMirrored :
            transform = CGAffineTransform(translationX: imageSize.width, y: 0.0);
            transform = transform.scaledBy(x: -1.0, y: 1.0);
            
        case .down :
            transform = CGAffineTransform(translationX: imageSize.width, y: imageSize.height);
            transform = transform.rotated(by: CGFloat(M_PI));
            
        case .downMirrored :
            transform = CGAffineTransform(translationX: 0.0, y: imageSize.height);
            transform = transform.scaledBy(x: 1.0, y: -1.0);
            
        case .left :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(translationX: 0.0, y: imageSize.width);
            transform = transform.rotated(by: 3.0 * CGFloat(M_PI) / 2.0);
            
        case .leftMirrored :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(translationX: imageSize.height, y: imageSize.width);
            transform = transform.scaledBy(x: -1.0, y: 1.0);
            transform = transform.rotated(by: 3.0 * CGFloat(M_PI) / 2.0);
            
        case .right :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(translationX: imageSize.height, y: 0.0);
            transform = transform.rotated(by: CGFloat(M_PI) / 2.0);
            
        case .rightMirrored :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
            transform = transform.rotated(by: CGFloat(M_PI) / 2.0);
        }
        
        UIGraphicsBeginImageContext(bounds.size)
        let context = UIGraphicsGetCurrentContext()
        
        if orient == .right || orient == .left {
            context!.scaleBy(x: -scaleRatio, y: scaleRatio);
            context!.translateBy(x: -height, y: 0);
        } else {
            context!.scaleBy(x: scaleRatio, y: -scaleRatio);
            context!.translateBy(x: 0, y: -height);
        }
        context!.concatenate(transform);
        UIGraphicsGetCurrentContext()!.draw(imgRef!, in: CGRect(x: 0, y: 0, width: width, height: height));
        
        let imageCopy = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return imageCopy!;
    }
    
    
    @IBAction func btnCancel(_ sender: AnyObject){
        self.imageVideoData = nil
        self.videoData_compress = nil
        self.viewForAlertCamera.isHidden = true
        self.txtViewForCaution.text = ""
        if txtForWriteStatus.text == ""
        {
            txtForWriteStatus.text = "See Something. Say Something."
        }
    }
    
    //Alert smile buttons action, send button
    @IBAction func btnSmileActionOnAlert(_ sender: UIButton){
        
//        if (!isNetworkAvailable) {
//             Utility.showNetWorkAlert()
//            return
//        }
        isActive = 0
        if sender.tag == 4 {
            if isSmileButtonGoodSelect == false{
                isSmileButtonGoodSelect = true
                isSmileButtonUglySelect = false
                isSmileButtonBadSelect = false
                self.rating = 1
                self.btnRatingOnAlert.setImage(UIImage(named: "good_on.png")! as UIImage, for: UIControlState())
            }else{
                isSmileButtonGoodSelect = false
                self.rating = 0
                self.btnRatingOnAlert.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            }
        } else if sender.tag == 5{
            if isSmileButtonBadSelect == false {
                isSmileButtonGoodSelect = false
                isSmileButtonUglySelect = false
                isSmileButtonBadSelect = true
                self.rating = 2
                self.btnRatingOnAlert.setImage(UIImage(named: "bad_on.png")! as UIImage, for: UIControlState())
            }else{
                isSmileButtonBadSelect = false
                self.rating = 0
                self.btnRatingOnAlert.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            }
        } else if sender.tag == 6 {
            if isSmileButtonUglySelect == false {
                isSmileButtonUglySelect = true
                isSmileButtonGoodSelect = false
                isSmileButtonBadSelect = false
                self.rating = 3
                self.btnRatingOnAlert.setImage(UIImage(named: "bad.png")! as UIImage, for: UIControlState())
            } else{
                isSmileButtonUglySelect = false
                self.rating = 0
                self.btnRatingOnAlert.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            }
        }
        self.viewForSmilePopUpOnCameraAlert.isHidden = true
        self.txtViewForCaution.isUserInteractionEnabled = true
    }

    
//    func getDefaultFolderPath() -> URL?{
//        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//        let documentsDirectory = paths[0]
//        let path = URL(string: documentsDirectory)
//
//        return path ?? nil
//    }
//
//    func getLocalMediaFileFolderPath(imageName:String) -> URL?{
//        let documentsDirectory = getDefaultFolderPath()?.appendingPathComponent(imageName).path
//        let path = URL(string: documentsDirectory!)
//        return path ?? nil
//    }
//
//    func saveImageToDocumentDirectory(_ chosenImage: UIImage) -> String {
//        //let directoryPath =  NSHomeDirectory().appending("/Documents/")
//        let filename = getCurentTime().appending(".jpg")
//
////        let directoryPath = getLocalMediaFileFolderPath(imageName: filename)
//        let directoryPath = getDefaultFolderPath()
////
////        if !FileManager.default.fileExists(atPath: (directoryPath?.path)!) {
////            do {
////                //try FileManager.default.createDirectory(at: NSURL.fileURL(withPath: (directoryPath?.path)!), withIntermediateDirectories: true, attributes: nil)
////            } catch {
////                print(error)
////            }
////        }
//
//        //let filepath = directoryPath.appending(filename)
//        let filepath = directoryPath?.appendingPathComponent(filename).path//.appending(filename)
//        let url = NSURL.fileURL(withPath: filepath!)
////        let url = getLocalMediaFileFolderPath(imageName: filename)
//        do {
//            try UIImageJPEGRepresentation(chosenImage, 1.0)?.write(to: url, options: .atomic)
//            return String("\(filename)")
//
//        } catch {
//            print(error)
//            print("file cant not be save at path \(filename), with error : \(error)");
//            return (directoryPath?.path)!
//        }
//    }
    
    /*
    func saveImageToDocumentDirectory(_ chosenImage: UIImage) -> String {
        let directoryPath =  NSHomeDirectory().appending("/Documents/")
        if !FileManager.default.fileExists(atPath: directoryPath) {
            do {
                try FileManager.default.createDirectory(at: NSURL.fileURL(withPath: directoryPath), withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error)
            }
        }
        let filename = getCurentTime().appending(".jpg")
        let filepath = directoryPath.appending(filename)
        let url = NSURL.fileURL(withPath: filepath)
        do {
            try UIImageJPEGRepresentation(chosenImage, 1.0)?.write(to: url, options: .atomic)
            return String("\(filepath)")
            
        } catch {
            print(error)
            print("file cant not be save at path \(filepath), with error : \(error)");
            return filepath
        }
    }*/
    

    func getImage(filepath:String) -> UIImage {
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filepath){
            return UIImage(contentsOfFile: filepath)!
        }else{
            print("No Image")
            return UIImage()
        }
    }

    
    @IBAction func btnSendOnAlert(_ sender: AnyObject){
        
       // self.txtViewForCaution.text = "Please enter your caption.."
        
        if (!isNetworkAvailable) {
            
            if  self.imgCameraGallary == nil{
                
                if imgCameraGallary == nil {
                    
                    self.loading.dismiss()
                    self.stopActivity()
                    _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select image/video for share!", showInView: self.view)
                    chosenImage = nil
                }
            } else {
                
                self.viewForAlertCamera.isHidden = true
                
                if chosenImage != nil {

                    let rat: String = String(rating)
                    if self.timeString.isEmpty {
                        timeString = ""
                    }
                 
                    if self.txtViewForCaution.text == "Please enter your caption.." || self.txtViewForCaution.text == ""  {
                        self.loading.dismiss()
                        self.txtViewForCaution.text = ""
                    }
                    
                let parameters : [String: AnyObject] = ["user_id"   : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject,
                                                            "description"  : txtViewForCaution.text as AnyObject,
                                                            "title"        : "title" as AnyObject,
                                                            "file_type"    : "image" as AnyObject,
                                                            "rating"       : rat as AnyObject,
                                                            "rating_timer" : timeString as AnyObject]

            var theJSONText = String()
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: parameters,
                options: []) {
                theJSONText = String(data: theJSONData,
                                     encoding: .ascii)!
                print("JSON string = \(theJSONText)")
            }
            
            let strFullName = AppTheme.getUserFullName()
            print("UserFullName = \(strFullName)")
            
            var urlImage: URL!
            
            if let str_profile_pic : String = AppTheme.getLoginDetails().value(forKey: "profile_pic") as? String{
                urlImage = URL(string: str_profile_pic)!
                print("str_profile_pic = \(urlImage!)")
            }
            
            let todayDate : String!
            let todayTime : String!
            
            todayDate = getTodayDate()
            todayTime = getCurentTime()
                    
            let strFilePath = BaseApp.sharedInstance.saveImageToDocumentDirectory(self.chosenImage)
        

            let postDetail = PostDetail(postIntId: 0, strAppUrl: "addPost_image", strAppUrlType: "post", strParamData:
                theJSONText, strImageVideoType: "image", strImageFilePath: strFilePath)
            
            let userID : String = (AppTheme.getLoginDetails().object(forKey: "id") as? String)!

                    let tempDict  = ["commentsoff" : strComment , "date": todayDate ,"disable": "0" ,"email": "" , "file_name": strFilePath ,"file_type": "image", "flight_id": "" , "like_by_me": "0" , "post_id": "", "posts": self.txtViewForCaution.text, "profile_pic": "\(urlImage!)","rating": rating , "share_url": "" , "tag": "post" , "time": todayTime , "timestamp": "", "total_comment": "0", "total_like": "0" , "user_id": userID , "username": strFullName] as NSDictionary
                    
                    self.arrOfResponseKey.insert(tempDict, at: 0)
                    self.tblWriteStatus.reloadData()
                    self.txtViewForCaution.text = ""
                    populateCategoryDetails(postDetail: postDetail)
                    
                } else if (imageVideoData != nil) || (imageVideoDataCompressed != nil) {
                    
                    let rat: String = String(rating)
                    
                    if self.txtViewForCaution.text == "Please enter your caption.." || self.txtViewForCaution.text == ""  {
                        self.loading.dismiss()
                        self.txtViewForCaution.text = ""
                    }

                    let parameters : [String: AnyObject] = ["user_id" : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject,
                                                            "description" : txtViewForCaution.text as AnyObject,
                                                            "file_type" : "Video" as AnyObject,
                                                            "rating" : rat as AnyObject]
                    

                    let strFilePath = BaseApp.sharedInstance.saveVideoToDocumentDirectory(urlString: self.selectedVideoPath)
                    
                    print("strFilePath = \(strFilePath)")

                    var theJSONText = String()
                    if let theJSONData = try? JSONSerialization.data(
                        withJSONObject: parameters,
                        options: []) {
                        theJSONText = String(data: theJSONData,
                                             encoding: .ascii)!
                        print("JSON string = \(theJSONText)")
                    }
                    
                    let strFullName = AppTheme.getUserFullName()
                    
                    var urlImage: URL!
                    
                    if let str_profile_pic : String = AppTheme.getLoginDetails().value(forKey: "profile_pic") as? String{
                        urlImage = URL(string: str_profile_pic)!
                    }
                    
                    let todayDate : String!
                    let todayTime : String!
                    
                    todayDate = getTodayDate()
                    todayTime = getCurentTime()
                    
                    let postDetail = PostDetail(postIntId: 0, strAppUrl: "addPost_image", strAppUrlType: "post", strParamData:
                        theJSONText, strImageVideoType: "Video", strImageFilePath: strFilePath)
                    
                    let userID : String = (AppTheme.getLoginDetails().object(forKey: "id") as? String)!
                    
                    let tempDict  = ["commentsoff" : strComment , "date": todayDate ,"disable": "0" ,"email": "" , "file_name": strFilePath ,"file_type": "video", "flight_id": "" , "like_by_me": "0" , "post_id": "", "posts": self.txtViewForCaution.text, "profile_pic": "\(urlImage!)","rating": rating , "share_url": "" , "tag": "post" , "time": todayTime , "timestamp": "", "total_comment": "0", "total_like": "0" , "user_id": userID , "username": strFullName] as NSDictionary
                    
                    self.arrOfResponseKey.insert(tempDict, at: 0)
                    self.tblWriteStatus.reloadData()
                    self.txtViewForCaution.text = ""
                    populateCategoryDetails(postDetail: postDetail)
                }
          }
        }  else {
        
        //Rajesh Work on Service calling
        
        if  self.imgCameraGallary == nil {
            
            if imgCameraGallary == nil {
                
                self.loading.dismiss()
                self.stopActivity()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select image/video for share!", showInView: self.view)
                chosenImage = nil
            }
        } else {
            
            self.viewForAlertCamera.isHidden = true
            
            if chosenImage != nil   {
                
                var fName = "FeedBack_iOS_Report_Image_";
                fName = fName + DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .medium)
                fName += ".jpeg"
                fName = fName.trimmingCharacters(in: CharacterSet.whitespaces)
                fName = fName.replacingOccurrences(of: " ", with: "")
                
                let rat: String = String(rating)
                
                if self.timeString.isEmpty {
                    timeString = ""
                }
                if (!isNetworkAvailable) {
                    return
                }
                
                self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
                self.loading.textLabel.text = "Sending..."
                self.loading.show(in: self.view)
                
                if self.txtViewForCaution.text == "Please enter your caption.." || self.txtViewForCaution.text == ""  {
                    self.loading.dismiss()
                    self.txtViewForCaution.text = ""
                }
                
                let parameters : [String: AnyObject] = ["user_id"   : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject,
                                                        "description" : txtViewForCaution.text as AnyObject,
                                                        "title"        : "title" as AnyObject,
                                                        "file_type"    : "image" as AnyObject,
                                                        "rating"       : rat as AnyObject,
                                                        "rating_timer" : timeString as AnyObject]
                                
                AppTheme().callPostWithMultipartServices(String(format: "%@add_blogs", AppUrl), param: parameters, imageData: imageVideoData, file_type: "imagePost") { (result, data) -> Void in
                    
                    if(result == "success") {
                        _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Post Sent.", showInView: self.view )
                        self.loading.dismiss()
                        self.getUserStatusList("0", str_bottom: "", str_limit: "", str_offset: "", str_add_offset: 0)
                        self.tblWriteStatus.reloadData()
                    } else {
                       _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error!Please try again laer.", showInView: self.view)
                        self.loading.dismiss()
                    }
                }
                chosenImage = nil
                
            } else if  (imageVideoData != nil) || (imageVideoDataCompressed != nil) {
                
                if self.txtViewForCaution.text == "Please enter your caption.." || self.txtViewForCaution.text.isEmpty  {
                    self.loading.dismiss()
                    self.txtViewForCaution.text = ""
                }
                self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
                self.loading.textLabel.text = "Sending..."
                self.loading.show(in: self.view)
                
              //  let fId: String = String(AppTheme.getUserFlight())
                let rat: String = String(rating)
                
                if (!isNetworkAvailable) {
                    self.loading.dismiss()
                    return
                }
                let parameters : [String: AnyObject] = ["user_id" : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject,
                                                        "description" : txtViewForCaution.text as AnyObject,
                                                      //  "flight_id" : fId as AnyObject,
                                                        "file_type" : "Video" as AnyObject,
                                                        "rating" : rat as AnyObject]
                
                if isCompressed == true {
                    isCompressed = false
                    videoData_compress = imageVideoDataCompressed
                } else {
                    videoData_compress = imageVideoData
                }
                AppTheme().callPostWithMultipartServices(String(format: "%@add_blogs", AppUrl), param: parameters, imageData: videoData_compress, file_type: "video") { (result, data) -> Void in
                    
                    if(result == "success") {
                        //                        self.stopActivity()
                        _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Post Sent.", showInView: self.view )
                        self.loading.dismiss()
                        
                        self.getUserStatusList("0", str_bottom: "", str_limit: "", str_offset: "", str_add_offset: 0)
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            
                            self.imageVideoData = nil
                            self.videoData_compress = nil
                            self.tblWriteStatus.reloadData()
                        })
                        
                    } else {
                        self.loading.dismiss()
                        _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: (data as? String)!, showInView: self.view )
                    }
                }
            }
          }
        }
        
       
    }
    
    
    /*func convertVideoWithMediumQuality(inputURL : NSURL){
     
     let VideoFilePath = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("mergeVideo\(arc4random()%1000)d")!.URLByAppendingPathExtension("mp4")!.absoluteString
     if NSFileManager.defaultManager().fileExistsAtPath(VideoFilePath!) {
     do {
     try NSFileManager.defaultManager().removeItemAtPath(VideoFilePath!)
     } catch { }
     }
     
     let savePathUrl =  NSURL(string: VideoFilePath!)!
     
     let sourceAsset = AVURLAsset(URL: inputURL, options: nil)
     
     let assetExport: AVAssetExportSession = AVAssetExportSession(asset: sourceAsset, presetName: AVAssetExportPresetMediumQuality)!
     assetExport.outputFileType = AVFileTypeMPEG4
     assetExport.outputURL = savePathUrl
     assetExport.exportAsynchronouslyWithCompletionHandler { () -> Void in
     
     switch assetExport.status {
     case AVAssetExportSessionStatus.Completed:
     dispatch_async(dispatch_get_main_queue(), {
     do {
     let videoData = try NSData(contentsOfURL: savePathUrl, options: NSDataReadingOptions())
     print("MB - \(videoData.length / (1024 * 1024))")
     } catch {
     print(error)
     }
     })
     case  AVAssetExportSessionStatus.Failed:
     // self.hideActivityIndicator(self.view)
     print("failed \(assetExport.error)")
     case AVAssetExportSessionStatus.Cancelled:
     //self.hideActivityIndicator(self.view)
     print("cancelled \(assetExport.error)")
     default:
     // self.hideActivityIndicator(self.view)
     print("complete")
     }
     }
     
     }*/
    //Smile button action on home view controller
    @IBAction func btnSmile(_ sender: UIButton) {
        
        if sender.tag == 10 {
            if isActive == 0 {
                isActive = 1
                self.viewForSmilePopUp.isHidden = false
                self.txtForWriteStatus.isUserInteractionEnabled = false
            }else{
                isActive = 0
                self.viewForSmilePopUp.isHidden = true
                self.txtForWriteStatus.isUserInteractionEnabled = true
            }
        } else if sender.tag == 11 {
            if isActive == 0 {
                isActive = 1
                self.viewForSmilePopUpOnCameraAlert.isHidden = false
                self.txtViewForCaution.isUserInteractionEnabled = false
            } else {
                isActive = 0
                self.viewForSmilePopUpOnCameraAlert.isHidden = true
                self.txtViewForCaution.isUserInteractionEnabled = true
            }
        }
    }
    @IBAction func btnSmileAction(_ sender: UIButton) {
        
        isActive = 0
        if sender.tag == 1
        {
            if isSmileButtonGoodSelect == false {
                isSmileButtonGoodSelect = true
                isSmileButtonUglySelect = false
                isSmileButtonBadSelect = false
                self.rating = 1
                self.btnRating.setImage(UIImage(named: "good_on.png")! as UIImage, for: UIControlState())
            } else {
                isSmileButtonGoodSelect = false
                self.rating = 0
                self.btnRating.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            }
        } else if sender.tag == 2 {
            if isSmileButtonBadSelect == false {
                isSmileButtonGoodSelect = false
                isSmileButtonUglySelect = false
                isSmileButtonBadSelect = true
                self.rating = 2
                self.btnRating.setImage(UIImage(named: "bad_on.png")! as UIImage, for: UIControlState())
        } else {
                isSmileButtonBadSelect = false
                self.rating = 0
                self.btnRating.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            }
        } else if sender.tag == 3 {
            if isSmileButtonUglySelect == false {
                isSmileButtonUglySelect = true
                isSmileButtonGoodSelect = false
                isSmileButtonBadSelect = false
                self.rating = 3
                self.btnRating.setImage(UIImage(named: "bad.png")! as UIImage, for: UIControlState())
        } else {
                isSmileButtonUglySelect = false
                self.rating = 0
                self.btnRating.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
            }
        }
        self.viewForSmilePopUp.isHidden = true
        self.txtForWriteStatus.isUserInteractionEnabled = true
    }
    
    @objc func getAllPostDFromNotificationPostMethod()  {
        
        self.getUserStatusList("0", str_bottom: "", str_limit: "", str_offset: "", str_add_offset: 0)

    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    @IBAction func btnPost(_ sender: UIButton) {
        
        if self.txtForWriteStatus.text == "See Something. Say Something." || self.txtForWriteStatus.text.isEmpty {
            
            if self.txtForWriteStatus.text == "" || self.txtForWriteStatus.text == "See Something. Say Something." {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "See Something. Say Something!", showInView: self.view )
            }
            self.btnRating.setImage(UIImage(named: "good_off.png")! as UIImage, for: UIControlState())
        } else {
            
            var strComment : String = "0"
            let paraDict = NSMutableDictionary()
            
            if (!isNetworkAvailable) {
                
                if (btnCommentOnOff.currentImage?.isEqual(UIImage(named: "chatIconGrey")))! {
                    btnCommentOnOff.setImage(UIImage(named: "chatIconYellow") as UIImage!, for: .normal)
                    strComment = "0"
                    
                } else if (btnCommentOnOff.currentImage?.isEqual(UIImage(named: "chatIconYellow")))! {
                    btnCommentOnOff.setImage(UIImage(named: "chatIconGrey") as UIImage!, for: .normal)
                    strComment = "1"
                }
                
                if let str_user_id : String = AppTheme.getLoginDetails().object(forKey: "id")! as? String {
                    
                    paraDict.setValue(str_user_id, forKey:"user_id")
                    paraDict.setValue("", forKey:"notify_user")
                    paraDict.setValue(self.txtForWriteStatus.text, forKey:"posts")
                    paraDict.setValue(rating, forKey:"rating")
                    paraDict.setValue(strComment, forKey:"enable_comment")
                }
                
                var theJSONText = String()
                if let theJSONData = try? JSONSerialization.data(
                    withJSONObject: paraDict,
                    options: []) {
                    theJSONText = String(data: theJSONData,
                                         encoding: .ascii)!
                    print("JSON string = \(theJSONText)")
                }
               
                let postDetail = PostDetail(postIntId: 0, strAppUrl: "WallPost", strAppUrlType: "post", strParamData:
                    theJSONText, strImageVideoType: "image", strImageFilePath: "")
                
                let strFullName = AppTheme.getUserFullName()
                print("UserFullName = \(strFullName)")
                
                var urlImage: URL!

                if let str_profile_pic : String = AppTheme.getLoginDetails().value(forKey: "profile_pic") as? String{
                     urlImage = URL(string: str_profile_pic)!
                    print("str_profile_pic = \(urlImage!)")
                }

                self.loading.dismiss()
                
                let todayDate : String!
                let todayTime : String!

                todayDate = getTodayDate()
                todayTime = getCurentTime()

                print("todayDate = \(todayDate!)")
                print("todayTime = \(todayTime!)")


                let userID : String = (AppTheme.getLoginDetails().object(forKey: "id") as? String)!

                let tempDict  = ["commentsoff" : strComment , "date": todayDate , "email": "" , "file_name": "" ,"file_type": "", "flight_id": "" , "like_by_me": "0" , "post_id": "", "posts": "\(self.txtForWriteStatus.text!)", "profile_pic": "" , "rating": rating , "share_url": "\(urlImage)" , "tag": "post" , "time": todayTime , "timestamp": "", "total_comment": "0", "total_like": "0" , "user_id": userID , "username": strFullName] as [String : Any]
                
                self.arrOfResponseKey.insert(tempDict, at: 0)
                self.tblWriteStatus.reloadData()
                
                self.txtForWriteStatus.text = ""
                populateCategoryDetails(postDetail: postDetail)
                
            } else {
                
                if let str_user_id : String = AppTheme.getLoginDetails().object(forKey: "id")! as? String {
                    
                    if let str_flight_id : Int = AppTheme.getUserFlight() as? Int {
                        
                        let paraDict = NSMutableDictionary()
                        paraDict.setValue(str_user_id, forKey:"user_id")
                        paraDict.setValue("", forKey:"notify_user")
                        paraDict.setValue(self.txtForWriteStatus.text, forKey:"posts")
                        paraDict.setValue(rating, forKey:"rating")
                        paraDict.setValue(strComment, forKey:"enable_comment")
                        
                        AppTheme().callPostService(String(format: "%@wallPost", AppUrl), param: paraDict) {(result, data) -> Void in
                            
                            if result == "success"  {
                                self.loading.dismiss()
                                
                                self.respDict = data as! NSDictionary
                                print("111111 == \(self.respDict)")
                                self.txtForWriteStatus.text = ""
                                self.getUserStatusList("0", str_bottom: "", str_limit: "", str_offset: "", str_add_offset: 0)
                                
                            } else if result == "error" {
                                self.loading.dismiss()
                                _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel:"Error found!", showInView: self.view )
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getTodayDate() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
       
        let today_string = String(year!) + "-" + String(month!) + "-" + String(day!)
        
        return today_string
        
    }
    
    func getCurentTime() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.hour,.minute,.second], from: date)
        
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
        
    }
    
    
    ///** To Insert INTO Database
    func populateCategoryDetails(postDetail : PostDetail?){

        databaseManager.addCategoryValuesToDatabase(postDetails: postDetail!)
    }
    
    func getSingleUserInfo(_ otherId : String){
        
        var destVC = UserChatOnCommentController()
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
        destVC.str_other_id = otherId
        destVC.isFromTrip = "true"
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    func getHotelList(_ hotel_Id: String, city_name: String) {
        
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
        
        if self.flightSearchBar.text != "" {
            self.strPresentLocName = self.flightSearchBar.text!
        }
        
        if strPresentLocName == nil {
            
            if isGPSOn() == false{
                self.loading.dismiss()
                return
            }
            callLatLongSourceDestination()
          //  strPresentLocName = "Indore"    //// 29-30 september 2016  TODO check values
        }
        
        let parameters : [String: AnyObject] = ["city" : city_name as AnyObject,
                                                "hotel_name" : hotel_Id as AnyObject]
        
        AppTheme().callGetService(String(format: "%@hotel_search_db", AppUrl), param: parameters) {(result, data) -> Void in                  ///Call services
            
            if result == "success"  {
                
                self.loading.dismiss()
                self.arrOfResponseKey.removeAllObjects()
                self.tblVwFlight.isHidden = false
                self.tblVwFlight.isUserInteractionEnabled = true
                
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                
                if let array_response : NSArray = respDict["response"] as? NSArray {
                    
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                    if self.arrOfResponseKey.count > 0  {
                        
                        for element in self.arrOfResponseKey {
                            self.dicData = element as! NSDictionary
                        }
                        if let str_hotel_name : String = self.dicData["hotel_name"] as? String{
                            self.hotelModel.strHotelName = str_hotel_name
                        }
                        if let str_hotel_address : String = self.dicData["address"] as? String{
                            self.hotelModel.strHotelAddress = str_hotel_address
                        }
                        self.tblVwFlight.reloadData()
                    }
                }
             } else {
                self.loading.dismiss()
                self.alertController()
                self.tblVwFlight.isHidden = true
            
            }
        }
        self.loading.dismiss()
    }
    
    func alertController(){
        
        let refreshAlert = UIAlertController(title: "Enter Hotel Details.", message: "No Hotel found. Do you want to enter Hotel details.?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
            self.txtArrivalCity_add_flight.text = ""
            self.txtDepartureCity_add_flight.text = ""
            self.txtAirlineName_add_flight.text = ""
            self.txtFlightNumber_add_flight.text = ""
            self.txtName_On_Ticket_add_flight.text = ""
            self.txtDestination.text = ""
            self.txt_booking_reference_add_flight.text = ""
            self.textview_depart_time_add_flight.text = ""
            self.textview_arrival_time_add_flight.text = ""
            
            self.viewFor_bg_popup_add_hotel.isHidden = false
            self.view_container_for_review_popup_add_hotel.isHidden = false
            self.view_container_for_checkInCheckOut_add_hotel.isHidden = false
            self.popUpDesign()
            self.viewForDatePicker.isHidden = true
            self.viewForCity_add_hotel.isHidden = true
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    func getUserStatusList(_ str_top : String, str_bottom: String, str_limit: String, str_offset: String, str_add_offset: NSInteger){
        
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
        
        let currentTimeZone: String = (TimeZone.current as NSTimeZone).description
        let arrayTime: NSArray = currentTimeZone .components(separatedBy: " ") as NSArray
        let zoneString : String = arrayTime .object(at: 0) as! String
        
        if (!isNetworkAvailable) {
            
            self.loading.dismiss()
            Utility.showNetWorkAlert()
            return
        }
        
        if strUserId != "" || strUserId != nil {
            
            let parameters : [String: AnyObject] = ["flight_id" : "" as AnyObject,
                                                    "user_id" : strUserId as AnyObject,
                                                    "timezone" : zoneString as AnyObject,
                                                    "offset" : str_offset as AnyObject,
                                                    "top" : str_top as AnyObject,
                                                    "bottom" : str_bottom as AnyObject,
                                                    "limit" : str_limit as AnyObject,
                                                    "adv_offset" : str_add_offset as AnyObject]
            
            AppTheme().callGetService(String(format: "%@get_posts", AppUrl), param: parameters) {(result, data) -> Void in
                
                self.refreshControl.endRefreshing()
                if result == "success"
                {
                    
                    self.arrOfResponseKey = []
                    self.loading.dismiss()
                    
                    let respDict = data as! NSDictionary
                    print(respDict)
                    
                    var temp_arr : NSMutableArray = NSMutableArray()

                    if let arrResponse: NSMutableArray = data as? NSMutableArray{
                        print("arrResponse: @%", arrResponse)
                        
                    } else if let dicRes: NSDictionary = data as? NSDictionary {
                        print("arrResponse 11111: @%")
                        
                        if let tempArr: NSArray = dicRes["response"] as? NSArray {
                            
                            if tempArr.count == 0 || tempArr.isEqual(nil) {
                                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry there is no data found!", showInView: self.view)
                            } else {
                                temp_arr = tempArr.mutableCopy() as! NSMutableArray
                            }
                        }
                    }
                    
                    if self.isScrolling == true{
                        self.loading.dismiss()
                        self.arrOfResponseKey.addObjects(from: temp_arr as [AnyObject])
                    } else {
                        self.loading.dismiss()
                        self.arrOfResponseKey = temp_arr.mutableCopy() as! NSMutableArray
                    }
                    self.tblWriteStatus.reloadData()
                } else {
                    self.loading.dismiss()
                    if self.isScrolling == true{
                        
                    } else {
                        self.loading.dismiss()
                        _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                    }
                }
            }
        }
    }
    
    func deleteSelectedPost(_ post_id : String , selectedIndex: Int)  {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(post_id, forKey:"post_id")
        
        AppTheme().callPostService(String(format: "%@post_delete", AppUrl), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Post deleted!", showInView: self.view )
             
                self.arrOfResponseKey.removeObject(at: selectedIndex)
                  self.tblWriteStatus.reloadData()
               // self.getUserStatusList("0", str_bottom: "", str_limit: "", str_offset: "", str_add_offset: 0)
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Error! Please try again.", showInView: self.view )
            }
        }
    }
    
    @IBAction func btnCancelFollowUnfollow(_ sender: UIButton)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.viewForUserFollowUnfollow.isHidden = true
        self.tblWriteStatus.isHidden = false
        getUserStatusList("", str_bottom: "", str_limit: "", str_offset: "", str_add_offset: 0)
    }
    
    //MARK:- IBActions for chat button, follow-unfollow
    
    @IBAction func btn_search_hotel(_ sender: UIButton) {
        
        if self.flightSearchBar.text != nil || self.flightSearchBar.text != "" {
            
            tblVwFlight.isHidden = true
            checkTableCell = 101
            isSearchButtonClick = true
            flightSearchBar.resignFirstResponder()
            if queue != nil {
                self.queue.cancelAllOperations()
            }
            getHotelList(self.flightSearchBar.text!, city_name: "")
        }
        flightSearchBar.endEditing(true)
    }
    
    @IBAction func chatButtonOnNaviOnUserDetail(_ sender: AnyObject) {
    }
    
    @IBAction func followUnfollowButtonOnNavigationOnUserDetail(_ sender: AnyObject) {
        
        if self.isFollow == 0 || self.isFollow == nil{
            
            isFollow = 1
        } else{
            isFollow = 0
        }
        followUnfollowActionNavigationBar(isFollow)
    }
    
    
    func followUnfollowActionNavigationBar(_ status : NSInteger) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(strUserId, forKey:"youruser_id")
        paraDict.setValue(userIdForSingleUserDetail, forKey:"otheruser_id")
        paraDict.setValue(status, forKey:"status")
        
        AppTheme().callPostService(String(format: "%@followers", AppUrl), param: paraDict) {(result, data) -> Void in                                                                                     ///Call services
            
            if result == "success"
            {
                self.loading = JGProgressHUD(style: .dark)
                
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if respDict["message"] as! String == "successfully UnGuide"
                {
                    self.followUnfollowButtonOnNavigationOnUserDetail.setImage(UIImage(named: "imgFollow.png"), for: UIControlState())
                    self.chatButtonOnNavigationUserDetail.isHidden = true
                    self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: respDict["message"] as! String, showInView: self.view )
                    self.tblUserDetailFollowUnfollow.isHidden = true
                } else{
                    self.followUnfollowButtonOnNavigationOnUserDetail.setImage(UIImage(named: "follow.png"), for: UIControlState())
                    self.chatButtonOnNavigationUserDetail.isHidden = false
                    self.tblUserDetailFollowUnfollow.isHidden = false
                    self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: respDict["message"] as! String, showInView: self.view )
                }
            }else {
                self.loading.dismiss()
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:data as! String, showInView: self.view )
            }
        }
    }
    
    //MARK: IBActions for folloers button, following button
    
    @IBAction func guidGuidedAction(_ sender: UIButton) {
        var destVC : GuideGuidedViewController!
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "GuideGuidedViewController") as! GuideGuidedViewController
        if destVC != nil {
            if sender.tag == 100 {
                destVC.titleString = "GUIDES"
                destVC.strUserId = userIdForSingleUserDetail
            } else if sender.tag == 101 {
                destVC.titleString = "GUIDED"
                destVC.strUserId = userIdForSingleUserDetail
            }
            self.navigationController?.pushViewController(destVC, animated: true)
        }
    }
    
    
    //MARK: - Database method
    func setAirportDetail(_ dataDict: NSDictionary)  {
        
        let entityDescription  = NSEntityDescription.entity(forEntityName: "Table_travel", in: appDelegate.managedObjectContext)
        let airport_Detail  = NSManagedObject(entity: entityDescription!, insertInto: appDelegate.managedObjectContext)
        
        //Changes done by Yash on 02 Jan 2017
       // Utility.getValidString(dataDict.value(forKey: "airport_code") as? String)
        
        if let airACode : String = dataDict.value(forKey: "code") as? String{
            airport_Detail.setValue(airACode, forKey: "airport_code")
        } else{
            airport_Detail.setValue("", forKey: "airport_code")
        }
        
        if let airId : String = dataDict.value(forKey: "id") as? String{
            airport_Detail.setValue(airId, forKey: "airport_id")
        } else{
            airport_Detail.setValue("", forKey: "airport_id")
        }
        
        if let airName : String = dataDict.value(forKey: "name") as? String{
            airport_Detail.setValue(airName, forKey: "airport_name")
        } else{
            airport_Detail.setValue("", forKey: "airport_name")
        }
        
        if let airAlternateNames : String = dataDict.value(forKey: "alternatenames") as? String{
            airport_Detail.setValue(airAlternateNames, forKey: "alternatenames")
        } else{
            airport_Detail.setValue("", forKey: "alternatenames")
        }
        
        if let airCity : String = dataDict.value(forKey: "city") as? String{
            airport_Detail.setValue(airCity, forKey: "airport_city")
        } else{
            airport_Detail.setValue("", forKey: "airport_city")
        }
        
        if let airTimezone : String = dataDict.value(forKey: "timezone") as? String{
            airport_Detail.setValue(airTimezone, forKey: "airport_timezone")
        } else{
            airport_Detail.setValue("", forKey: "airport_timezone")
        }
        
        if let airCityCode : String = dataDict.value(forKey: "city_code") as? String{
            airport_Detail.setValue(airCityCode, forKey: "airport_city_code")
        } else{
            airport_Detail.setValue("", forKey: "airport_city_code")
        }
        
        if  let airCountryCode : String = dataDict.value(forKey: "country_code") as? String{
            airport_Detail.setValue(airCountryCode, forKey: "airport_country_code")
        } else{
            airport_Detail.setValue("", forKey: "airport_country_code")
        }
        
        if let airGeonameId : String = dataDict.value(forKey: "geoname_id") as? String{
            airport_Detail.setValue(airGeonameId, forKey: "airport_geoname_id")
        } else{
            airport_Detail.setValue("", forKey: "airport_geoname_id")
        }
        
        if let airGMT : String = dataDict.value(forKey: "gmt") as? String{
            airport_Detail.setValue(airGMT, forKey: "airport_gmt")
        } else {
            airport_Detail.setValue("", forKey: "airport_gmt")
        }
        
        if let airICAO : String = dataDict.value(forKey: "icao") as? String{
            airport_Detail.setValue(airICAO, forKey: "airport_icao")
        }else{
            airport_Detail.setValue("", forKey: "airport_icao")
        }
        
        if let airLat : String = dataDict.value(forKey: "lat") as? String{
            airport_Detail.setValue(airLat, forKey: "airport_lat")
        } else {
            airport_Detail.setValue("", forKey: "airport_lat")
        }
        
        if  let airLong : String = dataDict.value(forKey: "lng") as? String{
            airport_Detail.setValue(airLong, forKey: "airport_lng")
        } else{
            airport_Detail.setValue("", forKey: "airport_lng")
        }
        appDelegate.saveContext()
        //        do {
        //            try  airport_Detail.managedObjectContext?.save()
        //        } catch {
        //            print(error)
        //        }
    }
    
    
    func getAirportDetail() -> NSArray {
        
        isTextfieldSelect = true
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Table_travel", in: self.appDelegate.managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        var result : NSArray!
        do {
            result = try self.appDelegate.managedObjectContext.fetch(fetchRequest) as NSArray
            
           // self.loading.dismiss()
            
            if (result.count > 0) {
               // self.loading.dismiss()
                for element in result {
                    let airportDetail = element as! NSManagedObject
                    let strCityNameCode = (airportDetail.value(forKey: "airport_city") as! String) + "," + (airportDetail.value(forKey: "airport_name") as! String)
                    self.arrOfDestinatioStation.add(strCityNameCode as String)
                    self.arrOfDestinatioStationCode.add(airportDetail.value(forKey: "airport_code") as! String)
                    self.arrOfDestinatioStationCity.add(airportDetail.value(forKey: "airport_name") as! String)
                }
                //sorted array for destination stations
                if self.arrOfDestinatioStation.count > 0{
                    self.reversed = (self.arrOfDestinatioStation as NSMutableArray as! [String]).sorted(by: { (s1: String, s2: String) -> Bool in
                        return s1 < s2
                    }) as NSArray
                }
                
            } else{
                getAirDetail()
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return result
    }
    
    func getFligtList() {
        
        if self.strSourceCode == ""{
            if self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.flightSearchBar.text!)) as! String  == ""{
                _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "to code empty", showInView: self.view )
            }
        }
        let parameters : [String: AnyObject] = ["from" : self.strSourceCode as AnyObject,
                                                "to"   : self.arrOfDestinatioStationCode.object(at: self.arrOfDestinatioStation.index(of: self.flightSearchBar.text!)) as AnyObject ]
        
        AppTheme().callGetService(String(format: "%@flights", AppUrl), param: parameters) {(result, data) -> Void in                                    ///Call services
            
            if result == "success" {
                self.tblVwFlight.isHidden = false
                self.tblVwFlight.isUserInteractionEnabled = true
                self.loading.dismiss()
                var respDict : NSDictionary = NSDictionary()
                respDict = data as! NSDictionary
                if let array_temp : NSArray = respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_temp.mutableCopy() as! NSMutableArray
                }
                self.button.tag = 1
                self.tblVwFlight.reloadData()
            } else {
                self.loading.dismiss()
                self.alertController_Flight()
                self.tblVwFlight.isHidden = true
            }
        }
    }
    
    
    func getNotificationList() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if let user_id = AppTheme.getLoginDetails().object(forKey: "id") {
            
            let parameters : [String: AnyObject] =
                                                   ["user_id" : user_id as AnyObject]
            
            AppTheme().callGetService(String(format: "%@get_notification", AppUrl), param: parameters) { (result, data) -> Void in
                
                if result == "success"{
                    //                    self.loading.dismiss()
                    var respDict : NSDictionary = NSDictionary()
                    respDict = data as! NSDictionary
                    
                    if let str_noti_count = respDict .value(forKey: "total_notification")  as? NSInteger{
                        AppTheme.setNotiCount(str_noti_count)
                    }else{
                        AppTheme.setNotiCount(0)
                    }
                }
            }
        }
    }
    
    func getUserInfoOnTag(_ str_id : String, str_user_name : String) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let parameters : [String: AnyObject] = ["id" : "" as AnyObject,
                                                "username" : str_user_name as AnyObject]
        
        AppTheme().callGetService(String(format: "%@user_info", AppUrl), param: parameters) { (result, data) -> Void in
            
            if result == "success"{
                self.loading.dismiss()
                var temp_arr : NSMutableArray! = NSMutableArray()
                var temp_dict : NSDictionary! = NSDictionary()
                if let arrResponse: NSMutableArray = data as? NSMutableArray
                {
                    print("arrResponse: @%", arrResponse)
                }
                else if let dicRes: NSDictionary = data as? NSDictionary
                {
                    if let tempArr: NSArray = dicRes["response"] as? NSArray
                    {
                        temp_arr = tempArr.mutableCopy() as! NSMutableArray
                        if temp_arr.count == 0 || temp_arr.isEqual(nil)
                        {
                            
                        } else{
                            temp_arr = tempArr.mutableCopy() as! NSMutableArray
                        }
                    }else {
                        temp_arr = dicRes["response"] as! NSMutableArray
                    }
                    for element in temp_arr {
                        temp_dict = element as! NSDictionary
                        if let str_other_id = temp_dict .value(forKey: "id") as? String{
                            let objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
                            objFlightDetailVC.str_other_id = str_other_id
                            objFlightDetailVC.isFromTrip = "true"
                            self.navigationController?.pushViewController(objFlightDetailVC, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func popUpDesign() {
        
        self.textview_description.layer.cornerRadius = 5
        self.textview_description.layer.borderColor = UIColor.lightGray.cgColor
        self.textview_description.layer.borderWidth = 1
        
//        self.btnCancelCheckInCheckOut_add_hotel.layer.cornerRadius = 2
//        self.btnCancelCheckInCheckOut_add_hotel.layer.masksToBounds = true
//        self.btnCancelCheckInCheckOut_add_hotel.layer.borderColor = UIColor.lightGray.cgColor
//        self.btnCancelCheckInCheckOut_add_hotel.layer.borderWidth = 1
//
//        self.btnSubmitCheckInCheckOut_add_hotel.layer.cornerRadius = 2
//        self.btnSubmitCheckInCheckOut_add_hotel.layer.masksToBounds = true
//        self.btnSubmitCheckInCheckOut_add_hotel.layer.borderColor = UIColor.lightGray.cgColor
//        self.btnSubmitCheckInCheckOut_add_hotel.layer.borderWidth = 1
        
        //Flight Pop up design
//        self.btnCancelCheckInCheckOut_add_flight.layer.cornerRadius = 2
//        self.btnCancelCheckInCheckOut_add_flight.layer.masksToBounds = true
//        self.btnCancelCheckInCheckOut_add_flight.layer.borderColor = UIColor.lightGray.cgColor
//        self.btnCancelCheckInCheckOut_add_flight.layer.borderWidth = 1
//
//        self.btnSubmitCheckInCheckOut_add_flight.layer.cornerRadius = 2
//        self.btnSubmitCheckInCheckOut_add_flight.layer.masksToBounds = true
//        self.btnSubmitCheckInCheckOut_add_flight.layer.borderColor = UIColor.lightGray.cgColor
//        self.btnSubmitCheckInCheckOut_add_flight.layer.borderWidth = 1
        
        self.viewFor_depart_arrival_time_add_flight.layer.cornerRadius = 1
        self.viewFor_depart_arrival_time_add_flight.layer.masksToBounds = true
        self.viewFor_depart_arrival_time_add_flight.layer.borderColor = UIColor.lightGray.cgColor
        self.viewFor_depart_arrival_time_add_flight.layer.borderWidth = 1
        
//        self.btnCancelSelection_add_flight.layer.cornerRadius = 1
//        self.btnCancelSelection_add_flight.layer.masksToBounds = true
//        self.btnCancelSelection_add_flight.layer.borderColor = UIColor.lightGray.cgColor
//        self.btnCancelSelection_add_flight.layer.borderWidth = 1
    }
    
    func ResizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    //====================================================
    //MARK:- Hotel search option view container
    //====================================================

    @IBAction func btn_add_hotel(_ sender: AnyObject) {
        
        if button.tag == 1 {
            
            self.tblVwFlight.isHidden = true
            self.viewFor_bg_popup_add_flight.isHidden = false
            self.view_container_for_review_popup_add_flight.isHidden = false
            self.view_container_for_checkInCheckOut_add_flight.isHidden = false
            self.viewForCity_Flight_add.isHidden = true
            self.viewForDatePicker_add_flight.isHidden = true
            
            if self.strPresentLocName != "" {
                txtDepartureCity_add_flight.text = self.strPresentLocName
            }
            txtAirlineName_add_flight.text = ""
            txtArrivalCity_add_flight.text = ""
            //            txtArrivalBookingRef_add_flight.text = ""
            txtFlightNumber_add_flight.text = ""
            txtAirlineName_add_flight.text = ""
            //            txtNameOnTicket_add_flight.text = ""
            popUpDesign()
            
        } else if button.tag == 3{
            self.viewFor_bg_popup_add_hotel.isHidden = false
            popUpDesign()
            self.view_container_for_review_popup_add_hotel.isHidden = false
            self.view_container_for_checkInCheckOut_add_hotel.isHidden = false
            self.tblVwFlight.isHidden = true
            if self.strPresentLocName != "" {
                txt_city.text = self.strPresentLocName
            }
        }
    }
    @IBAction func btn_cancel_on_registration(_ sender: AnyObject) {
        
        self.viewFor_bg_popup_add_hotel.isHidden = true
        self.view_container_for_review_popup_add_hotel.isHidden = true
        self.view_container_for_checkInCheckOut_add_hotel.isHidden = true
        self.tblVwFlight.isHidden = false
        if self.strPresentLocName != "" {
            txt_city.text = self.strPresentLocName
        }
        self.txt_Name_Used_Booking.text = ""
        self.txt_Hotel_name.text = ""
        self.txt_booking_reference.text = ""
        textview_description.text = ""
        
    }
    
    @IBAction func btn_submit_on_hotel_registration(_ sender: AnyObject) {
        
        if txt_city.text == "" || txt_city.text == nil   || txt_Hotel_name.text == "" || txt_Hotel_name.text == nil || textview_description.text == "" || textview_description.text == nil {
            
            if txt_city.text == "" || txt_city.text == nil {
                
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter City Name.", showInView: self.view)
            } else if txt_Hotel_name.text == "" || txt_Hotel_name.text == nil {
                
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Hotel Name.", showInView: self.view)
            } else if textview_description.text == "" || textview_description.text == nil{
                
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Description.", showInView: self.view)
            }
        } else {
            
            self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
            self.loading.textLabel.text = "Loading"
            self.loading.show(in: self.view)
            
            self.add_hotel_detail()
        }
    }
    
    func add_hotel_detail(){
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if textview_description.text == "Enter Description." || textview_description.text == ""  {
            textview_description.text = ""
        }
        var fName = "FeedBack_iOS_Report_Image_";
        fName = fName + DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .medium)
        fName += ".jpeg"
        fName = fName.trimmingCharacters(in: CharacterSet.whitespaces)
        fName = fName.replacingOccurrences(of: " ", with: "")
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        if let str_userID = AppTheme.getLoginDetails().object(forKey: "id") {
            let parameters : [String: AnyObject]!
              parameters = [ "hotel_name" : txt_Hotel_name.text as AnyObject,
                           "user_id" : str_userID as AnyObject,
                           "hotel_id" : "" as AnyObject,
                           "register_no" : "" as AnyObject,
                           "confirmation_number": txt_booking_reference.text as AnyObject,
                           "booking_name": txt_Name_Used_Booking.text as AnyObject,
                           "connected_like": connectedLike_add_hotel as AnyObject,
                           "description" : textview_description.text as AnyObject,
                           "city" : txt_city.text as AnyObject
            ]
            
            AppTheme().callPostWithMultipartServices(String(format: "%@add_hotel", AppUrl), param: parameters, imageData: imageVideoData_addDetail, file_type: "image") { (result, data) -> Void in
                
                if(result == "success") {
                    
                    let dict : NSDictionary = data as! NSDictionary
                    if let str_hotelId = dict .value(forKey: "hotel_id") as? String{
                        self.isHotelSearching_addDetail = true
                        self.getSelectedHotelDetail(str_hotelId, city_name: self.txt_city.text! as String)
                    }
                    if let str_hotelId = dict .value(forKey: "hotel_id") as? Int {
                        self.isHotelSearching_addDetail = true
                        self.getSelectedHotelDetail("\(str_hotelId)", city_name: self.txt_city.text! as String)
                    }
                } else {
                    self.loading.dismiss()
                    _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: (data as? String)!, showInView: self.view)
                }
                self.chosenImage = nil
            }
        }
    }
    
    @IBAction func btn_imageHotel(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Please Select", message: "", preferredStyle: .alert)
        let galleryAction = UIAlertAction(title: "Image from gallery", style: .default) { (action) -> Void in
            
            self.galleryAction_hotel_add()
        }
        let cameraAction = UIAlertAction(title: "Image from camera", style: .default) {
            (action) -> Void in
            self.cameraAction_hotel_add()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        alertController.addAction(galleryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(cancelAction)
        
        alertController.view.backgroundColor = UIColor.white
        alertController.view.tintColor = UIColor.gray
        alertController.view.layer.cornerRadius = 5
        alertController.view.layer.masksToBounds = true
        
        present(alertController, animated: true, completion: nil)
    }
    
    func galleryAction_hotel_add()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary))
        {
            isCamera = false
            self.picker?.delegate = self
            picker!.allowsEditing = true
            picker!.mediaTypes = [kUTTypeImage as String]
            picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
            
            self.viewFor_bg_popup_add_hotel.isHidden = false
            self.view_container_for_review_popup_add_hotel.isHidden = false
            self.view_container_for_checkInCheckOut_add_hotel.isHidden = false
            self.tblVwFlight.isHidden = true
            present(picker!, animated: true, completion: nil)

        }
    }
    func cameraAction_hotel_add()
    {
        isCamera = true
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            self.picker?.delegate = self
            picker!.allowsEditing = true
            picker!.mediaTypes = [kUTTypeImage as String]
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
            self.viewFor_bg_popup_add_hotel.isHidden = false
            self.view_container_for_review_popup_add_hotel.isHidden = false
            self.view_container_for_checkInCheckOut_add_hotel.isHidden = false
            self.tblVwFlight.isHidden = true
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func btn_check_uncheck_connected(_ sender: UIButton) {
        if connectedLike_add_hotel == "0" {
            connectedLike_add_hotel = "1"
            btnCheckForConnectToHotel_add_hotel.setImage(UIImage(named: "img_check.png"), for: UIControlState())
        }else{
            connectedLike_add_hotel = "0"
            btnCheckForConnectToHotel_add_hotel.setImage(UIImage(named: "img_uncheck.png"), for: UIControlState())
        }
    }
    
    //=======================================================
    //MARK:- Flight add popup actions
    //=======================================================

    func alertController_Flight(){
        
        let refreshAlert = UIAlertController(title: "Enter Flight Details", message: "No Flight found. Do you want to enter Flight details.?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
            self.flightSearchBar.resignFirstResponder()
            self.flightSearchBar.canResignFirstResponder

            self.viewFor_bg_popup_add_flight.isHidden = false
            self.view_container_for_review_popup_add_flight.isHidden = false
            self.view_container_for_checkInCheckOut_add_flight.isHidden = false
            self.popUpDesign()
            self.viewForDatePicker_add_flight.isHidden = true
            self.viewForCity_Flight_add.isHidden = true
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func btn_checkUncheck_FlightAdd(_ sender: AnyObject) {
        if connectedLike_add_hotel == "0" {
            connectedLike_add_hotel = "1"
            btnCheckForConnectToBusCompany_add_flight.setImage(UIImage(named: "img_check.png"), for: UIControlState())
        }else{
            connectedLike_add_hotel = "0"
            btnCheckForConnectToBusCompany_add_flight.setImage(UIImage(named: "img_uncheck.png"), for: UIControlState())
        }
    }
    @IBAction func btn_Cancel_FlightAdd(_ sender: AnyObject) {
        self.flightSearchBar.text = ""
        self.viewFor_bg_popup_add_flight.isHidden = true
        self.viewForDatePicker_add_flight.isHidden = true
        self.view_container_for_review_popup_add_flight.isHidden = true
    }
    
    
    func canValidData() -> Bool {
    
        if txtDepartureCity_add_flight.text == "" || txtDepartureCity_add_flight.text == nil {
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Departure City Name.", showInView: self.view)
            return false

        } else if txtArrivalCity_add_flight.text == "" || txtArrivalCity_add_flight.text == nil{
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Arrival City Name.", showInView: self.view)
            return false

        } else if txtAirlineName_add_flight.text == "" || txtAirlineName_add_flight.text == nil{
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Airline Name.", showInView: self.view)
            return false

        } else if txtDescrption_ticket.text == "" || txtDescrption_ticket.text == nil {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Description.", showInView: self.view)
            return false
            
        }  else if textview_depart_time_add_flight.text == "" || textview_depart_time_add_flight.text == nil || textview_depart_time_add_flight.text == "Departure Time" {
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Departure Time.", showInView: self.view)
            return false

        } else if textview_arrival_time_add_flight.text == "Arrival Time" || textview_arrival_time_add_flight.text == ""{
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Arrival Time.", showInView: self.view)
            return false

        }
        return true
    }
    
    @IBAction func btn_Submit_FlightAdd(_ sender: AnyObject) {
        
        if canValidData() {
            
            self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
            self.loading.textLabel.text = "Loading"
            self.loading.show(in: self.view)
            
            self.objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "FlightDetailVC") as? FlightDetailVC
            self.addFlightDetail()
        }
    }
    
    func addFlightDetail()  {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
      
        if departure_city_airport_addFlight == "" || departure_city_airport_addFlight == nil || departure_city_code_addFlight == "" || departure_city_code_addFlight == nil{
            departure_city_airport_addFlight = txtDepartureCity_add_flight.text
            departure_city_code_addFlight = self.strSourceCode
        }
        if let str_user_id : String = AppTheme.getLoginDetails().object(forKey: "id")! as? String{
            if let str_flight_id : Int = AppTheme.getUserFlight() {
               
                let paraDict = NSMutableDictionary()
                paraDict.setValue(str_user_id, forKey:"user_id")
                paraDict.setValue(self.txtFlightNumber_add_flight.text!, forKey:"flight_name")
                paraDict.setValue(self.airline_id_selected, forKey:"airline_id")
                paraDict.setValue(txt_booking_reference.text!, forKey:"booking_reference")
                paraDict.setValue(txtDescrption_ticket.text!, forKey:"description")
                paraDict.setValue("", forKey:"ticket_number")
                paraDict.setValue(connectedLike_add_hotel, forKey:"connected_like")
                paraDict.setValue(textview_arrival_time_add_flight.text!, forKey:"departure_time")
                paraDict.setValue(textview_arrival_time_add_flight.text!, forKey:"arrival_time")
                paraDict.setValue(departure_city_airport_addFlight, forKey:"departure_airport_city")
                paraDict.setValue(arrival_city_airport_addFlight, forKey:"arrival_airport_city")
                paraDict.setValue(departure_city_code_addFlight, forKey:"departure_airport_code")
                paraDict.setValue(arrival_city_code_addFlight, forKey:"arrival_airport_code")
                paraDict.setValue(txtName_On_Ticket_add_flight.text!, forKey:"ticket_name")
                
                AppTheme().callPostService(String(format: "%@add_flight", AppUrl), param: paraDict) {(result, data) -> Void in                                   ///Call services
                    if result == "success"{
                        
                        print(print("555555 == \(data)"))
                        
                        if data != nil {
                            
                         let dict : NSDictionary = data as! NSDictionary
                            
                         if dict != nil{
                            
                            if let str_flight_id = dict.value(forKey: "flight_id") as? String{
                                
                                self.getSingleFlightDetail(str_flight_id)
                            }
                            if let str_flight_id = dict.value(forKey: "flight_id") as? Int{

                                self.getSingleFlightDetail("\(str_flight_id)")
                            }
                            
                          }
                        }
                    } else {
                        self.loading.dismiss()
                    }
                }
            }
        }
    }
    
    @IBAction func btn_Cance_CitySearchFlightAdd(_ sender: AnyObject) {
        
        self.viewForCity_Flight_add.isHidden = true
        self.viewForCity_add_hotel.isHidden = true
        let test_arr : NSMutableArray! = NSMutableArray()
        arr_cityList = test_arr
    }
    
    func getSingleFlightDetail(_ flight_id : String){
        
        print("111")
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
//
//        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//        self.loading.textLabel.text = "Loading"
//        self.loading.show(in: self.view)
        
        if let str_user_id =  AppTheme.getLoginDetails().object(forKey: "id"){
            
            let parameters : [String: AnyObject] =
                ["flight_id" : flight_id as AnyObject,
                 "user_id" : str_user_id as AnyObject]
            
            AppTheme().callGetService(String(format: "%@getFlight", AppUrl), param: parameters) {(result, data) -> Void in
                
                if result == "success"  {
                    print("222")

                    var dictTemp: NSDictionary = NSDictionary()
                    var dataForAirlineFlightUser : NSDictionary = NSDictionary()
                    dataForAirlineFlightUser = data as! NSDictionary
                    
                    var dicData: NSDictionary = NSDictionary()
                    dicData = dataForAirlineFlightUser["response"] as! NSDictionary
                    
                    var testArr : NSArray = NSArray()
                    if let array_airlines : NSArray = Utility.getValueForObject(dicData["airlines"] as! NSArray) as? NSArray{
                        testArr = array_airlines
                    }
                    
                    let objAirlineDetailModel: AirLinesDetails = AirLinesDetails()
                    for airlines in testArr
                    {
                        dictTemp = airlines as! NSDictionary
                    }
                    if let str_airline_name : String = dictTemp .value(forKey: "airline_name") as? String{
                        objAirlineDetailModel.airlineName = str_airline_name
                    }
                    if let str_airline_pic : String = dictTemp["airline_pic"] as? String{
                        objAirlineDetailModel.airlineImage = str_airline_pic as NSString
                    }
                    if let str_about : String = dictTemp["about"] as? String{
                        objAirlineDetailModel.about = str_about
                    }
                    if let arr_for_airline : NSArray = Utility.getValueForObject(dicData["airlines"] as! NSArray) as? NSArray {
                        self.objFlightDetailVC?.arrForAirlines = arr_for_airline.mutableCopy() as! NSMutableArray
                    }
                    if let arr_for_flight : NSArray = Utility.getValueForObject(dicData["flights"] as! NSArray) as? NSArray {
                        self.objFlightDetailVC?.arrForFlight = arr_for_flight.mutableCopy() as! NSMutableArray
                    }

                    if let dict_for_user_mutable : NSDictionary = Utility.getValueForObject(dicData["users"] as! NSDictionary) as? NSDictionary {
                        self.objFlightDetailVC?.dictForUser = dict_for_user_mutable
                    }
                    var airLineId: NSInteger = 0
                    if let strID : String = dictTemp["airline_id"] as? String
                    {
                        airLineId = NSInteger(strID)!
                    }
                    else if let ailineID : NSInteger = dictTemp["airline_id"] as? NSInteger
                    {
                        airLineId = ailineID
                    }
                    self.loading.dismiss()
                    self.navigationController?.pushViewController(self.objFlightDetailVC!, animated: true)
                    
                    
                } else {
                    self.loading.dismiss()

                    _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:  "Sorry there is no flight details available!", showInView: self.view)
                    self.navigationController?.presentedViewController
                }
            }
        }
    }
    //Scale Image and resize of it.
    func getScaledDimension(_ width: CGFloat, height: CGFloat,new_width: CGFloat, new_height: CGFloat)-> CGPoint {
        
        let widthAspect =  (width / new_width)
        let heightAspect = (height / new_height)
        if widthAspect == 0 || heightAspect == 0 {
            return CGPoint(x: width, y: height)
        }
        var width1 : CGFloat = 0
        var height1 : CGFloat =  0
        if widthAspect > heightAspect {
            width1 = (width) / heightAspect
            height1 = (height) / heightAspect
        } else {
            width1 = (width) / widthAspect
            height1 = (height) / widthAspect
        }
        return CGPoint(x: width1, y: height1 )
    }
    
    //Resize image
    func ResizeImage_withDia(_ image: UIImage, targetSize: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: targetSize.width, height: targetSize.height)
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    /* func compressVideo(inputURL: NSURL, outputURL: NSURL, handler:(_ session: AVAssetExportSession)-> Void) {
     let urlAsset = AVURLAsset(URL: inputURL, options: nil)
     if let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) {
     exportSession.outputURL = outputURL
     exportSession.outputFileType = AVFileTypeMPEG4
     exportSession.shouldOptimizeForNetworkUse = true
     exportSession.exportAsynchronouslyWithCompletionHandler { () -> Void in
     handler(session: exportSession)
     }
     }
     } */
}

