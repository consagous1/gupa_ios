//
//  ImageVideoPlayerController.swift
//  Gupa Network
//
//  Created by MWM23 on 2/3/17.
//  Copyright © 2017 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class ImageVideoPlayerController: UIViewController {
 
    var img_url : URL!
    var str_username: String!
    var isFromController : String! = String()
    
    
    var loading : JGProgressHUD!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        self.loading.textLabel.text = "Loading"
        self.loading.show(in: self.view)
        if isFromController == "HomeController" {
            
        }
        if str_username != ""{
           self.title = str_username
        }
        self.contentSizeInPopup = CGSize(width: CGFloat(300), height: CGFloat(400))
        if img_url != nil {
            if let data = try? Data(contentsOf: img_url!) {
                self.loading.dismiss()
                let image: UIImage! = UIImage(data: data)
                let imageView = UIImageView(image: image!)
                imageView.frame = CGRect(x: 0, y: 0, width: 300, height: 400)
                view.addSubview(imageView)
            }
        }
        self.landscapeContentSizeInPopup = CGSize(width: CGFloat(400), height: CGFloat(200))
        // Do any additional setup after loading the view.
    }
    func nextBtnDidTap()  {
        popupController?.dismiss()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.loading.dismiss()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
