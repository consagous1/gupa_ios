//
//  FeedsCommentVC.swift
//  Gupa Network
//
//  Created by MWM23 on 8/6/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher

class FeedsCommentVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var leading_constraint: NSLayoutConstraint!
    @IBOutlet weak var tblCommentsList: UITableView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnPostComment: UIButton!
    @IBOutlet weak var txtCommentField: UITextField!
    
    @IBOutlet weak var img_user_dp: UIImageView!
    @IBOutlet weak var img_user_rating: UIImageView!
    @IBOutlet weak var img_user_image_post: UIImageView!
    @IBOutlet weak var img_user_post_height: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_user_name: UILabel!
    @IBOutlet weak var lbl_timestamp: UILabel!
    @IBOutlet weak var lbl_user_post: UILabel!
    @IBOutlet weak var lbl_post_total_like: UILabel!
    @IBOutlet weak var lbl_total_post_comment: UILabel!
   
    @IBOutlet weak var constraint_width_fourth: NSLayoutConstraint!
    @IBOutlet weak var contraint_width_three: NSLayoutConstraint!
    @IBOutlet weak var contraint_width_two: NSLayoutConstraint!
    @IBOutlet weak var contraint_width_one: NSLayoutConstraint!
    @IBOutlet weak var constraintCommentHeigth : NSLayoutConstraint!

    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var btn_share: UIButton!
    @IBOutlet weak var btn_post_delete: UIButton!
    @IBOutlet weak var btn_like: UIButton!
    @IBOutlet weak var video_user_post_video: SJPlayerView!
    @IBOutlet weak var video_user_post_height: NSLayoutConstraint!
    
    @IBOutlet weak var constraint_width_firstImg: NSLayoutConstraint!
    @IBOutlet weak var img_post_like_zeroth_heightCons: NSLayoutConstraint!
    @IBOutlet weak var img_post_like_one_heightCons: NSLayoutConstraint!
    @IBOutlet weak var img_post_like_two_heightCons: NSLayoutConstraint!
    @IBOutlet weak var img_post_like_three_heightCons: NSLayoutConstraint!
    
    @IBOutlet weak var img_post_like_one: UIImageView!
    @IBOutlet weak var img_post_like_two: UIImageView!
    
    @IBOutlet weak var img_post_like_three: UIImageView!
    @IBOutlet weak var img_post_like_four: UIImageView!
    @IBOutlet weak var btn_more: UIButton!
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var dicData : NSDictionary!
    var strFlighId : String!
    var strPostsId : String!
    var strUserId : String!
    var respDict : NSDictionary = NSDictionary()
    var dict_detail : NSDictionary = NSDictionary()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    static var idStr : String = "CommentCell"
    var isFollow : NSInteger!
    var userIdForSingleUserDetail : String = String()
    var likeByMe : NSInteger!
    var arr_total_like : NSMutableArray! // = <#value#>
    var arr_total_like_images : NSMutableArray! = NSMutableArray()
    var arr_total_comment : NSMutableArray!
    
     var isShowComment : Bool!
     static var isFromUser : Bool = false
     let identifier = "CommentCell"
   
    //-===============================
    //MARK: Life cycle methods
    //-===============================
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if let str_post_id = dicData .value(forKey: "post_id") as? String{
            strPostsId = str_post_id
        }
        
        appDelegate.centerNav = self.navigationController
        
        self.startProgressBar()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 45, height: 40)
        btnLeftBar.addTarget(self, action: #selector(FeedsCommentVC.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        self.title = "Comments"
        UserDefaults.standard.set(1, forKey: "isComment")
        self.txtCommentField.placeholder = "Comment here.."
        self.tblCommentsList.estimatedRowHeight = 100
        self.tblCommentsList.rowHeight = UITableViewAutomaticDimension
        self.tblCommentsList.setNeedsLayout()
        self.tblCommentsList.layoutIfNeeded()
        self.tblCommentsList.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
         tblCommentsList.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: identifier)
        
//        self.imgUser.layer.cornerRadius = 17 //self.imgUser.frame.height/2
//        self.imgUser.clipsToBounds = true
//        self.img_user_dp.layer.cornerRadius = 15 //self.imgUser.frame.height/2
//        self.img_user_dp.layer.masksToBounds = true
        
        if let pro_pic =  AppTheme.getLoginDetails().object(forKey: "profile_pic") as? String {
            let url = URL(string:pro_pic)
            let data = try? Data(contentsOf: url!)
            if data != nil {
                imgUser?.image = UIImage(data:data!)
            }
        }
        video_user_post_video.isHidden = true
        img_user_image_post.isHidden = true
        
        if isShowComment == true {
            constraintCommentHeigth.constant = 45
            viewComment.isHidden = false
        } else {
            constraintCommentHeigth.constant = 0
            viewComment.isHidden = true
        }
        
        self.getCommentsList()
    }

    @objc func leftBarBackButton(){
        if FeedsCommentVC.isFromUser == true {
            HomeVC.shouldHideNavBar = true
        } else {
            HomeVC.shouldHideNavBar = false
        }
        self.navigationController?.popViewController(animated: true)
    }
  
    override func viewWillAppear(_ animated: Bool) {
        
        if let str_f_type =  dicData .value(forKey: "file_type") as? String{
            if str_f_type == "text" {
                img_user_post_height.constant = 0
                video_user_post_height.constant = 0
            } else {
                if let str_file_url : String = dicData .value(forKey: "file_name") as? String{
                    let url = URL(string:str_file_url)
                    if verifyUrl(str_file_url) == true {
                        
                        if str_f_type == "video"{
                            
                            video_user_post_video.isHidden = false
                            img_user_image_post.isHidden = true
                            video_user_post_height.constant = 81
                            let videoURL = url
                            video_user_post_video.setVideoURL(videoURL!)
                        } else{
                           video_user_post_video.isHidden = true
                            img_user_image_post.isHidden = false
                            img_user_post_height.constant = 81
                            let url = url
                            
                            img_user_image_post.kf.setImage(with: url!, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: {
                                (image, error, cacheType, imageURL) in
                                print(image?.description ?? "")
                            })

                        }
                    }
                }else{
                    img_user_post_height.constant = 0
                    video_user_post_height.constant = 0
                }
            }
        }
    }
    func getCommentsList()
    {
        if (!isNetworkAvailable) {
            return
        }
//        if isRechableToInternet() == false{
//            return
//        }
        if let str_flight_id : String = dicData["flight_id"]! as? String{
            strFlighId = str_flight_id
        }
        if strPostsId != ""  {
            if let str_user_id =  AppTheme.getLoginDetails().object(forKey: "id") as? String{
                let str : String = str_user_id
                
                let parameters : [String: AnyObject]!
                parameters = ["posts_id" : self.strPostsId as AnyObject,
                              "user_id" : str as AnyObject]
                
                AppTheme().callGetService(String(format: "%@posts_details", AppUrl), param: parameters) {(result, data) -> Void in                                                                                  ///Call services
                    
                    if result == "success"
                    {
                        self.loading.dismiss()
                        self.respDict = data as! NSDictionary
                        print(self.respDict)
                        if let arrResponse: NSArray = self.respDict["response"] as? NSArray
                        {
                            self.arrOfResponseKey = arrResponse.mutableCopy() as! NSMutableArray
                        }
                        if let arrResponse: NSMutableArray = self.respDict["response"] as? NSMutableArray{
                            self.arrOfResponseKey = arrResponse
                        }
                
                        for element in self.arrOfResponseKey {
                            self.dict_detail = element as! NSDictionary
                            self.updateValues(self.dict_detail)
                        }
                        self.tblCommentsList.isHidden = false
                        self.tblCommentsList.reloadData()
                    } else{
                        self.loading.dismiss()
                        _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                    }
                }
            }
        }
    }
    func updateValues(_ dic_data : NSDictionary) {
        
        if let str_user_name = dic_data .value(forKey: "username") as? String {
            lbl_user_name.text = str_user_name
        }
        if let str_time_stamp =  dic_data .value(forKey: "timestamp") as? String{
            lbl_timestamp.text = str_time_stamp
        }
        if let str_total_like =  dic_data .value(forKey: "total_like") as? String{
            lbl_post_total_like.text = str_total_like
        }
        if let str_total_comment =  dic_data .value(forKey: "total_comment") as? String{
            lbl_total_post_comment.text = str_total_comment
        }
        if let str_user_post = dic_data .value(forKey: "posts") as? String {
            lbl_user_post.text = str_user_post
        }
        if let str_user_profile_pic =  dic_data .value(forKey: "profile_pic") as? String{
            let url = URL(string:str_user_profile_pic)
            if url != nil {
            img_user_dp.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                (image, error, cacheType, imageURL) in
                print(image?.description ?? "")
            })
                }
//           img_user_dp.kf_setImageWithURL(url, placeholderImage: UIImage(named: "ic_user.png") , optionsInfo: nil, progressBlock: nil, completionHandler: {
//                (image, error, cacheType, imageURL) in
//                print(image?.description)
//            })
        }
        
        if let str_file_type : String = dic_data .value(forKey: "file_type") as? String {
          //  strFileType = str_file_type
        }
        if let str_file_type : String = dic_data .value(forKey: "file_name") as? String
        {
            let url = URL(string:str_file_type)
            if verifyUrl(str_file_type) == true
            {
                if str_file_type == "video"{
                    video_user_post_video.isHidden = false
                    img_user_image_post.isHidden = true
                    video_user_post_height.constant = 81
                    let videoURL = url
                    video_user_post_video.setVideoURL(videoURL!)
                } else{
                    video_user_post_video.isHidden = true
                    img_user_image_post.isHidden = false
                   img_user_post_height.constant = 81
                    
                    let url = url
                    
                    img_user_image_post.kf.setImage(with: url!, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: {
                        (image, error, cacheType, imageURL) in
                        print(image?.description ?? "")
                    })
                    
//                    img_user_image_post.kf_setImageWithURL(url!, placeholderImage: nil, optionsInfo: nil , progressBlock: nil , completionHandler: { (image, error, cacheType, imageURL) in
//                        print(image?.description)
//                    } )
                }
            }else{
                img_user_post_height.constant = 0
                video_user_post_height.constant = 0
            }
        }
        
        var rating : NSInteger = 0
        if let strRating: String = dic_data["rating"] as? String
        {
            if strRating == ""{
                
            } else{
                rating = NSInteger(strRating)!
            }
        }
        else if let strRating: NSInteger = dic_data["rating"] as? NSInteger{
            rating = strRating
        }
        if rating == 0 {
            img_user_rating.image = nil
        }
        if rating == 1
        {
            img_user_rating.image = UIImage(named: "good_on.png")
        }
        else if rating == 2
        {
            img_user_rating.image = UIImage(named: "bad_on.png")
        }
        else if rating == 3
        {
            img_user_rating.image = UIImage(named: "bad.png")
        }
        if let strRating: String = dic_data["like_by_me"] as? String
        {
            likeByMe = NSInteger(strRating)!
        }
        else if let strRating: NSInteger = dic_data["like_by_me"] as? NSInteger
        {
            likeByMe = strRating
        }
        if likeByMe == 1
        {
            btn_like.setImage(UIImage(named: "img_like_on.png"), for: UIControlState())
        }else
        {
            btn_like.setImage(UIImage(named: "img_dislike.png"), for: UIControlState())
        }
        if let str_user_id : String = dic_data["user_id"] as? String {
            
            if let str_logged_user_id : String = AppTheme.getLoginDetails().value(forKey: "id") as? String{
                if (str_user_id == str_logged_user_id) {
                    btn_post_delete.isHidden = false
                    btn_post_delete.isUserInteractionEnabled = true
                } else
                {
                    btn_post_delete.isHidden = true
                    btn_post_delete.isUserInteractionEnabled = false
                }
            }
        }
        if let arr_comments = dic_data .value(forKey: "comments") as? NSArray {
            arr_total_comment = arr_comments.mutableCopy() as! NSMutableArray
            self.tblCommentsList.reloadData()
        }
        
        if let strCommentOnOff = dic_data .value(forKey: "commentsoff") as? String {
            
            if strCommentOnOff == "0" {
                tblCommentsList.isHidden = false
                constraintCommentHeigth.constant = 45
                viewComment.isHidden = false
            } else {
                constraintCommentHeigth.constant = 0
                viewComment.isHidden = true
                tblCommentsList.isHidden = true

            }
        }
        
        if let arr_like = dic_data .value(forKey: "likes") as? NSArray{
            arr_total_like = arr_like .mutableCopy() as! NSMutableArray
            for element in arr_total_like {
                let dic_like : NSDictionary = element as! NSDictionary
                if let str_like_image = dic_like .value(forKey: "profile_pic") as? String{
                    arr_total_like_images.add(str_like_image)
                }
            }
            if arr_total_like_images != nil {
                
//                img_post_like_one.layer.cornerRadius = 15
//                img_post_like_two.layer.cornerRadius = 12.5
//                img_post_like_three.layer.cornerRadius = 12.5
//                img_post_like_four.layer.cornerRadius = 12.5
//
//                img_post_like_one.layer.masksToBounds = true
//                img_post_like_two.layer.masksToBounds = true
//                img_post_like_three.layer.masksToBounds = true
//                img_post_like_four.layer.masksToBounds = true
                
                if arr_total_like_images.count == 4 || arr_total_like_images.count == 3 || arr_total_like_images.count == 2 || arr_total_like_images.count == 1 {
                    
                    if arr_total_like_images.count >= 1 {
                        if arr_total_like_images.count == 1 {
                            contraint_width_two.constant = 0.0
                            contraint_width_three.constant = 0.0
                            constraint_width_fourth.constant = 0.0
                        }
                        if let str_first_image = arr_total_like_images.object(at: 0) as? String {
                            let url = URL(string:str_first_image)
                            if url != nil {
                                img_post_like_one.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                                    (image, error, cacheType, imageURL) in
                                    print(image?.description ?? "")
                                })
                            }
                            
                            
                            
//                            img_post_like_one.kf_setImageWithURL(url, placeholderImage: UIImage(named: "ic_user.png") , optionsInfo: nil, progressBlock: nil, completionHandler: {
//                                (image, error, cacheType, imageURL) in
//                                print(image?.description)
//                            })
                        }
                        if arr_total_like_images.count >= 2  {
                            if arr_total_like_images.count == 2 {
                                contraint_width_three.constant = 0.0
                                constraint_width_fourth.constant = 0.0
                            }
                            if let str_second_image = arr_total_like_images.object(at: 1) as? String {
                                let url = URL(string:str_second_image)
                                
                                img_post_like_two.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                                    (image, error, cacheType, imageURL) in
                                    print(image?.description ?? "")
                                })
                                
                                
//                                img_post_like_two.kf_setImageWithURL(url, placeholderImage: UIImage(named: "ic_user.png") , optionsInfo: nil, progressBlock: nil, completionHandler: {
//                                    (image, error, cacheType, imageURL) in
//                                    print(image?.description)
//                                })
                            }
                            if arr_total_like_images.count >= 3 {
                                if arr_total_like_images.count == 3 {
                                    constraint_width_fourth.constant = 0.0
                                }
                                if let str_three_image = arr_total_like_images.object(at: 2) as? String {
                                    let url = URL(string:str_three_image)
                                    
                                    img_post_like_three.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                                        (image, error, cacheType, imageURL) in
                                        print(image?.description ?? "")
                                    })
                                    
                                    
//                                    img_post_like_three.kf_setImageWithURL(url, placeholderImage: UIImage(named: "ic_user.png") , optionsInfo: nil, progressBlock: nil, completionHandler: {
//                                        (image, error, cacheType, imageURL) in
//                                        print(image?.description)
//                                    })
                                }
                                if arr_total_like_images.count == 4 {
                                    if let str_fourth_image = arr_total_like_images.object(at: 3) as? String {
                                        let url = URL(string:str_fourth_image)
                                       
                                        img_post_like_four.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                                            (image, error, cacheType, imageURL) in
                                            print(image?.description ?? "")
                                        })
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                leading_constraint.constant = 0.0
                contraint_width_one.constant = 0.0
                contraint_width_two.constant = 0.0
                contraint_width_three.constant = 0.0
                constraint_width_fourth.constant = 0.0
            }
        }
    }
    
    
    // MARK: -  UItextView Delegate
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == txtCommentField
        {
            if txtCommentField.text == "Comment here.." {
                txtCommentField.text = ""
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtCommentField
        {
            if txtCommentField.text == "Comment here.." {
                txtCommentField.text = ""
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == txtCommentField
        {
            if txtCommentField.text == "" {
                txtCommentField.text = "Comment here.."
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.txtCommentField.resignFirstResponder()
    }
    
    
    //========================================================
    // MARK :- Button actions for Delete, share, like-dislike, and more
    //========================================================

    @IBAction func btn_delete_post(_ sender: AnyObject) {
        let refreshAlert = UIAlertController(title: "Delete post", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            
            if let str_post_id : String = self.dict_detail["post_id"]! as? String{
                self.strPostsId = str_post_id
                 self.deleteSelectedPost(str_post_id)
            }
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func deleteSelectedPost(_ post_id : String)  {
        
        if (!isNetworkAvailable) {
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(post_id, forKey:"post_id")
        
        AppTheme().callPostService(String(format: "%@post_delete", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Post deleted!", showInView: self.view )
                //self.getUserStatusList()
            }else{
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: data as! String, showInView: self.view )
            }
        }
    }
    
    @IBAction func btn_share(_ sender: AnyObject) {
        var data : Data = Data()
        var imgShare : UIImage = UIImage()
        var textToShare : String = String()
        
        if let str_textToShare : String = dict_detail["posts"]! as? String{
            textToShare = str_textToShare
            if let str_img_share : String = dict_detail["file_name"]! as? String {
                if str_img_share != "" {
                    let urlNew:String = str_img_share.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    let url  = URL(string: urlNew)
                    data = try! Data(contentsOf: url!)
                    imgShare = UIImage(data: data)!
                } else {
                    
                }
            }
        }
        if textToShare != "" && imgShare != nil {
            shareImageText(textToShare, image: imgShare)
        }
        if textToShare == "" && imgShare != nil {
            shareImageText(textToShare, image: imgShare)
        }
        if textToShare != "" && imgShare == nil {
            shareImageText(textToShare, image: imgShare)
        }
    }
    
    func shareImageText(_ text: String, image: UIImage){
        let objects2Share = [text, image] as [Any]
        let activityVC = UIActivityViewController(activityItems: objects2Share , applicationActivities: nil)
        
        let excludeActivities = [UIActivityType.airDrop, UIActivityType.copyToPasteboard , UIActivityType.mail, UIActivityType.addToReadingList]
        activityVC.excludedActivityTypes = excludeActivities
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func btn_like_dislike(_ sender: AnyObject) {
        
//        let parameters : [String: AnyObject]!
        let paraDict = NSMutableDictionary()
        
        if let str_user_id : String = self.dict_detail["user_id"]! as? String{
            strUserId = str_user_id
        }
        if let str_post_id : String = self.dict_detail["post_id"]! as? String{
            strPostsId = str_post_id
        }
        if likeByMe == 1{
            paraDict.setValue("", forKey:"flight_id")
            paraDict.setValue(strPostsId, forKey:"posts_id")
            paraDict.setValue(strUserId, forKey:"user_id")
            paraDict.setValue("0", forKey:"status")
            
        } else{
            paraDict.setValue("", forKey:"flight_id")
            paraDict.setValue(strPostsId, forKey:"posts_id")
            paraDict.setValue(strUserId, forKey:"user_id")
            paraDict.setValue("1", forKey:"status")
        }
        
        AppTheme().callPostService(String(format: "%@posts_like", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                print(data)
                self.loading.dismiss()
                if data.value(forKey: "message") as? String == "post unlike" {
                    self.likeByMe = 0
                    if let str_total_like =  self.dict_detail .value(forKey: "total_like") as? String{
                        self.lbl_post_total_like.text = String(Int(str_total_like)! - 1)
                    }
                   self.btn_like.setImage(UIImage(named: "img_dislike.png"), for: UIControlState())
                } else{
                   self.likeByMe = 1
                    if let str_total_like =  self.dict_detail .value(forKey: "total_like") as? String{
                        self.lbl_post_total_like.text = String(Int(str_total_like)! + 1)
                    }
                  self.btn_like.setImage(UIImage(named: "img_like_on.png"), for: UIControlState())
                }
            }
        }
    }
    @IBAction func btn_more(_ sender: AnyObject) {
        var destVc : UserCommentsListController! // = <#value#>
        destVc = self.storyboard?.instantiateViewController(withIdentifier: "UserCommentsListController") as! UserCommentsListController
        destVc.arr_like = arr_total_like
        self.navigationController?.pushViewController(destVc, animated: true)
    }
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
        if self.arr_total_comment != nil {
                row =  self.arr_total_comment.count
                return row
            } else{
                return 0
            }
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
   {
    self.loading.dismiss()
    let dic : NSDictionary = self.arr_total_comment.object(at: indexPath.row) as! NSDictionary
    
    var cell: CommentCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? CommentCell
    
    if cell == nil {
        cell = CommentCell(style: UITableViewCellStyle.default, reuseIdentifier: identifier)
    }
    
    if let str_first_name : String = dic["first_name"] as? String{
        cell.lblSubName.text = str_first_name
    }
    if let str_post: String = dic["comment"] as? String{
        cell.lbl_post.text = str_post
    }
    if let str_time: String = dic["create_date"] as? String{
        cell.lbl_date.text = str_time
    }
    if let str_profile_pic : String = dic["profile_pic"] as? String {
        let url = URL(string:str_profile_pic)
        
        if url != nil {
            cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                (image, error, cacheType, imageURL) in
                print(image?.description ?? "")
            })
        }
//        cell.imgUser.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
    }
    return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var destVC = UserChatOnCommentController()
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
        destVC.dic = self.arr_total_comment.object(at: indexPath.row) as! NSDictionary
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
//===============================
    //MARK: Post comment action
//===============================
    
    @IBAction func btnPostComment(_ sender: AnyObject)
    {
       self.startProgressBar()
        
        if (!isNetworkAvailable) {
            self.loading.dismiss()

            return
        }

        if let str_user_id : String = AppTheme.getLoginDetails().value(forKey: "id") as? String{
            strUserId = str_user_id
            if self.txtCommentField.text != "" && self.txtCommentField.text != nil
            {
                let paraDict = NSMutableDictionary()
                paraDict.setValue(strUserId, forKey:"user_id")
                paraDict.setValue(strFlighId, forKey:"flight_id")
                paraDict.setValue(strPostsId, forKey:"posts_id")
                paraDict.setValue(txtCommentField.text, forKey:"comment")
                
                AppTheme().callPostService(String(format: "%@posts_comment?", AppUrl), param: paraDict) {(result, data) -> Void in
                    if result == "success"
                    {
                        self.loading.dismiss()
                        self.txtCommentField.text = ""
                        self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Comment sent", showInView: self.view)
                        self.tblCommentsList.isHidden = false
                        self.getCommentsList()
                    }else {
                        self.loading.dismiss()
                        self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
                    }
                }
            } else{
                self.loading.dismiss()
                self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please write comment!", showInView: self.view)
            }
        }
    }
//==============================
    //MARK: Loader method
//==============================
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    //Check url nil or not
    func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if urlString == "" || urlString.isEmpty{
                return false
            }else{
                if let url = URL(string: urlString) {
                    // check if your application can open the NSURL instance
                    return UIApplication.shared.canOpenURL(url)
                }
            }
        }
        return false
    }
}
