//
//  FlightTransportGraphController.swift
//  Gupa Network
//
//  Created by MWM23 on 12/17/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
//import Charts
//import Charts
//import Charts

class FlightTransportGraphController: UIViewController {
    struct Sale {
        var month: String
        var value: Double
    }
    var strCheck: String! // = <#value#>
    var flightHotelDetailDict: NSDictionary!
    var strFlightId : String! // = <#value#>
    var values : NSMutableArray = NSMutableArray()
   //    var strFlightId: NSString?
    var strBusId: NSString?
    @IBOutlet weak var view_barChartFligthOrBus: BarChartView!
    @IBOutlet weak var view_barChartAirlineOrTransport: BarChartView!
    @IBOutlet weak var lbl_title_FlightOrBus : UILabel!
    @IBOutlet weak var lbl_title_AirlineOrTransport : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()                     // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnLeftBar.addTarget(self, action: #selector(FlightTransportGraphController.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()        //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        let btnRighBar: UIButton = UIButton ()
        btnRighBar.setImage(UIImage(named: "Home-icon.png"), for: UIControlState())
        btnRighBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnRighBar.addTarget(self, action: #selector(FlightTransportGraphController.rightBarBackButton), for: .touchUpInside)
        let rightBarButton:UIBarButtonItem = UIBarButtonItem()       //.....Set right button
        rightBarButton.customView = btnRighBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        
        
        if strCheck != "" && strCheck == "comeFromTransportRating" {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 440, height: 44))
            label.backgroundColor = UIColor.clear
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.center
            label.text = "Road Transport Ratings Chart"
            label.textColor = UIColor.white
            self.navigationItem.titleView = label
            view_barChartFligthOrBus.descriptionText = ""
            view_barChartFligthOrBus.xAxis.labelPosition = .bottom
//            view_barChartFligthOrBus.xAxis.setLabelsToSkip(0)
            view_barChartFligthOrBus.leftAxis.axisMinValue = 0.0
            view_barChartFligthOrBus.leftAxis.axisMaxValue = 10.0
            view_barChartFligthOrBus.rightAxis.enabled = false
            view_barChartFligthOrBus.xAxis.drawGridLinesEnabled = false
            view_barChartFligthOrBus.legend.enabled = true
            view_barChartFligthOrBus.scaleYEnabled = true
            view_barChartFligthOrBus.scaleXEnabled = true
            view_barChartFligthOrBus.pinchZoomEnabled = false
            view_barChartFligthOrBus.doubleTapToZoomEnabled = false
            view_barChartFligthOrBus.highlighter = nil
             let months = ["Exellent", "Good", "Bad"]
            var sales = [Sale]()
            var j : NSInteger = 0
            var ratingGood : NSInteger = 0
            var ratingBad : NSInteger = 0
            var ratingUgly : NSInteger = 0
            
            if self.flightHotelDetailDict.count != 0 {
                if let strGoodRating: String = self.flightHotelDetailDict["bus_good"] as? String    //Check for string/int
                {
                    ratingGood = NSInteger(strGoodRating)!
                }
                else if let strGoodRating: NSInteger = self.flightHotelDetailDict["bus_good"] as? NSInteger
                {
                    ratingGood = strGoodRating
                }
                if let strBadRating: String = self.flightHotelDetailDict["bus_bad"] as? String
                {
                    ratingBad = NSInteger(strBadRating)!
                }
                else if let strBadRating: NSInteger = self.flightHotelDetailDict["bus_bad"] as? NSInteger
                {
                    ratingBad = strBadRating
                }
                if let strUglyRating: String = self.flightHotelDetailDict["bus_ugly"] as? String
                {
                    ratingUgly = NSInteger(strUglyRating)!
                }
                else if let strUglyRating: NSInteger = self.flightHotelDetailDict["bus_ugly"] as? NSInteger
                {
                    ratingUgly = strUglyRating
                }
                //Create chart for Flight indivisual
                self.values = [ratingGood, ratingBad, ratingUgly]
                if let str_hotel_name : String = self.flightHotelDetailDict["from"] as? String {
                    if let str_to : String = self.flightHotelDetailDict["to"] as? String  {
                        lbl_title_FlightOrBus.text = " BUS: \(str_hotel_name) To \(str_to)"
                    }
                }
                if let str_hotel_name : String = self.flightHotelDetailDict["source_station"] as? String {
                    if let str_to : String = self.flightHotelDetailDict["destination_station"] as? String  {
                        lbl_title_FlightOrBus.text = " BUS: \(str_hotel_name) To \(str_to)"
                    }
                }
                
                for month in months {
                    let sale = Sale(month: month, value:Double(truncating: values.object(at: j) as! NSNumber))
                    sales.append(sale)
                    j += 1
                }
                // Initialize an array to store chart data entries (values; y axis)
                var salesEntries = [ChartDataEntry]()
                
                // Initialize an array to store months (labels; x axis)
                var salesMonths = [String]()
                
                var i = 0
                for sale in sales {
                    // Create single chart data entry and append it to the array
                    let saleEntry = BarChartDataEntry.init(x: Double(i), yValues: [sale.value])
                        //BarChartDataEntry(value: sale.value, xIndex: i)
                    salesEntries.append(saleEntry)
                    // Append the month to the array
                    salesMonths.append(sale.month)
                    i += 1
                }
                // Create bar chart data set containing salesEntries
                let chartDataSet = BarChartDataSet(values: salesEntries, label: "Profit")
                chartDataSet.colors = [NSUIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1),NSUIColor(red: 0, green: 128/255, blue: 0, alpha: 1), NSUIColor.red]
                // Create bar chart data with data set and array with values for x axis
                let chartData = BarChartData.init(dataSets: [chartDataSet])
                    //BarChartData(xVals: salesMonths, dataSets: [chartDataSet])
                // Set bar chart data to previously created data
                view_barChartFligthOrBus.data = chartData
                // Animation
                
                //easeInOutQuartEaseInOutQuart
                
                view_barChartFligthOrBus.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuart)
                
                airlingTransportChart()
            }
           
        }else if strCheck == "comeFromAirportService" || strCheck == "comeFromHotel"{
            view_barChartFligthOrBus.descriptionText = ""
            view_barChartFligthOrBus.xAxis.labelPosition = .bottom
//            view_barChartFligthOrBus.xAxis.setLabelsToSkip(0)
            view_barChartFligthOrBus.leftAxis.axisMinValue = 0.0
            view_barChartFligthOrBus.leftAxis.axisMaxValue = 10.0
            view_barChartFligthOrBus.rightAxis.enabled = false
            view_barChartFligthOrBus.xAxis.drawGridLinesEnabled = false
            view_barChartFligthOrBus.legend.enabled = true
            view_barChartFligthOrBus.scaleYEnabled = true
            view_barChartFligthOrBus.scaleXEnabled = true
            view_barChartFligthOrBus.pinchZoomEnabled = false
            view_barChartFligthOrBus.doubleTapToZoomEnabled = false
            view_barChartFligthOrBus.highlighter = nil
            
            lbl_title_AirlineOrTransport.isHidden = true
            view_barChartAirlineOrTransport.isHidden = true
            if strCheck == "comeFromHotel" {
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: 440, height: 44))
                label.backgroundColor = UIColor.clear
                label.numberOfLines = 0
                label.textAlignment = NSTextAlignment.center
                label.text = "Hotel Rating Chart"
                label.textColor = UIColor.white
                self.navigationItem.titleView = label
                flightChart()
                
            }else{
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: 440, height: 44))
                label.backgroundColor = UIColor.clear
                label.numberOfLines = 0
                label.textAlignment = NSTextAlignment.center
                label.text = "Flight Ratings Chart"
                label.textColor = UIColor.white
                self.navigationItem.titleView = label
            flightChart_airportService()
            }
        }else{
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 440, height: 44))
            label.backgroundColor = UIColor.clear
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.center
            label.text = "Flight Ratings Chart"
            label.textColor = UIColor.white
            self.navigationItem.titleView = label
            view_barChartFligthOrBus.descriptionText = ""
            view_barChartFligthOrBus.xAxis.labelPosition = .bottom
//            view_barChartFligthOrBus.xAxis.setLabelsToSkip(0)
            view_barChartFligthOrBus.leftAxis.axisMinValue = 0.0
            view_barChartFligthOrBus.leftAxis.axisMaxValue = 10.0
            view_barChartFligthOrBus.rightAxis.enabled = false
            view_barChartFligthOrBus.xAxis.drawGridLinesEnabled = false
            view_barChartFligthOrBus.legend.enabled = true
            view_barChartFligthOrBus.scaleYEnabled = true
            view_barChartFligthOrBus.scaleXEnabled = true
            view_barChartFligthOrBus.pinchZoomEnabled = false
            view_barChartFligthOrBus.doubleTapToZoomEnabled = false
            view_barChartFligthOrBus.highlighter = nil
            airlingChart()
            flightChart()
        }
        // Customization       
        // Do any additional setup after loading the view.
    }
    func airlingTransportChart(){
        view_barChartAirlineOrTransport.descriptionText = ""
        view_barChartAirlineOrTransport.xAxis.labelPosition = .bottom
//        view_barChartAirlineOrTransport.xAxis.setLabelsToSkip(0)
        view_barChartAirlineOrTransport.leftAxis.axisMinValue = 0.0
        view_barChartAirlineOrTransport.leftAxis.axisMaxValue = 10.0
        view_barChartAirlineOrTransport.rightAxis.enabled = false
        view_barChartAirlineOrTransport.xAxis.drawGridLinesEnabled = false
        view_barChartAirlineOrTransport.legend.enabled = true
        view_barChartAirlineOrTransport.scaleYEnabled = true
        view_barChartAirlineOrTransport.scaleXEnabled = true
        view_barChartAirlineOrTransport.pinchZoomEnabled = false
        view_barChartAirlineOrTransport.doubleTapToZoomEnabled = false
        view_barChartAirlineOrTransport.highlighter = nil
        
        let months = ["Exellent", "Good", "Bad"]
        var sales = [Sale]()
        var j : NSInteger = 0
       
        var ratingGoodAirline : NSInteger = 0
        var ratingBadAirline : NSInteger = 0
        var ratingUglyAirline : NSInteger = 0
        
        if let strGoodRatingAirline: String = self.flightHotelDetailDict["transport_Good"] as? String    //Check for string/int
        {
            ratingGoodAirline = NSInteger(strGoodRatingAirline)!
        }
        else if let strGoodRatingAirline: NSInteger = self.flightHotelDetailDict["transport_Good"] as? NSInteger
        {
            ratingGoodAirline = strGoodRatingAirline
        }
        if let strBadRatingAirline: String = self.flightHotelDetailDict["transport_Bad"] as? String
        {
            ratingBadAirline = NSInteger(strBadRatingAirline)!
        }
        else if let strBadRatingAirline: NSInteger = self.flightHotelDetailDict["transport_Bad"] as? NSInteger
        {
            ratingBadAirline = strBadRatingAirline
        }
        if let strUglyRatingAirline: String = self.flightHotelDetailDict["transport_Ugly"] as? String
        {
            ratingUglyAirline = NSInteger(strUglyRatingAirline)!
        }
        else if let strUglyRatingAirline: NSInteger = self.flightHotelDetailDict["transport_Ugly"] as? NSInteger
        {
            ratingUglyAirline = strUglyRatingAirline
        }
        
       values = [ratingGoodAirline, ratingBadAirline, ratingUglyAirline]
        //Create chart for Flight indivisual
       
        if let str_hotel_name : String = self.flightHotelDetailDict["transport_name"] as? String {
            lbl_title_AirlineOrTransport.text = " TRANSPORT COMPANY: \(str_hotel_name)"
        }
        
        
        for month in months {
            let sale = Sale(month: month, value:Double(values.object(at: j) as! NSNumber))
            sales.append(sale)
            j += 1
        }
        // Initialize an array to store chart data entries (values; y axis)
        var salesEntries_airlingOrTrans = [ChartDataEntry]()
        
        // Initialize an array to store months (labels; x axis)
        var salesMonths_airlingOrTrans = [String]()
        
        var k = 0
        for sale in sales {
            // Create single chart data entry and append it to the array
            let saleEntry = BarChartDataEntry.init(x: Double(k), yValues: [sale.value])
            //(value: sale.value, xIndex: k)
            salesEntries_airlingOrTrans.append(saleEntry)
            
            // Append the month to the array
            salesMonths_airlingOrTrans.append(sale.month)
            
            k += 1
        }
        
        // Create bar chart data set containing salesEntries
        let chartDataSet_airlingOrTrans = BarChartDataSet(values: salesEntries_airlingOrTrans, label: "Profit")
        chartDataSet_airlingOrTrans.colors = [NSUIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1),NSUIColor(red: 0, green: 128/255, blue: 0, alpha: 1), NSUIColor.red]
        // Create bar chart data with data set and array with values for x axis
        
        
//        let chartData_airlingOrTrans = BarChartData(xVals: salesMonths_airlingOrTrans, dataSets: [chartDataSet_airlingOrTrans])
        
        let chartData_airlingOrTrans = BarChartData.init(dataSets: [chartDataSet_airlingOrTrans])
        
        // Set bar chart data to previously created data
        view_barChartAirlineOrTransport.data = chartData_airlingOrTrans
        // Animation
        view_barChartAirlineOrTransport.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuart)
    }
    
    
    func airlingChart() {
        view_barChartAirlineOrTransport.descriptionText = ""
        view_barChartAirlineOrTransport.xAxis.labelPosition = .bottom
//        view_barChartAirlineOrTransport.xAxis.setLabelsToSkip(0)
        view_barChartAirlineOrTransport.leftAxis.axisMinValue = 0.0
        view_barChartAirlineOrTransport.leftAxis.axisMaxValue = 10.0
        view_barChartAirlineOrTransport.rightAxis.enabled = false
        view_barChartAirlineOrTransport.xAxis.drawGridLinesEnabled = false
        view_barChartAirlineOrTransport.legend.enabled = true
        view_barChartAirlineOrTransport.scaleYEnabled = true
        view_barChartAirlineOrTransport.scaleXEnabled = true
        view_barChartAirlineOrTransport.pinchZoomEnabled = false
        view_barChartAirlineOrTransport.doubleTapToZoomEnabled = false
        view_barChartAirlineOrTransport.highlighter = nil
        
        let months = ["Exellent", "Good", "Bad"]
        var sales = [Sale]()
        var j : NSInteger = 0
        
        var ratingGoodAirline : NSInteger = 0
        var ratingBadAirline : NSInteger = 0
        var ratingUglyAirline : NSInteger = 0
        
        if let strGoodRatingAirline: String = self.flightHotelDetailDict["Airline_Good"] as? String    //Check for string/int
        {
            ratingGoodAirline = NSInteger(strGoodRatingAirline)!
        }
        else if let strGoodRatingAirline: NSInteger = self.flightHotelDetailDict["Airline_Good"] as? NSInteger
        {
            ratingGoodAirline = strGoodRatingAirline
        }
        if let strBadRatingAirline: String = self.flightHotelDetailDict["Airline_Bad"] as? String
        {
            ratingBadAirline = NSInteger(strBadRatingAirline)!
        }
        else if let strBadRatingAirline: NSInteger = self.flightHotelDetailDict["Airline_Bad"] as? NSInteger
        {
            ratingBadAirline = strBadRatingAirline
        }
        if let strUglyRatingAirline: String = self.flightHotelDetailDict["Airline_Ugly"] as? String
        {
            ratingUglyAirline = NSInteger(strUglyRatingAirline)!
        }else if let strUglyRatingAirline: NSInteger = self.flightHotelDetailDict["Airline_Ugly"] as? NSInteger
        {
            ratingUglyAirline = strUglyRatingAirline
        }
        
        values = [ratingGoodAirline, ratingBadAirline, ratingUglyAirline]
        //Create chart for Flight indivisual
        if strCheck == "" {
            if let str_hotel_name : String = self.flightHotelDetailDict["airline_name"] as? String {
                lbl_title_AirlineOrTransport.text = "AIRLINE: \(str_hotel_name)"
            }
        }else{
            if let str_hotel_name : String = self.flightHotelDetailDict["airline_name"] as? String {
                lbl_title_AirlineOrTransport.text = "AIRLINE: \(str_hotel_name)"
            }
        }
        for month in months {
            let sale = Sale(month: month, value:Double(values.object(at: j) as! NSNumber))
            sales.append(sale)
            j += 1
        }
        // Initialize an array to store chart data entries (values; y axis)
        var salesEntries_airlingOrTrans = [ChartDataEntry]()
        
        // Initialize an array to store months (labels; x axis)
        var salesMonths_airlingOrTrans = [String]()
        
        var k = 0
        for sale in sales {
            // Create single chart data entry and append it to the array
            let saleEntry = BarChartDataEntry.init(x: Double(k), yValues: [sale.value])
            //(value: sale.value, xIndex: k)
            salesEntries_airlingOrTrans.append(saleEntry)
            
            // Append the month to the array
            salesMonths_airlingOrTrans.append(sale.month)
            
            k += 1
        }
        
        // Create bar chart data set containing salesEntries
        let chartDataSet_airlingOrTrans = BarChartDataSet(values: salesEntries_airlingOrTrans, label: "Profit")
        chartDataSet_airlingOrTrans.colors = [NSUIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1),NSUIColor(red: 0, green: 128/255, blue: 0, alpha: 1), NSUIColor.red]
        // Create bar chart data with data set and array with values for x axis
//        let chartData_airlingOrTrans = BarChartData(xVals: salesMonths_airlingOrTrans, dataSets: [chartDataSet_airlingOrTrans])
        
        
         let chartData_airlingOrTrans = BarChartData.init(dataSets: [chartDataSet_airlingOrTrans])
        
        // Set bar chart data to previously created data
        view_barChartAirlineOrTransport.data = chartData_airlingOrTrans
        // Animation
        view_barChartAirlineOrTransport.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuart)
    }
    
    
    func flightChart(){
        view_barChartFligthOrBus.descriptionText = ""
        view_barChartFligthOrBus.xAxis.labelPosition = .bottom
//        view_barChartFligthOrBus.xAxis.setLabelsToSkip(0)
        view_barChartFligthOrBus.leftAxis.axisMinValue = 0.0
        view_barChartFligthOrBus.leftAxis.axisMaxValue = 10.0
        view_barChartFligthOrBus.rightAxis.enabled = false
        view_barChartFligthOrBus.xAxis.drawGridLinesEnabled = false
        view_barChartFligthOrBus.legend.enabled = true
        view_barChartFligthOrBus.scaleYEnabled = true
        view_barChartFligthOrBus.scaleXEnabled = true
        view_barChartFligthOrBus.pinchZoomEnabled = false
        view_barChartFligthOrBus.doubleTapToZoomEnabled = false
        view_barChartFligthOrBus.highlighter = nil
        let months = ["Exellent", "Good", "Bad"]
        var sales = [Sale]()
        var j : NSInteger = 0
        self.title = "Flight Ratings Chart"
        var ratingGood : NSInteger = 0
        var ratingBad : NSInteger = 0
        var ratingUgly : NSInteger = 0
        
        if let strGoodRating: String = self.flightHotelDetailDict["Good"] as? String    //Check for string/int
        {
            ratingGood = NSInteger(strGoodRating)!
        }
        else if let strGoodRating: NSInteger = self.flightHotelDetailDict["Good"] as? NSInteger
        {
            ratingGood = strGoodRating
        }
        if let strBadRating: String = self.flightHotelDetailDict["Bad"] as? String
        {
            ratingBad = NSInteger(strBadRating)!
        }
        else if let strBadRating: NSInteger = self.flightHotelDetailDict["Bad"] as? NSInteger
        {
            ratingBad = strBadRating
        }
        if let strUglyRating: String = self.flightHotelDetailDict["Ugly"] as? String
        {
            ratingUgly = NSInteger(strUglyRating)!
        }
        else if let strUglyRating: NSInteger = self.flightHotelDetailDict["Ugly"] as? NSInteger
        {
            ratingUgly = strUglyRating
        }
        //Create chart for Flight indivisual
        self.values = [ratingGood, ratingBad, ratingUgly]
        
        if strCheck == "comeFromHotel" {
            if let str_hotel_name = self.flightHotelDetailDict["hotel_name"] as? String  {
                lbl_title_FlightOrBus.text = "HOTEL: \(str_hotel_name)"
            }
        }else{
            if let str_hotel_name : String = self.flightHotelDetailDict["airline_name"] as? String {
                if let str_to : String = self.flightHotelDetailDict["flight_name"] as? String  {
                    lbl_title_FlightOrBus.text = "FLIGHT: \(str_hotel_name)\(str_to)"
                }
            }
        }
        for month in months {
            let sale = Sale(month: month, value:Double(values.object(at: j) as! NSNumber))
            sales.append(sale)
            j += 1
        }
        // Initialize an array to store chart data entries (values; y axis)
        var salesEntries = [ChartDataEntry]()
        
        // Initialize an array to store months (labels; x axis)
        var salesMonths = [String]()
        
        var i = 0
        for sale in sales {
            // Create single chart data entry and append it to the array
            let saleEntry = BarChartDataEntry.init(x: Double(i), yValues: [sale.value])
            //(value: sale.value, xIndex: i)
            salesEntries.append(saleEntry)
            // Append the month to the array
            salesMonths.append(sale.month)
            i += 1
        }
        // Create bar chart data set containing salesEntries
        let chartDataSet = BarChartDataSet(values: salesEntries, label: "Profit")
        chartDataSet.colors = [NSUIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1),NSUIColor(red: 0, green: 128/255, blue: 0, alpha: 1), NSUIColor.red]
        // Create bar chart data with data set and array with values for x axis
//        let chartData = BarChartData(xVals: salesMonths, dataSets: [chartDataSet])
        
        let chartData = BarChartData.init(dataSets: [chartDataSet])
        // Set bar chart data to previously created data
        view_barChartFligthOrBus.data = chartData
        // Animation
        view_barChartFligthOrBus.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuart)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func rightBarBackButton(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @objc func leftBarBackButton(){                //Helper method for right navigationbar button
        self.navigationController?.popViewController(animated: true)
    }

    
    func flightChart_airportService() {
        view_barChartFligthOrBus.descriptionText = ""
        view_barChartFligthOrBus.xAxis.labelPosition = .bottom
//        view_barChartFligthOrBus.xAxis.setLabelsToSkip(0)
        view_barChartFligthOrBus.leftAxis.axisMinValue = 0.0
        view_barChartFligthOrBus.leftAxis.axisMaxValue = 10.0
        view_barChartFligthOrBus.rightAxis.enabled = false
        view_barChartFligthOrBus.xAxis.drawGridLinesEnabled = false
        view_barChartFligthOrBus.legend.enabled = true
        view_barChartFligthOrBus.scaleYEnabled = true
        view_barChartFligthOrBus.scaleXEnabled = true
        view_barChartFligthOrBus.pinchZoomEnabled = false
        view_barChartFligthOrBus.doubleTapToZoomEnabled = false
        view_barChartFligthOrBus.highlighter = nil
        let months = ["Exellent", "Good", "Bad"]
        var sales = [Sale]()
        var j : NSInteger = 0
        self.title = "Flight Ratings Chart"
        var ratingGood : NSInteger = 0
        var ratingBad : NSInteger = 0
        var ratingUgly : NSInteger = 0
        
        if let strGoodRating: String = self.flightHotelDetailDict["good"] as? String    //Check for string/int
        {
            ratingGood = NSInteger(strGoodRating)!
        }
        else if let strGoodRating: NSInteger = self.flightHotelDetailDict["good"] as? NSInteger
        {
            ratingGood = strGoodRating
        }
        if let strBadRating: String = self.flightHotelDetailDict["bad"] as? String
        {
            ratingBad = NSInteger(strBadRating)!
        }
        else if let strBadRating: NSInteger = self.flightHotelDetailDict["bad"] as? NSInteger
        {
            ratingBad = strBadRating
        }
        if let strUglyRating: String = self.flightHotelDetailDict["ugly"] as? String
        {
            ratingUgly = NSInteger(strUglyRating)!
        }
        else if let strUglyRating: NSInteger = self.flightHotelDetailDict["ugly"] as? NSInteger
        {
            ratingUgly = strUglyRating
        }
        //Create chart for Flight indivisual
        self.values = [ratingGood, ratingBad, ratingUgly]
            if let str_to : String = self.flightHotelDetailDict["name"] as? String  {
                lbl_title_FlightOrBus.text = "\(str_to) Airport"
            }

        for month in months {
            let sale = Sale(month: month, value:Double(values.object(at: j) as! NSNumber))
            sales.append(sale)
            j += 1
        }
        // Initialize an array to store chart data entries (values; y axis)
        var salesEntries = [ChartDataEntry]()
        
        // Initialize an array to store months (labels; x axis)
        var salesMonths = [String]()
        
        var i = 0
        for sale in sales {
            // Create single chart data entry and append it to the array
            let saleEntry = BarChartDataEntry.init(x: Double(i), yValues: [sale.value])
            //(value: sale.value, xIndex: i)
            salesEntries.append(saleEntry)
            // Append the month to the array
            salesMonths.append(sale.month)
            i += 1
        }
        // Create bar chart data set containing salesEntries
        let chartDataSet = BarChartDataSet(values: salesEntries, label: "Profit")
        chartDataSet.colors = [NSUIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1),NSUIColor(red: 0, green: 128/255, blue: 0, alpha: 1), NSUIColor.red]
        // Create bar chart data with data set and array with values for x axis
//        let chartData = BarChartData(xVals: salesMonths, dataSets: [chartDataSet])
        let chartData = BarChartData.init(dataSets: [chartDataSet])
        // Set bar chart data to previously created data
        view_barChartFligthOrBus.data = chartData
        // Animation
        view_barChartFligthOrBus.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuart)
    }
}
