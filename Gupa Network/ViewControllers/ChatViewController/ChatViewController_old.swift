//
//  ChatViewController.swift
//  Gupa Network
//
//  Created by mac on 14/07/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher

class ChatViewController_old : UIViewController,  UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {
    
    @IBOutlet var tableView_chat: UITableView!
    @IBOutlet var btn_send_message: UIButton!
    @IBOutlet var textview_msg: UITextView!
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var str_my_id : String! // = <#value#>
    var str_others_id : String!
     var str_title : String!
    var respDict : NSDictionary! = NSDictionary()
    var timer: Timer!
    
    var indexpath: NSInteger!
    var arrOfResponseKey: NSMutableArray = NSMutableArray()
    
    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        appDelegate.centerNav = self.navigationController
        
        self.startProgressBar()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(ChatViewController_old.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton

        self.title = str_title
        tableView_chat.dataSource = self
        tableView_chat.delegate = self
        if str_my_id != "" && str_others_id != "" {
            if timer != nil {
                timer.invalidate()
                timer = nil
            }

            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ChatViewController_old.getChatWithTime), userInfo: nil, repeats: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userOnChat()
    }
    
    
    func getChat(_ fromId: String, toId: String) {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
//        let parameters : [String: AnyObject] = ["from_id" : toId as AnyObject,
//                                                "to_id" : fromId as AnyObject]
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(toId, forKey:"from_id")
        paraDict.setValue(fromId, forKey:"to_id")
        
        AppTheme().callPostService(String(format: "%@get_user_chat", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                let dicRes: NSDictionary = data as! NSDictionary
                if let tempArr: NSArray = dicRes["response"] as? NSArray
                {
                    self.arrOfResponseKey = tempArr.mutableCopy() as! NSMutableArray
                    if self.arrOfResponseKey.count == 0 || self.arrOfResponseKey.isEqual(nil)
                    {
                    }
                    else
                    {
                        self.arrOfResponseKey = tempArr.mutableCopy() as! NSMutableArray
                    }
                }
                else
                {
                    self.arrOfResponseKey = dicRes["response"] as! NSMutableArray
                }
            } else {
                self.loading.dismiss()
            }
           self.tableView_chat.reloadData()
        }
    }
    
    @IBAction func getChatWithTime() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(str_others_id, forKey:"from_id")
        paraDict.setValue(str_my_id, forKey:"to_id")
        
        AppTheme().callPostService(String(format: "%@get_user_chat", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                self.respDict = data as! NSDictionary
                
                if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray {
                    self.arrOfResponseKey = array_Response_mutable
                }
                self.tableView_chat.reloadData()
                if self.arrOfResponseKey.count != 0 {
                    let iPath = IndexPath(row: self.tableView_chat.numberOfRows(inSection: 0)-1,
                                          section: self.tableView_chat.numberOfSections-1)
                   self.tableView_chat.scrollToRow(at: iPath,at: UITableViewScrollPosition.bottom, animated: true)
                }
            } else {
                self.loading.dismiss()
           }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func leftBarBackButton(){
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        //Helper method for right navigationbar button
        self.navigationController?.popViewController(animated: true)
    }
    
    //========================================================
    //MARK: UITableview Datasource and delegate methods
    //========================================================
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row : Int = Int()
        if self.arrOfResponseKey.count != 0
        {
            if tableView == tableView_chat
            {
                row = self.arrOfResponseKey.count
            }
        }else{
            row = 0
        }
        return row
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // self.loading.dismiss()
        var cell : ChatTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
        if cell == nil {
            cell = ChatTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "ChatTableViewCell")
        }
        
        let dicRes: NSDictionary = arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        
        if let str_my_id_response = dicRes .value(forKey: "user_id") as? String {
            
            if str_my_id == str_my_id_response {
                cell.label_HisHerTime.isHidden = true
                cell.image_HisHer.isHidden = true
                cell.label_MyTime.isHidden = false
                cell.image_My.isHidden = false
                
                if let str_user_name : String = dicRes["message"] as? String{
                    cell.textview_Msg.text  = str_user_name
                }
                if let str_my_date  = dicRes["date"] as? String{
                    if let str_my_time = dicRes .value(forKey: "time")  as? String{
                        cell.label_MyTime.text  = "\(str_my_date)  \(str_my_time)"
                    }
                }
//                if let str_title = dicRes .valueForKey("username") as? String {
//                    self.title = str_title
//                }
                if let str_user_profile : String = dicRes["profile_pic"] as? String{
                    let url = URL(string:str_user_profile)
                    if url != nil {
                    cell.image_My.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                        (image, error, cacheType, imageURL) in
                        print(image?.description ?? "")
                     })
                    }
//                    cell.image_My.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
                }
            }
            if str_others_id == str_my_id_response {
                cell.label_MyTime.isHidden = true
                cell.image_My.isHidden = true
                cell.label_HisHerTime.isHidden = false
                cell.image_HisHer.isHidden = false
                
                if let str_user_name : String = dicRes["message"] as? String{
                    cell.textview_Msg.text  = str_user_name
                }
                if let str_my_date : String = dicRes["date"] as? String{
                    if let str_my_time = dicRes .value(forKey: "time")  as? String{
                        cell.label_HisHerTime.text  = "\(str_my_date)  \(str_my_time)"
                    }
                }
                if let str_user_profile : String = dicRes["profile_pic"] as? String{
                    let url = URL(string:str_user_profile)
                    if url != nil {
                    cell.image_HisHer.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user.png"), options: nil, progressBlock: nil, completionHandler: {
                        (image, error, cacheType, imageURL) in
                        print(image?.description ?? "")
                    })
                    }
//                    cell.image_HisHer.kf_setImageWithURL(url!, placeholderImage: UIImage(named: "ic_user.png"), optionsInfo: nil , progressBlock: nil , completionHandler: nil )
                }
            }
        }
        return cell
    }
    
    // MARK: -  UItextView Delegate
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == textview_msg{
            if textview_msg.text == "Enter your message.." {
                textview_msg.text = ""
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == textview_msg  {
            if textview_msg.text == "Enter your message.." {
                textview_msg.text = ""
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == textview_msg
        {
            if textview_msg.text == "" {
                textview_msg.text = "Enter your message.."
            }
        }
    }
    @IBAction func btn_send_chat(_ sender: AnyObject) {
        
          if self.textview_msg.text == nil || self.textview_msg.text == "" || self.textview_msg.text == "Enter your message.."{
        } else {
            
            if (!isNetworkAvailable) {
                Utility.showNetWorkAlert()
                return
            }
//            self.startProgressBar()
//            let parameters : [String: AnyObject] = ["from_id" : str_my_id  as AnyObject,
//                                                    "to_id" :  str_others_id as AnyObject,
//                                                    "message" :  self.textview_msg.text as AnyObject,
//                                                    "is_on_page" : "1" as AnyObject]
            
            let paraDict = NSMutableDictionary()
            paraDict.setValue(str_my_id, forKey:"from_id")
            paraDict.setValue(str_others_id, forKey:"to_id")
            paraDict.setValue(self.textview_msg.text, forKey:"message")
            paraDict.setValue("1", forKey:"is_on_page")
            
            AppTheme().callPostService(String(format: "%@user_chat", AppUrl), param: paraDict) {(result, data) -> Void in
                if result == "success"
                {
                self.textview_msg.text = "Enter your message.."
                  self.getChat(self.str_my_id, toId: self.str_others_id)
            }
         }
      }
    }
    
    //MARK: User check for online/offline
    func userOnChat() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
  
        let paraDict = NSMutableDictionary()
        paraDict.setValue(str_my_id, forKey:"from_id")
        paraDict.setValue(str_others_id, forKey:"to_id")
        paraDict.setValue("1", forKey:"status")
        
          AppTheme().callPostService(String(format: "%@user_online", AppUrl), param: paraDict) {(result, data) -> Void in
                if result == "success"
                {
                  print("Change status")
                }
            }
    }
    //==============================
    //MARK: Loader method
    //==============================
    func startProgressBar() {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }

}
