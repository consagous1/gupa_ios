//
//  BlockLIstVC.swift
//  Gupa Network
//
//  Created by Apple on 20/12/17.
//  Copyright © 2017 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class BlockLIstVC: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet var tblPrivacySettings    : UITableView!
    
    // MARK: - Initialization
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var respDict : NSDictionary = NSDictionary()
    
    var btnSearchUser:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
        //**>> Manage Navigation Bar
        self.title = "Blocked Users"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 69.0/255.0, green: 198.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.navigationItem.leftBarButtonItem = AppTheme.AddNavigationLeftBarButton(revealViewController())
        
        showRightNavigationBarButtonHide()
        
        //**>> API Call for Block List
        getBlockList_ApiCall()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation Bar Button
    
    func showRightNavigationBarButtonHide()
    {
        let imgChat:UIImage = UIImage(named: "Home-icon.png")!
        self.btnSearchUser = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnSearchUser.setImage(imgChat, for: UIControlState())
        btnSearchUser.addTarget(self, action: #selector(NotificationListController.homeAction), for: UIControlEvents.touchUpInside)
        let btnForChat:UIBarButtonItem = UIBarButtonItem(customView: btnSearchUser)
        
        // Get the second button's image
        let imgWriteReview:UIImage = UIImage(named: "ime_user_search.png")!
        // Create the second button
        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFollow.setImage(imgWriteReview, for: UIControlState())
        btnFollow.addTarget(self, action: #selector(NotificationListController.searchUser), for: UIControlEvents.touchUpInside)
        
        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 8.0
        
        // Set -7px of fixed space before the two UIBarButtonItems so that they are aligned to the edge
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -7.0
        
        // Add the rightBarButtonItems on the navigation bar
        navigationItem.rightBarButtonItems = [btnForChat, negativeSpace, btnForFollow]
    }
    
    @objc func homeAction(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @objc func searchUser(){
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchUserListController") as! SearchUserListController
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    

    @objc func blockedUserAction(_ btnBlock: UIButton) {
        
        let dic: NSDictionary = self.arrOfResponseKey.object(at: btnBlock.tag) as! NSDictionary
        
        var str_UserUnblockId = ""
        
        if let userUnblockId =  dic["unblock_ids"] as? String {
            str_UserUnblockId = userUnblockId
            print(str_UserUnblockId)
        }
        
        //*> create the alert
        let alert = UIAlertController(title:"Are you sure you want to unblock user?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        present(alert, animated: true, completion: nil)
        
        //*> add the actions (buttons)
        alert.addAction(UIAlertAction(title: "NO", style: .default, handler: { action in
        }))
        
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            
          self.unblockUser_ApiCall(str_UserUnblockId , selectedIndex: btnBlock.tag)
        }))
    }
    
    func getBlockList_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"ids")
        
        AppTheme().callPostService(String(format: "%@get_block_list", AppUrlNew), param: paraDict) {(result, data) -> Void in                                                                                  ///Call services
        
                if result == "success"
                {
                    self.loading.dismiss()
                    self.respDict = data as! NSDictionary
                    if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray{
                    self.arrOfResponseKey = array_Response_mutable
                    }
                    self.tblPrivacySettings.reloadData()
                } else {
                    self.loading.dismiss()
                    _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Server Error! Please check again later.", showInView: self.view)
            }
        }
    }
    
    func unblockUser_ApiCall(_ strBlockToId : String , selectedIndex: Int) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"ids")
        paraDict.setValue(strBlockToId, forKey:"unblock_ids")
        
        AppTheme().callPostService(String(format: "%@post_unblock", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                self.arrOfResponseKey.removeObject(at: selectedIndex)
                self.tblPrivacySettings.reloadData()
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
}
    
    //****************************************************
    // MARK: - UITableViewDataSource
    //****************************************************
    
extension BlockLIstVC : UITableViewDataSource {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrOfResponseKey.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
    let cell : BlockListCell = tableView.dequeueReusableCell(withIdentifier: "BlockListCell") as! BlockListCell
        
        let dic: NSDictionary = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        
        if let str_UserName =  dic["username"] as? String {
            cell.lblName.text = str_UserName
        }
        
        cell.btnBlockUnblock.addTarget(self, action: #selector(BlockLIstVC.blockedUserAction(_:)), for: UIControlEvents.touchUpInside)
        cell.btnBlockUnblock.tag = indexPath.row
        
        if let str_ProfileImg =  dic["profile_pic"] as? String {
            
            let url = URL(string:str_ProfileImg)
            
            cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_hotel_orange.png"), options: nil, progressBlock: nil, completionHandler: {
                (image, error, cacheType, imageURL) in
                print(image?.description ?? "")
            })
        }
        
        return cell
    }

}
