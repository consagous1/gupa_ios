//
//  AddController.swift
//  Gupa Network
//
//  Created by MWM23 on 1/19/17.
//  Copyright © 2017 mobiweb. All rights reserved.
//

import UIKit
import Kingfisher

class AddController: UIViewController {

    var dicData : NSDictionary!
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var textView_dis: UITextView!
    @IBOutlet weak var view_img: UIView!
    @IBOutlet weak var img_add: UIImageView!
    @IBOutlet weak var view_video: SJPlayerView!
    @IBOutlet weak var textView_hieght: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(AddController.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        self.title = "Add Vertiesment"
       
    }
    @objc func leftBarBackButton(){
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if dicData.count > 0 {
            if let str_title = dicData .value(forKey: "posts") as? String {
                lbl_title.text = str_title
            }
            if let str_dis =  dicData .value(forKey: "description") as? String{
                if str_dis == "" {
                    textView_hieght.constant = 0
                }else{
                    textView_hieght.constant = 135
                    textView_dis.text = str_dis
                }
            }
            if let str_file_type = dicData .value(forKey: "file_type") as? String {
                if let str_file_url : String = dicData .value(forKey: "file_name") as? String{
                    let url = URL(string:str_file_url)
                    if verifyUrl(str_file_url) == true {
                        if str_file_type == "video"{
                            self.view_img.isHidden = true
                            self.view_video.isHidden = false
                            view.bringSubview(toFront: view_video)
                            let videoURL = url
                            self.view_video.setVideoURL(videoURL!)
                        } else{
                            self.view_video.isHidden = true
                            self.view_img.isHidden = false
                            view.bringSubview(toFront: view_img)
                          
                            let url = url
                            
                            
                            self.img_add.kf.setImage(with: url, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
                                                            print(image?.description ?? "")
                                                        })
                            
//                            self.img_add.kf_setImageWithURL(url!, placeholderImage: UIImage(named: ""), optionsInfo: nil , progressBlock: nil , completionHandler: { (image, error, cacheType, imageURL) in
//                                print(image?.description)
//                            } )
                        }
                    }
                }
            }
        }
    }
    //Check url nil or not
    func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if urlString == "" || urlString.isEmpty{
                return false
            }else{
                if let url = URL(string: urlString) {
                    // check if your application can open the NSURL instance
                    return UIApplication.shared.canOpenURL(url)
                }
            }
        }
        return false
    }
}
