//
//  GuideGuidedViewController.swift
//  Gupa Network
//
//  Created by MWM23 on 9/13/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//
//priyanka laptop

import UIKit
import JGProgressHUD
import Kingfisher

class GuideGuidedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tblGuidGuided: UITableView!
    @IBOutlet weak var segmentedControl: DGSegmentedControl!
    @IBOutlet weak var constraintSegmentHeigth : NSLayoutConstraint!
    
    var titleString : String!
    var strUserId : String!
    
    var isFromGuidedView = false
    var isFromGuidesView = false

    var isSentRequest = false
    var isPendingRequest = false
    var isGuide = false
    var isAcceptRequest = false
    var isFromOthersProfile = false

    var loading : JGProgressHUD! = JGProgressHUD()
    var dicData : NSDictionary!
    var respDict : NSDictionary = NSDictionary()
    var arrOfGuide :  NSMutableArray = NSMutableArray()
    var arrSentRequest :  NSMutableArray = NSMutableArray()
    var arrOfPending :  NSMutableArray = NSMutableArray()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()

    //-===============================
    //MARK: Life cycle methods
    //-===============================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        appDelegate.centerNav = self.navigationController
        
        self.title = titleString
        self.startProgressBar()
        
        if strUserId == AppTheme.getLoginDetails().object(forKey: "id")! as Any as! String {
            
            if isFromGuidedView == true {
                
                constraintSegmentHeigth.constant = 0
                segmentedControl.isHidden = true
                getGuidedListOfLoginUser_ApiCall()
            } else {
                decorateSegmentedControl()
            }
            
        } else {
            
            if isFromGuidedView == true {
                
                constraintSegmentHeigth.constant = 0
                segmentedControl.isHidden = true
                getGuidedListOfLoginUser_ApiCall()
                
            }  else if isFromGuidesView == true {
                
                self.isGuide = true
                self.isFromOthersProfile = true
                segmentedControl.isHidden = true
                constraintSegmentHeigth.constant = 0

                getGuideList_ApiCall()
            }
            segmentedControl.isHidden = true
            constraintSegmentHeigth.constant = 0
            getGuideListFromOtherUser_ApiCall()
        }

        SetNavigationBar()
    }
 
    
    func decorateSegmentedControl(){
        
        self.segmentedControl.items = ["Guides","Pending", "Sent"]
        segmentedControl.selectedIndex = 0
        segmentedControl.borderSize = 1
        segmentedControl.thumbColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        segmentedControl.selectedLabelColor = UIColor.darkGray
        segmentedControl.thumUnderLineSize = 4
        segmentedControl.font = UIFont.systemFont(ofSize: 18)
        self.segmentValueChanged(self.segmentedControl)
    }
    
    //MARK:- Segment control
    @IBAction func segmentValueChanged(_ sender: AnyObject) {
        
        if segmentedControl.selectedIndex == 0 {
            
            self.title = "Guides"
            isGuide = true
            isPendingRequest = false
            isSentRequest = false
            getGuideList_ApiCall()
            
        } else if segmentedControl.selectedIndex == 1 {
            
            self.title = "Pending"
            isGuide = false
            isPendingRequest = true
            isSentRequest = false
            getPendingList_ApiCall()
            
        } else  {
            
            self.title = "Sent"
            isGuide = false
            isPendingRequest = false
            isSentRequest = true
            getSentList_ApiCall()
        }
    }
    
    func SetNavigationBar() {
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()   // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(GuideGuidedViewController.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        UserDefaults.standard.set(0, forKey: "isComment")
        
        self.tblGuidGuided.dataSource = self
        self.tblGuidGuided.delegate = self
        //        self.tblGuidGuided.estimatedRowHeight = 60
        //        self.tblGuidGuided.rowHeight = UITableViewAutomaticDimension
    }
    
    @objc func leftBarBackButton(){
        //Helper method for right navigationbar button
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func generateImage(for view: UIView) -> UIImage? {
        defer {
            UIGraphicsEndImageContext()
        }
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            return UIGraphicsGetImageFromCurrentImageContext()
        }
        return nil
    }
    
    var iconWithTextImage: UIImage {
        
        let button = UIButton()
        let icon = UIImage(named: "home")
        button.setImage(icon, for: .normal)
        button.setTitle("Home", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 10)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 10)
        button.sizeToFit()
        return generateImage(for: button) ?? UIImage()
    }
    
    ///========================================================
    //  MARK:- IBAction Method
    // ========================================================
    
    @objc func cancelRequestAction(_ btnBlock: UIButton) {
        
        if isPendingRequest {
            
            var dic: NSDictionary!
            
            dic   = self.arrOfPending.object(at: btnBlock.tag) as! NSDictionary
            
            var str_OtherUserId = ""
            
            if let otherUserId =  dic["sender_id"] as? String {
                str_OtherUserId = otherUserId
                print(otherUserId)
            }
           
            let invitationAlert = UIAlertController(title: "Are you sure you want to decline request?", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            invitationAlert.addAction(UIAlertAction(title: "NO", style: .default, handler: { (action: UIAlertAction!) in
            }))
            
            invitationAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action: UIAlertAction!) in
                
                self.isAcceptRequest = false
                self.sendReuestToOtherUser_ApiCall(str_other_id: str_OtherUserId)
            }))
            self.present(invitationAlert, animated: true, completion: nil)
            
        } else {
            
            var dic: NSDictionary!
            var strTitle : String!

            if isSentRequest  == true {
                strTitle   = "Are you sure you want to cancel request?"
                dic       = self.arrSentRequest.object(at: btnBlock.tag) as! NSDictionary
            } else if isGuide {
                strTitle = "Are you sure you want to Unguide?"
                dic  = self.arrOfGuide.object(at: btnBlock.tag) as! NSDictionary
            }
            
            var str_OtherUserId = ""
            var str_followerId = ""
            
            if let otherUserId =  dic["reciver_id"] as? String {
                str_OtherUserId = otherUserId
                print(otherUserId)
            }
            
            if let followerId =  dic["followers_permission_id"] as? String {
                str_followerId = followerId
                print(str_followerId)
            }
            
            //*> create the alert
            let alert = UIAlertController(title: strTitle, message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            //*> add the actions (buttons)
            alert.addAction(UIAlertAction(title: "NO", style: .default, handler: { action in
            }))
            
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
                
                self.cancelSentRequest_ApiCall(strOtherUserId: str_OtherUserId , strFollwerId: str_followerId , selectedIndex: btnBlock.tag)
            }))
            
            present(alert, animated: true, completion: nil)
        }
       
    }

 
    @objc func acceptAction(_ btnBlock: UIButton) {
        
        var dic: NSDictionary!
        
        dic   = self.arrOfPending.object(at: btnBlock.tag) as! NSDictionary
        
        var str_OtherUserId = ""
        
        if let otherUserId =  dic["sender_id"] as? String {
            str_OtherUserId = otherUserId
            print(otherUserId)
        }
        
        let invitationAlert = UIAlertController(title: "Are you sure you want to accept request?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        invitationAlert.addAction(UIAlertAction(title: "NO", style: .default, handler: { (action: UIAlertAction!) in
        }))
        
        invitationAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action: UIAlertAction!) in
            self.isAcceptRequest = true
            self.sendReuestToOtherUser_ApiCall(str_other_id: str_OtherUserId)
        }))
        
        self.present(invitationAlert, animated: true, completion: nil)
    }
    
    
    func sendReuestToOtherUser_ApiCall(str_other_id : String) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"user_id")
        paraDict.setValue(str_other_id, forKey:"other_user_id")
        
        if isAcceptRequest == true
        {
            paraDict.setValue("1", forKey:"status")
        } else  {
            paraDict.setValue("2", forKey:"status")
        }
        
        AppTheme().callPostService(String(format: "%@guide_friends", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                self.getPendingList_ApiCall()
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error! Please try again later", showInView: self.view)
            }
        }
    }
   
    
    //Mark :- APi Call for |Guide List| When comes from other user Profile
    
    func getGuideListFromOtherUser_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        let paraDict = NSMutableDictionary()
        paraDict.setValue(strUserId, forKey:"user_id")
        
        AppTheme().callPostService(String(format: "%@get_guide_reciver", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                self.respDict = data as! NSDictionary
                
                if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray{
                    self.arrOfResponseKey = array_Response_mutable
                }
                self.tblGuidGuided.reloadData()
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error! Please try again later", showInView: self.view)
            }
        }
    }
    
    //Mark :- APi Call for |Guide List| When comes from Logged in Profile
    
    func getGuideList_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        let paraDict = NSMutableDictionary()
        paraDict.setValue(strUserId, forKey:"user_id")
        
        AppTheme().callPostService(String(format: "%@get_guide", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                self.respDict = data as! NSDictionary
                
                if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray{
                    self.arrOfGuide = array_Response_mutable
                    print("11 iteam count== \(self.arrOfGuide)")
                }
                if self.arrOfGuide.count >  0 {
                    self.segmentedControl.items = ["Guides \(self.arrOfGuide.count)","Pending", "Sent"]
                } else {
                    self.segmentedControl.items = ["Guides","Pending", "Sent"]
                }
                self.tblGuidGuided.reloadData()
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error! Please try again later", showInView: self.view)
            }
        }
    }
    
    func getPendingList_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"user_id")
        paraDict.setValue("pending" , forKey:"status")
    
        AppTheme().callPostService(String(format: "%@get_guide_list_all", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                self.respDict = data as! NSDictionary
                
                self.arrOfPending.removeAllObjects()
                
                if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray{
                    self.arrOfPending = array_Response_mutable
                    print("22 iteam count== \(self.arrOfPending)")
                }
                
                if self.arrOfPending.count >  0 {
                    self.segmentedControl.items = ["Guides","Pending \(self.arrOfPending.count)", "Sent"]
                } else {
                    self.segmentedControl.items = ["Guides","Pending", "Sent"]
                }
                self.tblGuidGuided.reloadData()
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error! Please try again later", showInView: self.view)
            }
        }
    }
    
    func getSentList_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"user_id")
        
        AppTheme().callPostService(String(format: "%@get_guide_pending_sending", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                self.respDict = data as! NSDictionary
                
                if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray {
                    self.arrSentRequest = array_Response_mutable
                    print("sent iteam count== \(self.arrSentRequest.count)")
                }
                
                if self.arrSentRequest.count >  0 {
                    self.segmentedControl.items = ["Guides","Pending", "Sent \(self.arrSentRequest.count)"]
                } else {
                    self.segmentedControl.items = ["Guides","Pending", "Sent"]
                }
                self.tblGuidGuided.reloadData()
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error! Please try again later", showInView: self.view)
            }
        }
    }
    
    func cancelSentRequest_ApiCall(strOtherUserId :String , strFollwerId : String , selectedIndex: Int) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(strOtherUserId, forKey:"user_id")
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"other_user_id")
        paraDict.setValue("2", forKey:"status")
        paraDict.setValue(strFollwerId, forKey:"delete")

        AppTheme().callPostService(String(format: "%@guide_friends_delete", AppUrlNew), param: paraDict) {(result, data) -> Void in

            if result == "success"
            {
                self.loading.dismiss()
                
                if self.isSentRequest == true {
                    self.arrSentRequest.removeObject(at: selectedIndex)
                    if self.arrSentRequest.count >  0 {
                        self.segmentedControl.items = ["Guides","Pending", "Sent \(self.arrSentRequest.count)"]
                    } else {
                        self.segmentedControl.items = ["Guides","Pending", "Sent"]
                    }
                } else {
                    self.arrOfGuide.removeObject(at: selectedIndex)
                    if self.arrOfGuide.count >  0 {
                        self.segmentedControl.items = ["Guides","Pending", "Sent \(self.arrOfGuide.count)"]
                    } else {
                        self.segmentedControl.items = ["Guides","Pending", "Sent"]
                    }
                }
                self.tblGuidGuided.reloadData()
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error! Please try again later", showInView: self.view)
            }
        }
    }
    
    func getGuidedListOfLoginUser_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        let paraDict = NSMutableDictionary()
        paraDict.setValue(strUserId, forKey:"user_id")
        
        AppTheme().callPostService(String(format: "%@get_guide_reciver", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                self.respDict = data as! NSDictionary
                
                if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray{
                    self.arrOfResponseKey = array_Response_mutable
                    print(self.arrOfResponseKey)
                }
                self.tblGuidGuided.reloadData()
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error! Please try again later", showInView: self.view)
            }
        }
    }
    
    ///========================================================
    // MARK:- UITableview Datasource and delegate methods
    // ========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.isSentRequest {
            return self.arrSentRequest.count
        } else if isPendingRequest {
            return self.arrOfPending.count
        } else if isGuide {
            return self.arrOfGuide.count
        } else {
            return self.arrOfResponseKey.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : GuidePendingListCell = tableView.dequeueReusableCell(withIdentifier: "GuidePendingListCell") as! GuidePendingListCell
        
        let dic: NSDictionary!
        
        if self.isSentRequest {
            dic = self.arrSentRequest.object(at: indexPath.row) as! NSDictionary
        } else if isPendingRequest {
            dic = self.arrOfPending.object(at: indexPath.row) as! NSDictionary
        } else if isGuide {
            dic = self.arrOfGuide.object(at: indexPath.row) as! NSDictionary
        } else {
            dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        }
        
        var strFirstName = ""
        var strLastName = ""
        
        if let str_user_full_name : String = dic["first_name"] as? String {
            strFirstName = str_user_full_name
        }
        
        if let str_user_name : String = dic["last_name"] as? String{
            strLastName = str_user_name
        }
        
        cell.lblName.text   = "\(strFirstName) \(strLastName)"
        cell.btnCancel.tag  = indexPath.row

        if isSentRequest == true {
            
            cell.constAcceptWidth.constant = 0
            cell.constDeclineWidth.constant = 80

            cell.btnAccept.isHidden = true
            cell.btnCancel.isHidden = false
            cell.btnAccept.isUserInteractionEnabled  = false
            cell.btnCancel.isUserInteractionEnabled  = true

            cell.btnCancel.setTitle("Cancel", for: .normal)
            cell.btnCancel.addTarget(self, action: #selector(GuideGuidedViewController.cancelRequestAction(_:)), for: UIControlEvents.touchUpInside)
            
        } else if isPendingRequest == true {
            
            cell.btnAccept.isHidden = false
            cell.btnCancel.isHidden = false
            
            cell.btnAccept.isUserInteractionEnabled  = true
            cell.btnCancel.isUserInteractionEnabled  = true

            cell.constAcceptWidth.constant = 80
            cell.constDeclineWidth.constant = 80

            cell.btnAccept.tag  = indexPath.row

            cell.btnCancel.setTitle("Decline", for: .normal)
            cell.btnAccept.setTitle("Accept", for: .normal)
            
            cell.btnAccept.layer.borderWidth = 1
            cell.btnAccept.layer.borderColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0).cgColor
            
            cell.btnAccept.addTarget(self, action: #selector(GuideGuidedViewController.acceptAction(_:)), for: UIControlEvents.touchUpInside)
            cell.btnCancel.addTarget(self, action: #selector(GuideGuidedViewController.cancelRequestAction(_:)), for: UIControlEvents.touchUpInside)

        } else if isGuide == true {
            
            if self.isFromOthersProfile == true {
                
                cell.constAcceptWidth.constant = 0
                cell.constDeclineWidth.constant = 80

                cell.btnAccept.isHidden = true
                cell.btnCancel.isHidden = true
                cell.btnAccept.isUserInteractionEnabled  = false
                cell.btnCancel.isUserInteractionEnabled  = false

            } else {
                
                cell.btnAccept.isHidden = true
                cell.btnCancel.isHidden = false
                cell.btnAccept.isUserInteractionEnabled  = false
                cell.btnCancel.isUserInteractionEnabled  = true

                cell.constAcceptWidth.constant = 0
                cell.constDeclineWidth.constant = 80

                cell.btnCancel.setTitle("Unguide", for: .normal)
                cell.btnCancel.addTarget(self, action: #selector(GuideGuidedViewController.cancelRequestAction(_:)), for: UIControlEvents.touchUpInside)
            }
           
        } else  {
            cell.constDeclineWidth.constant = 0
            cell.constDeclineWidth.constant = 0

            cell.btnAccept.isHidden = true
            cell.btnCancel.isHidden = true
            cell.btnAccept.isUserInteractionEnabled  = false
            cell.btnCancel.isUserInteractionEnabled  = false
        }
        
        if let str_profile_pic : String = dic["profile_pic"] as? String {
            
            let url = URL(string:str_profile_pic)
            if url != nil {
                cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "ic_user_off"), options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, imageURL) in
                    print(image?.description ?? "")
                })
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        var destVC = UserChatOnCommentController()
//        destVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
//        //destVC.dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
//        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
   
    //==============================
    //MARK:- Loader method
    //==============================
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
}



