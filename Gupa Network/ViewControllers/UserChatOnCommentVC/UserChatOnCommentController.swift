//
//  UserChatOnCommentController.swift
//  Gupa Network
//
//  Created by MWM23 on 11/19/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher
import QuartzCore
import AVKit
import AVFoundation
import MediaPlayer

class UserChatOnCommentController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var loading : JGProgressHUD = JGProgressHUD(style: JGProgressHUDStyle.dark)
    
    @IBOutlet var followUnfollowButtonOnNavigationOnUserDetail: UIButton!
    @IBOutlet weak var tblUserDetailFollowUnfollow: UITableView!
    @IBOutlet var chatButtonOnNavigationUserDetail: UIButton!
  //  @IBOutlet var followUnfollowButtonOnNavigationOnUserDetail: UIButton!
    var appDelegate: AppDelegate = AppDelegate()

     var userDetailArray: NSArray? = NSArray()
     var userPostArray: NSMutableArray! = NSMutableArray()
     var userPostDict: NSDictionary!
     var userDetailDict: NSDictionary!
    
    @IBOutlet weak var viewForUserFollowUnfollow: UIView!
    @IBOutlet weak var viewFollowersFollowingPost : UIView!
    @IBOutlet weak var viewUserPostTbl : UIView!
    @IBOutlet weak var btnCancelOnFollowUnfollow: UIButton!
    @IBOutlet weak var userNameTitleBar: UILabel!
    
    @IBOutlet weak var userBgImage: UIImageView!
    @IBOutlet weak var userProfileImageOnUserDetail: UIImageView!
    @IBOutlet weak var userFirstLastNameUserDetail: UILabel!
    @IBOutlet weak var userAddressUserDetail: UILabel!
  
    @IBOutlet weak var btnGuide: UIButton!
    @IBOutlet weak var btnGuides: UIButton!
    @IBOutlet weak var btnGuided: UIButton!
    @IBOutlet weak var btmBlockUser: UIButton!
    @IBOutlet weak var lblGuidePending: UILabel!

    @IBOutlet weak var viewGuide: UIView!
    @IBOutlet weak var viewGuides: UIView!
    @IBOutlet weak var viewGuided: UIView!
    @IBOutlet weak var viewBlockUser: UIView!
    @IBOutlet weak var constraint_widthSegment : NSLayoutConstraint!

    var str_total_followers : String!
    var str_total_following : String!
    var str_new_followers : String!
    var str_new_following : String!
    var otherUserMongo_obj : String!

    var isFirstTime : Bool = false
    var isFirstTime_following : Bool = false
    var isAcceptButton : Bool = false
    var isDecline : Bool = false
    var isUnguide : Bool = false

    var str_search_word : String!
    
    var dic : NSDictionary!
    var str_other_id : String! // = <#value#>
    var isFromTrip : String!  = "false"
    var isFromUserComment : String!  = "false"
    var strUserId : String!
    
    var str_User_title : String!
    var btnPlayPause: UIButton!
    var likeByMe : NSInteger!
    var flight_Id : NSInteger!
    var post_Id : NSInteger!
    var isFollow : NSInteger!
    
    var respDict : NSDictionary = NSDictionary()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var btnCheck: UIButton = UIButton()
    var btnChat:UIButton = UIButton()
    var btnFollow:UIButton = UIButton()
    
    var userIdForSingleUserDetail : String = String()
    
    
    // Socket Chat
    fileprivate var chatViewController : ChatViewController?

    //--------------------------------------
    //MARK:- Controller life cycle methods
    //--------------------------------------

      override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerNav = self.navigationController
        

        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 93.0/255.0, green: 191.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_bg.png"), for: . default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let btnLeftBar: UIButton = UIButton ()                                        // Create bar button here
        btnLeftBar.setImage(UIImage(named: "back_white.png"), for: UIControlState())
        btnLeftBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeftBar.addTarget(self, action: #selector(UserChatOnCommentController.leftBarBackButton), for: .touchUpInside)
        
        let leftBarButton:UIBarButtonItem = UIBarButtonItem()                          //.....Set left button
        leftBarButton.customView = btnLeftBar
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        self.userProfileImageOnUserDetail.layer.cornerRadius = 25 //self.imgUser.frame.height/2
        self.userProfileImageOnUserDetail.clipsToBounds = true
        self.viewFollowersFollowingPost.layer.cornerRadius = 5
        self.viewFollowersFollowingPost.clipsToBounds = true
        self.viewUserPostTbl.layer.cornerRadius = 5
        self.viewUserPostTbl.clipsToBounds = true
        
        self.tblUserDetailFollowUnfollow.estimatedRowHeight = 100
        self.tblUserDetailFollowUnfollow.rowHeight = UITableViewAutomaticDimension
        self.tblUserDetailFollowUnfollow.setNeedsLayout()
        self.tblUserDetailFollowUnfollow.layoutIfNeeded()
        self.tblUserDetailFollowUnfollow.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)

        
//        if appDelegate.isFromLeftMenu == true {
//            userIdForSingleUserDetail = appDelegate.strUserID
//            getSingleUserInfo(userIdForSingleUserDetail)
//        } else {
        
          if isFromTrip == "true" {
             if let str_userId = AppTheme.getLoginDetails().value(forKey: "id") as? String{
                userIdForSingleUserDetail = str_userId
            }
             getSingleUserInfo(str_other_id)
            
        } else if isFromTrip == "Notification" {
            if let str_userId = dic .value(forKey: "sender") as? String{
               userIdForSingleUserDetail = str_userId
                getSingleUserInfo(str_userId)
            }
        } else if isFromTrip == "SearchUser"{
            if let str_userId = dic .value(forKey: "id") as? String{
                userIdForSingleUserDetail = str_userId
                getSingleUserInfo(str_userId)
            }
        } else {
            if let str_userId = dic.value(forKey: "user_id") as? String{
            userIdForSingleUserDetail = str_userId
            getSingleUserInfo(str_userId)
            }
        }
       // }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        userOnChat()
        
//        if appDelegate.isFromLeftMenu == true {
//            userIdForSingleUserDetail = appDelegate.strUserID
//            getSingleUserInfo(userIdForSingleUserDetail)
//        } else {
        
        if isFromTrip == "true" {
            if let str : String =  AppTheme.getLoginDetails().value(forKey: "id") as? String{
                strUserId = str
            }
            
            let str_logged_user_id : String = (AppTheme.getLoginDetails().value(forKey: "id") as? String)!
            
            if str_other_id == str_logged_user_id {
                viewGuide.isHidden     = true
                viewBlockUser.isHidden = true
                constraint_widthSegment.constant = 176
            } else {
                viewGuide.isHidden = false
                viewBlockUser.isHidden = false
                constraint_widthSegment.constant = 292
                getSendingPendingBoth_ApiCall()
            }
            
        } else if isFromTrip == "Notification" {
            if let str_userId = dic .value(forKey: "sender") as? String{
                strUserId = str_userId
            }
        } else if isFromTrip == "SearchUser"{
            if let str_userId = dic .value(forKey: "id") as? String{
                strUserId = str_userId
            }
                let str_logged_user_id : String = (AppTheme.getLoginDetails().value(forKey: "id") as? String)!
                
                if strUserId == str_logged_user_id {
                    viewGuide.isHidden     = true
                    viewBlockUser.isHidden = true
                    constraint_widthSegment.constant = 176
                } else {
                    viewGuide.isHidden = false
                    viewBlockUser.isHidden = false
                    constraint_widthSegment.constant = 292
                    getSendingPendingBoth_ApiCall()
                }
        } else {
            if let str_userId = dic .value(forKey: "user_id") as? String{
                strUserId = str_userId
                
                if let str : String =  AppTheme.getLoginDetails().value(forKey: "id") as? String {
                    strUserId = str
                    
                    let str_logged_user_id : String = (AppTheme.getLoginDetails().value(forKey: "id") as? String)!
                    
                    if strUserId == str_logged_user_id {
                        viewGuide.isHidden     = true
                        viewBlockUser.isHidden = true
                        constraint_widthSegment.constant = 176
                    } else {
                        viewGuide.isHidden = false
                        viewBlockUser.isHidden = false
                        constraint_widthSegment.constant = 292
                        getSendingPendingBoth_ApiCall()
                    }
                }
            }
          }
       // }
    }
//    func showNavigationBarWithOneButton(_ img_string : String) {
//
//        let imgWriteReview:UIImage = UIImage(named: img_string)!
//        // Create the second button
//        self.btnFollow = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
//        btnFollow.setImage(imgWriteReview, for: UIControlState())
//        btnFollow.addTarget(self, action: #selector(UserChatOnCommentController.followUnfollowAction), for: UIControlEvents.touchUpInside)
//        let btnForFollow:UIBarButtonItem = UIBarButtonItem(customView: btnFollow)
//        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
//        fixedSpace.width = 8.0
//        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
//        negativeSpace.width = -7.0
//        navigationItem.rightBarButtonItems = [ negativeSpace, btnForFollow]
//    }
    
    func showRightNavigationBarButtonHide(isHide : Bool)  //RightNavigationBarButton hide/unhide
    {
        
        let btnRigthBar: UIButton = UIButton()
        btnRigthBar.setImage(UIImage(named: "img_chatWhite.png"), for: UIControlState())
        btnRigthBar.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        let rigthBarButton:UIBarButtonItem = UIBarButtonItem()
        rigthBarButton.customView = btnRigthBar
        
        if isHide == true {
            self.navigationItem.rightBarButtonItem = nil
        } else {
            self.navigationItem.rightBarButtonItem = rigthBarButton
            btnRigthBar.addTarget(self, action: #selector(UserChatOnCommentController.chatAction), for: .touchUpInside)
        }
    }
    
    @objc func chatAction(){
        
        var userlist:[APIRequestParam.GroupUserData] = []
        var userData:APIRequestParam.GroupUserData?
        
        userData = APIRequestParam.GroupUserData(_id:  otherUserMongo_obj ?? "")
        userlist.append(userData!)
        
        self.serApiForCreateSingleChatGroup(userList: userlist)
        
//        chatViewController = BaseApp.sharedInstance.getViewController(storyboardName: AppConstant.chatVCStoryboard, viewControllerName: ChatViewController.nameOfClass) as ChatViewController
//        chatViewController?.userName = self.str_User_title
//        //chatViewController?.blockId = userData._id!
//        chatViewController?.message_type = "text"
//        chatViewController?.message_by = ApplicationPreference.getUserId()
//        chatViewController?.sender_name = ApplicationPreference.getUserFullName()
//        chatViewController?.is_group = false
//        chatViewController?.strViewComeFrom = "SINGLECHAT"
//      //  chatViewController?.strTitle = userData.group_name!.trim()
//        self.navigationController?.pushViewController(chatViewController!, animated: true)
////
//        created_by:59e5d98c734d1d62dcbfc6c7
//        friend_name:admin test surname
//        group_name:Pramod Jain
//        is_group:false
//        status:1
//        userLists:[{_id: "5a0e9c04997c7421abdc4292"}]
        
        
        
//        if userData.is_group == false {
//
//            if (ApplicationPreference.getUserId()?.caseInsensitiveCompare(userData.createdby!) == ComparisonResult.orderedSame) {
//                let arrForUser = userData.userLists![0]
//                chatViewController?.userID = arrForUser._id!
//            } else {
//                chatViewController?.userID = userData.createdby!//ApplicationPreference.getUserId()
//            }
//        } else {
//            chatViewController?.userID = ""
//        }
        
//        if let str : String =  AppTheme.getLoginDetails().value(forKey: "id") as? String{
//            let destVC : ChatViewController!
//            destVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//            destVC.str_my_id = str
//
//            if isFromTrip == "true" {
//
//                destVC.str_others_id = str_other_id
//                destVC.str_title = str_User_title
//
//            } else if isFromTrip == "Notification" {
//
//                if let str_userId = dic .value(forKey: "sender") as? String{
//                     destVC.str_others_id =  str_userId
//                }
//                if let str_user_title = dic.value(forKey: "username") as? String{
//                    destVC.str_title = str_user_title
//                }
//            } else if isFromTrip == "SearchUser"  {
//
//                if let str_userId = dic .value(forKey: "id") as? String{
//                   destVC.str_others_id =  str_userId
//                }
//                if let str_user_title = dic.value(forKey: "username") as? String{
//                    destVC.str_title = str_user_title
//                }
//            } else {
//                if let str_userId = dic .value(forKey: "user_id") as? String{
//                   destVC.str_others_id =  str_userId
//                }
//                if let str_user_title = dic.value(forKey: "username") as? String{
//                    destVC.str_title = str_user_title
//                }
//            }
         //  self.navigationController?.pushViewController(destVC, animated: true)
        //}
    }
    
    func serApiForCreateSingleChatGroup(userList: [APIRequestParam.GroupUserData]) {
        
        if(BaseApp.sharedInstance.isNetworkConnected){
            BaseApp.sharedInstance.showProgressHudViewWithTitle(title: "")
        
            let createSingleAndGroupChatRequestParam = APIRequestParam.CreateSingleAndGroupChatInfo(group_icon: "", group_name: str_User_title, is_group: false, addedasfreind: "", lastmsgsentby: "", last_msg: "", friend_name: AppTheme.getUserFullName(), created_by: ApplicationPreference.getMongoUserId(), timestamp: "\(BaseApp.sharedInstance.getCurrentTimeMilliSeconds())", status: "1", created_id : AppTheme.getLoginDetails().object(forKey: "id")! as? String, friendId : str_other_id,userLists: userList)
            
            let createSingleAndGroupChat = CreateSingleAndGroupChatRequest(token: ApplicationPreference.getAppToken(), userId: (AppTheme.getLoginDetails().value(forKey: "id") as? String)! , createSingleAndGroupChatData: createSingleAndGroupChatRequestParam, onSuccess: {
                response in
                
                print("33333333===== \(createSingleAndGroupChatRequestParam)")

                
                print(response.toJSON())
                
                OperationQueue.main.addOperation() {
                    BaseApp.sharedInstance.hideProgressHudView()
                    
                       let dict = response.toJSON() as NSDictionary
                       self.chatViewController = BaseApp.sharedInstance.getViewController(storyboardName: AppConstant.chatVCStoryboard, viewControllerName: ChatViewController.nameOfClass) as ChatViewController
                        self.chatViewController?.blockId = "\(dict.value(forKey: "data")!)"
                        self.chatViewController?.message_by = ApplicationPreference.getMongoUserId()
                        self.chatViewController?.userID = self.otherUserMongo_obj
                        self.chatViewController?.userName = self.str_User_title
                        self.chatViewController?.sender_name = AppTheme.getUserFullName()
                        self.chatViewController?.otherUserId = self.str_other_id!
                        self.chatViewController?.createdID   = AppTheme.getLoginDetails().object(forKey: "id")! as? String
                        self.chatViewController?.is_group = false
                        self.chatViewController?.strViewComeFrom = "SINGLECHAT"
                        self.navigationController?.pushViewController(self.chatViewController!, animated: true)
                }
            }, onError: {
                error in
                print(error.toString())
                
                OperationQueue.main.addOperation() {
                    // when done, update your UI and/or model on the main queue
                    BaseApp.sharedInstance.hideProgressHudView()
                    BaseApp.sharedInstance.showAlertViewControllerWith(title: "Error", message: error.errorMsg!, buttonTitle: "OK", controller: self)
                }
            })
            
            //
            BaseApp.sharedInstance.jobManager?.addOperation(createSingleAndGroupChat)
            
        } else {
            BaseApp.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }
    
    @objc func leftBarBackButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //========================================================
    //MARK:- UITableview Datasource and delegate methods
    //========================================================
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var row : Int = Int()
        if  self.userPostArray.count != 0{
            row = self.userPostArray.count
        } else {
            row = 0
        }
        return row
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : WriteStatusCell = tableView.dequeueReusableCell(withIdentifier: "WriteStatusCell", for: indexPath) as! WriteStatusCell
        
        
        let temp_dict : NSDictionary = self.userPostArray?.object(at: indexPath.row) as! NSDictionary
        
        if let str_user_name : String = userDetailDict.value(forKey: "username") as? String{
            cell.lblUserName.text = str_user_name
        }
        cell.btn_image_post.isHidden = true
        cell.btn_video_post.isHidden = true
        
        if let str_posts : String = temp_dict .value(forKey: "posts") as? String {
            
            cell.lblStatus.text = str_posts
            cell.lblStatus.hashtagLinkTapHandler = {(label:KILabel,string:String?,range: NSRange?) -> ()
                in
                let objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "HashTagController") as! HashTagController
                print(indexPath.row)
                objFlightDetailVC.sr_search_word = string
                self.navigationController?.pushViewController(objFlightDetailVC, animated: true)
            }
            cell.lblStatus.userHandleLinkTapHandler = {(label:KILabel,string:String?,range: NSRange?) -> ()
                in
                if ((string?.contains("@")) != nil) {
                    var str : String! = String()
                    str = string!.replacingOccurrences(of: "@", with: "", options: NSString.CompareOptions.literal, range: nil)
                    self.getUserInfoOnTag("", str_user_name: str)
                }
            }
        }
        if let str_total_like : String = temp_dict .value(forKey: "total_like") as? String{
            cell.lblTotalLike.text = str_total_like
        }
        if let str_total_comment : String = temp_dict .value(forKey: "total_comment") as? String {
            cell.lblTotalComments.text = str_total_comment
        }
        HomeVC.isUserDetailLike = true
        cell.btnLike.tag = indexPath.row
        cell.btnLike.viewWithTag(5)
        
        cell.layer.cornerRadius = 10 //set corner radius here
        cell.layer.borderColor  = UIColor.lightGray.cgColor  // set cell border color here
        cell.layer.borderWidth  = 2
        
        if let strRating: String = temp_dict["like_by_me"] as? String {
            likeByMe = NSInteger(strRating)!
        }else if let strRating: NSInteger = temp_dict["like_by_me"] as? NSInteger{
            likeByMe = strRating
        }
        if likeByMe == 1 {
            cell.btnLike.setImage(UIImage(named: "img_like_on.png"), for: UIControlState())
        } else{
            cell.btnLike.setImage(UIImage(named: "img_dislike.png"), for: UIControlState())
        }
        if let str_flight_id : String = temp_dict .value(forKey: "flight_id") as? String{
            flight_Id = NSInteger(str_flight_id)
        }
        if let str_post_Id : String = temp_dict .value(forKey: "post_id") as? String{
            post_Id = NSInteger(str_post_Id)
        }
        
        cell.btnShare.addTarget(self, action: #selector(UserChatOnCommentController.shareAction(_:)), for: UIControlEvents.touchUpInside)
        cell.btnShare.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(UserChatOnCommentController.likeDislikeAction(_:)), for: UIControlEvents.touchUpInside)
        cell.btnComment.addTarget(self, action: #selector(UserChatOnCommentController.commentAction(_:)), for: UIControlEvents.touchUpInside)
        cell.btnComment.tag = indexPath.row + 1000
        
        if let str_logged_user_id : String = AppTheme.getLoginDetails().value(forKey: "id") as? String {
            
            if isFromTrip == "false" || isFromUserComment == "true" {
                
                if (dic.value(forKey: "user_id") as! String == str_logged_user_id)
                {
                    cell.deleteButton.isHidden = false
                    cell.deleteButton.isUserInteractionEnabled = true
                    cell.deleteButton.addTarget(self, action: #selector(UserChatOnCommentController.deletePost(_:)), for: .touchUpInside)
                    cell.deleteButton.tag = indexPath.row
                } else
                {
                    cell.deleteButton.isHidden = true
                    cell.deleteButton.isUserInteractionEnabled = false
                }
                
            } else if isFromTrip == "true" {
                
                if (self.str_other_id == str_logged_user_id)
                {
                    cell.deleteButton.isHidden = false
                    cell.deleteButton.isUserInteractionEnabled = true
                    cell.deleteButton.addTarget(self, action: #selector(UserChatOnCommentController.deletePost(_:)), for: .touchUpInside)
                    cell.deleteButton.tag = indexPath.row
                 }else {
                    cell.deleteButton.isHidden = true
                    cell.deleteButton.isUserInteractionEnabled = false
                }
            } else if isFromTrip == "Notification"
            {
                if (userIdForSingleUserDetail == str_logged_user_id) {
                    
                    cell.deleteButton.isHidden = false
                    cell.deleteButton.isUserInteractionEnabled = true
                    cell.deleteButton.addTarget(self, action: #selector(UserChatOnCommentController.deletePost(_:)), for: .touchUpInside)
                    cell.deleteButton.tag = indexPath.row
                    
                } else {
                    cell.deleteButton.isHidden = true
                    cell.deleteButton.isUserInteractionEnabled = false
                }
            } else if isFromTrip == "SearchUser" {
                
                if (userIdForSingleUserDetail == str_logged_user_id) {
                    cell.deleteButton.isHidden = false
                    cell.deleteButton.isUserInteractionEnabled = true
                    cell.deleteButton.addTarget(self, action: #selector(UserChatOnCommentController.deletePost(_:)), for: .touchUpInside)
                    cell.deleteButton.tag = indexPath.row
                } else{
                    cell.deleteButton.isHidden = true
                    cell.deleteButton.isUserInteractionEnabled = false
                }
            }
        }
        if let str_f_type =  temp_dict .value(forKey: "file_type") as? String {
            
            if str_f_type == "text" {
                cell.imageHeight.constant = 0
                cell.viewHeightConstraint.constant = 0
                
            }
            else {
                if let str_file_url : String = temp_dict .value(forKey: "file_name") as? String{
                    let url = URL(string:str_file_url)
                    
                    if verifyUrl(str_file_url) == true {
                        
                        if str_f_type == "video"{
                            cell.viewPlayer.isHidden = false
                            cell.imgAdd.isHidden = true
                            cell.viewHeightConstraint.constant = 81
                            cell.btn_image_post.isHidden = true
                            cell.btn_video_post.isHidden = false
                            cell.btn_video_post.addTarget(self, action: #selector(UserChatOnCommentController.videoTapped(_:)), for: .touchUpInside)
                            cell.btn_video_post.tag = indexPath.row
                            
                        } else{
                            cell.viewPlayer.isHidden = true
                            cell.imgAdd.isHidden = false
                            cell.btn_image_post.isHidden = false
                            cell.btn_video_post.isHidden = true
                            cell.imageHeight.constant = 81
                            cell.btn_image_post.addTarget(self, action:  #selector(UserChatOnCommentController.imageTapped(_:)), for: .touchUpInside)
                            cell.btn_image_post.tag = indexPath.row
                            let url = url
                            
                            cell.imgAdd.kf.setImage(with: url!, placeholder: UIImage(named: ""), options: nil, progressBlock: nil, completionHandler: {
                                (image, error, cacheType, imageURL) in
                                print(image?.description ?? "")
                            })
                            
                            
//                            cell.imgAdd.kf_setImageWithURL(url!, placeholderImage: UIImage(named: ""), optionsInfo: nil , progressBlock: nil , completionHandler: { (image, error, cacheType, imageURL) in
//                                print(image?.description)
//                            } )
                        }
                    }
                }else{
                    cell.imageHeight.constant = 0
                    cell.viewHeightConstraint.constant = 0
                }
            }
        }
       /* if let str_file_type : String = temp_dict .valueForKey("file_name") as? String
        {
            let url = NSURL(string:str_file_type)
            if verifyUrl(str_file_type) == true{
                if str_file_type == "video"{
                    cell.viewPlayer.hidden = false
                    cell.imgAdd.hidden = true
                    cell.viewHeightConstraint.constant = 81
                    let videoURL = url
                    cell.viewPlayer.setVideoURL(videoURL!)
                } else{
                    cell.viewPlayer.hidden = true
                    cell.imgAdd.hidden = false
                    cell.imageHeight.constant = 81
                  
                    if let url = NSURL(string: str_file_type) {
                        if let data = NSData(contentsOfURL: url) {
                            let image = UIImage(data: data)
                            cell.imgAdd.image = ResizeImage(image!, targetSize: CGSizeMake(100.0, 81.0))
                        }
                    }
                }
            }else{
                cell.imageHeight.constant = 0
                cell.viewHeightConstraint.constant = 0
            }
        } */
        if let str_profile_pic : String = userDetailDict.value(forKey: "profile_pic") as? String
        {
            let urlNew:String = str_profile_pic.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string:urlNew)
            
            cell.imgUser.kf.setImage(with: url!, placeholder: UIImage(named: "addprofilepic.png"), options: nil, progressBlock: nil, completionHandler: {
                (image, error, cacheType, imageURL) in
                print(image?.description ?? "")
            })
 
        }
        if let str_time : String = temp_dict .value(forKey: "timestamp") as? String{
            cell.lblDateTime.text = str_time
        }
        var rating : NSInteger = 0
        if let strRating: String = temp_dict["rating"] as? String {
            if strRating == ""{
            } else {
                rating = NSInteger(strRating)!
            }
        }else if let strRating: NSInteger = self.userPostDict["rating"] as? NSInteger {
            rating = strRating
        }
        if rating == 0 {
            cell.imgSmileStatus.image = UIImage(named: "")
        }
        if rating == 1 {
            cell.imgSmileStatus.image = UIImage(named: "good_on.png")
        }   else if rating == 2 {
            cell.imgSmileStatus.image = UIImage(named: "bad_on.png")
        }   else if rating == 3{
            cell.imgSmileStatus.image = UIImage(named: "ugly_on.png")
        }
        return cell
    }

    func getSingleUserInfo(_ otherId : String)
    {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        self.startProgressBar()
        var loggined_user_id : String!
        if let str_loggined_user_id : String = AppTheme.getLoginDetails().value(forKey: "id") as? String {
            loggined_user_id = str_loggined_user_id
            
            let paraDict = NSMutableDictionary()
            paraDict.setValue(loggined_user_id, forKey:"otheruser_id")
            paraDict.setValue(otherId, forKey:"user_id")
            
            AppTheme().callPostService(String(format: "%@user_info", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
                
                if result == "success"
                {
                    self.appDelegate.isFromLeftMenu = false

                    self.loading.dismiss()
                    if let arrResponse: NSArray = data as? NSArray {
                        print("arrResponse: @%", arrResponse)
                    }
                    if let arrResponse: NSMutableArray = data as? NSMutableArray{
                        print("arrResponse: @%", arrResponse)
                    } else if let dicRes: NSDictionary = data as? NSDictionary {
                        if let tempDic: NSDictionary = dicRes["response"] as? NSDictionary {
                            self.respDict = tempDic.mutableCopy() as! NSMutableDictionary
                        }else {
                            self.respDict = (dicRes["response"] as? NSMutableDictionary)!
                        }
                        if let int_follow : String = self.respDict .value(forKey: "is_follow") as? String{
                            self.isFollow = NSInteger(int_follow)
                            //Check for follow unfollow
                            if self.isFollow == 0 || self.isFollow == nil {
                                
                                self.followUnfollowButtonOnNavigationOnUserDetail.setImage(UIImage(named: "imgFollow.png"), for: UIControlState())
                                
                                if let str_loggined_user_id : String = AppTheme.getLoginDetails().value(forKey: "id") as? String{
                                    if str_loggined_user_id == otherId
                                    {
                                       self.tblUserDetailFollowUnfollow.isHidden = false
                                } else {
                                    // self.showNavigationBarWithOneButton("imgFollow.png")
                                        self.tblUserDetailFollowUnfollow.isHidden = true
                                    }
                                    
                                }
//                                self.tblUserDetailFollowUnfollow.hidden = true
                            } else{
                                self.followUnfollowButtonOnNavigationOnUserDetail.setImage(UIImage(named: "follow.png"), for: UIControlState())
                                if let str_loggined_user_id : String = AppTheme.getLoginDetails().value(forKey: "id") as? String{
                                    if str_loggined_user_id == otherId {
                                     //   self.showNavigationBarWithOneButton("follow.png")
                                    } else {
                                       // self.showRightNavigationBarButtonHide()
                                    }
                                }
                                self.tblUserDetailFollowUnfollow.isHidden = false
                            }
                        }
//                        if let str_review : String = String(self.respDict .value(forKey: "total_post") as! NSInteger){
//                            self.userTotalPostLabel.text = str_review
//                        }
                        if let str_followers : String = self.respDict .value(forKey: "total_followers") as? String                        {
                            //self.userFollowersLabel.text = str_followers
                            self.str_total_followers = str_followers
                        }
                        if let str_total_follow : String = self.respDict .value(forKey: "total_follow") as? String {
                          //  self.userFollowingLabel.text = str_total_follow
                            self.str_total_following = str_total_follow
                        }
                        if let tempUserDetailArray: NSArray = self.respDict .value(forKey: "users_detail") as? NSArray {
                            self.userDetailArray = tempUserDetailArray.mutableCopy() as! NSMutableArray
                        } else {
                            self.userDetailArray = self.respDict.value(forKey: "users_detail") as? NSMutableArray
                        }
                        for element in self.userDetailArray!
                        {
                            self.userDetailDict  = element as! NSDictionary
                        }
                        
                        if let str_user_name : String = self.userDetailDict .value(forKey: "username") as? String {
                            self.title = str_user_name
                            self.str_User_title = str_user_name
                        }
                        if let str_fullename : String = self.userDetailDict["fullname"] as? String {
                            self.userFirstLastNameUserDetail.text = str_fullename
                        }
                        
                        var str_cityname : String!
                        var str_countaryname : String!
                        
                        if let str_city_name : String = self.userDetailDict .value(forKey: "cityname") as? String {
                            
                            str_cityname = str_city_name
                            if let str_countary_name: String = self.userDetailDict .value(forKey: "countryname") as? String
                            {
                                str_countaryname = str_countary_name
                                let stringAddress : String = "\(str_cityname!)  \(str_countaryname!)"
                                self.userAddressUserDetail.text = stringAddress
                            }
                        }
                        
                        if let str_profile_pic : String = (self.userDetailDict.value(forKey: "profile_pic") as? String)!.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!{
                            
                            let url = URL(string:str_profile_pic)
                            
                            self.userBgImage.kf.setImage(with: url!, placeholder: UIImage(named: "ic_hotel-1.png"), options: nil, progressBlock: nil, completionHandler: {
                                (image, error, cacheType, imageURL) in
                                print(image?.description ?? "")
                            })
 
                             self.userProfileImageOnUserDetail.kf.setImage(with: url!, placeholder: UIImage(named: "ic_hotel-1.png"), options: nil, progressBlock: nil, completionHandler: {
                                (image, error, cacheType, imageURL) in
                                print(image?.description ?? "")
                            })
                        }
                    }
                    
                    
                    if let str_magoObj : String = self.userDetailDict["mongo_obj"] as? String {
                        self.otherUserMongo_obj = str_magoObj
                    }
                    if let tempUserPostArray: NSArray = self.respDict .value(forKey: "users_posts") as? NSArray {
                        self.userPostArray = tempUserPostArray.mutableCopy() as! NSMutableArray
                    } else {
                        self.userPostArray = (self.respDict .value(forKey: "users_posts") as? NSMutableArray)!
                    }
                    
                    for element in self.userPostArray
                    {
                        self.userPostDict = element as! NSDictionary
                        self.tblUserDetailFollowUnfollow.isHidden = false
                    }
                    
                    self.tblUserDetailFollowUnfollow.reloadData()
                    
                } else {
                    self.loading.dismiss()
                    //_ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:data as! String, showInView: self.view)
                }
            }
        }
        
    }
    
    @IBAction func btnBlockUser_Action(_ sender: AnyObject) {
        
        //*> create the alert
        let alert = UIAlertController(title:"Are you sure you want to block user?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        present(alert, animated: true, completion: nil)
        
        //*> add the actions (buttons)
        alert.addAction(UIAlertAction(title: "NO", style: .default, handler: { action in
        }))
        
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            
            self.blockUser_ApiCall()
        }))
        
    }
    // Mark :- New API Implmentation
    
    func blockUser_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"blockby")
        paraDict.setValue(str_other_id, forKey:"blockto")
        
        AppTheme().callPostService(String(format: "%@blockuser", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
   
    func sendReuestToOtherUser_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        self.startProgressBar()

        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"user_id")
        paraDict.setValue(str_other_id, forKey:"other_user_id")
        
        if isUnguide {
            paraDict.setValue("2", forKey:"status")
        } else {
            if isAcceptButton == true
            {
                paraDict.setValue("1", forKey:"status")
            } else if isDecline == true {
                paraDict.setValue("2", forKey:"status")
            } else {
                paraDict.setValue("0", forKey:"status")
            }
        }
      
        AppTheme().callPostService(String(format: "%@guide_friends", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                if self.isUnguide {
                    self.isUnguide = false
                }
                self.loading.dismiss()
                self.getGuideFriendList_ApiCall()
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
   
    func getSendingPendingBoth_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"user_id")
        paraDict.setValue(str_other_id, forKey:"user_id2")
        paraDict.setValue("mobile", forKey:"status")
        
        AppTheme().callPostService(String(format: "%@get_guide_pending_sending_both", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                 self.loading.dismiss()
                 self.respDict = data as! NSDictionary
                
                if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray{
                    self.arrOfResponseKey = array_Response_mutable
                    print("111111 === \(self.arrOfResponseKey)")
                }
                
                if self.arrOfResponseKey.count > 0 {
                    
                var receiverId : String!
                var senderId : String!
                var LoggedInUserID : String!
                var status : String!

                if let str_reciver_id : String = (self.arrOfResponseKey[0] as! NSDictionary).value(forKey: "reciver_id") as? String  {
                    receiverId = str_reciver_id
                }
                if let str_SenderID : String =  (self.arrOfResponseKey[0] as! NSDictionary).value(forKey: "sender_id") as? String {
                    senderId = str_SenderID
                }
                if let str_Status : String =  (self.arrOfResponseKey[0] as! NSDictionary).value(forKey: "status") as? String {
                    status = str_Status
                }
          
                LoggedInUserID = AppTheme.getLoginDetails().object(forKey: "id")! as! String
                
                 if (senderId == LoggedInUserID) && (status == "0")
                 {
                    self.lblGuidePending.text = "Request Pending"
                    self.btnGuide.isUserInteractionEnabled  = false
                    
                 } else if (senderId == LoggedInUserID) && (status == "1")
                 {
                    self.isUnguide = true
                    self.lblGuidePending.text = "Unguide"
                    self.btnGuide.isUserInteractionEnabled  = true
                    
                 } else if (senderId != LoggedInUserID) && (status == "1")
                 {
                    self.lblGuidePending.text = "Guide"
                    self.btnGuide.isUserInteractionEnabled  = true
                    
                 } else if (receiverId == LoggedInUserID) && (status == "0")
                 {
                    self.lblGuidePending.text = "Invitation Pending"
                    self.btnGuide.isUserInteractionEnabled  = false
                    
                    let invitationAlert = UIAlertController(title: "Invitation Pending", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    
                    invitationAlert.addAction(UIAlertAction(title: "Accept", style: .default, handler: { (action: UIAlertAction!) in
                        self.isAcceptButton = true
                        self.sendReuestToOtherUser_ApiCall()
                    }))
                    
                    invitationAlert.addAction(UIAlertAction(title: "Decline", style: .default, handler: { (action: UIAlertAction!) in
                        self.isDecline = true
                        self.sendReuestToOtherUser_ApiCall()
                    }))
                    
                    invitationAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                        
                    }))
                    
                    self.present(invitationAlert, animated: true, completion: nil)
                    
                    } else if (receiverId == LoggedInUserID) && (status == "1") {
                    
                     self.lblGuidePending.text = "Guide"
                     self.btnGuide.isUserInteractionEnabled  = true
                    
                    } else if (senderId == LoggedInUserID) && (receiverId == self.str_other_id) && (status == "1") {
                          print("show button")
                          self.showRightNavigationBarButtonHide(isHide: false)
                     } else {
                         self.showRightNavigationBarButtonHide(isHide: true)
                         print("Hide button")
                    }
                    
                    if (status == "0") {
                        self.showRightNavigationBarButtonHide(isHide: true)
                        print("Hide button")
                    } else {
                        print("show button")
                        self.showRightNavigationBarButtonHide(isHide: false)
                    }
                    
                }
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error! Please try again later.", showInView: self.view)
            }
        }
    }
    
    func getGuideFriendList_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"user_id")
        paraDict.setValue(str_other_id, forKey:"other_user_id")
        
        AppTheme().callPostService(String(format: "%@guide_friends_list", AppUrlNew), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                self.respDict = data as! NSDictionary
                
                self.arrOfResponseKey.removeAllObjects()
                
                if let array_Response_mutable : NSMutableArray = self.respDict["response"] as? NSMutableArray{
                    self.arrOfResponseKey = array_Response_mutable
                    print("22222 === \(self.arrOfResponseKey)")
                }
                
                if self.arrOfResponseKey.count > 0 {
                    
                    var receiverId : String!
                    var senderId : String!
                    var LoggedInUserID : String!
                    var status : String!
                    
                    if let str_reciver_id : String = (self.arrOfResponseKey[0] as! NSDictionary).value(forKey: "reciver_id") as? String  {
                        receiverId = str_reciver_id
                    }
                    if let str_SenderID : String =  (self.arrOfResponseKey[0] as! NSDictionary).value(forKey: "sender_id") as? String {
                        senderId = str_SenderID
                    }
                    if let str_Status : String =  (self.arrOfResponseKey[0] as! NSDictionary).value(forKey: "status") as? String {
                        status = str_Status
                    }
                    
                    LoggedInUserID = AppTheme.getLoginDetails().object(forKey: "id")! as! String
                    
                    if status == "0" {
                        
                        if (senderId == LoggedInUserID) && (status == "0") {
                            self.lblGuidePending.text = "Request Pending"
                            self.btnGuide.isUserInteractionEnabled  = false
                        } else if (receiverId == LoggedInUserID) && (status == "1") {
                            self.lblGuidePending.text = "Invitation Pending"
                            self.btnGuide.isUserInteractionEnabled  = false
                        } else {
                            self.lblGuidePending.text = "Guide"
                            self.btnGuide.isUserInteractionEnabled  = true
                        }
                    } else if status == "1" {
                        self.getSendingPendingBoth_ApiCall()
                    }
                    
                }   else {
                    self.lblGuidePending.text = "Guide"
                    self.btnGuide.isUserInteractionEnabled  = true
                }
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: data as! String, showInView: self.view)
            }
        }
    }
    
    @IBAction func btnActionGuidePendingRequest(_ sender: AnyObject) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        if isUnguide {
            
            let refreshAlert = UIAlertController(title: "Are you sure you want to Unguide?", message: "", preferredStyle: UIAlertControllerStyle.alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                self.sendReuestToOtherUser_ApiCall()
                self.showRightNavigationBarButtonHide(isHide: true)
              }))
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
        } else {
            sendReuestToOtherUser_ApiCall()
        }
    }
    
    @IBAction func btnActionGuided(_ sender: AnyObject) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        var destVC : GuideGuidedViewController!
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "GuideGuidedViewController") as! GuideGuidedViewController
        destVC.titleString = "GUIDED"
        destVC.isFromGuidedView = true
        
        if isFromTrip == "true"{
            destVC.strUserId = str_other_id
        } else {
          destVC.strUserId = userIdForSingleUserDetail
        }
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    @IBAction func btnActionGuide(_ sender: AnyObject) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        var destVC : GuideGuidedViewController!
        destVC = self.storyboard?.instantiateViewController(withIdentifier: "GuideGuidedViewController") as! GuideGuidedViewController
        destVC.titleString = "GUIDES"
        destVC.isFromGuidesView = true

        if isFromTrip == "true" {
            destVC.strUserId = str_other_id
        }else{
            destVC.strUserId = userIdForSingleUserDetail
        }
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
    //Check url nil or not
    func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if urlString == "" || urlString.isEmpty{
                return false
            }else{
                if let url = URL(string: urlString) {
                    // check if your application can open the NSURL instance
                    return UIApplication.shared.canOpenURL(url)
                }
            }
        }
        return false
    }
    
    
    //MARK:- Share, Like-dislike, Delete and comment action
    
   @objc func videoTapped(_ sender: UIButton!) {
    
        print("index: %@", sender.tag)
        let dicData: NSDictionary = self.userPostArray.object(at: sender.tag) as! NSDictionary
        print(dicData)
        if let img_post = dicData .value(forKey: "file_name") as? String{
            let popupView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 160))
            popupView.backgroundColor = UIColor.clear
            let url_video = URL(string: img_post)
            let player = AVPlayer(url: url_video!)
            let playerController = AVPlayerViewController()
            playerController.player = player
            present(playerController, animated: true) {
                player.play()
            }
    
            popupView.addSubview(playerController.view)
        }
    }
    
    @objc func imageTapped(_ sender: UIButton!) {
        
        print("index: %@", sender.tag)
        let dicData: NSDictionary = self.userPostArray.object(at: sender.tag) as! NSDictionary
        print(dicData)
        var popupVC: ImageVideoPlayerController! = ImageVideoPlayerController()
        popupVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageVideoPlayerController") as! ImageVideoPlayerController
        if let img_post = dicData .value(forKey: "file_name") as? String{
            let url_image = URL(string: img_post)
            popupVC.img_url = (url_image! as NSURL) as URL!
        }
        if let str_userName = dicData .value(forKey: "username") as? String{
            popupVC.str_username = str_userName
        }
        let popupDestVC = STPopupController(rootViewController: popupVC)
        popupDestVC.present(in: self)
    }
    
    @objc func shareAction(_ sender: UIButton!)
    {
        print("index: %@", sender.tag )
        let dicData: NSDictionary!
            dicData = self.userPostArray.object(at: sender.tag) as! NSDictionary
        var data : Data = Data()
        var imgShare : UIImage = UIImage()
        var textToShare : String = String()
        
        if let str_textToShare : String = dicData["posts"]! as? String {
            textToShare = str_textToShare
            if let str_img_share : String = dicData["file_name"]! as? String {
                if str_img_share != "" {
                    let urlNew:String = str_img_share.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    let url  = URL(string: urlNew)
                    data = try! Data(contentsOf: url!)
                    imgShare = UIImage(data: data)!
                }
                 else{
                }
            }
        }
        if textToShare != "" && imgShare != nil {
            shareImageText(textToShare, image: imgShare)
        }
        if textToShare == "" && imgShare != nil {
            shareImageText(textToShare, image: imgShare)
        }
        if textToShare != "" && imgShare == nil {
            shareImageText(textToShare, image: imgShare)
        }
        
        
    }
    func shareImageText(_ text: String, image: UIImage){
        
        let objects2Share = [text, image] as [Any]
        let activityVC = UIActivityViewController(activityItems: objects2Share , applicationActivities: nil)
        
        let excludeActivities = [UIActivityType.airDrop, UIActivityType.copyToPasteboard , UIActivityType.mail, UIActivityType.addToReadingList]
        
        activityVC.excludedActivityTypes = excludeActivities
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @objc func likeDislikeAction(_ sender: UIButton!) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        print("index: %@", sender.tag )
        
        let dicData: NSDictionary!
        dicData = self.userPostArray.object(at: sender.tag) as! NSDictionary
        
        let paraDict = NSMutableDictionary()
        
        var strFlighId : String! = String()
        var strPostsId : String! = String()
        
        if let str_flight_id : String = dicData["flight_id"]! as? String{
            strFlighId = str_flight_id
        }
        if let str_post_id : String = dicData["post_id"]! as? String{
            strPostsId = str_post_id
        }
        if let str_likeByMe : String = dicData["like_by_me"]! as? String{
            
            likeByMe = NSInteger(str_likeByMe)!
        } else if let str_likeByMe_int: NSInteger = dicData["like_by_me"]! as?  NSInteger {
            
            likeByMe = str_likeByMe_int
        }
        if likeByMe == 1{

            paraDict.setValue(strFlighId, forKey:"flight_id")
            paraDict.setValue(strPostsId, forKey:"posts_id")
            paraDict.setValue(strUserId, forKey:"user_id")
            paraDict.setValue("0", forKey:"status")
            
        } else {

            paraDict.setValue(strFlighId, forKey:"flight_id")
            paraDict.setValue(strPostsId, forKey:"posts_id")
            paraDict.setValue(strUserId, forKey:"user_id")
            paraDict.setValue("1", forKey:"status")
        }
        AppTheme().callPostService(String(format: "%@posts_like", AppUrl), param: paraDict) {(result, data) -> Void in                                    ///Call services
            
            if result == "success"
            {
                self.loading.dismiss()
                
                if HomeVC.isUserDetailLike == true {
                    
                    if self.isFromTrip == "true" {
                        self.getUserStatusList()
                    } else if self.isFromTrip == "Notification" {
                        if let str_userId = self.dic .value(forKey: "sender") as? String{
                            self.userIdForSingleUserDetail = str_userId
                            self.getSingleUserInfo(str_userId)
                        }
                    } else if self.isFromTrip == "SearchUser"{
                        if let str_userId = self.dic .value(forKey: "id") as? String{
                            self.userIdForSingleUserDetail = str_userId
                            self.getSingleUserInfo(str_userId)
                        }
                    } else {
                        if let str_userId = self.dic .value(forKey: "user_id") as? String{
                            self.userIdForSingleUserDetail = str_userId
                            self.getSingleUserInfo(str_userId)
                        }
                    }
                } else{
                    self.getUserStatusList()
                }
            }
        }
    }
    
    @objc func commentAction(_ sender: UIButton!){
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        print("index: %@", sender.tag )
        
        let objCommentVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedsCommentVC") as! FeedsCommentVC
        
        if sender.tag >= 1000 {
            
            sender.tag = sender.tag - 1000
            FeedsCommentVC.isFromUser = true
        } else {
                    FeedsCommentVC.isFromUser = false
                }
                if HomeVC.isUserDetailLike == true {
                    if let dict_temp : NSDictionary = self.userPostArray.object(at: sender.tag) as? NSDictionary{
                    objCommentVC.dicData = dict_temp
                }
            }
        self.navigationController?.pushViewController(objCommentVC, animated: true)
    }
    
    @objc func deletePost(_ sender: UIButton!)
    {
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        print("index: %@", sender.tag )
        let refreshAlert = UIAlertController(title: "Delete post", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            let dicData: NSDictionary!
                dicData = self.userPostArray.object(at: sender.tag) as! NSDictionary
            if let strPostsId : String = dicData["post_id"]! as? String {
                self.deleteSelectedPost(strPostsId)
            }
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func deleteSelectedPost(_ post_id : String)  {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(post_id, forKey:"post_id")
        
        AppTheme().callPostService(String(format: "%@post_delete", AppUrl), param: paraDict) {(result, data) -> Void in                                   ///Call services
            
            if result == "success" {
                _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: "Post deleted!", showInView: self.view )
                self.getUserStatusList()
            } else {
                self.loading.dismiss()
               _ = self.hudType(JGProgressHUDSuccessIndicatorView(), textLabel: data as! String, showInView: self.view )
            }
        }
    }

    func getUserStatusList()
    {

        let currentTimeZone: String = (TimeZone.current as NSTimeZone).description
        let arrayTime: NSArray = currentTimeZone .components(separatedBy: " ") as NSArray
        let zoneString : String = arrayTime .object(at: 0) as! String
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        let parameters : [String: AnyObject] = ["flight_id" : "" as AnyObject,
                                                "user_id" : strUserId as AnyObject,
                                                "timezone" : zoneString as AnyObject]  //dictForFlight["FlightNumber"]! as AnyObject ]
        
        AppTheme().callGetService(String(format: "%@get_posts", AppUrl), param: parameters) {(result, data) -> Void in                                   ///Call services
            if result == "success"
            {
                self.loading.dismiss()
                if let arrResponse: NSMutableArray = data as? NSMutableArray{
                    print("arrResponse: @%", arrResponse)
                } else if let dicRes: NSDictionary = data as? NSDictionary {
                    if let tempArr: NSArray = dicRes["response"] as? NSArray
                    {
                        self.userPostArray = tempArr.mutableCopy() as! NSMutableArray
                        if self.userPostArray.count == 0 || self.userPostArray.isEqual(nil)
                        {
                        } else{
                            self.userPostArray = tempArr.mutableCopy() as! NSMutableArray
                        }
                    } else {
                        self.userPostArray = dicRes["response"] as! NSMutableArray
                    }
                }
                self.tblUserDetailFollowUnfollow.reloadData()
            } else{
                self.loading.dismiss()
               _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Sorry there is some network error", showInView: self.view)
            }
        }
    }
    
    //==============================
    //MARK: Loader method
    //==============================
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    // MARK: Image Resize Code
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()        
        return newImage!
    }
    
    func ResizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func getUserInfoOnTag(_ str_id : String, str_user_name : String) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
            let parameters : [String: AnyObject] = ["id" : "" as AnyObject,
                                                    "username" : str_user_name as AnyObject]
            AppTheme().callGetService(String(format: "%@user_info", AppUrl), param: parameters) { (result, data) -> Void in
                
                if result == "success"{
                    self.loading.dismiss()
                    var temp_arr : NSMutableArray! = NSMutableArray()
                    var temp_dict : NSDictionary! = NSDictionary()
                    if let arrResponse: NSMutableArray = data as? NSMutableArray
                    {
                        print("arrResponse: @%", arrResponse)
                    }
                    else if let dicRes: NSDictionary = data as? NSDictionary
                    {
                        if let tempArr: NSArray = dicRes["response"] as? NSArray
                        {
                            temp_arr = tempArr.mutableCopy() as! NSMutableArray
                            if temp_arr.count == 0 || temp_arr.isEqual(nil)
                            {
                                
                            } else{
                                temp_arr = tempArr.mutableCopy() as! NSMutableArray
                            }
                        } else {
                            temp_arr = dicRes["response"] as! NSMutableArray
                        }
                        for element in temp_arr {
                            
                            temp_dict = element as! NSDictionary
                            if let str_other_id = temp_dict .value(forKey: "id") as? String {
                                
                                let objFlightDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
                                objFlightDetailVC.str_other_id = str_other_id
                                objFlightDetailVC.isFromTrip = "true"
                                self.navigationController?.pushViewController(objFlightDetailVC, animated: true)
                            }
                        }
                    }
                }
            }
        }
    
    //MARK:- User check for online/offline
    func userOnChat() {
        
//        if appDelegate.isFromLeftMenu == true {
//            userIdForSingleUserDetail = appDelegate.strUserID
//            getSingleUserInfo(userIdForSingleUserDetail)
//        } else {
        
        if let str : String =  AppTheme.getLoginDetails().value(forKey: "id") as? String {
            
            if isFromTrip == "true" {
                
                callUserOffLine(str, othersID: str_other_id)
            } else if isFromTrip == "Notification" {
                if let str_userId = dic .value(forKey: "sender") as? String{
                    callUserOffLine(str, othersID: str_userId)
                }
            } else if isFromTrip == "SearchUser"{
                if let str_userId = dic .value(forKey: "id") as? String{
                    callUserOffLine(str, othersID: str_userId)
                }
            } else {
                if let str_userId = dic .value(forKey: "user_id") as? String{
                    callUserOffLine(str, othersID: str_userId)
                }
            }
          }
     //   }
    }
    
    func  callUserOffLine(_ userID: String, othersID: String) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        let paraDict = NSMutableDictionary()
        paraDict.setValue(userID, forKey:"post_id")
        paraDict.setValue(othersID, forKey:"to_id")
        paraDict.setValue("0", forKey:"status")
        
        AppTheme().callPostService(String(format: "%@user_online", AppUrl), param: paraDict) {(result, data) -> Void in
            if result == "success"
            {
                print("Change status")
            }
        }
    }
}


