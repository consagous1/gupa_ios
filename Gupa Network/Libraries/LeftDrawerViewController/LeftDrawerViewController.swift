//
//  LeftDrawerViewController.swift
//  Gupa Network
//
//  Created by mac on 5/17/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD
import Kingfisher
import AddressBook
import FacebookLogin


class LeftDrawerViewController: UIViewController, UITableViewDataSource, UITableViewDelegate , SWRevealViewControllerDelegate {
    
    @IBOutlet var imgProfile: UIImageView!
    //Stirng value of user for session manage
    var strForSessionMana: String!
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblEmail: UILabel!
    
    //Controls use on the local page
    var arrForMenuOption: NSMutableArray = []
    var dictForMenuForLoginSignUp: NSDictionary = NSDictionary()
    var dictForMenuForLogined: NSDictionary = NSDictionary()
    var arrForLoginSignup: NSMutableArray = []
    var arrForMenuForLogined: NSMutableArray = []
    var slideOutAnimationEnabled: Bool!
    var commonViewController: UIViewController!
    var i: Int!
    var logUsrSession: UserDefaults!
    var dictOfResponseKey : NSDictionary = NSDictionary()

    @IBOutlet var tblMenuOption: UITableView!
    @IBOutlet weak var btnLoginSignup: UIButton!

    var loading : JGProgressHUD! = JGProgressHUD()
    var isEditProfile: Bool!
    var appDelegate: AppDelegate = AppDelegate()
    var sidebarMenuOpen = false
    var overlayView: UIView = UIView()
    var revealController: SWRevealViewController?
    
    let imageCache = NSCache<NSString, UIImage>()
    var databaseManager = DatabaseManager.sharedDBInterface // Local database

    
  //MARK:- Life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.revealViewController().delegate = self
        
        if let isLogin = AppTheme.getIsLogin() as? Bool {
        if isLogin == true {
        
         self.revealViewController().panGestureRecognizer()
         self.revealViewController().tapGestureRecognizer()
          }
        }
        
        getUserDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if (self.revealViewController()) != nil{
            self.revealViewController().frontViewController.view.isUserInteractionEnabled = false
        }
        
        if let isLogin = AppTheme.getIsLogin() as? Bool {
        if isLogin == true {
            
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.imgProfile.layer.cornerRadius = 45 // self.imgProfile.frame.height/2
        self.imgProfile.layer.masksToBounds = true
        
        let strUserName = UserDefaults.standard.string(forKey: "UserName")
        
        if (strUserName ?? "").isEmpty  {
            if let str_user_name : String = AppTheme.getLoginDetails().object(forKey: "full_name") as? String {
                self.lblName.text = str_user_name
            }
        } else {
            if let str_user_name : String = UserDefaults.standard.string(forKey: "UserName") {
                self.lblName.text = str_user_name
            }
        }
        
        if let str_user_email : String = AppTheme.getLoginDetails().object(forKey: "email") as? String {
            self.lblEmail.text = str_user_email
        }
         setProfileImage()
            }
        }
    }
    
    func setProfileImage(){
        
        if let str_profile_pic : String = AppTheme.getLoginDetails().value(forKey: "profile_pic") as? String{
            let url : URL = URL(string: str_profile_pic)!
                self.imgProfile.kf.setImage(with: url)
        }
    }
 
    override func viewWillDisappear(_ animated: Bool) {
        
        if let revealVC = self.revealViewController() {
            revealVC.frontViewController.view.isUserInteractionEnabled = true
        }
    }
    
    func getUserDetail() {
       
        //Array For Login signup user
        arrForLoginSignup = NSMutableArray()
        dictForMenuForLoginSignUp = ["MenuItemName": "Home", "MenuItemImage": "ic_home.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Book A Taxi", "MenuItemImage": "TaxiApp.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Travel Information", "MenuItemImage": "ic_travel_activity.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Airport Services", "MenuItemImage": "ic_airpor.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Road Transport", "MenuItemImage": "img_bus.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Flight Ratings", "MenuItemImage": "ic_flight.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Hotel Ratings", "MenuItemImage": "ic_hotelBedBlue.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Chats", "MenuItemImage": "chattingSideMenu.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Road Transport Ratings", "MenuItemImage": "img_transport_rating.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Edit Profile", "MenuItemImage": "ic_settings.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Blocked Users", "MenuItemImage": "blockeduser.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Change Password", "MenuItemImage": "ic_changePassword.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Invite", "MenuItemImage": "invite_image.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Raise A Qurey", "MenuItemImage": "RequestQuery.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "About Us", "MenuItemImage": "About-Us.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Terms & Conditions", "MenuItemImage": "ic_term&cond.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Privacy Settings", "MenuItemImage": "ic_settings.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Notifications", "MenuItemImage": "alarm-1.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
        dictForMenuForLoginSignUp = ["MenuItemName": "Logout", "MenuItemImage": "ic_logout.png"]
        arrForLoginSignup.add(dictForMenuForLoginSignUp)
    }
    
    func startProgressBar(){
        
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK :- Table view delegate data source methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if arrForLoginSignup.count != 0 {
             arrForMenuOption = arrForLoginSignup
            return arrForMenuOption.count
        }
      return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: LeftMenuCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell", for: indexPath) as! LeftMenuCell
            let dicTest: NSDictionary! = arrForLoginSignup[indexPath.row] as! NSDictionary
            cell.lblMenuItem.text = String( dicTest["MenuItemName"] as! String)
            cell.imgMenuImage.image = UIImage(named: (dicTest["MenuItemImage"] as! String))
            cell.lbl_noti_count.isHidden = true
            if String(dicTest["MenuItemName"] as! String) == "Notifications" {
               cell.lbl_noti_count.isHidden = false
                if let noti_count: NSInteger = AppTheme.getNotiCount() {
                    cell.lbl_noti_count.text = String(noti_count)
                }
             }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
            self.startProgressBar()
            switch indexPath.row {
                
            case 0:
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.loading.dismiss()
                break;

            case 1:
               commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TaxiBookingsVC") as! TaxiBookingsVC
                self.loading.dismiss()
                break;

            case 2:
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TravelActivityViewController") as! TravelActivityViewController
                self.loading.dismiss()
                break;

            case 3:
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "AirportActivityViewController") as! AirportActivityViewController
                self.loading.dismiss()
                break;

            case 4:
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TransportController") as! TransportController
                self.loading.dismiss()
                break;

            case 5:
                appDelegate.strURL = "Flights"
                AppTheme.setFirstTimeFlight(true)
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "FlightRatingVC") as! FlightRatingVC
                self.loading.dismiss()
                break;

            case 6:
                appDelegate.strURL = "HotelSelect"
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "HotelRatingVC") as! HotelRatingVC
                self.loading.dismiss()
                break;

            case 7:
                //   appDelegate.strURL = "HomeViewController"
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.loading.dismiss()
                break;

            case 8:
                appDelegate.strURL = "TransportSelect"
                AppTheme.setFirstTimeTransport(true)
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TransportRatingsController") as! TransportRatingsController
                self.loading.dismiss()
                break;

            case 9:
                appDelegate.strURL = "editVC"
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                self.loading.dismiss()
                break;

            case 10:
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlockLIstVC") as! BlockLIstVC
                self.loading.dismiss()
                break;

            case 11:
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                self.loading.dismiss()
                break;

            case 12:
                determineStatus()
                self.loading.dismiss()

                break;

            case 13:
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "RaiseAQureyVC") as! RaiseAQureyVC
                self.loading.dismiss()
                break;

            case 14:
                //AboutUs
                appDelegate.strURL   = "OnSideBar"
                appDelegate.strLink  =  AppLinkAboutUs
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionController") as! TermsAndConditionController
                self.loading.dismiss()
                break;

            case 15:
                appDelegate.strURL   = "OnSideBar"
                appDelegate.strLink  = "http://gupanetwork.com/#/toc"
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionController") as! TermsAndConditionController
                self.loading.dismiss()
                break;

            case 16:
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacySettingsVC") as! PrivacySettingsVC
                self.loading.dismiss()
                break;

            case 17:
                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListController") as! NotificationListController
                self.loading.dismiss()
                break;

            case 18:
                UserDefaults.standard.set(1, forKey: "ForLogout")
                UserDefaults.standard.synchronize()

                let loginManager = LoginManager()
                FBSDKAccessToken.setCurrent(nil)
                FBSDKProfile.setCurrent(nil)
                UserDefaults.standard.removeObject(forKey: "UserName")

                databaseManager.deleteRecordsFrom(tableName: DBConstants.DBTableName.TABLE_POST)
                databaseManager.deleteRecordsFrom(tableName: DBConstants.DBTableName.TABLE_REVIEW)

                loginManager.logOut()

                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.loading.dismiss()
                break
            default:
                break
        }
//            case 0:
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//             self.loading.dismiss()
//                break;
//
////            case 1:
////                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TaxiBookingsVC") as! TaxiBookingsVC
////                self.loading.dismiss()
////                break;
//
//            case 1:
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TravelActivityViewController") as! TravelActivityViewController
//                self.loading.dismiss()
//                break;
//
//            case 2:
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "AirportActivityViewController") as! AirportActivityViewController
//                self.loading.dismiss()
//                break;
//
//            case 3:
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TransportController") as! TransportController
//                self.loading.dismiss()
//                break;
//
//            case 4:
//                appDelegate.strURL = "Flights"
//                AppTheme.setFirstTimeFlight(true)
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "FlightRatingVC") as! FlightRatingVC
//                self.loading.dismiss()
//                break;
//
//            case 5:
//                appDelegate.strURL = "HotelSelect"
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "HotelRatingVC") as! HotelRatingVC
//                self.loading.dismiss()
//                break;
//
//           case 6:
//             //   appDelegate.strURL = "HomeViewController"
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                self.loading.dismiss()
//                break;
//
//            case 7:
//                appDelegate.strURL = "TransportSelect"
//                 AppTheme.setFirstTimeTransport(true)
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TransportRatingsController") as! TransportRatingsController
//                self.loading.dismiss()
//                break;
//
//            case 8:
//                 appDelegate.strURL = "editVC"
//                 commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
//                 self.loading.dismiss()
//                break;
//
//            case 9:
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlockLIstVC") as! BlockLIstVC
//                self.loading.dismiss()
//                break;
//
//            case 10:
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
//                self.loading.dismiss()
//                break;
//
//            case 11:
//            determineStatus()
//                break;
//
//            case 12:
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "RaiseAQureyVC") as! RaiseAQureyVC
//                self.loading.dismiss()
//                break;
//
//            case 13:
//                //AboutUs
//                appDelegate.strURL   = "OnSideBar"
//                appDelegate.strLink  =  AppLinkAboutUs
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionController") as! TermsAndConditionController
//                self.loading.dismiss()
//                break;
//
//            case 14:
//                appDelegate.strURL   = "OnSideBar"
//                appDelegate.strLink  = "http://gupanetwork.com/#/toc"
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionController") as! TermsAndConditionController
//                self.loading.dismiss()
//                break;
//
//            case 15:
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacySettingsVC") as! PrivacySettingsVC
//                self.loading.dismiss()
//                break;
//
//            case 16:
//                commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListController") as! NotificationListController
//                self.loading.dismiss()
//                break;
//
//            case 17:
//               UserDefaults.standard.set(1, forKey: "ForLogout")
//               UserDefaults.standard.synchronize()
//
//               let loginManager = LoginManager()
//               FBSDKAccessToken.setCurrent(nil)
//               FBSDKProfile.setCurrent(nil)
//               loginManager.logOut()
//
//               commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//               self.loading.dismiss()
//                break
//            default:
//                break
//        }
        
        if indexPath.row != 12 {
            let centerNavCont = UINavigationController(rootViewController: commonViewController)
            centerNavCont.setViewControllers([commonViewController], animated: true)
            self.revealViewController().setFront(centerNavCont, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
 
        }
   }
    
    
    //=============MJ 19 Nov 16 ========================
    //===Calling Address book permission method=========
    
    func determineStatus()
    {
        
        let status = ABAddressBookGetAuthorizationStatus()
        switch status {
            
        case .authorized:

             self.commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "InviteController") as! InviteController
             let centerNavCont = UINavigationController(rootViewController: self.commonViewController)
             centerNavCont.setViewControllers([commonViewController], animated: true)
             self.revealViewController().setFront(centerNavCont, animated: true)
             self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
            
        case .notDetermined:
            
            let ok = false
            ABAddressBookRequestAccessWithCompletion(nil)
            {
                (granted:Bool, err:CFError!) in
                DispatchQueue.main.async
                {
                    if granted
                    {

                       self.commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "InviteController") as! InviteController
                        let centerNavCont = UINavigationController(rootViewController: self.commonViewController)
                        centerNavCont.setViewControllers([self.commonViewController], animated: true)
                        self.revealViewController().setFront(centerNavCont, animated: true)
                        self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
                    }
                }
            }
            if ok == true {
            }
        case .restricted:
            self.loading.dismiss()
            break
        case .denied:
            
            Utility.showAlertWithMessage("", title: "You have denied the permisssion.Please go to the Settings for allow access you contact.")
            self.loading.dismiss()
            break
        }
    }
    
    
    //MARK: Login button action
    @IBAction func btnLoginSignup(_ sender: AnyObject) {
        
        commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let centerNavCont = UINavigationController(rootViewController: commonViewController)
        centerNavCont.setViewControllers([commonViewController], animated: true)
        self.revealViewController().setFront(centerNavCont, animated: true)
        self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
    }
    
    @IBAction func btnProfileDetail_Action(_ sender: UIButton!)  {
        
//        let strLoggedInuerId = AppTheme.getLoginDetails().value(forKey: "id") as? String
//        print("Logged in User Id = \(String(describing: strLoggedInuerId!))")
//
//        if strLoggedInuerId != nil {
//
//            appDelegate.strUserID = strLoggedInuerId!
//            appDelegate.isFromLeftMenu = true
//
//            self.commonViewController = self.storyboard?.instantiateViewController(withIdentifier: "UserChatOnCommentController") as! UserChatOnCommentController
//            let centerNavCont = UINavigationController(rootViewController: self.commonViewController)
//            centerNavCont.setViewControllers([self.commonViewController], animated: true)
//            self.revealViewController().setFront(centerNavCont, animated: true)
//            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
//        }
    }
    
}

