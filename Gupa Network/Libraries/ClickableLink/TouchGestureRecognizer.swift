//
//  TouchGestureRecognizer.swift
//  TestUILabelHyperlink
//
//  Created by Colin Reisterer on 4/4/16.
//  Copyright © 2016 CRCode. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

class TouchGestureRecognizer: UIGestureRecognizer {
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    state = UIGestureRecognizerState.began
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
    state = .failed
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
    state = .ended
  }
  
  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
    state = .cancelled
  }
  
}
