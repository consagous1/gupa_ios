//
//  NSAttributedString+Extensions.swift
//  TestUILabelHyperlink
//
//  Created by Colin Reisterer on 4/4/16.
//  Copyright © 2016 CRCode. All rights reserved.
//

import UIKit

extension NSAttributedString {
  
  func URLAtIndex(_ location: Int, effectiveRange: NSRangePointer) -> URL? {
    return attribute(NSAttributedStringKey.link, at: location, effectiveRange: effectiveRange) as? URL
  }
  
}

extension NSMutableAttributedString {
 
  func textAsLink(_ textToFind: String, withLinkURL url: URL, linkColor: UIColor?=nil) -> Bool {
    let range = mutableString.range(of: textToFind, options: .caseInsensitive)
    if range.location != NSNotFound {
        addAttribute(NSAttributedStringKey.link, value: url, range: range)
      if let _linkColor = linkColor {
        addAttribute(NSAttributedStringKey.foregroundColor, value: _linkColor, range: range)
      }
      return true
    } else {
      return false
    }
  }

}
