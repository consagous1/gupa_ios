//
//  SJPlayerView.swift
//  E-polis
//
//  Created by Mac on 8/4/16.
//  Copyright © 2016 Sumit Jagdev. All rights reserved.
//

import UIKit

import AVFoundation
import QuartzCore

private var playbackLikelyToKeepUpContext = 0

class SJPlayerView: UIView {
    let startPlayImage = UIImage(named: "img_play")
    let pauseImage = UIImage(named: "img_pause")
    let avPlayer = AVPlayer()
    var avPlayerLayer: AVPlayerLayer!
    let invisibleButton = UIButton()
    var timeObserver: AnyObject!
    let timeRemainingLabel = UILabel()
 //   let seekSlider = UISlider()
    var playerRateBeforeSeek: Float = 0
    let loadingIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    
        // An AVPlayerLayer is a CALayer instance to which the AVPlayer can
        // direct its visual output. Without it, the user will see nothing.
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        self.layer.insertSublayer(avPlayerLayer, at: 0)
        
        let timeInterval: CMTime = CMTimeMakeWithSeconds(1.0, 10)
        timeObserver = avPlayer.addPeriodicTimeObserver(forInterval: timeInterval,
                                                                   queue: DispatchQueue.main) { (elapsedTime: CMTime) -> Void in
                                                                    // print("elapsedTime now:", CMTimeGetSeconds(elapsedTime))
                                                                    self.observeTime(elapsedTime)
        } as AnyObject
        timeRemainingLabel.textColor = .darkGray
        self.addSubview(timeRemainingLabel)
        
        self.addSubview(invisibleButton)
        //        invisibleButton .backgroundColor = UIColor.redColor()
        invisibleButton.addTarget(self, action: #selector(invisibleButtonTapped),
                                  for: .touchUpInside)
        invisibleButton.setImage(startPlayImage, for: UIControlState())
        invisibleButton.setImage(pauseImage, for: .selected)
//        invisibleButton .setImage(UIImage(named: "play"), forState: UIControlState.Normal)
        
//        self.addSubview(seekSlider)
//        seekSlider.addTarget(self, action: #selector(sliderBeganTracking),
//                             forControlEvents: .TouchDown)
//        seekSlider.addTarget(self, action: #selector(sliderEndedTracking),
//                             forControlEvents: [.TouchUpInside, .TouchUpOutside])
//        seekSlider.addTarget(self, action: #selector(sliderValueChanged),
//                             forControlEvents: .ValueChanged)
//        
//        seekSlider.userInteractionEnabled = true
        
        
        loadingIndicatorView.hidesWhenStopped = true
        self.addSubview(loadingIndicatorView)
        avPlayer.addObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp",
                             options: .new, context: &playbackLikelyToKeepUpContext)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Layout subviews manually
        avPlayerLayer.frame = self.bounds
        invisibleButton.frame = self.bounds
        
        let controlsHeight: CGFloat = 30
        let controlsY: CGFloat = self.bounds.size.height - controlsHeight
        timeRemainingLabel.frame = CGRect(x: 5, y: controlsY, width: 60, height: controlsHeight)
        
        timeRemainingLabel.frame = CGRect(x: 5, y: controlsY, width: 60, height: controlsHeight)
       // seekSlider.frame = CGRect(x: timeRemainingLabel.frame.origin.x + timeRemainingLabel.bounds.size.width,
                           //       y: controlsY, width: self.bounds.size.width - timeRemainingLabel.bounds.size.width - 5, height: controlsHeight)
        loadingIndicatorView.center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        
    }
    
    func setVideoURL( _ url : URL) {
        let playerItem = AVPlayerItem(url: url)
        avPlayer.replaceCurrentItem(with: playerItem)
//        loadingIndicatorView.startAnimating()
        self.isHidden = false
    }
    
    fileprivate func updateTimeLabel(elapsedTime: Float64, duration: Float64) {
        print("videoDuration : %@",elapsedTime)
        print("duration : %@",duration)
        
        let timeRemaining: Float64 = CMTimeGetSeconds(avPlayer.currentItem!.duration) - elapsedTime
        timeRemainingLabel.text = String(format: "%02d:%02d", ((lround(timeRemaining) / 60) % 60), lround(timeRemaining) % 60)
        
        //        let videoDuration = CMTimeGetSeconds(avPlayer.currentItem!.duration)
       /// seekSlider.value = Float(elapsedTime/duration)
       // print("seekSlider.value : %@",seekSlider.value)
    }
    
    fileprivate func observeTime(_ elapsedTime: CMTime) {
        let duration = CMTimeGetSeconds(avPlayer.currentItem!.duration)
      //Rajesh
//        if isFinite(duration) {
//           // let elapsedTime = CMTimeGetSeconds(elapsedTime)
//          //  updateTimeLabel(elapsedTime: elapsedTime, duration: duration)
//
//        }
    }
    
    @objc func invisibleButtonTapped(_ sender: UIButton) {
        let playerIsPlaying = avPlayer.rate > 0
        if playerIsPlaying {
            avPlayer.pause()
            invisibleButton.setImage(pauseImage, for: .selected)
        } else {
            avPlayer.play() // Start the playback
            invisibleButton.setImage(startPlayImage, for: UIControlState())
        }
    }
    
    deinit {
        avPlayer.removeTimeObserver(timeObserver)
        avPlayer.removeObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp")
    }
    
    func sliderBeganTracking(_ slider: UISlider) {
        if avPlayer.currentItem == nil {
            return;
        }
        playerRateBeforeSeek = avPlayer.rate
        avPlayer.pause()
    }
    
    func sliderEndedTracking(_ slider: UISlider) {
        if avPlayer.currentItem == nil {
            return;
       // }
       // let videoDuration = CMTimeGetSeconds(avPlayer.currentItem!.duration)
      //  let elapsedTime: Float64 = videoDuration * Float64(seekSlider.value)
      //  updateTimeLabel(elapsedTime: elapsedTime, duration: videoDuration)
        
      //  avPlayer.seekToTime(CMTimeMakeWithSeconds(elapsedTime, 100)) { (completed: Bool) -> Void in
      //      if self.playerRateBeforeSeek > 0 {
      //          self.avPlayer.play()
     //       }
        }
    }
    
    func sliderValueChanged(_ slider: UISlider) {
        if avPlayer.currentItem == nil {
            return;
        }
        //let videoDuration = CMTimeGetSeconds(avPlayer.currentItem!.duration)
      ///  let elapsedTime: Float64 = videoDuration * Float64(seekSlider.value)
      //  updateTimeLabel(elapsedTime: elapsedTime, duration: videoDuration)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                                         change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if context == &playbackLikelyToKeepUpContext {
            if avPlayer.currentItem!.isPlaybackLikelyToKeepUp {
                loadingIndicatorView.stopAnimating()
            } else {
                loadingIndicatorView.startAnimating()
            }
        }
    }

}
