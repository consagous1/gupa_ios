//
//  DateTimeUtils.swift
//  TextApp
//


import Foundation

class DateTimeUtils {
    
    static let sharedInstance = DateTimeUtils()
    
    static let MILLI_SECOND_IN_HOUR:Double = 1000*60*60 //long
    static let MILLI_SECOND_IN_TWO_HOUR:Double = MILLI_SECOND_IN_HOUR * 2
    static let MILLI_SECOND_IN_DAY:Double = MILLI_SECOND_IN_HOUR*24;
    
    //    private let DF_TIME_hh_mm_aa = "hh:mm a"
    //    private let DF_TIME_yyyy_MM_dd = "yyyy-MM-dd"
    //    private let DF_MMMM_dd_yyyy_HHmmaa = "yyyy hh:mm a"
    //    private let DF_ISO_8601 = "yyyy-MM-dd 'T' HH:mm:ss 'Z'"
    
    private var DF_TIME_hh_mm_aa:DateFormatter?
    private var DF_TIME_yyyy_MM_dd:DateFormatter?
    private var DF_MMMM_dd_yyyy_HHmmaa:DateFormatter?
    
    private var DF_TIME_dd_MMM_yy:DateFormatter?
    private var  DF_TIME_dd_MMMM_yyyy:DateFormatter?
    private var  DF_dd_MMM_yyyy:DateFormatter?
    
    private var  DF_DAY_MMM_dd_yyyy:DateFormatter?
    
    private var  DF_dd_MMM_YYYY_HHMMa:DateFormatter?
    
    private var  DF_MMMM:DateFormatter?
    
    private var  DF_FULL_DAY_DD_MM_YYYY:DateFormatter?
    
    private var DF_ISO_8601:DateFormatter?
    
    private var msgTimeTextAnHourAgo:String?
    private var msgTimeTextTodayAt:String?
    private var msgTimeTextYesterdayAt:String?
    
    init(){
        loadFormatter()
        loadFormatterText()
    }
    
    //@SuppressLint("DefaultLocale")
    func formatTimeSeconds(timeInSeconds:CLong) -> String{
        let second = timeInSeconds % 60;
        let minute = (timeInSeconds / 60) % 60;
        let hour = (timeInSeconds / (60 * 60)) % 24;
        return String(format: "%02d:%02d:%02d", hour, minute, second)
    }
    
    //    @SuppressLint("DefaultLocale")
    func formatTimeSecondsReturnsMin(timeInSeconds:CLong) ->String{
        let second = timeInSeconds % 60
        let minute = (timeInSeconds / 60) % 60
        return String(format:"%02d:%02d", minute, second)
    }
    //
    func loadFormatter() {
        
        DF_TIME_hh_mm_aa = DateFormatter()
        if(DF_TIME_hh_mm_aa != nil){
            DF_TIME_hh_mm_aa?.dateFormat = "hh:mm a"
            DF_TIME_hh_mm_aa?.timeZone = TimeZone.current
            DF_TIME_hh_mm_aa?.locale = Locale.current
        }
        
        
        DF_TIME_yyyy_MM_dd = DateFormatter()
        if(DF_TIME_yyyy_MM_dd != nil){
            DF_TIME_yyyy_MM_dd?.dateFormat = "yyyy/MM/dd"
            DF_TIME_yyyy_MM_dd?.timeZone = TimeZone.current
            DF_TIME_yyyy_MM_dd?.locale = Locale.current
        }
        
        //
        DF_TIME_dd_MMM_yy = DateFormatter()
        if(DF_TIME_dd_MMM_yy != nil){
            DF_TIME_dd_MMM_yy?.dateFormat = "dd/MM/yyyy"
            DF_TIME_dd_MMM_yy?.timeZone = TimeZone.current
            DF_TIME_dd_MMM_yy?.locale = Locale.current
        }
        
        
        DF_MMMM_dd_yyyy_HHmmaa = DateFormatter()
        if(DF_MMMM_dd_yyyy_HHmmaa != nil){
            DF_MMMM_dd_yyyy_HHmmaa?.dateFormat = "MMMM dd yyyy hh:mm a"
            DF_MMMM_dd_yyyy_HHmmaa?.timeZone = TimeZone.current
            DF_MMMM_dd_yyyy_HHmmaa?.locale = Locale.current
        }
        
        DF_ISO_8601 = DateFormatter()
        if(DF_ISO_8601 != nil){
            DF_ISO_8601?.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            DF_ISO_8601?.timeZone = TimeZone.current
            DF_ISO_8601?.locale = Locale.current
        }
        
        DF_TIME_dd_MMMM_yyyy = DateFormatter()
        if(DF_TIME_dd_MMMM_yyyy != nil){
            DF_TIME_dd_MMMM_yyyy?.dateFormat = "dd MMMM yyyy"
            DF_TIME_dd_MMMM_yyyy?.timeZone = TimeZone.current
            DF_TIME_dd_MMMM_yyyy?.locale = Locale.current
        }
        
        DF_dd_MMM_yyyy = DateFormatter()
        if(DF_dd_MMM_yyyy != nil){
            DF_dd_MMM_yyyy?.dateFormat = "dd MMM yyyy"
            DF_dd_MMM_yyyy?.timeZone = TimeZone.current
            DF_dd_MMM_yyyy?.locale = Locale.current
        }
        
        DF_DAY_MMM_dd_yyyy = DateFormatter()
        if(DF_DAY_MMM_dd_yyyy != nil){
            DF_DAY_MMM_dd_yyyy?.dateFormat = "EEE, dd MMM, yyyy"
            DF_DAY_MMM_dd_yyyy?.timeZone = TimeZone.current
            DF_DAY_MMM_dd_yyyy?.locale = Locale.current
        }
        
        DF_dd_MMM_YYYY_HHMMa  = DateFormatter()
        if(DF_dd_MMM_YYYY_HHMMa != nil){
            DF_dd_MMM_YYYY_HHMMa?.dateFormat = "dd-MMM-yyyy hh:mm a"
            DF_dd_MMM_YYYY_HHMMa?.timeZone = TimeZone.current
            DF_dd_MMM_YYYY_HHMMa?.locale = Locale.current
        }
        
        
        DF_MMMM = DateFormatter()
        if(DF_MMMM != nil){
            DF_MMMM?.dateFormat = "MMMM yyyy"
            DF_MMMM?.timeZone = TimeZone.current
            DF_MMMM?.locale = Locale.current
        }
        
        DF_FULL_DAY_DD_MM_YYYY = DateFormatter()
        if(DF_FULL_DAY_DD_MM_YYYY != nil){
            DF_FULL_DAY_DD_MM_YYYY?.dateFormat = "EEEE, dd MMMM, yyyy"
            DF_FULL_DAY_DD_MM_YYYY?.timeZone = TimeZone.current
            DF_FULL_DAY_DD_MM_YYYY?.locale = Locale.current
        }
    }
    
    func loadFormatterText() { //Must reloaded in case of locale change.
        msgTimeTextAnHourAgo = BaseApp.sharedInstance.getMessageForCode("msg_time_an_hour_ago", fileName: "Strings")!
        msgTimeTextTodayAt = BaseApp.sharedInstance.getMessageForCode("msg_time_today_at", fileName: "Strings")!
        msgTimeTextYesterdayAt = BaseApp.sharedInstance.getMessageForCode("msg_time_yesterday_at", fileName: "Strings")!
    }
    
    func reminderDateTimeFormat(date:Date) ->String? {
        return (DF_MMMM_dd_yyyy_HHmmaa?.string(from: date))! //TODO change format as required.
    }
    
    func formatSimpleTime(date:Date) ->String? {
        return (DF_TIME_hh_mm_aa?.string(from: date))! //TODO change format as required.
    }
    
    func formatSimpleDateTime(date:Date) ->String? {
        return (DF_MMMM_dd_yyyy_HHmmaa?.string(from: date))! //TODO change format as required.
    }
    
    func  formatSimpleDate(date:Date) -> String?{
        //return DF_TIME_dd_MMM_yy.format(date);
        return (DF_TIME_dd_MMM_yy?.string(from: date))! //TODO change format as required.
    }
    
    func formatDateToString(date:Date) -> String{
        return (DF_TIME_yyyy_MM_dd?.string(from: date))!
    }
    
    func formateDateToMonthString(date:Date) -> String{
        return (DF_MMMM?.string(from: date))!
    }
    
    func formateFullDateStr(date:Date) -> String{
        return (DF_FULL_DAY_DD_MM_YYYY?.string(from:date))!
    }
    
    func parseISO8601Date(dateStr:String) -> Date?{
        if(!dateStr.isEmpty){
            return DF_ISO_8601?.date(from: dateStr)
        }
        return nil
    }
    
    func dateFromStr(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        let convertedStr = dateStr + " 00:00:00 z"
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    
    func dateFromServerStr(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss z"
        let convertedStr = dateStr + " 00:00:00 z"
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    
    func dateForCustomLog(dateStr:String, timeStr:String) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        let convertedStr = dateStr + " " + timeStr
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    
    func formatSimpleBirthDate(date:Date) -> String?{
        return (DF_TIME_dd_MMMM_yyyy?.string(from: date))! //TODO change format as required.
    }
    
    func formatSimpleData(date:Date) -> String?{
        return (DF_dd_MMM_yyyy?.string(from: date))!
    }
    
    func formatDayDateFormat(date:Date) -> String?{
        return (DF_DAY_MMM_dd_yyyy?.string(from: date))!
    }
    
    func julianDay(year:Int, month:Int, day:Int) -> Int{
        let a = (14 - month) / 12;
        let y = year + 4800 - a;
        let m = month + 12 * a - 3;
        return day + (153 * m + 2)/5 + 365*y + y/4 - y/100 + y/400 - 32045;
    }
    
    func julianDay(calendar:NSCalendar) -> Int{
        let date = Date()
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        return julianDay(year: year, month: month, day: day)
    }
    
    func julianToday() -> Int{
        return julianDay(calendar: NSCalendar.current as NSCalendar)
    }
    
    /**
     * Convert Date into easy readable string like, an hour ago, today @, yesterday @ etc
     * @param date Date to be converted
     * @param currentTime Current Time (Time to be compared)
     * @return Easy readable date time string.
     */
    func convertTimeToEasyString(sourceDate:Date) -> String?{
        var easyDateTimeString = ""
        let currentDate = Date()
        let difference = currentDate.offset(from: sourceDate)
        var sameDay = false
        if(difference.hasSuffix(Date.DIFF_HOUR) || difference.hasSuffix(Date.DIFF_MIN) || difference.hasSuffix(Date.DIFF_SEC)){
            sameDay = true
        }
        
        let currentTimeInterval:Double = currentDate.timeIntervalSinceNow
        let messageTimeInterval:Double = Date().timeIntervalSince(sourceDate)
        
        let dayCount = sourceDate.daysBetween(date: currentDate)
        
        if(sameDay && dayCount < 1){
            if (currentTimeInterval - messageTimeInterval < DateTimeUtils.MILLI_SECOND_IN_HOUR) {
                easyDateTimeString = String(format: msgTimeTextTodayAt!, (DF_TIME_hh_mm_aa?.string(from: sourceDate))!)
                
                //easyDateTimeString = "last seen today at \((DF_TIME_hh_mm_aa?.string(from: sourceDate))!)"//String(format: msgTimeTextTodayAt!, (DF_TIME_hh_mm_aa?.string(from: sourceDate))!)
            }
            if ((currentTimeInterval - messageTimeInterval > DateTimeUtils.MILLI_SECOND_IN_HOUR)
                && (currentTimeInterval - messageTimeInterval < DateTimeUtils.MILLI_SECOND_IN_TWO_HOUR)) {
                easyDateTimeString = msgTimeTextAnHourAgo!
            }
            if ((currentTimeInterval - messageTimeInterval > DateTimeUtils.MILLI_SECOND_IN_TWO_HOUR)) {
                easyDateTimeString = String(format: msgTimeTextTodayAt!, (DF_TIME_hh_mm_aa?.string(from: sourceDate))!)
            }
        }else{
            if (currentTimeInterval - messageTimeInterval < DateTimeUtils.MILLI_SECOND_IN_DAY && dayCount < 2) {
                easyDateTimeString = String(format: msgTimeTextYesterdayAt!, (DF_TIME_hh_mm_aa?.string(from: sourceDate))!)
            } else {
                easyDateTimeString = (DF_TIME_yyyy_MM_dd?.string(from: sourceDate))!
            }
        }
        return easyDateTimeString
    }
    
    //MARK:- Time/Date conversion
    func getTimeStampFromSecond(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3600
        
        var timeStamp = ""
        if(hours != 0 && minutes != 0){
            timeStamp = String(format: "%02d h:%02d m", hours, minutes)
        }else if(hours == 0 && minutes != 0){
            timeStamp = String(format: "%02d m",minutes)
        }else if(minutes == 0 && seconds != 0){
            timeStamp = String(format: "%02d s",seconds)
        }else if(seconds == 0){
            timeStamp = String(format: "0 s")
        }
        return timeStamp
    }
    
    func getDateFromUnixFormat(_ dateUnixFormate:Double) -> String {
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, hh:mm a"
        let dateString = dateFormatter.string(from: date)
        return dateString
        
    }
    
    func getDateTimeFromUnixFormat(_ dateUnixFormate:Double) -> String {
        let date = Date(timeIntervalSince1970: dateUnixFormate / 1000.0)
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = DateFormatter.Style.short
        timeFormatter.dateFormat = "hh:mm a"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let timeString = timeFormatter.string(from: date)
        let dateString = dateFormatter.string(from: date)
        let finalString = String(format:"%@ (%@)",timeString,dateString)
        return finalString
    }
    
    func getDateStrFromUnixFormat(_ dateUnixFormate:Double) -> String {
        let date = Date(timeIntervalSince1970: dateUnixFormate / 1000.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateString = dateFormatter.string(from: date)
        let finalString = String(format:"%@",dateString)
        return finalString
    }
    
    func getGMTDateStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func getDeviceTimeZoneFullDateStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func getDeviceTimeZoneDateStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate/1000.0)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func getDeviceTimeZoneTimeStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        
        let date = Date(timeIntervalSince1970: dateUnixFormate/1000.0)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "hh:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func isDateEqual(date1:String, date2:String){
        
    }
    
    func getDateFromTimeInterval(timeInterval:TimeInterval) -> Date{
        return Date(timeIntervalSince1970: timeInterval / 1000.0)
    }
    
    func getDateFromStringTimeInterval(timeInterval:String) -> Date?{
        let dateTimeMilliSecondString:String? = BaseApp.sharedInstance.removeOptionalWordFromString(timeInterval)
        
        if(dateTimeMilliSecondString != nil){
            let messageDate:Date? = self.getDateFromTimeInterval(timeInterval: Double(dateTimeMilliSecondString!)!)
            return messageDate
        }
        
        return nil
    }
    
    func getDateStringFromStringTimeInterval(timeInterval:String) -> String?{
        var dateTimeMilliSecondString:String? = BaseApp.sharedInstance.removeOptionalWordFromString(timeInterval)
        
        if(dateTimeMilliSecondString != nil){
            let messageDate:Date? = self.getDateFromTimeInterval(timeInterval: Double(dateTimeMilliSecondString!)!)
            if(messageDate != nil){
                dateTimeMilliSecondString = DateTimeUtils.sharedInstance.convertTimeToEasyString(sourceDate: messageDate!)
            }
        }
        
        return dateTimeMilliSecondString ?? nil
    }
    
    func getCurrentMonthName() -> String{
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let nameOfMonth = dateFormatter.string(from: now)
        return nameOfMonth
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60)
    }
}


