//
//  AppDelegate.swift
//  Gupa Network
//
//  Created by mac on 4/27/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//Gupa Network Group

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import CoreData
import Firebase
import FirebaseMessaging
import UserNotifications
import JGProgressHUD
import GoogleMaps
import Reachability

import AssetsLibrary
import QuartzCore
import AVKit
import AVFoundation
import Photos

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var arrOfDestinatioStationCode: NSMutableArray = NSMutableArray()
    var arrOfDestinatioStation: NSMutableArray = NSMutableArray()
    var reversed: NSArray = NSArray()
    var isNetworkBecomeAvailableFirstTime   = false

    var transportRatingData : AnyObject!
    var reachability : Reachability?

    var window: UIWindow?
    var strURL: String = String()
    var strLink: String = String()
    var strUserID: String = String()

    var strCheckForHotelOrFlight: String = String()
    var centerContainer = SWRevealViewController ()
    var driverDict : NSMutableDictionary = [:]

    var lat : Double = 0.0
    var long : Double = 0.0
    var firstLoc : Bool = false
    var isFromLeftMenu : Bool = false
    

    var loading : JGProgressHUD!
    
    //** Notification Dictionary
    var dictAPNs                  :  [String: AnyObject]  = [:]
    var isRemoteNotification      : Bool   = false

    var locationManager : CLLocationManager = CLLocationManager()
    var isEditProfile : Bool = false

    var databaseManager = DatabaseManager.sharedDBInterface // Local database

    var dictTaxiPassenger  : NSMutableDictionary = [:]
    
    var centerNav:UINavigationController?

    static var deviceTokenValue = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Thread.sleep(forTimeInterval: 0.00000002)
        
        _ = CurrentLocationManager.sharedInstance

        if let isLogin = AppTheme.getIsLogin() as? Bool {
            if isLogin == true {
                _ = getAirportDetail()
            }
        }
        
        if(UserDefaults.standard.bool(forKey: "HasLaunchedOnce"))
        {
            UserDefaults.standard.set(false, forKey: "HasLaunchedOnce")
            UserDefaults.standard.synchronize()
        }
        else
        {
            // This is the first launch ever
            UserDefaults.standard.set(true, forKey: "HasLaunchedOnce")
            UserDefaults.standard.synchronize()
        }
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let centerViewController = mainStoryboard.instantiateViewController(withIdentifier: "SplashRotationVC") as! SplashRotationVC
        
        let leftViewController = mainStoryboard.instantiateViewController(withIdentifier: "LeftDrawerViewController") as! LeftDrawerViewController
        centerNav = UINavigationController(rootViewController: centerViewController)
        
        centerContainer.rearViewController = leftViewController
        centerContainer.frontViewController = centerNav
        
        self.window?.rootViewController = centerContainer
       
      
        locationManager.startUpdatingLocation()
        IQKeyboardManager.sharedManager().enable = true
        
        application.registerForRemoteNotifications()
        registerForPushNotifications(application)
        
        if let device_id = UserDefaults.standard.value(forKey: "dt") {
            print(device_id)
        }
        
        //** Manage Notification
        let remoteNotif = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [String: AnyObject]
        if remoteNotif != nil {
            dictAPNs = remoteNotif!
            isRemoteNotification = true
        }
        
        //** Start reachability without a hostname intially
        startHost()

        return true
    }
    
    func registerForPushNotifications(_ application: UIApplication) {
        
        //** Register for APNS
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print(token)
        
        //** Remove special character and get valid device token string
        let characterSet: CharacterSet = CharacterSet(charactersIn: "<>")
        let deviceTokenString: String  = (token as NSString).trimmingCharacters(in: characterSet).replacingOccurrences(of: " ", with: "" ) as String

        print("device Token == \(deviceTokenString)")
        AppTheme.deviceToken = deviceTokenString
        AppDelegate.deviceTokenValue = deviceTokenString
        UserDefaults.standard.set(deviceTokenString, forKey: "dt")
        UserDefaults.standard.synchronize()
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("Fail to register for remote notifications. Error:\(error)")
        //  UserDefaults.standard.set("", forKey: kAPI_DeviceId)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        //** Get Userinfo dictionary:
        let dictNotification = userInfo as! [String: AnyObject]
        
        getRemoteNotifications(dictNotification)
        
        print("Dict Notification : \(dictNotification)")
        
    }
    
    func getRemoteNotifications(_ dictApns: [String: AnyObject]) {
        
        if let isLogin = AppTheme.getIsLogin() as? Bool {
            
            if isLogin == true {
                
                if let alert: NSDictionary = dictApns["aps"]!["alert"] as? NSDictionary {
                        
                        // Notification for passenger
                         if let notification_id = alert.value(forKey: "notification_id") as? Int  {
                            
                           print("notification_id ==\(notification_id)")

                            if notification_id == 1 {
                                
                                if let body = alert["body"] as? NSDictionary {
                                    
                                    let strTitle     = alert.value(forKey: "title") as? String

                                    let action_type  = body.value(forKey: "action_type") as? String
                                    let messsage     = body.value(forKey: "message") as? String
                                    let driverDetail = body.value(forKey: "driver_details") as? NSDictionary
                                   // let dictPickUp   = body.value(forKey: "pickup") as? NSDictionary
                                  //  let dictDropOff  = body.value(forKey: "drop") as? NSDictionary
                                    let strAmount    = body.value(forKey: "amount") as? String
                                    let strPaymentType = body.value(forKey: "payment_type") as? String
                                    let strBookingId  = body.value(forKey: "booking_id") as? String
                                    let strDistance  = body.value(forKey: "distance") as! String
                                    let strDateNTime  = body.value(forKey: "book_create_date_time") as! String
                                   
                                    let arrDropOff  = body.value(forKey: "drop") as? NSArray
                                    let dictDropOff = arrDropOff![0] as! NSDictionary
                                    
                                    let arrpickup  = body.value(forKey: "pickup") as? NSArray
                                    let dictPickUp = arrpickup![0] as! NSDictionary

                                    if action_type == "1" {
                                        
                                    let alertVC = UIAlertController(title: strTitle, message: "Your booking is confirmed by driver! Driver will arrive soon.", preferredStyle: .alert)
                                        
                                        let actionOK = UIAlertAction(title: "Ok", style: .default){ __ in
                                            
                                            let mainStoryboard : UIStoryboard = UIStoryboard(name: TaxiAppConstant.taxiVCStoryboard, bundle: nil)
                                            let tripDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "TripDetailsVC") as! TripDetailsVC
                                            tripDetailsVC.tripDetailDict = driverDetail!
                                            tripDetailsVC.str_SelectedAmount = strAmount!
                                            tripDetailsVC.str_SelectCardType = strPaymentType!
                                            tripDetailsVC.str_SelectedBookinID = strBookingId!
                                            tripDetailsVC.str_Distance = "\(String(describing: strDistance))"
                                            tripDetailsVC.pickUpDict = dictPickUp
                                            tripDetailsVC.dropOffDict = dictDropOff
                                            tripDetailsVC.str_BookingDateAndTime = strDateNTime
                                            self.centerNav?.pushViewController(tripDetailsVC, animated: false)

                                         }
                                         alertVC.addAction(actionOK)
                                         self.window?.rootViewController!.present(alertVC, animated: true, completion: nil)
                                        
                                    }  else if action_type == "2" {
                                        
                                        let alertVC = UIAlertController(title: "Booking Rejected!", message: "Your booking is rejected by driver! Please book for another driver.", preferredStyle: .alert)
                                        let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                                        
                                            let mainStoryboard : UIStoryboard = UIStoryboard(name: TaxiAppConstant.taxiVCStoryboard, bundle: nil)
                                            let tripDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "PickupAndDropLocationVC") as! PickupAndDropLocationVC
                                            self.centerNav?.pushViewController(tripDetailsVC, animated: false)

                                        }
                                        alertVC.addAction(actionOk)
                                        self.window?.rootViewController!.present(alertVC, animated: true, completion: nil)
                                        
                                    } else if action_type == "4" {
                                        
                                        let alertVC = UIAlertController(title: messsage, message: "Driver has arrived ! Now you can start your trip.", preferredStyle: .alert)
                                        
                                        let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                                            
                                            let mainStoryboard : UIStoryboard = UIStoryboard(name: TaxiAppConstant.taxiVCStoryboard, bundle: nil)
                                            let tripDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "TripDetailsVC") as! TripDetailsVC
                                            
                                            if !(self.window?.rootViewController is TripDetailsVC){
                                                tripDetailsVC.tripDetailDict = driverDetail!
                                                tripDetailsVC.str_SelectedAmount = strAmount!
                                                tripDetailsVC.str_SelectCardType = strPaymentType!
                                                tripDetailsVC.str_SelectedBookinID = strBookingId!
                                                tripDetailsVC.str_Distance = "\(String(describing: strDistance))"
                                                tripDetailsVC.pickUpDict = dictPickUp
                                                tripDetailsVC.dropOffDict = dictDropOff
                                                tripDetailsVC.str_BookingDateAndTime = strDateNTime
                                                self.centerNav?.pushViewController(tripDetailsVC, animated: false)
                                            }
                                           
                                            
                                        }
                                        alertVC.addAction(actionOk)
                                        self.window?.rootViewController!.present(alertVC, animated: true, completion: nil)
                                        
                                    }  else if action_type == "5" {
                                        
                                        let alertVC = UIAlertController(title: messsage, message: "Your trip has started.", preferredStyle: .alert)
                                        let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                                            
                                            let mainStoryboard : UIStoryboard = UIStoryboard(name: TaxiAppConstant.taxiVCStoryboard, bundle: nil)
                                            let tripDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "TripDetailsVC") as! TripDetailsVC
                                            
                                            if !(self.window?.rootViewController is TripDetailsVC){
                                                
                                            tripDetailsVC.tripDetailDict = driverDetail!
                                            tripDetailsVC.str_SelectedAmount = strAmount!
                                            tripDetailsVC.str_SelectCardType = strPaymentType!
                                            tripDetailsVC.str_SelectedBookinID = strBookingId!
                                            tripDetailsVC.str_Distance = "\(String(describing: strDistance))"
                                            tripDetailsVC.pickUpDict = dictPickUp
                                            tripDetailsVC.dropOffDict = dictDropOff
                                            tripDetailsVC.str_BookingDateAndTime = strDateNTime
                                            tripDetailsVC.isTripStarted = true
                                            self.centerNav?.pushViewController(tripDetailsVC, animated: false)
                                            }
                                            
                                        }
                                        alertVC.addAction(actionOk)
                                        self.window?.rootViewController!.present(alertVC, animated: true, completion: nil)
                                        
                                    } else if action_type == "6" {
                                        
                                        let alertVC = UIAlertController(title: messsage, message: "", preferredStyle: .alert)
                                        let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                                            
                                            let mainStoryboard : UIStoryboard = UIStoryboard(name: TaxiAppConstant.taxiVCStoryboard, bundle: nil)
                                            let tripDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "CompletedTripVC") as! CompletedTripVC
                                            tripDetailsVC.driverDict = driverDetail!
                                            tripDetailsVC.strBookingId = strBookingId!
                                            tripDetailsVC.pickUpDict = dictPickUp
                                            tripDetailsVC.dropOffDict = dictDropOff
                                            tripDetailsVC.strTripDate = strDateNTime
                                            self.centerNav?.pushViewController(tripDetailsVC, animated: false)
                                        }
                                        alertVC.addAction(actionOk)
                                        self.window?.rootViewController!.present(alertVC, animated: true, completion: nil)
                                    }
                              }
                            } else if notification_id == 4 {
                                
                                if let body = alert["body"] as? NSDictionary {
                                    
                                    let strTitle     = alert.value(forKey: "title") as? String
                                    
                                    let action_type  = body.value(forKey: "action_type") as? String
                                  //  let dictPickUp   = body.value(forKey: "pickup") as? NSDictionary
                                  //  let dictDropOff  = body.value(forKey: "drop") as? NSDictionary
                                    
                                    let arrDropOff  = body.value(forKey: "drop") as? NSArray
                                    let dictDropOff = arrDropOff![0] as! NSDictionary
                                    
                                    let arrpickup  = body.value(forKey: "pickup") as? NSArray
                                    let dictPickUp = arrpickup![0] as! NSDictionary
                                    
                                    let strBookingId  = body.value(forKey: "booking_id") as? String
                                    let strDateNTime  = body.value(forKey: "book_create_date_time") as! String
                                    let driverDetail = body.value(forKey: "driver_details") as? NSDictionary

                                if action_type == "1" {
                                    
                                    let alertVC = UIAlertController(title: strTitle, message: "Driver has confrimed your book later request.", preferredStyle: .alert)
                                    
                                    let actionOK = UIAlertAction(title: "Ok", style: .default){ __ in
                                        
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: TaxiAppConstant.taxiVCStoryboard, bundle: nil)
                                        let tripDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "ConfirmationTaxiVC") as! ConfirmationTaxiVC
                                        tripDetailsVC.str_SelectedBookinID = strBookingId!
                                        tripDetailsVC.pickUpDict = dictPickUp
                                        tripDetailsVC.dropOffDict = dictDropOff
                                        tripDetailsVC.driverDict = driverDetail!
                                        tripDetailsVC.str_BookingDateAndTime = strDateNTime
                                        self.centerNav?.pushViewController(tripDetailsVC, animated: false)
                                    }
                                    alertVC.addAction(actionOK)
                                    self.window?.rootViewController!.present(alertVC, animated: true, completion: nil)
                                }
                              }
                            }
                            
                        } else if let body = alert.value(forKey: "body") as? String {
                            let alertVC = UIAlertController(title: "Notification", message: body, preferredStyle: .alert)
                            let actionCancel = UIAlertAction(title: "Ok", style: .default){ __ in
                            }
                            alertVC .addAction(actionCancel)
                            self.window?.rootViewController!.present(alertVC, animated: true, completion: nil)
                    }
                }
            }
        }
        
    }
    
    func loadViewController(_ strStoryboardIdentifier : String){
        
        let rootViewController = self.window!.rootViewController as? UINavigationController
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = mainStoryboard.instantiateViewController(withIdentifier: strStoryboardIdentifier)
        rootViewController!.pushViewController(rootController, animated: true)
    }
            
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        // Stops monitoring network reachability status changes
        BaseApp.sharedInstance.stopMonitoring()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        // Starts monitoring network reachability status changes
        BaseApp.sharedInstance.startMonitoring()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
        BaseApp.sharedInstance.stopMonitoring()

    }
    /*
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }*/
    
    //****************************************************
    // MARK: - Core Data stack
    //****************************************************

    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "jf.Gupa_Network" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "Gupa_Network", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
       // var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    //****************************************************
    // MARK: - Core Data Saving support
    //****************************************************

    func saveContext () {
        
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    //****************************************************
    // MARK: - Network Reachability Methods
    //****************************************************
    
    /**
     * Start Network tracking
     */
    
    func startHost() {
        stopNotifier()
        setupReachability(nil, useClosures: true)
        startNotifier()
    }
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        
        let reachability: Reachability?
        if let hostName = hostName {
            reachability = Reachability(hostname: hostName)
        } else {
            reachability = Reachability()
        }
        self.reachability = reachability
        
       NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(_:)), name: .reachabilityChanged, object: reachability)

    }
    
    func updateLabelColourWhenNotReachable(_ reachability: Reachability) {
        print("\(reachability.description) - \(reachability.connection)")
        }
    
    
    func startNotifier() {
        print("--- start notifier")
        do {
            try reachability?.startNotifier()
        } catch {
//            networkStatus.textColor = .red
//            networkStatus.stringValue = "Unable to start\nnotifier"
            return
        }
    }
    
    func stopNotifier() {
        
        print("--- stop notifier")
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        reachability = nil
    }
    
   
    
    @objc func reachabilityChanged(_ note: Notification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.connection != .none {
            
            isNetworkAvailable = reachability.isReachable
            
            if isNetworkAvailable {
                
              //  databaseManager.deleteRecordsFrom(tableName: DBConstants.DBTableName.TABLE_POST)
               // databaseManager.deleteRecordsFrom(tableName: DBConstants.DBTableName.TABLE_REVIEW)
                
                isNetworkBecomeAvailableFirstTime = true
                
                if let isLogin = AppTheme.getIsLogin() as? Bool {
                    if isLogin == true {
                        
                        let offLinePostData = databaseManager.getPost()
                        let reviewData = databaseManager.getReviews()

                        var dictTemp : NSDictionary!
                        var dictReview : NSDictionary!

                        if (offLinePostData.count > 0) {
                            
                            for i in 0 ..< offLinePostData.count {
                                
                                let arrOfflinePost = offLinePostData[i]
                                
                                let strAppUrl = arrOfflinePost.appUrl
                                let dictParam = arrOfflinePost.paramData
                                print("11111 === \(arrOfflinePost)")

                                dictTemp = convertToDictionary(text: dictParam)! as NSDictionary
                                print("dictTemp === \(String(describing: dictTemp))")
                                
                                if strAppUrl == "WallPost" {
                                    
                                    let strPostId = arrOfflinePost.postId
                                    
                                    let strPost = dictTemp.value(forKey: "posts")
                                    let strEnableComment = dictTemp.value(forKey: "enable_comment")
                                    let intRating = dictTemp.value(forKey: "rating")
                                    
                                    sendWallPost(postID: strPostId ,txtcomment: strPost! as! String, rating: intRating! as! NSInteger, strCommentEnable: strEnableComment! as! String)
                                    
                                } else if strAppUrl == "addPost_image" {
                                    
                                    let postId = arrOfflinePost.postId
                                    let strUrlImage = arrOfflinePost.imageFilePath
                                    let fileType = arrOfflinePost.imageVideoType
                                    
                                    let strPost = dictTemp.value(forKey: "description")
                                    let intRating = dictTemp.value(forKey: "rating")
                                    var typeStr = ""
                                    
                                    var imageData = NSData()
                                    
                                   print("dictTemp === \(String(describing: strPost))")
                                    
                                    if fileType == "Video" {
                                        
                                        let fileName = BaseApp.sharedInstance.getLocalMediaFileFolderPath(imageName: strUrlImage)?.path
                                        imageData   = BaseApp.sharedInstance.getVideoData(filepath: fileName!)
                                        typeStr     = "video"
                                        
                                        let rat: String = String(describing: intRating)

                                        if (imageData as Data).count > 42603722 {
                                            
                                            let directoryPath = BaseApp.sharedInstance.getDefaultFolderPath()

                                            let filepath = directoryPath?.appendingPathComponent(fileName!).path
                                            let urlOfVideo = NSURL.fileURL(withPath: filepath!)
                                            
                                            let VideoFilePath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("mergeVideo\(arc4random()%1000)d").appendingPathExtension("mp4").absoluteString
                                            if FileManager.default.fileExists(atPath: VideoFilePath) {
                                                do {
                                                    try FileManager.default.removeItem(atPath: VideoFilePath)
                                                } catch {
                                                }
                                            }
                                            
                                            let savePathUrl =  URL(string: VideoFilePath)!
                                            
                                            let sourceAsset = AVURLAsset(url: urlOfVideo, options: nil)
                                            let assetExport: AVAssetExportSession = AVAssetExportSession(asset: sourceAsset, presetName: AVAssetExportPresetLowQuality)!
                                            assetExport.outputFileType = AVFileType.mp4
                                            assetExport.outputURL = savePathUrl
                                            
                                            assetExport.exportAsynchronously { () -> Void in
                                                
                                                switch assetExport.status {
                                                case AVAssetExportSessionStatus.completed:
                                                    DispatchQueue.main.async(execute: {
                                                        
                                                    })
                                                case AVAssetExportSessionStatus.failed:
                                                    self.loading.dismiss()
                                                    print("failed \(String(describing: assetExport.error))")
                                                case AVAssetExportSessionStatus.cancelled:
                                                    self.loading.dismiss()
                                                    print("cancelled \(String(describing: assetExport.error))")
                                                default:
                                                    self.loading.dismiss()
                                                    print("complete")
                                                    
                                                self.sendWallPost_image(postID: Int("\(postId)")! , txtcomment: "\(strPost!)", rating: rat, timeString: 0 , imageData_data: imageData as Data, typeImage_Video: typeStr)
                                                    
                                                }
                                            }
                                        } else {
                                            
                                            sendWallPost_image(postID: Int("\(postId)")! , txtcomment: "\(strPost!)", rating: rat, timeString: 0 , imageData_data: imageData as Data, typeImage_Video: typeStr)

                                        }
                                        
                                    } else {
                                        let filePath = BaseApp.sharedInstance.getLocalMediaFileFolderPath(imageName: strUrlImage)?.path
                                        let imgAdd = BaseApp.sharedInstance.getImage(filepath:  filePath!)
                                        imageData  = UIImageJPEGRepresentation(imgAdd, 1.0)! as NSData
                                        typeStr = "imagePost"
                                        
                                        let rat: String = String(describing: intRating)

                                        sendWallPost_image(postID: Int("\(postId)")! , txtcomment: "\(strPost!)", rating: rat, timeString: 0 , imageData_data: imageData as Data, typeImage_Video: typeStr)

                                    }
                                }
                                
                            }
                        }
                        
                          if (reviewData.count > 0) {
                            
                            for i in 0 ..< reviewData.count {
                                
                                let arrReview = reviewData[i]
                                
                                let strAppUrl = arrReview.appUrl
                                let dictParamReview = arrReview.paramData
                                print("11111 === \(arrReview)")

                                dictReview = convertToDictionary(text: dictParamReview)! as NSDictionary
                                print("dictTemp === \(String(describing: dictReview))")
                                
                                if strAppUrl == "Review" {
                                    
                                    let strPostId = arrReview.postId
                                    
                                    let strCode = dictReview.value(forKey: "iata_code")
                                    let strServiceCode = dictReview.value(forKey: "service")
                                    let strReview = dictReview.value(forKey: "review")
                                    let intRating = dictReview.value(forKey: "rating")

                                    sendReviewOnServerFromLocalDataBase(reviewID: strPostId, code: strCode as! String, strService: strServiceCode as! String, strReview: strReview as! String, rating: intRating as! String)
                                }
                            }
                        }

                    }
                    NotificationCenter.default.post(name: Notification.Name(rawValue: notificationNetworkBecomeAvailableFirstTime), object: self, userInfo:nil)

                }
            }
            
        }  else {
            isNetworkAvailable = reachability.isReachable
            NotificationCenter.default.post(name: Notification.Name(rawValue: notificationNetworkBecomeAvailableFirstTime), object: self, userInfo:nil)
        }

    }
    
    //****************************************************
    // MARK: - API Methods
    //****************************************************
    
    func getAirportDetail() -> NSArray {
      
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Table_travel", in: self.managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        var result : NSArray!
        
        do {
            result = try self.managedObjectContext.fetch(fetchRequest) as NSArray

            if (result.count > 0) {
                
                for i in 0 ..< result.count {
                    
                    let airportDetail = result[i] as! NSManagedObject
                    let strCityNameCode = (airportDetail.value(forKey: "airport_name") as! String) + " " + (airportDetail.value(forKey: "airport_code") as! String)
                    self.arrOfDestinatioStation.add(strCityNameCode as String)
                    self.arrOfDestinatioStationCode.add(airportDetail.value(forKey: "airport_code") as! String)
                }
              
                //sorted array for destination stations
                self.reversed = (self.arrOfDestinatioStation as NSArray as! [String]).sorted(by: { (s1: String, s2: String) -> Bool in
                    return s1 < s2
                }) as NSArray
            }
        } catch {

            let fetchError = error as NSError
            print(fetchError)
        }
        return self.reversed 
    }
    
    func sendWallPost(postID : Int ,txtcomment : String , rating: NSInteger , strCommentEnable : String) {
        
        if let str_user_id : String = AppTheme.getLoginDetails().object(forKey: "id")! as? String {
            
            
                let paraDict = NSMutableDictionary()
                paraDict.setValue(str_user_id, forKey:"user_id")
                paraDict.setValue("", forKey:"notify_user")
                paraDict.setValue(txtcomment, forKey:"posts")
                paraDict.setValue(rating, forKey:"rating")
                paraDict.setValue(strCommentEnable, forKey:"enable_comment")
                
                AppTheme().callPostService(String(format: "%@wallPost", AppUrl), param: paraDict) {(result, data) -> Void in
                    
                    if result == "success"  {
                        let respDict = data as! NSDictionary
                        print("111111 == \(respDict)")
                        self.databaseManager.deleteItemNameFrom(tableName: DBConstants.DBTableName.TABLE_POST,id: postID)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "LocalDatabaseData"), object: self, userInfo:nil)

                    } else if result == "error" {
                    }
                }
        }
    }
    
    func sendWallPost_image(postID : Int ,txtcomment : String , rating: String , timeString : NSInteger , imageData_data : Data, typeImage_Video : String) {
        
            if (!isNetworkAvailable) {
                return
            }
        
            let parameters : [String: AnyObject] = ["user_id" : AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject,
                                                    "description" : txtcomment as AnyObject,
                                                    "title" : "title" as AnyObject,
                                                    "file_type" : "image" as AnyObject,
                                                    "rating" : rating as AnyObject,
                                                    "rating_timer" : "0.0" as AnyObject]
            
        AppTheme().callPostWithMultipartServices(String(format: "%@add_blogs", AppUrl), param: parameters, imageData: imageData_data as Data , file_type: typeImage_Video) { (result, data) -> Void in
                
                if(result == "success") {
                    
                    print("Deleted record")

                    self.databaseManager.deleteItemNameFrom(tableName: DBConstants.DBTableName.TABLE_POST,id: postID)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "LocalDatabaseData"), object: self, userInfo:nil)
                    
                } else {
                  //  _ =  self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error!", showInView: self.view)
                 //   self.loading.dismiss()
            }
      }
    }
    
    
    func sendReviewOnServerFromLocalDataBase(reviewID : Int ,code : String , strService : String , strReview : String , rating : String) {
        
        if let str_user_id : String = AppTheme.getLoginDetails().object(forKey: "id")! as? String {

            let paraDict = NSMutableDictionary()
            paraDict.setValue(str_user_id, forKey:"user_id")
            paraDict.setValue(code, forKey:"iata_code")
            paraDict.setValue(strService, forKey:"service")
            paraDict.setValue(strReview, forKey:"review")
            paraDict.setValue(rating, forKey:"rating")
            
            AppTheme().callPostService(String(format: "%@airport_services_review?", AppUrl), param: paraDict) {(result, data) -> Void in
                
                if result == "success"
                {
                    let respDict = data as! NSDictionary
                    print("111111 == \(respDict)")
                    
                    self.databaseManager.deleteItemNameFrom(tableName: DBConstants.DBTableName.TABLE_REVIEW,id: reviewID)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "ReviewDatabaseData"), object: self, userInfo:nil)


                } else{
                    //self.loading.dismiss()
                }
            }
         }
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
   
}

