//
//  CurrentLocationManager.swift
//  Fantopias
//
//  Created by Neuron Mac on 11/01/16.
//  Copyright © 2016 Neuron_Mac_Mini 7. All rights reserved.
//

import CoreLocation

protocol LocationManagerProtocol {
    func didLocationUpdate (_ coordinate : CLLocationCoordinate2D)
}

class CurrentLocationManager : NSObject, CLLocationManagerDelegate {
    
    //** MARK: Shared Instance
    static let sharedInstance : CurrentLocationManager = CurrentLocationManager()

    var locationManager   : CLLocationManager!
    var latitude          : Double!
    var longitude         : Double!
    var currentCoordinate : CLLocationCoordinate2D!
    
    var delegate          : LocationManagerProtocol!
    
    /**
     * Initialize LocationManager Update
     **/
    
    override init() {
        super.init()
        
        locationManager                 = CLLocationManager()
        locationManager.delegate        = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    /**
     * Update location delegate
     */
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        CurrentLocationManager.sharedInstance.currentCoordinate = manager.location!.coordinate
        
        CurrentLocationManager.sharedInstance.latitude  = CurrentLocationManager.sharedInstance.currentCoordinate.latitude
        CurrentLocationManager.sharedInstance.longitude = CurrentLocationManager.sharedInstance.currentCoordinate.longitude

        print("locations = \(CurrentLocationManager.sharedInstance.currentCoordinate.latitude) \(CurrentLocationManager.sharedInstance.currentCoordinate.longitude)")
        
        CurrentLocationManager.sharedInstance.locationManager.stopUpdatingLocation()
        
       // delegate.didLocationUpdate(CurrentLocationManager.sharedInstance.currentCoordinate)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        CurrentLocationManager.sharedInstance.locationManager.stopUpdatingLocation()
        print(error)
    }
}
