//
//  CarTypeCell.swift
//  Gupa Network
//
//  Created by Apple on 26/02/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit

class CarTypeCell: UICollectionViewCell {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet weak var lblCarName     : UILabel!
    @IBOutlet weak var lblCarPrice    : UILabel!
    @IBOutlet weak var imgCar         : UIButton!
    @IBOutlet weak var viewBg    : UIView!

}
