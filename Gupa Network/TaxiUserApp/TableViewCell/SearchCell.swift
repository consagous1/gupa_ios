//
//  SearchCell.swift
//  Gupa Network
//
//  Created by Apple on 22/02/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet weak var lblAddress     : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
