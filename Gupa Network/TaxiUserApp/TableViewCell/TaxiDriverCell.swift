//
//  TaxiDriverCell.swift
//  Gupa Network
//
//  Created by Apple on 17/01/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit

class TaxiDriverCell: UITableViewCell {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet weak var lblDriverName  : UILabel!
    @IBOutlet weak var lblRating      : UILabel!
    @IBOutlet weak var imgRating      : UIImageView!
    @IBOutlet weak var imgProfile     : UIImageView!
    @IBOutlet weak var lblTripTime    : UILabel!
    @IBOutlet weak var lblStatus      : UILabel!
    @IBOutlet weak var lblAddress     : UILabel!
    @IBOutlet weak var constraintHeigthName     : NSLayoutConstraint!
    @IBOutlet weak var constraintWidthImage     : NSLayoutConstraint!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
