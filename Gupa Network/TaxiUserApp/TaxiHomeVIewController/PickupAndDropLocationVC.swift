//
//  ChooseCabVC.swift
//  Gupa Network
//
//  Created by Priyanka on 08/01/18.
//  Copyright © 2018 Consagous. All rights reserved.
//

import UIKit
import MapKit
import JGProgressHUD
import MapKit
import CoreLocation

class PickupAndDropLocationVC: UIViewController , MKMapViewDelegate , CLLocationManagerDelegate  {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var txtPickLocation         : UITextField!
    @IBOutlet var txtDropLocation         : UITextField!
    @IBOutlet var mapView                 : MKMapView!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
     var locationManager: CLLocationManager = CLLocationManager()
     fileprivate var annotation: MKAnnotation!

      var strPickUpId = ""
      var strDropoffId = ""
    
      var strSelectedPickupLat = ""
      var strSelectetPickUpLog = ""

      var strSelectedDropOffLat = ""
      var strSelectetDropOffLog = ""
  
    //****************************************************
    // MARK: - Life Cycle Methods
    //****************************************************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.centerNav = self.navigationController
        setNavigationController()
        
        txtPickLocation.addTarget(self, action: #selector(pickUpLocation_Action), for: UIControlEvents.touchDown)
        txtDropLocation.addTarget(self, action: #selector(dropOffLocation_Action), for: UIControlEvents.touchDown)
        
        txtPickLocation.text = ""
        txtDropLocation.text =  ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (CLLocationManager.locationServicesEnabled()) {
            if  locationManager == nil {
                locationManager = CLLocationManager()
            }
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }

    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func btnDone_Action(_ sender: UIButton) {

        if strPickUpId.isEmpty {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter Pick Up Address.", showInView: self.view)
            return
            
        } else if strDropoffId.isEmpty {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please Enter DropOff Address.", showInView: self.view)
            return
        }
        
        let chooseCabVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseCabVC") as! ChooseCabVC
        chooseCabVC.pickUpId  =  strPickUpId
        chooseCabVC.dropOffId =  strDropoffId
        appDelegate.dictTaxiPassenger = ["kAPI_PickUpId" : strPickUpId ,"kAPI_DropOffId" : strDropoffId ,"kAPI_PickupAddress" : txtPickLocation.text! ,"kAPI_dropOffAddress" : txtDropLocation.text! , "kAPI_PickUpLat" : strSelectedPickupLat , "kAPI_PickupLog" : strSelectetPickUpLog , "kAPI_DropOffLat" : strSelectedDropOffLat  , "kAPI_DropOffLog" : strSelectetDropOffLog]
        print(appDelegate.dictTaxiPassenger)
        self.navigationController?.pushViewController(chooseCabVC, animated: true)
    }
    
    @IBAction func btnBack_Action(_: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //****************************************************
    // MARK: - CLLocationManagerDelegate Methods
    //****************************************************
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.mapView.setRegion(region, animated: true)
        
        if self.mapView.annotations.count != 0 {
            annotation = self.mapView.annotations[0]
            self.mapView.removeAnnotation(annotation)
        }
        
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = location!.coordinate
        pointAnnotation.title = "Current location"
        
        if location?.coordinate.latitude != nil ||  location?.coordinate.longitude != nil {
        
           CurrentLocationManager.sharedInstance.currentCoordinate.latitude = location!.coordinate.latitude
          CurrentLocationManager.sharedInstance.currentCoordinate.longitude = location!.coordinate.longitude
         }
        
      // print("111 === \( CurrentLocationManager.sharedInstance.currentCoordinate.latitude)")
     //   print("222 === \(CurrentLocationManager.sharedInstance.currentCoordinate.longitude)")
          print(location!.coordinate.latitude)
          print(location!.coordinate.longitude)
        
        locationManager.stopUpdatingLocation()
        mapView.addAnnotation(pointAnnotation)
    }
    
    @objc func pickUpLocation_Action(textField: UITextField) {
        
        let searchTaxiLocationVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchTaxiLocationVC") as! SearchTaxiLocationVC
        searchTaxiLocationVC.isFromPickUp = true
        searchTaxiLocationVC.onSelect = {[weak self] strId , strAddress , strLatitude , strLogitude  in
            
            self?.txtDropLocation.text = ""
            self?.strDropoffId = ""
            self?.strSelectedDropOffLat = ""
            self?.strSelectetDropOffLog = ""
            
            self?.txtPickLocation.text = strAddress
            self?.strPickUpId = strId
            self?.strSelectedPickupLat = strLatitude
            self?.strSelectetPickUpLog = strLogitude
        }
        self.navigationController?.pushViewController(searchTaxiLocationVC, animated: true)
    }
    
    @objc func dropOffLocation_Action(textField: UITextField) {
        
        if strPickUpId.isEmpty  {
            txtDropLocation.resignFirstResponder()
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select Pickup Address first.", showInView: self.view)
            return
        }
        let searchTaxiLocationVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchTaxiLocationVC") as! SearchTaxiLocationVC
        searchTaxiLocationVC.isFromPickUp = false
        searchTaxiLocationVC.onSelect = {[weak self] strId , strAddress , strLatitude , strLogitude  in
            
                    self?.txtDropLocation.text = strAddress
                    self?.strDropoffId = strId
                    self?.strSelectedDropOffLat = strLatitude
                    self?.strSelectetDropOffLog = strLogitude
        }
      
        searchTaxiLocationVC.pickUpLocationID = strPickUpId
        self.navigationController?.pushViewController(searchTaxiLocationVC, animated: true)
    }
}
