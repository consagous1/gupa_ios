//
//  BookLaterVC.swift
//  Gupa Network
//
//  Created by Apple on 16/02/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit
import MapKit
import JGProgressHUD

class BookLaterVC: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var btnPickUpLocation: UIButton!
    @IBOutlet var btnBookingDate: UIButton!
    @IBOutlet var btnDropOffLocation: UIButton!
    @IBOutlet var btnSelectCarType: UIButton!
    @IBOutlet var pickerDate: UIDatePicker!
    @IBOutlet var pickerCarType: UIPickerView!
    
    @IBOutlet var viewContainer_heigth   : NSLayoutConstraint!
    @IBOutlet var viewContainerPicker_heigth   : NSLayoutConstraint!
    
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var arrCabDetail :  NSMutableArray = NSMutableArray()
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var dictAdreessData : NSDictionary = NSDictionary()
    var pickUplocationID = ""
    var dropOffID = ""
    var selectedCarType = ""
    var selectedCarId = ""
    var mainDate : Date?
    var intselectedValue = 0
    
    var selectedDate = ""
    var arrCarType = [String]()
    var arrCarName = [String]()
    
    var dateDifference = Date()
    
    //****************************************************
    // MARK: - Life Cycle Methods
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationController()
        appDelegate.centerNav = self.navigationController
        
        btnPickUpLocation.titleLabel?.numberOfLines  = 2
        btnDropOffLocation.titleLabel?.numberOfLines = 2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func show(){
        UIView.animate(withDuration: 0.3,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.viewContainer_heigth.constant = 227
                        self.view.layoutIfNeeded()
        }, completion: { (finished) -> Void in
        })
    }
    
    func hide(){
        UIView.animate(withDuration: 0.3,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: { () -> Void in
                        self.viewContainer_heigth.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: { (finished) -> Void in
        })
    }
    
    func showPicker(){
        UIView.animate(withDuration: 0.3,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.viewContainerPicker_heigth.constant = 238
                        self.view.layoutIfNeeded()
        }, completion: { (finished) -> Void in
        })
    }
    
    func hidepicker(){
        UIView.animate(withDuration: 0.3,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: { () -> Void in
                        self.viewContainerPicker_heigth.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: { (finished) -> Void in
        })
    }
    
    //**>> Check Validation
    func canBookLater()-> Bool
    {
        if pickUplocationID.isEmpty || dropOffID.isEmpty {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select Address first.", showInView: self.view)
            return false
            
        } else if selectedDate.isEmpty {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select date and time.", showInView: self.view)
            return false
            
        } else if selectedCarType.isEmpty {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select Car Type.", showInView: self.view)
            return false
        }
        return true
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func btnBack_Action(_: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBtnPickUpLocation(_ sender: UIButton) {
        
        let bookLaterSearchVC = self.storyboard?.instantiateViewController(withIdentifier: "BookLaterSearchVC") as! BookLaterSearchVC
        bookLaterSearchVC.isFromPickup = true
        bookLaterSearchVC.selecteAddressDone = {[weak self] addressData  in
            
            self?.btnPickUpLocation.setTitle((addressData["loc_name"] as? String)!, for: .normal)
            self?.pickUplocationID = (addressData["loc_id"] as? String)!
        }
        self.navigationController?.pushViewController(bookLaterSearchVC, animated: true)
    }
    
    @IBAction func actionBtnSelectCarType(_ sender: UIButton) {
        
        hide()
        if pickUplocationID.isEmpty || dropOffID.isEmpty {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select Address first.", showInView: self.view)
            return
        }
        getCabDetail_ApiCall()
    }
    
    @IBAction func actionBtnDropOffLocation(_ sender: UIButton) {
        
        if pickUplocationID.isEmpty  {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select Pickup Address first.", showInView: self.view)
            return
        }
        let bookLaterSearchVC = self.storyboard?.instantiateViewController(withIdentifier: "BookLaterSearchVC") as! BookLaterSearchVC
        bookLaterSearchVC.isFromDropOff = true
        bookLaterSearchVC.strPickUpID = self.pickUplocationID
        bookLaterSearchVC.selectDropOffAddress = {[weak self] addressData  in
            
            self?.btnDropOffLocation.setTitle((addressData["loc_name"] as? String)!, for: .normal)
            self?.dropOffID = (addressData["loc_id"] as? String)!
        }
        self.navigationController?.pushViewController(bookLaterSearchVC, animated: true)
    }
    
    @IBAction func actionBtnBookingDate(_ sender: UIButton) {
        hidepicker()
        show()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        mainDate = pickerDate.date.addingTimeInterval(TimeInterval(10.0 * 60.0))
        selectedDate = dateFormatter.string(from: mainDate!)
        let convertedDate = dateFormatter.date(from: selectedDate)
        pickerDate.date = convertedDate!
    }
    
    @IBAction func btn_CancelAction(_ sender: UIButton) {
        hide()
        selectedDate = ""
        pickerDate.date = Date()
    }
    
    @IBAction func btn_DoneAction(_ sender: UIButton) {
        
        hidepicker()
        self.btnBookingDate.setTitle(selectedDate, for: .normal)
        hide()
        pickerDate.date = Date()
    }
    
    @IBAction func actionBtnContinue(_ sender: UIButton) {
        
        if canBookLater() {
            
            let alertVC = UIAlertController(title: "Message", message: "Are you sure, you want to book trip?", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default){ __ in
                self.bookLater_ApiCall()
            }
            let actionCancel = UIAlertAction(title: "CANCEL", style: .default){ __ in
            }
            alertVC .addAction(actionOk)
            alertVC .addAction(actionCancel)
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btn_CancelPickerAction(_ sender: UIButton) {
        hidepicker()
    }
    
    @IBAction func btn_DonePickerAction(_ sender: UIButton) {
        
        selectedCarType = arrCarName[intselectedValue]
        selectedCarId = arrCarType[intselectedValue]
        self.btnSelectCarType.setTitle(selectedCarType, for: .normal)
        hidepicker()
    }
    
    //****************************************************
    // MARK: - API Call
    //****************************************************
    
    func getCabDetail_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(pickUplocationID, forKey:"pickup")
        paraDict.setValue(dropOffID, forKey:"dropoff")
        
        AppTheme().callPostService(String(format: "%@getcars", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)
                
                if let array_response : NSArray = respDict["response"] as? NSArray {
                    self.arrCabDetail = array_response.mutableCopy() as! NSMutableArray
                }
                
                if self.arrCabDetail.count == 0 {
                    _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "No Cab is available for this routine and time. Please choose another cab.", showInView: self.view)
                    return
                }
                for i in 0..<self.arrCabDetail.count {
                    
                    let dict = self.arrCabDetail.object(at: i) as! NSDictionary
                    if let str_carName : String = dict["taxi_type_name"] as? String {
                        self.arrCarName.append(str_carName)
                    }
                    if let str_carType : String = dict["route_car_type"] as? String{
                        self.arrCarType.append(str_carType)
                    }
                    print("11111 \(self.arrCarType)")
                }
                
                self.pickerCarType.reloadAllComponents()
                self.showPicker()
                
            } else{
                self.loading.dismiss()
            }
        }
    }
    
    //Book Later API Call
    
    func bookLater_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(pickUplocationID, forKey:"pickup")
        paraDict.setValue(dropOffID, forKey:"dropoff")
        paraDict.setValue(AppTheme.getLoginDetails().value(forKey: "id")! as AnyObject, forKey:"user_id")
        paraDict.setValue(selectedCarId, forKey:"car_type")
        paraDict.setValue(selectedDate, forKey:"traveltime")
        
        AppTheme().callPostService(String(format: "%@bookinglater", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)
                
                let alertVC = UIAlertController(title: "Message", message: "Your request has been send successfully.", preferredStyle: .alert)
                let actionOk = UIAlertAction(title: "OK", style: .default){ __ in
                    self.navigationController?.popViewController(animated: true)
                }
                
                alertVC .addAction(actionOk)
                self.present(alertVC, animated: true, completion: nil)
                
            } else {
                self.loading.dismiss()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Error! Please try again later.", showInView: self.view)
            }
        }
    }
    
    //** PickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCarName.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrCarName[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        intselectedValue = row
        //selectedCarType = arrCarName[row]
        //  selectedCarId = arrCarType[row]
    }
}

