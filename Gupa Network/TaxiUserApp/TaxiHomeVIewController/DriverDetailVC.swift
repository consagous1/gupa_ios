//
//  DriverDetailVC.swift
//  Gupa Network
//
//  Created by Apple on 16/02/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit
import MapKit
import JGProgressHUD

class DriverDetailVC: UIViewController , MKMapViewDelegate {

    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var lblDriverName     : UILabel!
    @IBOutlet var lblCarName        : UILabel!
    @IBOutlet var btnCarNumber      : UIButton!
    @IBOutlet var imgDriver         : UIButton!
    @IBOutlet var mapView           : MKMapView!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
     var driverDict : NSMutableDictionary = [:]
     var loading : JGProgressHUD! = JGProgressHUD()
     var arrOfResponseKey :  NSMutableArray = NSMutableArray()

     var driverType = ""
     var paymentType = ""
     var tripAmount = ""
     var strBookingId = ""
     var strTime = ""

    //****************************************************
    // MARK: - View Life Cycle
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        print(driverDict)
        driverDetail()
        setNavigationController()
        appDelegate.centerNav = self.navigationController
    }

    override func viewWillAppear(_ animated: Bool) {
        setAnnotationView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************

    func setNavigationController() {
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
      func driverDetail() {
        
        lblDriverName.text =  (driverDict["name"] as? String)!
        btnCarNumber.setTitle((driverDict["car_no"] as? String)!, for: .normal)
        lblCarName.text   =  (driverDict["taxi_type_name"] as? String)!
        
        if let str_profile_pic : String =  driverDict["profile_pic"] as? String {
            let url : URL = URL(string: str_profile_pic)!
            self.imgDriver.kf.setImage(with: url, for: .normal)
         }
    }
    
    func setAnnotationView() {
        
        let latitudeRegion  =  appDelegate.dictTaxiPassenger["kAPI_PickUpLat"] as! String
        let longitudeRegion =  appDelegate.dictTaxiPassenger["kAPI_PickupLog"] as! String
        
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: Double("\(latitudeRegion)")!, longitude: Double("\(longitudeRegion)")!), span: MKCoordinateSpan(latitudeDelta: 0.12, longitudeDelta: 0.12))
        mapView.setRegion(region, animated: true)
        
        let latPicklat = appDelegate.dictTaxiPassenger["kAPI_PickUpLat"] as! String
        let latPickLog = appDelegate.dictTaxiPassenger["kAPI_PickupLog"] as! String
        
        let latDropLat = appDelegate.dictTaxiPassenger["kAPI_DropOffLat"] as! String
        let latDropLog = appDelegate.dictTaxiPassenger["kAPI_DropOffLog"] as! String
        
        let pickupAddress = appDelegate.dictTaxiPassenger["kAPI_PickupAddress"] as! String
        let dropOffAddress = appDelegate.dictTaxiPassenger["kAPI_dropOffAddress"] as! String
        
        let pickUpPin = CityLocation(title: pickupAddress, coordinate: CLLocationCoordinate2D(latitude: Double(latPicklat)!, longitude: Double(latPickLog)!))
        let dropOff = CityLocation(title: dropOffAddress, coordinate: CLLocationCoordinate2D(latitude: Double(latDropLat)!, longitude: Double(latDropLog)!))
        
        mapView.addAnnotation(pickUpPin)
        mapView.addAnnotation(dropOff)
        mapView.addAnnotations([pickUpPin, dropOff])
    }
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    
    func cancalBooking_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        startProgressBar()
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(strBookingId as AnyObject, forKey:"booking_id")
        
        AppTheme().callPostService(String(format: "%@canceltrip", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                
                appDelegate.dictTaxiPassenger = NSMutableDictionary()
                
                let alertVC = UIAlertController(title: "Booking cancelled sucessfully!", message: "", preferredStyle: .alert)
                
                let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                    let newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.storyboardMain, viewControllerName: TaxiBookingsVC.nameOfClass) as TaxiBookingsVC
                    self.navigationController?.pushViewController(newGroupViewController, animated: true)
                }
                
                alertVC.addAction(actionOk)
                self.present(alertVC, animated: true, completion: nil)
                print(respDict)
                
            } else {
                self.loading.dismiss()
            }
        }
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
//    @IBAction func btnContinue_Action(_ sender: UIButton) {
//
//        let tripDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "TripDetailsVC") as! TripDetailsVC
//        tripDetailsVC.tripDetailDict = driverDict
//        tripDetailsVC.str_SelectCardType = paymentType
//        tripDetailsVC.str_SelectedAmount = tripAmount
//        tripDetailsVC.str_SelectedBookinID =  self.strBookingId
//        self.navigationController?.pushViewController(tripDetailsVC, animated: true)
//    }
    
    @IBAction func btnCancel_Action(_ sender: UIButton) {
        
        let alertVC = UIAlertController(title: "Alert", message: "Are you sure you want to cancel your trip?", preferredStyle: .alert)
        
        let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
            self.cancalBooking_ApiCall()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default){ __ in
        }
        
        alertVC.addAction(cancel)
        alertVC.addAction(actionOk)
        self.present(alertVC, animated: true, completion: nil)
    }

}
