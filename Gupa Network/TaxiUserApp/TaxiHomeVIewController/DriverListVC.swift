//
//  DriverListVC.swift
//  Gupa Network
//
//  Created by Apple on 17/01/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit
import MapKit
import JGProgressHUD

class DriverListVC: UIViewController , MKMapViewDelegate {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var tblDriverList     : UITableView!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
     var loading : JGProgressHUD! = JGProgressHUD()
     var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    
     var strCarType = ""
     var isDriverAvailable = false

     var dicDriverData : NSMutableDictionary = NSMutableDictionary()

     public var onSelectDone:(NSMutableDictionary ,Bool)->() = { _,_  in}

    //****************************************************
    // MARK: - View Life Cycle
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getDriverList_ApiCall()
        setNavigationController()
        appDelegate.centerNav = self.navigationController
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func btnContinue_Action(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
            self.onSelectDone(self.dicDriverData, self.isDriverAvailable)
        }
    }
    
    @IBAction func btnBack_Action(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //****************************************************
    //MARK: Loader method
    //****************************************************
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    //****************************************************
    // MARK: - API Call
    //****************************************************
    
    // Get Driver List
    
    func getDriverList_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        startProgressBar()
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(appDelegate.dictTaxiPassenger["kAPI_PickUpId"] as AnyObject, forKey:"pickup")
        paraDict.setValue(appDelegate.dictTaxiPassenger["kAPI_DropOffId"] as AnyObject, forKey:"dropoff")
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"user_id")
        paraDict.setValue(strCarType, forKey:"car_type")
        
        AppTheme().callPostService(String(format: "%@getdriver", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)
               
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }
                if self.arrOfResponseKey.count == 0 {
                    
                    self.isDriverAvailable = false
                    let alertVC = UIAlertController(title: "Alert", message: "Driver's are not available on this route.Please select other vehicle", preferredStyle: .alert)
                    let actionOk = UIAlertAction(title: "OK", style: .default){ __ in
                        self.dismiss(animated: true, completion: nil)
                    }
                    alertVC.addAction(actionOk)
                    self.present(alertVC, animated: true, completion: nil)
                    
                } else {
                    self.isDriverAvailable = true
                }
                self.tblDriverList.reloadData()
            } else{
                self.loading.dismiss()
            }
        }
    }
}


//****************************************************
// MARK: - UITableViewDataSource
//****************************************************

extension DriverListVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfResponseKey.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : TaxiDriverCell = tableView.dequeueReusableCell(withIdentifier: "TaxiDriverCell") as! TaxiDriverCell
        
        let dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        
        if let str_DriverName : String = dic["name"] as? String{
            cell.lblDriverName.text = str_DriverName
        }
        
        var rating : NSInteger = 0
        if let strRating: String = dic["rating"] as? String
        {
            rating = NSInteger(strRating)!
            if rating == 0 {
                cell.imgRating.image = UIImage(named: "")
            }
            if rating == 1    {
                cell.imgRating.image = UIImage(named: "Horrible")
            } else if rating == 2 {
                cell.imgRating.image = UIImage(named: "BadTaxi")
            } else if rating == 3 {
                cell.imgRating.image = UIImage(named: "GoodTaxi_icon")
            } else if rating == 4 {
                cell.imgRating.image = UIImage(named: "FairTaxi")
            } else if rating == 5 {
                cell.imgRating.image = UIImage(named: "ExcellentTaxi_icon")
            }
        }
        
        if let str_user_profile : String = dic["profile_pic"] as? String{
            let url = URL(string:str_user_profile)
            if url != nil {
                cell.imgProfile.kf.setImage(with: url!, placeholder: UIImage(named: "driver-list_icon"), options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, imageURL) in
                    print(image?.description ?? "")
                })
            }
        }
        return cell
    }
    
}

//****************************************************
// MARK: - UITableViewDelegate
//****************************************************

extension DriverListVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        
        dicDriverData = dic as! NSMutableDictionary

    }
        
}
