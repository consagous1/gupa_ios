//
//  MyTripsVC.swift
//  Gupa Network
//
//  Created by Apple on 17/01/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit
import MapKit
import JGProgressHUD

class MyTripsVC : UIViewController , MKMapViewDelegate {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************

    @IBOutlet weak var tblDriverList     : UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var constraintSegmentHeigth: NSLayoutConstraint!
    
    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()

    var isFromUpcoming : Bool = false
    var isFromcomplete : Bool = false

    //****************************************************
    // MARK: - Life Cycle Methods≈
    //****************************************************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        constraintSegmentHeigth.constant = 40
        
        appDelegate.centerNav = self.navigationController
        
        let attributes = [ NSAttributedStringKey.foregroundColor : UIColor.white,
                           NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16)];
        let attributesSelected = [ NSAttributedStringKey.foregroundColor : UIColor.white,
                                   NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16)];
        segmentedControl.setTitleTextAttributes(attributes, for: UIControlState.selected)
        segmentedControl.setTitleTextAttributes(attributesSelected, for: UIControlState.normal)
        
        segmentedControl.selectedSegmentIndex = 0
        getTripDetail_ApiCall(strType : "1")

        self.tblDriverList.rowHeight = UITableViewAutomaticDimension
        self.tblDriverList.estimatedRowHeight = 80
        
        setNavigationController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************

    @IBAction func btnBack_Action(_: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func indexChanged(_ sender: AnyObject) {
        
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            isFromUpcoming = true
            isFromcomplete = false
            self.arrOfResponseKey.removeAllObjects()
            getTripDetail_ApiCall(strType : "1") // 1 for book now and 2 for book later
        case 1:
            isFromUpcoming = false
            isFromcomplete = true
            self.arrOfResponseKey.removeAllObjects()
            getTripDetail_ApiCall(strType : "2")

        default:
            break
        }
    }
    
    //****************************************************
    // MARK: - API Call
    //****************************************************
    
    func getTripDetail_ApiCall(strType : String) {

        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(AppTheme.getLoginDetails().value(forKey: "id")! as AnyObject, forKey:"user_id")
        paraDict.setValue(strType, forKey:"booking_type")

        AppTheme().callPostService(String(format: "%@gettripdetail", AppLinkTaxi), param: paraDict) {(result, data) -> Void in

            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)

                if let array_response : NSArray = respDict["response"] as? NSArray {
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }
                self.tblDriverList.reloadData()
            } else{
                self.loading.dismiss()
            }
        }
    }
 
}

//****************************************************
// MARK: - UITableViewDataSource
//****************************************************

extension MyTripsVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfResponseKey.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : TaxiDriverCell = tableView.dequeueReusableCell(withIdentifier: "TaxiDriverCell") as! TaxiDriverCell
        
        let dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary

        if isFromcomplete {

            cell.constraintHeigthName.constant = 18
            cell.constraintWidthImage.constant = 22

            if let array_driver: NSArray = dic["driver"] as? NSArray
            {
                let dictDriver = array_driver.object(at: 0) as! NSDictionary

                if let str_carName : String = dictDriver["user_name"] as? String {
                  cell.lblDriverName.text = str_carName
                }

                if let str_user_profile : String = dictDriver["profile_pic"] as? String{

                    let url = URL(string:str_user_profile)
                    if url != nil {
                        cell.imgProfile.kf.setImage(with: url!, placeholder: UIImage(named: "driver-list_icon"), options: nil, progressBlock: nil, completionHandler: {
                            (image, error, cacheType, imageURL) in
                            print(image?.description ?? "")
                        })
                    }
                }
            }
              cell.lblStatus.text =  "Confirmed"

        } else {
               cell.constraintHeigthName.constant = 0
               cell.constraintWidthImage.constant = 0

               cell.lblStatus.text =  "Pending"
               cell.imgProfile.image = #imageLiteral(resourceName: "booklater")

        }

        if let str_DataAndTime : String = dic["traveltime"] as? String{
            cell.lblTripTime.text = str_DataAndTime
        }

        var addressPick = ""
        var addressDropoff = ""

        if let array_pickup : NSArray = dic["pickup"] as? NSArray {

            let dictpickAddress = array_pickup.object(at: 0) as! NSDictionary

            if let str_locName : String = dictpickAddress["loc_name"] as? String {
                addressPick = str_locName
            }
        }

        if let array_dropOff : NSArray = dic["drop"] as? NSArray {

            let dictpickAddress = array_dropOff.object(at: 0) as! NSDictionary

            if let str_locName : String = dictpickAddress["loc_name"] as? String {
                addressDropoff = str_locName
            }
        }
        cell.lblAddress.text = "\(addressPick) to \(addressDropoff)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isFromcomplete {
            return 105
        } else {
            return 90
        }
    }
    
}

//****************************************************
// MARK: - UITableViewDelegate
//****************************************************

extension MyTripsVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}
