//
//  SearchTaxiLocationVC.swift
//  Gupa Network
//
//  Created by Apple on 22/02/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class SearchTaxiLocationVC: UIViewController , UISearchBarDelegate {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet weak var search_user: UISearchBar!
    @IBOutlet weak var tbl_search_user: UITableView!
    
    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    
    var loading : JGProgressHUD! = JGProgressHUD()
    
    var isSearch : Bool = false
    var arrFilter : NSArray = NSArray()
    var isFromPickUp : Bool = false

    var pickUpLocationID : String = ""
    var filtered: NSArray = NSArray()
    
    public var onSelect:(String ,String, String , String)->() = { _,_ ,_,_ in}

    //****************************************************
    // MARK: -  Life Cycle Methods
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()

        appDelegate.centerNav = self.navigationController
       
         setNavigationController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if isFromPickUp {
            getLocationList()
        } else {
            dropoffLocation_ApiCall()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    // MARK: - Action Methods
    
    @IBAction func btnBack_Action(_: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //****************************************************
    // MARK: - API Call
    //****************************************************
    
    // Pick Up location API Call
    func getLocationList() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        startProgressBar()
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(String(CurrentLocationManager.sharedInstance.currentCoordinate.latitude), forKey:"latitude")
        paraDict.setValue(String(CurrentLocationManager.sharedInstance.currentCoordinate.longitude), forKey:"longitude")
        
//        paraDict.setValue(String(22.719568), forKey:"latitude")  //TODO : Uncomment
//        paraDict.setValue(String(75.857727), forKey:"longitude")
//
        AppTheme().callPostService(String(format: "%@getpickup", AppLinkTaxi), param: paraDict) {(result, data) -> Void in

            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)
                
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }
                self.tbl_search_user.reloadData()
            } else{
                self.loading.dismiss()
            }
        }
    }
    
    // Drop off location API Call

    func dropoffLocation_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        startProgressBar()
        let paraDict = NSMutableDictionary()
        paraDict.setValue(pickUpLocationID, forKey:"pickup")
        
        AppTheme().callPostService(String(format: "%@getdrop", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)
                
                if let array_response : NSArray = respDict["response"] as? NSArray {
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                } 
                self.tbl_search_user.reloadData()
            } else{
                self.loading.dismiss()
            }
        }
    }
    
    //==============================
    // MARK: Loader method
    //==============================
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    //=====================================
    //MARK: UISearchBar delegate methods
    //=====================================
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearch = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searchBar.resignFirstResponder()
//        searchBar.text = ""
//        isSearch = false;
//        getLocationList()
//    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let inputString = searchText.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        if inputString.count > 0
        {
            isSearch = true

            let searchPredicate = NSPredicate(format: "SELF.loc_name CONTAINS[c] %@", searchBar.text!)
            let array = (arrOfResponseKey as NSArray).filtered(using: searchPredicate)
            arrFilter = array as NSArray
            
            self.tbl_search_user.reloadData()
        }
        else
        {
            isSearch = false
            if isFromPickUp {
                getLocationList()
            } else {
                dropoffLocation_ApiCall()
            }

        }
    }
}

//========================================================
// MARK: UITableview Datasource and delegate methods
//========================================================

extension SearchTaxiLocationVC : UITableViewDelegate , UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(isSearch) {
            return arrFilter.count
        } else {
        return arrOfResponseKey.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
     self.loading.dismiss()
        
      var cell : SearchCell! = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
       if cell == nil {
          cell = SearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "SearchCell")
        }
        var dic: NSDictionary = NSDictionary()

         if(isSearch){
             dic = self.arrFilter.object(at: indexPath.row) as! NSDictionary
            } else {
             dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
           }
            
         if let str_user_full_name : String = dic["loc_name"] as? String{
            cell.lblAddress.text = str_user_full_name
        }
       return cell
    }
    

   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
   {
    var dic : NSDictionary = NSDictionary()
    
    if(isSearch){
        dic = self.arrFilter.object(at: indexPath.row) as! NSDictionary
    } else {
        dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
    }
    let strPickUpLocationId : String = (dic["loc_id"] as? String)!
    let strPickUpAddress : String    = (dic["loc_name"] as? String)!
    let strlocLatitude : String      = (dic["loc_lat"] as? String)!
    let strlocLogitude : String      = (dic["loc_long"] as? String)!
   
    self.onSelect(strPickUpLocationId, strPickUpAddress ,strlocLatitude ,strlocLogitude)
    
//    let pickupAndDropLocationVC = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.taxiVCStoryboard, viewControllerName: PickupAndDropLocationVC.nameOfClass) as PickupAndDropLocationVC
//    self.navigationController?.popToViewController(pickupAndDropLocationVC, animated: false)

    _ = self.navigationController?.popViewController(animated: true)

//    for controller in self.navigationController!.viewControllers as Array {
//        
//        if controller.isKind(of: PickupAndDropLocationVC.self) {
//            _ = self.navigationController?.popToViewController(controller as UIViewController, animated: true)
//            break
//        }
//    }
  }
}
