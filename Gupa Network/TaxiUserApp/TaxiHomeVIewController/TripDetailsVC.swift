//
//  TripDetailsVC.swift
//  Gupa Network
//
//  Created by Apple on 17/01/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class TripDetailsVC: UIViewController {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var lblDriverName     : UILabel!
    @IBOutlet var lblCarName        : UILabel!
    @IBOutlet var btnCarNumber      : UIButton!
    @IBOutlet var btnDriverImage    : UIButton!
    @IBOutlet var btnCancel   : UIButton!

    @IBOutlet var lblAddress         : UILabel!
    @IBOutlet var lblDistance        : UILabel!
    @IBOutlet var imgCard            : UIImageView!
    @IBOutlet var lblCashType        : UILabel!
    @IBOutlet var lblAmount          : UILabel!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var tripDetailDict : NSDictionary = [:]
    var pickUpDict : NSDictionary = [:]
    var dropOffDict : NSDictionary = [:]

    var loading : JGProgressHUD! = JGProgressHUD()

    var str_SelectCardType = ""
    var str_SelectedAmount = ""
    var str_SelectedBookinID = ""
    var str_BookingDateAndTime = ""
    var str_Distance = ""
    var isTripStarted : Bool = false

    //****************************************************
    // MARK: - View Life Cycle
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationController()
        appDelegate.centerNav = self.navigationController
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        setAllTripDetailData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        
        self.navigationItem.setHidesBackButton(true, animated: false)

        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func setAllTripDetailData() {
        
        print("111111 \(tripDetailDict)")
        
        lblCarName.text = (tripDetailDict["taxi_type_name"] as? String)!
        lblDriverName.text = (tripDetailDict["name"] as? String)!
        btnCarNumber.setTitle((tripDetailDict["car_no"] as? String)!, for: .normal)
        lblAddress.text = "\(String(describing: pickUpDict["loc_name"] as! String)) to \(String(describing: dropOffDict["loc_name"] as! String))"
        lblDistance.text! = "\(str_Distance)"

        lblAmount.text = "$ \(str_SelectedAmount)"
        
        if str_SelectCardType == "Credit card" {
            lblCashType.text = "Credit card"
            imgCard.image = #imageLiteral(resourceName: "Credit_card")
        } else if str_SelectCardType == "Debit card" {
            lblCashType.text = "Debit card"
            imgCard.image = #imageLiteral(resourceName: "Add_payment")
        } else if str_SelectCardType == "Cash" {
            lblCashType.text = "Cash"
            imgCard.image =  #imageLiteral(resourceName: "Cash")
        }
        
        if let str_profile_pic : String =  tripDetailDict["profile_pic"] as? String {
            let url : URL = URL(string: str_profile_pic)!
            self.btnDriverImage.kf.setImage(with: url, for: .normal)
        }
        
        if self.isTripStarted == true {
            btnCancel.isUserInteractionEnabled = false
            btnCancel.backgroundColor = UIColor.lightGray
        } else {
            btnCancel.isUserInteractionEnabled = true
            btnCancel.backgroundColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        }
    }
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    //****************************************************
    // MARK: - API Call
    //****************************************************
    
    func cancalBooking_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        startProgressBar()
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(str_SelectedBookinID as AnyObject, forKey:"booking_id")
        
        AppTheme().callPostService(String(format: "%@canceltrip", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                
                appDelegate.dictTaxiPassenger = NSMutableDictionary()
                
                let alertVC = UIAlertController(title: "Booking cancelled sucessfully!", message: "", preferredStyle: .alert)

                let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                    
                    let cancelTripVC = self.storyboard?.instantiateViewController(withIdentifier: "CancelTripVC") as! CancelTripVC
                    cancelTripVC.driverDict             = self.tripDetailDict
                    cancelTripVC.str_TripAmountAmount   = self.str_SelectedAmount
                    cancelTripVC.bookingId              = self.str_SelectedBookinID
                    cancelTripVC.timeAndDate            = self.str_BookingDateAndTime
                    self.navigationController?.pushViewController(cancelTripVC, animated: true)
                }
                
                alertVC.addAction(actionOk)
                self.present(alertVC, animated: true, completion: nil)
                print(respDict)
                
            } else {
                self.loading.dismiss()
            }
        }
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    
    @IBAction func btnCall_Action(_ sender: UIButton) {
        
        let strPhone = (tripDetailDict["phone"] as? String)!
        print("Contact number== \(strPhone)")

        if let url = URL(string: "tel://\(strPhone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else {
            print("Your device doesn't support this feature.")
        }
        
    }
    
    @IBAction func btnCancelTrip_Action(_ sender: UIButton) {
        
        let alertVC = UIAlertController(title: "Alert", message: "Are you sure you want to cancel your trip?", preferredStyle: .alert)
       
        let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
            self.cancalBooking_ApiCall()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default){ __ in
        }
        
        alertVC.addAction(cancel)
        alertVC.addAction(actionOk)
        self.present(alertVC, animated: true, completion: nil)
        
    }
}

