//
//  CompletedTripVC.swift
//  Gupa Network
//
//  Created by Apple on 17/01/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class CompletedTripVC: UIViewController {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var lblDriverName    : UILabel!
    @IBOutlet var lblDate          : UILabel!
    @IBOutlet var imgDriver        : UIImageView!
    @IBOutlet var lblAddress       : UILabel!
    @IBOutlet var btnRating        : [UIButton]!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var strBookingId = ""
    var strTripDate = ""

    var driverDict : NSDictionary = [:]
    var pickUpDict : NSDictionary = [:]
    var dropOffDict : NSDictionary = [:]

    var intRating : Int!
    var isRatingSelected = false

    //****************************************************
    // MARK: - View Life Cycle
    //****************************************************

    override func viewDidLoad() {
        super.viewDidLoad()

        appDelegate.centerNav = self.navigationController
        
        lblDriverName.text = (driverDict["name"] as? String)!
        lblDate.text! = strTripDate
        lblAddress.text = "\(String(describing: pickUpDict["loc_name"] as! String)) to \(String(describing: dropOffDict["loc_name"] as! String))"

        if let str_profile_pic : String =  driverDict["profile_pic"] as? String {
            let url : URL = URL(string: str_profile_pic)!
            self.imgDriver.kf.setImage(with: url, placeholder: UIImage(named: "driver-list_icon"))
        }
    }

     func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        setNavigationController()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    //****************************************************
    // MARK: - API Call
    //****************************************************
    
    func submitRating_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        startProgressBar()
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(strBookingId, forKey:"booking_id")
        paraDict.setValue(intRating, forKey:"rating")

        AppTheme().callPostService(String(format: "%@rating", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                
                let strMessage = respDict["message"] as? String
                
                appDelegate.dictTaxiPassenger = NSMutableDictionary()
                
                let alertVC = UIAlertController(title: strMessage, message: "", preferredStyle: .alert)
                
                let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                    
                    let newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.storyboardMain, viewControllerName: TaxiBookingsVC.nameOfClass) as TaxiBookingsVC
                    self.navigationController?.pushViewController(newGroupViewController, animated: true)
                }
                alertVC.addAction(actionOk)
                self.present(alertVC, animated: true, completion: nil)
                print(respDict)
                
            } else{
                self.loading.dismiss()
            }
        }
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func btnSubmit_Action(_ sender: UIButton) {
        
        if isRatingSelected == false {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select driver rating.", showInView: self.view)
            return
        }
        submitRating_ApiCall()
    }
    
    @IBAction func btnSelectRating_Action(_ sender: UIButton) {
        
        isRatingSelected = true
        
          for btn in btnRating {
      
            btn.backgroundColor = UIColor.clear
         }

          if sender.tag == 0 {
            intRating = 1
            sender.backgroundColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
            
          } else if sender.tag == 1 {
             intRating = 2
             sender.backgroundColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
            
          } else if sender.tag == 2 {
            intRating = 3
            sender.backgroundColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
            
         }  else if sender.tag == 3 {
            intRating = 4
            sender.backgroundColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
            
          } else if sender.tag == 4 {
            intRating = 5
            sender.backgroundColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
         }
    }

}
