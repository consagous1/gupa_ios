//
//  CancelTripVC.swift
//  Gupa Network
//
//  Created by Apple on 07/03/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class CancelTripVC : UIViewController {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var tblFeedBack   : UITableView!
    @IBOutlet var lblDriverName : UILabel!
    @IBOutlet var lblCarType    : UILabel!
    @IBOutlet var lblAmount     : UILabel!
    @IBOutlet var lblDate       : UILabel!
    @IBOutlet var imgDriver     : UIButton!
    @IBOutlet var lblPaymentType       : UILabel!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var driverDict : NSDictionary = [:]
    var loading : JGProgressHUD! = JGProgressHUD()
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()

    var bookingId = ""
    var strReasonId = ""
    var timeAndDate = ""
    var str_TripAmountAmount = ""
    var strPaymentType = ""

    var selectedIndex : IndexPath?
    
    //****************************************************
    // MARK: - View Life Cycle
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationController()
        getFeedbackList_ApiCall()
        
        appDelegate.centerNav = self.navigationController
        
        lblDriverName.text = "You trip with \(String(describing: driverDict["name"] as! String))"
        lblAmount.text  = "$\(str_TripAmountAmount)"
        lblDate.text!   = timeAndDate
        lblCarType.text = (driverDict["taxi_type_name"] as? String)!
        lblPaymentType.text = "\(strPaymentType) : Passenger Cancelled"

        if let str_profile_pic : String =  driverDict["profile_pic"] as? String {
            let url : URL = URL(string: str_profile_pic)!
            self.imgDriver.kf.setImage(with: url, for: .normal)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        
        self.navigationItem.setHidesBackButton(true, animated: false)

        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    //****************************************************
    // MARK: - API Call
    //****************************************************
    
    func getFeedbackList_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        startProgressBar()
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue("1", forKey:"type")
        
        AppTheme().callPostService(String(format: "%@getreason", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)
                
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }
                self.tblFeedBack.reloadData()
            } else{
                self.loading.dismiss()
            }
        }
    }
    
    func submitFeedBack_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        startProgressBar()
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(bookingId, forKey:"booking_id")
        paraDict.setValue(strReasonId, forKey:"reason")
        
        AppTheme().callPostService(String(format: "%@canceltripreason", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)
                
                appDelegate.dictTaxiPassenger = NSMutableDictionary()

                let alertVC = UIAlertController(title: "", message: "Feedback sent successfully!", preferredStyle: .alert)
                
                let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                    let newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.storyboardMain, viewControllerName: TaxiBookingsVC.nameOfClass) as TaxiBookingsVC
                    self.navigationController?.pushViewController(newGroupViewController, animated: true)
                }
                alertVC.addAction(actionOk)
                self.present(alertVC, animated: true, completion: nil)
                print(respDict)
            } else{
                self.loading.dismiss()
            }
        }
    }
   
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func btnSubmit_Action(_ sender: UIButton) {
        
        if strReasonId.isEmpty {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please select Reason to cancel trip.", showInView: self.view)
            return
        }
      submitFeedBack_ApiCall()
    }
  
}

//****************************************************
// MARK: - UITableViewDataSource
//****************************************************

extension CancelTripVC : UITableViewDataSource , UITableViewDelegate {


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfResponseKey.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CancelTripcell = tableView.dequeueReusableCell(withIdentifier: "CancelTripcell") as! CancelTripcell
        
        let dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        
        if let str_DriverName : String = dic["reason_text"] as? String{
            cell.itemLabel.text = str_DriverName
        }
        cell.radioButton.tag = indexPath.row
        
        if selectedIndex == indexPath as IndexPath {
            cell.radioButton.setImage(UIImage(named: "radio-on-button"), for: .normal)
           } else {
            cell.radioButton.setImage(UIImage(named: "radio-off"), for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath as IndexPath
        tblFeedBack.reloadData()
        
        let dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        
        strReasonId  = (dic["reason_id"] as? String)!
        
    }
}
