//
//  ChooseCabVC.swift
//  Gupa Network
//
//  Created by Priyanka on 08/01/18.
//  Copyright © 2018 Consagous. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces
import GoogleMaps
import CoreLocation

protocol EnterLocationPopupViewControllerDelegate {
    func removeEnterLocationPopupViewControllerScr()
}

class TaxiHomeVC: UIViewController {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var mapView         : GMSMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var delegate:EnterLocationPopupViewControllerDelegate?

    var latitude = -7.034323799999999
    var longitude = 110.42400399999997
    var displayAddress = [String]()
    private let locationManager = CLLocationManager()

    //****************************************************
    // MARK: - Life Cycle Methods
    //****************************************************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationController()
        appDelegate.centerNav = self.navigationController
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func btnBack_Action(_: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }  
}
