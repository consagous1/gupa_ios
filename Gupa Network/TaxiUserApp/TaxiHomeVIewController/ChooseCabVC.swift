//
//  ChooseCabVC.swift
//  Gupa Network
//
//  Created by Priyanka on 08/01/18.
//  Copyright © 2018 Consagous. All rights reserved.
//

import UIKit
import MapKit
import JGProgressHUD

class ChooseCabVC: UIViewController , MKMapViewDelegate {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var btnAddPayment          : UIButton!
    @IBOutlet var viewChooseCard         : UIView!
    @IBOutlet var mapView                : MKMapView!
    @IBOutlet var viewContainer_heigth   : NSLayoutConstraint!
    @IBOutlet var viewTimeEstimationHeigth: NSLayoutConstraint!
    @IBOutlet var cardImage              : UIImageView!
    @IBOutlet var collectionViewCab      : UICollectionView!
    @IBOutlet var btnCapacity            : UIButton!
    @IBOutlet var lblEstimatedTime       : UILabel!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var pickUpId : String = ""
    var dropOffId : String = ""
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var loading : JGProgressHUD! = JGProgressHUD()
    var arrLatLong    : NSMutableArray = []
    var selectedIndexPath = IndexPath(item: 0, section: 0)
    var taxiCarType = ""

    var coords : NSArray = []
    var finalAmount = ""
    var strBookingId = ""
    var strDateAndTime = ""

    //****************************************************
    // MARK: - Life Cycle Methods≈
    //****************************************************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationController()
        getCabDetail_ApiCall()
        print(appDelegate.dictTaxiPassenger)
        appDelegate.centerNav = self.navigationController
    }

    override func viewWillAppear(_ animated: Bool) {
        
        setAnnotationView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    
    func setNavigationController() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func show(){
        UIView.animate(withDuration: 0.3,
                      delay: 0.1,
                      options: UIViewAnimationOptions.curveEaseIn,
                      animations: { () -> Void in
                        self.viewContainer_heigth.constant = 270
                        self.view.layoutIfNeeded()
              }, completion: { (finished) -> Void in
                self.viewTimeEstimationHeigth.constant = 0
        })
    }
    
    func hide(){
        UIView.animate(withDuration: 0.3,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: { () -> Void in
                        self.viewContainer_heigth.constant = 0
                        self.view.layoutIfNeeded()
          }, completion: { (finished) -> Void in
            self.viewTimeEstimationHeigth.constant = 45
        })
    }
    
    func setAnnotationView() {
     
        let latitudeRegion  =  appDelegate.dictTaxiPassenger["kAPI_PickUpLat"] as! String
        let longitudeRegion =  appDelegate.dictTaxiPassenger["kAPI_PickupLog"] as! String

        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: Double("\(latitudeRegion)")!, longitude: Double("\(longitudeRegion)")!), span: MKCoordinateSpan(latitudeDelta: 0.12, longitudeDelta: 0.12))
        mapView.setRegion(region, animated: true)
        
        let latPicklat = appDelegate.dictTaxiPassenger["kAPI_PickUpLat"] as! String
        let latPickLog = appDelegate.dictTaxiPassenger["kAPI_PickupLog"] as! String

        let latDropLat = appDelegate.dictTaxiPassenger["kAPI_DropOffLat"] as! String
        let latDropLog = appDelegate.dictTaxiPassenger["kAPI_DropOffLog"] as! String

        let pickupAddress   = appDelegate.dictTaxiPassenger["kAPI_PickupAddress"] as! String
        let dropOffAddress  = appDelegate.dictTaxiPassenger["kAPI_dropOffAddress"] as! String

        let pickUpPin = CityLocation(title: pickupAddress, coordinate: CLLocationCoordinate2D(latitude: Double(latPicklat)!, longitude: Double(latPickLog)!))
        let dropOff = CityLocation(title: dropOffAddress, coordinate: CLLocationCoordinate2D(latitude: Double(latDropLat)!, longitude: Double(latDropLog)!))

        mapView.addAnnotation(pickUpPin)
        mapView.addAnnotation(dropOff)
        mapView.addAnnotations([pickUpPin, dropOff])
    }

    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func btnConfirm_Action(_ sender: UIButton) {
        
        if  btnAddPayment.titleLabel?.text == "Add Payment" {
            _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Please choose payment method.", showInView: self.view)
            return
        }
        
        let driverListVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverListVC") as! DriverListVC
        driverListVC.strCarType = taxiCarType
        driverListVC.onSelectDone = {[weak self] driverData , isDriverList  in
            
        if driverData.count != 0 {
            if isDriverList == true {
                self?.cabBooking_ApiCall(driverDataDict : driverData)
              }
            } else {
                let alertVC = UIAlertController(title: "Alert", message: "Please Select driver first!", preferredStyle: .alert)
                let actionOk = UIAlertAction(title: "OK", style: .default){ __ in
                }
                alertVC .addAction(actionOk)
                self?.present(alertVC, animated: true, completion: nil)
            }
        }
        driverListVC.modalPresentationStyle = .overCurrentContext

        self.present(driverListVC, animated: true, completion: nil)
    }
    
    @IBAction func btnAddPayment_Action(_ sender: UIButton) {
        
        self.show()
    }
    
    @IBAction func btnBack_Action(_: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
 
    @IBAction func btnSelectPayment_Action(_ sender: UIButton) {
        
        if sender.tag == 1 {
            btnAddPayment.setTitle("Credit card", for: .normal)
            cardImage.image = #imageLiteral(resourceName: "Credit_card")
        } else if sender.tag == 2 {
             btnAddPayment.setTitle("Debit card", for: .normal)
            cardImage.image = #imageLiteral(resourceName: "Add_payment")
        } else if sender.tag == 3  {
            btnAddPayment.setTitle("Cash", for: .normal)
            cardImage.image =  #imageLiteral(resourceName: "Cash")
        }
        self.hide()
    }
    
    //****************************************************
    //MARK: Loader method
    //****************************************************

    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    //****************************************************
    // MARK: - API Call
    //****************************************************
    
    // Get Cab detail
    
    func getCabDetail_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        startProgressBar()
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(pickUpId, forKey:"pickup")
        paraDict.setValue(dropOffId, forKey:"dropoff")
        
        AppTheme().callPostService(String(format: "%@getcars", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)
                
                if let str_Time : String = respDict["route_estimate"] as? String{
                    self.lblEstimatedTime.text = "Estimated time to Complete trip in \(str_Time) mins"
                }
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }
                self.collectionViewCab.reloadData()
            } else{
                self.loading.dismiss()
            }
        }
    }
    
    func cabBooking_ApiCall(driverDataDict :NSMutableDictionary ) {

        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }

        startProgressBar()

        let paraDict = NSMutableDictionary()
        paraDict.setValue(appDelegate.dictTaxiPassenger["kAPI_PickUpId"] as AnyObject, forKey:"pickup")
        paraDict.setValue(appDelegate.dictTaxiPassenger["kAPI_DropOffId"] as AnyObject, forKey:"dropoff")
        paraDict.setValue(AppTheme.getLoginDetails().object(forKey: "id")! as AnyObject, forKey:"user_id")
        paraDict.setValue(taxiCarType, forKey:"car_type")
        paraDict.setValue(self.btnAddPayment.titleLabel?.text!, forKey:"payment_type")
        paraDict.setValue(driverDataDict["dr_id"] as! String, forKey:"driver_id")

        AppTheme().callPostService(String(format: "%@booking", AppLinkTaxi), param: paraDict) {(result, data) -> Void in

            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)

                if let array_response = respDict["response"] as? NSDictionary {
                    self.finalAmount = array_response["amount"] as! String
                    self.strBookingId = String(describing: array_response["booking_id"] as! NSNumber)
                    self.strDateAndTime = array_response["datetime"] as! String

                    print(self.strBookingId)
                    
                    if SocketManager.sharedInstance.isConnected() {
                        SocketManager.sharedInstance.emitData("Searched Driver Detail", data: array_response)
                    }
                }
                
                let confirmationTaxiVC = self.storyboard?.instantiateViewController(withIdentifier: "DriverDetailVC") as! DriverDetailVC
                confirmationTaxiVC.driverDict  = driverDataDict
                confirmationTaxiVC.driverType  = (self.taxiCarType)
                confirmationTaxiVC.tripAmount  =  self.finalAmount
                confirmationTaxiVC.strTime     =  self.strDateAndTime
                confirmationTaxiVC.strBookingId =  self.strBookingId
                confirmationTaxiVC.paymentType = (self.btnAddPayment.titleLabel?.text!)!
                self.navigationController?.pushViewController(confirmationTaxiVC, animated: true)
            } else{
                self.loading.dismiss()
            }
        }
    }
 
}

 //MARK: - UICollectionViewDataSource, Delegate
extension ChooseCabVC : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfResponseKey.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarTypeCell", for: indexPath as IndexPath) as! CarTypeCell

        let dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary

        if let str_carType : String = dic["taxi_type_name"] as? String{
            cell.lblCarName.text = str_carType
        }
        if let str_carRate : String = dic["route_car_rate"] as? String{
            cell.lblCarPrice.text = "$\(str_carRate)"
        }
      
        if let str_user_profile : String = dic["taxi_icon"] as? String{
            let url = URL(string:str_user_profile)
            if url != nil {
                cell.imgCar.kf.setImage(with: url!, for: .normal, placeholder: UIImage(named: "driver-list_icon"), options: nil, progressBlock: nil, completionHandler: {
                    (image, error, cacheType, imageURL) in
                    print(image?.description ?? "")
                })
            }
        }
        
        if selectedIndexPath == indexPath as IndexPath {
            
            if let str_TaxiCapacity : String = dic["taxi_capacity"] as? String {
                btnCapacity.setTitle("1-\(str_TaxiCapacity)", for: .normal)
            }
            if let str_carType : String = dic["route_car_type"] as? String{
                taxiCarType   = str_carType
            }
            
            //Animate cell border width
            let widthAnimate : CABasicAnimation = CABasicAnimation(keyPath: "borderWidth")
            widthAnimate.fromValue =  3
            widthAnimate.duration = 0.1
            cell.viewBg.layer.add(widthAnimate, forKey: "borderWidth")
            
            cell.viewBg.layer.borderColor = UIColor.red.cgColor
            cell.viewBg.layer.borderWidth = 2
        } else {
             cell.viewBg.layer.borderColor = UIColor.clear.cgColor
             cell.viewBg.layer.borderWidth = 0
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       // let dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        
//        if let str_carType : String = dic["route_car_type"] as? String{
//          taxiCarType   = str_carType
//        }
        selectedIndexPath = indexPath as IndexPath
        collectionViewCab.reloadData()
    }
}

class CityLocation : NSObject, MKAnnotation {
    
    var title: String?
    var coordinate: CLLocationCoordinate2D
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
}
