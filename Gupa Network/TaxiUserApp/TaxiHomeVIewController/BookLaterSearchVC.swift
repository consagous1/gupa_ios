//
//  BookLaterSearchVC.swift
//  Gupa Network
//
//  Created by Apple on 14/03/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class BookLaterSearchVC: UIViewController , UISearchBarDelegate {

    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet weak var search_user: UISearchBar!
    @IBOutlet weak var tbl_search_user: UITableView!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var arrOfResponseKey :  NSMutableArray = NSMutableArray()
    var loading : JGProgressHUD! = JGProgressHUD()

    var strSearch     : String = ""
    var isFromPickup  : Bool = false
    var isFromDropOff : Bool = false
    var strPickUpID     : String = ""

    public var selecteAddressDone :(NSDictionary)->() = { _ in}
    public var selectDropOffAddress :(NSDictionary)->() = { _ in}

    //****************************************************
    // MARK: -  Life Cycle Methods
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationController()
        appDelegate.centerNav = self.navigationController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isFromDropOff {
            dropoffLocation_ApiCall()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    // MARK: - Action Methods
    
    @IBAction func btnBack_Action(_: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //****************************************************
    // MARK: - API Call
    //****************************************************
    
    // Pick Up location API Call
    
    func getLocationList(searchString : String) {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        startProgressBar()

        let parameters : [String: String] = ["string" : searchString as String]

        AppTheme().callGetService(String(format: "%@getaddresslist", AppLinkTaxi), param: parameters as [String : AnyObject]) { (result, data) -> Void in
            
            if result == "success" {
                
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)
                self.arrOfResponseKey.removeAllObjects()
                
                if let array_response : NSArray = respDict["response"] as? NSArray{
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }
                print("111111 === \(self.arrOfResponseKey)")

                self.tbl_search_user.reloadData()

            } else {
                self.loading.dismiss()
                self.arrOfResponseKey.removeAllObjects()
                self.tbl_search_user.reloadData()
                _ = self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "Record not found", showInView: self.view)
            }
        }
    }
    
    // Drop off location API Call

    func dropoffLocation_ApiCall() {

        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
      
        startProgressBar()
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(strPickUpID, forKey:"pickup")

        AppTheme().callPostService(String(format: "%@getdrop", AppLinkTaxi), param: paraDict) {(result, data) -> Void in

            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                print(respDict)

                if let array_response : NSArray = respDict["response"] as? NSArray {
                    self.arrOfResponseKey = array_response.mutableCopy() as! NSMutableArray
                }
                self.tbl_search_user.reloadData()
            } else{
                self.loading.dismiss()
            }
        }
    }

    //==============================
    // MARK: Loader method
    //==============================
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    //=====================================
    // MARK : UISearchBar delegate methods
    //=====================================
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
       // isSearch = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.loading.dismiss()

       // isSearch = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.loading.dismiss()

        getLocationList(searchString: searchText)
    }
}


//========================================================
// MARK: UITableview Datasource and delegate methods
//========================================================

extension BookLaterSearchVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
      return arrOfResponseKey.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.loading.dismiss()
        
        var cell : SearchCell! = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
        if cell == nil {
            cell = SearchCell(style: UITableViewCellStyle.default, reuseIdentifier: "SearchCell")
        }
         var dic: NSDictionary = NSDictionary()
         dic = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        
        if let str_user_full_name : String = dic["loc_name"] as? String{
            cell.lblAddress.text = str_user_full_name
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dicAddress = self.arrOfResponseKey.object(at: indexPath.row) as! NSDictionary
        if isFromPickup {
            self.selecteAddressDone(dicAddress)
        } else {
            self.selectDropOffAddress(dicAddress)
        }
        self.navigationController?.popViewController(animated: true)
 
    }
}
