//
//  ChooseCabVC.swift
//  Gupa Network
//
//  Created by Priyanka on 08/01/18.
//  Copyright © 2018 Consagous. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class TaxiBookingsVC: UIViewController , MKMapViewDelegate , CLLocationManagerDelegate {

    //****************************************************
    // MARK: - Properties
    //****************************************************
 
    @IBOutlet var mapView       : MKMapView!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    fileprivate var annotation: MKAnnotation!
    var locationManager: CLLocationManager = CLLocationManager()
    
    //****************************************************
    // MARK: - Life Cycle Methods≈
    //****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        appDelegate.centerNav = self.navigationController
        checkLocationService()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        setNavigationController()
    }
    
     override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
     func setNavigationController() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func checkLocationService() {
    
        if ReachabilityNetwork.isLocationServiceEnabled() == true {
            
            if (CLLocationManager.locationServicesEnabled()) {
                if  locationManager == nil {
                    locationManager = CLLocationManager()
                }
                locationManager.requestWhenInUseAuthorization()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestAlwaysAuthorization()
                locationManager.startUpdatingLocation()
            }
        } else {
            
            let alertVC = UIAlertController(title: "Location Services Disabled", message: "Please enable location services for booking.", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default){ __ in
                
                if let aString = URL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(aString)
                }
            }
            alertVC .addAction(actionOk)
            self.present(alertVC, animated: true, completion: nil)
         
        }
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func btnBookNow_Action(_ sender: UIButton) {
        
        let newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.taxiVCStoryboard, viewControllerName: PickupAndDropLocationVC.nameOfClass) as PickupAndDropLocationVC
        self.navigationController?.pushViewController(newGroupViewController, animated: true)
    }
    
    @IBAction func btnBookLater_Action(_ sender: UIButton) {
        let newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.taxiVCStoryboard, viewControllerName: BookLaterVC.nameOfClass) as BookLaterVC
        self.navigationController?.pushViewController(newGroupViewController, animated: true)
    }
    
    @IBAction func btnUpcoming_Action(_ sender: UIButton) {
        
        let myTrips = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.taxiVCStoryboard, viewControllerName: MyTripsVC.nameOfClass) as MyTripsVC
        self.navigationController?.pushViewController(myTrips, animated: true)
    }
    
    //****************************************************
    // MARK: - CLLocationManagerDelegate Methods
    //****************************************************
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.mapView.setRegion(region, animated: true)
        
        if self.mapView.annotations.count != 0 {
            annotation = self.mapView.annotations[0]
            self.mapView.removeAnnotation(annotation)
        }
        
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = location!.coordinate
        pointAnnotation.title = "Current location"
        
        if location!.coordinate.latitude != nil ||  location!.coordinate.longitude != nil {
            
            CurrentLocationManager.sharedInstance.currentCoordinate.latitude = location!.coordinate.latitude
            CurrentLocationManager.sharedInstance.currentCoordinate.longitude = location!.coordinate.longitude
        }
        
        // print("111 === \( CurrentLocationManager.sharedInstance.currentCoordinate.latitude)")
        //   print("222 === \(CurrentLocationManager.sharedInstance.currentCoordinate.longitude)")
        print(location!.coordinate.latitude)
        print(location!.coordinate.longitude)
        
        locationManager.stopUpdatingLocation()
        mapView.addAnnotation(pointAnnotation)
    }
    
}

open class ReachabilityNetwork {
    
    class func isLocationServiceEnabled() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            default:
                print("Something wrong with Location services")
                return false
            }
        } else {
            print("Location services are not enabled")
            return false
        }
    }
}
