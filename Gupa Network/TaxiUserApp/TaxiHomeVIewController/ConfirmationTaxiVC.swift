//
//  ConfirmationTaxiVC.swift
//  Gupa Network
//
//  Created by Apple on 17/01/18.
//  Copyright © 2018 mobiweb. All rights reserved.
//

import UIKit
import JGProgressHUD

class ConfirmationTaxiVC: UIViewController {
    
    //****************************************************
    // MARK: - Properties
    //****************************************************
    
    @IBOutlet var lblDriverName     : UILabel!
    @IBOutlet var lblCarName        : UILabel!
    @IBOutlet var imgDriver         : UIImageView!
    @IBOutlet var lblDate     : UILabel!
    @IBOutlet var lblPickUp     : UILabel!
    @IBOutlet var lblDropOff     : UILabel!

    //****************************************************
    // MARK: - Initialization
    //****************************************************
    
    var loading : JGProgressHUD! = JGProgressHUD()
    var str_SelectedBookinID = ""
    var str_BookingDateAndTime = ""

    var pickUpDict :  NSDictionary = [:]
    var dropOffDict: NSDictionary = [:]
    var driverDict :  NSDictionary = [:]
    
    //****************************************************
    // MARK: - View Life Cycle
    //****************************************************
   
     override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationController()
        appDelegate.centerNav = self.navigationController
        
        driverDetail()
    }  
    
    func driverDetail() {
        
        lblDriverName.text =  (driverDict["name"] as? String)!
        lblCarName.text   =  (driverDict["taxi_type_name"] as? String)!
        lblDate.text! = str_BookingDateAndTime
        
        if let str_profile_pic : String =  driverDict["profile_pic"] as? String {
            let url : URL = URL(string: str_profile_pic)!
            self.imgDriver.kf.setImage(with: url, placeholder: UIImage(named: "driver-list_icon"))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //****************************************************
    // MARK: - Private Methods
    //****************************************************
    
    func setNavigationController() {
        
        self.navigationItem.setHidesBackButton(true, animated: false)

        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14.0/255.0, green: 158.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func startProgressBar()
    {
        self.loading = JGProgressHUD(style: .dark)
        self.loading.textLabel.text = "Loading.."
        self.loading.show(in: self.view)
    }
    
    func cancalBooking_ApiCall() {
        
        if (!isNetworkAvailable) {
            Utility.showNetWorkAlert()
            return
        }
        
        startProgressBar()
        
        let paraDict = NSMutableDictionary()
        paraDict.setValue(str_SelectedBookinID as AnyObject, forKey:"booking_id")
        
        AppTheme().callPostService(String(format: "%@canceltrip", AppLinkTaxi), param: paraDict) {(result, data) -> Void in
            
            if result == "success"
            {
                self.loading.dismiss()
                let respDict = data as! NSDictionary
                
                appDelegate.dictTaxiPassenger = NSMutableDictionary()
                
                let alertVC = UIAlertController(title: "Booking cancelled sucessfully!", message: "", preferredStyle: .alert)
                
                let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
                    let newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.storyboardMain, viewControllerName: TaxiBookingsVC.nameOfClass) as TaxiBookingsVC
                    self.navigationController?.pushViewController(newGroupViewController, animated: true)
                }
                
                alertVC.addAction(actionOk)
                self.present(alertVC, animated: true, completion: nil)
                print(respDict)
                
            } else {
                self.loading.dismiss()
            }
        }
    }
    
    //****************************************************
    // MARK: - Action Methods
    //****************************************************
    
    @IBAction func btnConfirm_Action(_ sender: UIButton) {
        
        let newGroupViewController = BaseApp.sharedInstance.getViewController(storyboardName: TaxiAppConstant.storyboardMain, viewControllerName: TaxiBookingsVC.nameOfClass) as TaxiBookingsVC
        self.navigationController?.pushViewController(newGroupViewController, animated: true)
    }
    
    @IBAction func btnBack_Action(_: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancelTrip_Action(_ sender: UIButton) {
        
        let alertVC = UIAlertController(title: "Alert", message: "Are you sure you want to cancel your trip?", preferredStyle: .alert)
        
        let actionOk = UIAlertAction(title: "Ok", style: .default){ __ in
            self.cancalBooking_ApiCall()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default){ __ in
        }
        
        alertVC.addAction(cancel)
        alertVC.addAction(actionOk)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    
}
