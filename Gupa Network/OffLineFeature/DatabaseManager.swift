//
//  DatabaseManager.swift
//  Gupa Network
//
//  Created by Apple on 12/04/18.
//  Copyright © 2018 Consagous. All rights reserved.
//

import UIKit

class DatabaseManager: NSObject {
    
    let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    
    static let sharedDBInterface : DatabaseManager = {
        let instance = DatabaseManager()
        return instance
    }()
    
    //MARK: - Basic Init Method
    override init() {
        super.init()
        initDatabaseManager()
    }
    
    func initDatabaseManager(){
        
        let bundlePath = Bundle.main.path(forResource: "GupaClient", ofType: "db")
        print("Bundle Path is \(String(describing: bundlePath))")
        
        let fileURL = documents.appendingPathComponent("GupaClient.db")
        print("File Url is \(fileURL.path)")
        
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: fileURL.path) {
            do {
                try fileManager.copyItem(atPath: bundlePath!, toPath: fileURL.path)
                print("copied ")
                initializeAllTablesWithDatabase()
                
            }catch{
                print("\n")
                print(error)
            }
        } else {
            print("file exists")
            initializeAllTablesWithDatabase()
            
        }
    }
}

//MARK: Database Schema Changes & Related Methods
extension DatabaseManager{
    
    func initializeAllTablesWithDatabase(){
        
        createDatabaseAndTable(DBConstants.DATABASE_NAME, tableName: DBConstants.DBTableName.TABLE_POST, columns: DBConstants.DBColumnName.KEY_API_URL+" TEXT,"+DBConstants.DBColumnName.KEY_API_URL_TYPE+" TEXT,"+DBConstants.DBColumnName.KEY_PARAM_DATA+" TEXT,"+DBConstants.DBColumnName.KEY_TYPE+" TEXT,"+DBConstants.DBColumnName.KEY_FILE_PATH+" TEXT")
        
        createDatabaseAndTable(DBConstants.DATABASE_NAME, tableName: DBConstants.DBTableName.TABLE_REVIEW, columns: DBConstants.DBColumnName.KEY_API_URL+" TEXT,"+DBConstants.DBColumnName.KEY_API_URL_TYPE+" TEXT,"+DBConstants.DBColumnName.KEY_PARAM_DATA+" TEXT,"+DBConstants.DBColumnName.KEY_TYPE+" TEXT,"+DBConstants.DBColumnName.KEY_FILE_PATH+" TEXT")
                
    }
    
    //MARK: Create Database and Related Tables
    func createDatabaseAndTable(_ dbName:String,tableName:String,columns:String) {
        
        let fileURL = documents.appendingPathComponent(dbName)
        print("File URL is \(fileURL)")
        let database = SQDatabase.init(path: fileURL.path)
        if !database.open() {
            print("Unable to open database : \(dbName)")
            return
        }
        else {
            let tableColumns = DBConstants.DBColumnName.KEY_SERIAL_NO + " INTEGER PRIMARY KEY," + columns
            let query = "CREATE TABLE if not exists \(tableName) (\(tableColumns))"
            //print("Query is \(query)")
            if database.update(query){
                print("Execution Successful.")
            } else {
                self.executionFailForQuery(query)
            }
            
        }
        database.close()
    }
    
    func executionFailForQuery(_ query:String) {
        print("Unable to execute query : ",query)
    }
    
    //MARK: Delete unsed data from all tables
    func deleteRecordsFrom(tableName:String){
        let fileURL = documents.appendingPathComponent(DBConstants.DATABASE_NAME)
        let database = SQDatabase(path: fileURL.path)
        if !database.open(){
            print("Unable to open database")
            return
        }else {
            defer{database.close()}
            let query="DELETE FROM "+tableName
            if database.update(query) {
                print("Row Deletion execute successfully")
            }else{
                self.executionFailForQuery(""+tableName)
            }
        }
    }
    
    
    //MARK: Add POST Value To Database
    func addCategoryValuesToDatabase(postDetails : PostDetail){
        
        let fileURL = documents.appendingPathComponent(DBConstants.DATABASE_NAME)
        //print("File URL \(fileURL)")
        let database = SQDatabase.init(path: fileURL.path)
        if database.open(){
            defer {database.close()}
            
            let query = "INSERT INTO "+DBConstants.DBTableName.TABLE_POST+"("+DBConstants.DBColumnName.KEY_API_URL+","+DBConstants.DBColumnName.KEY_API_URL_TYPE+","+DBConstants.DBColumnName.KEY_PARAM_DATA+","+DBConstants.DBColumnName.KEY_TYPE+","+DBConstants.DBColumnName.KEY_FILE_PATH+")"+" VALUES (?,?,?,?,?)"
            
            if database.update(query, withObjects: [String(postDetails.appUrl)
                ,String(postDetails.appUrlType),String(postDetails.paramData),String(postDetails.imageVideoType),String(describing: postDetails.imageFilePath)])
            {
                print("query execute successfully")
            } else {
                self.executionFailForQuery(query+" "+postDetails.appUrl)
            }
        } else{
            print("Unable to open Database")
        }
    }
    
    
    
    //MARK: Get Account Details
    func getPost() -> [PostDetail]{
        
        var arrResult = [PostDetail]()
        let fileURL = documents.appendingPathComponent(DBConstants.DATABASE_NAME)
        let database = SQDatabase(path: fileURL.path)
        if !database.open(){
            print("Unable to open database")
            return (arrResult)
        }
        else {
            defer{database.close()}
            
            let query="SELECT * FROM "+DBConstants.DBTableName.TABLE_POST
            if let rs = database.query(query){
                while rs.next()
                {
                    guard let postId = rs.intForColumnIndex(0)
                        else
                    {
                        continue
                    }
                    
                    guard let appUrl = rs.stringForColumnIndex(1)
                        else
                    {
                        continue
                    }
                    
                    guard let appUrlType = rs.stringForColumnIndex(2)
                        else
                    {
                        continue
                    }
                    
                    guard let paramData = rs.stringForColumnIndex(3)
                        else
                    {
                        continue
                    }
                    guard let imageVideoType = rs.stringForColumnIndex(4)
                        else
                    {
                        continue
                    }
                    
                    guard let imageFilePath = rs.stringForColumnIndex(5)
                        else
                    {
                        continue
                    }
                    
                    arrResult.append(PostDetail(postIntId: postId , strAppUrl : appUrl ,strAppUrlType : appUrlType , strParamData : paramData , strImageVideoType : imageVideoType , strImageFilePath : imageFilePath))
                    print("ArrrResult \(arrResult)")
                }
                
            } else {
                self.executionFailForQuery(query)
            }
            return arrResult
        }
    }
    
    //MARK: Delete Records according to id basis
    func deleteItemNameFrom(tableName : String, id:Int){
        print("Cat Id is \(id)")
        let fileURL = documents.appendingPathComponent(DBConstants.DATABASE_NAME)
        print("File URL \(fileURL)")
        let database = SQDatabase.init(path: fileURL.path)
        if !database.open(){print("Unable to open database")} else {
            
            let query = "DELETE FROM "+tableName+" WHERE id = ?"
            
            if database.update(query, withObjects: [id]) {
                print("Query execute successfully")
            }
            else{
                self.executionFailForQuery("Failed in update Category ... \(id) ")
            }
        }
    }
    
    //MARK: Add Review Value To Database
    func addReviewToDatabase(review : Review){
        
        let fileURL = documents.appendingPathComponent(DBConstants.DATABASE_NAME)
        //print("File URL \(fileURL)")
        let database = SQDatabase.init(path: fileURL.path)
        if database.open(){
            defer {database.close()}
            
            let query = "INSERT INTO "+DBConstants.DBTableName.TABLE_REVIEW+"("+DBConstants.DBColumnName.KEY_API_URL+","+DBConstants.DBColumnName.KEY_API_URL_TYPE+","+DBConstants.DBColumnName.KEY_PARAM_DATA+")"+" VALUES (?,?,?)"
            
            if database.update(query, withObjects: [String(review.appUrl)
                ,String(review.appUrlType),String(review.paramData)])
            {
                print("query execute successfully")
            } else {
                self.executionFailForQuery(query+" "+review.appUrl)
            }
        } else{
            print("Unable to open Database")
        }
    }
    
    
    
    //MARK: Get Account Details
    func getReviews() -> [Review]{
        
        var arrResult = [Review]()
        let fileURL = documents.appendingPathComponent(DBConstants.DATABASE_NAME)
        let database = SQDatabase(path: fileURL.path)
        if !database.open(){
            print("Unable to open database")
            return (arrResult)
        }
        else {
            defer{database.close()}
            
            let query="SELECT * FROM "+DBConstants.DBTableName.TABLE_REVIEW
            if let rs = database.query(query){
                while rs.next()
                {
                    guard let postId = rs.intForColumnIndex(0)
                        else
                    {
                        continue
                    }
                    
                    guard let appUrl = rs.stringForColumnIndex(1)
                        else
                    {
                        continue
                    }
                    
                    guard let appUrlType = rs.stringForColumnIndex(2)
                        else
                    {
                        continue
                    }
                    
                    guard let paramData = rs.stringForColumnIndex(3)
                        else
                    {
                        continue
                    }
                    
                    arrResult.append(Review(postIntId: postId , strAppUrl : appUrl ,strAppUrlType : appUrlType , strParamData : paramData))
                    print("ArrrResult \(arrResult)")
                }
                
            } else {
                self.executionFailForQuery(query)
            }
            return arrResult
        }
    }
    

    
}

