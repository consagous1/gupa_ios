//
//  StructFiles.swift
//  MoneyManger
//
//  Created by Apple on 11/17/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import Foundation

struct PostDetail {
    
    var postId : Int
    var appUrl : String
    var appUrlType : String
    var paramData : String
    var imageVideoType : String
    var imageFilePath : String
    
    init(postIntId: Int , strAppUrl : String , strAppUrlType :String = "" , strParamData:String ,strImageVideoType: String , strImageFilePath: String) {
        
        self.postId = postIntId
        self.appUrl = strAppUrl
        self.appUrlType = strAppUrlType
        self.paramData = strParamData
        self.imageVideoType = strImageVideoType
        self.imageFilePath = strImageFilePath
    }
}


