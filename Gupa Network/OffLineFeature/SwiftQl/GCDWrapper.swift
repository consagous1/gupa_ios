//
//  DVGCD.swift
//  XPlayerTV
//
//  Created by Bhavesh Tiwari on 22/01/16.
//  Copyright © 2016 DigiValet. All rights reserved.
//

import Foundation

protocol ExcutableQueue {
    var queue: DispatchQueue { get }
}

extension ExcutableQueue {
    func execute(_ closure: @escaping () -> Void) {
        queue.async(execute: closure)
    }
}

enum Queue: ExcutableQueue {
    case main
    case userInteractive
    case userInitiated
    case utility
    case background
    case `default`
    
    var queue: DispatchQueue {
        switch self {
        case .main:
            return DispatchQueue.main
        case .userInteractive:
            return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
        case .userInitiated:
            return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated)
        case .utility:
            return DispatchQueue.global(qos: DispatchQoS.QoSClass.utility)
        case .background:
            return DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
        case .default:
            return DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
        }
    }
}

enum SerialQueue: String, ExcutableQueue {
    case DownLoadImage = "myApp.SerialQueue.DownLoadImage"
    case UpLoadFile = "myApp.SerialQueue.UpLoadFile"
    
    var queue: DispatchQueue {
        return DispatchQueue(label: rawValue, attributes: [])
    }
}
