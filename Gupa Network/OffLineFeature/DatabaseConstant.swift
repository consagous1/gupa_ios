//
//  DatabaseConstant.swift
//  MoneyManger
//
//  Created by Apple on 11/20/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import Foundation

struct DBConstants {

    //Database Constant
    static let DATABASE_NAME = "GupaClient.db"
    static let DATABASE_VERSION = 1.0;

    //Table Name
    struct DBTableName {
        static let TABLE_POST = "tbl_post"
        static let TABLE_REVIEW = "tbl_Review"

    }
    
    // Contacts Table Columns names
    struct DBColumnName {
        static let KEY_SERIAL_NO = "id"
        static let KEY_API_URL = "App_url"
        static let KEY_API_URL_TYPE = "App_Url_Type"
        static let KEY_PARAM_DATA = "Param_Data"
        static let KEY_TYPE = "Image_Video_type"
        static let KEY_FILE_PATH = "Image_Video_Path"
    }
}
