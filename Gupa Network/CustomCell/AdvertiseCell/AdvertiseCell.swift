//
//  AdvertiseCell.swift
//  Gupa Network
//
//  Created by mac on 29/07/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class AdvertiseCell: UITableViewCell {

    @IBOutlet weak var lblOffer: UILabel!
    @IBOutlet weak var lbl_dis: UILabel!
    @IBOutlet weak var imgAdd: UIImageView!
    @IBOutlet weak var view_video: UIView!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var btn_more: UIButton!
    @IBOutlet weak var img_profil_pic: UIImageView!
    
    @IBOutlet weak var btn_image: UIButton!
    @IBOutlet weak var btn_video: UIButton!
//    var btn_image: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.img_profil_pic.layer.cornerRadius = 20 //imgUser.frame.height/2.0
        self.img_profil_pic.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
