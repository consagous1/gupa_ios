//
//  FlightCell.swift
//  Gupa Network
//
//  Created by mac on 5/21/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class FlightCell: UITableViewCell {

    @IBOutlet var imgTitle: UIImageView!
    @IBOutlet var lblFlightName: UILabel!
    @IBOutlet var lblFlighNumber: UILabel!
    @IBOutlet var lblPassengerStatus: UILabel!
    @IBOutlet var lblSourceStation: UILabel!
    @IBOutlet var lblDestStation: UILabel!
    @IBOutlet var lblSourceTime: UILabel!
    @IBOutlet var lblDestTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
