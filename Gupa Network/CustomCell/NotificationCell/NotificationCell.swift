//
//  NotificationCell.swift
//  Gupa Network
//
//  Created by MWM23 on 12/3/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var lbl_post_type: UILabel!
    @IBOutlet weak var lbl_noti_time: UILabel!
    @IBOutlet weak var lbl_noti_msg: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.img_user.layer.cornerRadius = 15 //self.img_user.frame.height/2
        self.img_user.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
