//
//  TravelActivityTableCell.swift
//  Gupa Network
//
//  Created by MWM23 on 8/10/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class TravelActivityTableCell: UITableViewCell {
    @IBOutlet weak var viewForFlightDetail: UIView!
    @IBOutlet weak var lblFlightNameNumber: UILabel!
    @IBOutlet weak var lblFlightTime: UILabel!
    
    @IBOutlet weak var lblFlightDepartureCityName: UILabel!
    @IBOutlet weak var lblFlightArrivalCityName: UILabel!
    
    @IBOutlet weak var lblFlightDate: UILabel!
    
    @IBOutlet weak var viewForFlightStations: UIView!
    @IBOutlet weak var lblSorceStationNameCode: UILabel!
    @IBOutlet weak var lblDestinationStationNameCode: UILabel!
    
    @IBOutlet weak var viewForFlightActualTime: UIView!
    @IBOutlet weak var lblActualTimeIn: UILabel!
    @IBOutlet weak var lblActualTimeOut: UILabel!
    @IBOutlet weak var lblFlightStatus: UILabel!
    
    @IBOutlet weak var viewForFlightScheduleTime: UIView!
    @IBOutlet weak var lblScheduleTimeIn: UILabel!
    @IBOutlet weak var lblScheduleTimeOut: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func setBgEnable(enable : Bool) {
//        if enable {
//            bgImageView.image = UIImage(named: "Header.png")
//            bgImageView.hidden = false
//            iconView.hidden = false
//            storeButton.hidden = true
//            titleLabel.hidden = false
//            addressLabel.hidden = false
//            self.selectionStyle = UITableViewCellSelectionStyle.Default
//        }else{
//            bgImageView.image = nil
//            bgImageView.hidden = true
//            iconView.hidden = true
//            storeButton.hidden = true
//            titleLabel.hidden = true
//            addressLabel.hidden = true
//            self.selectionStyle = UITableViewCellSelectionStyle.None
//        }
//    }
    
}
