//
//  WriteStatusCell.swift
//  Gupa Network
//
//  Created by mac on 6/17/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class WriteStatusCell: UITableViewCell {

    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgSmileStatus: UIImageView!
    @IBOutlet weak var txtWriteStatus: UITextView!
    @IBOutlet weak var lblStatus: KILabel!
    @IBOutlet weak var imgAdd: UIImageView!
    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTotalLike: UILabel!
    @IBOutlet weak var lblTotalComments: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnUserFollowUnfollow: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var btnCommentOnOff: UIButton!
    @IBOutlet weak var btn_image_post: UIButton!
    @IBOutlet weak var btn_video_post: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgUser.layer.cornerRadius = 17 //imgUser.frame.height/2.0
        self.imgUser.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCellText(_ text:String){
        lblStatus.text = text
    }
}
