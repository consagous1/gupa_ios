//
//  FlightRatingCell.swift
//  Gupa Network
//
//  Created by mac on 6/2/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class FlightRatingCell: UITableViewCell {

    @IBOutlet var imgTitle: UIImageView!
    @IBOutlet var lblFlightName: UILabel!
    @IBOutlet var lblFlighNumber: UILabel!
    @IBOutlet var lblFlightime: UILabel!
    @IBOutlet var lblAirlinesName: UILabel!
    @IBOutlet var lblSourceStation: UILabel!
    @IBOutlet var lblDestStation: UILabel!
    @IBOutlet var lblGoodRating: UILabel!
    @IBOutlet var lblBadRating: UILabel!
    @IBOutlet var lblUglyRating: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgTitle.layer.cornerRadius = 22.5 //self.imgTitle.frame.height/2
        self.imgTitle.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
