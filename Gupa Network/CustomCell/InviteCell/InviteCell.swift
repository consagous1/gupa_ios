//
//  InviteCell.swift
//  Gupa Network
//
//  Created by MWM23 on 11/28/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class InviteCell: UITableViewCell {

    
    @IBOutlet weak var button_invite: UIButton!
    @IBOutlet weak var image_static: UIImageView!
    @IBOutlet weak var label_name: UILabel!
    @IBOutlet weak var label_email: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.image_static.layer.cornerRadius = 20 //self.image_static.frame.height/2
        self.image_static.clipsToBounds = true
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
