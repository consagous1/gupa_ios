//
//  GuidePendingListCell.swift
//  Gupa Network
//
//  Created by Apple on 23/12/17.
//  Copyright © 2017 mobiweb. All rights reserved.
//

import UIKit

class GuidePendingListCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet var lblName         : UILabel!
    @IBOutlet var imgUser         : UIImageView!
    @IBOutlet var btnCancel       : UIButton!
    @IBOutlet var btnAccept       : UIButton!
    @IBOutlet var constAcceptWidth  : NSLayoutConstraint!
    @IBOutlet var constDeclineWidth  : NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgUser.layer.cornerRadius = 17
        self.imgUser.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
