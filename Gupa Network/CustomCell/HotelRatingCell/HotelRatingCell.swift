//
//  HotelRatingCell.swift
//  Gupa Network
//
//  Created by mac on 6/4/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class HotelRatingCell: UITableViewCell {

    @IBOutlet weak var imgHotel: UIImageView!
    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblHotelAddress: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblHotelStarRating: UILabel!
    @IBOutlet var lblGoodRating: UILabel!
    @IBOutlet var lblBadRating: UILabel!
    @IBOutlet var lblUglyRating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgHotel.layer.cornerRadius = 25 // self.imgHotel.frame.height/2
        self.imgHotel.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
