//
//  ChatTableViewCell.swift
//  Gupa Network
//
//  Created by MWM23 on 10/4/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var image_HisHer: UIImageView!
    @IBOutlet weak var image_My: UIImageView!
    @IBOutlet weak var label_MyTime: UILabel!
    @IBOutlet weak var label_HisHerTime: UILabel!
    @IBOutlet weak var textview_Msg: UITextView!
     @IBOutlet weak var view_textview_Msg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        image_My.layer.cornerRadius = 17
        image_My.layer.masksToBounds = true
        
        image_HisHer.layer.cornerRadius = 17
        image_HisHer.layer.masksToBounds = true
        
        view_textview_Msg.layer.cornerRadius = 3
        view_textview_Msg.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
