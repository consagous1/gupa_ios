//
//  PrivacySettingsCell.swift
//  Gupa Network
//
//  Created by Apple on 20/12/17.
//  Copyright © 2017 mobiweb. All rights reserved.
//

import UIKit

class PrivacySettingsCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet var lblTitle    : UILabel!
    @IBOutlet var lblSubtitle : UILabel!
    @IBOutlet var switchOnOff : UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
