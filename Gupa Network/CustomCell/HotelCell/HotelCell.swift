//
//  HotelCell.swift
//  Gupa Network
//
//  Created by mac on 5/21/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class HotelCell: UITableViewCell {
    
    @IBOutlet var  lblHotelName : UILabel!
    @IBOutlet var imgHotelLogo : UIImageView!
    @IBOutlet var lblAddress : UILabel!
    @IBOutlet var lblDiscreption : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgHotelLogo.layer.cornerRadius =  25 // self.imgHotelLogo.frame.height/2
        self.imgHotelLogo.layer.masksToBounds = true
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
