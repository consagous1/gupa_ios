//
//  TransportCell.swift
//  Gupa Network
//
//  Created by MWM23 on 12/6/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class TransportCell: UITableViewCell {
    @IBOutlet weak var img_transport: UIImageView!

    @IBOutlet weak var lbl_trans_title: UILabel!
    @IBOutlet weak var lbl_trans_subtitle: UILabel!
    
    @IBOutlet weak var txtCall: UITextView!
    @IBOutlet weak var viewArriDepart: UIView!
    @IBOutlet weak var lbl_trans_depart_city: UILabel!
    @IBOutlet weak var lbl_trans_depart_time: UILabel!
    @IBOutlet weak var lbl_trans_arrival_city: UILabel!
    @IBOutlet weak var lbl_trans_arrival_time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        img_transport.layer.cornerRadius = 15
        img_transport.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
