//
//  HotelImageCell.swift
//  Gupa Network
//
//  Created by MWM23 on 8/9/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class HotelImageCell: UICollectionViewCell {
   @IBOutlet weak var imgHotel: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgHotel.layer.cornerRadius = self.imgHotel.frame.height/2
        self.imgHotel.clipsToBounds = true
    }
}
