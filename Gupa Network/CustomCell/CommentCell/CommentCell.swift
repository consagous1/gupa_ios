//
//  CommentCell.swift
//  Gupa Network
//
//  Created by MWM23 on 11/28/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserComments: UILabel!
    @IBOutlet weak var lblSubName: UILabel!
    @IBOutlet weak var btnUserDwtail: UIButton!
    
    @IBOutlet weak var img_rating: UIImageView!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_post: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.imgUser.layer.cornerRadius = 17
//        self.imgUser.layer.masksToBounds = true
//        // Initialization code
//        img_rating.layer.cornerRadius = 7.5
//        self.img_rating.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
