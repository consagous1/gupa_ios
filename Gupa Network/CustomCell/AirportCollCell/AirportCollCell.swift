//
//  AirportCollCell.swift
//  Gupa Network
//
//  Created by MWM23 on 9/20/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class AirportCollCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCollectionCell: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
}
