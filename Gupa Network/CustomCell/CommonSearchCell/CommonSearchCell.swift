//
//  CommonSearchCell.swift
//  Gupa Network
//
//  Created by MWM23 on 9/22/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class CommonSearchCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
