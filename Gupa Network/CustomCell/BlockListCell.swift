//
//  BlockListCell.swift
//  Gupa Network
//
//  Created by Apple on 20/12/17.
//  Copyright © 2017 mobiweb. All rights reserved.
//

import UIKit

class BlockListCell: UITableViewCell {

    // MARK: - Properties
    @IBOutlet var lblName         : UILabel!
    @IBOutlet var imgUser         : UIImageView!
    @IBOutlet var btnBlockUnblock : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgUser.layer.cornerRadius = 20
        self.imgUser.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
