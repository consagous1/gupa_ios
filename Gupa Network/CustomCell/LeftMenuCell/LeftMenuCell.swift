//
//  LeftMenuCell.swift
//  Gupa Network
//
//  Created by mac on 5/17/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class LeftMenuCell: UITableViewCell {
    @IBOutlet var lblMenuItem: UILabel!
    @IBOutlet var imgMenuImage: UIImageView!
    @IBOutlet weak var lbl_noti_count: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
