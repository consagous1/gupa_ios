//
//  SJAutoCompleteTextField.swift
//  SJAutoCompleteTextField
//
//  Created by Mac on 9/16/16.
//  Copyright © 2016 Sumit Jagdev. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


open class SJAutoCompleteTextField: UIView {

    fileprivate var responseData:NSMutableData?
    fileprivate var dataTask:URLSessionDataTask?
    
    fileprivate let googleMapsKey = "AIzaSyDg2tlPcoqxx2Q2rfjhsAKS-9j0n3JA_a4"
    fileprivate let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    
    @IBOutlet var textField : UITextField!
    
    
    /// Manages the instance of tableview
    @IBOutlet var resultTableView : UITableView!
    
    var containerView : UIView?
    var customConstraints : NSMutableArray?
    
    /// Holds the collection of attributed strings
    fileprivate lazy var attributedAutoCompleteStrings = [NSAttributedString]()
    /// Handles user selection action on autocomplete table view
    open var onSelect:(String, IndexPath)->() = {_,_ in}
    /// Handles textfield's textchanged
    open var onTextChange:(String)->() = {_ in}
    
    /// Font for the text suggestions
    open var autoCompleteTextFont = UIFont.systemFont(ofSize: 12)
    /// Color of the text suggestions
    open var autoCompleteTextColor = UIColor.black
    /// Used to set the height of cell for each suggestions
    open var autoCompleteCellHeight:CGFloat = 35.0
    /// The maximum visible suggestion
    open var maximumAutoCompleteCount = 3
    /// Used to set your own preferred separator inset
    open var autoCompleteSeparatorInset = UIEdgeInsets.zero
    /// Shows autocomplete text with formatting
    open var enableAttributedText = false
    /// User Defined Attributes
    open var autoCompleteAttributes:[NSAttributedStringKey : Any]?
    /// Hides autocomplete tableview after selecting a suggestion
    open var hidesWhenSelected = true
    /// Hides autocomplete tableview when the textfield is empty
    open var hidesWhenEmpty:Bool?{
        didSet{
            assert(hidesWhenEmpty != nil, "hideWhenEmpty cannot be set to nil")
            resultTableView?.isHidden = hidesWhenEmpty!
        }
    }

    /// The strings to be shown on as suggestions, setting the value of this automatically reload the tableview
    open var autoCompleteStrings:[String]?{
        didSet{ reload() }
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customConstraints = NSMutableArray()
        var view : UIView;
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SJAutoCompleteTextField", bundle: bundle)
        view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        containerView = view;
        view.translatesAutoresizingMaskIntoConstraints = false
        self .addSubview(view)
        
        self.layer.cornerRadius = 5;
        self.clipsToBounds = true
        self.setNeedsUpdateConstraints()
        
        self.hidesWhenEmpty = true
        
        self.textField.addTarget(self, action: #selector(SJAutoCompleteTextField.textFieldDidChange), for: .editingChanged)
        self.textField.addTarget(self, action: #selector(SJAutoCompleteTextField.textFieldDidEndEditing), for: .editingDidEnd)
//        self.resultTableView.layer.borderWidth = 1.0
//        self.resultTableView.layer.borderColor = UIColor.blackColor().CGColor
        
    }
    override open func updateConstraints() {
        for const in customConstraints! {
            self.removeConstraint(const as! NSLayoutConstraint)
        }
        customConstraints?.removeAllObjects()
        let view = containerView;
        let views: NSDictionary = ["view":view!]
        
        customConstraints?.addObjects(from: NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views as! [String : AnyObject]))
        customConstraints?.addObjects(from: NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views as! [String : AnyObject]))
        
        for const in customConstraints! {
            self.addConstraint(const as! NSLayoutConstraint)
        }
        super.updateConstraints()
    }
    
    //MARK: - Private Methods
    fileprivate func reload(){
        if enableAttributedText{
            let attrs = [NSAttributedStringKey.foregroundColor.rawValue:autoCompleteTextColor, NSAttributedStringKey.font:autoCompleteTextFont] as! [NSAttributedStringKey : Any]
            
            if attributedAutoCompleteStrings.count > 0 {
                attributedAutoCompleteStrings.removeAll(keepingCapacity: false)
            }
            
            if let autoCompleteStrings = autoCompleteStrings, let autoCompleteAttributes = autoCompleteAttributes {
                for i in 0..<autoCompleteStrings.count{
                    let str = autoCompleteStrings[i] as NSString
                    let range = str.range(of: textField.text!, options: .caseInsensitive)
                    let attString = NSMutableAttributedString(string: autoCompleteStrings[i], attributes: attrs)
                    attString.addAttributes(autoCompleteAttributes, range: range)
                   
                    attributedAutoCompleteStrings.append(attString)
                }
            }
        }
        resultTableView?.reloadData()
    }
    
    
     @objc func textFieldDidChange(){
        guard let _ = textField.text else {
            return
        }
        
        onTextChange(textField.text!)
        if textField.text!.isEmpty{ autoCompleteStrings = nil }
        DispatchQueue.main.async(execute: { () -> Void in
            self.resultTableView.isHidden =  self.hidesWhenEmpty! ? self.textField.text!.isEmpty : false
        })
    }
    
    @objc func textFieldDidEndEditing() {
        resultTableView?.isHidden = true
    }
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var constraint : NSLayoutConstraint?
        for con in self.constraints {
            if con.identifier?.isEmpty == false {
                if con.identifier!.contains("search") == true {
                    constraint = con
                }
            }
            
        }
        if (constraint != nil) {
            if autoCompleteStrings?.count > 0 {
                constraint?.constant = 150
                resultTableView.isHidden = false
            }else{
                constraint?.constant = 40
                resultTableView.isHidden = true
            }
        }
        
        return autoCompleteStrings != nil ? (autoCompleteStrings!.count > maximumAutoCompleteCount ? maximumAutoCompleteCount : autoCompleteStrings!.count) : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "autocompleteCellIdentifier"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil{
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        
        if enableAttributedText{
            cell?.textLabel?.attributedText = attributedAutoCompleteStrings[indexPath.row]
        }
        else{
            cell?.textLabel?.font = autoCompleteTextFont
            cell?.textLabel?.textColor = autoCompleteTextColor
            cell?.textLabel?.text = autoCompleteStrings![indexPath.row]
        }
        
        cell?.contentView.gestureRecognizers = nil
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        var constraint : NSLayoutConstraint!
        for con in self.constraints {
            if con.identifier?.isEmpty == false {
                if con.identifier!.contains("search") == true {
                    constraint = con
                }
            }
            
        }
        if (constraint != nil) {
            constraint.constant = 40
        }
        let cell = tableView.cellForRow(at: indexPath)
        
        if let selectedText = cell?.textLabel?.text {
            self.textField.text = selectedText
            onSelect(selectedText, indexPath)
            
        }
        
        DispatchQueue.main.async(execute: { () -> Void in
            tableView.isHidden = self.hidesWhenSelected
        })
    }
    
    func tableView(_ tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)){
            cell.separatorInset = autoCompleteSeparatorInset
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)){
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)){
            cell.layoutMargins = autoCompleteSeparatorInset
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return autoCompleteCellHeight
    }
}


