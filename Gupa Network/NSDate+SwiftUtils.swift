//
//  NSDate+SwiftUtils.swift
//  SwiftDateUtils
//
//  Created by Morgan Harris on 6/06/2014.
//  Copyright (c) 2014 Morgan Harris. All rights reserved.
//

import Foundation

let formatter: DateFormatter = DateFormatter()

extension Date {
    
    func format(_ s:String) -> String {
        formatter.dateFormat = s
        return formatter.string(from: self)
    }
    
    func format(_ date: DateFormatter.Style = .none, time: DateFormatter.Style = .none) -> String {
        formatter.dateFormat = nil;
        formatter.timeStyle = time;
        formatter.dateStyle = date;
        return formatter.string(from: self)
    }
    
    func add(_ components: DateComponents) -> Date {
        let cal = Calendar.current
        return (cal as NSCalendar).date(byAdding: components, to: self, options: [])!
    }
    
    func add(_ years: NSInteger = NSUndefinedDateComponent, months: NSInteger = NSUndefinedDateComponent, weeks:NSInteger = NSUndefinedDateComponent, days: NSInteger = NSUndefinedDateComponent, hours: NSInteger = NSUndefinedDateComponent, minutes: NSInteger = NSUndefinedDateComponent, seconds: NSInteger = NSUndefinedDateComponent) -> Date {
        var components = DateComponents()
        components.year = years
        components.month = months
        components.weekOfYear = weeks
        components.day = days
        components.hour = hours
        components.minute = minutes
        components.second = seconds
        
        return self.add(components)
    }
    
    func subtract(_ years: NSInteger = NSUndefinedDateComponent, months: NSInteger = NSUndefinedDateComponent, weeks:NSInteger = NSUndefinedDateComponent, days: NSInteger = NSUndefinedDateComponent, hours: NSInteger = NSUndefinedDateComponent, minutes: NSInteger = NSUndefinedDateComponent, seconds: NSInteger = NSUndefinedDateComponent) -> Date {
        return self.add(-years, months:-months, weeks:-weeks, days:-days, hours:-hours, minutes:-minutes, seconds:-seconds)
    }
    
    func subtract(_ components: DateComponents) -> Date {
        var components1 =  components
        func negateIfNeeded(_ i: NSInteger) -> NSInteger {
            if i == NSUndefinedDateComponent {
                return i
            }
            return -i
        }

        components1.year         = negateIfNeeded(components1.year!)
        components1.month        = negateIfNeeded(components1.month!)
        components1.weekOfYear   = negateIfNeeded(components1.weekOfYear!)
        components1.day          = negateIfNeeded(components1.day!)
        components1.hour         = negateIfNeeded(components1.hour!)
        components1.minute       = negateIfNeeded(components1.minute!)
        components1.second       = negateIfNeeded(components1.second!)
        components1.nanosecond   = negateIfNeeded(components1.nanosecond!)
        return self.add(components1)
    }
}

func + (left: Date, right:TimeInterval) -> Date {
    return left.addingTimeInterval(right)
}

func + (left: Date, right:DateComponents) -> Date {
    return left.add(right);
}

func - (left: Date, right:TimeInterval) -> Date {
    return left.addingTimeInterval(-right)
}

func - (left: Date, right:DateComponents) -> Date {
    return left.subtract(right);
}
func + (left: DateComponents, right: DateComponents) -> DateComponents {
    func addIfPossible(_ left: NSInteger, right:NSInteger) -> NSInteger {
        if left == NSUndefinedDateComponent && right == NSUndefinedDateComponent {
            return NSUndefinedDateComponent
        }
        if left == NSUndefinedDateComponent {
            return right
        }
        if right == NSUndefinedDateComponent {
            return left
        }
        return left + right
    }
    
    var components = DateComponents()
    components.year         = addIfPossible(left.year!, right: right.year!)
    components.month        = addIfPossible(left.month!, right: right.month!)
    components.weekOfYear   = addIfPossible(left.weekOfYear!, right: right.weekOfYear!)
    components.day          = addIfPossible(left.day!, right: right.day!)
    components.hour         = addIfPossible(left.hour!, right: right.hour!)
    components.minute       = addIfPossible(left.minute!, right: right.minute!)
    components.second       = addIfPossible(left.second!, right: right.second!)
    components.nanosecond   = addIfPossible(left.nanosecond!, right: right.nanosecond!)
    
    return components
}
extension Int {

    func seconds() -> DateComponents {
        var components = DateComponents()
        components.second = self
        return components
    }
    
    func minutes() -> DateComponents {
        var components = DateComponents()
        components.minute = self
        return components
    }
    
    func hour() -> DateComponents {
        var components = DateComponents()
        components.hour = self
        return components
    }
    
    func days() -> DateComponents {
        var components = DateComponents()
        components.day = self
        return components
    }
    
    func weeks() -> DateComponents {
        var components = DateComponents()
        components.weekOfYear = self
        return components
    }
    
    func month() -> DateComponents {
        var components = DateComponents()
        components.month = self
        return components
    }
    
    func years() -> DateComponents {
        var components = DateComponents()
        components.year = self
        return components
    }
    

}
