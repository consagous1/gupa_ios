//
//  Table_travel+CoreDataProperties.swift
//  
//
//  Created by MWM23 on 9/22/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Table_travel {

    @NSManaged var alternatenames: String?
    @NSManaged var airport_city: String?
    @NSManaged var airport_city_code: String?
    @NSManaged var airport_code: String?
    @NSManaged var airport_country_code: String?
    @NSManaged var airport_geoname_id: String?
    @NSManaged var airport_gmt: String?
    @NSManaged var airport_icao: String?
    @NSManaged var airport_id: String?
    @NSManaged var airport_lat: String?
    @NSManaged var airport_lng: String?
    @NSManaged var airport_name: String?
    @NSManaged var airport_timezone: String?

}
