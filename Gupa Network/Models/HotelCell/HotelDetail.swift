//
//  HotelDetail.swift
//  Gupa Network
//
//  Created by mac on 6/10/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class HotelDetail: NSObject {
    @IBOutlet var strHotelName : String!
    @IBOutlet var strHotelAddress : String!
    @IBOutlet var strHotelDescription : String!
    @IBOutlet var strPostalCode : String!
    @IBOutlet var strCity : String!
    @IBOutlet var strCountryCode : String!
    @IBOutlet var strHotel_Id : String!
    @IBOutlet var strState : String!
    @IBOutlet var imgHotel : UIImageView!

}
