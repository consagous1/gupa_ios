  //
  //  APIResponseParam.swift
  //
  
  import Foundation
  import ObjectMapper
  
  class APIResponseParam {
    class BaseResponse:Mappable{
        var message:String? // String
        var error:NSNumber?
        
        required init(){
            
        }
        
        // Convert Json response to class object
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->BaseResponse?{
            var baseResponse:BaseResponse?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<BaseResponse>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        baseResponse = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                if (tempJsonString?.isEmpty)! { // On Success response is empty
                    baseResponse = BaseResponse()
                }
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<BaseResponse>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            baseResponse = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return baseResponse ?? nil
        }
        
        required init?(map: Map){
        }
        
        func mapping(map: Map){
            message     <- map["data"]
            error       <- map["error"]
        }
    }
    
    class Error : BaseResponse{
        var code:NSNumber?
        
        required init(){
            super.init()
        }
        
        // Convert Json response to class object
        override func getModelObjectFromServerResponse(jsonResponse: AnyObject)->Error?{
            var error:Error?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<Error>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        error = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<Error>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            error = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return error ?? nil
        }
        
        func toString()->String{
            return "{code:"+String(describing: code)+"msg: "+message!+"}"
        }
        
        // MARK:- Mappable delegate method implementation
        required init?(map: Map) {
            super.init(map: map)
        }
        
        override func mapping(map: Map){
            // TODO: Update key-value pair when socket response fetch
            super.mapping(map: map)
            code        <- map["errorCode"]
            
        }
    }
    
    class Login : BaseResponse{
        var loginData:LoginData?
        var modifyPermissionData:ModifyPermissionData?
        var permissionData:PermissionData?

        required init(){
            super.init()
        }
        
        // Convert Json response to class object
        override func getModelObjectFromServerResponse(jsonResponse: AnyObject)->Login?{
            var login:Login?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<Login>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        login = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<Login>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            login = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return login ?? nil
        }
        
        // MARK:- Mappable delegate method implementation
        required init?(map: Map) {
            super.init(map: map)
        }
        
        override func mapping(map: Map){
            // TODO: Update key-value pair when socket response fetch
            super.mapping(map: map)
            loginData               <- map["data"]
            permissionData          <- map["Permission"]
            modifyPermissionData    <- map["modifyPermission"]
        }
        
        // Data in login response
        class LoginData: Mappable{
            
            var id:String?
            var username:String?
            var first_name:String?
            var last_name:String?
            var mobile:String?
            var email:String?
            var image:String?
            var user_group:String?
            var token:String?
            
            // Convert Json response to class object
            func getModelObjectFromServerResponse(jsonResponse: AnyObject)->LoginData?{
                var loginData:LoginData?
                if jsonResponse is NSDictionary{
                    let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                    if tempDic != nil{
                        let mapper = Mapper<LoginData>()
                        if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                            loginData = testObject
                        }
                    }
                }else if jsonResponse is String{
                    let tempJsonString = jsonResponse as? String
                    let data = tempJsonString?.data(using: String.Encoding.utf8)!
                    
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            let mapper = Mapper<LoginData>()
                            if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                                loginData = testObject
                            }
                        }
                    } catch let error as NSError {
                        AppConstant.klogString(error.localizedDescription)
                    }
                }
                return loginData ?? nil
            }
            
            
            required init?(map: Map){
                
            }
            
            func mapping(map: Map){
                id          <- map["id"]
                username    <- map["username"]
                first_name  <- map["first_name"]
                last_name   <- map["last_name"]
                mobile      <- map["mobile"]
                email       <- map["email"]
                image       <- map["image"]
                user_group  <- map["user_group"]
                token       <- map["token"]
            }
        }
        
        // Data in login response
        class PermissionData: Mappable{
            
            var isClearChat:Bool?
            var isContact:Bool?
            var isContactGroup:Bool?
            var isDeleteChat:Bool?
            var isErrorLog:Bool?
            var isFileManager:Bool?
            var isGroupChat:Bool?
            var isIncomingMsg:Bool?
            var isPrivateChat:Bool?
            var isProfile:Bool?
            var isSetting:Bool?
            var isToolBackup:Bool?
            var isToolUpload:Bool?
            var isUser:Bool?
            
            // Convert Json response to class object
            func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PermissionData?{
                var permissionData:PermissionData?
                if jsonResponse is NSDictionary{
                    let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                    if tempDic != nil{
                        let mapper = Mapper<PermissionData>()
                        if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                            permissionData = testObject
                        }
                    }
                }else if jsonResponse is String{
                    let tempJsonString = jsonResponse as? String
                    let data = tempJsonString?.data(using: String.Encoding.utf8)!
                    
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            let mapper = Mapper<PermissionData>()
                            if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                                permissionData = testObject
                            }
                        }
                    } catch let error as NSError {
                        AppConstant.klogString(error.localizedDescription)
                    }
                }
                return permissionData ?? nil
            }
            
            required init?(map: Map){
                
            }
            
            func mapping(map: Map){
                
                isClearChat     <- map["isClearChat"]
                isContact       <- map["isContact"]
                isContactGroup  <- map["isContactGroup"]
                isDeleteChat    <- map["isDeleteChat"]
                isErrorLog      <- map["isErrorLog"]
                isFileManager   <- map["isFileManager"]
                isGroupChat     <- map["isGroupChat"]
                isIncomingMsg   <- map["isIncomingMsg"]
                isPrivateChat   <- map["isPrivateChat"]
                isProfile       <- map["isProfile"]
                isSetting       <- map["isSetting"]
                isToolBackup    <- map["isToolBackup"]
                isToolUpload    <- map["isToolUpload"]
                isUser          <- map["isUser"]
            }
        }
        
        // Data in login response
        class ModifyPermissionData: Mappable{
            
            var isClearChat:Bool?
            var isContact:Bool?
            var isContactGroup:Bool?
            var isDeleteChat:Bool?
            var isErrorLog:Bool?
            var isFileManager:Bool?
            var isGroupChat:Bool?
            var isIncomingMsg:Bool?
            var isPrivateChat:Bool?
            var isProfile:Bool?
            var isSetting:Bool?
            var isToolBackup:Bool?
            var isToolUpload:Bool?
            var isUser:Bool?

            // Convert Json response to class object
            func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ModifyPermissionData?{
                var modifyPermissionData:ModifyPermissionData?
                if jsonResponse is NSDictionary{
                    let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                    if tempDic != nil{
                        let mapper = Mapper<ModifyPermissionData>()
                        if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                            modifyPermissionData = testObject
                        }
                    }
                }else if jsonResponse is String{
                    let tempJsonString = jsonResponse as? String
                    let data = tempJsonString?.data(using: String.Encoding.utf8)!
                    
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            let mapper = Mapper<ModifyPermissionData>()
                            if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                                modifyPermissionData = testObject
                            }
                        }
                    } catch let error as NSError {
                        AppConstant.klogString(error.localizedDescription)
                    }
                }
                return modifyPermissionData ?? nil
            }
            
            
            required init?(map: Map){
                
            }
            
            func mapping(map: Map){
                
                isClearChat     <- map["isClearChat"]
                isContact       <- map["isContact"]
                isContactGroup  <- map["isContactGroup"]
                isDeleteChat    <- map["isDeleteChat"]
                isErrorLog      <- map["isErrorLog"]
                isFileManager   <- map["isFileManager"]
                isGroupChat     <- map["isGroupChat"]
                isIncomingMsg   <- map["isIncomingMsg"]
                isPrivateChat   <- map["isPrivateChat"]
                isProfile       <- map["isProfile"]
                isSetting       <- map["isSetting"]
                isToolBackup    <- map["isToolBackup"]
                isToolUpload    <- map["isToolUpload"]
                isUser          <- map["isUser"]
            }
        }
    }
    
    
    class UserPermission : BaseResponse{
        var name:String?
        var permission:UserPermissionData?
        var updatePermission:UserPermissionData?
        
        required init(){
            super.init()
        }
        
        // Convert Json response to class object
        override func getModelObjectFromServerResponse(jsonResponse: AnyObject)->UserPermission?{
            var userPermission:UserPermission?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<UserPermission>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        userPermission = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<UserPermission>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            userPermission = testObject
                        }
                    }
                } catch let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return userPermission ?? nil
        }
        
        // MARK:- Mappable delegate method implementation
        required init?(map: Map) {
            super.init(map: map)
        }
        
        override func mapping(map: Map){
            // TODO: Update key-value pair when socket response fetch
            super.mapping(map: map)
            name                <- map["name"]
            permission          <- map["Permission"]
            updatePermission    <- map["modifyPermission"]
        }
        
        // Data in profileData response
        class UserPermissionData: Mappable{
            var isGroupChat:Bool?
            var isIncomingMsg:Bool?
            var isPrivateChat:Bool?
            var isFileManager:Bool?
            var isProfile:Bool?
            var isToolBackup:Bool?
            var isErrorLog:Bool?
            var isSetting:Bool?
            var isToolUpload:Bool?
            var isUser:Bool?
            var isContact:Bool?
            var isContactGroup:Bool?
            
            // Convert Json response to class object
            func getModelObjectFromServerResponse(jsonResponse: AnyObject)->UserPermissionData?{
                var userPermissionData:UserPermissionData?
                if jsonResponse is NSDictionary{
                    let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                    if tempDic != nil{
                        let mapper = Mapper<UserPermissionData>()
                        if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                            userPermissionData = testObject
                        }
                    }
                }else if jsonResponse is String{
                    let tempJsonString = jsonResponse as? String
                    let data = tempJsonString?.data(using: String.Encoding.utf8)!
                    
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            let mapper = Mapper<UserPermissionData>()
                            if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                                userPermissionData = testObject
                            }
                        }
                    } catch let error as NSError {
                        AppConstant.klogString(error.localizedDescription)
                    }
                }
                return userPermissionData ?? nil
            }
            
            required init?(map: Map){
                
            }
            
            func mapping(map: Map){
                
                isGroupChat     <- map["isGroupChat"]
                isIncomingMsg   <- map["isIncomingMsg"]
                isPrivateChat   <- map["isPrivateChat"]
                isFileManager   <- map["isFileManager"]
                isProfile       <- map["isProfile"]
                isToolBackup    <- map["isToolBackup"]
                isErrorLog      <- map["isErrorLog"]
                isSetting       <- map["isSetting"]
                isToolUpload    <- map["isToolUpload"]
                isUser          <- map["isUser"]
                isContact       <- map["isContact"]
                isContactGroup  <- map["isContactGroup"]
            }
        }
    }
    
    //Create Group list
    class ContactListInfo : BaseResponse {
        var contactListInfoData : [ContactListInfoData]?
        
        required init() {
            super.init()
        }
        
        //Convert Json response to class object
        override func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> ContactListInfo? {
            var contactInfo: ContactListInfo?
            if jsonResponse is NSDictionary {
                let tempDict:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDict != nil {
                    let mapper = Mapper<ContactListInfo>()
                    if let testObject = mapper.map(JSON: tempDict as! [String : Any]) {
                        contactInfo = testObject
                    }
                }
            } else if jsonResponse is String {
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ContactListInfo>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]) {
                            contactInfo = testObject
                        }
                    }
                } catch  let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return contactInfo ?? nil
        }
        
        //Mappable delegate method implementation
        required init?(map: Map) {
            super.init(map: map)
        }
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            contactListInfoData <- map["data"]
        }
        
        //Data in response
        class ContactListInfoData:  Mappable {
            
            var _id:String?
            var first_name:String?
            var last_name:String?
            var email:String?
            var mobile:String?
            var username:String?
            var status:String?
            var image_path:String?
            var password: String?
            var online:String?
            var type:String?
            var isContact:String?
            var userToken:String?
            var onlineTime:String?
            var lat:String?
            var lang:String?
            var strUserId:String?

            // Convert JSON response to chat object
            func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ContactListInfoData?{
                var contactListInfoData:ContactListInfoData?
                if jsonResponse is NSDictionary {
                    let tempDict: NSDictionary? = (jsonResponse as! NSDictionary)
                    if tempDict != nil {
                        let mapper = Mapper<ContactListInfoData>()
                        if let testObject = mapper.map(JSON: tempDict as! [String : Any]) {
                            contactListInfoData = testObject
                        }
                    }
                } else if jsonResponse is String {
                    let tempJsonString = jsonResponse as? String
                    let data = tempJsonString?.data(using: String.Encoding.utf8)!
                    
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            let mapper = Mapper<ContactListInfoData>()
                            if let testObject = mapper.map(JSON: jsonResult as! [String : Any]) {
                                contactListInfoData = testObject
                            }
                        }
                    } catch let error as NSError {
                        AppConstant.klogString(error.localizedDescription)
                    }
                }
                return contactListInfoData ?? nil
            }

            required init?(map: Map) {
                
            }
            
            func mapping(map: Map) {
                _id <- map["_id"]
                first_name <- map["first_name"]
                last_name <- map["last_name"]
                email <- map["email"]
                mobile <- map["mobile"]
                username <- map["username"]
                status <- map["status"]
                image_path <- map["image_path"]
                password <- map["password"]
                online <- map["online"]
                type <- map["type"]
                isContact <- map["isContact"]
                userToken <- map["userToken"]
                online <- map["onlineTime"]
                lat <- map["lat"]
                lang <- map["lang"]
                strUserId <- map["id"]

            }
        }
    }
    
    //Get user list
    class UserListInfo : BaseResponse {
        var userListInfoData : [UserListInfoData]?
        
        required init() {
            super.init()
        }
        
        //Convert Json response to class object
        override func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> UserListInfo? {
            var contactInfo: UserListInfo?
            if jsonResponse is NSDictionary {
                let tempDict:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDict != nil {
                    let mapper = Mapper<UserListInfo>()
                    if let testObject = mapper.map(JSON: tempDict as! [String : Any]) {
                        contactInfo = testObject
                    }
                }
            } else if jsonResponse is String {
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<UserListInfo>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]) {
                            contactInfo = testObject
                        }
                    }
                } catch  let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return contactInfo ?? nil
        }
        
        //Mappable delegate method implementation
        required init?(map: Map) {
            super.init(map: map)
        }
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            userListInfoData <- map["data"]
        }
        
        //Data in response
        class UserListInfoData:  Mappable {
            
            var _id:String?
            var first_name:String?
            var last_name:String?
            var email:String?
            var mobile:String?
            var username:String?
            var status:String?
            var image_path:String?
            var password: String?
            var online:String?
            var type:String?
            var isContact:String?
            var userToken:String?
            var onlineTime:String?
            var lat:String?
            var lang:String?
            var strUserId:String?

            // Convert JSON response to chat object
            func getModelObjectFromServerResponse(jsonResponse: AnyObject)->UserListInfoData?{
                var userListInfoData:UserListInfoData?
                if jsonResponse is NSDictionary {
                    let tempDict: NSDictionary? = (jsonResponse as! NSDictionary)
                    if tempDict != nil {
                        let mapper = Mapper<UserListInfoData>()
                        if let testObject = mapper.map(JSON: tempDict as! [String : Any]) {
                            userListInfoData = testObject
                        }
                    }
                } else if jsonResponse is String {
                    let tempJsonString = jsonResponse as? String
                    let data = tempJsonString?.data(using: String.Encoding.utf8)!
                    
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            let mapper = Mapper<UserListInfoData>()
                            if let testObject = mapper.map(JSON: jsonResult as! [String : Any]) {
                                userListInfoData = testObject
                            }
                        }
                    } catch let error as NSError {
                        AppConstant.klogString(error.localizedDescription)
                    }
                }
                return userListInfoData ?? nil
            }
            
            required init?(map: Map) {
                
            }
            
            func mapping(map: Map) {
                _id <- map["_id"]
                first_name <- map["first_name"]
                last_name <- map["last_name"]
                email <- map["email"]
                mobile <- map["mobile"]
                username <- map["username"]
                status <- map["status"]
                image_path <- map["image_path"]
                password <- map["password"]
                online <- map["online"]
                type <- map["type"]
                isContact <- map["isContact"]
                userToken <- map["userToken"]
                onlineTime <- map["onlineTime"]
                lat <- map["lat"]
                lang <- map["lang"]
                strUserId <- map["id"]

            }
        }
    }
    
    //Get user list
    class AllUserListInfo : BaseResponse {
        var allUserListInfoData : [AllUserListInfoData]?
        
        required init() {
            super.init()
        }
        
        //Convert Json response to class object
        override func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> AllUserListInfo? {
            var allUserInfo: AllUserListInfo?
            if jsonResponse is NSDictionary {
                let tempDict:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDict != nil {
                    let mapper = Mapper<AllUserListInfo>()
                    if let testObject = mapper.map(JSON: tempDict as! [String : Any]) {
                        allUserInfo = testObject
                    }
                }
            } else if jsonResponse is String {
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<AllUserListInfo>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]) {
                            allUserInfo = testObject
                        }
                    }
                } catch  let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return allUserInfo ?? nil
        }
        
        //Mappable delegate method implementation
        required init?(map: Map) {
            super.init(map: map)
        }
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            allUserListInfoData <- map["data"]
        }
        
        //Data in response
        class AllUserListInfoData:  Mappable {
            
            var _id:String?
            var first_name:String?
            var last_name:String?
            var email:String?
            var mobile:String?
            var username:String?
            var status:String?
            var image_path:String?
            var password: String?
            var online:Bool?
            var type:String?
            var isContact:String?
            var userToken:String?
            var onlineTime:NSNumber?
            var lat:String?
            var lang:String?
            var strUserId:String?

            // Convert JSON response to chat object
            func getModelObjectFromServerResponse(jsonResponse: AnyObject)->AllUserListInfoData?{
                var allUserListInfoData: AllUserListInfoData?
                if jsonResponse is NSDictionary {
                    let tempDict: NSDictionary? = (jsonResponse as! NSDictionary)
                    if tempDict != nil {
                        let mapper = Mapper<AllUserListInfoData>()
                        if let testObject = mapper.map(JSON: tempDict as! [String : Any]) {
                            allUserListInfoData = testObject
                        }
                    }
                } else if jsonResponse is String {
                    let tempJsonString = jsonResponse as? String
                    let data = tempJsonString?.data(using: String.Encoding.utf8)!
                    
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            let mapper = Mapper<AllUserListInfoData>()
                            if let testObject = mapper.map(JSON: jsonResult as! [String : Any]) {
                                allUserListInfoData = testObject
                            }
                        }
                    } catch let error as NSError {
                        AppConstant.klogString(error.localizedDescription)
                    }
                }
                return allUserListInfoData ?? nil
            }
            
            required init?(map: Map) {
                
            }
            
            func mapping(map: Map) {
                _id <- map["_id"]
                first_name <- map["first_name"]
                last_name <- map["last_name"]
                email <- map["email"]
                mobile <- map["mobile"]
                username <- map["username"]
                status <- map["status"]
                image_path <- map["image_path"]
                password <- map["password"]
                online <- map["online"]
                type <- map["type"]
                isContact <- map["isContact"]
                userToken <- map["userToken"]
                onlineTime <- map["onlineTime"]
                lat <- map["lat"]
                lang <- map["lang"]
                strUserId <- map["lang"]

            }
        }
    }
    
    //Get home list
    class HomeListInfo : BaseResponse {
        var homeListInfoData : [HomeListInfoData]?
        
        required init() {
            super.init()
        }
        
        //Convert Json response to class object
        override func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> HomeListInfo? {
            
            
            var homeInfo: HomeListInfo?
            if jsonResponse is NSDictionary {
                let tempDict:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDict != nil {
                    let mapper = Mapper<HomeListInfo>()
                    if let testObject = mapper.map(JSON: tempDict as! [String : Any]) {
                        homeInfo = testObject
                    }
                }
            } else if jsonResponse is String {
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<HomeListInfo>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]) {
                            homeInfo = testObject
                        }
                    }
                } catch  let error as NSError {
                    AppConstant.klogString(error.localizedDescription)
                }
            }
            return homeInfo ?? nil
        }
        
        //Mappable delegate method implementation
        required init?(map: Map) {
            super.init(map: map)
        }
        
        override func mapping(map: Map) {
            super.mapping(map: map)
            homeListInfoData <- map["data"]
        }
        
        //Data in response
        class HomeListInfoData:  Mappable {
         
            var _id:String?
            var addedasfreind: String?
            var createdby:String?
            var friend_name:String?
            var group_icon:String?
            var group_name:String?
            var is_group:Bool?
            var last_msg:String?
            var last_msg_sender_name:String?
            var last_msg_type:String?
            var lastmsgsentby:String?
            var lastmsgtime:NSNumber?//String?
            var timestamp:NSNumber?//String?
            var userLists:[UserListData]?
            var status: String?
            var id :String?
            var created_Id :String?
            var friend_Id :String?

            // Convert JSON response to chat object
            func getModelObjectFromServerResponse(jsonResponse: AnyObject)->HomeListInfoData?{
                var homeListInfoData:HomeListInfoData?
                if jsonResponse is NSDictionary {
                    let tempDict: NSDictionary? = (jsonResponse as! NSDictionary)
                    if tempDict != nil {
                        let mapper = Mapper<HomeListInfoData>()
                        if let testObject = mapper.map(JSON: tempDict as! [String : Any]) {
                            homeListInfoData = testObject
                        }
                    }
                } else if jsonResponse is String {
                    let tempJsonString = jsonResponse as? String
                    let data = tempJsonString?.data(using: String.Encoding.utf8)!
                    
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            let mapper = Mapper<HomeListInfoData>()
                            if let testObject = mapper.map(JSON: jsonResult as! [String : Any]) {
                                homeListInfoData = testObject
                            }
                        }
                    } catch let error as NSError {
                        AppConstant.klogString(error.localizedDescription)
                    }
                }
                return homeListInfoData ?? nil
            }
            
            required init?(map: Map) {
                
            }
            
            func mapping(map: Map) {
                
                _id <- map["_id"]
                addedasfreind <- map["addedasfreind"]
                createdby <- map["createdby"]
                friend_name <- map["friend_name"]
                group_icon <- map["group_icon"]
                group_name <- map["group_name"]
                is_group <- map["is_group"]
                last_msg <- map["last_msg"]
                last_msg_sender_name <- map["last_msg_sender_name"]
                last_msg_type <- map["last_msg_type"]
                lastmsgsentby <- map["lastmsgsentby"]
                lastmsgtime <- map["lastmsgtime"]
                timestamp <- map["timestamp"]
                userLists <- map["userLists"]
                status <- map["status"]
                id <- map["id"]
                created_Id <- map["createrId"]
                friend_Id <- map["freindId"]

            }
        }
        
        //
        //Data in response
        class UserListData:  Mappable {
            
            var _id:String?
            var email: String?
            var first_name:String?
            var image_path:String?
            var last_name:String?
            var mobile:String?
            var online:Bool?
            var password:String?
            var status:String?
            var type:String?
            var userToken:String?
            var username:[NSArray]?
            var strUserId:String?

            
            // Convert JSON response to chat object
            func getModelObjectFromServerResponse(jsonResponse: AnyObject)->UserListData?{
                var userListData:UserListData?
                if jsonResponse is NSDictionary {
                    let tempDict: NSDictionary? = (jsonResponse as! NSDictionary)
                    if tempDict != nil {
                        let mapper = Mapper<UserListData>()
                        if let testObject = mapper.map(JSON: tempDict as! [String : Any]) {
                            userListData = testObject
                        }
                    }
                } else if jsonResponse is String {
                    let tempJsonString = jsonResponse as? String
                    let data = tempJsonString?.data(using: String.Encoding.utf8)!
                    
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            let mapper = Mapper<UserListData>()
                            if let testObject = mapper.map(JSON: jsonResult as! [String : Any]) {
                                userListData = testObject
                            }
                        }
                    } catch let error as NSError {
                        AppConstant.klogString(error.localizedDescription)
                    }
                }
                return userListData ?? nil
            }
            
            required init?(map: Map) {
                
            }
            
            func mapping(map: Map) {
                _id <- map["_id"]
                email <- map["email"]
                first_name <- map["first_name"]
                image_path <- map["image_path"]
                last_name <- map["last_name"]
                mobile <- map["mobile"]
                online <- map["online"]
                password <- map["password"]
                status <- map["status"]
                userToken <- map["userToken"]
                username <- map["username"]
                strUserId <- map["id"]

            }
        }
    }
  }
  
