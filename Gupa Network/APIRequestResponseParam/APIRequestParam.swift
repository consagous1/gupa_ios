//
//  APIRequestParam.swift
//  MyPanditJi
//

import Foundation
import ObjectMapper

class APIRequestParam {
    
    class Login: Mappable {
        
        private var username:String?
        private var password:String?
        private var fcmToken:String?

        init(username:String?, password:String?, fcmToken:String?) {
            self.username = username
            self.password = password
            self.fcmToken = fcmToken
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            username    <- map["username"]
            password    <- map["password"]
            fcmToken    <- map["fcmToken"]
        }
    }
    
    class ChangePassword: Mappable {
        private var oldPassword:String?
        private var password:String?
        
        init(oldPassword:String?, password:String?) {
            self.oldPassword = oldPassword
            self.password = password
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            oldPassword <- map["oldPassword"]
            password <- map["password"]
        }
    }
    
    class ForgotPassword: Mappable {
        private var email:String?
        
        init(email:String?) {
            self.email = email
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            email  <- map["email"]
        }
    }
    
    class ResetPassword: Mappable {
        
        private var email:String?
        private var password:String?
        private var otp:String?

        init(email:String?, password:String?, otp: String?) {
            self.email = email
            self.password = password
            self.otp = otp
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            email    <- map["email"]
            password    <- map["password"]
            otp      <- map["otp"]
        }
    }
    
    class Registration: Mappable {
        private var firstName:String?
        private var lastName:String?
        private var personal_number:String?
        private var email:String?
        private var password:String?
        private var uid:String?
        private var deviceType:String?
        
        init(firstName:String?, lastName:String?, personal_number:String?, email:String?, password:String?, uid:String?, deviceType:String?) {
            self.firstName = firstName
            self.lastName = lastName
            self.personal_number = personal_number
            self.email = email
            self.password = password
            self.uid = uid
            self.deviceType = deviceType
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            firstName       <- map["firstName"]
            lastName        <- map["lastName"]
            personal_number <- map["personal_number"]
            email           <- map["email"]
            password        <- map["password"]
            uid             <- map["uid"]
            deviceType      <- map["deviceType"]
        }
    }
    
    //Contact list for New Group
    class ContactListInfo: Mappable {

        init() {
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
        }
    }
    
    //User list for New Chat
    class UserListInfo: Mappable {
        
        init() {
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
        }
    }
    
    //User list for New Chat
    class AllUserListInfo: Mappable {
        
        init() {
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
        }
    }
    
    //Home list for New Chat
    class HomeListInfo: Mappable {
        
        init() {
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
        }
    }
    
    //User list for New Chat
    class MobileUploadInfo: Mappable {
        
        init() {
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
        }
    }
    
    //Home list for New Chat
    class CreateSingleAndGroupChatInfo: Mappable {
        
        private var group_icon:String?
        private var group_name:String?
        private var is_group:Bool?
        private var addedasfreind:String?
        private var lastmsgsentby:String?
        private var last_msg:String?
        private var friend_name:String?
        private var created_by:String?
        private var timestamp:String?
        private var status:String?
        private var created_id:String?
        private var friendId:String?
        private var userLists:[GroupUserData]?

        init(group_icon:String?, group_name:String?, is_group:Bool?, addedasfreind:String?, lastmsgsentby:String?, last_msg:String?, friend_name:String?, created_by:String?, timestamp:String?, status:String?,created_id :String? , friendId : String?,   userLists:[GroupUserData]?) {
            
            self.group_icon = group_icon
            self.group_name = group_name
            self.is_group = is_group
            self.addedasfreind = addedasfreind
            self.lastmsgsentby = lastmsgsentby
            self.last_msg = last_msg
            self.friend_name = friend_name
            self.created_by = created_by
            self.timestamp = timestamp
            self.status = status
            self.created_id = created_id
            self.friendId = friendId
            self.userLists = userLists
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            group_icon      <- map["group_icon"]
            group_name      <- map["group_name"]
            is_group        <- map["is_group"]
            addedasfreind   <- map["addedasfreind"]
            lastmsgsentby   <- map["lastmsgsentby"]
            last_msg        <- map["last_msg"]
            friend_name     <- map["friend_name"]
            created_by      <- map["created_by"]
            timestamp       <- map["timestamp"]
            status          <- map["status"]
            created_id      <- map["createrId"]
            friendId        <- map["freindId"]
            userLists       <- map["userLists"]
        }
    }
    
    
    //Add participate
    class Addparticiaptes : Mappable {
        
        private var group_Id:String?
        private var userLists:[AddMemberData]?
        
        init(group_Id:String?, userLists:[AddMemberData]?) {
            self.group_Id = group_Id
            self.userLists = userLists
        }
        
        required init?(map: Map) {
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            group_Id      <- map["groupId"]
            userLists     <- map["userLists"]
        }
    }
    
    //
    //Home list for New Chat
    class MsgDeleteInfo: Mappable {
       
        private var deleteLists:[GroupUserData]?
        
        init(deleteList:[GroupUserData]?) {
            self.deleteLists = deleteList
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            deleteLists       <- map["msgIds"]
        }
    }
    
    //////////////
    class PujaDescribe: Mappable {
        private var section_id:String?
        private var city:String?
        
        init(section_id:String?, city:String?) {
            self.section_id = section_id
            self.city = city
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            section_id  <- map["section_id"]
            city        <- map["city"]
        }
    }
    
    class Logout: Mappable {
        private var user_id:String?
        
        init(user_id:String?) {
            self.user_id = user_id
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            user_id  <- map["user_id"]
        }
    }
    
    class City: Mappable {
        private var country_id:String?
        
        init(country_id:String?) {
            self.country_id = country_id
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            country_id  <- map["country_id"]
        }
    }
    
    class GetPanditG: Mappable {
        private var country_id:String?
        private var city_id:String?
        
        init(country_id:String?, city_id:String?) {
            self.country_id = country_id
            self.city_id = city_id
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            country_id  <- map["country_id"]
            city_id     <- map["city_id"]
        }
    }
    
    class Profile: Mappable {
        private var user_id:String?
        
        init(user_id:String?) {
            self.user_id = user_id
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            user_id         <- map["user_id"]
        }
    }
    
    class UploadProfile: Mappable {
        var user_id:String?
        var fileData:Data?
        var fileName:String?
        var filePath:String?
        
        init(user_id:String?, fileData:Data?, fileName:String?, filePath:String?) {
            self.user_id = user_id
            self.fileData = fileData
            self.fileName = fileName
            self.filePath = filePath
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            user_id         <- map["user_id"]
        }
    }
    
    class EditProfile: Mappable {
        private var user_id:String?
        private var personal_number:String?
        private var country_id:String?
        private var city_id:String?
        private var address:String?
        private var about_us:String?
        
        init(user_id:String?, personal_number:String?, country_id:String?, city_id:String?, address:String?, about_us:String?) {
            self.user_id = user_id
            self.personal_number = personal_number
            self.country_id = country_id
            self.city_id = city_id
            self.address = address
            self.about_us = about_us
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            user_id         <- map["user_id"]
            personal_number <- map["personal_number"]
            country_id      <- map["country_id"]
            city_id         <- map["city_id"]
            address         <- map["address"]
            about_us        <- map["about_us"]
        }
    }
    
    
    
    class BlogNotify: Mappable {
        private var uid:String?
        private var deviceType:String?
        
        init(uid:String?, deviceType:String?) {
            self.uid = uid
            self.deviceType = deviceType
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            uid         <- map["uid"]
            deviceType  <- map["deviceType"]
        }
    }
    
    class BookPooja: Mappable {
        
        private var customer_name:String?
        private var mobile_no:String?
        private var puja_id:String?
        private var puja_name:String?
        private var puja_prices:String?
        private var date:String?
        
        init(customer_name:String?, mobile_no:String?, puja_id:String?, puja_name:String?, puja_prices:String?, date:String?) {
            self.customer_name = customer_name
            self.mobile_no = mobile_no
            self.puja_id = puja_id
            self.puja_name = puja_name
            self.puja_prices = puja_prices
            self.date = date
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            customer_name   <- map["customer_name"]
            mobile_no       <- map["mobile_no"]
            puja_id         <- map["puja_id"]
            puja_name       <- map["puja_name"]
            puja_prices     <- map["puja_prices"]
            date            <- map["date"]
        }
    }
    
    class Favourite: Mappable {
        private var user_id:String?
        private var puja_id:String?
        
        init(user_id:String?, puja_id:String?) {
            self.user_id = user_id
            self.puja_id = puja_id
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            user_id     <- map["user_id"]
            puja_id     <- map["puja_id"]
        }
    }
    
    class FavouriteListShow: Mappable {
        private var user_id:String?
        
        init(user_id:String?) {
            self.user_id = user_id
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            user_id  <- map["user_id"]
        }
    }
    
    class GroupUserData: Mappable{
        
        private var _id:String?
        
        
        init(_id:String?) {
            self._id = _id
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            _id   <- map["_id"]
        }
    }
    
    class AddMemberData: Mappable{
        
        private var _id:String?
        
        
        init(_id:String?) {
            self._id = _id
        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            _id   <- map["_id"]
        }
    }
    
    class HomeListData: Mappable{
        
         var _id:String?
         var createdby:String?
         var friend_name:String?
         var group_icon:String?
         var group_name:String?
         var is_group:Bool?
         var lang: String?
         var lat: String?
         var location: String?
         var last_msg:String?
         var last_msg_sender_name:String?
         var last_msg_type:String?
         var lastmsgsentby:String?
         var lastmsgtime:NSNumber?
         var online:Bool?
         var status: String?
         var createdBy_Id:String?
         var friend_Id:String?
         var userLists:[APIResponseParam.HomeListInfo.UserListData]?

        init(_id:String?, createdby: String?, friend_name: String?, group_icon: String?, group_name: String?, is_group: Bool?, lang: String?, lat: String?, location: String?, last_msg: String?, last_msg_sender_name: String?, last_msg_type: String?, lastmsgsentby: String?, lastmsgtime: NSNumber?, online: Bool?, status: String?,createdBy_Id : String?,friend_Id : String? , userLists: [APIResponseParam.HomeListInfo.UserListData]) {
            
            self._id = _id
            self.createdby = createdby
            self.friend_name = friend_name
            self.group_icon = group_icon
            self.group_name = group_name
            self.is_group = is_group
            self.lang = lang
            self.lat = lat
            self.location = location
            self.last_msg = last_msg
            self.last_msg_sender_name = last_msg_sender_name
            self.last_msg_type = last_msg_type
            self.lastmsgsentby = lastmsgsentby
            self.lastmsgtime = lastmsgtime
            self.online = online
            self.status = status
            self.userLists = userLists
            self.createdBy_Id = createdBy_Id
            self.friend_Id = friend_Id

        }
        
        required init?(map: Map) {
            
        }
        
        //Map Auth Request Parameters
        func mapping(map: Map) {
            _id                  <- map["_id"]
            createdby            <- map["createdby"]
            friend_name          <- map["friend_name"]
            group_icon           <- map["group_icon"]
            group_name           <- map["group_name"]
            is_group             <- map["is_group"]
            lang                 <- map["lang"]
            lat                  <- map["lat"]
            location             <- map["location"]
            last_msg             <- map["last_msg"]
            last_msg_sender_name <- map["last_msg_sender_name"]
            last_msg_type        <- map["last_msg_type"]
            lastmsgsentby        <- map["lastmsgsentby"]
            online               <- map["online"]
            status               <- map["status"]
            createdBy_Id         <- map["createrId"]
            friend_Id            <- map["freindId"]
            userLists            <- map["userLists"]
           
        }
    }
}
