
//
//  AppTheme.swift
//  TalkBox
//
//  Created by mac on 11/20/15.
//  Copyright © 2015 MobiWebTech. All rights reserved.
//

//define
//**** Owner Details ****
let KeyWoofOwner = "KeyWoofOwner"

//**** Dog details Key ****
let KeyUserDogs = "KeyArrayDogs"
let KeyDogName = "KeyDogName"
let KeyDogSize = "KeyDogSize"
let KeyDogBirthdate = "KeyDateBirthdate"

//**** Setting Keys ****
let KeyUserShouldNotShareLocWoofLinks = "KeyUserShouldNotShareLocWoofLinks"
let KeyUserShouldNotShareLocUsers = "KeyUserShouldNotShareLocUsers"
let KeyUserShouldNotShareNameWoofLinks = "KeyUserShouldNotShareNameWoofLinks"

//****** Dog Woofs ****

let KeyWoofDogName = "KeyWoofDogName"
let KeyWoofDate = "KeyWoofDate"
let KeyWoofText = "KeyWoofText"

//****** Walk *******
let KeyWalkDateStartTime = "KeyWalkDateStartTime"
let KeyWalkDateEndTime = "KeyWalkDateEndTime"

//let AppUrl = "http://travel.ci.mwdemoserver.com/Webservices/"

// LIVE URL
//let AppUrl = "http://gupanetwork.com/backend/webservices/"

let AppurlforImage = "http://111.118.252.147/gupa/backend/"
// DEBUG URL 
let AppUrl         =  "http://111.118.252.147/gupa/backend/webservices/"
//let AppUrlNew      =  "http://111.118.252.147/gupa/backend/webservicesnew/"
let AppUrlNew        =  "http://gupanetwork.com/backend/webservicesnew/"
let AppLinkAboutUs   =  "http://111.118.252.147/gupa/#/aboutgupa"
let AppLinkTaxi      = "http://111.118.252.147/gupa/backend/taxi/"

let GetCodeUrl = "http://iatageo.com/"

//*******prefrences******

let kPrefrenceStatusHotelDetailFlightDetail = "kPrefrenceStatusHotelDetailFlightDetail"
let kPrefrenceNotifiCount = "kPrefrenceNotifiCount"
let kPrefrenceLoginDetails = "kPrefrenceLoginDetails"
let kPrefrenceAirportCode = "kPrefrenceAirportCode"
let kPrefrencePresentLocation = "kPrefrencePresentLocation"
let kPrefrenceWalkStatus = "kPrefrenceWalkStatus"
let kPrefrenceWalkStartTime = "kPrefrenceWalkStartTime"
let kPrefrenceDestinationStopDetails = "kPrefrenceDestinationStopDetails"
let kPrefrenceFirstTimeCheck = "kPrefrenceFirstTimeCheck"
let kPrefrenceCheckUserStatus = "kPrefrenceCheckUserStatus"
let kPrefrenceCheckUserFlight = "kPrefrenceCheckUserFlight"
let kPrefrenceCheckHotelId = "kPrefrenceCheckHotelId"
let kPrefrenceIsLogin = "kPrefrenceIsLogin"
let kPrefrenceIsFirstTimeForFlight = "kPrefrenceIsFirstTimeForFlight"
let kPrefrenceFlightData = "kPrefrenceFlightData"
let kPrefrenceIsFirstTimeForTransport = "kPrefrenceIsFirstTimeForTransport"
let kPrefrenceTransportData = "kPrefrenceTransportData"
let kPrefrenceTransRatingData = "kPrefrenceRatingData"
let kPrefrenceMangoUserID = "kPrefrenceMangoUserID"

let kPrefrenceCheckUserEmail_Login = "kPrefrenceCheckUserEmail_Login"
let kPrefrenceCheckLoginUserName = "kPrefrenceLoginUsername"

//************************


import Foundation
import UIKit
import Alamofire
import CoreLocation
import JGProgressHUD


open class MyServerTrustPolicyManager: ServerTrustPolicyManager {
    
    // Override this function in order to trust any self-signed https
    open override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {
        return ServerTrustPolicy.disableEvaluation
        
        // or, if `host` contains substring, return `disableEvaluation`
        // Ex: host contains `my_company.com`, then trust it.
    }
}

class AppTheme: NSObject
{
   
    var themeColorOrange : UIColor = UIColor(red: 243.0/255.0, green: 120.0/255.0, blue: 8.0/255.0, alpha: 1)
    /*Location Timer*/
    var myTimer: Timer!
    var loading : JGProgressHUD = JGProgressHUD(style: JGProgressHUDStyle.dark)
    var alamofireManager : Alamofire.SessionManager?
    
    static var deviceType = "1"
    static var deviceToken : String!
    
//========================================
    // MARK: - Services Method Main
//========================================
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
   
    func callPostService(_ urlString: String, param : NSMutableDictionary,  completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
        let parts: NSMutableArray = []
        if(param != nil)
        {
            for (key,value) in param{
                let part = String(format: "%@=%@", urlEncode(key as AnyObject?)!,urlEncode(value as AnyObject?)!)
                parts.add(part)
            }
        }
        
        let postString: NSString = (parts.componentsJoined(by: "&") as NSString)
        
        guard let serviceUrl = URL(string: urlString) else { return }
        var request = URLRequest(url: serviceUrl)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        //        let postString = "t_c=1&email=r@r.in&device_id=123456&password=123456&device_type=1"
        
        request.httpBody = postString.data(using: String.Encoding.utf8.rawValue)!
        //        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            DispatchQueue.main.async {
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    completion("error", error as AnyObject)
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    completion("error", error as AnyObject)
                }
                
                let responseString = String(data: data, encoding: .utf8)
                
                print("SERVERRESPONSE===>>",responseString ?? "")
                
                let dict = self.convertStringToDictionary(text: responseString!)
                completion("success", dict as AnyObject)
            }
            
        }
        task.resume()
        
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func callPostWithMultipartServices(_ urlString : String, param : [String: AnyObject]?, imageData : Data, file_type : String, completion : @escaping (_ result : String, _ data: AnyObject) -> Void)
    {
        
        // Testing Multi Part API
        
        
        let strUrl = URL(string: urlString)
        
//        let params = NSMutableDictionary()
        
        Alamofire.upload(multipartFormData:
            
            {
                (multipartFormData) in
                
                
                if file_type == "signup"
                {
                    multipartFormData.append(imageData, withName: "profile_pic", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                else if file_type == "imagePost"
                {
                    multipartFormData.append(imageData, withName: "file_name", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                else if file_type == "image"
                {
                    multipartFormData.append(imageData, withName: "profile_pic", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                else if file_type == "video"
                {
                    multipartFormData.append(imageData, withName: "file_name", fileName: "file.mov", mimeType: "video/quicktime")
                    
                }
                for (key, value) in param!
                    
                {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                }
                
        }, to:strUrl!,headers:nil)
            
        { (result) in
            
            switch result {
                
            case .success(let upload,_,_ ):
                
                upload.uploadProgress(closure: { (progress) in
                    
                    //Print progress
                    
                })
                
                upload.responseJSON
                    
                    { response in
                        
                        //print response.result
                        
                        
   if let JSON: AnyObject = response.result.value as AnyObject
                           {
          if let dict: NSDictionary = JSON as? NSDictionary
        {
            print(dict)
            if(dict["status"] as! Bool == true)
            {
                completion("success", JSON)
            }else
            {
                if let errorMessage : String = dict["message"] as? String
                {
//                    errorMessage.capitalizedString
                    completion("error", errorMessage as AnyObject)
                }
                else
                {
                    completion("error", "Error. Please try again." as AnyObject)
                }
            }
        }
    } else
    {
        completion("error", "Error. Please try again." as AnyObject)
    }
                        
//                        if response.result.value != nil
//                        {
//                            let dict :NSDictionary = response.result.value! as! NSDictionary
//                            let status = dict.value(forKey: "status")as! String
//                            if status=="1"
//                            {
//                                //self.imgChat = nil
//                                print("DATA UPLOAD SUCCESSFULLY")
//                            }
//                        }
                }
            case .failure(let encodingError):
                break
            }
        }
        
        
       /*
        alamofireManager?.upload(multipartFormData: { (multipleData) in
            if file_type == "signup"
            {
                multipleData.append(imageData, withName: "profile_pic", fileName: "file.jpg", mimeType: "image/jpeg")
            }
            else if file_type == "image"
            {
                multipleData.append(imageData, withName: "file_name", fileName: "file.jpg", mimeType: "image/jpeg")
              }
            else if file_type == "video"
            {
                multipleData.append(imageData, withName: "file_name", fileName: "file.mov", mimeType: "video/quicktime")
             }
            for (key, value) in param! {
//                multipleData.append(value.data(using: String.Encoding.utf8), withName: key)
            }
            
        }, to: urlString, encodingCompletion: { (encodingResult) in
            
            
//            switch encodingResult {
//            case .Success(let upload, _, _):
//                    upload.responseJSON { response in
//                        debugPrint(response)
//                        if let JSON: AnyObject = response.result.value
//                        {
//                            if let dict: NSDictionary = JSON as? NSDictionary
//                            {
//                                print(dict)
//                                if(dict["status"] as! Bool == true)
//                                {
//                                    completion(result: "success", data: JSON)
//                                }else
//                                {
//                                    if let errorMessage : String = dict["message"] as? String
//                                    {
//                                        errorMessage.capitalizedString
//                                        completion(result: "error", data: errorMessage.capitalizedString)
//                                    }
//                                    else
//                                    {
//                                        completion(result: "error", data: "Error. Please try again.")
//                                    }
//                                }
//                            }
//                        } else
//                        {
//                            completion(result: "error", data: "Error. Please try again.")
//                        }
//                    }
//            case .failure(EncodingError)
//                print(encodingError)
//            }
        })
        */
        
        
//        let boundary = generateBoundaryString()
//
//        let url = URL(string: urlString)!
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        
        
        /*
         upload(.POST, urlString, multipartFormData: { MultipartFormData in
         if file_type == "signup"
         {
         MultipartFormData.appendBodyPart(data: imageData, name: "profile_pic", fileName: "file.jpg", mimeType: "image/jpeg")
         }
         else if file_type == "image"
         {
         MultipartFormData.appendBodyPart(data: imageData, name: "file_name", fileName: "file.jpg", mimeType: "image/jpeg")
         }
         else if file_type == "video"
         {
         MultipartFormData.appendBodyPart(data: imageData, name: "file_name", fileName: "file.mov", mimeType: "video/quicktime")
         }
         
         for (key, value) in param! {
         MultipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
         }
         }, encodingCompletion: { encodingResult in
         
         switch encodingResult {
         case .Success(let upload, _, _):
         upload.responseJSON { response in
         debugPrint(response)
         if let JSON: AnyObject = response.result.value
         {
         if let dict: NSDictionary = JSON as? NSDictionary
         {
         print(dict)
         if(dict["status"] as! Bool == true)
         {
         completion(result: "success", data: JSON)
         }else
         {
         if let errorMessage : String = dict["message"] as? String
         {
         errorMessage.capitalizedString
         completion(result: "error", data: errorMessage.capitalizedString)
         }
         else
         {
         completion(result: "error", data: "Error. Please try again.")
         }
         }
         }
         } else
         {
         completion(result: "error", data: "Error. Please try again.")
         }
         }
         case .Failure(let encodingError):
         print(encodingError)
         }
         }
         )
         */
    }
    
    func callPostWithMultipartService(_ urlString: String, param : [String: AnyObject]?, imageData : Data,  completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
     
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 60.0 // seconds
        
//        let urlWithParams = urlString+"?"+(param?.stringFromHttpParameters())!
        
//        print("UrlWith Param \(urlWithParams)")
        let Url = String(format: urlString)
        guard let serviceUrl = URL(string: Url) else { return }
       
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "gupanetwork.com": .disableEvaluation
        ]
        
        alamofireManager = Alamofire.SessionManager.init(configuration: configuration, delegate: SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager.init(policies: serverTrustPolicies))
        alamofireManager?.request(serviceUrl, method: .post, parameters: param, encoding:  JSONEncoding.default, headers: nil).response(completionHandler: {
            response in
            print(response.request ?? "");
            
            
            if let JSON: AnyObject = response.data as? AnyObject
            {
                if let dict: NSDictionary = JSON as? NSDictionary
                {
                    print(dict)
                    if(dict["status"] as! Bool == true)
                    {
                        completion("success", JSON)
                    }
                    else
                    {
                        if let errorMessage : String = dict["message"] as? String
                        {
                            completion("error", errorMessage.localizedCapitalized as AnyObject)
                        }
                        else
                        {
                            completion("error", "Error. Please try again." as AnyObject)
                        }
                    }
                }
            }
            else
            {
                completion("error", "Error. Please try again." as AnyObject)
            }
        })
        
    
        //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
       /*
         print("Parameter \(param)")
         let parameterDictionary = param
         var request = URLRequest(url: serviceUrl)
         request.httpMethod = "POST"
         request.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
         request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
         guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary ?? "", options: []) else {
         return
         }
         request.httpBody = httpBody
         
         let session = URLSession.shared
         session.dataTask(with: request) { (data, response, error) in
         if let response = response {
         print(response)
         }
         if let data = data {
         do {
         let json = try JSONSerialization.jsonObject(with: data, options: [])
         print(json)
         }catch {
         print(error)
         }
         }
         }.resume()
         */
        
        /*
        alamofireManager?.upload(multipartFormData: { (multipleData) in
           
            if imageData.count != 0 {
                 multipleData.append(imageData, withName: "profile_pic", fileName: "file.jpg", mimeType: "image/jpeg")
            }
            
           
            for (key, value) in param! {
                //                multipleData.append(value.data(using: String.Encoding.utf8), withName: key)
            }
            
        }, to: urlString, encodingCompletion: { (encodingResult) in
            
            
            //            switch encodingResult {
            //            case .Success(let upload, _, _):
            //                    upload.responseJSON { response in
            //                        debugPrint(response)
            //                        if let JSON: AnyObject = response.result.value
            //                        {
            //                            if let dict: NSDictionary = JSON as? NSDictionary
            //                            {
            //                                print(dict)
            //                                if(dict["status"] as! Bool == true)
            //                                {
            //                                    completion(result: "success", data: JSON)
            //                                }else
            //                                {
            //                                    if let errorMessage : String = dict["message"] as? String
            //                                    {
            //                                        errorMessage.capitalizedString
            //                                        completion(result: "error", data: errorMessage.capitalizedString)
            //                                    }
            //                                    else
            //                                    {
            //                                        completion(result: "error", data: "Error. Please try again.")
            //                                    }
            //                                }
            //                            }
            //                        } else
            //                        {
            //                            completion(result: "error", data: "Error. Please try again.")
            //                        }
            //                    }
            //            case .failure(EncodingError)
            //                print(encodingError)
            //            }
        })
        */
/* upload(.POST, urlString, multipartFormData: { MultipartFormData in
         if imageData.length != 0 {
         MultipartFormData.appendBodyPart(data: imageData, name: "profile_pic", fileName: "file.jpg", mimeType: "image/jpeg")
         }
         
         for (key, value) in param! {
         
         MultipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
         }
         }, encodingCompletion: { encodingResult in
         
         switch encodingResult {
         case .Success(let upload, _, _):
         upload.responseJSON { response in
         debugPrint(response)
         if let JSON: AnyObject = response.result.value
         {
         if let dict: NSDictionary = JSON as? NSDictionary
         {
         print(dict)
         if(dict["status"] as! Bool == true)
         {
         completion(result: "success", data: JSON)
         }else
         {
         if let errorMessage : String = dict["message"] as? String
         {
         errorMessage.capitalizedString
         completion(result: "error", data: errorMessage.capitalizedString)
         }
         else
         {
         completion(result: "error", data: "Error. Please try again.")
         }
         }
         }
         } else
         {
         completion(result: "error", data: "Error. Please try again.")
         }
         }
         case .Failure(let encodingError):
         print(encodingError)
         }
         }
         )
         */
    }
    
    func callGetService(_ urlString: String, param : [String: AnyObject]?,  completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 60.0 // seconds
        
        var urlWithParams = String()
        if param == nil {
          urlWithParams = urlString
        } else {
          urlWithParams = urlString+"?"+(param?.stringFromHttpParameters())!
        }
        
        // Create NSURL Ibject
        let mainUrl = NSURL(string: urlWithParams);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(url:mainUrl! as URL);
        
        // Set request HTTP method to GET. It could be POST as well
        request.httpMethod = "GET"
        
        // Excute HTTP Request
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil {
                
                print(error!.localizedDescription)
                
            } else {
                
                do {
                    
                    if let dict = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                    {
                        //Implement your logic
                        print(dict)
                         DispatchQueue.main.async {
                            if(dict["status"] as! Bool == true){
                                print("True")
                                completion("success", dict)
                            }else{
                                
                                if let errorMessage : String = dict["message"] as? String
                                {
                                    completion("error", errorMessage.capitalized as AnyObject)//errorMessage.capitalized)
                                } else{
                                    completion("error","Error. Please try again." as AnyObject)
                                }
                            }
                        }
                    }
                    
                } catch {
                    
                    print("error in JSONSerialization")
                    
                }
            }
            
        }
        task.resume()
        
        
//        alamofireManager = Alamofire.Manager(configuration: configuration)
        
//        var alamofireManager : Alamofire.Manager?
//        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
//        configuration.timeoutIntervalForResource = 30000 // seconds
//        let URL = NSURL(string: urlString)!
//        let mutableURLRequest = NSMutableURLRequest(URL: URL)
//        mutableURLRequest.timeoutInterval = 30000.0
//
//        alamofireManager = Alamofire.Manager(configuration: configuration)
//        alamofireManager!
       
        /*
         Alamofire.request(.GET, urlString, parameters: param).responseJSON {
         response in
         if let JSON: AnyObject = response.result.value
         {
         if let dict: NSDictionary = JSON as? NSDictionary
         {
         print(dict)
         if(dict["status"] as! Bool == true)
         {
         completion(result: "success", data: JSON)
         }
         else
         {
         if let errorMessage : String = dict["message"] as? String
         {
         errorMessage.capitalizedString
         completion(result: "error", data: errorMessage.capitalizedString)
         } else{
         completion(result: "error", data: "Error. Please try again.")
         }
         }
         }
         }
         else
         {
         completion(result: "error", data: "Error. Please try again.")
         }
         }
         */
    }
    
    func callFaceBookService(_ urlString: String, param : [String: AnyObject]?,  completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 60.0 // seconds
        
        var urlWithParams = String()
        if param == nil {
            urlWithParams = urlString
        }else{
            urlWithParams = urlString+"?"+(param?.stringFromHttpParameters())!
        }
        
        // Create NSURL Ibject
        let mainUrl = NSURL(string: urlWithParams);
        
        // Creaste URL Request
        let request = NSMutableURLRequest(url:mainUrl! as URL);
        
        // Set request HTTP method to GET. It could be POST as well
        request.httpMethod = "GET"
        
        // Excute HTTP Request
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil {
                
                print(error!.localizedDescription)
                
            } else {
                
                do {
                    
                    if let dict = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                    {
                        //Implement your logic
                        print(dict)
                        DispatchQueue.main.async {
                            if(dict["status"] as! Bool == true){
                                print("True")
                                completion("success", dict)
                            }else{
                                
                                if let errorMessage : String = dict["message"] as? String
                                {
                                    completion("error", errorMessage.capitalized as AnyObject)//errorMessage.capitalized)
                                } else{
                                    completion("error","Error. Please try again." as AnyObject)
                                }
                            }
                        }
                    }
                    
                } catch {
                    
                    print("error in JSONSerialization")
                    
                }
            }
            
        }
        task.resume()
    
    }
    
    func getImageFromUrl(_ urlString : String) -> UIImage
    {
        var image : UIImage = UIImage(named: "logo")!
        let urlNew:String = urlString.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        if let imageData = try? Data(contentsOf: URL(string: urlNew)!)
        {
            image = UIImage(data: imageData)!
            print(image)
            return image
        }
        else
        {
            return image
        }
    }
    
    //==============================
    // MARK: - Utility Methods
    //==============================
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
//========================================
    // MARK: - Frequently Called Methods
//========================================
    
    func drawBorder(_ view: UIView, color: UIColor, borderWidth:CGFloat, cornerRadius:CGFloat)
    {
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = cornerRadius
    }
    
    func setNavigationBarTitleImage() -> UIImageView
    {
        var titleView : UIImageView
        // set the dimensions you want here
        titleView = UIImageView(frame:CGRect(x: 0, y: 0, width: 80, height: 22))
        // Set how do you want to maintain the aspect
        titleView.contentMode = .scaleAspectFill
        titleView.image = UIImage(named: "TalkbloxLogo")
        return titleView
        
    }
    
    func showAlert(_ errorMessage : String , errorTitle : String) -> UIAlertController
    {
        let errorAlert : UIAlertController = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
        errorAlert.view.backgroundColor = UIColor.white
        drawBorder(errorAlert.view, color: AppTheme().themeColorOrange, borderWidth: 3.0, cornerRadius: 12.0)
        let OKAction = UIAlertAction(title: "OK", style: .default)
            {
                (action) in
                // ...
        }
        errorAlert.addAction(OKAction)
        
        return errorAlert
    }
    
    func setTableProperties(_ table: UITableView) -> UITableView
    {
        table.autoresizingMask = UIViewAutoresizing.flexibleWidth
        
        table.layer.borderColor = themeColorOrange.cgColor
        table.layer.borderWidth = 1
        table.separatorColor = themeColorOrange
        table.separatorInset = UIEdgeInsetsMake(0, 0, 0, 15);
        table.tableFooterView = UIView()
        
        return table
    }
    
    func convertToStingWithNSDate(_ date: Date, withFormatString strFormater: String) -> String
    {
        ///@"yyyy-MM-dd HH(or hh):mm:ss a zzz"
        let dateFomatter: DateFormatter = DateFormatter()
        dateFomatter.dateFormat = strFormater
        let strDate: String = dateFomatter.string(from: date)
        return strDate
    }
    
    func getDocumentFilePath(_ strAppendPath : String) -> String
    {
        // Find the video in the app's document directory
        let paths = NSSearchPathForDirectoriesInDomains(
            FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory: AnyObject = paths[0] as AnyObject
        
        let dataPath = documentsDirectory.appending(strAppendPath)
        return dataPath
    }
    
    //==================================
    // MARK: - Prefrences Methods
    //==================================
    
    class func setNotiCount(_ count : NSInteger){
         UserDefaults.standard.set(count, forKey: kPrefrenceNotifiCount)
    }
    class func getNotiCount() -> NSInteger {
        return UserDefaults.standard.integer(forKey: kPrefrenceNotifiCount)
    }
    
    class func setIsLogin(_ isLogin : Bool){
         UserDefaults.standard.set(isLogin, forKey: kPrefrenceIsLogin)
    }
    class func getIsLogin() -> Bool {
        return UserDefaults.standard.bool(forKey: kPrefrenceIsLogin) 
    }
    class func setLoginDetails(_ UserParam: NSDictionary) {
        UserDefaults.standard.set(UserParam, forKey: kPrefrenceLoginDetails)
    }
    class func getLoginDetails() -> NSDictionary {
        return UserDefaults.standard.object(forKey: kPrefrenceLoginDetails) as! NSDictionary
    }
    
//    class func getMangoObjIdLoggedInUser() -> String {
//        return (UserDefaults.standard.object(forKey: kPrefrenceMangoUserID) as AnyObject).value(forKey: "mongo_obj") as! String
//    }
   
    class func getUserFullName() -> String {
        return (UserDefaults.standard.object(forKey: kPrefrenceLoginDetails) as AnyObject).value(forKey: "full_name") as! String
    }
    
    class func setFirstTimeFlight(_ isFirstTimeForFlight : Bool){
         UserDefaults.standard.set(isFirstTimeForFlight, forKey: kPrefrenceIsFirstTimeForFlight)
    }
    class func getIsFirstTimeFlight() -> Bool {
        return UserDefaults.standard.bool(forKey: kPrefrenceIsFirstTimeForFlight)
    }
    class func setFirstTimeTransport(_ isFirstTimeForTransport : Bool){
         UserDefaults.standard.set(isFirstTimeForTransport, forKey: kPrefrenceIsFirstTimeForTransport)
    }
    class func getIsFirstTimeTransport() -> Bool {
        return UserDefaults.standard.bool(forKey: kPrefrenceIsFirstTimeForTransport)
    }
    class func setFlightRatingData(_ setObject : AnyObject){
         UserDefaults.standard.set(setObject, forKey: kPrefrenceFlightData)
    }
    class func getFlightRatingData() -> AnyObject {
        return UserDefaults.standard.object(forKey: kPrefrenceFlightData)! as AnyObject
    }
    
    class func setTransportRatingData(_ obj: AnyObject) {
        let appDele = UIApplication.shared.delegate as! AppDelegate
        appDele.transportRatingData = obj
//         NSUserDefaults.standardUserDefaults().setObject(obj, forKey: kPrefrenceFlightData)
    }
    class func getTransportRatingData() -> AnyObject {
        let appDele = UIApplication.shared.delegate as! AppDelegate
        return appDele.transportRatingData
//        return NSUserDefaults.standardUserDefaults().objectForKey(kPrefrenceFlightData)!
    }
    
    class func setAirportCode(_ UserParam: NSString) {
        UserDefaults.standard.set(UserParam, forKey: kPrefrenceAirportCode)
    }
    class func getAirportCode() -> NSString {
        return UserDefaults.standard.object(forKey: kPrefrenceAirportCode) as! NSString
    }
    
    class func setPresentLocation(_ UserPresentLoc: NSString) {
        UserDefaults.standard.set(UserPresentLoc, forKey: kPrefrencePresentLocation)
    }
    class func getPresentLocation() -> NSString {
        return UserDefaults.standard.object(forKey: kPrefrencePresentLocation) as! NSString
    }
    
    class func setUserStatus(_ UserParam: NSString) {
        UserDefaults.standard.set(UserParam, forKey: kPrefrenceCheckUserStatus)
    }
    class func getUserStatus() -> NSString {
        return UserDefaults.standard.object(forKey: kPrefrenceCheckUserStatus) as! NSString
    }
    
    class func setUserStatusInt(_ UserParam: Int) {
        UserDefaults.standard.set(UserParam, forKey: kPrefrenceCheckUserStatus)
    }
    class func getUserStatusInt() -> Int {
        return UserDefaults.standard.object(forKey: kPrefrenceCheckUserStatus) as! Int
    }
    class func setUserFlight(_ UserParam: Int) {
        UserDefaults.standard.set(UserParam, forKey: kPrefrenceCheckUserFlight)
    }
    class func getUserFlight() -> Int {
        return UserDefaults.standard.integer(forKey: kPrefrenceCheckUserFlight) 
    }
    
    class func setHotelId(_ hotelId : Int)
    {
        UserDefaults.standard.set(hotelId, forKey: kPrefrenceCheckHotelId)
    }
    class func getHotelId() -> Int {
        return UserDefaults.standard.integer(forKey: kPrefrenceCheckHotelId)
    }
    
    class func setDestinationStopDetails(_ ResponseData: NSMutableArray){
        UserDefaults.standard.set(ResponseData, forKey: kPrefrenceDestinationStopDetails)
    }
    class func getDestinationStopDetails() -> NSMutableArray {
        return UserDefaults.standard.object(forKey: kPrefrenceDestinationStopDetails) as! NSMutableArray
    }

    class func getUserEmail() -> String {
        return (UserDefaults.standard.object(forKey: kPrefrenceLoginDetails) as AnyObject).value(forKey: "email") as! String
    }
    
    class func setUserEmail_Login(_ UserParam: NSString) {
        UserDefaults.standard.set(UserParam, forKey: kPrefrenceCheckUserEmail_Login)
    }
    class func getUserEmail_Login() -> NSString {
        return UserDefaults.standard.object(forKey: kPrefrenceCheckUserEmail_Login) as! NSString
    }
    
//    class func setUserName_Login(_ UserParam: String) {
//        UserDefaults.standard.set(UserParam, forKey: kPrefrenceCheckLoginUserName)
//    }
//    class func getUserName_Login() -> String  {
//        return UserDefaults.standard.object(forKey: kPrefrenceCheckLoginUserName) as! String
//    }
    
    class func setStatusHotelFlightDetail(_ Status: NSString) {
        UserDefaults.standard.set(Status, forKey: kPrefrenceStatusHotelDetailFlightDetail)
    }
    
    class func getStatusHotelFlightDetail() -> NSString {
        return UserDefaults.standard.object(forKey: kPrefrenceStatusHotelDetailFlightDetail) as! NSString
    }

    //=======================================
    // Mark: NavigationBar button design
    //=======================================
    
    class func AddNavigationLeftBarButton(_ revealViewController: SWRevealViewController) -> UIBarButtonItem {

    let btnLeftBar: UIButton = UIButton ()
    btnLeftBar.setImage(UIImage(named: "Menu.png"), for: UIControlState())
    btnLeftBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
    btnLeftBar.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
    
    //.....Set left button
    let leftBarButton:UIBarButtonItem = UIBarButtonItem()
    leftBarButton.customView = btnLeftBar
    
    return leftBarButton
    
    }
    
//    class func AddNavigationLeftBarButtonForBack(revealViewController: TermsAndConditionController) -> UIBarButtonItem {
//        
//        let btnLeftBar: UIButton = UIButton ()
//        btnLeftBar.setImage(UIImage(named: "Home-icon.png"), forState: .Normal)
//        btnLeftBar.frame = CGRectMake(0, 0, 30, 30)
//        btnLeftBar.addTarget(revealViewController, action: #selector(revealViewController.backAction(_:)), forControlEvents: .TouchUpInside)
//        
//        //.....Set left button
//        let leftBarButton:UIBarButtonItem = UIBarButtonItem()
//        leftBarButton.customView = btnLeftBar
//        
//        
//        return leftBarButton
//        
//    }
    
    //RightBar button
    
    class func AddNavigationRightBarButton(_ backAction :HomeVC) -> UIBarButtonItem {
        let btnRighBar: UIButton = UIButton ()
        btnRighBar.setImage(UIImage(named: "Home-icon.png"), for: UIControlState())
        btnRighBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnRighBar.addTarget(backAction, action: Selector("homeButton"), for: .touchUpInside)
        
        //.....Set right button
        let rightBarButton:UIBarButtonItem = UIBarButtonItem()
        rightBarButton.customView = btnRighBar
        
        return rightBarButton

    }
    
//    class func SetWalkStatus(YesNo : Bool){
//        return NSUserDefaults.standardUserDefaults().setBool(YesNo, forKey: kPrefrenceWalkStatus)
//    }
//    class func getWalkStatus() -> Bool {
//        return NSUserDefaults.standardUserDefaults().boolForKey(kPrefrenceWalkStatus)
//    }

    
//    class func setWalkStartTime(startTime: NSString) {
//        NSUserDefaults.standardUserDefaults().setObject(startTime, forKey: kPrefrenceWalkStartTime)
//    }
//        class func getWalkStartTime() -> String {
//        return NSUserDefaults.standardUserDefaults().objectForKey(kPrefrenceWalkStartTime) as! String
//    }
    
    
   //========================================
     //MARK: Get station code method
   //========================================
    
    func callGetCode(_ urlString: String, param : [String: AnyObject]?,  completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
        var urlMainString = ""
        
        print("Param is \(String(describing: param))")
        
        if param == nil {
            urlMainString = urlString
        }
        else {
            
            let myString = param?.stringFromHttpParameters()
            print("myString is \(String(describing: myString))")
            urlMainString = urlString+myString!
        }
        print("urlMainString \(urlMainString) and Param is \(String(describing: param))")
        
        let mainUrl = NSURL(string: urlMainString);
        let request = NSMutableURLRequest(url:mainUrl! as URL);
        request.httpMethod = "GET"
        request.timeoutInterval = 5.0
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil {
                print(error!.localizedDescription)
                
            } else {
                do {
                    if let dict = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                    {
                        //Implement your logic
                        print("Dictionary code ==== 11111 \(dict)")
                        completion("success", dict)
                    } else {
                        completion("error", "" as AnyObject)
                    }
                    
                } catch {
                    print("error in JSONSerialization")
                }
            }
            
        }
        task.resume()
        
//        let URL = NSURL(string: "https://httpbin.org/get")!
//        let mutableURLRequest = NSMutableURLRequest(URL: URL)
//        mutableURLRequest.timeoutInterval = 5.0
        
        /*
         Alamofire.request(.GET, urlString, parameters: param)
         .responseJSON {
         response in
         if response.result.value != nil
         {
         
         if let JSON: AnyObject = response.result.value
         {
         if let dict: NSDictionary = JSON as? NSDictionary
         {
         //  print(dict)
         completion(result: "success", data: JSON)
         }
         }
         }else
         {
         completion(result: "error", data: "")
         }
         }
         */
    }
    
    func notPrettyString(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    func convertDictToString(_ dict: NSDictionary?) -> String{
        
        let parts: NSMutableArray = []
        if(dict != nil)
        {
            for (key,value) in dict!{
                let part = String(format: "%@=%@", urlEncode(key as AnyObject?)!,urlEncode(value as AnyObject?)!)
                parts.add(part)
            }
        }
        
        let queryString: NSString = (parts.componentsJoined(by: "&") as NSString)
        
        return queryString as String
    }
    
    func UTCtoLocalTimeConvert(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let date = dateFormatter.date(from: date)  // create   date from string
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "EEE, MMM d, yyyy - h:mm a"
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
    func toString(_ object: String?) -> String?{
        
        return String(format: "%@", object! as NSString)
    }
    
    func urlEncode(_ object: AnyObject?) -> String?{
        var string = String()
        
        if object is String{
            string = toString(object as? String)!
        }else{
            let convertedValue = String(describing: string)
            string = toString(convertedValue)!
        }
        
//        let string: String = toString(object)!
        return string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
    
    func convertDicToStr(_ dict: NSDictionary) -> String{
        do{
            if let data = try JSONSerialization.data(withJSONObject: dict, options: []) as? Data {
                if let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return json as String
                }
            }
        } catch let error as NSError {
            //AppConstant.klogString(error.localizedDescription)
        }
        
        return ""
    }
}


extension Dictionary {
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func stringFromHttpParameters() -> String {
       var percentEscapedValue = ""
        let parameterArray = map { key, value1 -> String in
            let percentEscapedKey = (key as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            
            if value1 is Int {
                print("Integer Value")
            } else if value1 is Float{
                print("Float Value is \(value1)")
                
                let number : NSNumber = NSNumber(floatLiteral: value1 as! Double)
                percentEscapedValue = number.stringValue
                print("percentEscapedValue is \(percentEscapedValue)")
            }else {
                percentEscapedValue = (value1 as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!

            }
            
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
}
