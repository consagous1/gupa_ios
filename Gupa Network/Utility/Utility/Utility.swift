//
//  Utility.swift
//  Gupa Network
//
//  Created by mac on 6/8/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit
import CoreLocation
import JGProgressHUD
import Reachability

let appDelegate = UIApplication.shared.delegate as! AppDelegate

//** This notification is posted when First time network become available
let notificationNetworkBecomeAvailableFirstTime     = "com.Gupa.NetworkBecomeAvailableFirstTime"
let notificationNetworkAvailable                    = "com.Gupa.NetworkAvailable"

//** MARK: Global Properties
var isNetworkAvailable = false

class Utility: NSObject {
    
    class func getValueForObject(_ sender: AnyObject)-> AnyObject
    {
        if sender as! NSObject  == NSNull()
        {
            return sender
        }else
        {
            return  sender
        }
    }
    
    class func showNetWorkAlert()
    {
        showAlertWithMessage("Gupa requires an internet connection. Please check your connection and try again.", title:"")
    }
    
    class func showAlertWithMessage(_ message: String, title: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        appDelegate.window?.rootViewController!.present(alert, animated: true, completion: nil)
    }
    
    class func getValidString(_ string: String?) -> (String) {
        
        if string == nil || string!.isKind(of: NSNull.self) || string == "null" || string == "<null>" || string == "(null)" {
            
            return ""
        }
        return string!.trimmingCharacters(in: CharacterSet.whitespaces)
    }
}

extension UIViewController {
    
//    func isRechableToInternet() -> Bool {
////        if isConnected() == false {
////            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel:  "Please check network connection!", showInView: self.view)
////            //            showAlert(title: "Error", message: "Internet Connection is not avilable on your device.")
////            return false
////        }else{
////            return true
////        }
//    }
    
    func isGPSOn() -> Bool {
        
        if (CLLocationManager .locationServicesEnabled() == false || (CLLocationManager .authorizationStatus() == CLAuthorizationStatus.denied || CLLocationManager .authorizationStatus() == CLAuthorizationStatus.restricted)) {
            self.hudType(JGProgressHUDErrorIndicatorView(), textLabel: "GPS seems off! Please ON GPS services. Please ON GPS services first for the Gupa.", showInView: self.view)
            //            let alert = UIAlertController(title: "Your title", message: "GPS access is restricted. In order to use tracking, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: UIAlertControllerStyle.Alert)
            //            alert.addAction(UIAlertAction(title: "Go to Settings now", style: UIAlertActionStyle.Default, handler: { (alert: UIAlertAction!) in
            //                print("")
            //              //  UIApplication.sharedApplication().openURL(NSURL(string:UIApplicationOpenSettingsURLString)!)
            //            }))
            
            //            showAlert(title: "Error", message: "GPS access is restricted. In order to use tracking, please enable GPS in the Settigs app under Privacy, Location Services.".localized())
            return false
        }else{
            
            return true
        }
    }
    
    
    //func isConnected() -> Bool {
//        let r : Reachability = Reachability (hostName: "www.google.com")
//        let status = r .currentReachabilityStatus() as NetworkStatus
//
//        if status == NetworkStatus.ReachableViaWiFi ||  status == NetworkStatus.ReachableViaWWAN{
//            return true
//        }else{
//            return false
//        }
   // }
    
    func startLoader(_ textLabel: String, showInView: UIView) -> UIView {
        var loading : JGProgressHUD!
        loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        loading.textLabel.text = textLabel
        loading.show(in: showInView)
        return loading
    }
    // Stop activity indicator.
    func stopActivity() {
        let loading : JGProgressHUD!
        loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        loading.dismiss()
    }
    
    
    func hudType(_ indicatorView: JGProgressHUDIndicatorView, textLabel: String, showInView: UIView) -> UIView {
        var loading : JGProgressHUD!
        loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
        loading.indicatorView = indicatorView
        loading.textLabel.text = textLabel
        loading.show(in: showInView)
        loading.dismiss(afterDelay: 2.2)
        return loading
    }
    
    func viewCornerDesign(_ inputView: UIView, masksToBounds: Bool, cornerRadius: CGFloat, showInView: UIView) -> UIView {
        inputView.layer.shadowColor = UIColor.gray.cgColor
        inputView.layer.shadowOffset = CGSize(width: 0, height: 2);
        inputView.layer.shadowOpacity = 1;
        if showInView.isEqual(nil) == false{
            
        }
        else
        {
            inputView.addSubview(showInView)
        }
        return inputView
    }
    
 
}
