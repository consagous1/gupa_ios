//
//  ReviewCell.swift
//  Gupa Network
//
//  Created by mac on 6/9/16.
//  Copyright © 2016 mobiweb. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgReview: UIImageView!
    @IBOutlet weak var lblUserReview: UILabel!
    @IBOutlet weak var lblDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgUser.layer.cornerRadius = 12.5 // self.imgUser.frame.height/2
        self.imgUser.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
